# $Id: Makefile,v 1.9 2013/11/14 05:23:42 brian Exp $
include ${FSLCONFDIR}/default.mk
#include systemvars.mk

PROJNAME = su_mvdisc
#CC = llvm-gcc
#CXX = llvm-g++
#ARCHFLAGS = -arch x86_64
#ARCHLDFLAGS = -arch x86_64 
#ARCHFLAGS+=-stdlib=libc++
#ARCHLDFLAGS+=-stdlib=libc++

ARCHFLAGS+=-std=c++11
ARCHLDFLAGS+=-std=c++11
INC_NEWMAT=./newmat
#/usr/local/fsl/extras/include/newmat
LIB_NEWMAT=./newmat
#/usr/local/fsl/extras/lib/


USRINCFLAGS += -I${INC_NEWMAT}  -I../misc_utils -I..//clapack_utils -I..//common -I/usr/local/include/freetype2/
USRINCFLAGS += -I./include -I/usr/local/include -I${FSLDIR}/include 
# -I./utils -I./newimage -I./miscmaths

USRLDFLAGS = -L./ -L${LIB_NEWMAT} -L..//misc_utils -L..//clapack_utils  -L./libs -L./utils -L${FSLDIR}/extras/lib/ -L/usr/local/lib
# -L./utils -L./newimage -L./miscmaths -L..//misc_utils/cprob

#LIBS3RD = -lsvm 
GLLIBS=-L/usr/local/lib -lftgl -framework GLUT -framework OpenGL


FMRIBLIBS= -lnewimage -lmiscmaths -lprob -lfslio -lniftiio -lznz -lnewmat  -lutils -lz

XFILES= discriminate clustering image_info corrp
#SCRIPTS = run_first run_first_all first_flirt concat_bvars first_roi_slicesdir first_boundary_corr surface_fdr

headers = ClassificationTree.h ClassificationTreeNode.h data_source.h image_source.h TextFileSource.h BaseFilter.h base_image_filter.h NormalizeFilter.h cluster_filter.h pca_filter.h glm_filter.h group_select_filter.h target.h base_mvdisc.h lda.h svm_disc.h   reliability_filter.h DataWriter.h nifti_writer.h

objects = ClassificationTree.o ClassificationTreeNode.o data_source.o image_source.o TextFileSource.o BaseFilter.o base_image_filter.o NormalizeFilter.o cluster_filter.o pca_filter.o glm_filter.o group_select_filter.o target.o base_mvdisc.o lda.o svm_disc.o reliability_filter.o run_loo.o  DataWriter.o nifti_writer.o

libraries = libsu_mvdisc.a libdatasource.a libdatawriter.a libdatafilter.a libClassificationTree.a 

LIBS = -lClassificationTree -lClassificationTreeNode  -lwrappers -lclassifiers -ldataSource -ldatawriter -lDataFilter  -lmisc_utils -lclapack_utils

FMBLIBS= -lnewimage -lmiscmaths -lfslio -lniftiio -lznz -lnewmat  -lutils -lz 



UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Linux)
	GNU_ANSI_FLAGS += -DARCHLINUX
	CXX_CFLAGS += -DARCHLINUX
    LIBS3RD = -L..//libs -llapack_LINUX -lcblas_LINUX -lblas_f2c_LINUX -lblas_LINUX  -lf2c -lsvm_linux

else


	LIBS3RD = -lsvm_osx
	FRAMEWORKS      =  -framework Accelerate
endif






all: classification_tree classification_tree_node classifiers  data_source data_writer data_filter node_wrappers  ${XFILES} 
#gaussian_process_test


classification_tree: ClassificationTree.o tree_sampler.o
	${AR} -r libClassificationTree.a ClassificationTree.o tree_sampler.o
classification_tree_node: ClassificationTreeNode.o target.o
	${AR} -r libClassificationTreeNode.a ClassificationTreeNode.o target.o

data_source: classification_tree_node data_source.o image_source.o TextFileSource.o  
	${AR} -r libdataSource.a data_source.o image_source.o TextFileSource.o

data_filter: classification_tree_node BaseFilter.o base_image_filter.o NormalizeFilter.o fill_data.o pca_filter.o glm_filter.o group_select_filter.o mask_filter.o gp_filter.o rv_transform.o quantile_regression.o dimension_select_filter.o cluster_filter.o
	${AR} -r libDataFilter.a BaseFilter.o base_image_filter.o NormalizeFilter.o fill_data.o pca_filter.o glm_filter.o group_select_filter.o mask_filter.o gp_filter.o rv_transform.o quantile_regression.o dimension_select_filter.o  cluster_filter.o

classifiers: classification_tree_node classification_tree base_mvdisc.o lda.o qda.o svm_disc.o decisionTree.o log_regression.o log_regression.o classifierFusion.o
	${AR} -r libclassifiers.a base_mvdisc.o lda.o qda.o svm_disc.o decisionTree.o log_regression.o log_regression.o classifierFusion.o

node_wrappers: classification_tree classification_tree_node  data_filter data_source reliability_filter.o  adaBoost.o
	${AR} -r libwrappers.a reliability_filter.o adaBoost.o

data_writer:  classification_tree_node  DataWriter.o nifti_writer.o  
	${AR} -r libdatawriter.a DataWriter.o nifti_writer.o  

discriminate: discriminate.o classification_tree classification_tree_node data_source data_filter node_wrappers classifiers data_writer
	${CXX} ${CXXFLAGS} ${LDFLAGS} -o $@ discriminate.o ${LIBS} ${LIBS3RD} ${FRAMEWORKS} ${FMRIBLIBS} 

gaussian_process:  gaussian_process.o classification_tree classification_tree_node data_source data_filter classifiers data_writer
	${CXX} ${CXXFLAGS} ${LDFLAGS} -o $@ gaussian_process.o ${LIBS} ${LIBS3RD} ${FRAMEWORKS} ${FMRIBLIBS}


clustering:  clustering.o classification_tree classification_tree_node data_source data_filter classifiers data_writer
	${CXX} ${CXXFLAGS} ${LDFLAGS} -o $@ clustering.o  ${LIBS} ${LIBS3RD} ${FRAMEWORKS} ${FMRIBLIBS} -lboost_filesystem -lboost_system ${GLLIBS}

corrp:  corrp.o 
	${CXX} ${CXXFLAGS} ${LDFLAGS} -o $@ corrp.o -lmisc_utils  -lboost_filesystem -lboost_system

image_info:  image_info.o
	${CXX} ${CXXFLAGS} ${LDFLAGS} -o $@ image_info.o ${FMRIBLIBS}



train_classifier: train_classifier.o classification_tree classification_tree_node data_source data_filter classifiers data_writer
	${CXX} ${CXXFLAGS} ${LDFLAGS} -o $@ train_classifier.o ${LIBS} ${LIBS3RD} ${FRAMEWORKS} ${FMRIBLIBS}


#install:
#	@if [ ! -d ${ETKINLAB_DIR}/include/su_mvdisc ] ; then \
		${CHMOD} -R a+w ${ETKINLAB_DIR}/ ; \
		${MKDIR} -p ${ETKINLAB_DIR}/include/su_mvdisc ; \
		else \
		${CHMOD} -R a+w ${ETKINLAB_DIR}/ ; \
	fi ; \
	${CP} ${headers} ${ETKINLAB_DIR}/include/su_mvdisc/
#	@if [ ! -d ${ETKINLAB_DIR}/lib ] ; then \
		${MKDIR} -p ${ETKINLAB_DIR}/lib ; \
	fi
#	${MV} ${libraries} ${ETKINLAB_DIR}/lib/

#	${CHMOD} -R a-w ${ETKINLAB_DIR}/ ; 
#clean :
#	-rm $(objects) depend.mk run_loo lib*.a
