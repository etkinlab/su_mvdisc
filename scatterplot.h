#ifndef SCATTERPLOTTER_H
#define SCATTERPLOTTER_H
#include <vector>
//#include "my_structs.h"
#include <FTGL/ftgl.h>
#include <misc.h>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp> // after <glm/glm.hpp>
#include <OpenGL/gl.h>


namespace su_mvdisc_gui_name{

class scatterPlot{

    public:
    scatterPlot();
    ~scatterPlot();
//    void setX( const std::vector<float> & x ) { v_values_x_ = x; }
//    void setY( const std::vector<float> & y ) { v_values_y_ = y; }
//    void setXY( const std::vector<float> & x,  const std::vector<float> & y ) { v_values_x_ = x; v_values_y_ = y;  }
//    void setDataPoints( const std::vector<float> & x,  const std::vector<float> & y );
    void setDataPoints( const std::vector<float2> & xy , float t = 0 );
    void generateBars( float t);
    void setEnablePoints( const bool & enable ) { do_points_ = enable; }
    void setEnableLines( const bool & enable ) { do_lines_ = enable; }
void setEnableBars( const bool & enable ) { do_bars_ = enable; }
//    void setScalars( const std::vector<float> & targ ) { v_target_ = targ;}
    void setTickSpacing( const float & val ) { tick_spacing_ = val; }
    void setTitle(const std::string & name ) {     Title_ =  name; }
    void setAxesLimits();
    void setAxesLimits( const float4 & limits );
    void updateAxesXFM();

//    void setVBOs( GLuint* vbos_in ) { vbos = vbos_in; }

    //------GL
    void initializeGL();
//    void setVBOs(GLuint* vbos_tree_in ) {vbos_tree_ = vbos_tree_in; }

    void paintGL();
    void paintTitle();
    void paintAxesValues();

    void createViewports();
    void setWindow( const float & left, const float & right, const float & bottom, const float & top  );//deault,-1,1,-1,1
void setPlotBounds( const float & left, const float & right, const float & bottom, const float & top  );
    void createAxes();
    void setShaders();
    void setModelViewProjMatrix();
    void setModelViewProjMatrixXFMmodel( const glm::vec3 & scale_fac );
    void setModelViewProjMatrixXFMmodel(const glm::vec3 & tx , const glm::vec3 & scale_fac );
    void setModelViewProjMatrixXFMview(const glm::vec3 & tx , const glm::vec3 & scale_fac );

    float render_depth_;
    GLint vertexLoc_, normalLoc_, scalarLoc_;
    GLuint* vbos_lines_;//verts, line_inds, linestrip_inds
    GLuint* vbos_triangles_;
    GLuint* vbos_datapts_;
    GLuint* vbos_bars_;

    GLuint v_index_cmap_,f_index_cmap_, p_index_cmap_;
    GLfloat r_lut[4],g_lut[4],b_lut[4],a_lut[4],sc_lut[4];
    GLint loc_r_lut,loc_g_lut,loc_b_lut,loc_a_lut, loc_sc_lut;

    //--------
    //void renderTickLabel(const float & val );

    //void renderTicks( const std::vector<float> & t_vals );
    private:
    bool do_buffer_;   
    bool do_bars_,do_lines_,do_points_;
    float tick_spacing_;
//    std::vector<float> v_values_x_,v_values_y_, v_target_;
  std::vector<float3> xyt_;//x,y coordinate and target label


    std::vector< vertex > triangles_verts_;
    std::vector< uint3 > triangle_inds_;

//Contains vertcies and indices for lines and line strips
    std::vector< vertex > lines_verts_;
    std::vector< uint2 > lines_inds_;
    //std::vector< unsigned int > linestrip_inds_;

    std::vector< vertex > datapts_verts_;
 std::vector< unsigned int > datapts_inds_;
 std::vector< vertex > bars_verts_;
std::vector< unsigned int > bars_inds_;



    float4 axes_lims_;
    float4 axes_truelims_;

    float2 axes_centre_;
    float2 axes_scale_, axes_tx_;

    float w_left_,w_right_,w_bottom_,w_top_;
    float plot_left_,plot_right_,plot_bottom_,plot_top_;
    float vertical_spacing_,horizontal_spacing_;
    float height_,width_;
    FTGLPolygonFont* TitleFont_;
//    FTFont* TitleTextureFont_;
//    GLuint textureID[2];

    unsigned int TitleFace_size_;
    FTSimpleLayout TitleText_;
    std::string Title_;
    FTSimpleLayout tick_values_;

    FTGLPolygonFont* AxisFont_;
    unsigned int Axis_size_;
//    FTSimpleLayout AxisText_;
//    std::string Title_;



};
}

#endif // SCATTERPLOTTER_H
