#ifndef ImageSource_H
#define ImageSource_H

/*
 *  image_source.h
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 7/20/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#include "data_source.h"

//STL include
#include <iostream>
#include <list>
#include <su_mvdisc_structs.h>

namespace su_mvdisc_name{
	
	class ImageSource : public DataSource 
	{
	public: 
		ImageSource();
		~ImageSource();
		
		void readFile( );
		void setOptions( std::stringstream & ss_options);

		
		
		std::vector<int>::iterator mask_begin();
		std::vector<int>::iterator mask_end();

		
		float4 res();
		float xres();
		float yres();
		float zres();
		float tres();

		uint3 size();
		unsigned int xsize();
		unsigned int ysize();
		unsigned int zsize();

		
	protected:

	private:
		enum masking { NONE, UNION, INTERSECTION };

		bool includeFeature( const int & cl);
        
        masking mask_mode;
		
		bool ApplyExternalMask;
		bool NormalizeDataPreMask;

		std::string ext_mask_filename, atlas_filename;
		bool use3Dimages;
        bool useMultiImages_t_eq_s;
        bool use4Dimages;
        bool useAtlas;
        std::list<int>* dim_include_;
	//	int USE_ONLY_NONZERO;
		std::vector<int> * mask;
		uint3 im_dims;
		float4 im_res;
	};
	
}

#endif
