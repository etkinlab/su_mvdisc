/*
 *  ClassificationTree.cpp
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 7/20/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#define MAX_LINE_SIZE 1024
#include <misc_utils.h>

#include <cstdlib>
#include <limits>
#include <iostream>
#include <fstream>

#include "ClassificationTree.h"
#include "all_nodes.h"


//#ifdef ARCHLINUX
//#include <cstdlib>

//#endif
//#ifdef ARCHDARWIN
//#endif


#include <sys/time.h>


using namespace std;
using namespace clapack_utils_name;
//using namespace su_mvdisc_name;
using namespace misc_utils_name;
namespace su_mvdisc_name{
    
    //constructor
    ClassificationTree::ClassificationTree()
    {
        
        Verbose_=0;
        target = NULL;
        test_target = NULL;
        treeTop_count=0;
        map_node_names["NodeNotFound"];
        totalNumberOfIterations=0;
        
        srand_fine();
        
    }
    //constructor given configuration file
    ClassificationTree::ClassificationTree( const std::string & filename )
    {
        Verbose_=0;
        target = NULL;
        test_target = NULL;
        treeTop_count=0;
        map_node_names["NodeNotFound"];
        totalNumberOfIterations=0;
        srand_fine(); //ensure that new seeds given small variations in start time
        
        //load from file
        load(filename);
        
        
    }
    
    //destructor
    ClassificationTree::~ClassificationTree()
    {
        if (target != NULL)
        {
            delete target;
            target=NULL;
        }
        if (target != NULL)
        {
            delete test_target;
            test_target=NULL;
        }
        
        for ( vector< DataSource* >::iterator i = data_sources.begin(); i!=data_sources.end(); i++)
        {
            delete *i;
            *i=NULL;
        }
        
        for ( vector< DataSource* >::iterator i = test_data_sources.begin(); i!=test_data_sources.end(); i++)
        {
            delete *i;
            *i=NULL;
            
        }
        
        for ( vector< ClassificationTreeNode* >::iterator i = tree_nodes.begin(); i!=tree_nodes.end(); i++)
        {
            delete *i;
            *i=NULL;
            
        }
        
        
    }
    
    std::string ClassificationTree::getTreeTopParameterFileName()
    {
        return tree_nodes[treeTop]->getParameterFileName();
        
    }
    
    TreeNodeType ClassificationTree::getTreeTopType()
    {
        return tree_nodes[treeTop]->getTreeNodeType();
    }
    
    TreeNodeType ClassificationTree::getTreeTopTypeSpec()
    {
        return tree_nodes[treeTop]->getTreeNodeTypeSpec();
    }
    
    
    //Provides a check to ensure that the number of subject found in the target (outcomes) is equal
    //to the number of samples
    void ClassificationTree::check_Ns_Targ_eq_Ns_Src(){
        
        for (vector<DataSource*>::iterator i = data_sources.begin(); i!=data_sources.end();i++)
        {
            if (target->getNumberOfSamples() != (*i)->getNumberOfInputSamples())
            {
                //                cerr<<"Shoudl be throwing an error here "<<endl;
                //                cerr<<"Shoudl be throwing an error here "<<num2str(target->getNumberOfSamples())<<" "<<num2str((*i)->getNumberOfInputSamples())<<endl;
                
                throw ClassificationTreeException(("Number of Samples in Target does not match that from Data Source. "+ num2str(target->getNumberOfSamples()) + " /  "+num2str((*i)->getNumberOfInputSamples())).c_str());
            }
        }
        
    }
    
    //Clears the stored output data. i.e. classifications and distances scores.
    void  ClassificationTree::clearOutputData()
    {
        estimated_class.clear();
        estimated_distance_scores.clear();
    }
    
    //Clears the entired tree. Target, nodes, associated data and outputs
    void ClassificationTree::clearAll()
    {
        if (target != NULL)
        {
            delete target;
            target=NULL;
        }
        for ( vector< DataSource* >::iterator i = data_sources.begin(); i!=data_sources.end(); i++)
        {
            delete *i;
        }
        
        for ( vector< ClassificationTreeNode* >::iterator i = tree_nodes.begin(); i!=tree_nodes.end(); i++)
        {
            delete *i;
        }
        
        map_node_names.clear();
        map_node_names["NodeNotFound"];
        estimated_class.clear();
        estimated_distance_scores.clear();
        treeTop_count=0;
    }
    
    
    
    bool ClassificationTree::storeGenData()
    {
        return static_cast<base_mvdisc*>(tree_nodes[treeTop])->getDoGeneralizationAccuracy();
    }
    
    bool ClassificationTree::storeGenDataThresh()
    {
        //store
        return (( static_cast<base_mvdisc*>(tree_nodes[treeTop])->getDoGeneralizationAccuracy() ==true ) && (static_cast<base_mvdisc*>(tree_nodes[treeTop])->getDoKFold() ==false));
    }
    
    
    
    TpFpTnFn ClassificationTree::getGeneralizationData() {
        //        cerr<<"TreeTop "<<treeTop<<" "<< tree_nodes[treeTop]->nodeName()<<endl;
        
        return static_cast<base_mvdisc*>(tree_nodes[treeTop])->getGeneralizationData();
    }
    
    smatrix ClassificationTree::getGeneralizationDataThresh() {
        return static_cast<base_mvdisc*>(tree_nodes[treeTop])->getGeneralizationDataThresh();
    }
    
    //Create a node in the tree given the specified names. This will be called when reading a configuration file.
    void ClassificationTree::createNode(  stringstream & ss_node )
    {
        vector<string> v_parent;
        string type_of_node, custom_name, parent;
        //int node_number;
        ss_node >> custom_name >> type_of_node ;
        if (Verbose_ >=1)
        {
            cout<<"     Creating node... "<<endl;
            cout<<"         type: "<<type_of_node<<endl;
            cout<<"         name: "<<custom_name<<endl;
        }
        if ( custom_name.length() == 0 )
        {
            return;
        }else if ( type_of_node == "Target" )
        {
            if (target!=NULL)
                throw ClassificationTreeNodeException("Trying to replace exisiting target.");
            
            target = new Target();
            target->setOptions(ss_node);
            
            if (Verbose_>=1)
            {
                cout<<"         reading target... ";
            }
            
            target->readFile();
            if (Verbose_>=1)
            {
                cout<<"  done"<<endl;
            }
            
        }else
        {
            map_node_names[custom_name]=tree_nodes.size()+1;//plus one because of NodeNotFOund
            ss_node>>parent;
            v_parent=csv_2_vector(parent);
            //            print(v_parent,"v_parent");
            vector<int> v_inds;
            for (vector<string>::iterator i_p = v_parent.begin(); i_p != v_parent.end();++i_p)
                v_inds.push_back(map_node_names[*i_p]-1);
            //            cerr<<"parent node "<<parent<<" "<<map_node_names[v_parent[0]]<<endl;
            if ( type_of_node == "ImageSource" )
            {
                ImageSource* node = new ImageSource();
                node->setOptions(ss_node);
                node->setName(custom_name);
                //	attachInputToLeaf(map_node_names[parent]-1, node);
                attachInputToLeaf(v_inds, node);
                //cout<<"created ImageSource"<<endl;
                
            }else if ( type_of_node == "TextSource" )
            {
                TextFileSource* node = new TextFileSource();
                //cout<<"created TextSource"<<endl;
                node->setOptions(ss_node);
                node->setName(custom_name);
                
                //attachInputToLeaf(map_node_names[parent]-1, node);
                attachInputToLeaf(v_inds, node);
                
                
                
            }else if ( type_of_node == "NormalizeFilter" )
            {
                NormalizeFilter* node = new NormalizeFilter();
                //cout<<"created NormalizeFilter"<<endl;
                node->setOptions(ss_node);
                node->setName(custom_name);
                
                //attachInputToLeaf(map_node_names[parent]-1, node);
                
                attachInputToLeaf(v_inds, node);
                
                
            }else if ( type_of_node == "FillDataFilter" )
            {
                FillDataFilter* node = new FillDataFilter();
                //cout<<"created NormalizeFilter"<<endl;
                node->setOptions(ss_node);
                node->setName(custom_name);
                
                //attachInputToLeaf(map_node_names[parent]-1, node);
                
                attachInputToLeaf(v_inds, node);
                
                
            }else if ( type_of_node == "ClusterFilter" )
            {
                ClusterFilter* node = new ClusterFilter();
                //cout<<"created NormalizeFilter"<<endl;
                node->setOptions(ss_node);
                node->setName(custom_name);

                //attachInputToLeaf(map_node_names[parent]-1, node);

                attachInputToLeaf(v_inds, node);


            }else if ( type_of_node == "PCA_Filter" )
            {
                PCA_Filter* node = new PCA_Filter();
                //cout<<"created LDA_mvdisc"<<endl;
                node->setOptions(ss_node);
                node->setName(custom_name);
                
                //attachInputToLeaf(map_node_names[parent]-1, node);
                attachInputToLeaf(v_inds, node);
                
                
                
            }else if ( type_of_node == "GLM_Filter" )
            {
                GLM_Filter* node = new GLM_Filter();
                //cout<<"created LDA_mvdisc"<<endl;
                node->setOptions(ss_node);
                node->setName(custom_name);
                
                //attachInputToLeaf(map_node_names[parent]-1, node);
                attachInputToLeaf(v_inds, node);
                
                
                
            }else if ( type_of_node == "RV_Transform" )
            {
                RV_Transform* node = new RV_Transform();
                //cout<<"created LDA_mvdisc"<<endl;
                node->setOptions(ss_node);
                node->setName(custom_name);
                
                // attachInputToLeaf(map_node_names[parent]-1, node);
                attachInputToLeaf(v_inds, node);
                
            }else if ( type_of_node == "GroupSelect_Filter" )
            {
                GroupSelect_Filter* node = new GroupSelect_Filter();
                node->setOptions(ss_node);
                node->setName(custom_name);
                
                //attachInputToLeaf(map_node_names[parent]-1, node);
                attachInputToLeaf(v_inds, node);
                
            }else if ( type_of_node == "DimensionSelect_Filter" )
            {
                DimensionSelect_Filter* node = new DimensionSelect_Filter();
                node->setOptions(ss_node);
                node->setName(custom_name);
                
                //attachInputToLeaf(map_node_names[parent]-1, node);
                attachInputToLeaf(v_inds, node);
                
            }else if ( type_of_node == "Reliability_Filter" )
            {
                Reliability_Filter* node = new Reliability_Filter();
                node->setOptions(ss_node);
                node->setName(custom_name);
                
                //attachInputToLeaf(map_node_names[parent]-1, node);
                attachInputToLeaf(v_inds, node);
                
            }else if (type_of_node == "Ada_Boost")
            {
                //                Ada_Boost* node = new Ada_Boost();
                //		node->setOptions(ss_node);
                //                node->setName(custom_name);
                
                //		attachInputToLeaf(map_node_names[parent]-1, node);
                
            }else if ( type_of_node == "QuantileRegression" ){
                QuantileRegression* node = new QuantileRegression();
                node->setOptions(ss_node);
                node->setName(custom_name);
                
                //attachInputToLeaf(map_node_names[parent]-1, node);
                attachInputToLeaf(v_inds, node);
                
            }else if ( type_of_node == "LDA" )
            {
                LDA_mvdisc* node = new LDA_mvdisc();
                cout<<"created LDA_mvdisc"<<endl;
                node->setOptions(ss_node);
                node->setName(custom_name);
                
                cout<<"created LDA_mvdisc - options are set"<<endl;
                
                //attachInputToLeaf(map_node_names[parent]-1, node);
                attachInputToLeaf(v_inds, node);
                
            }else if ( type_of_node == "QDA" )
            {
                QDA_mvdisc* node = new QDA_mvdisc();
                //cout<<"created QDA_mvdisc"<<endl;
                node->setOptions(ss_node);
                node->setName(custom_name);
                
                //attachInputToLeaf(map_node_names[parent]-1, node);
                attachInputToLeaf(v_inds, node);
                
            }else if ( type_of_node == "SVM" )
            {
                SVM_mvdisc* node = new SVM_mvdisc();
                //cout<<"created SVM_mvdisc"<<endl;
                node->setOptions(ss_node);
                node->setName(custom_name);
                
                //attachInputToLeaf(map_node_names[parent]-1, node);
                attachInputToLeaf(v_inds, node);
                
            }else if ( type_of_node == "DecisionTree" )
            {
                DecisionTree* node = new DecisionTree();
                //cout<<"created SVM_mvdisc"<<endl;
                node->setOptions(ss_node);
                node->setName(custom_name);
                
                //attachInputToLeaf(map_node_names[parent]-1, node);
                attachInputToLeaf(v_inds, node);
                
            }else if ( type_of_node == "LogisticRegression" )
            {
                log_regression_mvdisc* node = new log_regression_mvdisc();
                //cout<<"created log_regression_mvdisc"<<endl;
                node->setOptions(ss_node);
                node->setName(custom_name);
                
                //attachInputToLeaf(map_node_names[parent]-1, node);
                attachInputToLeaf(v_inds, node);
                
            }else if ( type_of_node == "GaussianProcessFilter" )
            {
                GaussianProcessFilter* node = new GaussianProcessFilter();
                //cout<<"created SVM_mvdisc"<<endl;
                node->setOptions(ss_node);
                node->setName(custom_name);
                
                //attachInputToLeaf(map_node_names[parent]-1, node);
                attachInputToLeaf(v_inds, node);
                
            }else if ( type_of_node == "ClassifierFusion" )
            {
                CLFusion_mvdisc * node = new CLFusion_mvdisc();
                //cout<<"created SVM_mvdisc"<<endl;
                node->setOptions(ss_node);
                node->setName(custom_name);
                
                //	attachInputToLeaf(map_node_names[parent]-1, node);
                attachInputToLeaf(v_inds, node);
            }else if ( type_of_node == "Clustering" )
            {
                ClusterFilter* node = new ClusterFilter();
                node->setOptions(ss_node);
                node->setName(custom_name);
                
                //	attachInputToLeaf(map_node_names[parent]-1, node);
                attachInputToLeaf(v_inds, node);
                
            }else {
                throw ClassificationTreeNodeException(("Tried to load node type: " + type_of_node + ", which does not exist").c_str());
                
            }
        }
        
    }
    std::list< std::vector<int> > ClassificationTree::readSelectionMask( const std::string & filename  )
    {
        std::list< std::vector<int> > sel_mask;
        ifstream fin(filename.c_str());
        char* line = new char[MAX_LINE_SIZE];
        
        while (fin.getline(line, MAX_LINE_SIZE))
        {
            sel_mask.push_back(csv_string2nums_i(string(line)));
        }
        return sel_mask;
    }
    
    std::list< std::vector<int> > ClassificationTree::readSubSampleMask_from_SelMask( const std::string & filename  )
    {
        std::list< std::vector<int> > sel_mask;
        ifstream fin(filename.c_str());
        char* line = new char[MAX_LINE_SIZE];
        vector<int> subsample_mask;
        unsigned int count=0;
        while (fin.getline(line, MAX_LINE_SIZE))
        {
            if (count==0)
            {
                subsample_mask = csv_string2nums_i(string(line));
                ++count;
            }else {
                vector<int> v_sel = csv_string2nums_i(string(line));
                vector<int>::iterator i_sel = v_sel.begin();
                for (vector<int>::iterator i = subsample_mask.begin(); i != subsample_mask.end() ; ++i , ++i_sel)
                    if (*i_sel != 0 )
                        *i=1;
            }
        }
        sel_mask.push_back(subsample_mask);
        return sel_mask;
    }
    
    //  void ClassificationTree::loadStructure(const string & filename)
    // {
    
    
    //}
    void  ClassificationTree::setLevels()
    {
        tree_nodes[treeTop]->setLevel(0);
    }
    
    std::list< std::pair<unsigned int, std::string> >  ClassificationTree::getLevelsAndNames()
    {
        list< pair<unsigned int, string> > l_level_name;
        for (vector<DataSource*>::iterator i = data_sources.begin(); i!=data_sources.end();i++)
        {
            l_level_name.push_back( pair<unsigned int, string>((*i)->getLevel(), (*i)->nodeName() ) );
        }
        
        for (vector<ClassificationTreeNode*>::iterator i = tree_nodes.begin(); i!=tree_nodes.end();i++)
        {
            
            l_level_name.push_back( pair<unsigned int, string>((*i)->getLevel(), (*i)->nodeName() ) );
        }
        
        l_level_name.sort();
        return l_level_name;
        
    }
    
    void ClassificationTree::load(const string & filename)
    {
        
        //loading an existing classification workflow
        if (Verbose_>1)
        {
            cout<<"ClassificationTree::load - "<<filename<<endl;
        }
        
        //clean up anything pre-existing
        clearAll();
        
        //on workflow file
        ifstream fin(filename.c_str());
        
        //make sure it could be opened
        if (!fin.is_open())
        {
            cerr<<"Could not open tree file : "<<filename<<endl;
            throw ClassificationTreeException("Could not open Classification Tree file");
        }
        
        //----------------------Read The File-------------------------//
        {
            string type_of_node;
            string line;
            while (getline(fin,line))
            {
                
                stringstream ss_node;
                
                //read the node type
                ss_node << line ;
                //read line up until comments
                getline(ss_node,line,'#');
                
                ss_node.str(line);
                ss_node.clear();
                
                if (Verbose_>2)
                    cout<<"Create node..."<<endl;
                
                createNode(ss_node);
                
                
            }
        }
        //----------------------Read The File-------------------------//
        
        
        fin.close();
        if (Verbose_>3)
            cout<<"ClassificationTree::load - closing file."<<endl;
        //this line will run the input data sources
        //cout<<"trag "<<target->getNumberOfSamples()<<" "<<data_sources[0]->getNumberOfSamples()<<endl;
        
        if (treeTop_count!=1)
            throw ClassificationTreeException("Error: TreeTop was set an invalid number of times (should only be set once).");
        
        if (target==NULL)
            throw ClassificationTreeException("Error: Tree loaded, but t=no target was specified.");
        //	cout<<"sample "<<target->getNumberOfSamples() <<data_sources[0]->getNumberOfSamples()<<endl;
        
        
        //this check actually causes the tree to read in all sources
        //check to make sure that number of samples in targets matches the number of samples
        
        //        check_Ns_Targ_eq_Ns_Src();
        
        //		for (vector<DataSource*>::iterator i = data_sources.begin(); i!=data_sources.end();i++)
        //			 {
        //				 if (target->getNumberOfSamples() != (*i)->getNumberOfSamples())
        //					 throw ClassificationTreeException("Number of Sample in Target does not match that from Data /Source.");
        //}
        
        
        
        setTargets();
        if (Verbose_>1)
        {
            cout<<"treetop "<<treeTop<<endl;
            cout<<"ClassificationTree::load - complete."<<endl;
        }
        
        
    }
    void ClassificationTree::save( const string & filename)
    {
        
    }
    
    std::vector<unsigned int> ClassificationTree::getInputChunks()
    {
        vector<unsigned int> v_chunks;
        for (std::vector< DataSource* >::iterator i_src = data_sources.begin(); i_src != data_sources.end(); ++i_src)
        {
            //            cerr<<"get chunks "<<endl;
            (*i_src)->setInputData();
            if ((*i_src)->getChunksPointer() != NULL){
                //                cerr<<"not NULL"<<endl;
                v_chunks.insert(v_chunks.end(), (*i_src)->getChunksBegin(), (*i_src)->getChunksEnd());
                //                print(v_chunks,"v_chunks");
            }
            //            cerr<<"done null check"<<endl;
            
        }
        return v_chunks;
    }
    
    
    std::vector<unsigned int> ClassificationTree::getNumberOfObservationsPerClass( ) const
    {
        return target->getNumberOfSubjectsPerClass();
    }
    std::vector<unsigned int> ClassificationTree::getNumberOfObservationsPerClass( const  std::vector<int> & v_mask ) const {
        
        
        return target->getNumberOfSubjectsPerClass(v_mask);
    }
    
    
    
    void ClassificationTree::writeOutput()
    {
        //	int count=0;
        cerr<<"write output"<<endl;
        for (vector<ClassificationTreeNode*>::iterator i = tree_nodes.begin(); i!=tree_nodes.end();i++)
        {
            
            (*i)->writeNodeOutputs();
        }
        cerr<<"done write output"<<endl;
    }
    void ClassificationTree::addDataSource(  DataSource * src )
    {
        data_sources.push_back(src);
    }
    void ClassificationTree::setTreeTop(   const unsigned int & index  )
    {
        cout<<"set tree top "<<index<<endl;
        treeTop=index;
        //	tree_nodes.push_back(topper);
    }
    void ClassificationTree::addRoot( ClassificationTreeNode * new_leaf)
    {
        vector<int> v_index(1,-1);
        attachInputToLeaf(v_index,new_leaf);
    }
    
    void ClassificationTree::attachToRoot( ClassificationTreeNode * new_leaf)
    {
        vector<int> v_index(1,treeTop);
        attachInputToLeaf(v_index,new_leaf);
    }
    
    
    void ClassificationTree::attachInputToLeaf( const vector<int> & index, ClassificationTreeNode * new_leaf)
    {
        //if ((index > static_cast<int>(tree_nodes.size())-1 ) && (index != -1))
        //	throw ClassificationTreeException("Tried to attach a node to one that doesn't exist");
        //		cerr<<"attach lead "<<index[0]<<endl;
        if (new_leaf->getTreeNodeType()==SOURCE)
        {
            //cout<<"push back src"<<endl;
            //never finsihed doing this , not sure if needed
            
            
            //			if (dynamic_cast<DataSource*>(new_leaf)->isTestSource()){
            //				cout<<"set data as test source"<<endl;
            //				test_data_sources.push_back(dynamic_cast<DataSource*>(new_leaf));
            //			}else {
            data_sources.push_back(dynamic_cast<DataSource*>(new_leaf));
            
            //			}
            
        }else {
            
            
            tree_nodes.push_back(new_leaf);
            
        }
        
        if (index[0] == -1)///-1 only be if 1 thing
        {
                        cerr<<"treetop1 "<<treeTop<<" "<<treeTop_count<<endl;
            if (! tree_nodes.empty())
            {//protect against source as tree top
            treeTop_count++;
            treeTop = tree_nodes.size()-1;
            }else{
                throw ClassificationTreeException("\n Exception : ImageSoure Cannot be at top of Workflow. \n");
            }
            cerr<<"treetop2 "<<treeTop<<" "<<treeTop_count<<endl;

        }else
        {
            //			cout<<"add input"<<endl;
            for (vector<int>::const_iterator i_ind = index.begin(); i_ind != index.end(); ++i_ind )
                tree_nodes.at(*i_ind)->addInput(new_leaf);
            //cout<<"done add input"<<endl;
            //if the classifier is not a the top need to apply it to the training data to pass up info
            if (new_leaf->getTreeNodeType() == DISCRIMINANT )
                static_cast<base_mvdisc*>(new_leaf)->setClassifyTrainingData(1);
            
        }
        //cout<<"done adding lef"<<endl;
    }
    
    void ClassificationTree::setTarget( Target * targ_in )
    {
        target = targ_in;
        for (vector<DataSource*>::iterator i = data_sources.begin(); i!=data_sources.end();i++)
        {
            (*i)->setTarget(target);
        }
        
    }
    void ClassificationTree::setNodeTargets( Target * targ_in )
    {
        for (vector<DataSource*>::iterator i = data_sources.begin(); i!=data_sources.end();i++)
        {
            (*i)->setTarget(targ_in);
        }
        
    }
    
    void ClassificationTree::setTargets( )
    {
        for (vector<DataSource*>::iterator i = data_sources.begin(); i!=data_sources.end();i++)
        {
            (*i)->setTarget(target);
        }
        
    }
    void ClassificationTree::run_randomSubSampling_NoRep( const unsigned int & N )
    {
        check_Ns_Targ_eq_Ns_Src();
        
        for (unsigned int obs = 0 ; obs < N ; obs++)
        {
            //		vector< vector<unsigned int> > selection_mask = generate_one_KFold_Mask(K);
            //
            //		vector<int2> estimates  = this->classify(selection_mask);
            //vector<int> classifictions = treeTop->ClassifyData();
            //	cout<<"done classify"<<endl;
            //	estimated_class.push_back(estimates);
            //	cout<<"done geenrating selection mask"<<endl;
            
        }
        
    }
    
    unsigned int  ClassificationTree::run_KFold_CV( const unsigned int & Kin , const vector<int> & mask,  const vector<int> & test_mask, const clear_flag & clear , ofstream & flog_mask, int run_offset)
    {
        //mask defines which to be used to train
        //test_mask defines which of the exclude to include for testing, this is here in order to disable recurring samples in subsampling
        check_Ns_Targ_eq_Ns_Src();
        //cout<<"Enter run Kfold "<<sum<int>(mask)<<endl;
        
        if (clear == CLEAR)
        {
            estimated_class.clear();
            estimated_distance_scores.clear();
        }
        if (!flog_mask.is_open())
            throw ClassificationTreeException("Tried to run K-fold cross-validation without an open ofstream.");
        
        
        unsigned int NumberOfSamples = data_sources.at(0)->getNumberOfSamples();
        unsigned int NumberOfSamples_masked = 0;
        for (vector<int>::const_iterator i = mask.begin(); i!=mask.end();i++)
        {
            //cout<<"mask "<<*i<<endl;
            if (*i > 0 ) NumberOfSamples_masked++;
        }
        //IF K =1 run leave-one-out
        // cout<<"Kin1 "<<Kin <<" "<<NumberOfSamples_masked<<" "<<K<<endl;
        
        unsigned int K =  ( Kin == 1 ) ? NumberOfSamples_masked : Kin;
        
        cout<<"Kin "<<Kin <<" "<<NumberOfSamples_masked<<" "<<K<<endl;
        if (Verbose_)
            cout<<"Running "<<K<<"-fold cross-valdiation. Number of samples "<<NumberOfSamples<<endl;
        
        if (K>NumberOfSamples_masked)
            throw ClassificationTreeNodeException("Invalid choice of K in K-fold cross-validation. It exceeds number of training samples");
        
        unsigned int N_per_class = NumberOfSamples_masked / K;
        unsigned int left_over = NumberOfSamples_masked - N_per_class * K;
        srand_fine();
        //		srand ( time(NULL) );
        //srand ( seed );
        //seed+=rand();
        vector< vector< unsigned int > > group_samples;
        
        
        list< unsigned int > available_samples;
        for ( unsigned int i = 0 ; i < NumberOfSamples_masked ; i++ )
            available_samples.push_back(i);
        
        // print<unsigned int>(available_samples,"available_samples");
        
        unsigned int N_available = available_samples.size();
        //cout<<"Number of availabel sampels "<<N_available<<endl;
        //this setsup group samples
        //randomly samples from available data then remove chosen
        //group samples will be the sample subjects in each class
        for ( unsigned int gr = 0 ; gr < K ; gr++ )
        {//for each group
            vector< unsigned int > group;
            
            //spread out extras
            unsigned int left_over_one=0;
            if (left_over>0)
            {
                left_over_one=1;
                left_over--;
            }
            for (unsigned int i =0; i<N_per_class+left_over_one ;i++)
            {
                unsigned int sample = rand() % N_available;
                list< unsigned int >::iterator i_element = available_samples.begin();
                advance(i_element,sample);
                
                group.push_back(*i_element);
                available_samples.erase(i_element);
                N_available--;
                
            }
            
            group_samples.push_back(group);
            
        }
        
        //---------------set up selection mask--------------//
        //this nad above is the heart of the cross-valdiation
        //i.e. convert data into binary mask vector so that its compatible with applysleection mask,
        vector< vector<int> > selection_mask;
        
        //group samples
        for (unsigned int gr = 0 ; gr < K ; gr ++)
        {
            //initialize mask to all zero, then select included
            //mask size differs from number fo smaple because of subsampling?
            vector<int>* one_mask = new vector<int>(mask.size(),0);
            
            //create one sleection mask for each left out group
            for (unsigned int gr_sub = 0 ; gr_sub < K ; gr_sub++)
            {
                if ( gr_sub != gr )
                { //include every group that is not which is being left out
                    //this get trickier because of mask changing of indcies are mapped
                    
                    for ( vector< unsigned int >::iterator i = group_samples[gr_sub].begin(); i!=group_samples[gr_sub].end(); i++)
                    {//set the appropriate subject to one
                        //then input mask can trump any samples
                        //count is actually index
                        unsigned int mask_index=-1; //keep track of which non-zero index
                        //start at -1, to accoutn for masking out first element
                        vector<int>::iterator i_onem = one_mask->begin();
                        for (vector<int>::const_iterator i_mask = mask.begin(); i_mask != mask.end(); ++i_mask,++i_onem){
                            if (*i_mask > 0){
                                ++mask_index;
                                if (mask_index == *i){
                                    *i_onem = 1;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            
            selection_mask.push_back(*one_mask);
            
            delete one_mask;
        }
        
        //----------------------set up repeated copy of test mask
        
        vector<int> one_test_mask(test_mask.begin(),test_mask.end());
        //the test masks allows to omit subject left out at upperlevel subsampling
        vector< vector<int> > selection_test_mask(selection_mask.size(),one_test_mask);
        //done creating selection  masks
        //----------------------set up repeated copy of test mask
        
        
        if (Verbose_){
            print<int>(selection_test_mask[0],"Selection test mask ");
            print<int>(selection_mask[0],"Selection mask ");
        }
        
        vector< pair<int,float> > distance_scores;// = new vector< pair<int,float> >();
        
        if (Verbose_)
            cout<<"do classification... "<<endl;
        
        ///-----------------------THIS IS THE PART THAT RUNS the TREE -------------------//
        vector<int4> withinGroupEstimates;
        vector<int3> estimates  = this->classify(distance_scores, selection_mask,selection_test_mask,withinGroupEstimates, run_offset);
        ///-----------------------THIS IS THE PART THAT RUNS the TREE -------------------//
        //cout<<"...done classification "<<endl;
        for (vector< vector<int> >::iterator i = selection_mask.begin() ; i != selection_mask.end(); ++i)
            for (vector< int >::iterator ii = i->begin(); ii!= i->end(); ++ii)
            {
                flog_mask<<*ii;
                if (ii == (i->end()-1))
                    flog_mask<<endl;
                else {
                    flog_mask<<",";
                }
                
            }
        
        if (Verbose_)
        {
            for (vector<int3>::iterator i = estimates.begin(); i!=estimates.end(); i++)
                cout<<"estimate "<<i->x<<" "<<i->y<<" "<<i->z<<endl;
        }
        
        estimated_class.push_back(estimates);
        estimated_distance_scores.push_back(distance_scores);
        
        data_sources.at(0)->getNumberOfSamples();
        
        return selection_mask.size();//number of iterations
        
    }
    
    unsigned int  ClassificationTree::run_KFold_CV( const unsigned int & K, const clear_flag & clear  )
    {
        check_Ns_Targ_eq_Ns_Src();
        
        
        if (clear == CLEAR)
        {
            estimated_class.clear();
            estimated_distance_scores.clear();
        }
        unsigned int NumberOfSamples = data_sources.at(0)->getNumberOfSamples();
        if (Verbose_)
            cout<<"Runnning "<<K<<"-fold cross-validation..."<<endl<<"Number of Samples: "<<NumberOfSamples<<endl;
        
        if (K>NumberOfSamples)
            throw ClassificationTreeNodeException("Invalid choice of K in K-fold cross-validation. It exceeds number of training samples");
        
        unsigned int N_per_class = NumberOfSamples / K;
        unsigned int left_over = NumberOfSamples - N_per_class * K;
        srand_fine();
        //		srand ( time(NULL) );
        //srand ( seed );
        //seed+=rand();
        vector< vector< unsigned int > > group_samples;
        
        
        list< unsigned int > available_samples;
        for ( unsigned int i = 0 ; i < NumberOfSamples ; i++ )
            available_samples.push_back(i);
        
        print<unsigned int>(available_samples,"avai sampl");
        unsigned int N_available = available_samples.size();
        //cout<<"Navailabel sampel "<<N_available<<endl;
        //group subjects into K groups
        for ( unsigned int gr = 0 ; gr < K ; gr++ )
        {//for each CV group
            //  cout<<"CV group : "<<gr<<" of "<<K<<endl;
            vector< unsigned int > group;
            //spread out extras
            unsigned int left_over_one=0;
            if (left_over>0)
            {
                left_over_one=1;
                left_over--;
            }
            for (unsigned int i =0; i<N_per_class+left_over_one ;i++)
            {
                unsigned int sample = rand() % N_available;
                list< unsigned int >::iterator i_element = available_samples.begin();
                advance(i_element,sample);
                
                group.push_back(*i_element);
                available_samples.erase(i_element);
                N_available--;
                
            }
            group_samples.push_back(group);
            //    print<unsigned int>(group,"groups");
            
        }
        
        vector< vector< int> > selection_mask;
        
        
        for (unsigned int gr = 0 ; gr < K ; gr ++)
        {
            // cout<<"CV group selection mask : "<<gr<<" of "<<K<<endl;
            
            vector< int> one_mask(NumberOfSamples,0);
            
            //cout<<"groupK "<<gr<<endl;
            for (unsigned int gr_sub = 0 ; gr_sub < K ; gr_sub++)
            {
                //  cout<<"gr_sub "<<gr_sub<<endl;
                if ( gr_sub != gr )
                {
                    //cout<<"setting maks"<<endl;
                    for ( vector< unsigned int >::iterator i = group_samples[gr_sub].begin(); i!=group_samples[gr_sub].end(); i++)
                        one_mask[*i]=1;
                    //      print<int>(one_mask,"one mask ");
                    
                }
            }
            selection_mask.push_back(one_mask);
            //   print<int>(one_mask,"one mask final ");
            
            //	selection_test_mask.push_back(one_test_mask);
        }
        vector<int> one_test_mask(NumberOfSamples,1);
        vector< vector<int> > selection_test_mask(selection_mask.size(),one_test_mask);
        
        if (Verbose_)
            print<int>(selection_mask[0],"selection mask ");
        
        vector< pair<int,float> > distance_scores;
        vector<int4> estimates_train;
        vector<int3> estimates  = this->classify(distance_scores, selection_mask,selection_test_mask,estimates_train);
        
        //	cout<<"DONE CLASSIFY"<<endl;
        estimated_class.push_back(estimates);
        estimated_distance_scores.push_back(distance_scores);
        
        //	data_sources.at(0)->getNumberOfSamples();
        //	vector<float> all_accuracies = getAllAccuracies();
        
        if (Verbose_)
        {
            //cout<<"Ran "<<K<<"-fold Cross-Validation "<<all_accuracies.size()<<"times."<<endl;
            //		print<float>(all_accuracies,"All Accuracies");
        }
        return selection_mask.size();//number of iterations
        
        //cout<<"done K-fold"<<endl;
    }
    
    void ClassificationTree::setDoGenAccKFold(const bool & enabled )
    {
        cout<<"tree_nodes "<<endl;
        cout<<" "<<treeTop<<endl;
        tree_nodes[treeTop]->setDoKFold(enabled);
    }
    
    void ClassificationTree::run_leave_N_out( const unsigned int & N)
    {
        check_Ns_Targ_eq_Ns_Src();
        
        
        if (data_sources.empty())
        {
            throw ClassificationTreeException("Tried to run LOOCV with no data sources");
        }
        
        
        
        
        unsigned int K = data_sources.at(0)->getNumberOfSamples() / N  ;
        if ( K * N !=  data_sources.at(0)->getNumberOfSamples() )
            throw ClassificationTreeException("Specified invalid Leave-N-Out. Must be divisible into number of samples");
        //		cout<<"running K fold "<<K<<" "<<data_sources.at(0)->getNumberOfSamples()<<" "<<data_sources.at(0)->getNumberOfSamples()<<endl;
        //this will flag file read
        totalNumberOfIterations += run_KFold_CV(K, CLEAR);
        //cout<<"done running K fold "<<endl;
        
    }
    /*
     void ClassificationTree::oneClassClassifier( const unsigned int & Class_Label )
     {
     //N is the numbver of subsamples
     //K is teh K-fold CV done for each subsample
     totalNumberOfIterations=0;
     
     cout<<"Run sub sampling"<<endl;
     estimated_class.clear();
     //make sure to clear old data
     
     
     //only support 2 class sampling at the moment
     //must be a 2 group sample
     if (target==NULL)
     throw ClassificationTreeException("Didn't set target prior to runnning permutations.");
     
     //adding ability to subselect two classes
     //implemented for 2 classes
     unsigned int Kclasses = target->getNumberOfClasses();
     
     vector<int> labels = target->getClassLabels();
     
     //mapping between class label and index in in labels
     map<int, unsigned int> label_2_ind;
     for (unsigned int i=0; i< Kclasses ;  i++)
     label_2_ind.insert(pair<int,unsigned int>(labels[i],i));
     
     unsigned int Nobs = data_sources.at(0)->getNumberOfSamples();
     
     
     //---------------create mask for single class ----------------//
     vector<unsigned int> one_class_mask(Nobs,0);
     vector<int>::iterator i_t
     for (vector<int>::iterator i_t = target.begin(); i_t != target.end(); ++i_t)
     if (*i_t == Class_Label)
     *i_cl=1;
     //---------------------------------------------//
     ofstream flog_selection_mask("selection_mask.csv");
     
     run_KFold_CV(K,*i,NO_CLEAR,flog_selection_mask,0);
     flog_selection_mask.close();
     
     
     vector<float> all_accuracies = getAllAccuracies();
     cout<<"Ran "<<all_accuracies.size()<<" K-fold Cross-Validation."<<endl;
     print<float>(all_accuracies,"All Accuracies");
     
     
     
     
     }
     */
    vector<int> ClassificationTree::getStratifyLabels(){
        
        if (target == NULL)
            throw ClassificationTreeException("Tried to get labels to stratify from target that does not exist");
        
        return target->getStratifyLabels();
        
    }
    
    void ClassificationTree::run_subsampling( const string & output_base, unsigned int N , const unsigned int & K, const unsigned int & minSizeOffset, const unsigned int & sampleSizePerGroup)
    {
        
        if (data_sources.empty())
            return;
        
        check_Ns_Targ_eq_Ns_Src();
        
        
        vector<int> strat_labels = target->getStratifyLabels();
        print<int>(strat_labels,"startlabels ");
        if (strat_labels.empty())
        {
            //cout<<"dont run start labels"<<endl;
            run_subsampling(output_base, N,K,target->getClassLabels(), minSizeOffset );
        }else{
            run_subsampling(output_base, N,K,strat_labels, minSizeOffset );
            
            
        }
    }
    void ClassificationTree::run_subsampling( const string & output_base,  unsigned int N , const unsigned int & K , const vector<int> & stratified_labels , const unsigned int & minSizeOffset, unsigned int sampleSizeAbs, bool USE_NON_STRATIFIED_SUBJECTS, bool USE_LEFTOUT_FOR_TEST, list< vector<int> > * input_subsamplings)
    {
        //******************THE SUBSAMPLING IS ONLY AVAILABLE FOR 2 CLASSES*********************//
        
        //N is the numbver of subsamples
        //K is teh K-fold CV done for each subsample
        //stratified_labels ensure equal sample sizes
        //minSizeOffset allows to use less subject per group than the minimum group size (maybe useful for valdiation)
        //sampleSizeAbs provides an absolute sample size per class
        //USE_LEFTOUT_FOR_TEST will omit data left out from subsampling if false
        //USE_NON_STRATIFIED_SUBJECTS this option will add back in the subjects if they were not used for starifying
        if (Verbose_)
            cout<<"Run Sub-sampling with nested cross-validation for equal group size..."<<endl;
        //cout<<"Run Sub-sampling with nested cross-validation for equal group size..."<<endl;
        
        //------Some Error Checking---------//
        if (target==NULL)
            throw ClassificationTreeException("Didn't set target prior to runnning permutations.");
        
        check_Ns_Targ_eq_Ns_Src();
        
        
        //---------------clear any classifcation set variables-----------/
        estimated_class.clear();
        totalNumberOfIterations=0;
        print<int>(stratified_labels,"stratified_labels");
        //	cout<<"hmm:"<<endl;
        //-----------------Compute Class Information----------------//
        //only support 2 class sampling at the moment ---NEED TO CHECK THIS
        unsigned int Kclasses = target->getNumberOfClasses();
        vector<int> labels = target->getClassLabels();
        // vector<int> strat_labels = target->getStratifyLabels();
        //mapping between class label and index in in labels
        map<int, unsigned int> label_2_ind;
        for (unsigned int i=0; i< Kclasses ;  i++)
            label_2_ind.insert(pair<int,unsigned int>(labels[i],i));
        
        //----------------Implement the subselection of two classes-------------//
        //if stratified_labels is empty than it wont erase anything
        //labels allows you to choose which classes to stratify
        //number of subjects to use from each class
        vector<unsigned int> N_per_class = target->getNumberOfSubjectsPerClass();
        vector<int>::iterator i_l = labels.begin();
        //trimed N_per_class to only have those classes you wish to stratify
        for ( vector<unsigned int>::iterator i = N_per_class.begin() ; i != N_per_class.end(); ++i, ++i_l)
        {
            bool select=false;
            //check to see if the label is one of the specified interest ones
            for (vector<int>::const_iterator ii = stratified_labels.begin(); ii != stratified_labels.end(); ++ii)
            {
                if (*ii == *i_l) //is the label been startified one of the specified labels
                    select=true;
            }
            if (!select)//if its not erase the entry
            {
                N_per_class.erase(i);
                --i;
            }
        }
        
        
        //Total number of observations (samples), just the sum across number of obs. per class
        unsigned int Nobs = sum<unsigned int>(N_per_class);
        
        //Option to offset the minimun sample size for a class
        unsigned int NperClass_min = min<unsigned int>(N_per_class);
        
        //cout<<"NperClass_min2 "<<NperClass_min<<" "<<minSizeOffset<<endl;
        
        if ( NperClass_min <= minSizeOffset)
            throw ClassificationTreeException("In run subsampling, user specified to reduce minimum group by as much or more than the minimum group size.");
        else {
            NperClass_min -= minSizeOffset;
        }
        
        //This is used to enforce a specific smaple size
        //It is a per class sample size
        if (sampleSizeAbs>0)
        {
            for (vector<unsigned int>::iterator i = N_per_class.begin(); i != N_per_class.end();++i)
                if ( sampleSizeAbs > (*i))
                    throw ClassificationTreeException("In run subsampling, absolute group sample size is larger than an inidividual group size.");
            NperClass_min=sampleSizeAbs;///Kclasses;
        }
        cout<<"NperClass_min "<<NperClass_min<<" "<<minSizeOffset<<endl;
        //-----------Now, Let's generate all the possible subsamplings---------//
        //number of sample to take from data
        
        //This is the bit that ensures equal sample sizes
        //		unsigned Nobs_ss = NperClass_min * Kclasses;
        //		unsigned int remainder = Nobs - Nobs_ss; //number of left out samples
        
        
        //---------------------------------DO RANDOM SUBSAMPLING------------------------------------------------//
        //subsample labels, only two (only support for 2-class subsampling)
        vector< vector<int> > all_ss_combinations;//all possible ways of randomly sampling the data to exclude desired amount
        vector<float> targ_data = target->getTargetData<float>();
        vector<int> nonstrat_labels;//keep tracjk of left out labels for later
        
        if ( input_subsamplings == NULL)//this will allow to use an extternal seleection mask
        {
            
            vector<int> ss_labels(2,1);
            ss_labels[1]=0;
            
            
            //needs to be labels[0]
            //basesamples is just an initial vector to be manipulated
            vector<int> base_sampler(Nobs,ss_labels[0]);
            
            //reset to original, would have been altered at the stratification stage
            N_per_class = target->getNumberOfSubjectsPerClass();
            vector<int>::iterator i_l = labels.begin();
            for ( vector<unsigned int>::iterator i = N_per_class.begin() ; i != N_per_class.end(); ++i, ++i_l)
            {
                bool select=false;
                //check to see if the label is one of the specified interest ones
                for (vector<int>::const_iterator ii = stratified_labels.begin(); ii != stratified_labels.end(); ++ii)
                {
                    if (*ii == *i_l) //is the label been startified one of the specified labels
                        select=true;
                }
                if (!select)//if its not erase the entry
                {
                    nonstrat_labels.push_back(*i_l);
                    *i=0;
                }
            }
            
            //create vector saying what to smaple off each class, make sure to not select more than what exists (particuylarly for sample by which you are not stratifying)
            unsigned max_combinations=1;//initialize to 1, change if necessary
            vector<unsigned int> v_Nselect(Kclasses);//keeps tracj of number picked from each class
            vector<unsigned int>::iterator ii = N_per_class.begin();
            for (vector<unsigned int>::iterator i = v_Nselect.begin(); i != v_Nselect.end(); ++i, ++ii)
            {//for each class
                cout<<"calculating max combonations "<<max_combinations<<" "<<endl;
                if (*ii <= NperClass_min)
                {//if number of observations per class is less than the minimum, only select the minimum from that group
                    *i=*ii;
                }else{
                    *i=NperClass_min;
                    //calculating maximum number of combinatioons of choosing NperClassMin from NperClass
                    
                    double comb = combination(*ii,NperClass_min);
                    cout<<"combination "<<comb<<endl;
                    if ((comb > std::numeric_limits<unsigned int>::max() ) || ( max_combinations*comb >= std::numeric_limits<unsigned int>::max()))
                        max_combinations = std::numeric_limits<unsigned int>::max();
                    else
                        max_combinations *= comb;
                }
            }
            
            print<unsigned int>(N_per_class,"N_perclass2");
            //    print<unsigned int>(v_Nselect,"N_select");
            
            //check to make sure that you haven't chosen to many samples
            if (Verbose_)
                cout<<"Maximum number of possible unique subsamples "<<max_combinations<<endl;
            if (N>max_combinations)//make sure input was not selected higher than number permissble
            {
                //instead of throw push throgh with max
                N = max_combinations;
                cout<<"Warning: You have chosen too many samples. Setting to "<<N<<endl;
                //throw ClassificationTreeException("Number of samples requested exceeds number of possible unique examples.");
            }
            
            //now find all the binary combinations of sampling the data
            //only select the desired amount, contained in binary_combinations
            if (Verbose_)
            {
                cout<<"Runnning "<<N<<" subsamples."<<endl;
                print<unsigned int>(v_Nselect,"v_Nselect");
            }
            
            //ensure equal smaples sizes
            binary_combinations(all_ss_combinations,target->getTargetData<int>(), N,v_Nselect, labels );
            
            
            //the equal sample size should be hanfdled by binary combinations function via Nselect
            //remove all configurations that does preserve equal sample sizes
            //			vector<unsigned int> class_counter(Kclasses,0);
            //
            //			for (vector< vector<int> >::iterator i = all_ss_combinations.begin(); i != all_ss_combinations.end(); ++i)
            //			{
            //              //  print<int>(*i,"ss_combo");
            //				class_counter.assign(Kclasses,0);
            //
            //				vector<float> ss = subsample<float>(targ_data, *i);
            //				//lets count labels per class
            //				for ( vector<float>::iterator i_ss = ss.begin() ; i_ss != ss.end(); i_ss++)
            //					class_counter[ label_2_ind.find(*i_ss)->second ]++;
            //
            //				bool is_equal=true;
            //                ///check to see if it indeed equal sampling
            //                i_l = labels.begin();
            //				for ( unsigned int k=0; k<Kclasses; k++,++i_l)
            //				{
            //					bool select=false;
            //		//			int lb = labels[k];
            //					//check to see if the label is one of the specified interest ones
            //					for (vector<int>::const_iterator ii = stratified_labels.begin(); ii != stratified_labels.end(); ++ii)
            //						if (*ii == *i_l) //is the label been startified one of the specified labels
            //							select=true;
            //                    ////cout<<"class "<<k<<" "<<class_counter[k]<<" "<<" "<<*i_l<<" "<<select<<endl;
            //					//this check only valied for stratified labels
            //					if (( class_counter[k] != NperClass_min ) && (select))
            //					{
            //                        //cout<<"is false "<<k<<" "<<class_counter[k]<<endl;
            //						is_equal = false;
            //						break;
            //					}
            //				}
            //				if (!is_equal)
            //				{
            //					i=all_ss_combinations.erase(i)--;
            //					ss = subsample<float>(targ_data, *i);
            //					i--;
            //
            //
            //				}
            //			}
        }else {
            all_ss_combinations.insert(all_ss_combinations.begin(),input_subsamplings->begin(), input_subsamplings->end()) ;
        }
        
        //all_ss_test_combinations is used as a mask to incclude/exclude the left our data from the test sample
        //initialized to all one
        vector< vector<int> > all_ss_test_combinations(N);
        
        //-------------OMIT SUBEJCTS LEFT OUT FROM SUBSAMPLIGN FROM PREDICTIONS-----------------//
        //false means leave them out
        
        if (!USE_LEFTOUT_FOR_TEST){
            //in not use left out for test
            //in this case just copy selection mask
            vector< vector<int> >::iterator i_test = all_ss_test_combinations.begin();
            for (vector< vector<int> >::iterator i = all_ss_combinations.begin(); i != all_ss_combinations.end(); ++i,++i_test)
            {
                i_test->insert(i_test->end(),i->begin(),i->end());
                
            }
        }else {//set to all 1 vectors
            unsigned int n_ins = all_ss_combinations.front().size();
            for ( vector< vector<int> >::iterator i_test=all_ss_test_combinations.begin(); i_test != all_ss_test_combinations.end() ; ++i_test)
                i_test->insert(i_test->end(),n_ins,1);
            
        }
        
        //-------------END OMIT SUBEJCTS LEFT OUT FROM SUBSAMPLIGN FROM PREDICTIONS-----------------//
        
        
        //---------------------------------DONE RANDOM SUBSAMPLING------------------------------------------------//
        //cout<<"done random subsampling"<<endl;
        {
            
            vector< vector<int> >::iterator i_test = all_ss_test_combinations.begin();
            unsigned int SS_count=1;
            for (vector< vector<int> >::iterator i = all_ss_combinations.begin(); i != all_ss_combinations.end(); ++i,++i_test,++SS_count)
            {
                if (USE_NON_STRATIFIED_SUBJECTS) {//lets add back the labels we ommitted when stratifying
                    vector<int>::iterator i_ss = i->begin();
                    vector<int>::iterator ii_test = i_test->begin();
                    for ( vector<float>::iterator i_targ = target->begin(); i_targ != target->end(); ++i_targ, ++i_ss,++ii_test)
                    {
                        if (*i_ss == 0 )
                        {
                            for ( vector<int>::iterator i_nst = nonstrat_labels.begin(); i_nst != nonstrat_labels.end(); ++i_nst)
                                if (*i_nst == *i_targ)
                                {
                                    //"found left out class "<<*i_nst<<endl;
                                    *i_ss = 1;
                                    *ii_test = 1;
                                }
                        }
                        
                        //"non strat "<<*i_ss<<" "<<*i_targ<<endl;
                    }
                }
                
                vector<float> ss = subsample<float>(targ_data, *i);
                //lets count labels per class
                
                
                //if (Verbose_)
                cout<<"Run "<<K<<"-fold for "<<SS_count<<" sub-sample"<<endl;
                
                //save selection mask for each iteration
                ofstream flog_selection_mask((output_base + "_selection_mask_"+num2str(SS_count)+".csv").c_str());
                
                totalNumberOfIterations += run_KFold_CV(K,*i,*i_test,NO_CLEAR,flog_selection_mask,totalNumberOfIterations);
                
                flog_selection_mask.close();
                
            }
        }
        vector<float> all_accuracies = getAllAccuracies();
        if (Verbose_)
        {
            cout<<"Ran "<<K<<"-fold Cross-Validation "<<all_accuracies.size()<<"times."<<endl;
            print<float>(all_accuracies,"All Accuracies");
        }
        
        
    }
    void ClassificationTree::permuteTarget(){
        //this will set out_taregt only fopr tree
        target->permuteTargetData();
        
        //now set at all sources
        
        //for now not permutaing all groups not allowing selection
        // cout<<"permuet target data "<<static_cast<unsigned int>(time(NULL))<<" "<<time(NULL)<<" "<<endl;
        //  srand ( seed );
        //   in_target_orig = in_target;
        //copy because going to pop out
        vector<float> perm_target = target->getTargetData<float>();//_orig;
        
        //set data at sources
        for (vector<DataSource*>::iterator i = data_sources.begin(); i!=data_sources.end();i++)
        {
            (*i)->setTargetData(perm_target);
        }
    }
    
    template <class T>
    std::vector<T> ClassificationTree::getTargetDataFromSrc()
    {
        if (data_sources.empty())
            throw ClassificationTreeException("Classification Tree Error: Tried to access target data from data source, but no sources exist.");
        
        return data_sources[0]->getTargetData<T>();
    }
    template std::vector<int> ClassificationTree::getTargetDataFromSrc<int>();
    //    template std::vector<unsigned int> ClassificationTree::getTargetDataFromSrc<unsigned int>();
    template std::vector<float> ClassificationTree::getTargetDataFromSrc<float>();
    
    
    void ClassificationTree::loadPermutedTarget(const std::vector<float> & v_targ ){
        //this will set out_taregt only fopr tree
        //    target->permuteTargetData();
        
        //now set at all sources
        
        //for now not permutaing all groups not allowing selection
        // cout<<"permuet target data "<<static_cast<unsigned int>(time(NULL))<<" "<<time(NULL)<<" "<<endl;
        //  srand ( seed );
        //   in_target_orig = in_target;
        //copy because going to pop out
        //vector<float> perm_target = target->getTargetData<float>();//_orig;
        
        //skip all the randomization, juts load from file
        
        //set data at sources
        for (vector<DataSource*>::iterator i = data_sources.begin(); i!=data_sources.end();i++)
        {
            (*i)->setTargetData(v_targ);
        }
    }
    void ClassificationTree::permuteTarget( const std::vector<int> & v_sel_mask )
    {
        //        cerr<<" ClassificationTree::permuteTarget "<<endl;
        
        target->permuteTargetData(v_sel_mask);
        //        cerr<<" ClassificationTree::permuteTarget - end call target"<<endl;
        
        vector<float> perm_target = target->getTargetData<float>();//_orig;
        
        //set data at sources
        for (vector<DataSource*>::iterator i = data_sources.begin(); i!=data_sources.end();i++)
        {
            
            (*i)->setTargetData(perm_target);
        }
        //        cerr<<" end ClassificationTree::permuteTarget "<<endl;
        
    }
    
    
    void ClassificationTree::run_permutationTest(  const unsigned int & Nperms ,const std::string & output_base, const string & class_inc_map_name,  unsigned int  N , const unsigned int & K,const std::vector<int> & stratified_labels ,   const unsigned int & minSizeOffset,const unsigned int & perms_offset, bool USE_NON_STRATIFIED_SUBJECTS, bool USE_LEFTOUT_FOR_TEST , std::list< std::vector<int> >*  input_subsamplings )
    {
        srand_fine();
        //        srand ( static_cast<unsigned int>(time(NULL)) );
        // srand ( seed );
        // seed+=rand();
        
        if (target==NULL)
            throw ClassificationTreeException("Didn't set target prior to runnning permutations.");
        
        check_Ns_Targ_eq_Ns_Src();
        
        
        //implemented for 2 classes
        //		unsigned int Nobs = data_sources.at(0)->getNumberOfSamples();
        
        bool use_class_inc = false;
        map<int,int> class_map_inc;
        if (class_inc_map_name != "")
        {
            cout<<"read class map "<<class_inc_map_name<<endl;
            use_class_inc = true;
            class_map_inc = readClassMap(class_inc_map_name);
        }
        
        
        
        
        vector<int> labels = target->getClassLabels();
        vector<unsigned int> N_per_class = target->getNumberOfSubjectsPerClass();
        //	unsigned int total = sum<unsigned int>(N_per_class);
        //Target targ_org (target);//copy target
        
        ofstream fperms((output_base+"_perms.csv").c_str());
        for ( unsigned int i = 0 ; i < Nperms ; ++i)
        {//loop around each permutation
            //clear estimated data
            clearOutputData();
            //don't need to retain original configuration, just keep scrambling
            vector<float> targ_data = target->getTargetData<float>();
            //need no initialize before trimming
            vector<float> targ_rand;//(targ_data.size(),-333);//-333 is there to look for errors
            //lets remove all non-permuting labels
            vector<unsigned int> non_perm_inds;
            vector<int> non_perm_vals;
            unsigned int index=0;
            
            if (use_class_inc){
                cout<<"USse class inc "<<endl;
                for (vector<float>::iterator i_t = targ_data.begin(); i_t != targ_data.end(); ++i_t,++index)
                {
                    map<int,int>::iterator i_map =  class_map_inc.find(*i_t);
                    if ( i_map == class_map_inc.end()) //not in map exclude
                        throw ClassificationTreeException("Missing class from permutation inclusion map.");
                    
                    if (i_map->second == 0){///don't permute
                        
                        non_perm_inds.push_back(index);
                        non_perm_vals.push_back(*i_t);
                        i_t = targ_data.erase(i_t);
                        --i_t;
                    }else if (i_map->second != 1){//do permute
                        throw ClassificationTreeException("Permutation inclusion map should have binary mapping");
                    }
                    
                }
            }
            print<int>(non_perm_vals,"nonperm vals");
            print<float>(targ_data,"targ_data");
            index=0;
            //initialise instead of pushing back to be able to exclude people from the permutations
            vector<unsigned int>::iterator i_nonperm_ind = non_perm_inds.begin();
            //vector<int>::iterator i_nonperm_val = non_perm_vals.begin();
            
            while ( (! targ_data.empty()) || (!non_perm_vals.empty())) {
                
                if (use_class_inc){//has a map been included for which to permute
                    
                    //    cout<<"do permuting with clas map "<<index<<" "<<non_perm_vals.size()<<" "<<*i_nonperm_ind<<endl;
                    if (*i_nonperm_ind == index){//this was a non permute index
                        //                        targ_rand.push_back(*i_nonperm_val);
                        // cout<<"addpersm "<<non_perm_vals[0]<<endl;
                        // print<int>(non_perm_vals,"perms_left");
                        targ_rand.push_back(non_perm_vals[0]);
                        non_perm_vals.erase(non_perm_vals.begin());
                        ++i_nonperm_ind;
                        //          ++i_nonperm_val;
                        ++index;
                    }else{
                        unsigned int sample = rand() % targ_data.size();
                        targ_rand.push_back(targ_data[sample]);
                        targ_data.erase(targ_data.begin()+sample);
                        ++index;
                    }
                    
                }else{
                    unsigned int sample = rand() % targ_data.size();
                    targ_rand.push_back(targ_data[sample]);
                    targ_data.erase(targ_data.begin()+sample);
                    
                }
                //                targ_rand.push_back(targ_data[sample]);
                //				targ_data.erase(targ_data.begin()+sample);
                
                
            }
            cout<<"targrean size "<<targ_rand.size()<<endl;
            print<float>(targ_rand,"permute labels");
            //write the permutations to a file
            for (vector<float>::iterator i_perm = targ_rand.begin(); i_perm != targ_rand.end(); ++i_perm)
            {
                fperms<<*i_perm;
                if (i_perm != (targ_rand.end()-1))
                    fperms<<",";
                else {
                    fperms<<endl;
                }
                
                
            }
            
            target->setTargetData(targ_rand);
            setTargets();
            //			unsigned int N_in =1;
            run_subsampling( output_base, N, K, stratified_labels , minSizeOffset,  USE_NON_STRATIFIED_SUBJECTS,  USE_LEFTOUT_FOR_TEST ,  input_subsamplings );
            string snperm = num2str<int>(i+perms_offset);
            
            cout<<"output stats "<<output_base+"_perm"+snperm +"_" + "_per_subject_stats.csv"<<endl;
            outputPerSubjectStats( output_base+"_perm"+snperm + "_per_subject_stats.csv");
            
            outputPerSessionStats( output_base + "perm" + snperm + "_" + "_subject_stats.csv");
            
            
        }
        fperms.close();
        
        
        /*		vector<int> targ(Nobs,labels[0]);
         list< vector<int> > all_targs;
         int count=0;
         binary_combinations(all_targs, targ, labels, count, 0, N_per_class[1]);
         
         list< vector<int> > permutations = sampleWithoutReplacement<int>( all_targs , N );
         
         vector<float> pred_err;
         
         count=1;
         for ( list< vector<int> >::iterator i = permutations.begin(); i!=permutations.end();i++,count++)
         {
         cout<<count<<" ";
         print<int>(*i,"Permute Labels: ");
         
         target->setTargetData(*i);
         //run_subsampling(output_base, N,K,target->getClassLabels(), minSizeOffset );
         //	run_subsampling(output_base, N,K, 0 );
         unsigned int N_in = N;
         //			run_subsampling(output_base, N_in,K,target->getClassLabels(), 0 );
         run_subsampling( output_base, N_in , K, stratified_labels , minSizeOffset,  USE_NON_STRATIFIED_SUBJECTS,  USE_LEFTOUT_FOR_TEST ,  input_subsamplings );
         
         
         //run_KFold_CV(K,CLEAR);
         pred_err.push_back(target->expected_prediction_error( estimated_class[0] ));
         cout<<"prederr "<<pred_err.back()<<endl;
         }
         
         print<float>(pred_err,"Expected Prediction Error");
         
         
         cout<<"Mean Prediction Error "<<mean(pred_err)<<endl;
         //	int Ncomb=combination(N_per_class[0]+N_per_class[1],N_per_class[1]);
         
         //	cout<<"done comb "<<all_targs.size()<<" "<<combination(Nobs, N_per_class[1])<<endl;
         //	cout<<Ncomb<<endl;
         
         */
        
    }
    
    vector<int3> ClassificationTree::classify()
    {
        if (tree_nodes.empty())
            return vector<int3>();
        
        
        
        std::vector< std::pair<int,float> >  distance_scores;
        std::vector<int> selection_mask(tree_nodes[0]->getNumberOfSamples(),1);
        std::vector<int> selection_test_mask(tree_nodes[0]->getNumberOfTestSamples());
        std::vector<int4> withinGroup;
        unsigned int offset=0;
        
        return classify( distance_scores,  selection_mask ,  selection_test_mask ,   withinGroup, offset);
        
        
    }
    
    
    vector<int3> ClassificationTree::classify(std::vector< std::pair<int,float> > & distance_scores,  const std::vector<int> selection_mask , const std::vector<int> selection_test_mask ,  std::vector<int4> & withinGroup,unsigned int offset)
    {
        	cout<<"ClassificationTree::classify1 - number of classifications "<<selection_mask.size()<<endl;
        //don't do anything with distance_scores if it comes in NULL
        if (target == NULL)
        {
            throw ClassificationTreeException("Target has not been set.");
        }
        
        //	if (distance_scores != NULL)
        //		distance_scores->clear();
        
        
        vector<int> output_classes;
        vector<int> output_indices;
        vector<int> output_run;
        
        
        
        vector<int2> est_classes;
        
        int group = offset ;//allows multiple runs of classify with proper run count
        //	print(selection_mask,"ClassificationTree :: classify :: selection mask");
        //	print(target->getTargetData<int>(),"ClassificationTree :: classify :: target data - masked");
        
        //Trainign data error stuff
        vector<int> training_indices;
        vector<int> train_run;
        
        unsigned int index=0;
        for (vector<int>::const_iterator i = selection_mask.begin(); i != selection_mask.end();++i,++index)
            if (*i > 0) {
                train_run.push_back(group);
                training_indices.push_back(index);
            }
        
        //   vector<int> train_classes = target->getTargetData<int>(selection_mask);
        
        
        
        vector<int> leftout_indices;
        for (vector<DataSource*>::iterator i = data_sources.begin(); i!=data_sources.end();i++)
        {
            
            //print<int>((*i_mask),"selection mask ");
            //print<int>((*i_test_mask),"selection test mask ");
            
            leftout_indices = (*i)->applySelectionMask(selection_mask,selection_test_mask);
        }
        cerr<<"Run tree top : "<<tree_nodes[treeTop]->nodeName()<<endl;
        tree_nodes[treeTop]->runNode();
        cerr<<"end running treeTop "<<tree_nodes.size()<<" "<<treeTop<<endl;
        //get classifications from the top of the tree
        // cout<<"get output test data"<<endl;
        
        vector<int> classifications = tree_nodes[treeTop]->getOutputTestData<int>();
        vector<int> train_classifications = tree_nodes[treeTop]->getOutputData<int>();
        vector<int> train_clusters = tree_nodes[treeTop]->getClusterData<int>();
        
        cout<<"number of output classifications "<<train_classifications.size()<<endl;
        //   cout<<"get output test data end"<<endl;
        cout<<"number of training classifications and clusters "<<train_classifications.size()<<" "<<train_clusters.size()<<endl;

        
        
        
        // print<int>(classifications,"classification?");
        //This part handles distance measures from discriminant, e.g. distance from hyperplane
        
        //this requires that the head of the tree is a dscendant of  base_mvdisc
        vector<float> dscores = tree_nodes[treeTop]->getDistanceScores();//static_cast<base_mvdisc*>(tree_nodes[treeTop])->getDistanceScores();
        //if (Verbose_)
        //                print<float>(dscores,"Distance scores ");
        
        if (dscores.size() != leftout_indices.size())
            throw ClassificationTreeException(("ClassificationTree: Distance Score not equal to Number of Left Subjects : "+ num2str(dscores.size()) + " / " + num2str(leftout_indices.size())).c_str());
        
        vector<int>::iterator i_ind = leftout_indices.begin();
        vector<int>::iterator i_cl = classifications.begin();
        for (vector<float>::iterator i_d = dscores.begin() ; i_d != dscores.end(); ++i_d, ++i_ind,++i_cl) {
            // cout<<"distance "<<*i_d<<" "<<*i_ind<<" "<<*i_cl<<endl;
            distance_scores.push_back(pair<int,float>(*i_ind,*i_d));
            
        }
        
        
        if (leftout_indices.size() != classifications.size())
            throw ClassificationTreeException(("ClassificationTree: Number of Left Subjects not equal to number of Classifications: "+ num2str(leftout_indices.size()) + " / " + num2str(classifications.size())).c_str());
        
        
        output_indices.insert(output_indices.end(),leftout_indices.begin(),leftout_indices.end());
        output_classes.insert(output_classes.end(),classifications.begin(),classifications.end());
        //    cerr<<"Classification Tree Vervbose "<<Verbose_<<endl;
        if (Verbose_ >=2)
        {
            print<int> (classifications, "Classifications of Left Out Data ");
            print<int> (output_indices, "Output_indices ");
        }
        
        for (unsigned int i =0 ; i<leftout_indices.size();i++)
        {
            output_run.push_back(group);
        }
        
        //print(train_classifications,"train class");
        //print(training_indices, "train indices");
        withinGroup = int_2_int4(training_indices,train_classifications,train_clusters, train_run);
        cout<<"wihtin Group sizes "<<training_indices.size()<<" "<<train_classifications.size()<<" "<<train_run.size()<<" "<<train_clusters.size()<<" "<<withinGroup.size()<<endl;
        // cout<<"outputclaasify "<<output_indices.size()<<" "<<output_classes.size()<<" "<<output_run.size()<<endl;
        return int_2_int3(output_indices,output_classes,output_run);
    }
    
    
    vector<int3> ClassificationTree::classify(std::vector< std::pair<int,float> > & distance_scores,  const std::vector< std::vector<int> >  selection_mask , const std::vector< std::vector<int> >  selection_test_mask , std::vector<int4> & withinGroup, unsigned int offset)
    {
        //        if (Verbose_>=1)
        cout<<"ClassificationTree::classify2 - number of classifications "<<selection_mask.size()<<endl;
        //don't do anything with distance_scores if it comes in NULL
        if (target == NULL)
        {
            throw ClassificationTreeException("Target has not been set.");
        }
        
        //	if (distance_scores != NULL)
        //		distance_scores->clear();
        
        vector<int3> output;
        
        vector<int2> est_classes;
        vector<int4> out_train;
        
        //cout<<"wihtin Group sizes "<<training_indices.size()<<" "<<train_classifications.size()<<" "<<train_run.size()<<" "<<train_clusters.size()<<" "<<withinGroup.size()<<endl;

        int group = offset ;//allows multiple runs of classify with proper run count
        vector< vector<int> >::const_iterator i_test_mask = selection_test_mask.begin();
        for (vector< vector<int> >::const_iterator i_mask = selection_mask.begin(); i_mask!= selection_mask.end(); i_mask++,++i_test_mask, group++)
        {
            vector<int3> output_temp=classify(distance_scores, *i_mask , *i_test_mask ,out_train, group);
            output.insert(output.end(),output_temp.begin(),output_temp.end());
        }
        cout<<"ClassificationTree::classify"<<endl;
        return output;
    }
    
    
    float ClassificationTree::getAccuracy( const unsigned int & index )
    {
        if ( index>= estimated_class.size() )
            return -1;
        return target->expected_prediction_error( estimated_class[index] );
    }
    float ClassificationTree::getAccuracy( const std::vector<int3> & estimates )
    {
        //cout<<"calculate accuracy "<<endl;
        unsigned int N = estimates.size();
        float sum=0;
        for (vector<int3>::const_iterator i_e  = estimates.begin(); i_e != estimates.end(); ++i_e)
        {
            sum+=(target->getInTarget(i_e->x) == i_e->y) ? 1 : 0;
            //    cout<<i_e->x<<" "<<target->getInTarget(i_e->x)<<" "<<target->getOutTarget(i_e->x)<<" "<< i_e->y<<endl;
        }
        
        return sum/N;
    }
    float ClassificationTree::getAccuracy( const std::vector<int4> & estimates )
    {
        //cout<<"calculate accuracy "<<endl;
        unsigned int N = estimates.size();
        float sum=0;
        for (vector<int4>::const_iterator i_e  = estimates.begin(); i_e != estimates.end(); ++i_e)
        {
            sum+=(target->getInTarget(i_e->x) == i_e->y) ? 1 : 0;
            //    cout<<i_e->x<<" "<<target->getInTarget(i_e->x)<<" "<<target->getOutTarget(i_e->x)<<" "<< i_e->y<<endl;
        }
        
        return sum/N;
    }
    
    
    //
    //    float ClassificationTree::getAccuracy( const std::vector<int2> & estimates, const std::vector<int> & mask )
    //    {
    //
    ////        unsigned int N = estimates.size();
    //        unsigned int N = 0;
    //
    //        float sum=0;
    //
    //        unsigned int index=0;
    //        vector<int>::const_iterator i_m = mask.begin();
    //        for (vector<int2>::const_iterator i_e  = estimates.begin(); i_e != estimates.end(); ++i_e,++index,++i_m)
    //        {
    //            if (*i_m)
    //            {
    //                sum+=(target->getInTarget(index) == i_e->y) ? 1 : 0;
    //                ++N;
    //            }
    //            //    cout<<index<<" "<<target->getInTarget(index)<<" "<< i_e->y<<endl;
    //        }
    //        return sum/N;
    //    }
    //
    
    float ClassificationTree::getAccuracy( const std::vector<int2> & estimates )
    {
        
        unsigned int N = estimates.size();
        float sum=0;
        
        unsigned int index=0;
        for (vector<int2>::const_iterator i_e  = estimates.begin(); i_e != estimates.end(); ++i_e,++index)
        {
            sum+=(target->getInTarget(index) == i_e->y) ? 1 : 0;
            //    cout<<index<<" "<<target->getInTarget(index)<<" "<< i_e->y<<endl;
        }
        return sum/N;
    }
    
    
    float ClassificationTree::getAccuracy( const std::vector<int2> & estimates ,const std::vector<int> & strat_labels )
    {
        
        unsigned int N = 0;//estimates.size();
        float sum=0;
        
        unsigned int index=0;
        
        for (vector<int2>::const_iterator i_e  = estimates.begin(); i_e != estimates.end(); ++i_e,++index)
        {
            int targ = target->getInTarget(index);
            //  cout<<index<<" -acctarg- "<<targ<<" -pred "<<i_e->x<<" "<<i_e->y<<endl;
            if ( i_e->x != 0 )
                for (vector<int>::const_iterator i_s = strat_labels.begin(); i_s != strat_labels.end(); ++i_s)
                {
                    if ( targ == *i_s )
                    {
                        sum+=( targ == i_e->y ) ? 1 : 0;
                        ++N;
                        //        cout<<"include in accuracy"<<endl;
                    }
                    //  cout<<index<<" "<<target->getInTarget(index)<<" "<< i_e->y<<endl;
                }
        }
        return sum/N;
    }
    
    
    
    float ClassificationTree::getAccuracy( const std::vector<int2> & estimates ,const std::vector<int> & strat_labels, const std::vector<int> & mask  , bool in )
    {
        //        cerr<<"ClassificationTree::getAccuracy "<<endl;
        unsigned int N = 0;//estimates.size();
        float sum=0;
        //        print(strat_labels,"strat_labels");
        bool useStratLabels=!(strat_labels.empty());
        unsigned int index=0;
        vector<int>::const_iterator i_m = mask.begin();
        for (vector<int2>::const_iterator i_e  = estimates.begin(); i_e != estimates.end(); ++i_e,++index,++i_m)
        {
            //            cerr<<"estimate "<<i_e->x<<" "<<i_e->y<<" "<<*i_m<<" "<<in<<endl;
            if ( ((*i_m > 0 )&&(in==1)) || ((*i_m == 0 )&&(in==0)) )
            {
                int targ = target->getInTarget(index);
                //                 cout<<index<<" -acctarg- "<<targ<<" -pred "<<i_e->x<<" "<<i_e->y<<endl;
                if ( i_e->x != 0 )
                {
                    
                    if (useStratLabels)
                        for (vector<int>::const_iterator i_s = strat_labels.begin(); i_s != strat_labels.end(); ++i_s)
                        {
                            if ( targ == *i_s )
                            {
                                sum+=( targ == i_e->y ) ? 1 : 0;
                                ++N;
                                //        cout<<"include in accuracy"<<endl;
                            }
                            //  cout<<index<<" "<<target->getInTarget(index)<<" "<< i_e->y<<endl;
                        }else{
                            sum+=( targ == i_e->y ) ? 1 : 0;
                            ++N;
                        }
                }
                //                cerr<<"sum/N "<<sum<<" "<<N<<endl;
            }
        }
        return sum/N;
    }
    //
    //    float ClassificationTree::getSensitivity( const std::vector<int2> & estimates, const std::vector<int> & mask   ){
    //        float tp=0;
    //        //  float fp=0;
    //        //float tn=0;
    //        float fn=0;
    //        unsigned int index=0;
    //        vector<int>::const_iterator i_m = mask.begin();
    //
    //        for (vector<int2>::const_iterator i_e  = estimates.begin(); i_e != estimates.end(); ++i_e,++index,++i_m)
    //        {
    //            if (*i_m)
    //            {
    //                int targ = target->getInTarget(index);
    //
    //                tp+=((targ == 1) && (  i_e->y == 1 ) )? 1 : 0;
    //                //fp+=((target->getInTarget(i_e->x) == 0) && (  i_e->y == 1 ) )? 1 : 0;
    //                //  tn+=((target->getInTarget(i_e->x) == 0) && (  i_e->y == 0 ) )? 1 : 0;
    //                fn+=((targ == 1) && (  i_e->y == 0 ) )? 1 : 0;
    //
    //                //cout<<target->getInTarget(i_e->x)<<" "<< i_e->y<<endl;
    //            }
    //        }
    //        return tp/(tp+fn);
    //
    //    }
    
    //estimate is N,prediction
    float ClassificationTree::getSensitivity( const std::vector<int2> & estimates,const std::vector<int> & strat_labels, const std::vector<int> & mask , bool in  )
    {
        if (estimates.size() != getNumberOfSamples())
            throw ClassificationTreeException("In calculating sensitivity, number of estimates do not match number of samples");
        //float tp=0;
        float tp=0;
        float fn=0;
        //float fn=0;
        unsigned int index=0;
        vector<int>::const_iterator i_m = mask.begin();
        
        for (vector<int2>::const_iterator i_e  = estimates.begin(); i_e != estimates.end(); ++i_e,++index,++i_m)
        {
            if ( ((*i_m > 0 )&&(in==1)) || ((*i_m == 0 )&&(in==0)) )
            {
                int targ = target->getInTarget(index);
                //  cout<<index<<" -senstarg- "<<targ<<" -pred "<<i_e->x<<" "<<i_e->y<<endl;
                if ( i_e->x != 0 )
                    for (vector<int>::const_iterator i_s = strat_labels.begin(); i_s != strat_labels.end(); ++i_s)
                        if ( targ == *i_s )
                        {
                            tp+=((targ == 1) && (  i_e->y == 1 ) )? 1 : 0;
                            fn+=((targ == 1) && (  i_e->y == 0 ) )? 1 : 0;
                        }
            }
        }
        //        cout<<"Calculate Sensitity "<<tp<<" "<<fn<<endl;
        
        return tp/(tp+fn);
    }
    
    
    float ClassificationTree::getSensitivity( const std::vector<int2> & estimates ){
        float tp=0;
        //  float fp=0;
        //float tn=0;
        float fn=0;
        unsigned int index=0;
        for (vector<int2>::const_iterator i_e  = estimates.begin(); i_e != estimates.end(); ++i_e,++index)
        {
            int targ = target->getInTarget(index);
            
            tp+=((targ == 1) && (  i_e->y == 1 ) )? 1 : 0;
            //fp+=((target->getInTarget(i_e->x) == 0) && (  i_e->y == 1 ) )? 1 : 0;
            //  tn+=((target->getInTarget(i_e->x) == 0) && (  i_e->y == 0 ) )? 1 : 0;
            fn+=((targ == 1) && (  i_e->y == 0 ) )? 1 : 0;
            
            //cout<<target->getInTarget(i_e->x)<<" "<< i_e->y<<endl;
        }
        return tp/(tp+fn);
        
    }
    
    //estimate is N,prediction
    float ClassificationTree::getSensitivity( const std::vector<int2> & estimates,const std::vector<int> & strat_labels )
    {
        if (estimates.size() != getNumberOfSamples())
            throw ClassificationTreeException("In calculating sensitivity, number of estimates do not match number of samples");
        //float tp=0;
        float tp=0;
        float fn=0;
        //float fn=0;
        unsigned int index=0;
        for (vector<int2>::const_iterator i_e  = estimates.begin(); i_e != estimates.end(); ++i_e,++index)
        {
            int targ = target->getInTarget(index);
            //  cout<<index<<" -senstarg- "<<targ<<" -pred "<<i_e->x<<" "<<i_e->y<<endl;
            if ( i_e->x != 0 )
                for (vector<int>::const_iterator i_s = strat_labels.begin(); i_s != strat_labels.end(); ++i_s)
                    if ( targ == *i_s )
                    {
                        tp+=((targ == 1) && (  i_e->y == 1 ) )? 1 : 0;
                        fn+=((targ == 1) && (  i_e->y == 0 ) )? 1 : 0;
                    }
        }
        //        cout<<"Calculate Sensitity "<<tp<<" "<<fn<<endl;
        
        return tp/(tp+fn);
    }
    TpFpTnFn ClassificationTree::getErrorData( const std::vector<int2> & estimates,const std::vector<int> & strat_labels )
    {
        if (estimates.size() != getNumberOfSamples())
            throw ClassificationTreeException("In calculating sensitivity, number of estimates do not match number of samples");
        // vector<unsigned int> tp_fp_tn_fn(4,0);
        TpFpTnFn tp_fp_tn_fn(0,0,0,0);
        //float tp=0;
        //float tp=0;
        // float fn=0;
        //float fn=0;
        unsigned int index=0;
        for (vector<int2>::const_iterator i_e  = estimates.begin(); i_e != estimates.end(); ++i_e,++index)
        {
            int targ = target->getInTarget(index);
            //cout<<index<<" -senstarg- "<<targ<<" -pred "<<i_e->x<<" "<<i_e->y<<endl;
            if ( i_e->x != 0 )
                for (vector<int>::const_iterator i_s = strat_labels.begin(); i_s != strat_labels.end(); ++i_s)
                    if ( targ == *i_s )
                    {
                        //      cout<<"include"<<endl;
                        //tp,fp,tn,fn
                        tp_fp_tn_fn.tp+=((targ == 1) && (  i_e->y == 1 ) )? 1 : 0;
                        tp_fp_tn_fn.fp+=((targ == 0) && (  i_e->y == 1 ) )? 1 : 0;
                        tp_fp_tn_fn.tn+=((targ == 0) && (  i_e->y == 0 ) )? 1 : 0;
                        tp_fp_tn_fn.fn+=((targ == 1) && (  i_e->y == 0 ) )? 1 : 0;
                        
                        
                    }
        }
        // cout<<"Calculate Sensitity "<<tp<<" "<<fn<<endl;
        
        return tp_fp_tn_fn;
    }
    TpFpTnFn ClassificationTree::getErrorData( const std::vector<int2> & estimates,const std::vector<int> & strat_labels,  const std::vector<int> & mask , bool in )
    {
        if (estimates.size() != getNumberOfSamples())
            throw ClassificationTreeException("In calculating sensitivity, number of estimates do not match number of samples");
        // vector<unsigned int> tp_fp_tn_fn(4,0);
        TpFpTnFn tp_fp_tn_fn(0,0,0,0);
        //float tp=0;
        //float tp=0;
        // float fn=0;
        //float fn=0;
        unsigned int index=0;
        vector<int>::const_iterator i_m = mask.begin();
        for (vector<int2>::const_iterator i_e  = estimates.begin(); i_e != estimates.end(); ++i_e,++index,++i_m)
        {
            if ( ((*i_m > 0 )&&(in==1)) || ((*i_m == 0 )&&(in==0)) )
            {
                int targ = target->getInTarget(index);
                //cout<<index<<" -senstarg- "<<targ<<" -pred "<<i_e->x<<" "<<i_e->y<<endl;
                if ( i_e->x != 0 )
                    for (vector<int>::const_iterator i_s = strat_labels.begin(); i_s != strat_labels.end(); ++i_s)
                        if ( targ == *i_s )
                        {
                            //      cout<<"include"<<endl;
                            //tp,fp,tn,fn
                            tp_fp_tn_fn.tp+=((targ == 1) && (  i_e->y == 1 ) )? 1 : 0;
                            tp_fp_tn_fn.fp+=((targ == 0) && (  i_e->y == 1 ) )? 1 : 0;
                            tp_fp_tn_fn.tn+=((targ == 0) && (  i_e->y == 0 ) )? 1 : 0;
                            tp_fp_tn_fn.fn+=((targ == 1) && (  i_e->y == 0 ) )? 1 : 0;
                            
                            
                        }
            }
        }
        //cerr<<"Calculate error data "<<tp_fp_tn_fn.tp<<" "<<tp_fp_tn_fn.fn<<endl;
        
        return tp_fp_tn_fn;
    }
    
    //
    //
    //    float ClassificationTree::getSpecificity( const std::vector<int2> & estimates,  const std::vector<int> & mask  ){
    //        //float tp=0;
    //        float fp=0;
    //        float tn=0;
    //        //float fn=0;
    //        unsigned int index=0;
    //        vector<int>::const_iterator i_m = mask.begin();
    //
    //        for (vector<int2>::const_iterator i_e  = estimates.begin(); i_e != estimates.end(); ++i_e,++index,++i_m)
    //        {
    //            if (*i_m)
    //            {
    //                int targ = target->getInTarget(index);
    //
    //                //tp+=((target->getInTarget(i_e->x) == 1) && (  i_e->y == 1 ) )? 1 : 0;
    //                fp+=((targ == 0) && (  i_e->y == 1 ) )? 1 : 0;
    //                tn+=((targ == 0) && (  i_e->y == 0 ) )? 1 : 0;
    //                //fn+=((target->getInTarget(i_e->x) == 1) && (  i_e->y == 0 ) )? 1 : 0;
    //            }
    //        }
    //        return tn/(tn+fp);
    //    }
    //
    float ClassificationTree::getSpecificity( const std::vector<int2> & estimates,const std::vector<int> & strat_labels,  const std::vector<int> & mask  , bool in)
    {
        
        
        
        //float tp=0;
        float fp=0;
        float tn=0;
        //float fn=0;
        unsigned int index=0;
        vector<int>::const_iterator i_m = mask.begin();
        
        for (vector<int2>::const_iterator i_e  = estimates.begin(); i_e != estimates.end(); ++i_e,++index,++i_m)
        {
            if ( ((*i_m > 0 )&&(in==1)) || ((*i_m == 0 )&&(in==0)) )
            {
                int targ = target->getInTarget(index);
                // cout<<index<<" --spectarg- "<<targ<<" -pred "<<i_e->x<<" "<<i_e->y<<endl;
                if ( i_e->x != 0 )
                    for (vector<int>::const_iterator i_s = strat_labels.begin(); i_s != strat_labels.end(); ++i_s)
                        if ( targ == *i_s )
                        {
                            
                            fp+=((targ == 0) && (  i_e->y == 1 ) )? 1 : 0;
                            tn+=((targ == 0) && (  i_e->y == 0 ) )? 1 : 0;
                        }
            }
        }
        //cout<<"Calculate specificity  "<<tn<<" "<<fp<<endl;
        
        return tn/(tn+fp);
        
    }
    
    //estimate is N,prediction
    
    float ClassificationTree::getSpecificity( const std::vector<int2> & estimates ){
        //float tp=0;
        float fp=0;
        float tn=0;
        //float fn=0;
        unsigned int index=0;
        for (vector<int2>::const_iterator i_e  = estimates.begin(); i_e != estimates.end(); ++i_e,++index)
        {
            int targ = target->getInTarget(index);
            
            //tp+=((target->getInTarget(i_e->x) == 1) && (  i_e->y == 1 ) )? 1 : 0;
            fp+=((targ == 0) && (  i_e->y == 1 ) )? 1 : 0;
            tn+=((targ == 0) && (  i_e->y == 0 ) )? 1 : 0;
            //fn+=((target->getInTarget(i_e->x) == 1) && (  i_e->y == 0 ) )? 1 : 0;
        }
        return tn/(tn+fp);
    }
    
    float ClassificationTree::getSpecificity( const std::vector<int2> & estimates,const std::vector<int> & strat_labels )
    {
        
        
        
        //float tp=0;
        float fp=0;
        float tn=0;
        //float fn=0;
        unsigned int index=0;
        
        for (vector<int2>::const_iterator i_e  = estimates.begin(); i_e != estimates.end(); ++i_e,++index)
        {
            int targ = target->getInTarget(index);
            // cout<<index<<" --spectarg- "<<targ<<" -pred "<<i_e->x<<" "<<i_e->y<<endl;
            if ( i_e->x != 0 )
                for (vector<int>::const_iterator i_s = strat_labels.begin(); i_s != strat_labels.end(); ++i_s)
                    if ( targ == *i_s )
                    {
                        
                        fp+=((targ == 0) && (  i_e->y == 1 ) )? 1 : 0;
                        tn+=((targ == 0) && (  i_e->y == 0 ) )? 1 : 0;
                    }
        }
        //cout<<"Calculate specificity  "<<tn<<" "<<fp<<endl;
        
        return tn/(tn+fp);
        
    }
    
    vector<int> ClassificationTree::getPredictionsForClass( const int & cl )
    {
        vector<int> preds;
        //  cout<<"getPreds "<<endl;
        for ( vector< vector<int3> >::iterator i = estimated_class.begin() ; i != estimated_class.end() ; i++)
        {
            //    cout<<"estimated class"<<endl;
            for (vector<int3>::iterator j = i->begin(); j!=i->end();j++)
            {
                //      cout<<"ClassificationTree::getPredictionsForClass "<<j->x<<endl;
                if (target->getInTarget(j->x) == cl )
                    preds.push_back(j->y);
            }
        }
        return preds;
    }
    
    map<int,pair<float,unsigned int> > ClassificationTree::getClassSensitivities(const std::vector<int2> & estimates,const std::vector<int> & strat_labels )
    {
        if (estimates.size() != target->getNumberOfSamples() )
            throw ClassificationTreeException("ClassificationTree::getWithinClassAccuracies - Number if predictions not equal to number of samples.");
        map<int, pair<float,unsigned int> > class_acc;
        
        unsigned int index=0;
        for (vector<int2>::const_iterator i_e  = estimates.begin(); i_e != estimates.end(); ++i_e,++index)
        {
            int targ = target->getInTarget(index);
            int targ_orig=targ;
            //  cout<<"est targ to add "<<targ<<endl;
            //-----This part will set the target to one, hence accuracies will refelct the proportion labeled as 1
            bool isStrat=false;
            for (vector<int>::const_iterator i_s = strat_labels.begin(); i_s != strat_labels.end(); ++i_s)
            {
                if (targ == *i_s)
                {
                    isStrat=true;
                    break;
                }
                //                    targ=1;
            }
            if (!isStrat)
                targ=1;
            //-----
            
            // cout<<"targ "<<targ<<endl;
            //place via original class labels (post-map)
            if ( i_e->x != 0 )
            {   if ( class_acc.count(targ_orig) )
            {
                map<int, pair<float,unsigned int> >::iterator i_map = class_acc.find(targ_orig);
                
                //  cout<<"exists "<<class_acc.size()<<" "<<i_map->first<<" "<<class_acc.count(targ)<<endl;
                i_map->second.first += ( targ == i_e->y ) ? 1 : 0;
                i_map->second.second++;
            }else{
                //  cout<<"dopesn't exist, add "<<endl;
                class_acc.insert( pair< int, pair<float,unsigned int> >( targ_orig, pair<float,unsigned int>( (targ == i_e->y ) ? 1 : 0,1) ));
                //  class_acc[targ] = pair<float,unsigned int>( targ == i_e->y ) ? 1 : 0,1);
            }
            }
        }
        
        for (map<int, pair<float, unsigned int> >::iterator i = class_acc.begin(); i != class_acc.end(); ++i)
        {
            
            i->second.first /= i->second.second;
            
            //    cout<<"class "<<i->first<<" : "<<i->second.first<<" : "<<i->second.second<<endl;
        }
        return class_acc;
        
    }
    map<int,pair<float,unsigned int> > ClassificationTree::getClassSensitivities(const std::vector<int2> & estimates,const std::vector<int> & strat_labels ,  const std::vector<int> & mask  , bool in)
    {
        if (estimates.size() != target->getNumberOfSamples() )
            throw ClassificationTreeException("ClassificationTree::getWithinClassAccuracies - Number if predictions not equal to number of samples.");
        map<int, pair<float,unsigned int> > class_acc;
        
        
        vector<int>::const_iterator i_m = mask.begin();
        
        
        unsigned int index=0;
        for (vector<int2>::const_iterator i_e  = estimates.begin(); i_e != estimates.end(); ++i_e,++index,++i_m)
        {
            if ( ((*i_m > 0 )&&(in==1)) || ((*i_m == 0 )&&(in==0)) )
            {
                int targ = target->getInTarget(index);
                int targ_orig=targ;
                //  cout<<"est targ to add "<<targ<<endl;
                //-----This part will set the target to one, hence accuracies will refelct the proportion labeled as 1
                bool isStrat=false;
                for (vector<int>::const_iterator i_s = strat_labels.begin(); i_s != strat_labels.end(); ++i_s)
                {
                    if (targ == *i_s)
                    {
                        isStrat=true;
                        break;
                    }
                    //                    targ=1;
                }
                if (!isStrat)
                    targ=1;
                //-----
                
                // cout<<"targ "<<targ<<endl;
                //place via original class labels (post-map)
                if ( i_e->x != 0 )
                {
                    if ( class_acc.count(targ_orig) )
                    {
                        map<int, pair<float,unsigned int> >::iterator i_map = class_acc.find(targ_orig);
                        
                        //  cout<<"exists "<<class_acc.size()<<" "<<i_map->first<<" "<<class_acc.count(targ)<<endl;
                        i_map->second.first += ( targ == i_e->y ) ? 1 : 0;
                        i_map->second.second++;
                    }else{
                        //  cout<<"dopesn't exist, add "<<endl;
                        class_acc.insert( pair< int, pair<float,unsigned int> >( targ_orig, pair<float,unsigned int>( (targ == i_e->y ) ? 1 : 0,1) ));
                        //  class_acc[targ] = pair<float,unsigned int>( targ == i_e->y ) ? 1 : 0,1);
                    }
                }
            }
        }
        
        for (map<int, pair<float, unsigned int> >::iterator i = class_acc.begin(); i != class_acc.end(); ++i)
        {
            
            i->second.first /= i->second.second;
            
            //    cout<<"class "<<i->first<<" : "<<i->second.first<<" : "<<i->second.second<<endl;
        }
        return class_acc;
        
    }
    
    
    
    
    vector<float> ClassificationTree::getAllAccuracies( )
    {	
        vector<float> all_accuracies;
        
        for ( vector< vector<int3> >::iterator i = estimated_class.begin() ; i != estimated_class.end() ; i++)
            all_accuracies.push_back(target->expected_prediction_error( *i ));
        
        return all_accuracies;
        
    }
    vector< vector<float> > ClassificationTree::getAllClassAccuracies()
    {	
        vector< vector<float> > all_accuracies;
        
        for ( vector< vector<int3> >::iterator i = estimated_class.begin() ; i != estimated_class.end() ; i++)
            all_accuracies.push_back(target->perclass_expected_prediction_error( *i ));
        
        return all_accuracies;
        
    }
    void ClassificationTree::outputPerSessionStats(const std::string & filename_in)
    {
        string filename = filename_in;
        int pos = filename.length()-4;
        
        if (!filename.compare(pos,4, ".csv"))
            filename.erase(pos,4);
        
        //-----------raw clasifications--------------------/
        ofstream fout((filename+"_session_acc.csv").c_str());
        
        if (fout.is_open())
        {
            
            vector<int> cl_labels = target->getClassLabels();
            fout<<"Session,";
            for (vector<int>::iterator i_cl = cl_labels.begin(); i_cl != cl_labels.end(); ++i_cl)
                fout<<"Class_"<<*i_cl<<",";
            fout<<"overallACC"<<endl;
            vector< vector<float> > percl_acc = getAllClassAccuracies();
            vector<float> acc  = getAllAccuracies();
            
            vector< vector<float> >::iterator i_perCl = percl_acc.begin();			
            unsigned int sess=0;
            for (vector<float>::iterator i_acc = acc.begin(); i_acc != acc.end(); ++i_acc,++i_perCl,++sess)
            {
                fout<<sess;
                //for alignment purposes
                if (sess<10)
                    fout<<"  ";
                else if (sess<100)
                    fout<<" ";
                fout<<",";
                
                //perclass
                for (vector<float>::iterator ii_perCl = i_perCl->begin(); ii_perCl != i_perCl->end(); ++ii_perCl)
                    fout<<*ii_perCl<<",";
                
                //overall
                fout<<*i_acc<<endl;
            }
            
            fout.close();
        }
    }
    
    
    void ClassificationTree::outputPerSubjectStats(const std::string & filename_in)
    {
        //------------OUTPUT classifications of each suject and stats
        //		size_t pos = filename.find(".csv", 0);
        string filename = filename_in;
        int pos = filename.length()-4;
        
        if (!filename.compare(pos,4, ".csv"))
            filename.erase(pos,4);
        
        //-----------raw clasifications--------------------/
        
        ofstream fout_dist((filename+"_dist.csv").c_str());
        fout_dist<<"Subject_Index,Class,NumberOfPredictions,Accuracy,DistanceScores..."<<endl;
        ofstream fout((filename+"_pred.csv").c_str());
        fout<<"Subject_Index,Class,NumberOfPredictions,Accuracy,Predictions..."<<endl;
        ofstream foutX((filename+"_predX.csv").c_str()); //one fills in with Xs for everyiteration
        foutX<<"SI,CL,Nsam,Accuracy,";
        
        unsigned int NSubjects = target->getNumberOfSamples();
        //		vector<unsigned int> v_Npred(NSubjects,0);
        //	vector<int> base_v;
        //	vector<float> base_vf;
        
        //sort the data to map to subject index
        vector< vector<int> > perSubjectPredictions(NSubjects,vector<int>());
        vector< vector<float> > perSubjectDistances(NSubjects,vector<float>());
        
        //unsigned int session=0;		
        cout<<"CTREE DIST SIZE "<<estimated_distance_scores.size()<<endl;
        vector<int> s_mask(totalNumberOfIterations,0);
        vector< vector<int> > session_mask( NSubjects, s_mask );
        vector< vector< pair<int,float> > >::iterator  i_est_dist = estimated_distance_scores.begin();
        
        cout<<"per sub stats "<<estimated_class.size()<<" "<<estimated_distance_scores.size()<<endl;
        for ( vector< vector<int3> >::iterator i = estimated_class.begin() ; i != estimated_class.end() ; ++i, ++i_est_dist)//, ++session)
        {//loop across session
            //	cout<<"inner"<<endl;
            vector< pair<int,float> >::iterator ii_dist = i_est_dist->begin();
            for (vector<int3>::iterator ii = i->begin(); ii != i->end(); ++ii, ++ii_dist)
            {//loop across predictions
                if ( ii_dist->first  != ii->x)
                    throw ClassificationTreeException("ClassificationTree : PerSubject Distances and Accuracies have differing subject indices. ");
                cout<<"dist "<<ii_dist->first<<" "<<ii_dist->second<<endl;
                //      cout<<NSubjects<<" "<<ii_dist->first<<" "<<ii->x<<endl;
                //       cout<<session_mask[ii->x].size()<<" "<<ii->z<<endl;
                perSubjectDistances[ii_dist->first].push_back(ii_dist->second);
                perSubjectPredictions[ii->x].push_back(ii->y);
                session_mask[ii->x][ii->z]=1;
            }
            //	cout<<"outer"<<endl;
            
            
        }
        //cout<<"done loop"<<endl;
        
        //------------------FOR PREDX GET PER RUN ACCURACIES--------------//
        //cout<<"estomated class size "<<estimated_class.size()<<endl;
        vector<float> perRunAcc = target->perrun_expected_prediction_error(estimated_class);
        for (vector<float>::iterator i = perRunAcc.begin(); i != perRunAcc.end(); ++i)
        {
            //    cout<<"write line "<<*i<<" "<<perRunAcc.size()<<endl;
            foutX<<*i;
            if ( (i+1) == perRunAcc.end() )
                foutX<<endl;
            else 
                foutX<<",";
            
        }
        //-------------------------------//
        
        
        vector<int> targ = target->getTargetData<int>();
        
        {
            
            unsigned int subject_ind = 0;
            vector< vector<float> >::iterator i_dist = perSubjectDistances.begin();
            for (vector< vector<int> >::iterator i = perSubjectPredictions.begin(); i != perSubjectPredictions.end(); ++i, ++subject_ind, ++i_dist)
            {
                //makes spacing consistent
                if (subject_ind < 10){
                    fout<<"  ";
                    fout_dist<<"  ";
                    foutX<<"  ";
                }else if (subject_ind <100){
                    fout_dist<<" ";
                    fout<<" ";
                    foutX<<" ";
                }				
                
                int correct_lb = targ[subject_ind];
                float sub_acc = 0 ;
                for (vector<int>::iterator ii = i->begin(); ii != i->end(); ++ii)
                    sub_acc += (correct_lb == *ii);
                unsigned int Nsamp = i->size();
                sub_acc/=Nsamp;
                
                fout_dist<<subject_ind<<","<<targ[subject_ind]<<","<<Nsamp;
                fout<<subject_ind<<","<<targ[subject_ind]<<","<<Nsamp;
                foutX<<subject_ind<<","<<targ[subject_ind]<<","<<Nsamp;
                if (Nsamp<10)
                {
                    fout_dist<<"   ,";
                    fout<<"   ,";
                    foutX<<"   ,";
                }else if (Nsamp<100) {
                    fout_dist<<"   ,";
                    fout<<"  ,";
                    foutX<<"  ,";
                }else if (Nsamp<1000){
                    fout_dist<<"   ,";
                    fout<<" ,";
                    foutX<<" ,";	
                }else {
                    fout_dist<<"   ,";
                    fout<<",";
                    foutX<<",";	
                }
                
                fout_dist<<sub_acc<<",";
                fout<<sub_acc<<",";
                foutX<<sub_acc<<",";
                //cout<<"Accuracies output "<<endl;
                
                unsigned int session = 0; 
                vector<float>::iterator ii_dist = i_dist->begin();
                for (vector<int>::iterator ii = i->begin(); ii != i->end(); ++ii,++ii_dist)
                {
                    //places 'X' for exclusion
                    while (session_mask[subject_ind][session] == 0) {
                        
                        foutX<<"X,";
                        ++session;
                        
                    }
                    
                    fout_dist<<*ii_dist;
                    fout<<*ii;
                    foutX<<*ii;
                    ++session;
                    
                    if (session != totalNumberOfIterations) {
                        fout_dist<<",";
                        fout<<",";
                        foutX<<",";
                    }
                    
                }
                
                //fill in end zeros
                for (unsigned int i = session; i<totalNumberOfIterations; ++i)//while (session != totalNumberOfIterations)) {
                {
                    foutX<<"X";
                    if (session != (totalNumberOfIterations-1) )//check if is last element
                        foutX<<",";
                }
                fout_dist<<endl;
                fout<<endl;
                foutX<<endl;
            }
            fout_dist.close();
            fout.close();
            foutX.close();
            
        }
        // cerr<<"CTree : Write summary Stats"<<endl;
        {
            //----------------------summary stats
            ofstream fout_sum((filename+"_summary.csv").c_str());
            
            vector<int> cls = target->getClassLabels();
            fout_sum<<"SubjectIndex,Class,NumberOfSamples,";
            for (vector<int>::iterator i = cls.begin(); i!= cls.end(); ++i)
            {
                fout_sum<<"Class_"<<*i;
                if (i!= (cls.end()-1))
                    fout_sum<<",";
                
            }
            fout_sum<<endl;
            unsigned int subject_ind = 0;
            for (vector< vector<int> >::iterator i = perSubjectPredictions.begin(); i != perSubjectPredictions.end(); ++i, ++subject_ind)
            {
                float accuracy = 0;
                fout_sum<<subject_ind<<","<<targ[subject_ind]<<","<<i->size()<<",";
                for (vector<int>::iterator i_cl = cls.begin(); i_cl!= cls.end(); ++i_cl)
                {
                    float count=0;
                    for (vector<int>::iterator ii = i->begin(); ii != i->end(); ++ii)
                        if ( *ii == *i_cl)
                        {
                            ++count;
                        }
                    
                    fout_sum<<count/i->size();
                    if ( *i_cl == targ[subject_ind])
                        accuracy=count/i->size();
                    
                    if (i_cl != (cls.end() -1 )) {
                        fout_sum<<",";
                    }else {
                        fout_sum<<","<<accuracy<<endl;
                        
                    }
                    
                }
                
                
            }
            
            
            fout_sum.close();
        }
    }
    
    void ClassificationTree::predictionError( const unsigned int & index )
    {	
        
        float mean, std_err;
        target->prediction_error( estimated_class[index] , mean, std_err);
        
    }
    
    
    
    
}
