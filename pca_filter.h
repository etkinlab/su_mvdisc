#ifndef PCA_Filter_H
#define PCA_Filter_H
/*
 *  NormalizeFilter.h
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 8/12/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include <base_image_filter.h>

#include <vector>
namespace su_mvdisc_name{

	//WS = within sample
	//AS = across sample
	//enum NORM_MODE { VAR_NORM_WS,MEAN_VAR_NORM_WS,MINMAX_WS, MEAN_NORM_WS,NUMBER_OF_WS,VAR_NORM_AS,MEAN_VAR_NORM_AS,MEAN_NORM_AS, MINMAX_AS  };

	
class PCA_Filter : public base_image_filter
{
	public:
		PCA_Filter();
		virtual ~PCA_Filter();
		void setOptions( std::stringstream & ss_options);
		void applyToTestData();
		void estimateParameters();
		void applyParametersToTrainingData();
		//void runSpecificFilter();
//		void setNormMode( NORM_MODE mode );
//	void setNormMode( const std::string & mode );

protected:
	
private:
	//void normalize( std::vector<float>::iterator  i_begin, std::vector<float>::iterator  i_end, const unsigned int & inc);
	//void ApplyNormalize( std::vector<float>::iterator  i_begin, std::vector<float>::iterator  i_end, const unsigned int & inc,const unsigned int & index);
	float *D,*U,*VT,*work,*cumD2;
	float *mean;
	float totalD2;
	float varThresh;
	unsigned int DimTrim;
	//std::vector<float> cumSumD;
	//void calculateBiasAndScale( float & bias, float & scale, std::vector<float>::iterator  i_begin, std::vector<float>::iterator  i_end, const unsigned int & inc);
	
//	NORM_MODE normalization_method;
//	std::vector<float> v_bias, v_scale;
//	float lower_limit, upper_limit;
};

}
	
#endif
