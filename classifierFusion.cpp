//
//  classifierFusion.cpp
//  su_mvdisc
//
//  Created by Brian M. Patenaude on 7/17/12.
//  Copyright 2012 Stanford University - Etkin Lab. All rights reserved.
//

#include "classifierFusion.h"
#include "misc_utils.h"
#include <clapack_utils.h>
#include <map>
using namespace std;
using namespace misc_utils_name;
using namespace clapack_utils_name;
#ifdef ARCHLINUX
extern "C" {
#include "cblas.h"
#include "f2c.h"
    
    typedef integer __CLPK_integer;
    typedef real __CLPK_real;
#include "clapack.h"
}


#else
#include <Accelerate/Accelerate.h>

#endif

namespace su_mvdisc_name{

    
    CLFusion_mvdisc::CLFusion_mvdisc(){
//        dist_cost=L2NORM;
        dist_cost=CORR;

        inc_thresh=0.05;
    }
    CLFusion_mvdisc::~CLFusion_mvdisc(){
        
    }
    void CLFusion_mvdisc::setOptions( std::stringstream & ss_options)
	{ 
		
		//-------BEFORE SETTING CUSTOM OPTIONS RESET TO DEFAULT----------//
        
		//\---------------SET CUTOM TOPTIONS ------------------//
		
		
		map<string,string> options = parse_sstream(ss_options);
		for ( map<string,string>::iterator it = options.begin(); it!=options.end();it++)
		{
			
			if ( it->first == "Cost")
			{
				if (it->second == "Corr")
                    dist_cost = CORR ;
				else if (it->second == "EqualProb")
					dist_cost = L2NORM;
                else if (it->second == "MajorityVote")
					dist_cost = MAJORITY;///assume 1/0 -1/1 is also compatible use >0 condition
			}else {
				if (!setTreeNodeOptions(it->first, it->second))
					throw ClassificationTreeNodeException((NodeName + " : Invalid input argument : " + it->first).c_str());
			}
		}
		
        
		
	}
    
    int CLFusion_mvdisc::classifyTrainingData()  {
        smatrix train(Dimensionality_,NumberOfSamples_);
        copyToSingleVector(in_data, train.sdata,NumberOfSamples_);
        //  smatrix_print(train, "training data");
        
        if ( dist_cost == MAJORITY){
            ///count to sum across dimensionaltiy
            smatrix_sumRows(train);
            
            
        }else{
            
            float *dif;
            if ( (dist_cost == L2NORM) || (dist_cost == L1NORM) )
                dif = new float[Dimensionality_];
            
            for (unsigned int sub = 0 ; sub < NumberOfSamples_; ++sub)
            {
                list< pair<float,unsigned int> > l_costs;
                
                for (unsigned int sub2 = 0 ; sub2 < NumberOfSamples_; ++sub2)
                {
                    if (sub2!=sub)
                    {
                        
                        
                        if (dist_cost == CORR)
                        {
                            l_costs.push_back(pair<float,unsigned int>(correlation(train.sdata+ sub*Dimensionality_,train.sdata + sub2*Dimensionality_ ,Dimensionality_), sub2) );
                            // cerr<<"Correlation "<<sub<<" "<<sub2<<" "<<corr<<endl;
                            
                        }else if (dist_cost == L2NORM )
                        {
                            cblas_scopy(Dimensionality_, train.sdata + sub*Dimensionality_, 1, dif, 1);
                            cblas_saxpy(Dimensionality_, -1, train.sdata+ sub2*Dimensionality_, 1, dif, 1);
                            
                            float l1=cblas_sasum(Dimensionality_, dif, 1);//check to make sure its not exactly the same
                            
                            float l2=0;
                            if (l1>0)
                                l2 = cblas_snrm2(Dimensionality_, dif, 1);
                            
                            l_costs.push_back(pair<float,unsigned int>(l2, sub2));
                            
                            
                        }else if (dist_cost == L1NORM )
                        {
                            
                            cblas_scopy(Dimensionality_, train.sdata + sub*Dimensionality_, 1, dif, 1);
                            cblas_saxpy(Dimensionality_, -1, train.sdata+ sub2*Dimensionality_, 1, dif, 1);
                            
                            //  float l1=cblas_sasum(Dimensionality_, dif, 1);//check to make sure its not exactly the same
                            l_costs.push_back(pair<float,unsigned int>(cblas_sasum(Dimensionality_, dif, 1), sub2) );
                            
                            
                        }
                        
                    }
                }
                //       cout<<"CLassify subject "<<sub<<endl;
                l_costs.sort();
                unsigned int index_th = l_costs.size()*inc_thresh;
                // cout<<"index_th "<<index_th<<" "<<l_costs.size()<<" "<<inc_thresh<<endl;
                if (index_th==0)
                    index_th=1;
                list< pair<float,unsigned int> >::iterator i_c = l_costs.begin();
                map<float,unsigned int> voter;
                for ( unsigned int i = 0 ; i<index_th;++i,++i_c)
                {
                    //  cout<<"dist/index "<<i_c->first<<" "<<i_c->second<<" "<<training_target->getOutTarget(i_c->second)<<" "<<voter.count( training_target->getOutTarget(i_c->second) )<<endl;
                    if ( voter.count( training_target->getOutTarget(i_c->second) ) == 0 )
                    {
                        voter[training_target->getOutTarget(i_c->second)]=1;
                    }else{
                        voter[training_target->getOutTarget(i_c->second)]++;
                    }
                    
                }
                //            cerr<<"map "<<endl;
                unsigned int count_max=0;
                float class_max=0;
                for (map<float,unsigned int>::iterator i_m = voter.begin(); i_m != voter.end();++i_m)
                {
                    //           cerr<<i_m->first<<" "<<i_m->second<<endl;
                    if (i_m->second > count_max)
                    {
                        count_max=i_m->second;
                        class_max=i_m->first;
                    }
                }
                
                //  cerr<<"max "<<count_max<<" "<<class_max<<endl;
                //cerr<<endl;
                out_data->push_back(class_max);
                
                
            }
            if ( (dist_cost == L2NORM) || (dist_cost == L1NORM) )
                delete[] dif;
        }
        
        return 0;
    }
    
    int CLFusion_mvdisc::classifyTestData()  {
        
        smatrix train(Dimensionality_,NumberOfSamples_);
        copyToSingleVector(in_data, train.sdata,NumberOfSamples_);
        // smatrix_print(train, "training data- apply to test");

        smatrix test(Dimensionality_,NumberOfTestSamples_);
        copyToSingleVector(in_test_data, test.sdata,NumberOfTestSamples_);
        //smatrix_print(test, "test data- apply to test");

        float *dif; 
        if ( (dist_cost == L2NORM) || (dist_cost == L1NORM) )
            dif = new float[Dimensionality_];
        
        for (unsigned int sub = 0 ; sub < NumberOfTestSamples_; ++sub)//sub test samples
        {
            list< pair<float,unsigned int> > l_costs;  
            
            for (unsigned int sub2 = 0 ; sub2 < NumberOfSamples_; ++sub2)//sub2 train samples
            {
                //sub and sub2 index different arrays
                //  if (sub2!=sub)
                {
                    
                    
                    if (dist_cost == CORR)
                    {
                        l_costs.push_back(pair<float,unsigned int>(correlation(test.sdata+ sub*Dimensionality_,train.sdata + sub2*Dimensionality_ ,Dimensionality_), sub2) );
                        // cerr<<"Correlation "<<sub<<" "<<sub2<<" "<<corr<<endl;
                        
                    }else if (dist_cost == L2NORM )
                    {                        
                        cblas_scopy(Dimensionality_, test.sdata + sub*Dimensionality_, 1, dif, 1);
                        cblas_saxpy(Dimensionality_, -1, train.sdata+ sub2*Dimensionality_, 1, dif, 1);
                        
                        float l1=cblas_sasum(Dimensionality_, dif, 1);//check to make sure its not exactly the same
                        
                        float l2=0;
                        if (l1>0)
                            l2 = cblas_snrm2(Dimensionality_, dif, 1);
                        
                        l_costs.push_back(pair<float,unsigned int>(l2, sub2));
                        
                        
                    }else if (dist_cost == L1NORM )
                    {
                        
                        cblas_scopy(Dimensionality_, test.sdata + sub*Dimensionality_, 1, dif, 1);
                        cblas_saxpy(Dimensionality_, -1, train.sdata+ sub2*Dimensionality_, 1, dif, 1);
                        
                        //  float l1=cblas_sasum(Dimensionality_, dif, 1);//check to make sure its not exactly the same
                        l_costs.push_back(pair<float,unsigned int>(cblas_sasum(Dimensionality_, dif, 1), sub2) );
                        
                        
                    }
                    
                }
            }
//            cout<<"CLassify subject "<<sub<<endl;
            l_costs.sort();
            unsigned int index_th = l_costs.size()*inc_thresh;
//             cout<<"index_th "<<index_th<<" "<<l_costs.size()<<" "<<inc_thresh<<" "<<NumberOfSamples_<<" "<<NumberOfTestSamples_<<endl;
            if (index_th==0)
                index_th=1;
            list< pair<float,unsigned int> >::iterator i_c = l_costs.begin();
            map<float,unsigned int> voter;
            for ( unsigned int i = 0 ; i<index_th;++i,++i_c)
            {
                // cout<<"dist/index "<<i_c->first<<" "<<i_c->second<<" "<<training_target->getOutTarget(i_c->second)<<" "<<voter.count( training_target->getOutTarget(i_c->second) )<<endl;
                if ( voter.count( training_target->getOutTarget(i_c->second) ) == 0 )
                {
                    voter[training_target->getOutTarget(i_c->second)]=1;
                }else{
                    voter[training_target->getOutTarget(i_c->second)]++;
                }
                
            }
            //            cerr<<"map "<<endl;
            unsigned int count_max=0;
            float class_max=0;
            for (map<float,unsigned int>::iterator i_m = voter.begin(); i_m != voter.end();++i_m)
            {
//                           cerr<<i_m->first<<" "<<i_m->second<<endl;
                if (i_m->second > count_max)
                {
                    count_max=i_m->second;
                    class_max=i_m->first;
                }
            }
//              cerr<<"max "<<count_max<<"/"<<voter.size()<<" "<<class_max<<endl;
            //cerr<<endl;
            out_test_data->push_back(class_max);
            
            
        }
        if ( (dist_cost == L2NORM) || (dist_cost == L1NORM) )
            delete[] dif;
        
        return 0;
    }
}