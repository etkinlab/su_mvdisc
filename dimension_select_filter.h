#ifndef dimension_select_filter_H
#define dimension_select_filter_H

/*
 *  group_select_filter.h
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 11/12/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


#include <BaseFilter.h>

#include <vector>
namespace su_mvdisc_name{
	
	//WS = within sample
	//AS = across sample
	//enum NORM_MODE { VAR_NORM_WS,MEAN_VAR_NORM_WS,MINMAX_WS, MEAN_NORM_WS,NUMBER_OF_WS,VAR_NORM_AS,MEAN_VAR_NORM_AS,MEAN_NORM_AS, MINMAX_AS  };

		
	class DimensionSelect_Filter : public base_filter
	{
	public:
		DimensionSelect_Filter();
		virtual ~DimensionSelect_Filter();
		void setOptions( std::stringstream & ss_options);
        void setDimension( const unsigned int & dim  );
		void applyToTestData();
		void runSpecificFilter( bool with_estimation = true);
		
	protected:
		
	private:
		enum FILTER_MODE { INCLUDE, EXCLUDE };
		std::vector<unsigned int> _select_dimensions;
		std::vector<short> _dim_mask;
        unsigned int _dimTrim;
		FILTER_MODE filt_mode;
	};
	
}

#endif
