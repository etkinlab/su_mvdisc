#!/bin/sh 
function Usage(){
    echo "\n\n run_confidence.sh <disc_dir> <n_iterations>  <out_file> \n\n"
    exit 1
}


  

if [ $# -ne 3 ] ; then
    Usage
fi

INDIR=$1
DISCOUT_DIR=$INDIR
NITER=$2
OUTFILE=$3


DISCOUT=${DISCOUT_DIR}/Leave-0-Out_
EXEDIR=`dirname $0`
RUN=0
while [ $RUN -lt $NITER ]; do 
    echo run $RUN
    DISCOUT=${DISCOUT_DIR}/Leave-0-Out_${RUN}
    if [ ! -f $DISCOUT/cv_results_disc_errordata.csv ] ; then 
	echo RUN $RUN for $INDIR does not exist
	exit 1
    fi
    ls ${DISCOUT}/cv_results_disc_errordata.csv
    ACC="$ACC `sed -n '2p' ${DISCOUT}/cv_results_disc_errordata.csv | awk -F , '{ print $2 }'`"
    SENS="$SENS `sed -n '2p' ${DISCOUT}/cv_results_disc_errordata.csv | awk -F , '{ print $3 }'`"
    SPEC="$SPEC `sed -n '2p' ${DISCOUT}/cv_results_disc_errordata.csv | awk -F , '{ print $4 }'`"

  #  echo $ACC
  #  echo $SENS
  #  echo $SPEC
    let RUN+=1
done

confidence_interval -f ${DISCOUT_DIR}/Leave-0-Out_ci.acc $ACC  
confidence_interval -f ${DISCOUT_DIR}/Leave-0-Out_ci.sens $SENS  
confidence_interval -f ${DISCOUT_DIR}/Leave-0-Out_ci.spec $SPEC 

