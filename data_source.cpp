/*
 *  data_source.cpp
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 7/20/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#include <misc_utils.h>
#include <cstdlib>
#include "data_source.h"
#include <fstream>
//brian includes

//#include <misc_utils/misc_utils.h>
//#include <misc_utils.h>

#ifdef ARCHLINUX
extern "C" {
#include "cblas.h"
#include "f2c.h"
    
    typedef integer __CLPK_integer;
    typedef real __CLPK_real;
#include "clapack.h"
}


#else
#include <Accelerate/Accelerate.h>

#endif

#include <clapack_utils.h>
using namespace clapack_utils_name;
using namespace misc_utils_name;
using namespace std;

namespace su_mvdisc_name{
	
	DataSource::DataSource()
	{
		//cout<<"DataSource : Constructor"<<endl;
		NumberOfSamples_ = NumberOfTestSamples_ = Dimensionality_ = NumberOfSources_ = 0;
		nodeType=SOURCE;
		FileHasBeenRead=false;
		hasFileNameBeenSet=false;
        multiFileHasBeenSet=false;
		hasDataBeenSet=false;
		NodeName="DataSource";
		PassDownOutput=false;
        src_image_res_=NULL;
        src_image_dims_=NULL;
        src_names_=NULL;
        src_dims_=NULL;

        
	}
	
	DataSource::~DataSource()
	{
		for ( vector< vector<float>* >::iterator i = in_data.begin(); i != in_data.end();i++)
        {
            if ( *i != NULL )
                delete *i;
		}
		for ( vector< vector<float>* >::iterator i = in_test_data.begin(); i != in_test_data.end();i++)
            if ( *i != NULL )
                delete *i;        
	}
	
	void DataSource::readFile( )
	{
		FileHasBeenRead=true;
	}
	void DataSource::deleteInput( bool setDimensionalityToZero )
    {
        for ( vector< vector<float>* >::iterator i = in_data.begin(); i != in_data.end();i++)
        {
            if ( *i != NULL )
                delete *i;
		}
        in_data.clear();
        
        if (setDimensionalityToZero)
            Dimensionality_=0;
        
    }
    
  
    
    void DataSource::readFeatureNames( const std::string & fnames){
        //this funciton provides checking for compatible dimensions and tries to accomodate different input formats
//        cerr<<"DataSource::readFeatureNames:  "<<fnames<<endl;

//        if ( out_data_names_ != NULL)
//            delete out_data_names_;
//        out_data_names_ = new vector<string>();
        
        vector<string>* v_names = new vector<string>();
        
        
		ifstream f_in ( fnames.c_str() );
		
        if ( f_in.is_open() )
        {
          
                int Nlines=0;
                string line;
                vector<string> all_ssv,all_csv,all_nofmt;
                while ( getline(f_in, line) )
                {
                    Nlines+=1;
                    vector<string> l_ssv = ssv_2_vector(line);
                    vector<string> l_csv = ssv_2_vector(line);
                    all_ssv.insert(all_ssv.end(), l_ssv.begin(),l_ssv.end());
                    all_csv.insert(all_csv.end(), l_csv.begin(),l_csv.end());
                    all_nofmt.push_back(line);
                }
                if (Nlines == 1)//if only 1 line, needs to be ssv or csv within line
                {
                    if (all_ssv.size() == Dimensionality_ )
                        *v_names=all_ssv;
                    else if (all_csv.size() == Dimensionality_ )
                        *v_names=all_csv;
                    else
                        throw ClassificationTreeNodeException(("DataSource::readFeatureNames : " + fnames + " : invalid format or incorrect number of elements" \
                                                          + num2str(Dimensionality_) + "(Dim) / " + num2str(all_ssv.size()) + "(ssv) / " + num2str(all_csv.size()) + "(csv)").c_str());
                }else if (Nlines > 0 ){
                    if (Nlines == Dimensionality_)
                    {
                        *v_names=all_nofmt;
                        
                    }else
                        throw ClassificationTreeNodeException(("DataSource::readFeatureNames : " + fnames + " : invalid format or incorrect number of elements" \
                                                              + num2str(Dimensionality_) + "(Dim) / " + num2str(Nlines) + "(Number of Lines)").c_str());
                }else{
                    throw ClassificationTreeNodeException(("Could not open feature name file : "+ fnames + " seems to be empty.").c_str());
                }

            //if (filetype == 0 )
            //    ssv_2_vector(header);
            //else  if (filetype == 1)
             //   _header = csv_2_vector(header);
        
        }else{
            cerr<<("Could not open feature name file : "+ fnames)<<endl;
            throw ClassificationTreeNodeException(("Could not open feature name file : "+ fnames).c_str()  );
        }
        f_in.close();
        print<string>(*v_names , "features_names_");
//        copyToSingleVector(in_data_names_, out_data_names_,1);
//        cerr<<"in_data_names_ "<<in_data_names_.size()<<endl;
        in_data_names_.push_back(v_names);
        cerr<<"out_data_names_size "<<v_names->size()<<endl;
    }
void DataSource::setFeatureNames( const std::vector<string> & v_names )
{
	if ( ! in_data_names_.empty()) in_data_names_.clear();

	vector<string> *names = new vector<string>(v_names.begin(),v_names.end());
	        in_data_names_.push_back(names);

	
}

    
	void DataSource::setInputData()
	{
		if ((!hasFileNameBeenSet) && (!hasDataBeenSet)  && (!multiFileHasBeenSet))
			throw ClassificationTreeNodeException("Tried to read file without having set a filename");
//		cout<<"DataSource::setInputData"<<endl;
		if ((!FileHasBeenRead) && ((hasFileNameBeenSet) || (multiFileHasBeenSet)))
		{
            //Read input file
			readFile();

			FileHasBeenRead=true;

            //if fetaures name isn't empty load them
            if ( ! features_names_.empty() ) {
                readFeatureNames(features_names_);
            }else{
                vector<string>* v_names = new vector<string>();
                for (unsigned int i = 0 ; i < Dimensionality_; ++i)
                {
                    v_names->push_back( "feature_" + to_string(i));
                }
                in_data_names_.push_back(v_names);

            }

            
            
		}
        if (Verbose)
            cout<<NodeName<<" set input data with "<<NumberOfSamples_<<" samples."<<endl;

		//cout<<"set numver of input
		
	}
    
    
    
	void DataSource::addInputData( vector<float> * in_d, const unsigned int & Dim, const unsigned int & Ns )
	{
		if ( (NumberOfSamples_ != Ns) && ( !in_data.empty() ))
			throw ClassificationTreeNodeException("Tried to addInputData to source, but number of smaples didn't match.");
		if ( (in_d->size()/Dim) != Ns )
			throw ClassificationTreeNodeException("Tried to addInputData to source, product of number of samples and Dimensionality_ does not equal size of data.");
		
		
		//cout<<"drsc "<<in_data.size()<<endl;
		in_data.push_back(in_d);
        
		NumberOfSamples_ = Ns;
		Dimensionality_  += Dim;
		v_dims_.push_back(Dim);
		if (src_image_res_ != NULL)
		{
	        src_image_res_->push_back(float4(0,0,0,0));
	        src_image_dims_->push_back(uint3(0,0,0));
	        src_names_->push_back(NodeName);
	        src_dims_->push_back(Dim);
		}
        
		hasDataBeenSet=true;
		FileHasBeenRead=false;

	}
    void DataSource::setInputData( vector<float> * in_d, const unsigned int & Dim, const unsigned int & Ns )
	{
        deleteInput(true);

        
		if ( (NumberOfSamples_ != Ns) && ( !in_data.empty() ))
			throw ClassificationTreeNodeException("Tried to addInputData to source, but number of smaples didn't match.");
		if ( (in_d->size()/Dim) != Ns )
			throw ClassificationTreeNodeException("Tried to addInputData to source, product of number of samples and Dimensionality_ does not equal size of data.");
		
		
		//cout<<"drsc "<<in_data.size()<<endl;
		in_data.push_back(in_d);
        
		NumberOfSamples_ = Ns;
		Dimensionality_  += Dim;
		v_dims_.push_back(Dim);
		if (src_image_res_ != NULL)
			{
		        src_image_res_->push_back(float4(0,0,0,0));
		        src_image_dims_->push_back(uint3(0,0,0));
		        src_names_->push_back(NodeName);
		        src_dims_->push_back(Dim);
			}
		hasDataBeenSet=true;
		FileHasBeenRead=false;
        
	}
  void DataSource::setInputData( const clapack_utils_name::smatrix& d )
  {
    
    deleteInput(true);
    
addInputData(d);
 
 
 
  }
  
    void DataSource::addInputData( const clapack_utils_name::smatrix& d )
	{
        
		if ( (NumberOfSamples_ != static_cast<unsigned int>(d.MRows)) && ( !in_data.empty() ))
			throw ClassificationTreeNodeException("Tried to addInputData to source, but number of smaples didn't match.");
        
		vector<float>* vec = new vector<float>(d.MRows * d.NCols);
                

		//memcpy(&(vec->at(0)), d.sdata, sizeof(float) * vec->size());
        for (__CLPK_integer r = 0 ; r< d.MRows ; ++r)
            cblas_scopy(d.NCols, d.sdata+r,d.MRows, &(vec->at(r*d.NCols)),1);    
   
        in_data.push_back(vec);
        //print(*(in_data[0]),"indata added to src");
		NumberOfSamples_ = d.MRows;
		Dimensionality_  += d.NCols;
		v_dims_.push_back(d.NCols);

        if (src_image_res_ != NULL)
				{
        src_image_res_->push_back(float4(0,0,0,0));
        src_image_dims_->push_back(uint3(0,0,0));
        src_names_->push_back(NodeName);
        src_dims_->push_back(Dimensionality_);
				}

        
		hasDataBeenSet=true;
		FileHasBeenRead=false;
        
	}
    
    
	void DataSource::setFileName( const std::string & s )
	{
		filename=s;
		FileHasBeenRead=false;
		hasFileNameBeenSet=true;
	}
    void DataSource::setFileNames( const std::vector< std::string > & s )
    {
        v_filenames.clear();
        for ( vector<string>::const_iterator i = s.begin(); i != s.end(); ++i)
            v_filenames.push_back(*i);
        multiFileHasBeenSet=true;
        FileHasBeenRead=false;


    }

	
	
	void DataSource::setIOoutput()
	{
		if (output_feature_mask_ == NULL)
			output_feature_mask_ = new vector<bool>(Dimensionality_+DimTrim,1);
        
       // cout<<"setIOoutput "<<NodeName<<" "<<output_feature_mask->size()<<endl;

	//	else {
	//		output_feature_mask->clear();
	//		output_feature_mask->resize(Dimensionality_+DimTrim, 1);
	//	}

	}
	//for inital tree setup
	void DataSource::setTarget( Target* target_in )
	{
        if (training_target!=NULL)
            delete training_target;
		training_target = new Target(target_in);
			Kclasses_ = target_in->getNumberOfClasses();
	}
	
    
    void DataSource::setTargetData( const std::vector<float> & targ_data )
    {
        if (training_target==NULL)
            throw ClassificationTreeNodeException("Tried to set target data at Data Source, however target does not yet exist");
        
        training_target->setTargetData(targ_data);
        
    }

    
	//for call within tree
	void DataSource::setTarget()
	{
		
	}
/*	void DataSource::applyToTestData()
	{
		cout<<"apply test datat"<<endl;
	}
*/
	void DataSource::setTestData()
	{
        //cout<<" DataSource::setTestData "<<endl;
		//if (out_test_data->empty())
			//throw ClassificationTreeNodeException("No test data at source. Either apply selection mask or set explicitly.");
		if (out_test_data == NULL)
			cerr<<"WARNING: No test data at source. Either apply selection mask or set explicitly."<<endl;
		else{
			if (NumberOfTestSamples_ == 0 )
				cerr<<"WARNING: Number of Test Data  is equal to zero."<<endl;
		}
//			throw ClassificationTreeNodeException("Number of Test Data  is equal to zero.");
		//cout<<"warning, not test data "<<endl;
	}
	void DataSource::setTestData( std::vector<float>* t_data, const unsigned int & Ns )
	{
		//in_test_data = t_data;
        
        
        if (out_test_data != NULL)
			delete	out_test_data;
		
		out_test_data = t_data;
		NumberOfTestSamples_ = Ns;
	}
    void DataSource::setTestData( const std::vector<float> & t_data, const unsigned int & Ns )
    {
        //in_test_data = t_data;
        
        
        if (out_test_data != NULL)
            delete	out_test_data;
        
        out_test_data = new vector<float>(t_data);
        
        NumberOfTestSamples_ = Ns;
    }
    
    //shopudl be interleaving stuff in there
    /*
	void DataSource::copyInTestData( const std::vector< std::vector<float>* >  & t_data, const unsigned int & N)
	{
              
	//	cout<<"copy in test data "<<endl;
		if (out_test_data == NULL)
			out_test_data = new vector<float>();
		for (vector< std::vector<float>* >::const_iterator i = t_data.begin(); i != t_data.end(); ++i)
		{
			out_test_data->insert(out_test_data->end(),(*i)->begin(),(*i)->end());
	//		cout<<"copying in test data "<<(*i)->size()<<endl;
		}
	//	cout<<"test data szie "<<out_test_data->size()<<endl;
		NumberOfTestSamples_=N;

	}
     */
    
    
    void DataSource::copyInData_to_OutData()
    {
    
        if (Verbose)
        {
           //cout<<"DataSource::copyInData_to_OutData"<<endl;
            cout<<in_data.size()<<endl;
            cout<<in_data[0]->size()<<endl;
        }
        if (out_data == NULL)
			out_data = new vector<float>();
		else {
            out_data->clear();
			//out_data->resize(NumberOfSamples);
		}
        
//        cout<<"oudata1 "<<in_data.size()<<" "<<out_data->size()<<endl;

		for ( vector< vector<float>* >::iterator i = in_data.begin(); i != in_data.end();i++)
			out_data->insert(out_data->end(),(*i)->begin(),(*i)->end());
     //   cout<<"oudata2 "<<out_data->size()<<endl;
        
    }

	void DataSource::copyInData_to_TestData()
	{

		if (out_test_data == NULL){
            out_test_data = new vector<float>();
        
        }else {
            out_test_data->clear();
		}

		for ( vector< vector<float>* >::iterator i = in_data.begin(); i != in_data.end();i++)
			out_test_data->insert(out_test_data->end(),(*i)->begin(),(*i)->end());

		NumberOfTestSamples_ = NumberOfSamples_;
        //cerr<<"DataSource::copyInData_to_TestData "<<NumberOfTestSamples_<<endl;

		
	}
	unsigned int DataSource::getNumberOfSources() const
	{
		return NumberOfSources_;
	}
    unsigned int DataSource::getNumberOfInputSamples() 
	{
//        cout<<"DataSource::getNumberOfInputSamples"<<endl;
		if (!FileHasBeenRead)
			setInputData();
//        cout<<"DataSource::getNumberOfInputSamplesdone "<<endl;

        if (in_data.empty())
            return 0;
        
		return  in_data[0]->size()/v_dims_[0];
	}
    
	
	unsigned int DataSource::getNumberOfSamples() 
	{
		if (!FileHasBeenRead)
			setInputData();
		return NumberOfSamples_;
	}

	
	
	vector<int> DataSource::applySelectionMask( const vector<int> & selection_mask, const vector<int> & selection_test_mask)
	{//returns indices to classes left out
    // cout<<"data sourrce enter apply selection mask"<<endl;
        if (!FileHasBeenRead)
        {
           // cout<<"file has not been read yet, set input data"<<endl;
            setInputData();
            
        }//else
         // cout<<"file has already been read"<<endl;
       // cout<<"data source applyselection mask"<<endl;
        if (training_target==NULL)
		{
			throw ClassificationTreeNodeException("Tried to apply selection mask before setting the target");	
		}else {
           // cout<<"DS: target applysel mask"<<endl;
			training_target->applySelectionMask(selection_mask,selection_test_mask);
		}
		//handles Number of Sample
	//	NumberOfSamples = NumberOfSamples_Orig;
		
		
		
	//	cout<<"i justr applied it to  the target "<<endl;
		vector<int> leftout_indices;
	
		//This function differs from previous by allowing to apply a selection mask (useful for cross-vlidation
		//***could add as integers for boot strapping
		
		//outer vector is number of input sources 
		//innner vector is unwrapped data of size NxM
		clearOutData();
		/*
		if (out_data == NULL) {
			out_data = new	vector<float>();
		}else {
			out_data->clear();
		}
		
		if (out_test_data == NULL) {
			out_test_data = new	vector<float>();
		}else {
			out_test_data->clear();
		}

	*/
	
//		print<unsigned int>(selection_mask,"Selection Mask");
        cout<<"copy names "<<in_data_names_.size()<<endl;
        copyToSingleVector(in_data_names_,out_data_names_, 1);
        
		//clear all data
		NumberOfSamples_=0;
		NumberOfTestSamples_=0;
		
		for (vector< int>::const_iterator i=selection_mask.begin();i!=selection_mask.end();i++)
			NumberOfSamples_+=*i;
		
	//	cout<<"DataSOurce : NumberOfSamples_ "<<NumberOfSamples_<<endl;
		//combine data from all sources
		//subject refers to row of input data, rows could be input multiple times in bootstrapping case (why not called sample)
		
		vector< int>::const_iterator i_mask = selection_mask.begin();
		vector< int>::const_iterator i_test_mask = selection_test_mask.begin();
		//cout << "selmask 2 "<< selection_mask.size()<<" "<<selection_mask.size()<<endl;
		
		unsigned int sample_count=0;//used for safety check
		for (unsigned int subject=0;subject<selection_mask.size();++subject,++i_mask, ++i_test_mask)
		{
			//this first if determines whether/which left out subjects to testdata
			if ((*i_mask==0) && (*i_test_mask !=0))
			{
				vector<unsigned int>::const_iterator i_M = v_dims_.begin();
				for (vector< vector<float>* >::iterator i_sources=in_data.begin(); i_sources!=in_data.end();i_sources++,i_M++)//loop around each input source
				{
					out_test_data->insert(out_test_data->end() , (*i_sources)->begin()+ subject * (*i_M) ,(*i_sources)->begin() + (subject+1) * (*i_M)  );
					
				}

				leftout_indices.push_back(subject);
				NumberOfTestSamples_++;
				
			}else {		
				
				//by allowing multiple inputs of same sample it facilitates bootstrapping
				for (  int NperSubject=0; NperSubject< *i_mask ; NperSubject++)
				{
					sample_count++;
					
					//this loops over number of input sources (i.e. multiple inputs of N sample with correspoinding subjects)
					vector<unsigned int>::const_iterator i_M = v_dims_.begin();
					for (vector< vector<float>* >::iterator i_sources=in_data.begin(); i_sources!=in_data.end();++i_sources,++i_M)//loop around each input source
					{
						//copy data from all sources
						//case of 1 source it is a copy of the vector
						out_data->insert(out_data->end() , (*i_sources)->begin()+ subject * (*i_M) ,(*i_sources)->begin() + (subject+1) * (*i_M)  );
						
					}
				}

			}
			
		}

		if (sample_count!=NumberOfSamples_)
		{
			cerr<<"Inappropriate number of samples were loaded. "<<endl;
			cerr<<"Expected "<<NumberOfSamples_<<" samples, added "<<sample_count<<endl;
			exit (EXIT_FAILURE);
		}
//		print<float>(*in_data[0],"training data set -  indata after applying selection mask");
  //      displayTrainingData();
		//cout<<"Indata siuze "<<in_data.size()<<endl;
		//print<float>(*out_data,"training data set - after applying selection mask");
		return leftout_indices;
	}
	
	
	void DataSource::displayTrainingData()
	{
		unsigned int number_of_sources=in_data.size();

		for ( unsigned int i = 0 ; i < number_of_sources ; i++ )
		{
			//count<<endl<<"Source: "<<i<<endl<<endl;
			//count<<"Mdims: "<<Dimensionality_<<endl;
			int offset = 0;

			for ( unsigned int subject=0; subject<NumberOfSamples_;subject++)
			{
				for (unsigned int i_off=0;i_off<i;i_off++)
					offset+=v_dims_.at(i_off);
				
				//count<<"subject "<<subject<<" dim "<<v_dims[i]<<" "<<offset<<endl;
				vector<float>::iterator i_data=out_data->begin() + offset + Dimensionality_*subject  ;
				for (unsigned int j =0 ; j<v_dims_.at(i); j++,i_data++)
				{
					cout<<*i_data<<" ";
				}
				cout<<endl;
			}
			cout<<endl<<endl;
		}
		
	//	cout<<"combined sources:"<<endl;
		for (vector<float>::iterator i=out_data->begin();i!=out_data->end();i++)
			cout<<*i<<" ";
		cout<<endl;
		
		//print<int>(training_target->getTargetData(),"Training Target");
		
	}
	void DataSource::displayTestData()
	{
		
			cout<<"Mdims: "<<Dimensionality_<<endl;
			int offset = 0;
		cout<<"cuurrently only displayes test data for a single source"<<endl;
			for ( unsigned int subject=0; subject<NumberOfTestSamples_;subject++)
			{
			
				cout<<"subject "<<subject<<" dim "<<v_dims_[0]<<" "<<offset<<" "<<in_test_data.size()<<endl;
				vector<float>::iterator i_data=out_test_data->begin() + offset + Dimensionality_*subject  ;
				for (unsigned int j =0 ; j<v_dims_[0]; j++,i_data++)
				{
					cout<<*i_data<<" ";
				}
				cout<<endl;
			}
			cout<<endl<<endl;

		
	}
	
}
