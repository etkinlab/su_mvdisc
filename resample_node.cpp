/*
 *  Resample_Node.cpp
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 11/11/10.
 *  Copyright 2010 NRI. All rights reserved.
 *
 */

#include "resample_node.h"
#include <vector>
#include <misc_utils.h>
//#include <misc_utils/misc_utils.h>

//base nodes
#include <data_source.h>
#include <BaseFilter.h>


//compatible nodes
#include <glm_filter.h>
#include <svm_disc.h>

using namespace std;
using namespace misc_utils_name;

namespace su_mvdisc_name{



ResampleNode::ResampleNode()
{
	nodeType=NESTED_TREE;
	//filter_t = nt_Unspecified;
	NodeName="Resample Node";
	//RequireSelectionMask=false;
	//filter_object=NULL;
	seed_init=RANDOM;
//	useAbsoluteValue=false;
//	accu_thresh=0.65;
//	filterMode = FEATURE_SELECTION;
//	rf_inc_crit = NoExclusions;
//	testMethod = WihtinSample;
	nodeSampleMethod=BOOTSTRAP;
//	rf_thresh=0;
//	ci_alpha=0.05;
//	B=1000;
}
	


ResampleNode::~ResampleNode()
	{
	//	if ( filter_object != NULL )
	//		delete filter_object;
	}

	
	void ResampleNode::setOptions( std::stringstream & ss_options)
	{
		map<string,string> options = parse_sstream(ss_options);
		for ( map<string,string>::iterator it = options.begin(); it!=options.end();it++)
			if ( it->first == "sampleMethod")
			{
				if (it->second == "bootstrap")
					nodeSampleMethod = BOOTSTRAP;
		
			}else if (it->first == "Initial_Seed")
			{
				if ( it->second == "Zero")
				{
					seed_init = ZERO;
				}else if (it->second == "Random" ) {
					seed_init =	RANDOM;
				}else {
					throw ClassificationTreeNodeException("Reliability Filter: Invalid Initial_Seed " + it->second + ".");
					
				}
				
			}else {
				if (!setTreeNodeOptions(it->first, it->second))
					throw ClassificationTreeNodeException("Reliability Filter: Option, "+it->first+", does not exist.");
				
			}

		

		
		//else {
		//			throw ClassificationTreeNodeException("Invalid input argument");
		//		}
		
	}
	
	
void ResampleNode::runNode( bool with_estimation )
{

	//reliability doesn't use the with estimation
	if (Verbose)
		cout<<"Running Relaibility Filter"<<endl;

    //run_node
    
	
	//filter_object->setOptions(node_options);
	for (vector< ClassificationTreeNode* >::iterator i = node_inputs.begin() ; i != node_inputs.end(); i++)
	{
		(*i)->runNode();
	}

	
	setTarget();
	
	setInputData();
	
	setTestData();

//----------------------------------- LETS SET UP OUR TREE NODE THAT WERE GOING TO DO RELIABILITY FILTER FOR----------------------------//
	/*//First we need to store the original training data, so we can set it back later
	DataSource* d_source  = new DataSource();
	d_source->setTarget(training_target);
	for ( vector< vector<float>* >::iterator i = in_data.begin(); i != in_data.end();i++)
	{
		vector<float>* data_src = new vector<float>((*i)->begin(),(*i)->end());
		d_source->addInputData(data_src,Dimensionality,NumberOfSamples);		
	}

	filter_object->addInput(d_source);
	
	//generate selection mask, here is where you allows options for bootstrap etc...
	if ( seed_init == ZERO )
		srand ( 0 );
	else if (seed_init == RANDOM) {
		srand ( time(NULL) );
	}

//	
	vector< vector< int> > selection_masks;
	
	if ( sampleMethod == BOOTSTRAP )
	{
		if (Verbose)
			cout<<"Start Bootstrapping (NumberOfSamples): "<<NumberOfSamples<<endl;
		//for (unsigned int i = 0 ; i < B; ++i)
		unsigned int i_B = 0 ;
		while (  i_B < B ) 
		{
			vector< int > one_mask(NumberOfSamples,0);
			//allows multiple occurances, selection masks allow for repeated number of sample. Yeah for me for enabling this at the beginning :D
			for (unsigned int j = 0 ; j < NumberOfSamples ; ++j)
				++one_mask[rand() % NumberOfSamples];
			
			
		//	if (Verbose)
		//	{
		//		cout<<"B "<<i_B<<" "<<NumberOfSamples<<" "<<training_target->getNumberOfSamples()<<endl;
		//		print< int>(one_mask, "A bootstrap sample");
		//		print<int>(training_target->getTargetData<int>(),"targetrf ");
		//	}
			//lets just make sure that we have at least one of each group, or else group tests will fail
			//this will be mostly for debugging
			bool valid_sample=true;

			{//test if vlaid sample
				map<int,int> label2count;
			//	for (unsigned int ii = 0 ; ii< NumberOfSamples; ++ii)
					unsigned int t_index=0;
					for(vector<int>::iterator i_one_mask = one_mask.begin(); i_one_mask != one_mask.end(); ++i_one_mask,++t_index)
				{
					//if (one_mask[ii]>0)
					if (*i_one_mask > 0)
					{
						for (int samp = 0; samp < *i_one_mask; ++samp)
						{
//						label2count[training_target->getInTarget(ii)]++;
//						cout<<"ii "<<ii<<" "<<training_target->getInTarget(ii)<<endl;
							label2count[training_target->getInTarget(t_index)]++;
						//	cout<<"ii "<<t_index<<" "<<*i_one_mask<<" "<<training_target->getInTarget(t_index)<<endl;
						}
					}
				}
				//enforce requirement that at least as 2 samples per class
				if (label2count.size() != Kclasses )
				{
				//	cout<<"failed "<<label2count.size()<<" "<<Kclasses<<endl;
					//bootstrap must contain at least one sample from each class
					valid_sample=false;
				}								
				for (map<int,int>::iterator i_m = label2count.begin(); i_m != label2count.end(); ++i_m)
					if (i_m->second < 2)
					{
						//bootstrap must contain at least 2 samples from each class
						valid_sample=false;
					}
				
			}
			//cout<<"push back sel mask"<<endl;
			//valid if sample contains at least 2 samples from each class
			if (valid_sample)
			{
				selection_masks.push_back(one_mask);
				++i_B;
			}
		}
	}	
     */
	//count<<"Number of Sample in RF filter "<<NumberOfSamples<<endl;
	//count<<"NNs3 "<<d_source->getNumberOfSamples()<<endl;
//------------------------------START RUNNING THE BOOTSTRAP OR OTHER SAMPLE-----------------------------------------------------//
    
    for (vector< ClassificationTreeNode* >::iterator i = node_inputs.begin() ; i != node_inputs.end(); i++)
    {
        (*i)->deleteOutData();
    }
}
	
	

}