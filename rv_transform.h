
#ifndef rv_transform_H
#define rv_transform_H
//
//  rv_transform.h
//  su_mvdisc
//
//  Created by Brian M. Patenaude on 7/23/12.
//  Copyright 2012 Stanford University - Etkin Lab. All rights reserved.
//

#include <base_image_filter.h>

//#include <vector>
namespace su_mvdisc_name{
    
	//WS = within sample
	//AS = across sample
	enum XFM_MODE { SQUARE, SQRT, LOG, INVLOG, THRESH  };
    enum XFM_TARG { FEATURES, TARGET  };

	
    class RV_Transform : public base_filter
    {
	public:
		RV_Transform();
		virtual ~RV_Transform();
		void setOptions( std::stringstream & ss_options);
        void applyToTestData();
        //no parameters to estimate
        //  void estimateParameters();
        void applyParametersToTrainingData();
        void setXFMTarg( const std::string & mode );

		void setXFMMode( XFM_MODE mode );
        void setXFMMode( const std::string & mode );
        
    protected:
        
    private:
    
        std::vector<float> xfm_params;
        XFM_TARG _xfm_targ;
        XFM_MODE transformation_method;

    };
    
}

#endif
