//
//  classifierFusion.h
//  su_mvdisc
//
//  Created by Brian M. Patenaude on 7/17/12.
//  Copyright 2012 Stanford University - Etkin Lab. All rights reserved.
//

#ifndef classifierfusion_mvdisc_
#define classifierfusion_mvdisc_


#include <vector>
#include <string>
#include <sstream>
#include "base_mvdisc.h"

#include <clapack_utils.h>


namespace su_mvdisc_name{
    
    //#pragma GCC visibility push(default)
    
    class CLFusion_mvdisc : public base_mvdisc
    {
    public:
        enum COST { MAJORITY, CORR, L1NORM, L2NORM  };

       CLFusion_mvdisc();
        
        ~CLFusion_mvdisc();
        void setOptions( std::stringstream & ss_options);
        
        //void estimateParameters();
     //   void setParameters();
        //int classify(const std::vector<float> & vec) const ;
        int classifyTrainingData()  ;
        int classifyTestData()  ;
        
    private:
		COST dist_cost;
        float inc_thresh;//what percentage to include
    };
    
}
//#pragma GCC visibility pop
#endif
