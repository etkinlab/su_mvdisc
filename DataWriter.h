#ifndef DataWriter_H
#define DataWriter_H

/*
 *  DataWriter.h
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 8/19/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#include <string>
//#include <ClassificationTreeNode.h>

class DataWriterException : public std::exception{
	
public:
	const char* errmesg;
	DataWriterException(const char* msg)
	{
		errmesg= ("DataWriterException  : " + std::string(msg)).c_str();
	}
	DataWriterException(const std::string msg)
	{
		errmesg=("DataWriterException  : " + msg).c_str();
	}
	
private:
	virtual const char* what() const throw()
	{
		//std::cout<<errmesg<<std::endl;
		return errmesg;
	}
};

namespace su_mvdisc_name{

	class DataWriter 
	{
	public:
		DataWriter();
		virtual ~DataWriter();
		virtual void write();
		virtual void write( const std::string & filename );
		//virtual static void write( const std::string & filename );

	protected:
	private:
		
	};
}

#endif
