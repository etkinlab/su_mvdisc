//
//  qda.h
//  su_mvdisc
//
//  Created by Natasha Parikh on 5/30/12.
//  Copyright (c) 2012 Stanford University - Etkin Lab. All rights reserved.
//

#ifndef su_mvdisc_qda_h
#define su_mvdisc_qda_h 1

#include <vector>
#include <string.h>
#include <sstream>
#include "base_mvdisc.h"

#include <clapack_utils.h>


namespace su_mvdisc_name{
    
    class QDA_mvdisc : public base_mvdisc
    {
        
    public:
        
        enum PRIOR_TYPE { SAMPLE_PROB, EQUAL_PROB };
        
        // Constructor and destructor
        QDA_mvdisc();
        ~QDA_mvdisc();
        
    
        // setOptions
        // Parses a stringstream of options, puts them into a map, and changes 
        // data members (cl_prior_choice and frac_var_thresh) accordingly
        void setOptions(std::stringstream & ss_options);
        
        
        // estimateParamenters
        // Finds the means for each class, the sum of the eigenvalues' logs, and
        // stores the svd of each class's data
        void estimateParameters();
        
        
        void setParameters();
        
        
        // classifyTrainingData
        // Calculates the distance from the mean for the training data.
        // It also predicts which class each of the samples fall into
        int classifyTrainingData();
        
        
        // classifyTestData
        // Calculates the distance from the mean for the test data.
        // It also predicts which class each of the test samples fall into
        int classifyTestData();
        
    private:
		
        float ZERO_THRESH;
        float frac_var_thresh;
        
        std::vector< float > class_priors, eigenSums_;                          // vectors of the class priors and the sum of the log of the eigenvalues,
                                                                                // separated by class
        
        std::vector< clapack_utils_name::smatrix > dataVec_, DxUtVec_, muVec_;  // vectors of data, svd information DxUt, and means,
                                                                                // separated into matrices by class
        
        unsigned int alloc_Dimensionality, alloc_NumberOfSamples;
        PRIOR_TYPE cl_prior_choice;
        
    };
    
}


#endif // su_mvdisc_qda_h
