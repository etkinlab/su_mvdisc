//
//  logistic_regression_exec.cpp
//  su_mvdisc
//
//  Created by Natasha Parikh on 6/8/12.
//  Copyright (c) 2012 Stanford University - Etkin Lab. All rights reserved.
//



#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <log_regression.h>
#include <data_source.h>
#include <target.h>

#include <clapack_utils.h>

#include <misc_utils.h>
//#include <atlas_utils/atlas_utils.h>

using namespace std;
using namespace misc_utils_name;
using namespace clapack_utils_name;

using namespace su_mvdisc_name;


using namespace std;

void usage() {
    cerr << "\n Usage: \n \n logistic_regression_exec  <data.txt> <target.txt> <test_data.txt> \n " << endl;
    //cerr << "\n [test_data.txt] is required if using image data \n" << endl;
}

int main( int argc, char * argv[])
{
    if ( argc <  2 ) {
        cerr << "\n  ***Not enough input arguments***" << endl;
        usage();
        exit (EXIT_FAILURE);
    }
    
    //N is number of lines (i.e. dimensionaility)
    //bool doSpatialReg=true;
    //bool useImTarget=false;
    int arg_ind=1;
    string in_data = argv[arg_ind];
    cout << "In data " << in_data << endl;
    
    ++arg_ind;    
    
    string in_target = argv[arg_ind];
    ++arg_ind;
    
    cout << "in target " << endl;
    string in_test_data;
    
    cerr << "test data: " << argv[arg_ind] << endl; 
    
    in_test_data = argv[arg_ind];
    ++arg_ind;
    
    vector<float>* data = new vector<float>();
    vector<float>* test_data = new vector<float>();
    
    vector<float> target;
    cout << "read data " << endl;
    
    // read in data 
    ifstream fdata(in_data.c_str());
    string stemp;
    while (getline(fdata,stemp)) {
        vector<float> vftemp = csv_string2nums<float>(stemp);
        data->insert(data->end(), vftemp.begin(), vftemp.end());        
    }
    
    fdata.close();
    
    fdata.open(in_data.c_str());
    cout << "done data read" << endl;
    
    int Ns = 0;
    //char* line = new char[10000];
    
    while (getline(fdata,stemp)) //fdata.getline(line,10000))
        ++Ns;
    
    fdata.close();
    //delete[] line;
    
    cout << "done count " << Ns << endl;
    unsigned int Ndim = static_cast<unsigned int>(data->size() / Ns);
    cout << "Ndim : " << Ndim << " Ns : " << Ns << endl;
    
//    for (int i = 0; i < Ns; ++i) {
//        cout << "i " << i << endl;
//        
//        for (int j = 0; j < Ndim; ++j)
//            cout << data->at(i * Ndim + j) << " ";
//        
//        cout << endl;
//    }
    
    //read in test data 
    float ftemp;
    ifstream fdata_test(in_test_data.c_str());
    while (getline(fdata_test, stemp) ) {
        vector<float> vftemp = csv_string2nums<float>(stemp);
        test_data->insert(test_data->end(), vftemp.begin(), vftemp.end());        
    }
        
    fdata_test.close();
        
    ifstream ftarget(in_target.c_str());
    while (ftarget >> ftemp) {
        target.push_back(ftemp);
        //cout << "target " << ftemp << endl;
    }
        
    ftarget.close();
        
    ///dimensionality oif test data and input need be the same
    int Ns_test = static_cast<int>(test_data->size() / Ndim);
    
    Target * node_targ = new Target();
    node_targ->setTargetData(target);
            
    cout << "LogRegExec: data src" << endl;
    DataSource * data_src = new DataSource();
    data_src->setVerbose(false);
    cout << "datasrc " << data->size() << endl;
    //    cout << "datasrc " << data->size() << " " << data[0] << endl;
    //print(data,"log reg exec");
    data_src->addInputData(data, Ndim, Ns);
        
    //needed for non-crossvalidation
    data_src->copyInData_to_OutData();
    data_src->setTarget(node_targ);
    data_src->setTestData(test_data, Ns_test);
        
    cout << "LogRegExec: LogisticRegression" << endl;
    log_regression_mvdisc* lr = new log_regression_mvdisc();
    lr->setVerbose(false);
    //lr->setBackStepWiseGenAccOn(true, 2);
    //lr->setForwardStepWiseLROn(true);
    lr->addInput(data_src);
    lr->runNode();
        
    delete lr;
    delete data_src;
        
    // data source delete should take care of vector
    delete node_targ;
    
    return 0;
}