//
//  generalization_filter.h
//  su_mvdisc
//
//  Created by Brian Patenaude on 7/23/12.
//  Copyright (c) 2012 Stanford University - Etkin Lab. All rights reserved.
//

#ifndef su_mvdisc_generalization_filter_h
#define su_mvdisc_generalization_filter_h
#include <ClassificationTreeNode.h>
#include <sstream>

namespace su_mvdisc_name{
    
    class GenFilter : public ClassificationTreeNode
	{
	public:
		GenFilter();
        
		virtual ~GenFilter();
        
        //	void setNode( ClassificationTreeNode* data_filter );
		void runNode(bool with_estimation = true);
        
        //	virtual void runSpecificFilter();
		void setOptions( std::stringstream & ss_options);
        
        // a way for the user to change the number of weighted
        // iterations Ada Boost will complete before ending
        // void setMIterations(int M);
        
        //void setIOoutputSpecificFeatureSelect()
        
	protected:
		
	private:
        
		enum Node_Type { nt_Unspecified, nt_LDA, nt_QDA, nt_LR, nt_SVM };
        
        /*
         enum RandSeedInit { ZERO, RANDOM };
         
         RF_mode filterMode;
         RF_SampleMethod sampleMethod;
         TestDataMethod testMethod;
         unsigned int B;
         float rf_thresh;
         float ci_alpha;
         float accu_thresh;
         bool useAbsoluteValue;
         //ClassificationTreeNode* filter_object;
         
         Inc_Criteria rf_inc_crit;
         Node_Type filter_t;
         RandSeedInit seed_init;
         */
		
        std::stringstream node_options;
        Node_Type discriminant_t_;
        int mRuns_;                     // the number of iterations of Ada Boost
        
	};
    
    
    
    
}




#endif
