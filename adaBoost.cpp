//
//  adaBoost.cpp
//  su_mvdisc
//
//  Created by Natasha Parikh on 6/25/12.
//  Copyright (c) 2012 Stanford University - Etkin Lab. All rights reserved.
//

#include <iostream>
#include "adaBoost.h"
#include <vector>
#include <cmath>
#include <misc_utils.h>
#include <clapack_utils.h>

//base nodes
#include <data_source.h>
#include <BaseFilter.h>

//compatible nodes
#include <lda.h>
#include <qda.h>
#include <log_regression.h>
#include <svm_disc.h>

using namespace std;
using namespace su_mvdisc_name;
using namespace misc_utils_name;

namespace su_mvdisc_name {
    
    Ada_Boost::Ada_Boost() 
    : discriminant_t(nt_Unspecified), univariateClassifier_(true), mRuns_(100)
    {
        nodeType = ADA_BOOST;
        NodeName = "Ada_Boost";
    }
    
    
    Ada_Boost::~Ada_Boost() 
    {
        for (unsigned int i = 0; i < discObjs_.size(); ++i)
            delete discObjs_[i];
    }
    
    
    void Ada_Boost::setOptions(stringstream & ss_options) 
    {
        map<string,string> options = parse_sstream(ss_options);
        for ( map<string,string>::iterator it = options.begin(); it != options.end();it++) {
            
            if (it->first == "nodeType") {
                
                if (it->second == "LDA")
                    discriminant_t = nt_LDA;
                else if (it->second == "SVM")
                    discriminant_t = nt_SVM;
                else if (it->second == "QDA")
                    discriminant_t = nt_QDA;
                else if (it->second == "LogRegression")
                    discriminant_t = nt_LR;
                else
                    throw ClassificationTreeNodeException(("Ada Boost: Tried to load invalid node type: " + it->second + ".").c_str());
                
            } else if (it->first == "nodeOptions") {
				node_options << it->second;
			} else if (it->first == "mRuns") {
                stringstream ss; 
                ss << it->second;
                ss >> mRuns_;           
            } else if (it->first == "univariate") {
                
                if ((it->second == "true") || (it->second == "1"))
					univariateClassifier_ = true;
				else if ((it->second == "false") || (it->second == "0"))
					univariateClassifier_ = false;
                
            } else {
				if (!setTreeNodeOptions(it->first, it->second))
					throw ClassificationTreeNodeException(("Ada Boost: Option, " + it->first + ", does not exist.").c_str());
			}
        }
    }
    
    
    void Ada_Boost::setMIterations(int M) 
    {
        mRuns_ = M;
    }
    
    
    void Ada_Boost::runNode( bool with_estimation )
    {
        if (Verbose)
            cout<<"Running Ada Boost"<<endl;
        
        // run all the previous nodes on the tree
        for (vector< ClassificationTreeNode* >::iterator i = node_inputs.begin() ; i != node_inputs.end(); i++)
            (*i)->runNode();
        
        // set up the training and target data for this node
        setTarget();
        setInputData();   
        setTestData();
        
        // not sure if this should still be an option, since classify training is combined with estimate parameters in this case
        if (with_estimation)
            estimateParameters();
        
        //cerr << "finished ada boost estimate paramaters" << endl;
        
        classifyTestData();
        
        for (vector< ClassificationTreeNode* >::iterator i = node_inputs.begin(); i != node_inputs.end(); i++)
            (*i)->deleteOutData();
        
        if (Verbose)
            cout << "**************************** end Ada Boost ****************************" << endl;
        
        //exit (EXIT_FAILURE);

    }
    
    
    void Ada_Boost::estimateParameters()
    {            
        if (discriminant_t == nt_Unspecified)
			throw ClassificationTreeNodeException("Tried to run Ada Boost without setting discriminant type.");
        
        base_mvdisc* discriminant_object;
        
        // copy training data to device, in column major format
        // we need to store the original training data, so we reuse it at each iteration
        smatrix data(NumberOfSamples_, Dimensionality_);
        copyToSingleVectorTranspose(in_data, data.sdata, NumberOfSamples_);
        
        //cerr << "**** got the training data ready" << endl;
        //smatrix_print(data, "initial data");
        
        // set data source 
        DataSource* d_source  = new DataSource();
        d_source->setTarget(training_target);
        //cerr << "**** set target in data source" << endl;

        // get real training classifications to compare to predictions
        vector<int> targ_true = vector<int>(training_target->class_index_begin(), training_target->class_index_end());
        
        //cerr << "intestdata in est pars 1: " << (*in_test_data[0])[0] << endl;
        
        // set up to run mRuns different weak classifiers
        smatrix weights(NumberOfSamples_, 1), weightedData, wrongInClass(NumberOfSamples_, 1), inGroupClass, tempFix(NumberOfSamples_, 1), finalG(NumberOfSamples_, 1); 
        smatrix_set(weights, 1.0f / NumberOfSamples_);       // weights are initialized to 1/N
        smatrix_set(tempFix, -1);                           // this is used to change y = {0, 1} to {-1, 1}
        smatrix_set(finalG, 0);                             // at each iteration, add the weighted classifications here to find the final predictions, G
        
        for (int i = 0; i < mRuns_; ++i) {
            //smatrix_print(weights, "weights");
            float weightSum = 0;                            // sum of all the weights
            float corrAndWeightSum = 0;                     // sum of the weights for the samples that were incorrectly classified
            smatrix_DiagDxA(weights, data, weightedData);   
            //smatrix_print(weightedData, "weighted data");
                      
            // create the appropriate weak classifier
            switch (discriminant_t) {
                case nt_LDA:
                    discriminant_object = new LDA_mvdisc();
                    break;
                case nt_QDA:
                    discriminant_object = new QDA_mvdisc();
                    break;
                case nt_LR:
                    discriminant_object = new log_regression_mvdisc();
                    break;
                case nt_SVM:
                    discriminant_object = new SVM_mvdisc();
                    break;
                default:
                    cout << "No node type specified for AdaBoost" << endl;
                    break;
            }
            
            discriminant_object->setOptions(node_options);
            discriminant_object->addInput(d_source);
            
            
            /***** was creating this to subsample large data for documentation cleanness *******
            //vector< vector<int> > selection_mask;
            // initialize mask to all zero, then select included
            // mask size differs from number for sample because of subsampling?
            vector<int>* one_mask = new vector<int>(NumberOfSamples_, 0);
                
            // create one selection mask for each left out group
            // include every group that is not which is being left out
            // this get trickier because of mask changing of indcies are mapped
            for ( int k = 0; k < 10; ++k) {
            
                int index = rand() % NumberOfSources;
                
                // set the appropriate subject to one
                // then input mask can trump any samples
                one_mask->at(index) = 1;
            }
                    0
                
            d_source->applySelectionMask(*one_mask, sel_test); // this adds the left out subjects to the test data
            
            
            delete one_mask;
             */
                
            if (univariateClassifier_) {                        // use each feature individually as classifier 

                if (Verbose)
                    cerr << "univariate classifier is true!" << endl;
                
                smatrix column(NumberOfSamples_, 1);
                float bestAccuracy = 0.0f;
                unsigned int bestDimension = 0;                          // set to a default value to make troubleshooting easier
                for (unsigned int d = 0; d < Dimensionality_; ++d) {
                
                    // set data to one feature at a time
                    smatrix_setCol(column, 0, weightedData, d);
                
                    // data source's in_data needs to be erased so it can be properly added in at each iteration
                    d_source->deleteInput();       
                    d_source->addInputData(column);
                    d_source->copyInData_to_OutData();
                
                    // run the node without classifying test data (as we don't know what it is yet!)
                    discriminant_object->setClassifyTestData(false);
                    discriminant_object->runNode();
                
                    vector<float>* discOutData = discriminant_object->getDataPointer();
                    float accuracy = discriminant_object->getAccuracy(*discOutData);
                
                    if (accuracy > bestAccuracy) {
                        bestAccuracy = accuracy;
                        bestDimension = d;
                    }
                }
            
                //cout << "best dimension: " << bestDimension << endl;
                bestDimensionVec_.push_back(bestDimension);
            
                // reapply the best classifier so that out_data is updated
                smatrix_setCol(column, 0, weightedData, bestDimension);
            
                // data source's in_data needs to be erased so it can be properly added in at each iteration
                d_source->deleteInput();       
                d_source->addInputData(column);
                d_source->copyInData_to_OutData();
                discriminant_object->setClassifyTestData(false);
                discriminant_object->runNode();
                
            } else {                                        // use all dimensions in classifier
                
                if (Verbose)
                    cerr << "univariate classifier is false!" << endl;
                
                // data source's in_data needs to be erased so it can be properly added in at each iteration
                d_source->deleteInput();       
                d_source->addInputData(weightedData);
                d_source->copyInData_to_OutData();
                         
             
                // apply discriminate without classify training
                discriminant_object->setClassifyTestData(false);
                discriminant_object->runNode();
            }
            
            //cerr << "**** just ran the disc obj node" << endl;
            
            // get predicted classifications
            vector<float>* discOutData = discriminant_object->getDataPointer();

            // get the numerator and denominator of the error
            for (unsigned int j = 0; j < NumberOfSamples_; ++j) {
                weightSum += weights.sdata[j];

                if (discOutData->at(j) != targ_true[j])
                    corrAndWeightSum += weights.sdata[j];
            }
            //cout << "corrAndWeightSum: " << corrAndWeightSum << endl;
            //cout << "weightSum: " << weightSum << endl;

            // find the error for this sample, and calculate its associated alpha value
            float err = corrAndWeightSum / weightSum;
            float alpha = log((1 - err) / err);
            
            if (Verbose) {
                cout << "***** err (corr&weight/weight): " << err << endl;
                cout << "***** alpha log((1-err)/err) "  << alpha << endl;
            }
            
            alphas_.push_back(alpha);

            for (unsigned int j = 0; j < NumberOfSamples_; ++j) {
                wrongInClass.sdata[j] = discOutData->at(j);     // copy out_data to an smatrix
                
                if (discOutData->at(j) != targ_true[j])         // update the weights based upon the alpha
                    weights.sdata[j] = weights.sdata[j] * exp(alpha);
            }

            // save the discrimant for future use
            discriminant_object->clearInTestData();
            discObjs_.push_back(discriminant_object); 

            // get the final training classifications by summing each iteration's classifications (as {-1, 1} instead of {0, 1}) * alpha
            smatrix_scaleAndAddMatrices(wrongInClass, 2, tempFix, inGroupClass); 
            smatrix_scale(inGroupClass, alpha);
            smatrix_scaleAndAddMatrices(inGroupClass, 1, finalG);
        }
         
        //cerr << "intestdata in est pars 4: " << (*in_test_data[0])[0] << endl;
        
        // delete ada boost's out_data before setting training predictions
        if (out_data != NULL) 
            delete out_data;
        out_data = new vector<float>();
        
        float train_err = 0;
        vector<int>::iterator class_iter = training_target->class_index_begin();
        for (int i = 0; i < finalG.MRows; ++i, ++class_iter) {
            finalG.sdata[i] = (finalG.sdata[i] > 0) ? 1 : 0;        // set predictions back to {0, 1}
            out_data->push_back(finalG.sdata[i]);                   // save them into out_data
            train_err += (finalG.sdata[i] == *class_iter) ? 1 : 0;  // calculate the training accuracy by counting correct classifications
        }
        
        // print true classifications
        cout << "class " << endl;
        for (vector<int>::iterator class_iter = training_target->class_index_begin(); class_iter != training_target->class_index_end(); class_iter++)
            cout << *class_iter << " ";    
        cout << endl;
        
        train_err /= NumberOfSamples_;
        cout << "training accuracy " << train_err << endl;
        
        // print predicted classifications
        cout << "classify training" << endl;
        for (unsigned int i = 0; i < NumberOfSamples_; ++i)
            cout << finalG.sdata[i] << " ";
        cout << endl;
                
        delete d_source;
        
        if (Verbose)
            cerr << "ending est params in adaboost" << endl;
    }
    
    
    int Ada_Boost::classifyTestData()
    {        
        if (Verbose)
            cerr << "adaBoost: classifyTestData: just starting" << endl;
        
        if (in_test_data.empty())
            throw ClassificationTreeNodeException("Trying to classify using Ada Boost, but no test data to classify exists.");
        
        //cerr << "first elem in in_test_data0: " << (*in_test_data[0])[0] << endl;
        //print(*in_test_data[0], "test data");
    
        
        /********************* univariate classifier method ***********************/
        smatrix testData(NumberOfTestSamples_, Dimensionality_);
        //cerr << "first elem in in_test_data1: " << (*in_test_data[0])[0] << endl;

        copyToSingleVectorTranspose(in_test_data, testData.sdata, NumberOfTestSamples_);
        
        //cerr << "first elem in in_test_data2: " << (*in_test_data[0])[0] << endl;
        //cerr << testData.sdata[0] << endl;

        
        smatrix column(NumberOfTestSamples_, 1), wrongOutClass(NumberOfTestSamples_, 1), outGroupClass, tempFix(NumberOfTestSamples_, 1), finalG(NumberOfTestSamples_, 1); 
        smatrix_set(tempFix, -1);
        smatrix_set(finalG, 0);
        int count = 0;
        for (vector<base_mvdisc*>::iterator i_disc = discObjs_.begin(); i_disc != discObjs_.end(); ++i_disc, ++count) {
            
            // set the disc object's test data and classify it
            (*i_disc)->clearInTestData();
            
            if (univariateClassifier_) {                    // univariate classifier method
                smatrix_setCol(column, 0, testData, bestDimensionVec_[count]);
                (*i_disc)->copyInTestData(column);
            } else {                                        // entire data as classifier 
                (*i_disc)->copyInTestData(in_test_data);
            }
            
            //cerr << "just copied in test data " << in_test_data.size() << endl;
            //print(*in_test_data[0], "test data");
             
            (*i_disc)->classifyTestData();
            
            // get test classifications
            vector<float>* discOutTestData = (*i_disc)->getTestDataPointer();
            
            // copy out_data to an smatrix
            for (unsigned int j = 0; j < discOutTestData->size(); ++j)
                wrongOutClass.sdata[j] = discOutTestData->at(j);
            
            // change the predictions to {-1, 1}, scale by alpha, and add to create final classifications
            smatrix_scaleAndAddMatrices(wrongOutClass, 2, tempFix, outGroupClass);
            smatrix_scale(outGroupClass, alphas_[count]);
            smatrix_scaleAndAddMatrices(outGroupClass, 1, finalG);
        
         }
        
        // delete ada boost's out_test_data before setting testing predictions
        if (out_test_data != NULL) 
            delete out_test_data;
        out_test_data = new vector<float>();
        
        // print predicted classifications
        for (unsigned int i = 0; i < NumberOfTestSamples_;  ++i)
            out_test_data->push_back( (finalG.sdata[i] > 0) ? 1 : 0 ); 
        print(*out_test_data, "classify test");
        
        if (Verbose)
            cout << "adaBoost : classify test end" << endl;
        
        //exit (EXIT_FAILURE);
        return 0;
    }

    

    
}   

    
