//
//  gauss_proc.h
//  su_mvdisc
//
//  Created by Brian Patenaude on 7/6/11.
//  Copyright 2011 Stanford University - Etkin Lab. All rights reserved.
//

#ifndef gauss_proc_h
#define gauss_proc_h

#include <vector>
#include <string>
#include "base_image_filter.h"
//#include "svm.h"
#define PI 3.14159265
namespace su_mvdisc_name {
    int lineSearch_GPLogLikelihood(void (*Kfunc)(const float *, float*,const std::vector<float> &,const unsigned int & , const unsigned int & ), const float* data,const float* y, std::vector<float> & hyperparams, const float* deriv,const unsigned int & NumberOfSamples, const unsigned int & Ndim,float step_size=0.001);

    float GPregression_logLikelihood(void (*Kfunc)(const float *, float*,const std::vector<float> &,const unsigned int & , const unsigned int & ), const float* data,const float* y, std::vector<float> & hyperparams, const unsigned int & NumberOfSamples, const unsigned int & Ndim);
    
    void dLikelihood_dHyp(void (*Kfunc)(const float *, float*,const std::vector<float> &,const unsigned int & , const unsigned int & ),const float * data , const float* y, std::vector<float> & hyperparams,std::vector<float*> dK, const unsigned int & NumberOfSamples, const unsigned int & Ndim,float* grad);

    void GPregression_conjugateGradientSearch( void (*Kfunc)(const float *, float*,const std::vector<float> &,const unsigned int & , const unsigned int & ),const float * data ,  const float* y, std::vector<float> & hyperparams, const unsigned int & NumberOfSamples, const unsigned int & Ndim  );

    
    void GPregression_gradientSearch( void (*Kfunc)(const float *, float*,const std::vector<float> &,const unsigned int & , const unsigned int & ),const float * data ,  const float* y, std::vector<float> & hyperparams, const unsigned int & NumberOfSamples, const unsigned int & Ndim  );

    
    //using vector for hyperparameters so that I can write a generic gradient search with a function as an input 
    
    void squaredExponentialCovariance_dK( const float * Data, const float* y,  std::vector<float*> dK , const std::vector<float> & hyperparams, const unsigned int & Nrows, const unsigned int & Ncols );

    //kernel functions
    void squaredExponentialCovariance_K( const float * Data, float* K , const std::vector<float> & hyperparams, const unsigned int & Nrows, const unsigned int & Ncols );

    void squaredExponentialCovariance_Kstar( const float * Data,const float * DataStar, float* Kstar, const std::vector<float> & hyperparams, const unsigned int & Nrows, const unsigned int & Ncols , const unsigned int & Ntest);
   // void squaredExponentialCovariance( const float * Data, const float * DataStar, float* K , float* Kstar ,const float & lengScale,const float & var_f, const float & var_n,  const unsigned int & Nrows, const unsigned int & Ncols, const unsigned int & Ntest );
    
    //#pragma GCC visibility push(default)
    
    class GaussianProcessFilter : public base_image_filter
    {
    public:
        GaussianProcessFilter();
        ~GaussianProcessFilter();
      //  enum kernelType { LINEAR , POLYNOMIAL, RBF, SIGMOID };
        void applyToTestData();
        void estimateParameters();
        void applyParametersToTrainingData();
        
        
        void setOptions( std::stringstream & ss_options);
             
    protected:
        // void setIOoutputSpecific();
        //void runTrackStats();
        //covariance functions 
 
        
    private:
        enum GaussProc_mode { LSClassifier, Regression }; 
        enum CovarianceFunctions { Exponential };
        // std::vector<svm_node> vectorToSVMnodes( const std::vector<float> & vec ) const ;
      //  std::vector<svm_node> vectorToSVMnodes( const std::vector<float> * vec ) const ;
        float* K;
        
        float* alpha;
        std::vector<float> cov_hyperpars;
        
        GaussProc_mode GP_mode; //
        CovarianceFunctions cov_func; //covariance function to be used
       // svm_problem libsvm_data;
      //  svm_parameter libsvm_parameter;
     //   svm_model* libsvm_model;
        
        //used for entering data into libsvm format
		
        //std::vector< std::vector<svm_node>* > svm_training_data_tracker;
     //   void delete_svmTrainingData();
    //    std::vector<svm_node*> svm_training_data;
        
  //      bool UseAsFilter;
    //    std::string name;	
    //    std::vector< std::vector<float> > means; //
     //   std::vector< float > Sigma; //common covariance matrix 
        
        
    };
}
//#pragma GCC visibility pop
#endif
