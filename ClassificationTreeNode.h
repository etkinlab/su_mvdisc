#ifndef ClassificationTreeNode_H
#define ClassificationTreeNode_H

/*
 *  ClassificationTreeNode.h
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 7/20/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#include <vector>
#include <list>

#include <smatrix.h>
#include <target.h>
#include <ClassificationTreeNodeException.h>
#include <vector_structs.h>
#include <iostream>
namespace su_mvdisc_name{
	

		
	

	enum TreeNodeType { UNDEFINED, SOURCE, FILTER, DISCRIMINANT, NESTED_TREE, WRITER, ADA_BOOST,  \
			    LDA, QDA, SVM,DECISION_TREE };
	enum CTree_OutputFormat { NOT_SPECIFIED, IMAGE };

	class ClassificationTreeNode
	{
		
		
		
	public:
		ClassificationTreeNode();
		virtual ~ClassificationTreeNode();
		
		bool hasTrainingData();

        void setLevel( const unsigned int & level_in );
        unsigned int getLevel();

        void setVerbose(const int & on ){ Verbose = on ; }
        
		virtual void recursivelyEstimateParameters();
        template <class T>
        std::vector<T> getTargetData( );

		virtual void setTarget();
        
		virtual void estimateParameters();
		//these return iterators to the parameters of what
		virtual void setParameters( const std::vector<float> & params );
		virtual void setParameters( const std::vector<float> & params, const std::vector<unsigned int> & mask );

		//bool requiresSelectionMask() { return RequireSelectionMask; }
		virtual std::vector<float> getParameters();
		//this adds parameters to 
		virtual void getParameters( std::vector< std::list<float> > & all_params );
        virtual void saveParameters();
        virtual std::string getParameterFileName();
		//virtual void getParameters( std::vector< std::list<float> > & all_params , const std::vector< unsigned int > & selection_mask );

		virtual unsigned int getNumberOfParameters();

		virtual void setInputData();
		virtual void setTestData();
        
        void copyInTestData( const std::vector< std::vector<float>* > & test_data );
        void copyInTestData( const clapack_utils_name::smatrix & test_data );
		void setOuputDataNames( const std::vector<std::string> & names );

        //these will clear test_chunks
		virtual void setTestData( std::vector< std::vector<float>* > & test_data );
        virtual void setTestData( std::vector< std::vector<float>* > & test_data, const unsigned int & Dim );
		virtual void setTestData( std::vector< std::vector<float>* > & test_data , std::vector< std::vector<unsigned int>* > & test_chunks );
        virtual void setTestData( std::vector< std::vector<float>* > & test_data, const unsigned int & Dim, std::vector< std::vector<unsigned int>* > & test_chunks );
        
		virtual void setIOoutput();
		virtual void setIOoutputSpecific();
        virtual std::vector<float> getDistanceScores();

		
        bool isTestSource();
		void clearOutData();
        void clearInTestData();

        
        //these work on all but data sources
        void deleteOutData();
        //void deleteInData();

		
		virtual void setOptions( std::stringstream & ss_options);
		bool setTreeNodeOptions( const std::string & map_key, const std::string & map_value );

	//	virtual void setInputAndLeftOutData();
	//	virtual void runTopNode();
		virtual void runNode( bool with_estimation = true);

        
        void setDoKFold( const bool & state ) { doKfold_ = state; }
        bool getDoKFold() { return doKfold_; }
        
		virtual void applyToTestData();
	//	virtual void write();
		virtual void runTrackStats();

		virtual void writeStats() const;
	//	virtual void writeStats( nifti_Writer* writer ); 
		void writeNodeOutputs();
		void setPassDownOutput( const bool & state ) { PassDownOutput = state; std::cout<<"pass down output"<<std::endl; } 
		void enablePassDownOutput() { PassDownOutput = true; } 
		void disablePassDownOutput() { PassDownOutput = false; } 
	//	virtual void setOutputFeatureMask();
		
		CTree_OutputFormat getOutputFormat() { return out_format; };
		std::vector<bool>::iterator outFM_begin() { return output_feature_mask_->begin(); }
		std::vector<bool>::iterator outFM_end() { return output_feature_mask_->end(); }
        
		std::vector<uint3>::iterator outSRC_imdims_begin() { return src_image_dims_->begin(); }
        std::vector<uint3>::iterator outSRC_imdims_end() { return src_image_dims_->end(); }
        std::vector<float4>::iterator outSRCres_begin() { return src_image_res_->begin(); }
        std::vector<float4>::iterator outSRCres_end() { return src_image_res_->end(); }
        std::vector<std::string>::iterator outSRCnames_begin() { return src_names_->begin(); }
        std::vector<std::string>::iterator outSRCnames_end() { return src_names_->end(); }
        std::vector<unsigned int>::iterator outSRCdims_begin() { return src_dims_->begin(); }
        std::vector<unsigned int>::iterator outSRCdims_end() { return src_dims_->end(); }
		//	void setTarget( Target* target_in );

		//ClassificationTreeNode* getLeftOut_OutData();
	//	void setLeftOut_InData(ClassificationTreeNode* input_node);

		//allocate memory associated with the object, should only be called after need parameters have been set (e.g. NumberOfSamples, Dimensionality, NumberOfSources, Kclasses);
		virtual void AllocateMemory();
		
		//virtual std::vector<unsigned int> applySelectionMask( const std::vector<unsigned int> & selection_mask);
        void incOutputConnections();
		void addInput( ClassificationTreeNode* input);
		void deleteInput( ClassificationTreeNode* input);

		void attachWriter( ClassificationTreeNode* writer ); 
		//void setLeftOutData();
		void setName(const std::string & name );
		
        std::vector<std::string> getOutputNames();
		template<class T>
		std::vector<T> getOutputData();
		template<class T>
		std::vector<T> getOutputTestData();
        template<class T>
        std::vector<T> getClusterData();

        
		std::vector<float>*  getDataPointer() ;
        std::vector<std::string>*  getNamesPointer() ;

		std::vector<float>*  getTestDataPointer() ;
        std::vector<unsigned int>*  getChunksPointer();
        std::vector<unsigned int>*  getTestChunksPointer();
        //{ return _out_test_chunks; }

        std::vector<unsigned int>::iterator getChunksBegin();
        std::vector<unsigned int>::iterator getChunksEnd();
        
        //clusters access
        std::vector<unsigned int>*  getClustersPointer();
        std::vector<unsigned int>::iterator getClustersBegin();
        std::vector<unsigned int>::iterator getClustersEnd();


        
		Target* getTargetPointer();
		
		unsigned int getNumberOfClasses() const; 
		unsigned int getDimensionality() const;
		virtual unsigned int getNumberOfSamples();
        virtual unsigned int getNumberOfInputSamples();
		virtual unsigned int getNumberOfTestSamples();
        unsigned int getNumberOfChunks();
        unsigned int getNumberOfTestChunks();

		
		std::string nodeName();
		std::string nodeTypeName();
		
		TreeNodeType getTreeNodeType();
        TreeNodeType getTreeNodeTypeSpec();

        float getAccuracy( const std::vector<float> & estimates );
        
  

	protected:
        
        unsigned int calculateNumberOfInChunks();
        
        void deleteInputsOutputData();        
		TreeNodeType nodeType;
        TreeNodeType nodeTypeSpec;

		unsigned int _output_connections;
        bool DELETED_INPUT_DATA;//keep track whether a call has been made to delete the output form the inputs
		std::vector<  ClassificationTreeNode* > node_inputs, v_writers;
		std::vector< std::vector<float>* > in_data;
        std::vector< std::vector<unsigned int >* > in_chunks_;
        std::vector< std::vector<std::string>* > in_data_names_;
        std::vector< std::vector< unsigned int >* >  in_clusters_;

		//std::vector<float>* in_data_all;
        bool _doChunks;
        
		std::vector<float> *out_data;
        std::vector<unsigned int> *out_chunks_;
		std::vector<std::string> *out_data_names_;
		std::vector<unsigned int> *out_clusters_;

		//std::vector<float> *in_leftout_data, *out_leftout_data;
		std::vector< std::vector<float>* > in_test_data;
		std::vector< float > *out_test_data;//, test_data;
        std::vector< std::vector<unsigned int>* > in_test_chunks_;
        std::vector<unsigned int> *out_test_chunks_;

        std::vector< std::vector<unsigned int>* > in_test_clusters_;
        std::vector<unsigned int> *out_test_clusters_;

        
        
        
		std::vector<unsigned int> v_dims_;
		Target * training_target;//, *out_target;

        //adding capability for 3rd test sample that it is not validating against
        std::vector< std::vector<float>* > in_ext_test_data;
        
        std::vector<float> *out_ext_test_data;

        std::vector<float>* disc_distance;
      
		std::string NodeName;
		std::string output_basename;//used to write outputs
		unsigned int NumberOfSamples_,NumberOfChunks_, NumberOfTestSamples_,NumberOfTestChunks_, Dimensionality_, NumberOfSources_, Kclasses_;
		//adding capability for 3rd test sample that it is not validating against
        unsigned int NumberOfExternalTestSamples;
		//-----primarily for treee visualizaton-----------//
        unsigned int level;

		//--------for IO output---------// 
		int Verbose;
		bool saveParams_;
        bool trackStats;
		bool SwapFeaturesAndDimensions;
		unsigned int DimTrim;
        std::vector<unsigned int> dim_trims_;
		//	bool RequireSelectionMask;
		bool PassDownOutput;
        bool doKfold_;
        
        std::string features_names_;
        
        //also used for mask filter
		std::vector<bool>* output_feature_mask_;
		std::vector<float>* output_parameters;
		CTree_OutputFormat out_format;
		
		enum SOURCE_TYPE { TRAINING, TEST};
		SOURCE_TYPE src_type;
		bool DoGenAcc_;
        
		unsigned int N_stats;
        std::vector<unsigned int>* stats_sum_N;
		std::vector<float>* stats_sum_x;
		std::vector<float>* stats_sum_xx;
        

        std::vector<std::string>* src_names_;
        std::vector<su_mvdisc_name::float4>* src_image_res_;
		std::vector<su_mvdisc_name::uint3>* src_image_dims_;
        std::vector<unsigned int>* src_dims_;

		
		//----------------------------//
	private:

        

		
	};
	
}

#endif
