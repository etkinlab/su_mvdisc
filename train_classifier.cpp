/*
 *  run_loo.cpp
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 7/19/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

//
//#include "lda.h"

//#include "svm_disc.h"
//#include "NormalizeFilter.h"
//#include "nifti_writer.h"

//FSL headers                                                                                                                             
 #include "utils/options.h"

#include <fstream>
#include <iostream>
#include <vector>
#include <list>
#include <sys/stat.h> 
#include <sys/time.h> 
#include <cmath>


#include <miscmaths/t2z.h>

//brian headers
//#include <misc_utils.h>

//#include "image_source.h"
#include "ClassificationTree.h"
#include "tree_sampler.h"
#include <misc_utils.h>
#include "su_mvdisc_structs.h"
#include "lda.h"
#include "glm_filter.h"
#include "ClassificationTreeNode.h"
#include "TextFileSource.h"
//FSL headers
// #include "utils/options.h"

//3rd party tools
//#include "cublas.h"

using namespace std;
//FSL namespaces

using namespace Utilities;
//our namespaces
using namespace misc_utils_name;
using namespace su_mvdisc_name;



string title="discriminate (Version 0.1) Stanford University (Brian Patenaude)";
string examples="discriminate -i data.tree -o output ";


Option<int> verbose(string("-v,--verbose"), 0,
					 string("switch on diagnostic messages"), 
					 false, requires_argument);
Option<bool> help(string("-h,--help"), false,
				  string("display this message"),
				  false, no_argument);
//Standard Inputs
Option<string> inname(string("-i,--in"), string(""),
					  string("Filename of input discriminant tree."),
					  true, requires_argument);
Option<string> inname_test(string("-j,--intest"), string(""),
                      string("Filename of input test data."),
                      true, requires_argument);
Option<string> target(string("-t,--target"), string(""),
                           string("Filename of target."),
                           true, requires_argument);
Option<int> node(string("-n,--nodetype"), 0,
                      string("Node Type to Run : \n \
                             0: LDA \n \
                             1: GLMfilter"),
                      true, requires_argument);
Option<string> outname(string("-o,--outputName"), string(""),
					   string("Output name"),
					   true, requires_argument);

int nonoptarg;








//taken from http://www.techbytes.ca/techbyte103.html
bool FileExists( const string & strFilename) { 
	struct stat stFileInfo; 
	bool blnReturn; 
	int intStat; 
	
	// Attempt to get the file attributes 
	intStat = stat(strFilename.c_str(),&stFileInfo); 
	if(intStat == 0) { 
		// We were able to get the file attributes 
		// so the file obviously exists. 
		blnReturn = true; 
	} else { 
		// We were not able to get the file attributes. 
		// This may mean that we don't have permission to 
		// access the folder which contains this file. If you 
		// need to do that level of checking, lookup the 
		// return values of stat which will give you 
		// more details on why stat failed. 
		blnReturn = false; 
	} 
	
	return(blnReturn); 
}



int do_work( const string & filename, const string & testdata, const string & target, const string & outname, const int & node_type, const int & verb )
{
    //train LDA classifier
    //read
    if (! FileExists(filename) )
    {
        cerr<<"Data file does not exist: "<<filename<<endl;
    }
    
    vector< float > all_test_data ;
    int Ns=0;
    ifstream fin(testdata.c_str());
    if (fin.is_open())
    {
        
        string line;
        while (getline(fin,line))
        {
            cout<<"linetest "<<line<<endl;
            stringstream ss;
            ss<<line;
            float ftemp;
            while ( ss >> ftemp )
            {
                all_test_data.push_back(ftemp);

            }
            ++Ns;
        }
        fin.close();
    }
    
    cout<<"setup target "<<endl;
    Target * targ = new Target();
    targ->setFileName(target);
    targ->readFile();
    targ->calculateClassInfo();
    
    cout<<"setup textsrc "<<endl;

    TextFileSource* text_src = new TextFileSource();
    text_src->setFileName(filename);
    text_src->setTestData( all_test_data ,Ns);
    text_src->setTarget(targ);
    text_src->setHeader(0);
    text_src->setVerbose(true);
    text_src->runNode();
    text_src->copyInData_to_OutData();

    LDA_mvdisc* lda_cl;
    GLM_Filter* glm_filt;
    switch ( node_type ){
            
        case 0 :
            lda_cl = new LDA_mvdisc();
            lda_cl->addInput(text_src);
            lda_cl->setTarget();
            lda_cl->setVerbose(2);
            lda_cl->runNode();
            delete lda_cl;
            break;
        case 1 :
            glm_filt = new GLM_Filter();
            glm_filt->addInput(text_src);
            glm_filt->setTarget();
            glm_filt->setVerbose(true);
            glm_filt->runNode();
            delete glm_filt;
            break;
        default :
            break;
    }
 
//    text_src->copyInData_to_TestData();
    cout<<"setup lda "<<endl;

//
//    
//    LDA_mvdisc* lda_cl = new LDA_mvdisc();
//    lda_cl->addInput(text_src);
//    lda_cl->runNode();
//    cout<<"done run node"<<endl;
//    for ( vector< vector<float>* >::iterator i = all_test_data.begin(); i != all_test_data.end(); ++i)
//        delete *i;
    
//    delete text_src;
//    delete targ;
//    delete lda_cl;
//    
	return 0;
}


int main( int argc, char * argv[])
{
	
	
	//Tracer tr("main");
	OptionParser options(title, examples);
	
	try {
		// must include all wanted options here (the order determines how
		//  the help message is printed)
		options.add(verbose);
		options.add(help);
		options.add(inname);
        options.add(inname_test);
        options.add(target);
        
		options.add(outname);
        options.add(node);
        
        nonoptarg = options.parse_command_line(argc, argv);
		
		// line below stops the program if the help was requested or 
		//  a compulsory option was not set
		if (  (!options.check_compulsory_arguments(true) ))
		{
			options.usage();
			exit(EXIT_FAILURE);
		}
      
 
		do_work(inname.value(), inname_test.value(), target.value(), outname.value(), node.value(), verbose.value());
		
		
	}  catch(X_OptionError& e) {
		options.usage();
		cerr << endl << e.what() << endl;
		exit(EXIT_FAILURE);
	} catch(std::exception &e) {
		cerr << e.what() << endl;
        exit(EXIT_FAILURE);

	}
	
	return 0;// do_work(argc,argv);
	
}



