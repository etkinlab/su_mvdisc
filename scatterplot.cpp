#include "scatterplot.h"
#include <iostream>
#include <sstream>

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

using namespace std;
namespace su_mvdisc_gui_name{

scatterPlot::scatterPlot()
{
    TitleFace_size_ = 72;
    TitleFont_ = new FTGLPolygonFont("/Library/Fonts/HelveticaCY.dfont");
    TitleFont_->FaceSize(TitleFace_size_);
    TitleText_.SetAlignment(FTGL::ALIGN_LEFT );
    TitleText_.SetFont(TitleFont_);
    TitleText_.SetLineLength(100000);
    Title_ = "My Plot Title";

    Axis_size_ = 72;
    AxisFont_ = new FTGLPolygonFont("/Library/Fonts/HelveticaCY.dfont");
    AxisFont_->FaceSize(Axis_size_);

    tick_values_.SetAlignment(FTGL::ALIGN_RIGHT );
    tick_values_.SetFont(AxisFont_);
tick_values_.SetLineLength(1000);

//    FTSimpleLayout AxisText_;
//    std::string Title_;

//    glGenTextures(2, textureID);
//    for(int i = 0; i < 2; i++)
//      {
//          glBindTexture(GL_TEXTURE_2D, textureID[i]);
//          glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//          glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//          glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
//          glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
//          glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 4, 4, 0, GL_RGB, GL_FLOAT,
//                       textures[i]);
//      }
//  }
//TitleTextureFont_ = new FTTextureFont("/Library/Fonts/HelveticaCY.dfont");
    tick_spacing_ = 1;


    render_depth_=1;
    vertexLoc_=0;
    normalLoc_=1;
    vertexLoc_=2;
    do_buffer_=true;
    w_left_=-1;
    w_right_=1;
    w_bottom_=-1;
    w_top_=1;
    plot_left_=-0.95;
    plot_right_=0.95;
    plot_bottom_=-0.95;
    plot_top_=0.95;
axes_tx_ = float2(0,0);
axes_scale_ = float2(1.0,1.0);
axes_lims_ = float4(0,1,0,1);
axes_truelims_=float4(-1,1,-1,1);
createAxes();
updateAxesXFM();
    vertical_spacing_=0.1;
    horizontal_spacing_=0.1;
height_=1;
width_=1;
    //pointers
    vbos_lines_=vbos_triangles_=vbos_datapts_=vbos_bars_=NULL;

 do_bars_=false;
 do_lines_ = false;
 do_points_ = true;
}

scatterPlot::~scatterPlot()
{
    delete TitleFont_;
//delete TitleTextureFont_;
    if (vbos_lines_!=NULL)
    {
        glDeleteBuffers(2,vbos_lines_);
        delete[] vbos_lines_;
    }

    if (vbos_triangles_!=NULL)
    {
        glDeleteBuffers(2,vbos_triangles_);
        delete[] vbos_triangles_;
    }
    if (vbos_datapts_!=NULL)
    {
        glDeleteBuffers(2,vbos_datapts_);
        delete[] vbos_datapts_;
    }

    if (vbos_bars_!=NULL)
    {
        glDeleteBuffers(2,vbos_bars_);
        delete[] vbos_bars_;
    }


}

void scatterPlot::initializeGL()
{
    setShaders();
    setModelViewProjMatrix();

    //used for frawing GL_LINES elements
    vbos_lines_ = new GLuint[3];
    glGenBuffers(3,vbos_lines_);

    //used for drawing triangles

    vbos_triangles_ = new GLuint[2];
    glGenBuffers(2,vbos_triangles_);

    vbos_datapts_ = new GLuint[2];
    glGenBuffers(2,vbos_datapts_);

    vbos_bars_ = new GLuint[2];
    glGenBuffers(2,vbos_bars_);


}
//void scatterPlot::createAxes()
//{
//    lines_verts_.clear();
//    linestrip_inds_.clear();
//
//    height_ = plot_top_ - plot_bottom_;
//    width_ = plot_right_ - plot_left_ ;
//
//    axes_lims_=float4(plot_left_ + width_*horizontal_spacing_ , \
//                      plot_right_ - width_*horizontal_spacing_, \
//                      plot_bottom_ + height_ * vertical_spacing_,      \
//                      plot_top_ - height_ * vertical_spacing_  \
//                      );
//            axes_centre_=float2(plot_left_ + width_*horizontal_spacing_, plot_bottom_ + height_ * vertical_spacing_);
//
//    lines_verts_.push_back( vertex(float4(axes_lims_.x,axes_lims_.t,render_depth_,0)));
//    lines_verts_.push_back( vertex(float4( axes_centre_.x,axes_centre_.y,render_depth_,0)));
//    lines_verts_.push_back( vertex(float4(axes_lims_.y, axes_lims_.z ,render_depth_,0)));
//    lines_verts_.push_back( vertex(float4(axes_lims_.x,axes_lims_.t,render_depth_,0)));
//    lines_verts_.push_back( vertex(float4( axes_centre_.x,axes_centre_.y,render_depth_,0)));
//    lines_verts_.push_back( vertex(float4(axes_lims_.y, axes_lims_.z ,render_depth_,0)));
//
//    cout<<"lines "<<lines_verts_[0].x<<" "<<lines_verts_[0].y<<endl;
//    cout<<"lines "<<lines_verts_[1].x<<" "<<lines_verts_[1].y<<endl;
//    cout<<"lines "<<lines_verts_[2].x<<" "<<lines_verts_[2].y<<endl;
//    linestrip_inds_.push_back(0);
//    linestrip_inds_.push_back(1);
//    linestrip_inds_.push_back(2);
//updateAxesXFM();
//
//}
    void scatterPlot::createAxes()
    {
        lines_verts_.clear();
//        linestrip_inds_.clear();
          lines_inds_.clear();

        height_ = plot_top_ - plot_bottom_;
        width_ = plot_right_ - plot_left_ ;
        
        axes_lims_=float4(plot_left_ + width_*horizontal_spacing_ , \
                          plot_right_ - width_*horizontal_spacing_, \
                          plot_bottom_ + height_ * vertical_spacing_,      \
                          plot_top_ - height_ * vertical_spacing_  \
                          );
        axes_centre_=float2(plot_left_ + width_*horizontal_spacing_, plot_bottom_ + height_ * vertical_spacing_);
        
//        lines_verts_.push_back( vertex(float4(axes_lims_.x,axes_lims_.t,render_depth_,0)));
//        lines_verts_.push_back( vertex(float4( axes_centre_.x,axes_centre_.y,render_depth_,0)));
//        lines_verts_.push_back( vertex(float4(axes_lims_.y, axes_lims_.z ,render_depth_,0)));
//        lines_verts_.push_back( vertex(float4(axes_lims_.x,axes_lims_.t,render_depth_,0)));
//        lines_verts_.push_back( vertex(float4( axes_centre_.x,axes_centre_.y,render_depth_,0)));
//        lines_verts_.push_back( vertex(float4(axes_lims_.y, axes_lims_.z ,render_depth_,0)));
//
        
        lines_verts_.push_back( vertex(float4(0.5*(axes_lims_.x+axes_lims_.y),axes_lims_.z,render_depth_,0)));
        lines_verts_.push_back( vertex(float4(0.5*(axes_lims_.x+axes_lims_.y),axes_lims_.t,render_depth_,0)));
        lines_verts_.push_back( vertex(float4(axes_lims_.x,0.5*(axes_lims_.z+axes_lims_.t),render_depth_,0)));
        lines_verts_.push_back( vertex(float4(axes_lims_.y,0.5*(axes_lims_.z+axes_lims_.t),render_depth_,0)));

        lines_verts_.push_back( vertex(float4( axes_centre_.x,axes_centre_.y,render_depth_,0)));
        lines_verts_.push_back( vertex(float4(axes_lims_.y, axes_lims_.z ,render_depth_,0)));
        //
        
        
        cout<<"lines "<<lines_verts_[0].x<<" "<<lines_verts_[0].y<<endl;
        cout<<"lines "<<lines_verts_[1].x<<" "<<lines_verts_[1].y<<endl;
        cout<<"lines "<<lines_verts_[2].x<<" "<<lines_verts_[2].y<<endl;
        lines_inds_.push_back(uint2(0,1));
        lines_inds_.push_back(uint2(2,3));


        updateAxesXFM();
        
    }


void scatterPlot::setWindow( const float & left, const float & right, const float & bottom, const float & top  )
{
    w_left_=left;
    w_right_=right;
    w_bottom_=bottom;
    w_top_=top;
}

void scatterPlot::setPlotBounds( const float & left, const float & right, const float & bottom, const float & top  )
{
    plot_left_=left;
    plot_right_=right;
    plot_bottom_=bottom;
    plot_top_=top;
    createAxes();

    updateAxesXFM();
}

void scatterPlot::updateAxesXFM(  )
{
   // axes_truelims_ = float4(min.x,max.y,min.y,max.x);
    cout<<"update scale "<< (axes_lims_.t - axes_lims_.z)<<" "<<(axes_truelims_.t - axes_truelims_.z)<<endl;
    cout<<"update scale "<< (axes_lims_.y - axes_lims_.x)<<" "<<(axes_truelims_.y - axes_truelims_.x)<<endl;
    cout<<"update scale "<< axes_lims_.y <<" "<< axes_lims_.x<<" "<<axes_truelims_.y <<" "<< axes_truelims_.x<<endl;



    axes_scale_ = float2( (axes_lims_.y - axes_lims_.x)/ (axes_truelims_.y - axes_truelims_.x), \
                          (axes_lims_.t - axes_lims_.z)/ (axes_truelims_.t - axes_truelims_.z));
    cout<<"update scale2 "<<axes_scale_.x<<" "<<axes_scale_.y<<endl;
    axes_tx_ = float2( axes_centre_.x - axes_truelims_.x*axes_scale_.x , axes_centre_.y - axes_truelims_.z*axes_scale_.y);
//axes_tx_.x /= axes_scale_.x;//compensate for the scaling
//        axes_tx_.y /= axes_scale_.y;
         cout<<"update tx "<<axes_tx_.x<<" "<<axes_tx_.y<<endl;

}


void scatterPlot::setAxesLimits( const float4 & limits )
{
    axes_truelims_ = limits;
//    axes_centre_ = float2(limits.x, limits.z);
    updateAxesXFM();
}

void scatterPlot::setAxesLimits()
{
    cout<<"setaxes "<<endl;
    if ( xyt_.empty() ) return;

    float2 min;
    float2 max;


        min.x =  max.x = xyt_[0].x;
        min.y = max.y = xyt_[0].y;



    vector<float3>::iterator i_xyt = xyt_.begin()+1;
    for ( ; i_xyt != xyt_.end(); ++i_xyt)
    {
        if (i_xyt->x < min.x ) min.x = i_xyt->x;
        if (i_xyt->y < min.y ) min.y = i_xyt->y;
        if (i_xyt->x > max.x ) max.x = i_xyt->x;
        if (i_xyt->y > max.y ) max.y = i_xyt->y;

    }
//    axes_truelims_=float4(-1,5,-1,5);
cout<<min.x<<" "<<max.x<<" "<<min.y<<" "<<max.y<<endl;
    axes_truelims_=float4(min.x,max.x,min.y,max.y);
// axes_truelims_=float4(-1,5,-1,5);


    updateAxesXFM();

    cout<<"done setaxes "<<endl;
return;
}

void scatterPlot::createViewports()
{
    triangles_verts_.clear();
    triangle_inds_.clear();
    //lets make 4 viewports
    //create a shader where integer maps to colour, 0=black, 1=white
    //viewport 1
    //    triangles_verts_.push_back( vertex(float4(0,0,render_depth_,1)));
    //    triangles_verts_.push_back( vertex(float4(50,0,render_depth_,1)));
    //    triangles_verts_.push_back( vertex(float4(50,50,render_depth_,1)));
    //    triangles_verts_.push_back( vertex(float4(0,50,render_depth_,1)));
    cout<<"scatter bounds "<<plot_left_<<" "<<plot_right_<<" "<<plot_bottom_<<" "<<plot_top_<<endl;
    triangles_verts_.push_back( vertex(float4(plot_left_,plot_bottom_,render_depth_,1)));
    triangles_verts_.push_back( vertex(float4(plot_right_,plot_bottom_,render_depth_,1)));
    triangles_verts_.push_back( vertex(float4(plot_right_,plot_top_,render_depth_,1)));
    triangles_verts_.push_back( vertex(float4(plot_left_,plot_top_,render_depth_,1)));

    triangle_inds_.push_back(uint3(0,1,2));
    triangle_inds_.push_back(uint3(0,2,3));

}

void scatterPlot::setModelViewProjMatrixXFMmodel(const glm::vec3 & scale_fac )
{
    glm::mat4 Projection = glm::ortho<float>(w_left_,w_right_,w_bottom_,w_top_, 0.0f, 10000.0000f);


    // Camera matrix
    glm::mat4 View       = glm::lookAt(
                glm::vec3(0,0,20), // Camera is at (4,3,3), in World Space
                glm::vec3(0,0,0), // and looks at the origin
                glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
                );
    // Model matrix : an identity matrix (model will be at the origin)
    glm::mat4 Model      = glm::scale(glm::mat4(1.0f),scale_fac);  // Changes for each model !

    // Our ModelViewProjection : multiplication of our 3 matrices
    glm::mat4 MVP        = Projection * View * Model; // Remember, matrix multiplication is the other way around
    GLuint MatrixID = glGetUniformLocation(p_index_cmap_, "MVP");
    glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
}

void scatterPlot::setModelViewProjMatrixXFMmodel(const glm::vec3 & tx , const glm::vec3 & scale_fac )
{
    glm::mat4 Projection = glm::ortho<float>(w_left_,w_right_,w_bottom_,w_top_, 0.0f, 10000.0000f);


    // Camera matrix
    glm::mat4 View       = glm::lookAt(
                glm::vec3(0,0,20), // Camera is at (4,3,3), in World Space
                glm::vec3(0,0,0), // and looks at the origin
                glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
                );


    // Model matrix : an identity matrix (model will be at the origin)
//    glm::mat4 Model      = glm::scale(glm::mat4(1.0f),scale_fac);  // Changes for each model !
    glm::mat4 Model      = glm::mat4(1.0f);  // Changes for each model !

   Model = glm::translate(Model,tx);
   Model = glm::scale(Model,scale_fac);

    // Our ModelViewProjection : multiplication of our 3 matrices
    glm::mat4 MVP        = Projection * View * Model; // Remember, matrix multiplication is the other way around
    GLuint MatrixID = glGetUniformLocation(p_index_cmap_, "MVP");
    glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
}

void scatterPlot::setModelViewProjMatrixXFMview(const glm::vec3 & tx , const glm::vec3 & scale_fac )
{
    glm::mat4 Projection = glm::ortho<float>(w_left_,w_right_,w_bottom_,w_top_, 0.0f, 10000.0000f);


    // Camera matrix
    glm::mat4 View       = glm::lookAt(
                glm::vec3(0,0,20), // Camera is at (4,3,3), in World Space
                glm::vec3(0,0,0), // and looks at the origin
                glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
                );
    View = glm::scale(View,scale_fac);

    View = glm::translate(View,tx);

    // Model matrix : an identity matrix (model will be at the origin)
//    glm::mat4 Model      = glm::scale(glm::mat4(1.0f),scale_fac);  // Changes for each model !
    glm::mat4 Model      = glm::mat4(1.0f);  // Changes for each model !

    // Our ModelViewProjection : multiplication of our 3 matrices
    glm::mat4 MVP        = Projection * View * Model; // Remember, matrix multiplication is the other way around
    GLuint MatrixID = glGetUniformLocation(p_index_cmap_, "MVP");
    glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
}





void scatterPlot::setModelViewProjMatrix()
{
    cout<<"scatterPlot::setModelViewProjMatrix()"<<endl;
    //    glUseProgram(p_index_cmap_);
    // Projection matrix : 45° Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
    //    glm::mat4 Projection = glm::perspective(45.0f, 1.015f, 0.1f, 10000.0f);
    cout<<"scatter window "<<w_left_<<" "<<w_right_<<" "<<w_bottom_<<" "<<w_top_<<endl;

    //    glm::mat4 Projection = glm::ortho(-1.0f,1.0f,-1.0f,1.0f, 0.0f, 10000.0000f);
    glm::mat4 Projection = glm::ortho<float>(w_left_,w_right_,w_bottom_,w_top_, 0.0f, 10000.0000f);

    //     glm::mat4 Projection = glm::ortho(-100.0f,100.0f,-100.0f,100.0f, 0.0f, 10000.0000f);

    // Camera matrix
    glm::mat4 View       = glm::lookAt(
                glm::vec3(0,0,20), // Camera is at (4,3,3), in World Space
                glm::vec3(0,0,0), // and looks at the origin
                glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
                );
    // Model matrix : an identity matrix (model will be at the origin)
    glm::mat4 Model      = glm::mat4(1.0f);  // Changes for each model !

    // Our ModelViewProjection : multiplication of our 3 matrices
    glm::mat4 MVP        = Projection * View * Model; // Remember, matrix multiplication is the other way around
    GLuint MatrixID = glGetUniformLocation(p_index_cmap_, "MVP");
    glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
}

void scatterPlot::setShaders()
{


    char *vs,*fs;


    //-------------------------shader for constant colour---------------//

    v_index_cmap_ = glCreateShader(GL_VERTEX_SHADER);
    f_index_cmap_ = glCreateShader(GL_FRAGMENT_SHADER);
    //cout<<"read shaders "<<endl;
    vs = textFileRead("./glsl_shaders/scatterplot.vert");
    fs = textFileRead("./glsl_shaders/scatterplot.frag");

    const char * vv_index_cmap_ = vs;
    const char * ff_index_cmap_ = fs;
    //cout<<"shader "<<vv_light_dir<<endl;

    glShaderSource(v_index_cmap_, 1, &vv_index_cmap_,NULL);
    glShaderSource(f_index_cmap_, 1, &ff_index_cmap_,NULL);

    glCompileShader(v_index_cmap_);
    glCompileShader(f_index_cmap_);

    p_index_cmap_ = glCreateProgram();
    cout<<"LDA CREATE PROGRAM "<<p_index_cmap_<<endl;
    glAttachShader(p_index_cmap_,v_index_cmap_);
    glAttachShader(p_index_cmap_,f_index_cmap_);

    glBindAttribLocation(p_index_cmap_,0,"InVertex");
    glBindAttribLocation(p_index_cmap_,1,"InNormal");
    glBindAttribLocation(p_index_cmap_,2,"InScalar");

    glLinkProgram(p_index_cmap_);

    GLint* params = new GLint[3];
    glGetProgramiv(p_index_cmap_,GL_LINK_STATUS,params);
    cerr<<"scatterplot  LinkStatus "<<p_index_cmap_<<" "<<*params<<" "<<GL_TRUE<<endl;

    glUseProgram(p_index_cmap_);

    vertexLoc_ = glGetAttribLocation(p_index_cmap_, "InVertex");
    normalLoc_ = glGetAttribLocation(p_index_cmap_, "InNormal");
    scalarLoc_ = glGetAttribLocation(p_index_cmap_, "InScalar");
    //replace later if lda has own program(s)
    //    ana_lda->setVertexAttributeLocs(vertexLoc,normalLoc,scalarLoc);
//
//    cout<<"done set shaders gltree "<<glGetAttribLocation(p_index_cmap_, "InVertex")<<" "<<vertexLoc_<<" "<<normalLoc_<<" "<<scalarLoc_<<endl;
//    
//    glLinkProgram(p_index_cmap_);
//    
////    GLint* params = new GLint[3];
//    glGetProgramiv(p_index_cmap_,GL_LINK_STATUS,params);
//    cerr<<"scatterplot  LinkStatus "<<p_index_cmap_<<" "<<*params<<" "<<GL_TRUE<<endl;

    
    
    cerr<<"setup colourmap"<<endl;
    loc_r_lut = glGetUniformLocation(p_index_cmap_,"r_lut");
    loc_g_lut = glGetUniformLocation(p_index_cmap_,"g_lut");
    loc_b_lut = glGetUniformLocation(p_index_cmap_,"b_lut");
    loc_a_lut = glGetUniformLocation(p_index_cmap_,"a_lut");
    loc_sc_lut = glGetUniformLocation(p_index_cmap_,"sc_lut");
    cout<<"loc_sc_lut_dec_form "<<loc_sc_lut<<endl;

    r_lut[0]=0.37;
    g_lut[0]=0.67;
    b_lut[0]=0.98;
    a_lut[0]=1.0;
    sc_lut[0]=-0.5;

    r_lut[1]=0.29;
    g_lut[1]=0.71;
    b_lut[1]=0.29;
    a_lut[1]=1.0;
    sc_lut[1]=-0.45;


    r_lut[2]=0.29;
    g_lut[2]=0.71;
    b_lut[2]=0.29;
    a_lut[2]=1.0;
    sc_lut[2]=0.45;

    r_lut[3]=0.98;
    g_lut[3]=0.63;
    b_lut[3]=0.0;
    a_lut[3]=1.0;
    sc_lut[3]=0.5;


    cerr<<"copy colourmap"<<endl;

    glUniform4fv(loc_r_lut,1,r_lut);
    glUniform4fv(loc_g_lut,1,g_lut);
    glUniform4fv(loc_b_lut,1,b_lut);
    glUniform4fv(loc_a_lut,1,a_lut);
    glUniform4fv(loc_sc_lut,1,sc_lut);




}



void scatterPlot::paintAxesValues(){
    //rednering text
    //    string text = "EigenSpectrum of Covariance Matrix";
    glEnable(GL_LINE_SMOOTH);
    glHint(GL_LINE_SMOOTH_HINT,  GL_NICEST);
    GLint cur_prog;
    glGetIntegerv(GL_CURRENT_PROGRAM,&cur_prog);
    glUseProgram(0);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(w_left_,w_right_,w_bottom_,w_top_, 0.0f, 10000.0000f);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glTranslatef(0,0,-20);
    //    Title_ =  "AB";
    glColor3f(0.0,0.0,0.0);
    glEnable(GL_TEXTURE_2D);
    cout<<"paint xaxis"<<endl;
    {//x-axis
        vector<float> values;//value and pos
        values.push_back(axes_truelims_.x);
        values.push_back(axes_truelims_.y);
        cout<<"loop values"<<endl;
        for (vector<float>::iterator i_v = values.begin(); i_v != values.end(); ++i_v )
        {
            cout<<"val "<<(*i_v)<<endl;
            glMatrixMode(GL_MODELVIEW);
            glPushMatrix();
            
            stringstream ss;
//            ss.precision(2);
            ss.setf( std::ios::fixed, std:: ios::floatfield );
            ss<<*i_v;//miny
            string val;
            ss>>val;
            cout<<"value bounding box "<<val<<" "<<tick_values_.BBox(val.c_str()).Lower().X()<<" "<<\
            tick_values_.BBox(val.c_str()).Lower().Y()<<" "<<\
            tick_values_.BBox(val.c_str()).Upper().X()<<" "<<\
            tick_values_.BBox(val.c_str()).Upper().Y()<<endl;
            
            float val_height = tick_values_.BBox(val.c_str()).Upper().Y() - tick_values_.BBox(val.c_str()).Lower().Y();
            float val_width = tick_values_.BBox(val.c_str()).Upper().X() - tick_values_.BBox(val.c_str()).Lower().X();
            
            float scale = horizontal_spacing_ *width_ / val_width;
            float scaleY = vertical_spacing_ *height_ / TitleText_.BBox(Title_.c_str()).Upper().Y();
            //0.05 places close within axes
            //    float scaleX = (width_ - 2 * horizontal_spacing_) / TitleText_.BBox(Title_.c_str()).Upper().X();
            //    float scale = (scaleX < scaleY ) ? scaleX : scaleY;
            scale*=0.8;//extra bit of shrinking
            float shiftx = *i_v - axes_truelims_.x;
            cout<<"shiftx "<<axes_centre_.x<<" "<<*i_v<<" "<<axes_truelims_.x<<" "<<shiftx<<endl;
            float3 txyz( axes_centre_.x + 0.5*val_width * scale - tick_values_.BBox(val.c_str()).Upper().X() * scale + shiftx*axes_scale_.x, \
                        axes_centre_.y -  0.5*val_height * scale - tick_values_.BBox(val.c_str()).Upper().Y() * scale,\
                        0);
            //+  0.5*val_width * scale + shiftx*axes_scale_.x
            //    float3 txyz( plot_left_ +  width_*0.5 - TitleText_.BBox(Title_.c_str()).Upper().X()*0.5*scale, plot_bottom_ + height_ * (1.0f - vertical_spacing_),0);
            cout<<"scale "<<scale<<" "<<TitleText_.BBox(Title_.c_str()).Upper().Y()<<" "<<TitleText_.BBox(Title_.c_str()).Upper().X()<<endl;
            
            //only used when using GLSL
            //    setModelViewProjMatrixXFMmodel( glm::vec3(scale,scale,scale) );
            
            cout<<"translate "<<txyz.x<<" "<<txyz.y<<" "<<txyz.z<<endl;
            //add bit tolerance for appearance
//            txyz.x-=0.01;
            glTranslatef(txyz.x,txyz.y,txyz.z);
            
            glScalef(scale,scale,scale);
            //need to translate
            //   glTranslatef(txyz.x,txyz.y,txyz.z);
            
            
            
            
            //    float yu=textBox.BBox(text.c_str()).Upper().Y();
            //    float yl=textBox.BBox(text.c_str()).Lower().Y();
            //    float ydist=(i_v+2)->y - (i_v+1)->y;
            //    float shift_y = (spacing - (yu-yl))/2.0; //match with mid spacing
            //    glTranslatef( -(xu+1) + tx, count * spacing + shift_y + ty, 0.0 );
            tick_values_.Render(val.c_str());//text.size());
            
            glMatrixMode(GL_MODELVIEW);
            glPopMatrix();
        }
    }////----
    
    cout<<"done xaxis "<<endl;
    {//y-axis
        vector<float> values;//value and pos
        values.push_back(axes_truelims_.z);
        values.push_back(axes_truelims_.t);
        
        for (vector<float>::iterator i_v = values.begin(); i_v != values.end(); ++i_v )
        {
            glMatrixMode(GL_MODELVIEW);
            glPushMatrix();
            
            stringstream ss;
//            ss.precision(2);
            ss.setf( std::ios::fixed, std:: ios::floatfield );
            ss<<*i_v;//miny
            string val;
            ss>>val;
            cout<<"value bounding box "<<val<<" "<<tick_values_.BBox(val.c_str()).Lower().X()<<" "<<\
            tick_values_.BBox(val.c_str()).Lower().Y()<<" "<<\
            tick_values_.BBox(val.c_str()).Upper().X()<<" "<<\
            tick_values_.BBox(val.c_str()).Upper().Y()<<endl;
            
            float val_height = tick_values_.BBox(val.c_str()).Upper().Y() - tick_values_.BBox(val.c_str()).Lower().Y();
            float val_width = tick_values_.BBox(val.c_str()).Upper().X() - tick_values_.BBox(val.c_str()).Lower().X();
            
            float scale = horizontal_spacing_ *width_ / val_width;
            float scaleY = vertical_spacing_ *height_ / TitleText_.BBox(Title_.c_str()).Upper().Y();
            //0.05 places close within axes
            //    float scaleX = (width_ - 2 * horizontal_spacing_) / TitleText_.BBox(Title_.c_str()).Upper().X();
            //    float scale = (scaleX < scaleY ) ? scaleX : scaleY;
            scale*=0.8;//extra bit of shrinking
            float shifty = *i_v - axes_truelims_.z;
            cout<<"shifty "<<*i_v<<" "<<axes_truelims_.z<<" "<<shifty<<endl;
            float3 txyz( axes_centre_.x - tick_values_.BBox(val.c_str()).Upper().X() * scale,axes_centre_.y -  0.5*val_height * scale + shifty*axes_scale_.y,0);
            
            //    float3 txyz( plot_left_ +  width_*0.5 - TitleText_.BBox(Title_.c_str()).Upper().X()*0.5*scale, plot_bottom_ + height_ * (1.0f - vertical_spacing_),0);
            cout<<"scale "<<scale<<" "<<TitleText_.BBox(Title_.c_str()).Upper().Y()<<" "<<TitleText_.BBox(Title_.c_str()).Upper().X()<<endl;
            
            //only used when using GLSL
            //    setModelViewProjMatrixXFMmodel( glm::vec3(scale,scale,scale) );
            
            cout<<"translate "<<txyz.x<<" "<<txyz.y<<" "<<txyz.z<<endl;
            //add bit tolerance for appearance
            txyz.x-=0.01;
            glTranslatef(txyz.x,txyz.y,txyz.z);
            
            glScalef(scale,scale,scale);
            //need to translate
            //   glTranslatef(txyz.x,txyz.y,txyz.z);
            
            
            
            
            //    float yu=textBox.BBox(text.c_str()).Upper().Y();
            //    float yl=textBox.BBox(text.c_str()).Lower().Y();
            //    float ydist=(i_v+2)->y - (i_v+1)->y;
            //    float shift_y = (spacing - (yu-yl))/2.0; //match with mid spacing
            //    glTranslatef( -(xu+1) + tx, count * spacing + shift_y + ty, 0.0 );
            tick_values_.Render(val.c_str());//text.size());
            
            glMatrixMode(GL_MODELVIEW);
            glPopMatrix();
        }
    }///-----
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();


    glDisable(GL_TEXTURE_2D);
    glDisable(GL_LINE_SMOOTH);
    glUseProgram(cur_prog);

}


void scatterPlot::paintTitle(){
    //rednering text
    //    string text = "EigenSpectrum of Covariance Matrix";
    glEnable(GL_LINE_SMOOTH);
    glHint(GL_LINE_SMOOTH_HINT,  GL_NICEST);
    GLint cur_prog;
    glGetIntegerv(GL_CURRENT_PROGRAM,&cur_prog);
    glUseProgram(0);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(w_left_,w_right_,w_bottom_,w_top_, 0.0f, 10000.0000f);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glTranslatef(0,0,-20);
    //    Title_ =  "AB";
    glColor3f(0.0,0.0,0.0);
    glEnable(GL_TEXTURE_2D);
    float scaleY = vertical_spacing_ *height_ / TitleText_.BBox(Title_.c_str()).Upper().Y();
    //0.05 places close within axes
    float scaleX = (width_ - 2 * horizontal_spacing_) / TitleText_.BBox(Title_.c_str()).Upper().X();
    float scale = (scaleX < scaleY ) ? scaleX : scaleY;
    scale*=0.95;//extra bit of shrinking
    //float3 txyz( horizontal_spacing_*width_, height_ * (1.0f - vertical_spacing_),0);
    //float3 txyz( plot_left_ + horizontal_spacing_*width_ , plot_bottom_ + height_ * (1.0f - vertical_spacing_),0);
cout<<"extra shift "<<TitleText_.BBox(Title_.c_str()).Upper().X()<<" "<<TitleText_.BBox(Title_.c_str()).Upper().X()*0.5*scaleX<<endl;
   float3 txyz( plot_left_ +  width_*0.5 - TitleText_.BBox(Title_.c_str()).Upper().X()*0.5*scale, plot_bottom_ + height_ * (1.0f - vertical_spacing_),0);
    cout<<"scale "<<scale<<" "<<TitleText_.BBox(Title_.c_str()).Upper().Y()<<" "<<TitleText_.BBox(Title_.c_str()).Upper().X()<<endl;

    //only used when using GLSL
    //    setModelViewProjMatrixXFMmodel( glm::vec3(scale,scale,scale) );

    cout<<"translate "<<txyz.x<<" "<<txyz.y<<" "<<txyz.z<<endl;
    //add bit tolerance for appearance
    txyz.x+=0.025;
    glTranslatef(txyz.x,txyz.y,txyz.z);

    glScalef(scale,scale,scale);
    //need to translate
    //    glTranslatef(txyz.x,txyz.y,txyz.z);


//    glBegin(GL_TRIANGLES);
//    glVertex3f(0,0,1);
//    glVertex3f(0.5,0,1);
//    glVertex3f(0.5,1,1);

//    glEnd();
    //    float xu=textBox.BBox(text.c_str()).Upper().X();
    //    float yu=textBox.BBox(text.c_str()).Upper().Y();
    //    float yl=textBox.BBox(text.c_str()).Lower().Y();
    //    float ydist=(i_v+2)->y - (i_v+1)->y;
    //    float shift_y = (spacing - (yu-yl))/2.0; //match with mid spacing
    //    glTranslatef( -(xu+1) + tx, count * spacing + shift_y + ty, 0.0 );
    TitleText_.SetLineSpacing(100);
    cout<<"lineinfo "<<TitleText_.GetLineLength()<<" "<<TitleText_.GetLineSpacing()<<endl;
    TitleText_.Render(Title_.c_str());//text.size());
    //    Title_ =  "CD";
    //    TitleText_.SetFont(TitleTextureFont_);
    cout<<"done render "<<endl;


    //cout<<"textureID "<<textureID[1]<<endl;
    //    glBindTexture(GL_TEXTURE_2D, textureID[1]);

    //    TitleText_.Render(Title_.c_str());//text.size());

    //    glPopMatrix();
        glMatrixMode(GL_MODELVIEW);
      glPopMatrix();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();


    glDisable(GL_TEXTURE_2D);
    glDisable(GL_LINE_SMOOTH);
    glUseProgram(cur_prog);

}

void scatterPlot::generateBars( float t)
{
    bars_verts_.clear();
    bars_inds_.clear();
    unsigned int vert_count=0;
    for ( vector< vertex >::iterator i_v = datapts_verts_.begin(); i_v != datapts_verts_.end(); ++i_v,++vert_count)
    {
        //.z is the y minimum value, i.e. location of x-axis
        cout<<"geneating bars "<<i_v->x<<" "<<i_v->y<<" "<<axes_truelims_.z<<endl;
        bars_verts_.push_back(vertex(float4(i_v->x-0.25, axes_truelims_.z,render_depth_,t)));
        bars_verts_.push_back(vertex(float4(i_v->x-0.25, i_v->y,render_depth_,t)));
        bars_verts_.push_back(vertex(float4(i_v->x+0.25, i_v->y,render_depth_,t)));
        bars_verts_.push_back(vertex(float4(i_v->x+0.25, axes_truelims_.z,render_depth_,t)));

        unsigned int baseind = vert_count*4;
        bars_inds_.push_back(baseind );
        bars_inds_.push_back(baseind +1 );
        bars_inds_.push_back(baseind + 2);
        bars_inds_.push_back(baseind  );
        bars_inds_.push_back(baseind +3);
        bars_inds_.push_back(baseind +2);

    }
}

void scatterPlot::setDataPoints( const std::vector<float2> & xy, float t )
{
    cout<<"setdatapoints "<<endl;
    datapts_verts_.clear();
    datapts_inds_.clear();

    xyt_.clear();

    vector<float2>::const_iterator i_xy = xy.begin();
    unsigned int index=0;
    for ( ; i_xy != xy.end(); ++i_xy,++index)
    {
        cout<<"setdatapoints "<<index<<" "<<i_xy->x<<" "<<i_xy->y<<endl;
        xyt_.push_back(float3(i_xy->x,i_xy->y,t));

        datapts_verts_.push_back(vertex(float4(i_xy->x,i_xy->y,render_depth_,t)));
        datapts_inds_.push_back(index);

       //add bar graph capabilities, starting using 0.5 offsets for bvar thickness



    }
    //generateBars(2);
    setAxesLimits();
    cout<<"done setdatapoints "<<endl;
}

//void scatterPlot::setDataPoints( const std::vector<float> & x,  const std::vector<float> & y )
//{
//    cout<<"setdatapoints "<<endl;
//    datapts_verts_.clear();
//    datapts_inds_.clear();
//    v_values_x_=x;
//    v_values_y_=y;

//    if ( x.size() != y.size() )
//        return;


//    vector<float>::const_iterator i_x = x.begin();
//    vector<float>::const_iterator i_y = y.begin();
//    unsigned int index=0;
//    for ( ; i_x != x.end(); ++i_x, ++i_y,++index)
//    {
//        cout<<"setdatapoints "<<index<<endl;

//        datapts_verts_.push_back(vertex(float4(*i_x,*i_y,render_depth_,2)));
//        datapts_inds_.push_back(index);
//    }
//    cout<<"done setdatapoints "<<endl;


//}

void scatterPlot::paintGL(){
    //   setShaders();
cout<<"scatterPlot::paintGL() "<<Title_<<endl;
generateBars(3);
    // Enable depth test
    //    glEnable(GL_DEPTH_TEST);
    //    // Accept fragment if it closer to the camera than the former one
    //    glDepthFunc(GL_LESS);
    //    glClear(GL_DEPTH_BUFFER_BIT);
    GLint cur_prog;
    glGetIntegerv(GL_CURRENT_PROGRAM,&cur_prog);
    cout<<"done get"<<endl;
//    cout<<"prgram "<<p_index_cmap_<<vbos_triangles_[0]<<" "<<vbos_triangles_[1]<<endl;

    glUseProgram(p_index_cmap_);
    setModelViewProjMatrix();

    
    createViewports();
    createAxes();


    cout<<"do vert buffer "<<triangles_verts_.size()<<" "<<triangle_inds_.size()<<endl;

//---------THIS SHOUDL EVENTUALLY BE MOVE ELSEWHERE FOR EFFICIENCU---------------------
    //------BUFFER TRIANGLES
    glBindBuffer(GL_ARRAY_BUFFER, vbos_triangles_[0]);
    glBufferData(GL_ARRAY_BUFFER, triangles_verts_.size()*sizeof(su_mvdisc_gui_name::vertex), reinterpret_cast<float*>(&(triangles_verts_[0])), GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    cout<<"done vert buffer"<<endl;
    //    unsigned int* inds = reinterpret_cast<unsigned int*>(&(vbos_triangles_[0]));
    //    cout<<"inds "<<inds[0]<<" "<<inds[1]<<" "<<inds[2]<<" "<<inds[3]<<" "<<inds[4]<<endl;

    for (vector<vertex>::iterator i = triangles_verts_.begin(); i != triangles_verts_.end(); ++i)
        cout<<"background verts "<<i->x<<" "<<i->y<<" "<<i->z<<endl;
    
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbos_triangles_[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, triangle_inds_.size()*sizeof( su_mvdisc_gui_name::uint3), reinterpret_cast<unsigned int*>(&(triangle_inds_[0])),GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    //------BUFFER LINES

    glBindBuffer(GL_ARRAY_BUFFER, vbos_lines_[0]);
    glBufferData(GL_ARRAY_BUFFER, lines_verts_.size()*sizeof(su_mvdisc_gui_name::vertex), reinterpret_cast<float*>(&(lines_verts_[0])), GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    for (vector<vertex>::iterator i = lines_verts_.begin(); i != lines_verts_.end(); ++i)
        cout<<"lines verts "<<i->x<<" "<<i->y<<" "<<i->z<<endl;
    
    
    unsigned int * inds =  reinterpret_cast<unsigned int*>(&(lines_inds_[0]));
                                                           cout<<"inds "<<lines_inds_.size()<<" "<<lines_inds_[1].x<<" "<<(*inds)<<" "<<(*(inds+1))<<" "<<(*(inds+2))<<" "<<(*(inds+3))<<endl;
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbos_lines_[2]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 2* lines_inds_.size()*sizeof( unsigned int), reinterpret_cast<unsigned int*>(&(lines_inds_[0])),GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    //------BUFFER data points

    glBindBuffer(GL_ARRAY_BUFFER, vbos_datapts_[0]);
    glBufferData(GL_ARRAY_BUFFER, datapts_verts_.size()*sizeof(su_mvdisc_gui_name::vertex), reinterpret_cast<float*>(&(datapts_verts_[0])), GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    for (vector<vertex>::iterator i = datapts_verts_.begin(); i != datapts_verts_.end(); ++i)
        cout<<"pts verts "<<i->x<<" "<<i->y<<" "<<i->z<<endl;
    

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbos_datapts_[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, datapts_inds_.size()*sizeof( unsigned int), reinterpret_cast<unsigned int*>(&(datapts_inds_[0])),GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    //BUffer bars for bar graph
    glBindBuffer(GL_ARRAY_BUFFER, vbos_bars_[0]);
    glBufferData(GL_ARRAY_BUFFER, bars_verts_.size()*sizeof(su_mvdisc_gui_name::vertex), reinterpret_cast<float*>(&(bars_verts_[0])), GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbos_bars_[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, bars_inds_.size()*sizeof( unsigned int), reinterpret_cast<unsigned int*>(&(bars_inds_[0])),GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);





    //--------Draw TRIANGLES ----Drawinf triangles is for backgroudns, bars are triangle but no include in these traingles


    //    cout<<"done tri buffer"<<endl;
    cout<<"VBOS "<<vbos_lines_[0]<<" "<<vbos_lines_[1]<<endl;
    //done copying do rendering
    glBindBuffer(GL_ARRAY_BUFFER, vbos_triangles_[0] );         // for vertex coordinates
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbos_triangles_[1] );

    //    cout<<"open gl scatter plot :: done bind buffers "<<vertexLoc_<<" "<<normalLoc_<<" "<<scalarLoc_<<endl;
    glEnableVertexAttribArray(vertexLoc_);
    glEnableVertexAttribArray(normalLoc_);
    glEnableVertexAttribArray(scalarLoc_);

    glVertexAttribPointer(vertexLoc_, 3, GL_FLOAT, GL_FALSE, sizeof(vertex),  BUFFER_OFFSET(0));
    glVertexAttribPointer(normalLoc_, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), BUFFER_OFFSET(3*sizeof(float)));
    glVertexAttribPointer(scalarLoc_, 1, GL_FLOAT, GL_FALSE, sizeof(vertex), BUFFER_OFFSET(6*sizeof(float)));

    cout<<"scatter plot darw triangles "<<triangle_inds_.size()<<endl;
    glDrawElements(GL_TRIANGLES, triangle_inds_.size()*3,GL_UNSIGNED_INT,0);

    glDisableVertexAttribArray(vertexLoc_);
    glDisableVertexAttribArray(normalLoc_);
    glDisableVertexAttribArray(scalarLoc_);





    //    glBindBuffer(GL_ARRAY_BUFFER, 0 );         // for vertex coordinates
    //    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0 );
    //glUseProgram(0);
    //--------Draw LINES

    glLineWidth(2);
    glEnable(GL_LINE_SMOOTH);
    glHint(GL_LINE_SMOOTH_HINT,  GL_NICEST);

    glBindBuffer(GL_ARRAY_BUFFER, vbos_lines_[0] );         // for vertex coordinates
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbos_lines_[2] );

    //    cout<<"open gl scatter plot :: done bind buffers "<<vertexLoc_<<" "<<normalLoc_<<" "<<scalarLoc_<<endl;
    glEnableVertexAttribArray(vertexLoc_);
    glEnableVertexAttribArray(normalLoc_);
    glEnableVertexAttribArray(scalarLoc_);

    glVertexAttribPointer(vertexLoc_, 3, GL_FLOAT, GL_FALSE, sizeof(vertex),  BUFFER_OFFSET(0));
    glVertexAttribPointer(normalLoc_, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), BUFFER_OFFSET(3*sizeof(float)));
    glVertexAttribPointer(scalarLoc_, 1, GL_FLOAT, GL_FALSE, sizeof(vertex), BUFFER_OFFSET(6*sizeof(float)));

    cout<<"scatter plot darw lines "<<lines_inds_.size()<<endl;
    glDrawElements(GL_LINES, lines_inds_.size()*2,GL_UNSIGNED_INT,0);
    //    glDrawElements(GL_LINE_STRIP, linestrip_inds_.size(),GL_UNSIGNED_INT,0);

    //    glDrawElements(GL_LINES, 4,GL_UNSIGNED_INT,0);

    glDisableVertexAttribArray(vertexLoc_);
    glDisableVertexAttribArray(normalLoc_);
    glDisableVertexAttribArray(scalarLoc_);

    glDisable(GL_LINE_SMOOTH);


    glBindBuffer(GL_ARRAY_BUFFER, 0 );         // for vertex coordinates
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0 );


    glPointSize(6);

cout<<"axes translate "<<axes_tx_.x<<" "<<axes_tx_.y<<endl;
cout<<"axes scale "<<axes_scale_.x<<" "<<axes_scale_.y<<endl;

    setModelViewProjMatrixXFMmodel(glm::vec3(axes_tx_.x, axes_tx_.y,0),glm::vec3(axes_scale_.x,axes_scale_.y,1.0));

    if (do_bars_)
{
    glBindBuffer(GL_ARRAY_BUFFER, vbos_bars_[0] );         // for vertex coordinates
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbos_bars_[1] );

    //    cout<<"open gl scatter plot :: done bind buffers "<<vertexLoc_<<" "<<normalLoc_<<" "<<scalarLoc_<<endl;
    glEnableVertexAttribArray(vertexLoc_);
    glEnableVertexAttribArray(normalLoc_);
    glEnableVertexAttribArray(scalarLoc_);

    glVertexAttribPointer(vertexLoc_, 3, GL_FLOAT, GL_FALSE, sizeof(vertex),  BUFFER_OFFSET(0));
    glVertexAttribPointer(normalLoc_, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), BUFFER_OFFSET(3*sizeof(float)));
    glVertexAttribPointer(scalarLoc_, 1, GL_FLOAT, GL_FALSE, sizeof(vertex), BUFFER_OFFSET(6*sizeof(float)));

    cout<<"scatter plot darw triangles "<<bars_inds_.size()<<endl;
    glDrawElements(GL_TRIANGLES, bars_inds_.size(),GL_UNSIGNED_INT,0);

    glDisableVertexAttribArray(vertexLoc_);
    glDisableVertexAttribArray(normalLoc_);
    glDisableVertexAttribArray(scalarLoc_);

}
//    glUseProgram(0);

    glBindBuffer(GL_ARRAY_BUFFER, vbos_datapts_[0] );         // for vertex coordinates
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbos_datapts_[1] );

       cout<<"open gl scatter plot :: done bind buffers "<<vertexLoc_<<" "<<normalLoc_<<" "<<scalarLoc_<<endl;
    glEnableVertexAttribArray(vertexLoc_);
    glEnableVertexAttribArray(normalLoc_);
    glEnableVertexAttribArray(scalarLoc_);

    glVertexAttribPointer(vertexLoc_, 3, GL_FLOAT, GL_FALSE, sizeof(vertex),  BUFFER_OFFSET(0));
    glVertexAttribPointer(normalLoc_, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), BUFFER_OFFSET(3*sizeof(float)));
    glVertexAttribPointer(scalarLoc_, 1, GL_FLOAT, GL_FALSE, sizeof(vertex), BUFFER_OFFSET(6*sizeof(float)));
    glColor3f(1.0,0,0);
    cout<<"scatter plot draw lines "<<datapts_inds_.size()<<endl;
//    if (do_lines_)
    for ( vector<unsigned int>::iterator i = datapts_inds_.begin(); i != datapts_inds_.end(); ++i)
    {
        cout<<"vert inds "<<*i<<endl;
    }
    
//    glDrawArrays(GL_LINE_STRIP,datapts_inds_.size(),datapts_inds_.size());

    if (do_points_)
    glDrawElements(GL_POINTS, datapts_inds_.size(),GL_UNSIGNED_INT,0);
    //    glDrawElements(GL_LINES, 4,GL_UNSIGNED_INT,0);

    glDisableVertexAttribArray(vertexLoc_);
    glDisableVertexAttribArray(normalLoc_);
    glDisableVertexAttribArray(scalarLoc_);



    glBindBuffer(GL_ARRAY_BUFFER, 0 );         // for vertex coordinates
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0 );



    glUseProgram(cur_prog);


    paintTitle();
    cout<<"done paintTitle"<<endl;

    paintAxesValues();

    cout<<"done paintAxesValues"<<endl;


}

}
