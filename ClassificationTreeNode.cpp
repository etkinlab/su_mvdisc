/*
 *  ClassificationTreeNode.cpp
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 7/20/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include <misc_utils.h>
#include <assert.h>
//#include <cstdio>
#include <type_traits>

#include <cstdlib>
#include "ClassificationTreeNode.h"


#include <nifti_writer.h>
#ifdef ARCHLINUX
extern "C" {
#include "cblas.h"
  //#include "f2c.h"
  //typedef long int __CLPK_integer;                                                                                                                
  //typedef float  __CLPK_real;                                                                                                                     

  // typedef integer __CLPK_integer;
  //  typedef real __CLPK_real;
  //#include "clapack.h"
}
#else
//#endif
//#ifdef ARCHDARWIN
#include <Accelerate/Accelerate.h>
#endif

//STL inludesgen
#include <iostream>
#include <sstream>
#include <fstream>
#include <set>

using namespace std;
using namespace misc_utils_name;

namespace su_mvdisc_name{

	ClassificationTreeNode::ClassificationTreeNode()
	{
		//cerr<<"~ClassificationTreeNode : Constructor "<<NodeName<<endl;


		nodeType=UNDEFINED;
        nodeTypeSpec=UNDEFINED;
		out_data=nullptr;// new vector<float>();
        _output_connections=0;//keeps tracks of number of connection, used for deletion of ouput data in case of multiple dependecy
        DELETED_INPUT_DATA=false;
		src_type = TRAINING;
		Verbose=false;
		SwapFeaturesAndDimensions=false;
		//in_test_data= new vector<float>();
//		out_target= new Target();
		out_test_data=nullptr;//new vector<float>();
        out_ext_test_data=nullptr;//new vector<float>();

        out_chunks_=nullptr;
        out_test_chunks_=nullptr;
        out_data_names_=nullptr;
        out_clusters_=nullptr;
        out_test_clusters_=nullptr;
		//training_target=nullptr;
		training_target = nullptr;
		output_feature_mask_ = nullptr;
        disc_distance=nullptr;
//        genData_=TpFpTnFn(0,0,0,0);
        DoGenAcc_=false;
        doKfold_=false;
        
		out_format = NOT_SPECIFIED;
		trackStats=false;
        saveParams_=false;

		DimTrim=0;
		//in_leftout_data= new vector<float>();
		NodeName="ClassificationTreeNode";
		//cout<<"ClassificationTreeNode : Constructor "<<NodeName<<endl;
			output_basename="output_"+NodeName;
		//RequireSelectionMask = false;
		PassDownOutput=false;
        stats_sum_N  = nullptr;
		stats_sum_x  = nullptr;
		stats_sum_xx = nullptr;
        
        src_image_res_ = nullptr;
		src_image_dims_ = nullptr;
        src_names_ = nullptr;
        src_dims_ = nullptr;
//		stats_image_res.x = stats_image_res.y = stats_image_res.z = 0;
//		stats_image_dims.x = stats_image_dims.y = stats_image_dims.z = 0;
        level=0;
        NumberOfChunks_=0;
        NumberOfTestChunks_=0;

	}

	ClassificationTreeNode::~ClassificationTreeNode()
	{
		//		cerr<<"~ClassificationTreeNode : Destructor "<<NodeName<<endl;
		//clean up all pointers

		if (out_data!=nullptr)	delete out_data;

		if (out_data_names_!=nullptr)	delete out_data_names_;

		if (out_chunks_!=nullptr)	delete out_chunks_;

		if (out_test_data !=nullptr)
		{
			delete out_test_data;
			out_test_data=nullptr;
		}

		if (out_test_chunks_ !=nullptr)	delete out_test_chunks_;

		if (out_clusters_ !=nullptr)	delete out_clusters_;

        if (out_ext_test_data !=nullptr)	delete out_ext_test_data;

        if (training_target!=nullptr)	delete training_target;

        if (disc_distance != nullptr)	delete disc_distance;
        
		if (output_feature_mask_ != nullptr)	delete output_feature_mask_;
		
		if (stats_sum_x != nullptr)	delete stats_sum_x;

		if (stats_sum_xx != nullptr)	delete stats_sum_xx;

        if (stats_sum_N != nullptr)	delete stats_sum_N;
        
        if (src_image_res_ != nullptr)	delete src_image_res_;
        
        if (src_image_dims_ != nullptr)	delete src_image_dims_;
        
        if (src_names_ != nullptr )	delete src_names_;

        if (src_dims_ != nullptr )	delete src_dims_;

	}
	void ClassificationTreeNode::AllocateMemory()
	{
	}
	
	bool ClassificationTreeNode::setTreeNodeOptions( const std::string & map_key, const std::string & map_value )
	{
		//treeNode-wide options that you don't wish to re-implement
		//we don't throw errors because options will be used 
		//return if found, this is to be used in subclasses 

			if ( map_key == "Verbose"){
				if ((map_value == "true") || (map_value == "1"))
					Verbose = 1;
				else if ((map_value == "false") || (map_value == "0"))
					Verbose = 0;
                else //integer value
                    Verbose=atoi(map_value.c_str());

//                {
//                    if (! isNumeric(map_value))
//                        throw ClassificationTreeNodeException("Verbose need to be true/false or numeric");
//                    Verbose=static_cast<int>(atof(map_value.c_str()));//yields precitable vlaues for non-interger input, though odd
//                }
				return 1;
				//cout<<"Set Verbose"<<endl;
			}else if ( map_key == "Output_Name"){
				output_basename = map_value;
				return 1;
				//cout<<"Set Verbose"<<endl;
			}else if (map_key == "featureNames" ){
                features_names_ = map_value;
                return 1;
            }else if ( map_key == "TrackStats"){
                if (map_value == "YES"){	
                    PassDownOutput=true;
                    trackStats=true;
                }else
                    trackStats=false;
                return 1;
            }else if ( map_key == "SaveParameters"){
                cerr<<"ClassificationTreeNode::setTreeNodeOptions "<<"SaveParameters"<<" "<<map_value<<endl;
                if  ((map_value == "true") || (map_value == "1")) {
                    saveParams_=true;
                }if ((map_value == "false") || (map_value == "0"))
                    saveParams_=false;
                return 1;

            }else if (map_key == "calcGenAccuracy"){
                if  ((map_value == "true") || (map_value == "1")) {
                    DoGenAcc_=true;
                }if ((map_value == "false") || (map_value == "0"))
                    DoGenAcc_=false;
                return 1;
             }
        

//			else if ( map_key == "SwapFeaturesAndDimensions"){
//				if ((map_value == "true") || (map_value == "1"))
//					SwapFeaturesAndDimensions = true;
//				else if ((map_value == "false") || (map_value == "0"))
//					SwapFeaturesAndDimensions = false;
//				return 1;
//				cout<<"Set swap features and dimensions"<<endl;
			
//			}
		return 0;
			
	}

	void ClassificationTreeNode::setOptions( std::stringstream & ss_options)
	{//this is virtual and generally re-implemented
		map<string,string> options = parse_sstream(ss_options);
		for ( map<string,string>::iterator it = options.begin(); it!=options.end();it++)
		{
			if (!setTreeNodeOptions(it->first,it->second))
				throw ClassificationTreeNodeException("ClassificationTreeNode invalid option");
		}
	}
    
    //mainly used for visualization ranks height in tree, 0 on top
    void ClassificationTreeNode::setLevel( const unsigned int & level_in ){
        level=level_in;
        for ( vector<  ClassificationTreeNode* >::iterator i_inputs = node_inputs.begin(); i_inputs != node_inputs.end(); i_inputs++)
        {
            (*i_inputs)->setLevel(level_in+1);
        }
    }
    
    unsigned int ClassificationTreeNode::getLevel( ){
        return level;
    }
    
	
	void ClassificationTreeNode::recursivelyEstimateParameters()
	{
		for ( vector<  ClassificationTreeNode* >::iterator i_inputs = node_inputs.begin(); i_inputs != node_inputs.end(); i_inputs++)
			(*i_inputs)->recursivelyEstimateParameters();
		
		this->estimateParameters();
		
	}
	void ClassificationTreeNode::setOuputDataNames( const std::vector<std::string> & names )
	{ 
		if (out_data_names_ ==nullptr )out_data_names_ = new vector<string>();
		
		*out_data_names_ = names; 
		}

	
 

	bool ClassificationTreeNode::hasTrainingData()
	{
		return (in_data.size()>0);
	}
    
    template <class T>
    std::vector<T> ClassificationTreeNode::getTargetData(  )
    {
        if (training_target==nullptr)
            throw ClassificationTreeNodeException("ClassificationTreeNode : Tried to get target data, however target does not yet exist");
        
        return training_target->getTargetData<T>();
    }

    template std::vector<int> ClassificationTreeNode::getTargetData<int>(  );

    //    template std::vector<unsigned int> ClassificationTreeNode::getTargetData<unsigned int>(  );
    template std::vector<float> ClassificationTreeNode::getTargetData<float>(  );

	void ClassificationTreeNode::setTarget()
	{

        if (Verbose>=1)
        {
            cout<<"ClassificationTreeNode : setTarget :  "<<NodeName<<endl;
        }
        
		if (node_inputs.empty())
			throw ClassificationTreeNodeException("Trying to set target with no input nodes");

        if (Verbose>=3)
        {
            cout<<"Delete trainign target"<<endl;
        }
        
        if (training_target!=nullptr)
        {
            if (Verbose>=3)
            {
                cerr<<"Training target is not nullptr...delete"<<endl;
            }
            delete training_target;
            training_target=nullptr;
            if (Verbose>=3)
            {
                cerr<<"...succesfully deleted training target."<<endl;
            }
        }
        
        if (Verbose>=3)
        {
            cout<<"Create new target..."<<endl;
        }
        
        training_target = new Target(node_inputs.at(0)->getTargetPointer());
        
        if (Verbose>=3)
        {
            cout<<"...sucessfully created target."<<endl;
        }
        Kclasses_ = training_target->getNumberOfClasses();

       // cout<<"done setTarget"<<endl;

	}
	void ClassificationTreeNode::applyToTestData()
	{
        
		if (out_test_data == nullptr)	out_test_data = new vector<float>();

        if (out_test_chunks_ == nullptr)	out_test_chunks_ = new vector<unsigned int>();

        copyToSingleVector( in_test_data, out_test_data, NumberOfTestSamples_ );
        
        if ( in_test_chunks_[0] != nullptr )	copyToSingleVector( in_test_chunks_, out_test_chunks_, NumberOfTestSamples_);//number of test samples is to interleave the data

        if ( in_test_clusters_[0] != nullptr )
        {
        	copyToSingleVector( in_test_clusters_, out_test_clusters_, NumberOfTestSamples_);//number of test samples is to interleave the data
        }


	}
    std::vector<unsigned int>*  ClassificationTreeNode::getChunksPointer()
    {
        return out_chunks_;
    }

    std::vector<unsigned int>::iterator ClassificationTreeNode::getChunksBegin()
    {
        return out_chunks_->begin();
    }
    std::vector<unsigned int>::iterator ClassificationTreeNode::getChunksEnd()
    {
         return out_chunks_->end();
    }
    
    std::vector<unsigned int>*  ClassificationTreeNode::getClustersPointer()
    {
        return out_clusters_;
    }

    std::vector<unsigned int>::iterator ClassificationTreeNode::getClustersBegin()
    {
        return out_clusters_->begin();
    }
    std::vector<unsigned int>::iterator ClassificationTreeNode::getClustersEnd()
    {
         return out_clusters_->end();
    }
    
    
	bool ClassificationTreeNode::isTestSource()
	{
			return ( src_type == TEST );
	}

	void ClassificationTreeNode::setTestData()
	{
		cout<<"set test data "<<endl;
        if (Verbose) {
            cout<<"setTestData "<<NodeName<<endl;
            cout<<"ClassificationTreeNode::setTestData "<<endl;
        }

        in_test_data.clear();
        in_test_chunks_.clear();

        if (node_inputs.empty())
        	throw ClassificationTreeNodeException("Trying to set Test Data with no input nodes");
        cout<<"set test data1 "<<endl;

        for ( vector<  ClassificationTreeNode* >::iterator i_inputs = node_inputs.begin(); i_inputs != node_inputs.end(); i_inputs++)
        {
        	//There are two possible ways to combine test data, etiher by dimensions or by adding subjects
        	if ( ! (*i_inputs)->isTestSource() )
        	{
        		in_test_data.push_back((*i_inputs)->getTestDataPointer());
        		in_test_chunks_.push_back((*i_inputs)->getTestChunksPointer());
        	}
        }
        cout<<"set test data 2"<<endl;

        NumberOfTestSamples_ = node_inputs[0]->getNumberOfTestSamples();
        NumberOfTestChunks_ = node_inputs[0]->getNumberOfTestChunks();

		if (Verbose)
        {
			cout<<"Number of Test Samples "<<NumberOfTestSamples_<<endl;
            cout<<"Number of Test Chunks "<<NumberOfTestChunks_<<endl;
        }
		//do non test sourcess first, going to append to them
		//check to make sure a consistent number of samples are in each source
		unsigned int count=0;
		unsigned int Nprev=0;		
		for (vector< ClassificationTreeNode* >::iterator i = node_inputs.begin() ; i != node_inputs.end(); i++, count++)
		{
				unsigned int N = (*i)->getNumberOfTestSamples();

				if ((count != 0) && (N!=Nprev))	throw ClassificationTreeNodeException("ClassificationTreeNode::setTestData : Number of test samples in each source is not equal !!!" );

				Nprev=N;
		}
		
		if (NumberOfTestSamples_==0)
        {
            if (Verbose)	cout<<"Number of Test Samples is 0, not applying to test data. "<<endl;
			return;
		}
		cout<<"apply test data end - set test data "<<endl;

       applyToTestData();
		cout<<"end - set test data "<<endl;

		
	}
	void ClassificationTreeNode::setTestData( std::vector< std::vector<float>* > & test_data )
	{
        //delete nitial data
        for ( std::vector< std::vector<float>* >::iterator i = in_test_data.begin(); i != in_test_data.end();++i)
            if (*i != nullptr)	delete *i;
        //delete iunitial chunks
        for ( std::vector< std::vector<unsigned int>* >::iterator i = in_test_chunks_.begin(); i != in_test_chunks_.end();++i)
            if (*i != nullptr)
                delete *i;
        
        if (! in_test_data.empty() )	in_test_data.clear();

        in_test_data = test_data;
        
        if (! in_test_chunks_.empty() )	in_test_chunks_.clear();
  
        
        if (! in_test_data.empty())	NumberOfTestSamples_=in_test_data[0]->size()/Dimensionality_;
        
        NumberOfTestChunks_=0;
	}
    
    
	void ClassificationTreeNode::setTestData( std::vector< std::vector<float>* > & test_data, const unsigned int & Dim )
	{
        
        Dimensionality_=Dim;
        
        //delete data
        for ( std::vector< std::vector<float>* >::iterator i = in_test_data.begin(); i != in_test_data.end();++i)
            if (*i != nullptr)	delete *i;

        //delete initial chunks
        for ( std::vector< std::vector<unsigned int>* >::iterator i = in_test_chunks_.begin(); i != in_test_chunks_.end();++i)
            if (*i != nullptr)	delete *i;
       
        if (! in_test_data.empty() )	in_test_data.clear();

        in_test_data = test_data;
        
        if (! in_test_chunks_.empty() )	in_test_chunks_.clear();
                
        if (! in_test_data.empty())	NumberOfTestSamples_=in_test_data[0]->size()/Dimensionality_;
        
        NumberOfTestChunks_=0;

	}
    
    unsigned int ClassificationTreeNode::calculateNumberOfInChunks()
    {
        set<unsigned int> chunks;
        for (vector<  vector<unsigned int>* >::iterator i = in_test_chunks_.begin(); i != in_test_chunks_.end(); ++i)
            for (vector<unsigned int>::iterator ii = (*i)->begin(); ii != (*i)->end(); ++ii)
                chunks.insert(*ii);

        return chunks.size();
    }

     void ClassificationTreeNode::setTestData( std::vector< std::vector<float>* > & test_data , std::vector< std::vector<unsigned int>* > & test_chunks )
    {
        //delete initial data
        for ( std::vector< std::vector<float>* >::iterator i = in_test_data.begin(); i != in_test_data.end();++i)
            if (*i != nullptr)	delete *i;
        //delete initial chunks
        for ( std::vector< std::vector<unsigned int>* >::iterator i = in_test_chunks_.begin(); i != in_test_chunks_.end();++i)
            if (*i != nullptr)	delete *i;
        
        if (! in_test_data.empty() )	in_test_data.clear();

		in_test_data = test_data;

        if (! in_test_chunks_.empty() )	in_test_chunks_.clear();

        in_test_chunks_ = test_chunks;

        if (! in_test_data.empty())	NumberOfTestSamples_=in_test_data[0]->size()/Dimensionality_;
        
        //need to calculate number of TestChunks
        if (! in_test_chunks_.empty())	NumberOfTestChunks_=calculateNumberOfInChunks();//in_test_data[0]->size()/Dimensionality;

    }
    
     void ClassificationTreeNode::setTestData( std::vector< std::vector<float>* > & test_data, const unsigned int & Dim, std::vector< std::vector<unsigned int>* > & test_chunks )
    {
        Dimensionality_=Dim;
        
        //delete data
        for ( std::vector< std::vector<float>* >::iterator i = in_test_data.begin(); i != in_test_data.end();++i)
            if (*i != nullptr)	delete *i;
        
        //delete initial chunks
        for ( std::vector< std::vector<unsigned int>* >::iterator i = in_test_chunks_.begin(); i != in_test_chunks_.end();++i)
            if (*i != nullptr)	delete *i;

        if (! in_test_data.empty() )	in_test_data.clear();
        
		in_test_data = test_data;
        
        if (! in_test_chunks_.empty() )	in_test_chunks_.clear();

        in_test_chunks_ = test_chunks;

        if (! in_test_data.empty())	NumberOfTestSamples_=in_test_data[0]->size()/Dimensionality_;
        
        //need to calculate number of TestChunks
        if (! in_test_chunks_.empty())	NumberOfTestChunks_=calculateNumberOfInChunks();//in_test_data[0]->size()/Dimensionality_;

    }
    
    
    
    void ClassificationTreeNode::copyInTestData( const std::vector< std::vector<float>* > & test_data )
	{
        for ( std::vector< std::vector<float>* >::iterator i = in_test_data.begin(); i != in_test_data.end();++i)
            if (*i != nullptr)	delete *i;
        
        //delete initial chunks
        for ( std::vector< std::vector<unsigned int>* >::iterator i = in_test_chunks_.begin(); i != in_test_chunks_.end();++i)
            if (*i != nullptr)	delete *i;

        in_test_data.clear();

        for ( std::vector< std::vector<float>* >::const_iterator i = test_data.begin(); i != test_data.end();++i)
        {
            vector<float>* vtemp = new vector<float>((*i)->begin(),(*i)->end());
            in_test_data.push_back( vtemp );     
		}
        
        if (! in_test_data.empty())	NumberOfTestSamples_=in_test_data[0]->size()/Dimensionality_;

        //--------------------------------
        if (! in_test_chunks_.empty() )	in_test_chunks_.clear();

        NumberOfTestChunks_=0;
		
	}
    
    
    void ClassificationTreeNode::copyInTestData( const clapack_utils_name::smatrix & test_data )
	{
        for ( std::vector< std::vector<float>* >::iterator i = in_test_data.begin(); i != in_test_data.end();++i) {
            if (*i != nullptr)	delete *i;
        }

        in_test_data.clear();
        
        std::vector<float>* vec = new std::vector<float>(test_data.MRows * test_data.NCols);

        for (__CLPK_integer r = 0 ; r < test_data.MRows ; ++r)
            cblas_scopy(test_data.NCols, test_data.sdata + r, test_data.MRows, &(vec->at(r * test_data.NCols)), 1);
        
        in_test_data.push_back(vec);
        
        if (! in_test_data.empty())	NumberOfTestSamples_ = in_test_data[0]->size()/Dimensionality_;
        
        //--------------------------------
        if (! in_test_chunks_.empty() )	in_test_chunks_.clear();
        
        NumberOfTestChunks_=0;

	}
    
    
	void ClassificationTreeNode::runTrackStats()
	{
		
	}
    std::vector<float> ClassificationTreeNode::getDistanceScores()
	{
		if (disc_distance==nullptr)
			disc_distance = new vector<float>(NumberOfTestSamples_,0);
//        else{
//            disc_distance->clear();
//            disc_distance->resize(NumberOfTestSamples_);
        
//        }		
        return *disc_distance;
	}

    
    //used to pass down feature mask
	void ClassificationTreeNode::setIOoutputSpecific()
	{
		
	}


	void ClassificationTreeNode::setIOoutput()
	{
		if (output_feature_mask_ == nullptr){
			output_feature_mask_ = new vector<bool>();
		}else {
			output_feature_mask_->clear();
		}
        
        if (src_image_dims_ == nullptr){
			src_image_dims_ = new vector<uint3>();
        }else {
			src_image_dims_->clear();
		}
        if (src_image_res_ == nullptr){
			src_image_res_ = new vector<float4>();
        }else {
			src_image_res_->clear();
		}
        if (src_names_ == nullptr){
			src_names_ = new vector<string>();
        }else {
			src_names_->clear();
		}
        if (src_dims_ == nullptr){
			src_dims_ = new vector<unsigned int>();
        }else {
			src_dims_->clear();
        }

        for ( vector<  ClassificationTreeNode* >::iterator i_inputs = node_inputs.begin(); i_inputs != node_inputs.end(); i_inputs++)
        {
        	output_feature_mask_->insert(output_feature_mask_->end(), (*i_inputs)->outFM_begin() , (*i_inputs)->outFM_end() );
        	src_image_dims_->insert(src_image_dims_->end(), (*i_inputs)->outSRC_imdims_begin() , (*i_inputs)->outSRC_imdims_end() );
        	src_image_res_->insert(src_image_res_->end(), (*i_inputs)->outSRCres_begin() , (*i_inputs)->outSRCres_end() );
        	src_names_->insert(src_names_->end(), (*i_inputs)->outSRCnames_begin() , (*i_inputs)->outSRCnames_end() );
        	src_dims_->insert(src_dims_->end(), (*i_inputs)->outSRCdims_begin() , (*i_inputs)->outSRCdims_end() );

        }

        //-------for images can only pass down image dimensions if there is one input---------//
        //		if (node_inputs.size()==1)
        //		{
        //			stats_image_res = node_inputs[0]->stats_image_res;
        //			stats_image_dims = node_inputs[0]->stats_image_dims;
        //
        //		}

        //if you want to track stats write the output
		//if (trackStats)
		//{
		
		//}
		int N=0;
        //counts non-zero voxels
        for ( vector<bool>::iterator i = output_feature_mask_->begin(); i != output_feature_mask_->end();++i)
			N+=*i;
	}
	
	
	void ClassificationTreeNode::estimateParameters()
	{
	}
	
	vector<float> ClassificationTreeNode::getParameters()
	{
		//vector<float> dummy;
		return vector<float>();
	}
	void ClassificationTreeNode::getParameters( std::vector< std::list<float> > & all_params)
	{
//		vector<unsigned int>  dummy;
//		getParameters(all_params,  dummy );
	}
//	void ClassificationTreeNode::getParameters( std::vector< std::list<float> > & all_params , const std::vector< unsigned int > & selection_mask )
//	{
//		
//	}

	string ClassificationTreeNode::getParameterFileName(){ return "" ; }
	void ClassificationTreeNode::saveParameters( ){}
	void ClassificationTreeNode::setParameters( const vector<float> & params ){}
	void ClassificationTreeNode::setParameters( const std::vector<float> & params, const std::vector<unsigned int> & mask ){ }

	unsigned int ClassificationTreeNode::getNumberOfParameters()
	{
		return 0;
	}

    vector<string> ClassificationTreeNode::getOutputNames()
    {
        vector<string> v_names;
        for (vector<string>::iterator i = out_data_names_->begin(); i!=out_data_names_->end();i++)
			v_names.push_back(*i);
        return v_names;
    }
    
	template<class T>
	vector<T> ClassificationTreeNode::getOutputData()
	{
        
		vector<T> v_out;
		for (vector<float>::iterator i = out_data->begin(); i!=out_data->end();i++)
			v_out.push_back(static_cast<T> (*i));
        
		return v_out;
	}
	template vector<int> ClassificationTreeNode::getOutputData<int>();
	template vector<float> ClassificationTreeNode::getOutputData<float>();
	template vector<unsigned int> ClassificationTreeNode::getOutputData<unsigned int>();
    
    template<class T>
    vector<T> ClassificationTreeNode::getClusterData()
    {
        
        vector<T> v_out;
        for (auto i = out_clusters_->begin(); i!=out_clusters_->end();i++)
            v_out.push_back(static_cast<T> (*i));
        
        return v_out;
    }
    template vector<int> ClassificationTreeNode::getClusterData<int>();
    template vector<float> ClassificationTreeNode::getClusterData<float>();
    template vector<unsigned int> ClassificationTreeNode::getClusterData<unsigned int>();
    
    
    
    
	template<class T>
	vector<T> ClassificationTreeNode::getOutputTestData()
	{
        vector<T> v_out;
        if (out_test_data != nullptr)
        {
            for (vector<float>::iterator i = out_test_data->begin(); i!=out_test_data->end();i++)
                v_out.push_back(static_cast<T> (*i));
        }
        return v_out;
    }
    template vector<int> ClassificationTreeNode::getOutputTestData<int>();
    template vector<float> ClassificationTreeNode::getOutputTestData<float>();
	template vector<unsigned int> ClassificationTreeNode::getOutputTestData<unsigned int>();
	
	    
    
	void ClassificationTreeNode::clearOutData()
	{
		if (out_data == nullptr) {
			out_data = new	vector<float>();
		}else {
			out_data->clear();
		}
        
        if (out_data_names_ == nullptr) {
			out_data_names_ = new	vector<string>();
		}else {
			out_data_names_->clear();
		}
		
		if (out_test_data == nullptr) {
			out_test_data = new	vector<float>();
		}else {
			out_test_data->clear();
		}
        
        if (out_chunks_ == nullptr) {
			out_chunks_ = new	vector<unsigned int>();
		}else {
			out_chunks_->clear();
		}
		
		if (out_test_chunks_ == nullptr) {
			out_test_chunks_ = new	vector<unsigned int>();
		}else {
			out_test_chunks_->clear();
		}
		
	}
    
	void ClassificationTreeNode::clearInTestData()
	{
        in_test_data.clear();
		in_test_chunks_.clear();
        in_data_names_.clear();
        
	}
	
	TreeNodeType ClassificationTreeNode::getTreeNodeType(){
		return	nodeType;
	}
    TreeNodeType ClassificationTreeNode::getTreeNodeTypeSpec(){
		return	nodeTypeSpec;
	}
    
    
    float ClassificationTreeNode::getAccuracy( const std::vector<float> & estimates ) 
    {
        
        unsigned int N = estimates.size();
        float sum=0;
        
        unsigned int index=0;
        for (vector<float>::const_iterator i_e  = estimates.begin(); i_e != estimates.end(); ++i_e,++index)
            sum += (training_target->getInTarget(index) == *i_e) ? 1 : 0; 
        
        return sum / N;
    }
    
    
    void ClassificationTreeNode::incOutputConnections()
    {
        ++_output_connections;
    }

	void ClassificationTreeNode::addInput( ClassificationTreeNode* input)
	{
			if ( nodeType == SOURCE )
			{
				throw ClassificationTreeNodeException("Tried to add an input to a DataSource");
			}
		node_inputs.push_back(input);
        input->incOutputConnections();
	}
	void ClassificationTreeNode::deleteInput( ClassificationTreeNode* input){

		bool found_input=false;

		std::vector<unsigned int>::iterator  i_v_dims = v_dims_.begin();
		std::vector< std::vector<float>* >::iterator i_in_data = in_data.begin();
		vector< vector<float>* >::iterator i_in_test_data = in_test_data.begin();
        std::vector< std::vector<std::string>*  >::iterator i_in_names =  in_data_names_.begin();
        std::vector< std::vector<unsigned int >* >::iterator i_in_chunks = in_chunks_.begin();
        std::vector< std::vector<unsigned int >* >::iterator i_in_clusters = in_clusters_.begin();

		for (vector< ClassificationTreeNode* >::iterator i = node_inputs.begin(); i != node_inputs.end(); ++i,++i_in_data,++i_in_test_data,++i_v_dims,\
                                                                                                            ++i_in_names,++i_in_chunks,++i_in_clusters)
			if ( (*i) == input )
			{
				//ClassificationTreeNode* temp = *i;
				found_input=true;
				node_inputs.erase(i);
				in_data.erase(i_in_data);
				in_test_data.erase(i_in_test_data);
                in_data_names_.erase(i_in_names);
                in_chunks_.erase(i_in_chunks);
                in_clusters_.erase(i_in_clusters);
				--NumberOfSources_;
				NumberOfSamples_-=(*i)->getNumberOfSamples();
                NumberOfChunks_ -= (*i)->getNumberOfChunks();

                v_dims_.erase(i_v_dims);
                
                delete (*i);

				--i;
			}
	}

	void ClassificationTreeNode::setName(const std::string & name )
	{
		NodeName = name;
	}

	
	void ClassificationTreeNode::setInputData()
	{
        if (Verbose>2)	cout<<"ClassificationTreeNode::setInputData - "<<NodeName<<" - "<<Dimensionality_<<endl;
        
		if (node_inputs.empty())	throw ClassificationTreeNodeException("ClassificationTreeNode::setInputData : Trying to set input, but no input nodes connnected" );

		v_dims_.clear();
		in_data.clear();
        in_data_names_.clear();
		Dimensionality_=0;
        DimTrim=0;
		int count=0;
		unsigned int Nprev=0;

		//Pass all the pointers from the output of each input
		for (vector< ClassificationTreeNode* >::iterator i = node_inputs.begin() ; i != node_inputs.end(); i++, count++)
		{
			if ((*i)->hasTrainingData())//check to see if there is any input data
			{
                if ((*i)->getDataPointer() == nullptr)
                {                
                    throw ClassificationTreeNodeException(("ClassificationTreeNode::setInputData - " + NodeName + "Input Data Pointer is nullptr ").c_str());
                }
                
                in_data.push_back((*i)->getDataPointer());

                (*i)->getNamesPointer();

                in_data_names_.push_back((*i)->getNamesPointer());

                in_chunks_.push_back((*i)->getChunksPointer());

                in_clusters_.push_back((*i)->getChunksPointer());

				Dimensionality_+=(*i)->getDimensionality();
                
				v_dims_.push_back((*i)->getDimensionality());
				
				unsigned int N = (*i)->getNumberOfSamples();
				if ((count != 0) && (N!=Nprev)){
					throw ClassificationTreeNodeException("ClassificationTreeNode::setInputData : Number of samples in each source is not equal !!!" );
				}
				Nprev=N;
			}else{
                throw ClassificationTreeNodeException("Tried to set input, but no trianing data");
            }
		}

		NumberOfSamples_ = node_inputs[0]->getNumberOfSamples(); ///assumes all input have same number of samples, although not explicit check at tthe moment
        NumberOfChunks_ = node_inputs[0]->getNumberOfChunks();

        if (Dimensionality_==0)
            throw ClassificationTreeNodeException("ClassificationTreeNode::setInputData :Dimemsionality is equal to 0 !!!" );

        DELETED_INPUT_DATA=false;

	}


	void ClassificationTreeNode::runNode( bool with_estimation )
	{
		if (Verbose >=1 )	cout<<"Node name "<<NodeName<<endl;

		for (vector< ClassificationTreeNode* >::iterator i = node_inputs.begin() ; i != node_inputs.end(); i++)
		{

			if (Verbose)	cout<<"Running input "<<(*i)->nodeName()<<endl;

			if (PassDownOutput)
				(*i)->enablePassDownOutput();//tells input to pass down output information

			(*i)->runNode();

		}	

		if (Verbose >= 2)	cout<<"set target "<<endl;

		setTarget();

		if (Verbose)	cout<<"set input data "<<endl;

		setInputData();

		if (Verbose >= 2)	cout<<"done."<<endl<<"Set test data... ";//<<endl;

		setTestData();

		if (Verbose >= 2)	cout<<"done."<<endl;


		if ( PassDownOutput )
		{
			if (Verbose)	cout<<"pass down treenode "<<PassDownOutput<<endl;

			//need this first to reset the feature mask
			setIOoutput();

			//run node specific manipoulation of mask
			setIOoutputSpecific();

		}

		deleteInputsOutputData();

	}
    
    void ClassificationTreeNode::deleteOutData()
    {
        if (Verbose)
            cerr<<"deleteOutData "<<NodeName<<endl;

        --_output_connections;//needs to ensure all have used it up
        if (_output_connections==0)
        {            
            if (out_data != nullptr)
            {
                delete out_data;
                out_data=nullptr;
            }

            if (out_data_names_ != nullptr) {
                delete out_data_names_;
                out_data_names_=nullptr;
            }

            if (out_test_data != nullptr) {
                delete out_test_data;
                out_test_data=nullptr;
            }

            if (out_test_chunks_ != nullptr) {
                delete out_test_chunks_;
                out_test_chunks_=nullptr;
            }

            if (out_test_clusters_ != nullptr) {
            	delete out_test_clusters_;
            	out_test_clusters_=nullptr;
            }


            //dont remove target if its a source, it loads the target
            if (( training_target != nullptr) && ( nodeType!=SOURCE)){
                delete training_target;
                training_target=nullptr;
            }
        }
    }
  

	void ClassificationTreeNode::deleteInputsOutputData() 
    {
        if (!DELETED_INPUT_DATA)//protects from calling this twice, reset in setInput
        {
         //   cout<<"Delete inputs output data"<<endl;
            for (vector< ClassificationTreeNode* >::iterator i = node_inputs.begin() ; i != node_inputs.end(); i++)
            {
                cout<<"delete node outdata"<<endl;
                (*i)->deleteOutData();
                cout<<"done delete node"<<endl;
            }
            DELETED_INPUT_DATA=true;
        }
    }

	void ClassificationTreeNode::writeStats() const
	{
        cout<<"write stats tree node "<<NodeName<<" "<<output_basename<<endl;
        
        vector<uint3>::const_iterator i_im_dims = src_image_dims_->begin();
        vector<float4>::const_iterator i_im_res = src_image_res_->begin();
        vector<string>::const_iterator i_names = src_names_->begin();
        vector<unsigned int>::const_iterator   i_dims = src_dims_->begin();
//        for (vector<unsigned int>::const_iterator i_dims = src_dims_->begin(); i_dims != src_dims_->end(); ++i_dims,++i_im_res, ++i_im_dims,++i_names)
//        {
//            cout<<"input info "<<*i_names<<" "<<*i_dims<<" "<<i_im_res->x<<" "<<i_im_res->y<<" "<<i_im_res->z<<" "<<i_im_dims->x<<" "<<i_im_dims->y<<" "<<i_im_dims->z<<endl;
//            
//        }
		///check to see if image exists
//		if ((stats_image_res.x) || (stats_image_res.y) ||(stats_image_res.z) || \
//			(stats_image_dims.x) || (stats_image_dims.y) || (stats_image_dims.z))
//		{
//			if (output_feature_mask_->size() != (stats_image_dims.x*stats_image_dims.y*stats_image_dims.z))
//				throw ClassificationTreeNodeException("Tried to write stats image, but image dimensions do not match dimensionality");
//			
//				vector<float> mean(stats_image_dims.x*stats_image_dims.y*stats_image_dims.z,0);
//				vector<float> variance(stats_image_dims.x*stats_image_dims.y*stats_image_dims.z,0);
//				
//				
//				vector<float>::iterator i_var = variance.begin();
//				vector<float>::iterator i_mean = mean.begin();
//				vector<float>::iterator i_x = stats_sum_x->begin();
//                vector<float>::iterator i_xx = stats_sum_xx->begin();
//                vector<unsigned int>::iterator i_N = stats_sum_N->begin();
//        //   long unsigned int count=0;
//				for (vector<bool>::iterator i_mask = output_feature_mask_->begin(); i_mask != output_feature_mask_->end(); ++i_mean, ++i_var, ++i_mask,++i_N,++i_x, ++i_xx)
//				{
//                   // cout<<"count "<<count<<" "<<*i_mask<<endl;
//                 //   ++count;
//				//	if (*i_mask)
//				//	{
//                    //    if (*i_N==0){//set to zero region where no samples
//                      //      *i_mean = 0;
//                     //       *i_var = 0;
//                          // ++i_x; 
//                           // ++i_xx;
//                       // }else{
//                    *i_mean = *i_x / N_stats; 
//                    *i_var = ( *i_xx - (*i_x)*(*i_x)/N_stats ) / (N_stats - 1);
//
//                 //   count+=*i_x;
//                 //       cout<<"mean "<<*i_x<<" "<<N_stats<<" "<<stats_sum_x->size()<<endl;
//                        //					*i_mean = *i_x / N_stats;
//	//					*i_var = ( *i_xx - (*i_x)*(*i_x)/N_stats ) / (N_stats - 1);
//					//	++i_x; 
//					//	++i_xx;
//                        //}
//				//	}
//				}
//			//cout<<"Sum "<<count<<endl;
//			nifti_Writer::write( mean, stats_image_dims, stats_image_res,output_basename+"_stats_mean") ;
//			nifti_Writer::write( variance, stats_image_dims, stats_image_res,output_basename+"_stats_var") ;
//			//nifti_Writer::write( variance, stats_image_dims, stats_image_res,output_basename+"_stats_N") ;
//
//			//static void write(  const std::vector<float> * data, const std::vector<bool> * mask, const uint3 & dims, const float3 & res );
//
//			//cout<<"write NIFTI output "<<NodeName<<" "<<N_stats<<endl;
//		}else {
//            cout<<"write track stats"<<endl;
//            vector<float> mean(output_feature_mask_->size(),0);
//            vector<float> variance(output_feature_mask_->size(),0);
//            
//            
//            vector<float>::iterator i_var = variance.begin();
//            vector<float>::iterator i_mean = mean.begin();
//            vector<float>::iterator i_x = stats_sum_x->begin();
//            vector<float>::iterator i_xx = stats_sum_xx->begin();
//            vector<unsigned int>::iterator i_N = stats_sum_N->begin();
//            //   long unsigned int count=0;
//            for (vector<bool>::iterator i_mask = output_feature_mask_->begin(); i_mask != output_feature_mask_->end(); ++i_mean, ++i_var, ++i_mask,++i_N,++i_x, ++i_xx)
//            {
//                *i_mean = *i_x / N_stats; 
//                *i_var = ( *i_xx - (*i_x)*(*i_x)/N_stats ) / (N_stats - 1);
//            }
//            
//            ofstream f_mean_var((output_basename+"_stats_mean_var.csv").c_str()); 
//            vector<float>::iterator i_m = mean.begin();
//            f_mean_var<<*i_m;
//            for ( ;i_m != mean.end();++i_m)
//            {
//                f_mean_var<<" "<<*i_m;
//            }
//            f_mean_var<<endl;
//            vector<float>::iterator i_v = variance.begin();
//            f_mean_var<<*i_v;
//            for ( ;i_v != variance.end();++i_v)
//            {
//                f_mean_var<<" "<<*i_v;
//            }
//            f_mean_var<<endl;
//            
//            f_mean_var.close();
//
//            
//            
//
//        }
		
		
		
	}

	void ClassificationTreeNode::writeNodeOutputs()
	{
		//for (vector< ClassificationTreeNode* >::iterator i = v_writers.begin() ; i != v_writers.end(); i++)	
		//	(*i)->write();
		cout<<"writeNodeOutputs trackstats "<<trackStats<<endl;
		if (trackStats)
			writeStats();
	}
	unsigned int ClassificationTreeNode::getDimensionality() const 
	{
		return Dimensionality_;
	}
	
	unsigned int ClassificationTreeNode::getNumberOfSamples() 
	{
		return NumberOfSamples_;
	}
    unsigned int ClassificationTreeNode::getNumberOfChunks()
	{
		return NumberOfChunks_;
	}
    unsigned int ClassificationTreeNode::getNumberOfTestChunks()
	{
		return NumberOfTestChunks_;
	}
    unsigned int ClassificationTreeNode::getNumberOfInputSamples()
    {
        
        if (in_data.empty())
            return 0;
        
		return  in_data[0]->size()/v_dims_[0];
    }

	unsigned int ClassificationTreeNode::getNumberOfTestSamples() 
	{
		return NumberOfTestSamples_;
	}
	unsigned int ClassificationTreeNode::getNumberOfClasses() const
	{
		return Kclasses_;
	}
	
	string ClassificationTreeNode::nodeName()
	{
		return NodeName;
	}

	string ClassificationTreeNode::nodeTypeName()
	{
		string name;
		switch (nodeType)
		{
			case 0:
				name="UNDEFINED";
				break;
			case 1:
				name="SOURCE";
				break;
			case 2:
				name="FILTER";
				break;
			case 3:
				name="DISCRIMINANT";
				break;
         
			default:
				name="Invalid Node Type";
				
		}
		
		return name;
	}

    std::vector<string>*  ClassificationTreeNode::getNamesPointer()
    {
        //cerr<<"ClassificationTreeNode::getNamesPointer "<<out_data_names_->size()<<endl;
        return out_data_names_;
    }

	vector<float>* ClassificationTreeNode::getDataPointer()
	{
//        cerr<<"get data pointer "<<endl;
		return out_data;
	}
	vector<float>* ClassificationTreeNode::getTestDataPointer() 
	{
		
		return out_test_data;
	}
    vector<unsigned int>* ClassificationTreeNode::getTestChunksPointer()
    {
        return out_test_chunks_;
    }
    
	Target* ClassificationTreeNode::getTargetPointer()
	{
        //cout<<"ClassificationTreeNode::getTargetPointer-2"<<endl;
		return training_target;
	}
	
	
}
