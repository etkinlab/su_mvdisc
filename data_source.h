#ifndef data_source_H
#define data_source_H

/*
 *  data_source.h
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 7/20/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "ClassificationTreeNode.h"
#include <iostream>
#include <vector>
#include <smatrix.h>

namespace su_mvdisc_name{
	
	
	class DataSource : public ClassificationTreeNode
	{
	public:
		DataSource();
		virtual ~DataSource();
		
		virtual void readFile();
//		void setTarget( const std::vector<int> & target);
	
		void setInputData();
        void deleteInput( bool setDimensionalityToZero=true );
        void addInputData( const clapack_utils_name::smatrix& d );
        void setInputData( const clapack_utils_name::smatrix& d );

        void setInputData( std::vector<float> * in_d, const unsigned int & Dim, const unsigned int & Ns );

		void addInputData( std::vector<float> * in_d, const unsigned int & Dim, const unsigned int & Ns );
		void setIOoutput();
		//void runNode();
		void setFileName( const std::string & s );
        void setFileNames( const std::vector< std::string > & s );

		void setTarget( Target* target_in );
        void setTargetData( const std::vector<float> & targ_data );
		void setTarget( );
		void setTestData();
		void setTestData( std::vector<float>*  t_data, const unsigned int & Ns);//shallow copy, just pointing the pointer
        void setTestData( const std::vector<float> &  t_data, const unsigned int & Ns);
		void setFeatureNames( const std::vector<std::string> & v_names );

	//	void copyInTestData( const std::vector< std::vector<float>* >  & t_data, const unsigned int & N );
		void copyInData_to_TestData();
        void copyInData_to_OutData();

		//void applyToTestData();

		//void recursivelySetTarget();

		unsigned int getNumberOfSources() const;
		unsigned int getNumberOfSamples();
       unsigned int getNumberOfInputSamples();

		//unsigned int getNumberOfInputSamples() const { return NumberOfInputSamples; } 

		
		std::vector<int> applySelectionMask( const std::vector<int> & selection_mask, const std::vector<int> & selection_test_mask);
	//	std::vector<float> getSample( const std::vector<unsigned int> & selection_mask, const unsigned int index );
		
		void displayTrainingData();
		void displayTestData();

		
		
	protected:
        void readFeatureNames( const std::string & fnames);
	//	std::vector< unsigned int > v_Dimensionality;
		//unsigned int NumberOfSamples_Orig;
		std::string filename;
        std::vector< std::string > v_filenames;
		bool FileHasBeenRead,multiFileHasBeenSet, hasFileNameBeenSet, hasDataBeenSet;

		//unsigned int Nsamples,Mdims,Kclasses; 
		//unsigned int NumberOfInputSamples;
	
	//	std::vector< std::vector<float> > data;
		
	
	};
	
}
#endif
