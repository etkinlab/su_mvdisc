//
//  gaussian_process.cpp
//  su_mvdisc
//
//  Created by Brian Patenaude on 3/16/12.
//  Copyright (c) 2012 Stanford University - Etkin Lab. All rights reserved.
//


//STL includes
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <exception>

//su_mvdisc includes
#include <ClassificationTree.h>
#include <ClassificationTreeNodeException.h>
#include <cluster_filter.h>
//#include <data_source.h>
#include <target.h>
#include <misc_utils.h>
#include <TextFileSource.h>
#include <image_source.h>
//#include <newimage/newimageall.h>
#include <smatrix.h>
//BOOST includes
#include <boost/filesystem.hpp>

//GLUT / OPENGL includes
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
//#include <scatterplot.h>


//#include <atlas_utils/atlas_utils.h>
using namespace std;
//using namespace NEWIMAGE;
using namespace misc_utils_name;
//using namespace MISCMATHS;
using namespace clapack_utils_name;
using namespace su_mvdisc_name;
//using namespace su_mvdisc_gui_name;
namespace fs = boost::filesystem;

//class myexception: public exception
//{
//public:
//    myexception(const char* msg){
//        emsg=string(msg);
//    }
//    const char* what() const throw()
//    {
//        return emsg.c_str();
//    }
//    string emsg;
//};
//need glopal for glut access
//vector<float2> DATA_MEANS;



void usage(){
    
    cout<<"\n Usage: \n \n clustering  [options] <data.txt> \n "<<endl;
    cout<<"         --options:  \n "<<endl;
    cout<<"              -init <Forgy,RandomPartitions> : Initialization method. Default = Forgy. "<<endl;
    cout<<"              -test <filename> : Data file containing hold-out data to apply the model to. Optional. "<<endl;
    cout<<"              -K <number of classes>  : Number of classes to partition the data into. Default = 2."<<endl;
    cout<<"              -R <number of repitions> : Number of times K-means will be run. Default = 100."<<endl;
    cout<<"              -show : Show results as plot."<<endl;

    cout<<endl;

    
}



void renderScene( ) {
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glBegin(GL_TRIANGLES);
    glVertex3f(-0.5,-0.5,0.0);
    glVertex3f(0.5,0.0,0.0);
    glVertex3f(0.0,0.5,0.0);
    glEnd();
//    vector<float2> data_pts(3);
//    data_pts[0]=float2(0,0);
//    data_pts[1] = float2(1,0.5);
//    data_pts[2] = float2(0.5,0.5);
    
//    
//    su_mvdisc_gui_name::scatterPlot* sc_p = new su_mvdisc_gui_name::scatterPlot();
//   sc_p->setWindow(-3,3,-3,3);
//    sc_p->initializeGL();
//    sc_p->setDataPoints(data_pts,3);
//
//    sc_p->setTickSpacing(0.1);
//    sc_p->setTitle("K-means");
//    sc_p->setPlotBounds(-2,2,-2,2);
//    sc_p->setAxesLimits(su_mvdisc_gui_name::float4(-1,1,-1,1));
//    sc_p->paintGL();
    glutSwapBuffers();
//    delete sc_p;
}
void processNormalKeys(unsigned char key, int x, int y) {
    
    if ((key == 27) || (key == 'e'))
        exit(0);
}


int main( int argc, char * argv[])
{
    //Check to see if there is at least 1 argument
    //If not, give usage and exit
    if ( argc <=  1 ){
        cerr<<"\n  ***Not enough input arguments***"<<endl;
        usage();
        exit (EXIT_FAILURE);
        
    }
    
    
    //Set up default options
    INIT_METHOD init_method = FORGY;
    //Input data to apply the k-means to
    fs::path in_test_data("");
    fs::path output("");

    //Number of Classes to partitions subjects into.
    unsigned int K = 2 ;

    //Number of repititions of the k-means algorithm used to find a stable global solution.
    unsigned int R = 100 ;

    //If set, uses GLUT to display the clustering results
    bool doVisualization =false;
    //N is number of lines (i.e. dimensionaility)
    int arg_ind=1;
    
    //Read in options, but only increment arg_ind if a valid option is specified
    bool valid_option;
    do
    {
        //valid option is used to break out of the options loop
        valid_option = false;
        string option = argv[arg_ind];
        if ( option == "-init" )
        {  //Specify initialization method. Forgy (default) uses randomly selected samples as mean.
            //Random Partitions partitions the data into classes randomly and calculate the means.
            ++arg_ind;
            if ( string(argv[arg_ind++]) == "RandomPartitions" )
            {
                init_method = RNDM_PART;
            }
            valid_option=true;
            
        }else if (option == "-test")
        { //Get name of file of data on which to apply the k-means to.
            ++arg_ind;
            
            //Read filename for test data
            in_test_data = fs::path(argv[arg_ind++]);
            //Test for existence of the input file.
            if ( ! fs::exists(in_test_data) )
            {
                cerr<<"Input file does not exist : "<<in_test_data.string()<<endl;
                exit (EXIT_FAILURE);
            }
            
            valid_option=true;
            
        }else if (option == "-o")
        { //Get name of file to write output data.
            ++arg_ind;

            //Read filename for test data
            output = fs::path(argv[arg_ind++]);

            valid_option=true;

        }else if (option == "-K")
        { //Get name of file of data on which to apply the k-means to.
            ++arg_ind;
           
            K=atoi(argv[arg_ind++]);
           
            valid_option=true;
            
        }else if (option == "-R")
        { //Get name of file of data on which to apply the k-means to.
            ++arg_ind;
            
            R=atoi(argv[arg_ind++]);
            
            valid_option=true;
            
        }else if (option == "-show")
        { //Get name of file of data on which to apply the k-means to.
            doVisualization=1;
            valid_option=true;

        }else if ( option == "--help" )
        {
            //output usage if --help is specified
            usage();
            exit (EXIT_FAILURE);
        }
    }while(valid_option);//set valid option to true, otherwise move on to reading input
    
    


    //Read input data name from the command line
    fs::path in_data(argv[arg_ind++]);
    
    //Test for existence of the input file.
    if ( ! fs::exists(in_data) )
    {
        cerr<<"Input file does not exist : "<<in_data.string()<<endl;
        exit (EXIT_FAILURE);
    }
    
    //Display the input file specified.
    cout<<"In data : "<<in_data.string()<<endl;
    
    
//    vector<float>* data = new vector<float>();
    vector<float>* test_data = new vector<float>();
    
//    //read in data
//    ifstream fdata(in_data.c_str());
//    string stemp;
//    int Ns=0;
//    
//    while ( getline(fdata,stemp) )
//    {
//        vector<float> vftemp = csv_string2nums_f(stemp);
//        data->insert(data->end(),vftemp.begin(),vftemp.end());
//        ++Ns;
//        
//    }
    
//    fdata.close();
    
//    unsigned int Ndim = static_cast<unsigned int>(data->size()/Ns);
//    cout<<"Ndim : "<<Ndim<<" Ns : "<<Ns<<endl;
    
    //read in test data
    if (! in_test_data.empty())//check to see if test data is specified
    {
        float ftemp;
        ifstream fdata_test(in_test_data.c_str());
        string stemp;
        while ( getline(fdata_test,stemp) )
        {
            vector<float> vftemp = csv_string2nums_f(stemp);
            test_data->insert(test_data->end(),vftemp.begin(),vftemp.end());
        }
        
        fdata_test.close();
    }
    
    
    ///dimensionality oif test data and input need be the same
//    int Ns_test = static_cast<int>(test_data->size() / Ndim);
    cout<<"Instantiating discriminant..."<<endl;
    
//    ClassificationTreeNodeException c = ClassificationTreeNodeException("lalalallaa");

    // node_targ->setTargetData(target);
    try {
        
        //check for valid file extension. .txt, .csv, .ssv, .nii, .nii.gz
        if (!( ( in_data.extension().string() == ".csv" ) ||  ( in_data.extension().string() == ".txt" ) || ( in_data.extension().string() == ".ssv" ) || ( in_data.extension().string() == ".nii" ) || ( ( in_data.stem().extension().string() + in_data.extension().string()) == ".nii.gz" ) ) )
        {
            throw ClassificationTreeNodeException("Invalid file type/extension");
        }

        //Create DataSource. As inidicated by the name, this object is used to pass data
        //to the processing nodes.
        DataSource* data_src=NULL;
        if (( in_data.extension().string() == ".nii" ) || ( ( in_data.stem().extension().string() + in_data.extension().string()) == ".nii.gz" ))
        {
            data_src = new ImageSource();
            
        }else{
            data_src = new TextFileSource();
            if (in_data.extension().string() == "txt")
                static_cast<TextFileSource*>(data_src)->setFileType("ssv");
            else
                static_cast<TextFileSource*>(data_src)->setFileType(in_data.extension().string());

            
        }
        data_src->setVerbose(false);  //turn off verboseness.
        data_src->setFileName(in_data.string());
               data_src->setInputData();
        
//        data_src->addInputData( data,Ndim, Ns ); //Add in data in vector format with dimensionality and number of samples.
        
        //Need to copy the "InData" to the "OutData" location. This is needed for when not using cross-validation.
        //In the cross-validation context, the sampling would copy the InData to OutData. In this manner the original
        //data is always preserved.
        data_src->copyInData_to_OutData();
        
        //Target is the class labels used for supervised learning. The code will throw errors if this is not present.
        //In the context of clustering, this node will set the target labels. Any downstream nodes, could use these labels
        //for a supervised learning approach.
        Target * node_targ = new Target();
        data_src->setTarget(node_targ);
        
        //Sets a hold out data set to apply the estimated model too.
        //It only does this if a test data set is passed into the program.
        //Under Cross-Validation conditions, the left-out samples are copied into Test Data.
        if ( ! test_data->empty())
            data_src->setTestData(test_data,data_src->getNumberOfSamples());
        else
            delete test_data;
        
        
        cout<<"Run Clustering... "<<endl;
        //Setup the clustering node.
        ClusterFilter* clust = new ClusterFilter();
        
        //Connect the data source to the clustering node.
        clust->addInput(data_src);

        //Set the clustering options. If an option was not provided, the default as specified at the begining of main
        //will be used.

        clust->setVerbose(false);
        clust->setInitMethod(init_method);
        clust->setK(K);
        clust->setRepititions(R);

        //Run the clustering algorithm.
        clust->runNode();
        smatrix means = clust->getClusterMeans();

        
       //Output the input data and clusters into a single file
        //This will be particularly helpful for debugging
        ofstream fout(output.c_str());
        if ( fout.is_open())
        {
        	cout<<"Writing output... "<<endl;
        	data_src->copyInData_to_OutData();
        	cout<<data_src->getDataPointer()->size()<<endl;

        	vector<float>::const_iterator i_src  = data_src->getDataPointer()->begin();
//        	for (vector<float>::const_iterator i_cluster = clust->getDataPointer()->begin(); i_cluster != clust->getDataPointer()->end(); ++i_cluster,++i_src)
        	for (vector<unsigned int>::const_iterator i_cluster = clust->getClustersPointer()->begin(); i_cluster != clust->getClustersPointer()->end(); ++i_cluster,++i_src)
        	{
        		fout<<(*i_cluster)<<endl;
//        		fout<<(*i_src)<<" "<<(*i_cluster)<<endl;
    		}

        		//        DATA_MEANS.push_back(float2(0.5,0.5));
        	fout.close();
        }
        smatrix_print(means,"means");
        delete clust;
        delete data_src;
        //data source delete should take car eof vector
        //Provide visualization of the results
        if (doVisualization)
        {
        	glutInit(&argc, argv);
        	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
        	glutInitWindowPosition(100,100);
        	glutInitWindowSize(320,320);
        	glutCreateWindow("Clustering");
        	// register callbacks
        	glutDisplayFunc(renderScene);
        	glutKeyboardFunc(processNormalKeys);
        	glutMainLoop();
        }
    } catch(ClassificationTreeNodeException& e) {
        cerr << endl <<"ClassificationTreeNodeException : "<<e.what() << endl;
        exit(EXIT_FAILURE);
    }catch(ClassificationTreeException& e) {
        cerr << endl <<"ClassificationTreeException : "<< string(e.what()) << endl;
        exit(EXIT_FAILURE);
    }catch(std::exception &e) {
        cerr <<"Exception : "<< e.what() << endl;
        exit (EXIT_FAILURE);
    } 
    
    
    return 0;
}



