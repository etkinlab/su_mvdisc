//
//  mask_filter.h
//  su_mvdisc
//
//  Created by Brian Patenaude on 5/27/11.
//  Copyright 2011 Stanford University - Etkin Lab. All rights reserved.
//

#include <base_image_filter.h>

#include <vector>
namespace su_mvdisc_name{
	
	//WS = within sample
	//AS = across sample
	//enum NORM_MODE { VAR_NORM_WS,MEAN_VAR_NORM_WS,MINMAX_WS, MEAN_NORM_WS,NUMBER_OF_WS,VAR_NORM_AS,MEAN_VAR_NORM_AS,MEAN_NORM_AS, MINMAX_AS  };
	
	
	class Mask_Filter : public base_image_filter
	{
	public:
		Mask_Filter();
		virtual ~Mask_Filter();
		void setOptions( std::stringstream & ss_options);
		void applyToTestData();
		void estimateParameters();
	//	std::vector<float> getParameters();
	//	void getParameters( std::vector< std::list<float> > & all_params);
	//	void setParameters( const std::vector< float > & params);
	//	void setParameters( const std::vector<float> & params, const std::vector<unsigned int> & mask );
	//	void setIOoutputSpecific();
        
	//	unsigned int getNumberOfParameters();
        
		void applyParametersToTrainingData();
        
		//virtual void writeStats();
        
        //	void runSpecificFilter();
		//		void setNormMode( NORM_MODE mode );
		//	void setNormMode( const std::string & mode );
		
	protected:
        //void runTrackStats();
	private:
		enum FILTER_MODE { LABEL_SELECT, THRESHOLD };

        FILTER_MODE filt_mode;
        bool* mask;
        bool have_readData;
        std::string filename;
		float threshold;
        int label_select;
	

	};
	
}
