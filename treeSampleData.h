#ifndef TreeSampleData_H
#define TreeSampleData_H
/*
 *  mvdisc_structs.h
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 8/11/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
namespace su_mvdisc_name{


struct TreeSampleData{
    TreeSampleData()
    {//default KFold =0 is chosen to comply with default minsubsampling behavior
        Kfold=0;
        acc_thresh=0;
        stratified_labels.assign(2,0);
        stratified_labels[1]=1;
        model_thresh_mode=0;
        subSamplesFilename="";
        writeLevel=0;
        bootstrap=0;

    }
    TreeSampleData( const unsigned int & Kfold_in )
    {
        Kfold=Kfold_in;
        acc_thresh=0;
        stratified_labels.assign(2,0);
        stratified_labels[1]=1;
        model_thresh_mode=0;
        subSamplesFilename="";
        writeLevel=0;
        bootstrap=0;
    }
    TreeSampleData( const unsigned int & Kfold_in, const std::vector<int> & labels_in)
    {
        Kfold=Kfold_in;
        acc_thresh=0;
        stratified_labels=labels_in;
        model_thresh_mode=0;
        subSamplesFilename="";
        writeLevel=0;
        bootstrap=0;


    }
    TreeSampleData( const unsigned int & Kfold_in, const std::vector<int> & labels_in, const float & acc_thresh_in )
    {
        Kfold=Kfold_in;
        acc_thresh=acc_thresh_in;
        stratified_labels=labels_in;
        model_thresh_mode=1;
        subSamplesFilename="";
        writeLevel=0;
        bootstrap=0;

    }
    TreeSampleData( const unsigned int & Kfold_in, const float & acc_thresh_in )
    {
        Kfold=Kfold_in;
        acc_thresh=acc_thresh_in;
        stratified_labels.assign(2,0);
        stratified_labels[1]=1;
        model_thresh_mode=1;
        subSamplesFilename="";
        writeLevel=0;
        bootstrap=0;
    }
    //constant that are set
    unsigned int Kfold;
    float acc_thresh;
    int model_thresh_mode;//0=none,1=set explicitly, 2=use MOG
    int writeLevel;//how many files to write out, added to reduce output from permutations
    std::vector<int> stratified_labels;
    //thinsg rthat are output
    std::vector<TpFpTnFn> genData_;
    std::vector< clapack_utils_name::smatrix> genDataThresh_;

    std::string subSamplesFilename;
    int bootstrap; //0=no bootstrap, 1=bootstrap for each subsample, 2 = bootstrap prior to to each subsample
//    std::string permTargetFilename;

    
    std::vector< std::pair<float3,float3> > AccSensSpec;
    
    std::vector< std::vector<int> > predictions;
    std::vector< std::map<int, std::pair<float, unsigned int> > > class_sensitivities;
    std::vector< std::pair<TpFpTnFn,TpFpTnFn> > v_tp_fp_tn_fn;
    std::vector< std::pair<TpFpTnFn,TpFpTnFn> > v_tp_fp_tn_fn_thresh;
    std::vector< float3 > v_withinGrErr;
    std::list< std::pair<float,unsigned int> > l_accuracy_pergroup;
    std::vector<int4> estimates_train_all;
    std::vector<int3> estimates_all;
    std::vector< std::vector< std::pair<int, float> > > estimated_distance_scores;
    
};
}
#endif
