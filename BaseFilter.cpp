/*
 *  BaseFilter.cpp
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 7/27/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include <misc_utils.h>

#include "BaseFilter.h"
//#include <misc_utils/misc_utils.h>
//#include <misc_utils.h>
/*
#ifdef ARCHLINUX
extern "C" {
#include "cblas.h"
#include "f2c.h"
  //typedef long int __CLPK_integer;                                                                                                                
  //typedef float  __CLPK_real;                                                                                                                     

  typedef integer __CLPK_integer;
  typedef real __CLPK_real;
#include "clapack.h"
}

#endif
*/
#ifdef ARCHDARWIN

#include <Accelerate/Accelerate.h>
#endif

using namespace std;
using namespace misc_utils_name;

namespace su_mvdisc_name{
	
	
	base_filter::base_filter()
	{
		nodeType=FILTER;
		NodeName="Filter";
	//	trackStats=false;
	//	PassDownOutput=false;

        DimTrim=0;
		Dimensionality_=0;
		NumberOfSamples_=0;
		NumberOfTestSamples_=0;
		Kclasses_=0;
        
		N_stats = 0;
	}
	
	base_filter::~base_filter()
	{
#ifdef ARCHDARWIN
			ATLU_DestroyThreadMemory();
#endif
	}
	
	
	void base_filter::runNode( bool with_estimation )
	{

		for (vector< ClassificationTreeNode* >::iterator i = node_inputs.begin() ; i != node_inputs.end(); i++)
		{
			
			if (PassDownOutput)
			{
				(*i)->enablePassDownOutput();//tells input to pass down output information
			}
			
			(*i)->runNode(true);
			
		}
		//    cout<<"done running inputs : base filter "<<endl;
        
        //loop above setups the passdown

		//       cout<<"set target"<<endl;
		setTarget();
      //  cout<<"Dim0.5 "<<NodeName<<" "<<Dimensionality_<<endl;
		//    cout<<"set input data"<<endl;

		setInputData();
		
        //cout<<"Dim1 "<<NodeName<<" "<<Dimensionality_<<endl;
        //cout<<"run specific filter"<<endl;        

        //this is where we need to replicate the node before the specific filter, can't let it override
        
        
		runSpecificFilter(with_estimation);
		//        cout<<"done run specific filter"<<endl;
        if ( PassDownOutput )
			setIOoutput();
        
    
        
        if ( PassDownOutput )
            setIOoutputSpecific();
        
        if (trackStats)
			runTrackStats();

		setTestData();
		
        //cout<<"filetr delete prior data"<<endl;
        //delete data preceding the node
        //help manage storage.
       deleteInputsOutputData();
        cout<<"runNode : done deleteInputsOutputData"<<endl;
	}
	

	void base_filter::estimateParameters()
	{
		
	}

	
	void base_filter::applyParametersToTrainingData()
	{
		if ( out_data == NULL)
			out_data = new vector<float>();
		copyToSingleVector( in_data, out_data, NumberOfSamples_ );
        copyToSingleVector( in_data_names_, out_data_names_, 1);
	}
	
	
	

	
	
	
	void base_filter::runSpecificFilter( bool with_estimation )
	{
	  //       cerr<<"base_filter::runSpecificFilter"<<endl;
		if (with_estimation)
			estimateParameters();
		//        cout<<"apply to trainign data "<<in_data.size()<<endl;
        //cout<<"apply to trainign data2 "<<in_data[0]->size()<<endl;
		applyParametersToTrainingData();
	}

	
	///--------STATIC MEMBERS----------//
    void base_filter::applyFeatureFilterToData( const std::vector< std::vector<string>* > & in_data, const std::vector<short> & mask, std::vector<string>* out_data, const unsigned int & NumberOfSamples, unsigned int & Dimensionality )
	{
		//this is important, need the number of samples form the node
		copyToSingleVector( in_data, out_data, NumberOfSamples );
        
		unsigned int DimTrim = 0 ;
		std::vector<string>::iterator i_out = out_data->begin();
		std::vector<string>::iterator i_out_write = out_data->begin();
        
		for (unsigned int i = 0 ; i < NumberOfSamples; ++i)
		{
			for (std::vector<short>::const_iterator i_mask = mask.begin(); i_mask != mask.end(); ++i_mask, ++i_out) {
				if (*i_mask>0){
                    //    cerr<<"keep "<<*i_out<<endl;
					*i_out_write = *i_out;
                        ++i_out_write;
				}else if (i == 0){
					++DimTrim;
                    //cerr<<"delete "<<*i_out<<endl;
                    
				}
			}
			
		}
        
		if (Dimensionality==DimTrim)
			throw ClassificationTreeNodeException("applyFilterToTrainingData : Invalid threshold - no features survived threshold");
		
		Dimensionality-=DimTrim;
        
		out_data->resize(Dimensionality*NumberOfSamples);
        
		
	}
	void base_filter::applyFeatureFilterToData( const std::vector< std::vector<float>* > & in_data, const std::vector<short> & mask, std::vector<float>* out_data, const unsigned int & NumberOfSamples, unsigned int & Dimensionality )
	{
		//this is important, need the number of samples form the node
		copyToSingleVector( in_data, out_data, NumberOfSamples );

        //  cerr<<"copy single"<<endl;
        // cerr<<"NS "<<NumberOfSamples<<" "<<Dimensionality_<<" "<<mask.size()<<" "<<in_data[0]->size()<<endl;
		unsigned int DimTrim = 0 ; 
		std::vector<float>::iterator i_out = out_data->begin();
		std::vector<float>::iterator i_out_write = out_data->begin();
        
		for (unsigned int i = 0 ; i < NumberOfSamples; ++i)
		{
			for (std::vector<short>::const_iterator i_mask = mask.begin(); i_mask != mask.end(); ++i_mask, ++i_out) {
				if (*i_mask>0){
                    //    cerr<<"keep "<<*i_out<<endl;
					*i_out_write = *i_out;
                        ++i_out_write;
				}else if (i == 0){
					++DimTrim;
                    //cerr<<"delete "<<*i_out<<endl;

				}
			}
			
		}

		if (Dimensionality==DimTrim)
			throw ClassificationTreeNodeException("applyFilterToTrainingData : Invalid threshold - no features survived threshold");
		
		Dimensionality-=DimTrim;

		out_data->resize(Dimensionality*NumberOfSamples);

		
	}
	
	void base_filter::applySubjectFilterToData( const std::vector< std::vector<float>* > & in_data, const std::vector<short> & sample_mask, std::vector<float>* out_data,const unsigned int & Dimensionality, unsigned int & NumberOfSamples )
	{
		//this is important, need the number of samples form the node
		copyToSingleVector( in_data, out_data, sample_mask );
        //   copyToSingleVector( in_data_names_, out_data_names_, 1 );//no suject removal

		NumberOfSamples = 0;
		for ( vector<short>::const_iterator i_mask = sample_mask.begin(); i_mask != sample_mask.end();++i_mask)
			if (*i_mask != 0)
				++NumberOfSamples;
		//should already be the right size
		//out_data->resize(Dimensionality*NumberOfSamples);
		
	}
	
	
	
}
