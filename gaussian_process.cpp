//
//  gaussian_process.cpp
//  su_mvdisc
//
//  Created by Brian Patenaude on 3/16/12.
//  Copyright (c) 2012 Stanford University - Etkin Lab. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <gp_filter.h>
#include <data_source.h>
#include <target.h>
//#include <newimage/newimageall.h>
#include <log_regression.h>
#include <vector>


#include <misc_utils.h>
//#include <atlas_utils/atlas_utils.h>
using namespace std;
//using namespace NEWIMAGE;
using namespace misc_utils_name;
//using namespace MISCMATHS;

using namespace su_mvdisc_name;


using namespace std;

void usage(){
    
    cerr<<"\n Usage: \n \n gaussian_process  <data.txt> <-im> <target.txt> [test_data.txt] \n "<<endl;
    cerr<<"\n [test_data.txt] is reuqired if using image data \n"<<endl;
    
}

int main( int argc, char * argv[])
{
    if ( argc <  2 ){
        cerr<<"\n  ***Not enough input arguments***"<<endl;
        usage();
        exit (EXIT_FAILURE);
        
    }
    //N is number of lines (i.e. dimensionaility)
    bool doSpatialReg=true;
    bool useImTarget=false;
    int arg_ind=1;
       string in_data = argv[arg_ind];
    cout<<"In data "<<in_data<<endl;
    
    ++arg_ind;
    if ( string(argv[arg_ind]) == "-im" )
    {
        useImTarget=true;
        ++arg_ind;
    }
    
    
    string in_target = argv[arg_ind];
    ++arg_ind;
    
    cout<<"in target "<<endl;
    string in_test_data;
    if (!useImTarget)
    {
        in_test_data = argv[arg_ind];
        ++arg_ind;
    }
    vector<float>* data = new vector<float>();
    vector<float>* test_data = new vector<float>();

    vector<float> target;
    cout<<"read data "<<endl;
    
   // float ftemp;
    //read in data 
    ifstream fdata(in_data.c_str());
    string stemp;
    while ( getline(fdata,stemp) )
    {
        vector<float> vftemp = csv_string2nums<float>(stemp);
        data->insert(data->end(),vftemp.begin(),vftemp.end());        
    }
    
    fdata.close();
    
    fdata.open(in_data.c_str());
    cout<<"done data read"<<endl;
    int Ns=0;
    //char* line = new char[10000];
    
    while (getline(fdata,stemp))//fdata.getline(line,10000))
    {
        ++Ns;
    }
    fdata.close();
    //delete[] line;
    
    cout<<"done count "<<Ns<<endl;
    unsigned int Ndim = static_cast<unsigned int>(data->size()/Ns);
    cout<<"Ndim : "<<Ndim<<" Ns : "<<Ns<<endl;
    
    for (int i = 0 ; i < Ns; ++i)
    {
        cout<<"i "<<i<<endl;
        for (int j = 0 ; j < Ndim ; ++j)
            cout<<data->at(i*Ndim+j)<<" ";
        cout<<endl;
    }

    
    if ( ! useImTarget ) {
        
        //read in test data 
        float ftemp;
        ifstream fdata_test(in_test_data.c_str());
               string stemp;
        while ( getline(fdata_test,stemp) )
        {
            vector<float> vftemp = csv_string2nums<float>(stemp);
            test_data->insert(test_data->end(),vftemp.begin(),vftemp.end());        
        }

        fdata_test.close();
        
        ifstream ftarget(in_target.c_str());
        while (ftarget>>ftemp) {
            target.push_back(ftemp);
            cout<<"target "<<ftemp<<endl;
            
        }
        ftarget.close();

        
            
        ///dimensionality oif test data and input need be the same
        int Ns_test = static_cast<int>(test_data->size() / Ndim);
        
        
        Target * node_targ = new Target();
        node_targ->setTargetData(target);
        
        cout<<"Data src"<<endl;
        DataSource * data_src = new DataSource();
        data_src->setVerbose(true);
        cout<<"datasrc "<<data->size()<<endl;
        //    cout<<"datasrc "<<data->size()<<" "<<data[0]<<endl;
        data_src->addInputData( data,Ndim, Ns );
        //needed for non-crossvalidation
        data_src->copyInData_to_OutData();
        data_src->setTarget(node_targ);
        data_src->setTestData(test_data,Ns_test);
        
        
        
        cout<<"GP "<<endl;
        GaussianProcessFilter* gp = new GaussianProcessFilter();
        gp->setVerbose(true);
        
        gp->addInput(data_src);
        // gp->estimateParameters();
        gp->runNode();
        
        delete gp;
        delete data_src;
        //data source delete should take car eof vector
        delete node_targ;

    }else if ( doSpatialReg){
        cout<<"DO psatial regression"<<endl;
        volume4D<float> im_targ;        
        read_volume4D(im_targ, in_target);
        int sx=im_targ.xsize();
        int sy=im_targ.ysize();
        int sz=im_targ.zsize();
        int st = im_targ.tsize();
    //    if (Ns_im != Ns)
      //  {
        //    cerr<<"Number of subjects in target does no match that in the predictors"<<endl;
          //  exit (EXIT_FAILURE);
        //}
        
        vector<float>* data_in = new vector<float>();

        //do a spatial regression
        unsigned int vox_count=0;
        for ( int t = 0 ; t< st; ++t){
            for (int x =0 ; x< sx ; ++x)
                for (int y=0; y<sy; ++y )
                    for (int z=0; z<sz ; ++z)
                    {
                        float imval=im_targ[t].value(x,y,z);
                        if (imval != 0 ) 
                        {
                            //loading free data
                            data_in->push_back(x);
                            data_in->push_back(y);
                            data_in->push_back(z);
                            data_in->push_back(imval);
                            ++vox_count;

                        }
                    }
            vector<float> target(data->size()*vox_count);// = new vector<float>(data->size()*vox_count);
            vector<float>::iterator i_d = data->begin();
            for (vector<float>::iterator i_t = target.begin();i_t != target.end();++i_d)
            {
                *i_t = *i_d;
                ++i_t;
                *i_t = *i_d;
                ++i_t;
                *i_t = *i_d;
                ++i_t;
                *i_t = *i_d;
    
            }
            try{
                
                Target * node_targ = new Target();
                GaussianProcessFilter* gp = new GaussianProcessFilter();
                DataSource * data_src = new DataSource();
                
                //dimensionaility is 4 
                //number if voxels x number of subjects
                data_src->addInputData( data,4, vox_count*st );
                data_src->setTestData(data,vox_count*st);
                
                cout<<"datasrc "<<data->size()<<endl;
                
                
                node_targ->setTargetData(target);
                data_src->setVerbose(true);
                //needed for non-crossvalidation
                
                data_src->copyInData_to_OutData();
                data_src->setTarget(node_targ);
                
                gp->setVerbose(true);
                // if (gp_count==0)
                gp->addInput(data_src);
                // gp->estimateParameters();
                gp->runNode();
                cout<<"done run node"<<endl;
                vector<float> odata = gp->getOutputTestData<float>();
                int t=0;
                
              //  cout<<"set im pred data "<<odata.size()<<" "<<im_pred.tsize()<<endl;
            ///for (vector<float>::iterator i_o = odata.begin(); i_o != odata.end()-1;++i_o,++t)
               //     im_pred[t].value(x, y, z) = *i_o;
                
                
                
                delete gp;
                //data source delete should take car eof vector
                delete node_targ;
                cout<<"done targ delete"<<endl;
                delete data_src;
                
            } catch(std::exception &e) {
                cerr << e.what() << endl;
            } 

            
        }
        delete data_in;
     //   delete target;
        
    }else{
        
        volume4D<float> im_targ;

        read_volume4D(im_targ, in_target);
        int sx=im_targ.xsize();
        int sy=im_targ.ysize();
        int sz=im_targ.zsize();
        int Ns_im = im_targ.tsize();

      
        if (Ns_im != Ns)
        {
            cerr<<"Number of subjects in target does no match that in the predictors"<<endl;
            exit (EXIT_FAILURE);
        }
                     
        //Assuming 1D in x lets generate some test data bas; ed on range
        pair<float, float> x_range(0,0);
        x_range.first=data->at(0);
        for (vector<float>::iterator i_d = data->begin(); i_d != data->end();++i_d)
        {
            cout<<"x "<<*i_d<<endl;
            if ( *i_d < x_range.first ) x_range.first=*i_d;
            if ( *i_d > x_range.second ) x_range.second =*i_d;

        }
        float res=0.1f;
        int Ns_test= static_cast<int>((x_range.second- x_range.first)/res)+1;
//        vector<float> test_data(Ns_test);
        test_data->resize(Ns_test);
        cout<<"xrange "<<x_range.first<<" "<<x_range.second<<endl;
        
        volume4D<float> im_pred(sx,sy,sz,Ns_test);
        im_pred.copyproperties(im_targ);
        im_pred=0;
        
        
        int count=0;
        for (vector<float>::iterator i_t = test_data->begin(); i_t != test_data->end(); ++i_t,++count)
            *i_t=x_range.first + count*res;
        
        print<float>(*test_data,"test_data");
     
        int gp_count=0;
        //run on every voxels
        for (int x =0 ; x< sx ; ++x)
            for (int y=0; y<sy; ++y )
                for (int z=0; z<sz ; ++z,++gp_count)
                {
                    cout<<"xyz "<<x<<" "<<y<<" "<<z<<endl;
                    if (x==y)
                    {
                        --gp_count;
                        continue;
                    }
                    vector<float> target(Ns);
                    int count=0;
                    //omit voxel that contain an exact 0
                    bool anyZero=false;
                    for (vector<float>::iterator i_t = target.begin(); i_t != target.end(); ++i_t,++count)
                    {
                        float val = im_targ[count].value(x,y,z);
                        if ( val == 0 )
                        {
                            anyZero=true;
                            break;
                        }
                        //loading data from image.
                        *i_t = val;
                        
                    }
                    
                    vector<float>* data_in = new vector<float>();
                    vector<float>* test_data_in = new vector<float>();
                    //these shoudl be delete by deleting nodes
                    *data_in=*data;
                    *test_data_in=*test_data;
                    
                    
                    try{
                    
                    Target * node_targ = new Target();
                    GaussianProcessFilter* gp = new GaussianProcessFilter();
                    DataSource * data_src = new DataSource();
                    
                    data_src->addInputData( data_in,Ndim, Ns );
                    data_src->setTestData(test_data_in,Ns_test);

                    cout<<"datasrc "<<data->size()<<endl;

                    
                    node_targ->setTargetData(target);
                    data_src->setVerbose(true);
                    //needed for non-crossvalidation
              
                    data_src->copyInData_to_OutData();
                    data_src->setTarget(node_targ);

                    gp->setVerbose(true);
                   // if (gp_count==0)
                    gp->addInput(data_src);
                    // gp->estimateParameters();
                    gp->runNode();
                    cout<<"done run node"<<endl;
                    vector<float> odata = gp->getOutputTestData<float>();
                    int t=0;
                    
                    cout<<"set im pred data "<<odata.size()<<" "<<im_pred.tsize()<<endl;
                    for (vector<float>::iterator i_o = odata.begin(); i_o != odata.end()-1;++i_o,++t)
                        im_pred[t].value(x, y, z) = *i_o;
                    

                    
                    delete gp;
                    //data source delete should take car eof vector
                    delete node_targ;
                    cout<<"done targ delete"<<endl;
                    delete data_src;

                    } catch(std::exception &e) {
                    cerr << e.what() << endl;
                } 
                   //return 0;
                    
                }
        save_volume4D(im_pred, "im_pred");


    
    }
    


    
    
    return 0;
}