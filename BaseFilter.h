#ifndef base_filter_H
#define base_filter_H

/*
 *  BaseFilter.h
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 7/27/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include <ClassificationTreeNode.h>

namespace su_mvdisc_name{


class base_filter : public ClassificationTreeNode
{
	public:
		base_filter();
		virtual ~base_filter();
		void runNode( bool with_estimation = true );
		virtual void estimateParameters();
		virtual void applyParametersToTrainingData();
			virtual void runSpecificFilter( bool with_estimation = true );
    
    static void applyFeatureFilterToData( const std::vector< std::vector<std::string>* > & in_data, const std::vector<short> & mask, std::vector<std::string>* out_data, const unsigned int & NumberOfSamples, unsigned int & Dimensionality );

	static void applyFeatureFilterToData( const std::vector< std::vector<float>* > & in_data, const std::vector<short> & mask, std::vector<float>* out_data, const unsigned int & NumberOfSamples, unsigned int & Dimensionality );
	static void applySubjectFilterToData( const std::vector< std::vector<float>* > & in_data, const std::vector<short> & sample_mask, std::vector<float>* out_data, const unsigned int & Dimensionality, unsigned int & NumberOfSamples  );

protected:
	//these are used to keep track of data for viewing output


	
private:
	
	
		
};
	

	
	
}

#endif
