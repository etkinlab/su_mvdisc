/*
 *  nifti_writer.cpp
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 8/19/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "nifti_writer.h"

//FSL libraries
//FSL includes
//#include "newimage/newimageall.h"
#include "newimage/newimageio.h"
#include "newimage/newimage.h"


using namespace std;
using namespace NEWIMAGE;

namespace su_mvdisc_name
{
	nifti_Writer::nifti_Writer() 
	{
		/*dims = uint3(0,0,0);
		res  = float4(0,0,0,1);
		useMask=false;
		im_src=NULL;
		 */
	}
	nifti_Writer::~nifti_Writer() {}
/*	
	void nifti_Writer::setMask( ImageSource * im_src_in ) 
	{
		useMask	=	true;
		im_src	=	im_src_in;
		dims	=	im_src->size();
		res		=	im_src->res();
	 
	}

	void nifti_Writer::setImageDimensions( const uint3 & dims_in )
	{
		//cout<<"set dims "<<dims_in.x<<" "<<dims_in.y<<" "<<dims_in.z<<endl;

		dims=dims_in;
	}

	void nifti_Writer::setImageDimensions(const unsigned int & x, const unsigned int & y , const unsigned int & z )
	{
		dims=uint3(x,y,z);
	}
	
	void nifti_Writer::setImageResolution( const float4 & res_in )
	{
		res=res_in;
	}
	
	void nifti_Writer::setImageResolution(const float & x, const float & y , const float & z)
	{
		res=float4(x,y,z,1);
	}
	void nifti_Writer::setImageResolution(const float & x, const float & y , const float & z, const float & t)
	{
		res=float4(x,y,z,t);
	}
 */

	void nifti_Writer::write( const std::vector<float> & data, const uint3 & dims, const float3 & res , const string & outname)
	{
		//cout<<"write NIFTI image "<<data.size()<<" "<<" "<<dims.x * dims.y * dims.z<<endl;
		//unsigned nvoxels = ; 
		
		if (data.size() != (dims.x * dims.y * dims.z))
			throw DataWriterException("Data size does not match number of voxels in image.");
		volume<float>* image = new volume<float>(dims.x,dims.y,dims.z);
		image->setdims(res.x,res.y,res.z);
		
		float min=-1e12;
		float max=1e12;
		vector<float>::const_iterator i_data = data.begin();
		for ( unsigned int z=0; z<dims.z ; z++)
			for ( unsigned int y=0; y<dims.y ; y++)
				for (unsigned int x=0; x<dims.x ; x++,  i_data++)
				{
					if ( min > *i_data) min = *i_data;
					if ( max > *i_data) min = *i_data;

					//if (*i_data != 0 )
					//	cout<<"data write : "<<*i_data<<endl;
					image->value(x,y,z)=*i_data;
										
				}
		
			image->setDisplayMaximumMinimum(min,max);
		save_volume(*image, outname);
		delete image;
	}
/*
	void nifti_Writer::write( const std::string & filename )
	{
		
		cout<<"nifit_Writer : write : "<<filename<<dims.x<<" "<<dims.y<<" "<<dims.z<<" "<<endl;
		NumberOfSamples = node_inputs[0]->getNumberOfSamples();

		volume4D<float>* im_out = new volume4D<float>(dims.x, dims.y, dims.z, NumberOfSamples); 
		im_out->setdims(res.x,res.y,res.z,res.t);//last is fourth dimension //subject number
	//	im_out->setsize(res.x,res.y,res.z,res.t);//last is fourth dimension //subject number

		if (node_inputs.empty())
		{
			throw	ClassificationTreeNodeException("Trying to write an image, but there's no input.");
		}
		if (node_inputs.size()>1)
		{
			throw	ClassificationTreeNodeException("Image writing currently only supports one input.");
		}
		
		//cout<<"fill values "<<NumberOfSamples<<endl;
		vector<float>::iterator i_out = node_inputs[0]->getDataPointer()->begin();
		if (useMask)
		{
			//cout<<"useMASK in wirting"<<endl;
			if (im_src==NULL)
				throw ClassificationTreeNodeException("Tried to apply maks when writing file, but no image source was set.");
			
			//cout<<"got mask iterator"<<endl;
			for ( unsigned int i = 0 ; i < NumberOfSamples ; i++)
			{
				vector<int>::iterator i_mask = im_src->mask_begin();
			//	int count=0;
				////cout<<"sample "<<i<<endl;
				//	volume<float> im_1(dims.x, dims.y, dims.z);
				for ( unsigned int z=0; z<dims.z ; z++)
					for ( unsigned int y=0; y<dims.y ; y++)
						for (unsigned int x=0; x<dims.x ; x++,  i_mask++)
						{
					//		//cout<<"xyz "<<count<<" "<<x<<" "<<y<<" "<<z<<" "<<endl;
					//		//cout<<"xyz2 "<<count<<" "<<dims.x<<" "<<dims.y<<" "<<dims.z<<" "<<endl;

							//		im_1.value(x,y,z)=*i_out;
							if (*i_mask)
							{
								im_out->value(x,y,z,i)=*i_out;
								i_out++;
							}else {
								im_out->value(x,y,z,i)=0;
							}

						}
			}
			
		}else{
			for ( unsigned int i = 0 ; i < NumberOfSamples ; i++)
				{
					for ( unsigned int z=0; z<dims.z ; z++)
						for ( unsigned int y=0; y<dims.y ; y++)
							for (unsigned int x=0; x<dims.x ; x++,i_out++)
							{
								im_out->value(x,y,z,i)=*i_out;
								
							}
					
				}
			}
								//cout<<"write file "<<filename<<im_out->xdim()<<" "<<im_out->ydim()<<" "<<im_out->zdim()<<endl;
		//cout<<"write file "<<filename<<" "<<im_out->xsize()<<" "<<im_out->ysize()<<" "<<im_out->zsize()<<" "<<im_out->tsize()<<endl;

		save_volume4D(*im_out, filename);
		//cout<<"done write"<<endl;
		delete im_out;
		
	}
 */
}