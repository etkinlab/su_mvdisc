/*
 *  lda.cpp
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 7/15/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

//#define ZERO_THRESH 0.000001


//#define USE_CUDA 0

#include "lda.h"
//#include <misc_utils/misc_utils.h>
#include <misc_utils.h>
#include <string.h>
#include <fstream>

#ifdef ARCHLINUX
extern "C" {
#include "cblas.h"
#include "f2c.h"
    //typedef long int __CLPK_integer;
    //typedef float  __CLPK_real;
    
    typedef integer __CLPK_integer;
    typedef real __CLPK_real;
#include "clapack.h"
}
#else
//#endif
//#ifdef ARCHDARWIN

#include <Accelerate/Accelerate.h>

#endif

//OSX include
//#include <vecLib/cblas.h>
//#include <vecLib/clapack.h>




//STL includes
#include <cmath>

using namespace std;
using namespace misc_utils_name;
using namespace clapack_utils_name;
//using namespace cuda_mvdisc;
namespace su_mvdisc_name
{
	
	LDA_mvdisc::LDA_mvdisc()
	{
        nodeTypeSpec=LDA;
        NodeName="LDA_Discriminant";
		cl_prior_choice = EQUAL_PROB;
        ZERO_THRESH=1e-6;
        frac_var_thresh=1;
        output_basename="output_";
        
        
	}
	
	
	
	
	
	LDA_mvdisc::~LDA_mvdisc()
	{
        
		
	}
	void LDA_mvdisc::setOptions( std::stringstream & ss_options)
	{
		
		//-------BEFORE SETTING CUSTOM OPTIONS RESET TO DEFAULT----------//
        
		//\---------------SET CUTOM TOPTIONS ------------------//
		
		
		map<string,string> options = parse_sstream(ss_options);
		for ( map<string,string>::iterator it = options.begin(); it!=options.end();it++)
		{
            if ( it->first == "Prior")
			{
				if (it->second == "SampleProb")
                cl_prior_choice = SAMPLE_PROB;
				else if (it->second == "EqualProb")
                cl_prior_choice = EQUAL_PROB;
            }else if ( it->first == "FracVariance")
			{
                stringstream ss;
                ss<<it->second;
                ss>>frac_var_thresh;
            }else {
				if (!setTreeNodeOptions(it->first, it->second))
                {
                    cerr<<NodeName + " : Invalid input argument : ." + it->first <<"."<<endl;
                    exit (EXIT_FAILURE);
                    //					throw ClassificationTreeNodeException(NodeName + " : Invalid input argument : " + it->first);
                }
            }
		}
        
	}
	
	
	
	void LDA_mvdisc::estimateParameters()
	{
        if (Verbose>=2)
        cout<<" LDA_mvdisc::estimateParameters "<<endl;

        
        cout<<" LDA_mvdisc::estimateParameters "<<Dimensionality_<<" "<<NumberOfSamples_<<endl;
        
		//need to set data on device
		vector<unsigned int> NperClass = training_target->getNumberOfSubjectsPerClass();
		
		
		//scaling factor for calculating mean
		vector<float> scale;
        unsigned int class_ind=0;
		class_priors.resize(Kclasses_);
        for (vector<unsigned int>::iterator i = NperClass.begin(); i != NperClass.end(); i++,++class_ind)
		{
			cout<<"N "<<*i<<endl;
			scale.push_back(1.0/static_cast<float>(*i));
			if (cl_prior_choice == EQUAL_PROB )
			{
				class_priors[class_ind]=(1.0/Kclasses_);
				//cout<<"Use equal probabilities for class priors "<<(1.0/Kclasses_)<<endl;
			}else { //default of SAMPLE_PROB
				class_priors[class_ind]=(static_cast<float>(*i) /NumberOfSamples_ );
				//cout<<"Use sample probabilities for class priors "<<static_cast<float>(*i) /NumberOfSamples_<<endl;
                
			}
            
            //		class_priors.push_back(0.5);
			//cout<<" class prior: "<<*i<<" "<<NumberOfSamples_<<" "<<static_cast<float>(*i) /NumberOfSamples_<<endl;
		}
		//cout<<"use cuda in lda"<<endl;
		print<float>(scale,"scale");
		
		int minMN=(NumberOfSamples_ < Dimensionality_) ? NumberOfSamples_ : Dimensionality_ ;
        
        mu_sphere.resize(Kclasses_, Dimensionality_);
        
//        cout << "LDA: estimate params: just resized mu" << endl;
        
        //smatrix data(NumberOfSamples_,Dimensionality_);
        smatrix Vt(NumberOfSamples_,Dimensionality_);
        cout<<"copy vector "<<NumberOfSamples_<<" "<<Dimensionality_<<endl;
        cout<<"indata size "<<in_data.size()<<endl;
        cout<<"indata "<<in_data[0]->size()<<" "<<Dimensionality_<<endl;
        cout<<"indata2"<<endl;
		copyToSingleVectorTranspose(in_data, Vt.sdata, NumberOfSamples_);
        cout<<"done copy vector "<<endl;

        if (Verbose>=2)
            smatrix_print(Vt,"LDA::estimate_parameters : input data");

        
        
        //calculate standard deviations
        smatrix m_target(NumberOfSamples_,1);
        float * i_t = m_target.sdata;
        for (vector<int>::iterator i_c = training_target->class_index_begin(); i_c != training_target->class_index_end(); i_c++,++i_t)
        *i_t = *i_c;
        
	//        smatrix_print(Vt,"uni_stdev_");

        
        if (Verbose>=2)
        {//matlab code
            cout<<"%----Matlab Code ------------%"<<endl;
            smatrix_printForMatlab(Vt, "X");
            smatrix_printForMatlab(m_target, "Y");
            cout<<"mu0=mean(X(Y==0,:)); mu1=mean(X(Y==1,:));"<<endl;
            cout<<"N0=sum(Y==0); N1=sum(Y==1);"<<endl;
            cout<<"mu=repmat(mu0,N0+N1,1); mu(Y==1,:)=repmat(mu1,N1,1);"<<endl;
            cout<<"Xdemean=X-mu"<<endl;
            cout<<"[u,d,v]=svd(Xdemean'*Xdemean,0)"<<endl;

            cout<<"%----Matlab Code ------------%"<<endl;

            
        }

        
        
        uni_stdev_ = smatrix_stdev(Vt,m_target);
//        smatrix_print(uni_stdev_,"uni_stdev_");
        
        //cout << "LDA: estimate params: just copied data to a single vector" << endl;
        
        smatrix_set(mu_sphere,0);   // sets everything in mu_sphere.sdata to 0
        
//        cout << "LDA: estimate params: just resized mu and added data" << endl;
        
		//sum values across classes
		unsigned int count = 0 ;
        nperclass_.assign(Kclasses_, 0);

//        nperclass_.resize(Kclasses_, 0);
        //cout<<"class"<<endl;
		for (vector<int>::iterator i_c = training_target->class_index_begin(); i_c != training_target->class_index_end(); i_c++, count++ )
        {
            smatrix_addRow2Row(Vt,count,mu_sphere,*i_c);
            ++nperclass_[*i_c];
            //            smatrix_print(mu_sphere,"mu_spere-accumulate");
            //                cout<<"classin "<<*i_c<<endl;
		}
		//divide by number of sample, scale is not use as accumulating means because precision issues
		for (unsigned int i=0; i<Kclasses_;i++)
        {
//            cout<<"nperclass "<<nperclass_[i]<<endl;
            smatrix_rowScale(mu_sphere,i,1.0f/nperclass_[i]);
        }
        //display class means
        if (Verbose>=2)
        smatrix_print(mu_sphere,"mu");
        //demean the data (by class specific means
        
        //        vector<smatrix> group_dm; //store demeand group data, need if not in consecutive order?
		count = 0 ;
		for (vector<int>::iterator i_c = training_target->class_index_begin(); i_c != training_target->class_index_end(); i_c++, count++ )
        smatrix_scaleAndAddRow2Row(mu_sphere,*i_c,-1,Vt,count);
        
        //lets do 2 processing streams, M>N and N>M
        
        if (Dimensionality_ <= NumberOfSamples_)
        {
            smatrix D;//,U;
            //After svd DxUt store Ut
            //DxUt contain -1/2inverse
                smatrix_print(Vt,"LDA:data");
//            smatrix_AtxA(Vt,U);
            //            smatrix_print(U,"Wsscp");
            DxUt.resize( minMN,Dimensionality_);
            __CLPK_integer info = smatrix_svd_of_transpose(Vt, DxUt, D,'S');
            //        smatrix_rowScale
                        smatrix_print(Vt,"LDA:data-post");
            
                        smatrix_print(DxUt,"LDA:DxUt");
                        smatrix_print(D,"LDA:D");
            D/=sqrtf(NumberOfSamples_  - training_target->getNumberOfClasses()) ;
            //            smatrix_print(D,"LDA:D");
            
            if (! (info==0) )
            {
                stringstream ss;
                ss<<info;
                string sinfo;
                ss>>sinfo;
                throw ClassificationTreeNodeException( ("SVD error in estimating LDA a parameters" + sinfo).c_str() );
            }
            //threshold
            //do if keeping less than 100% of variance
            if ( frac_var_thresh < 1 ){
                float total_var  =  cblas_sdot(minMN, D.sdata, 1, D.sdata, 1);
                //                cout<<"Total Variance "<<total_var<<endl;
                //zero thresh is actially on singulart values
                float var_thresh = (total_var*frac_var_thresh);
                
                //D is singular values not eignevalues
                //invert D
                float cum_var=0;
                for (__CLPK_integer row = 0 ; row < minMN; ++row)
                {
                    cum_var += D.sdata[row]*D.sdata[row];
                    //if cumulative variance is less then total being kep and singular value is above zero threshold
                    D.sdata[row]  = (cum_var < var_thresh) ?  ( (D.sdata[row] > ZERO_THRESH) ? 1.0f/D.sdata[row] : 0 ) : 0 ;
                }
                
            }else{
                
                //D is singular values not eignevalues
                //invert D
                for (__CLPK_integer row = 0 ; row < minMN; ++row)
                D.sdata[row]  = (D.sdata[row] > ZERO_THRESH) ? 1.0f/D.sdata[row] : 0 ;
                
            }
            
            
            
            //            smatrix_print(D, "Dinv");
            //            smatrix_print(DxUt, "DxUt");
            
            
            for (__CLPK_integer row = 0 ; row < minMN; ++row)
            smatrix_rowScale(DxUt, row, D.sdata[row]);
            
            
            //            smatrix_print(mu_sphere, "means");
            
            //            smatrix_print(DxUt, "Sphering matrix");
            
            smatrix_BxAt(mu_sphere,DxUt);

        }else{
            smatrix D;//,U;
            //After svd DxUt store Ut
            //DxUt contain -1/2inverse
                       smatrix_print(Vt,"LDA:data");
//            smatrix_AtxA(Vt,U);
            //            smatrix_print(U,"Wsscp");
            DxUt.resize( minMN,Dimensionality_);
            __CLPK_integer info = smatrix_svd_of_transpose(Vt, DxUt, D,'S');
            //Vt is actually going to be U
//            __CLPK_integer info = smatrix_svd(Vt, DxUt, D,'S');
         //   __CLPK_integer info = smatrix_svd(Vt,D,DxUt);

            //        smatrix_rowScale
                        smatrix_print(Vt,"LDA:data-post");
            
                        smatrix_print(DxUt,"LDA:DxUt");
                        smatrix_print(D,"LDA:D");
            D/=sqrtf(NumberOfSamples_  - training_target->getNumberOfClasses()) ;
            //            smatrix_print(D,"LDA:D");
            
            if (! (info==0) )
            {
                stringstream ss;
                ss<<info;
                string sinfo;
                ss>>sinfo;
                throw ClassificationTreeNodeException( ("SVD error in estimating LDA a parameters" + sinfo).c_str() );
            }
            //threshold
            //do if keeping less than 100% of variance
            if ( frac_var_thresh < 1 ){
                float total_var  =  cblas_sdot(minMN, D.sdata, 1, D.sdata, 1);
                //                cout<<"Total Variance "<<total_var<<endl;
                //zero thresh is actially on singulart values
                float var_thresh = (total_var*frac_var_thresh);
                
                //D is singular values not eignevalues
                //invert D
                float cum_var=0;
                for (__CLPK_integer row = 0 ; row < minMN; ++row)
                {
                    cum_var += D.sdata[row]*D.sdata[row];
                    //if cumulative variance is less then total being kep and singular value is above zero threshold
                    D.sdata[row]  = (cum_var < var_thresh) ?  ( (D.sdata[row] > ZERO_THRESH) ? 1.0f/D.sdata[row] : 0 ) : 0 ;
                }
                
            }else{
                
                //D is singular values not eignevalues
                //invert D
                for (__CLPK_integer row = 0 ; row < minMN; ++row)
                D.sdata[row]  = (D.sdata[row] > ZERO_THRESH) ? 1.0f/D.sdata[row] : 0 ;
                
            }
            
            
            
                        smatrix_print(D, "Dinv");
                        smatrix_print(DxUt, "DxUt");
            
            
            for (__CLPK_integer row = 0 ; row < minMN; ++row)
            smatrix_rowScale(DxUt, row, D.sdata[row]);
            
            
                        smatrix_print(mu_sphere, "means");
            
                        smatrix_print(DxUt, "Sphering matrix");
            
            smatrix_BxAt(mu_sphere,DxUt);
             smatrix_print(DxUt, "Sphering matrix");
            smatrix_print(mu_sphere, "sphered means");

        }
        
//        cerr << "LDA: end estimate params" << endl;
        
		
	}
	
    
    void LDA_mvdisc::setParameters()
    {
        
    }
	
	int LDA_mvdisc::classifyTrainingData()
	{
       cout<<"LDA_mvdisc::classifyTrainingData"<<endl;
        if (in_data.empty())
        throw ClassificationTreeNodeException("Trying to classify training using LDA , but no data to classify exists.");
        
        smatrix target(NumberOfSamples_,Dimensionality_); // target to classify
                                                        //smatrix dist(NumberOfSamples_,Dimensionality_);   // distance in each dimension from sphered mean
        
        // load training data into target matrix
        copyToSingleVectorTranspose(in_data, target.sdata, NumberOfSamples_);
        //        smatrix_print(target, "training_data_to_classify");
        //        smatrix_print(DxUt, "DxUt");
        
        smatrix_BxAt(target, DxUt);
        //class labels
        vector<int> cl_labels = training_target->getClassLabels();
        vector< pair<unsigned int,float> > v_dist(NumberOfSamples_);
        for (unsigned int class_ind = 0; class_ind < Kclasses_; class_ind++)
        {
            //            cout<<"class "<<class_ind<<endl;
            
            // copy the training data
            smatrix diff(target.MRows,target.NCols);//f=target;
            memcpy(diff.sdata, target.sdata, target.MRows*target.NCols*sizeof(float));
            //     smatrix_print(diff, "diff - initial data");
            //  smatrix_BxAt(diff, DxUt);
            //smatrix_print(diff, "diff - sphered data");
            
            smatrix_scaleAndAddRow2Matrix(mu_sphere,class_ind,-1.0, diff);
            //
            //            smatrix_print(diff, "difference from mean");
            //
            
            for (__CLPK_integer i = 0; i < diff.MRows; ++i)
            {
                //                smatrix_print(DxUt, "DxUt");
                //                smatrix_print(mu_sphere, "muspeherw");
                
                //distance modulo prior
                //                cout<<"sdot "<<diff.NCols<<" "<<i<<" "<<diff.MRows<<endl;
                float dist = cblas_sdot(diff.NCols, diff.sdata+i,diff.MRows , diff.sdata+i, diff.MRows) - 2.0*logf(class_priors[class_ind]);
                dist = cblas_sdot(diff.NCols, diff.sdata+i,diff.MRows , diff.sdata+i, diff.MRows);
                
                
                //                cout<<"dist "<<i<<" "<<class_ind<<" "<<dist<<" "<<- 2.0*logf(class_priors[class_ind])<<endl;
                if ( class_ind == 0 )
                {
                    //    cout<<"domcpare0 "<<endl;
                    v_dist[i].first = class_ind;
                    v_dist[i].second = dist;
                }else if (v_dist[i].second > dist){
                    v_dist[i].first=class_ind;
                    v_dist[i].second = dist;
                }
                //    cerr<<"dcompare2 "<<class_ind<<" "<<i<<" "<<v_dist[i].second<<" "<<dist<<endl;
                
            }
        }
        
        float train_err=0;
        vector<int>::iterator class_iter = training_target->class_index_begin();
        for (vector< pair<unsigned int, float> >::iterator i_dist = v_dist.begin(); i_dist != v_dist.end(); ++i_dist, ++class_iter) {
            out_data->push_back(static_cast<float> (cl_labels[i_dist->first]));
            train_err += (cl_labels[i_dist->first] == *class_iter) ? 1 : 0;
        }
        
        //        print(*out_data,"training classification");
        
        
        //   cout<<"class "<<endl;
        //  for (vector<int>::iterator class_iter = training_target->class_index_begin(); class_iter != training_target->class_index_end(); class_iter++)
        //     cout << *class_iter << " ";
        //cout << endl;
        
        train_err /= NumberOfSamples_;
        //        cout << "LDA: training accuracy " << train_err << endl;
        
        
        if (Verbose)
        cout << "LDA_mvdisc : classify training end" << endl;

        cout << "LDA_mvdisc : classify training end" << endl;
        
        
		///--------------------------------------------------------------------NEED TO APPLY TO TRAINING DATA--------------------------------------------//
        // exit (EXIT_FAILURE);
		return 0;
	}
    string  LDA_mvdisc::getParameterFileName()
    {
        return (output_basename + "." + NodeName + ".params.txt");
    }
	void LDA_mvdisc::saveParameters( )
    {
        //   cerr<<"LDA_mvdisc::saveParameters "<<output_basename<<" "<<Verbose<<endl;
        
        if (Verbose >= 3)
        cerr<<"LDA_mvdisc::saveParameters "<<output_basename<<endl;
        
        if  ( output_basename.empty() ) //don't save parameters if output name has not been set
        return;
        //want to conatin all models specifics in a single line
        ofstream f_params((output_basename + "." + NodeName + ".params.txt").c_str(),ios_base::out | ios_base::app);
        f_params<<"ASCII LDA "<<Dimensionality_<<" "<<NumberOfSamples_<<" "<<Kclasses_;
        for (vector<unsigned int>::iterator i_nper = nperclass_.begin(); i_nper != nperclass_.end(); ++i_nper)
        f_params<<" "<<*i_nper;
        
        f_params<<" "<<cl_prior_choice<<" "<<ZERO_THRESH<<" "<<frac_var_thresh;
        
        //output univariate standard deviations, pooled
        float * i_stdev = uni_stdev_.sdata;
        for (unsigned int i = 0 ; i < Dimensionality_; ++i,++i_stdev)
        {
            f_params<<" "<<*i_stdev;
        }
        
        {
//            cout<<"MUSPHERE "<<mu_sphere.MRows<<" "<<mu_sphere.NCols<<endl;
            
            float* p_mu_sph = mu_sphere.sdata;
            //Dimensionality_ depends on minMN
            unsigned int Nele= mu_sphere.MRows*mu_sphere.NCols;//should be same as Row X Cols
            for (unsigned int i = 0 ; i < Nele; ++i, ++p_mu_sph)
            {
                f_params<<" "<<*p_mu_sph;
            }
        }
        {    //output decomposed covariances matrix DxUT
            float * p_dxut = DxUt.sdata;
            unsigned int Nele = DxUt.MRows * DxUt.NCols;
            for (unsigned int i = 0 ; i < Nele; ++i, ++p_dxut)
            f_params<<" "<<*p_dxut;
        }
        f_params<<endl;
        f_params.close();
        
    }
    
	
    int LDA_mvdisc::classifyTestData()
    {
        	cerr<<"LDA_mvdisc : classify test data"<<endl;
        
        
        if (in_test_data.empty())
		throw ClassificationTreeNodeException("Trying to classify using LDA , but no data to classify exists.");
        
        // int minMN=(NumberOfSamples_ < (Dimensionality_ + DimTrim)) ? NumberOfSamples_ : (Dimensionality_ + DimTrim) ;
        
        //cerr << "Dimensionality_: " << Dimensionality_ << " "<<NumberOfTestSamples<<endl;
        
        smatrix target(NumberOfTestSamples_, Dimensionality_); //target to classify
                                                             //smatrix dist(NumberOfTestSamples,Dimensionality_);  //distance in each dinesion form sphered mean
        
        //cerr << "created target matrix. intestdata size: " << in_test_data.size() << " Number of test samples: " << NumberOfTestSamples << "." << endl;
        //cerr << "intestdata1 size: " << in_test_data[0]->size() << endl;
        
        copyToSingleVectorTranspose(in_test_data, target.sdata, NumberOfTestSamples_);
        if (Verbose>2)
        smatrix_print(target, "LDA : Data Classify");
        //cerr << "copied in test_data to target" << endl;
        
        //    smatrix_print(target, "DataToClassify");
        
        //spehere test data
        smatrix_BxAt(target,DxUt);
        
        //cerr << "multiplied by DxUt" << endl;
        
        //    smatrix_print(target, "Sphered target");
        //class labels
        vector<int> cl_labels = training_target->getClassLabels();
        vector< pair<unsigned int,float> > v_dist(NumberOfTestSamples_);
        for (unsigned int class_ind =0; class_ind<Kclasses_;++class_ind)
        {
            
            //cerr << "multiplied by DxUt1" << endl;
            
            smatrix diff(target.MRows,target.NCols);//f=target;
            memcpy(diff.sdata, target.sdata, target.MRows*target.NCols*sizeof(float));
            
            // smatrix_print(diff, "diff - data spehered - test ");
            //cerr << "multiplied by DxUt2" << endl;
            
            smatrix_scaleAndAddRow2Matrix(mu_sphere,class_ind,-1.0, diff);
            //        smatrix_print(diff, "diff from mean - test ");
            for ( __CLPK_integer i = 0 ; i <diff.MRows;++i)
            {
                //distance modulo prior
                float dist = cblas_sdot(diff.NCols, diff.sdata+i,diff.MRows , diff.sdata+i, diff.MRows) - 2.0*logf(class_priors[class_ind]);
                //            cerr<<"dist "<<cblas_sdot(diff.NCols, diff.sdata+i,diff.MRows , diff.sdata+i, diff.MRows)<<" "<<- 2.0*logf(class_priors[class_ind])<<endl;
                if ( class_ind == 0 )
                {
                    //    cout<<"domcpare0 "<<endl;
                    v_dist[i].first = class_ind;
                    v_dist[i].second = dist;
                }else if (v_dist[i].second > dist){
                    v_dist[i].first=class_ind;
                    v_dist[i].second = dist;
                }
            }
            //cerr << "multiplied by DxUt3" << endl;
            
            
        }
        //cout<<"move on"<<endl;
        out_test_data->resize(v_dist.size());
        vector<float>::iterator i_outtest=out_test_data->begin();
        for (vector< pair<unsigned int, float> >::iterator i_dist = v_dist.begin(); i_dist != v_dist.end();++i_dist, ++i_outtest)
        {
            //cerr<<"pushing back "<<cl_labels.size()<<" "<<i_dist->first<<endl;
            //  out_test_data->push_back(static_cast<float> (cl_labels[i_dist->first]));
            *i_outtest = static_cast<float> (cl_labels[i_dist->first]);
        }
        
            print(*out_test_data,"test classification");
        
          cout<<"LDA_mvdisc : doen classify test data "<<Dimensionality_<<endl;
        
        //exit (EXIT_FAILURE);
        return 0;
        
    }
	
}
