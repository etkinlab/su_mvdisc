/*
 *  target.cpp
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 7/27/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#define MAX_LINE_SIZE 1028
#include <misc_utils.h>
#include <cstdlib>
#include "target.h"
//#include <cstdlib>
#include <string.h>
#include <fstream>
#include <sys/time.h>



using namespace std;
using namespace misc_utils_name;
using namespace clapack_utils_name;
namespace su_mvdisc_name{
 
	Target::Target()
	{
       // cout<<"target constructor"<<endl;
       
   //     struct timeval time;
     //   gettimeofday(&time,NULL);
        // microsecond has 1 000 000
        // Assuming you did not need quite that accuracy
        // Also do not assume the system clock has that accuracy.
       // srand((time.tv_sec * 1000) + (time.tv_usec / 1000));
        
		FileHasBeenRead=false;
		hasFileNameBeenSet=false;
        reg_FileHasBeenRead=false;
		reg_hasFileNameBeenSet=false;
		TargetEdited=false;
	//	nodeType = TARGET;
		Kclasses=0;
        classmap_name="";
       // maskname="";
	}
	Target::Target( Target * targ )
	{
    
    //    seed=static_cast<unsigned int>(time(NULL));
     
     
   //     struct timeval time;
  //      gettimeofday(&time,NULL);
        // microsecond has 1 000 000
        // Assuming you did not need quite that accuracy
        // Also do not assume the system clock has that accuracy.
   //     srand((time.tv_sec * 1000) + (time.tv_usec / 1000));
	
       // cout<<"target : copyconstructor "<<endl;
		FileHasBeenRead=targ->FileHasBeenRead;
		hasFileNameBeenSet=targ->hasFileNameBeenSet;
		TargetEdited=targ->TargetEdited;
        reg_FileHasBeenRead=targ->reg_FileHasBeenRead;
		reg_hasFileNameBeenSet=targ->reg_hasFileNameBeenSet;
        
      
		in_target.insert(in_target.end(),targ->out_target.begin(),targ->out_target.end());
		out_target=in_target;
       
        in_regressors.insert(in_regressors.end(), targ->out_regressors.begin(), targ->out_regressors.end());
        out_regressors = in_regressors;
        
            //need to pass on test data to make predictions, beware of introducing bias
        in_test_regressors.insert(in_test_regressors.end(), targ->out_test_regressors.begin(), targ->out_test_regressors.end());
        out_test_regressors = in_test_regressors;
        
        
        strat_labels=targ->strat_labels;
		Kclasses = targ->getNumberOfClasses();
		target_class_index.insert(target_class_index.end(),targ->target_class_index.begin(),targ->target_class_index.end());
		NperClass_ = targ->NperClass_;
		vClasses_ = targ->vClasses_;
        
//        maskname=targ->maskname;
        v_masknames=targ->v_masknames;
        v_test_masknames=targ->v_test_masknames;

        //cout<<"end copy constructor"<<endl;
	}

	Target::~Target()
	{
		//cout<<"taregt destructor"<<endl;
	}
	void Target::setOptions( std::stringstream & ss_options)
	{
		map<string,string> options = parse_sstream(ss_options);
        
		for ( map<string,string>::iterator it = options.begin(); it!=options.end();it++)
        {
//            cout<<"target parse options "<<it->first<<" "<<it->second<<endl;
            if ( it->first == "StratifyLabels")
			{
                
			//	cout<<"startify labels "<<it->first<<" "<<it->second<<endl;
				strat_labels = csv_string2nums_i(it->second);
				print<int>(strat_labels,"sttratify labels");
			}else if ( it->first == "filename")
			{
            //    cout<<"target found file name"<<endl;
                setFileName( it->second );
				                
			}else if ( it->first == "maskname")
			{
                vector<string> masks=csv_2_vector(it->second);
                print(masks,"Vmasknames");
                for ( vector<string>::iterator i_m = masks.begin(); i_m != masks.end(); ++i_m)
                {
                cout<<"target found file maskname "<<*i_m<<endl;
                setMaskName( *i_m );
                }
			}else if ( it->first == "testmaskname")
			{
                vector<string> masks=csv_2_vector(it->second);
                print(masks,"Vmasknames");
                for ( vector<string>::iterator i_m = masks.begin(); i_m != masks.end(); ++i_m)
                {
                    cout<<"target found file maskname "<<*i_m<<endl;
                    setTestMaskName( *i_m );
                }
			}else if ( it->first == "regressors")
			{
              //  cout<<"target found regressors file "<<endl;
                setRegressorsFileName( it->second );
                
			}else if ( it->first == "class_map")
			{
                //cout<<"map target classes"<<endl;
                classmap_name = it->second;
                
			}else {
			//	if (!setTreeNodeOptions(it->first, it->second))
					throw ClassificationTreeNodeException(("Target : Invalid input argument : " + it->first).c_str());
			}
        }
	}
    void  Target::printData()
    {
        misc_utils_name::print<float>(out_target,"out_target");
    }

	void Target::setFileName( const std::string & s )
	{
		filename=s;
		FileHasBeenRead=false;
		hasFileNameBeenSet=true;
	}
    void Target::setMaskName( const std::string & s )
	{
//		maskname=s;
        v_masknames.push_back(s);
		FileHasBeenRead=false;
		hasFileNameBeenSet=true;
	}
    void Target::setTestMaskName( const std::string & s )
	{
        //		maskname=s;
        v_test_masknames.push_back(s);
		FileHasBeenRead=false;
		hasFileNameBeenSet=true;
	}
    void Target::setRegressorsFileName( const std::string & s )
	{
//        cout<<"set regressors filename"<<endl;
		reg_filename=s;
		reg_FileHasBeenRead=false;
		reg_hasFileNameBeenSet=true;
	}
	
	void Target::readFile()
	{
//        cout<<" Target::readFile() "<<FileHasBeenRead<<" "<<in_target.size()<<endl;
		if (!hasFileNameBeenSet)
			throw ClassificationTreeNodeException("Tried to read target file without first setting filename");

		
		if ((!in_target.empty()) || (FileHasBeenRead))
			throw ClassificationTreeNodeException("Tried to read target file whilst target is not empty");

        bool useMap= (classmap_name == "") ? false  : true;
        map<int,int> class_map;
        if (useMap)
        {
            class_map = readClassMap(classmap_name);
            
        }
        
        vector<short> mask;
        vector<short> test_mask;

        //if ( maskname != "" )
        if (! v_masknames.empty())
        {
//            cerr<<"load mask"<<endl;
            for (vector<string>::iterator i_mname = v_masknames.begin(); i_mname != v_masknames.end(); ++i_mname)
            {
               // cout<<"Read mask : "<<*i_mname<<endl;
                //ifstream fmask(maskname.c_str());
                ifstream fmask(i_mname->c_str());
                
                if ( ! fmask.is_open() )
                    throw ClassificationTreeNodeException(("Unable to open mask : " + *i_mname).c_str());
                
                short val;
                if (i_mname == v_masknames.begin()){
                 //   cout<<"newmask "<<endl;
                    while (fmask>>val)
                    {
                        mask.push_back(val);
                    }
                    
                }else{
                    //intersection of masks > 0 criteria
                  //  cout<<"intersect mask "<<endl;

                    vector<short>::iterator i_m =  mask.begin();
                    while (fmask>>val)
                    {
                        //implements intersection of multiple masks
                        *i_m = ( (*i_m>0) && (val>0) ) ? 1 : 0 ;    //if masks are all greater than 0 ==> 1, otherwise 0 
                        ++i_m;
                    }
                }
                    
                fmask.close();
                
            }
        }
//            else{
//           cerr<<"Found no target masks"<<endl;
//        }
        if (! v_test_masknames.empty())
        {
            for (vector<string>::iterator i_mname = v_test_masknames.begin(); i_mname != v_test_masknames.end(); ++i_mname)
            {
//                 cout<<"Read test mask : "<<*i_mname<<endl;
                //ifstream fmask(maskname.c_str());
                ifstream fmask(i_mname->c_str());
                
                if ( ! fmask.is_open() )
                    throw ClassificationTreeNodeException(("Unable to open mask : " + *i_mname).c_str());
                
                short val;
                if (i_mname == v_masknames.begin()){
                    //   cout<<"newmask "<<endl;
                    while (fmask>>val)
                    {
                        mask.push_back(val);
                    }
                    
                }else{
                    //intersection of masks > 0 criteria
                    //  cout<<"intersect mask "<<endl;
                    
                    vector<short>::iterator i_m =  mask.begin();
                    while (fmask>>val)
                    {
                        //implements intersection of multiple masks
                        *i_m = ( (*i_m>0) && (val>0) ) ? 1 : 0 ;    //if masks are all greater than 0 ==> 1, otherwise 0 
                        ++i_m;
                    }
                }
                
                fmask.close();
                
            }
        }
//        }else{
//            cerr<<"Found no target test masks"<<endl;
//        }
        
        ///check for consistency and non-overlap between test masks
        {
            if ( (! mask.empty()) && ( ! test_mask.empty() ))
            {
                
                if (mask.size() != test_mask.size())
                    throw ClassificationTreeNodeException("Input and Test data mask are not the same size!");
                
//                cerr<<"Checking overlap between input and test masks...";
                int overlap=0;
                vector<short>::iterator i_mt = test_mask.begin();
                for (vector<short>::iterator i_m = mask.begin(); i_m  != mask.end();++i_m,++i_mt)
                    overlap+=(*i_m)*(*i_mt);
                if (overlap>0)
                    throw ClassificationTreeNodeException("Input and Test Data masks have overlaping selected subjects");
//                cerr<<"passed."<<endl;
            }
        }            
//        print(mask,"combined mask");
//        print(test_mask,"test mask");
        //mask file or not?
        if (mask.empty())
        {
            ifstream fclasses(filename.c_str());
            if (fclasses.is_open())
            {
                vClasses_.clear();
                target_class_index.clear();
                NperClass_.clear();
                
                //this was origuinally float, not sure why.
                //for regression pusposes
                float cl;
                while (fclasses>>cl)
                {
                    //map input label into something else
                    if (useMap)
                    {
                        map<int,int>::iterator i_map = class_map.find(cl);
                        cl=i_map->second;
                    }
                    in_target.push_back(cl);//static_cast<int>(cl));
                    addClassLabel(cl);
                }
            }else {
                cerr<<"Cannot open "<<filename<<endl;
                exit(EXIT_FAILURE);
            }
            fclasses.close();
        }else{  
          //  cout<<"read in Target"<<endl;
            ifstream fclasses(filename.c_str());
            if (fclasses.is_open())
            {
                vClasses_.clear();
                target_class_index.clear();
                NperClass_.clear();
                
                //this was origuinally float, not sure why.
                //for regression pusposes
                float cl;
                vector<short>::iterator i_mask=mask.begin();
                while (fclasses>>cl)
                {
                 //   cout<<"clin "<<cl<<endl;
                    if (*i_mask>0)
                    {
                        //map input label into something else
                        if (useMap)
                        {
                            map<int,int>::iterator i_map = class_map.find(cl);
                            cl=i_map->second;
                    //        cout<<"map "<<cl<<endl;
                        }
                     //   cout<<"push_back "<<cl<<endl;
                        in_target.push_back(cl);//static_cast<int>(cl));
                        addClassLabel(cl);
                    }
                    ++i_mask;
                }
            }else {
                cerr<<"Cannot open "<<filename<<endl;
                exit(EXIT_FAILURE);
            }
            fclasses.close();
        
        }
      //  print(in_target,"in_target"); 
            
		Kclasses = NperClass_.size();
		out_target = in_target;
        //in_target_orig = in_target;
		FileHasBeenRead=true;
        
        
        //--------------READ EXTRAS REGRESSORS, need to include these in leave one out---------------//
        if (reg_hasFileNameBeenSet)
        {
//            cout<<"Read regressor file."<<reg_FileHasBeenRead<<"."<<reg_filename<<". "<<in_regressors.size()<<endl;
            if ((!in_regressors.empty()) || (reg_FileHasBeenRead))
                throw ClassificationTreeNodeException("Tried to read additional regressors file whilst target is not empty");
            
            if (mask.empty())
            {
                ifstream f_in(reg_filename.c_str());
                if (f_in.is_open())
                {
//                    char* line = new char[MAX_LINE_SIZE];
                    string line;
                     while (getline(f_in,line))//while (f_in.getline(line,MAX_LINE_SIZE))
                    {
                        in_regressors.push_back(csv_string2nums_f(line));
                        //                    print<float>(in_regressors.back(),"readRegs: ");
                    }
                  //  delete[] line;
                }
                f_in.close();
            }else{
                ifstream f_in(reg_filename.c_str());
                if (f_in.is_open())
                {
                    //char* line = new char[MAX_LINE_SIZE];
                    string line;
                    vector<short>::iterator i_mask=mask.begin();
                    while (getline(f_in,line))//f_in.getline(line,MAX_LINE_SIZE))
                    {
                        if ( *i_mask > 0)
                            in_regressors.push_back(csv_string2nums_f(line));
                        //                    print<float>(in_regressors.back(),"readRegs: ");
                        ++i_mask;
                    }
                  //  delete[] line;
                }
                f_in.close();
            }
            
            
            if (in_target.size() != in_regressors.size())
            {
                string r1,r2;
                stringstream ss;
                ss<<in_target.size()<<" "<<in_regressors.size();
                ss>>r1>>r2;
                throw ClassificationTreeNodeException(("Target classes and regressors do not have the same number of entries (rows). \n Target and Regressors have " +r1 + "and" + r2 +" respectively.").c_str() );
            }

            out_regressors = in_regressors;
            reg_FileHasBeenRead=true;
        }
        
//        print(in_target,"   Target:in_target");
        //-----------------------
		if ( ! strat_labels.empty() )
            strat_labels= vClasses_;
	}
	void Target::calculateClassInfo()
	{	
		vClasses_.clear();
		target_class_index.clear();
		NperClass_.clear();
		
		for ( vector<float>::iterator i_targ = out_target.begin(); i_targ!=out_target.end(); ++i_targ )
			addClassLabel(*i_targ);
			
		        
		for ( vector<float>::iterator i_targ = out_target.begin(); i_targ!=out_target.end(); ++i_targ )
		{	
			//find index of class label
			int count=0;
			for (vector<int>::iterator i =  vClasses_.begin(); i!= vClasses_.end(); ++i,++count)
				if (*i_targ == *i)
					target_class_index.push_back(count);
			
		}

		Kclasses = NperClass_.size();
		TargetEdited = false;

		
	}
	
	void Target::copyTarget( Target * targ )
	{
		in_target = targ->in_target;
		//in_target = vector<float>(10,0);


		out_target = targ->out_target;

        in_regressors = targ->in_regressors;
        out_regressors = targ->out_regressors;
        
        in_test_regressors = targ->in_test_regressors;
        out_test_regressors = targ->out_test_regressors;
        
		Kclasses = targ->getNumberOfClasses();

		target_class_index = targ->target_class_index;
		NperClass_ = targ->NperClass_;
		vClasses_ = targ->vClasses_;
	}

	
	std::vector<float>::iterator Target::begin()
	{   
       // cout<<"outtargsize "<<out_target.size()<<endl;
		return out_target.begin();
	}
	std::vector<float>::iterator Target::end()
	{
		return out_target.end();
	}
	void Target::erase( vector<float>::iterator & it )
	{
		out_target.erase(it);
		TargetEdited = true;
	}

	
	std::vector<int>::iterator Target::class_index_begin()
	{
		return target_class_index.begin();
	}
	std::vector<int>::iterator Target::class_index_end()
	{
		return target_class_index.end();
	}
	
		void Target::setTargetData(  const clapack_utils_name::smatrix &  m_target)
		{
			int Ncols = m_target.NCols;
			int Nrows = m_target.MRows;
			int Nele = Ncols * Nrows;
			if ((Ncols != 1) && (Nrows != 1))
			            throw ClassificationTreeNodeException("Target:setTargetData - Invalid Matrix Size ");
			
			in_target.resize(Nele,0);
					memcpy(&(in_target[0]), m_target.sdata, sizeof(float)*in_target.size());
				out_target=in_target;
			calculateClassInfo();

		}
	void Target::setTargetData(  vector<float>   target)
	{
	//	cout<<"set target data "<<endl;
		//in_target.push_back(0);
	//	cout<<"set target data2 "<<endl;
					
		in_target=target;
        //in_target_orig = in_target;
	//	cout<<"print target "<<endl;
		//print<float>(target,"new target");
		
		//defaults output is input (i.e. no selection mask)
		out_target=target;
		cout<<"classinfo "<<endl;
		calculateClassInfo();

	}		
    void Target::permuteTargetData( const vector<int> & v_sel_mask_in )
    {
        //for now not permutaing all groups not allowing selection
        // cout<<"permuet target data "<<static_cast<unsigned int>(time(NULL))<<" "<<time(NULL)<<" "<<endl;
        //  srand ( seed );
        //   in_target_orig = in_target;
        //copy because going to pop out
        vector<int> v_sel_mask = v_sel_mask_in;
        
        cerr<<"permute target data"<<endl;
        if (v_sel_mask.empty())//select all subject
            v_sel_mask.resize(in_target.size(),1);
        
        print(v_sel_mask,"v_sel_mas");

        
        vector<int> v_inds, v_inds_elim;
        int ind=0;
        for (vector<int>::const_iterator i = v_sel_mask.begin() ; i != v_sel_mask.end(); ++i,++ind) {
            if ( *i > 0 )
                v_inds.push_back(ind);
        }
        v_inds_elim = v_inds;
        
        vector<float> targ_data = in_target;//_orig;
                                            //need no initialize before trimming
                                            //vector<float> targ_rand;
        rand() ;//for some reaosn the ifrst rand was always the same, so call rand once
//        out_target.resize(targ_data.size());
  //copy in originial data then only replace those within maks
        out_target = in_target;
//        cerr<<"before "<<endl;
        for ( vector<int>::iterator i = v_inds.begin(); i != v_inds.end();++i)
            cerr<<out_target[*i]<<" ";
        cerr<<endl;
        unsigned int index=0;
        while ( (! v_inds_elim.empty()) ) {
            unsigned int sample = rand() % v_inds_elim.size();
//            cerr<<v_inds_elim[sample]<<endl;
//            cerr<<"Sample "<<sample<<"- "<<v_inds_elim.size()<<" "<<v_inds_elim[sample]<<endl;
            ////sample = sample % targ_data.size();
            // cout<<"Sample2 "<<sample<<"- "<<targ_data.size()<<endl;
            //set target at original index with one from new randomized index
            out_target[v_inds[index]]=targ_data[v_inds_elim[sample]];
            v_inds_elim.erase(v_inds_elim.begin()+sample);
            ++index;
            
        }
//        cerr<<"after "<<endl;
//        for ( vector<int>::iterator i = v_inds.begin(); i != v_inds.end();++i)
//            cerr<<out_target[*i]<<" ";
//        cerr<<endl;
        //set input and output in case selection mask is not applied
        // out_target = in_target;
        
//        print(in_target,"permuted outpinoputut target");
        print(out_target,"permuted output target");
        //keeps on makign sure that the seed changes every time this is called
        //  seed+=rand();
    }

    void Target::permuteTargetData(){
        //for now not permutaing all groups not allowing selection
       // cout<<"permuet target data "<<static_cast<unsigned int>(time(NULL))<<" "<<time(NULL)<<" "<<endl;
      //  srand ( seed );
     //   in_target_orig = in_target;
        //copy because going to pop out
        vector<float> targ_data = in_target;//_orig;
        //need no initialize before trimming
        //vector<float> targ_rand;
        rand() ;//for some reaosn the ifrst rand was always the same, so call rand once
        out_target.resize(targ_data.size());
        unsigned int index=0;
        while ( (! targ_data.empty()) ) {
            unsigned int sample = rand() % targ_data.size();
            
            //cout<<"Sample "<<sample<<"- "<<targ_data.size()<<endl;
            ////sample = sample % targ_data.size();
             // cout<<"Sample2 "<<sample<<"- "<<targ_data.size()<<endl;
            out_target[index]=targ_data[sample];
            targ_data.erase(targ_data.begin()+sample);
            ++index;
       
        }
        //set input and output in case selection mask is not applied
       // out_target = in_target;
        
        print(in_target,"permuted outpinoputut target");
        print(out_target,"permuted output target");
        //keeps on makign sure that the seed changes every time this is called
      //  seed+=rand();

        
    }

    
	template<class T>
	vector<T> Target::getTargetData() const 
	{
        // cerr<<"Target : getTargetData "<<in_target.size()<<" "<<out_target.size()<<endl;
		vector<T> targ; 
		for (vector<float>::const_iterator i = out_target.begin(); i!=out_target.end();i++)
		{
       //    cerr<<"targ "<<*i<<endl;
			targ.push_back(static_cast<T> (*i));
		}
		return targ;
	}
	template vector<int> Target::getTargetData<int>() const;
	template vector<float> Target::getTargetData<float>() const;

    template<class T>
	vector<T> Target::getTargetData(const vector<int> & mask) const 
	{
		vector<T> targ; 
		for (vector<float>::const_iterator i = out_target.begin(); i!=out_target.end();i++)
		{
            // cout<<"targ "<<*i<<endl;
			targ.push_back(static_cast<T> (*i));
		}
		return targ;
	}
	template vector<int> Target::getTargetData<int>(const vector<int> & mask) const;
	template vector<float> Target::getTargetData<float>(const vector<int> & mask) const;
    
    void Target::getTargetData(  smatrix & data ) const
    {
        data.resize(out_target.size(), 1);
        memcpy(data.sdata,&(out_target[0]), out_target.size()*sizeof(float));
    }

    
    
    
    
    unsigned int Target::getTestRegressorData( std::vector<float> & regs) const
    {
        ///collapses 2D into 1D to play nice with LAPACK/BLAS
        if (out_test_regressors.empty())
            throw ClassificationTreeNodeException("Tried to get Test Regressor, but does not exists.");
        
        regs.resize(out_test_regressors.size()*out_test_regressors.front().size());
        
        vector<float>::iterator i_r=regs.begin();
        for (vector< vector<float> >::const_iterator i = out_test_regressors.begin(); i!=out_test_regressors.end();i++)
		{
            
            for ( vector<float>::const_iterator ii = i->begin(); ii != i->end(); ++ii,++i_r)
                *i_r=*ii;
        }
        
        if (out_test_regressors.size()==0)
            return 0;
		return out_test_regressors.front().size();

    }

    unsigned int Target::getRegressorData(vector<float> & regs) const
    {
           ///collapses 2D into 1D to play nice with LAPACK/BLAS
        regs.resize(out_regressors.size()*out_regressors.front().size());
     
        vector<float>::iterator i_r=regs.begin();
        for (vector< vector<float> >::const_iterator i = out_regressors.begin(); i!=out_regressors.end();i++)
		{
     
            for ( vector<float>::const_iterator ii = i->begin(); ii != i->end(); ++ii,++i_r)
                *i_r=*ii;
        }
        
        if (out_regressors.size()==0)
            return 0;
		return out_regressors.front().size();
	}
    unsigned int Target::getNumberOfRegressors(){
        if (out_regressors.size()==0)
            return 0;
        return out_regressors.front().size();
    }
    
	int Target::getClassLabel( const unsigned int & index )
	{
			if ( index>=vClasses_.size() )
				throw ClassificationTreeNodeException("Tried to get invalid class label");
	
		
		return vClasses_[index];
	}
	unsigned int Target::getClassIndex( const int & label ) const 
	{
		unsigned int index =0;
	
		for ( vector<int>::const_iterator i_t = vClasses_.begin(); i_t != vClasses_.end(); ++i_t,++index)
		{
			if ( (*i_t) == label )
				return index;
		}
		
		throw ClassificationTreeNodeException("Was unable to find the specified class label");
		
		return 0;
	}

	unsigned int Target::getNumberOfClasses() 
	{
		if (TargetEdited)
			calculateClassInfo();
		return Kclasses;
	}
	int Target::getInTarget( const unsigned int & index )
	{
		return in_target[index];
	}
	
	int Target::getOutTarget( const unsigned int & index )
	{
        //print(in_target,"in_target");
        //print(out_target,"out_target");

      //  cerr<<"getOutTarget "<<index<<"/"<<out_target.size()<<" "<<out_target[index]<<endl;
		return out_target[index];
	}



	vector<unsigned int> Target::getNumberOfSubjectsPerClass() const
	{
		return NperClass_;
	}
    
    vector<unsigned int> Target::getNumberOfSubjectsPerClass(const vector<int> & v_mask) const
    {
        if (out_target.size() != v_mask.size())
            throw ClassificationTreeNodeException(("Target : mask size does not match target size. " + num2str(out_target.size()) + " / " +  num2str(v_mask.size())).c_str() );
        
        vector<unsigned int> n_per_class(Kclasses,0);

        vector<float>::const_iterator i_targ = out_target.begin();
        for ( vector<int>::const_iterator i_m = v_mask.begin(); i_m != v_mask.end(); ++i_m, ++i_targ)
        {
            if (*i_m > 0 )
            {
                vector<unsigned int>::iterator i_per = n_per_class.begin();
                for (  vector<int>::const_iterator i_c = vClasses_.begin(); i_c != vClasses_.end(); ++i_c,++i_per)
                    if (*i_c == *i_targ)
                        *i_per += 1 ; 
                
            }
          
            
        }
        return n_per_class;
    }
	
	void Target::addClassLabel( const int & cl )
	{
		if ( vClasses_.empty() )//add back first class
		{
			vClasses_.push_back(cl);
			NperClass_.push_back(1);
		}else {
			vector<int>::iterator i_c = vClasses_.begin();
			//unsigned int index=0;
			vector<unsigned int>::iterator i_n_per_c = NperClass_.begin();

			for ( ; i_c != vClasses_.end(); i_c++,i_n_per_c++)
			{                
				if (cl == *i_c ) //break if class already found
				{
					(*i_n_per_c)++;
					break;
				}else if (cl<*i_c) //insert one that has nto been found (stores as ordered)
				{                    
					vClasses_.insert(i_c,cl);
					NperClass_.insert(i_n_per_c,1);
					break;
				}
			}
			
			if (cl>vClasses_.back()) //if reached the end and haven't found class add to back
			{
				vClasses_.push_back(cl);
				NperClass_.push_back(1);
				
			}
		}
		
	}
	
	template<class T>
	void Target::applySelectionMask( const std::vector<T> & selection_mask ,  const std::vector<T> & selection_test_mask , bool clearTestRegressors )
	{
        //selection_test_mask is only used for rgeressors, for passing down appropriate test data
        //the last paarmeter if set to zero (default true), will force the slection mask process 
        //to not clear the test regressor. e.g. used in group selection filtermask 
        //shoudl only not be cleared in the middle of the pipline
        //cout<<"apply selection mask regressoords"<<endl;
		vClasses_.clear();
		target_class_index.clear();
		NperClass_.clear();
		out_target.clear();
        out_regressors.clear();

        //this 
        if (clearTestRegressors)
        {
           // cout<<"clear out Regressors"<<endl;
            out_test_regressors.clear();
        }
        //dont clear out test regressors...this differs because its the test not training
        //wasn't sure if it be best to due through nodes or leave in target
        //left in target
        //out_test_regressors.clear();
		//load in classes
		//initialize perclass subject counter (NperClass_)

//        print(selection_mask,"targ sel mask");
  //      print(in_target,"intaregt");
        typename vector<T>::const_iterator i_mask = selection_mask.begin();
		for (vector<float>::const_iterator i_k = in_target.begin() ; i_k!=in_target.end(); i_k++, i_mask++)
		{
			if ( *i_mask !=0 ) //to make generic so that only classes within selection mask are used
			{
              //  cerr<<"add "<<*i_k<<endl;
				addClassLabel(*i_k);
			}//else
             //   cerr<<"omit "<<*i_k<<endl;
		}
		Kclasses=vClasses_.size();
	//	//cout<<"Number of Classes "<<Kclasses<<endl;
		
		if (Kclasses==0)
			throw ClassificationTreeNodeException("Tried to apply selection mask but no classes were found");
		
		//print<int>(vClasses_,"Classes found");
		
		//print<unsigned int>(NperClass_,"Inital Subject per Class Counter");
		
		///apply the slection mask
		//combine data from all sources
		//subject refers to row of input data, rows could be input multiple times in bootstrapping case (why not called sample)
		
		NperClass_.assign(NperClass_.size(),0);
		
		
		i_mask = selection_mask.begin();
		vector<float>::const_iterator i_targ = in_target.begin();
        
        //print(in_target,"in_target : apply selection maks");
     //   cout<<"outtarget "<<endl;
		unsigned int sample_count=0;//used for safety check
		for (unsigned int subject=0;subject<selection_mask.size();subject++,i_mask++,++i_targ)
		{			
			//by allowing multiple inputs of same sample it facilitates bootstrapping
			for ( int NperSubject=0; NperSubject< *i_mask ; NperSubject++)
			{
				sample_count++;
				out_target.push_back(*i_targ); //add user input classification
              //  cout<<out_target.back()<<" ";
               // out_regressors.insert(out_regressors.end(),i_reg->begin(), i_reg->end() );
                
                //only use regressors if it has been set
           //     if (reg_hasFileNameBeenSet)
             //       out_regressors.push_back(*i_reg);
               
				//create mapping of targets
				unsigned int count=0;
				for (vector<int>::iterator i =  vClasses_.begin(); i!= vClasses_.end(); i++,count++)
				{
					if (*i_targ == *i)
					{
						target_class_index.push_back(count);
						NperClass_.at(count)++;//count number of subject per class
					}
				}
			}
            //only use regressors if it has been set
            //if (reg_hasFileNameBeenSet)
              //  ++i_reg;
		}
	//	cout<<endl;
        //cout<<"applysel make to regressors "<<reg_hasFileNameBeenSet<<endl;
        ///---------------appoly to regressors------------------//
        if (reg_hasFileNameBeenSet)
        {
            //cout<<"applying selection mask REGRESSORS"<<endl;
            i_mask = selection_mask.begin();
            typename vector<T>::const_iterator i_test_mask = selection_test_mask.begin();

            sample_count=0;//used for safety check
            vector< vector<float> >::const_iterator i_reg = in_regressors.begin();
            for (unsigned int subject=0;subject<selection_mask.size();++subject ,++i_mask ,++i_test_mask, ++i_reg)
            {
                //cout<<"subject "<<subject<<" "<<*i_mask<<" "<<*i_test_mask<<endl;
                for (  int NperSubject=0; NperSubject< *i_mask ; NperSubject++)
                {
                    //by allowing multiple inputs of same sample it facilitates bootstrapping
                    sample_count++;
                    out_regressors.push_back(*i_reg);
                }
                
                //pass down test regressors. Need to make sure its not part of trainng and is
                //included in test set
                if ((*i_mask == 0 ) && (*i_test_mask != 0)) 
                {
                    //cout<<"add test regressor"<<endl;
                    out_test_regressors.push_back(*i_reg);
                }
                //cout<<"done subject"<<endl;
            }
            //cout<<"done applying selection mask REGRESSORS "<<out_test_regressors.size()<<" "<<in_test_regressors.size()<<endl;

        }
        
        
        
        //-----------------------------------------------------//
        
        
        
		//print<int>(out_target,"Target: out_target");
//		print<int>(target_class_index,"target class map");
	//	//cout<<"Done loading target"<<endl;
		
	}

	template void Target::applySelectionMask<int>( const std::vector<int> & selection_mask, const std::vector<int> & selection_test_mask, bool clearTestRegressors);
	template void Target::applySelectionMask<short>( const std::vector<short> & selection_mask , const std::vector<short> & selection_test_mask, bool clearTestRegressors );

	
	float Target::expected_prediction_error( const vector<int3> & estimate ) const
	{
		//cout<<"Startestimate "<<endl;
	//	for (unsigned int i =0 ; i<estimate.size();i++)
			//cout<<" "<<estimate[i].x<<" "<<estimate[i].y<<" "<<estimate[i].z<<" "<<in_target[estimate[i].x]<<" /";
	//	cout<<endl;
		
//		int N_runs = estimate.back().z + 1;//plus one because zero indexing
//		int N_runs = 1; //assume that estimate is from one run
	//	vector<float> pred_err(N_runs,0);
	//	vector<int> v_N_runs(N_runs,0);
		int Nsamples = 0 ;
		float pred_err = 0 ; 
		
//		vector<float>::const_iterator i_targ = in_target.begin();
	
		int index=0;
		for (vector<int3>::const_iterator i_est = estimate.begin(); i_est!= estimate.end(); i_est++,index++)
		{
//			cout<<"estimate "<<i_est->x<<" "<<i_est->y<<" "<<i_est->z<<" "<<endl;

			if ( in_target[i_est->x] == i_est->y )
				++pred_err;

			++Nsamples;

		}

		return pred_err/Nsamples;
		
	}

	std::vector<float> Target::perclass_expected_prediction_error( const std::vector<int3> & estimate ) const 
	{
		
		//		int N_runs = estimate.back().z + 1;//plus one because zero indexing
		//int N_runs = 1; //assume that estimate is from one run
		vector<float> pred_err( Kclasses , 0);
		vector<int> Nsamples( Kclasses , 0);
	
//		vector<float>::const_iterator i_targ = in_target.begin();
		
		int index=0;
		for (vector<int3>::const_iterator i_est = estimate.begin(); i_est!= estimate.end(); i_est++,index++)
		{
		//	cout<<"estimate "<<i_est->x<<" "<<i_est->y<<" "<<i_est->z<<" "<<endl;
			unsigned int class_index = getClassIndex(in_target[i_est->x]);

			if ( in_target[i_est->x] == i_est->y )
				++pred_err[class_index];

			++Nsamples[class_index];

		}
	
		divide<float,int>(pred_err,Nsamples);

		return pred_err;///Nsamples;
		
	}
	
	std::vector<float> Target::perrun_expected_prediction_error( const vector< vector<int3> > & estimates ) const 
	{
		
		//		int N_runs = estimate.back().z + 1;//plus one because zero indexing
		int N_runs=1;// = 1; //assume that estimate is from one run

		
		//find number of runs
		for ( vector< vector<int3> >::const_iterator i = estimates.begin(); i != estimates.end(); ++i)
        {
            //cout<<"esimate ind "<<i->size()<<endl;
			for (vector<int3>::const_iterator i_est = i->begin(); i_est!= i->end(); i_est++)
            {
				if ( N_runs < i_est->z ) N_runs = i_est->z + 1;//i_est_z range from 0 -> Nruns-1
            
            }
        }
        //cout<<"Nruns "<<N_runs<<endl;
        vector<float> pred_err( N_runs , 0);

		
		
		vector<int> Nsamples( N_runs , 0);
		
//		vector<float>::const_iterator i_targ = in_target.begin();
		//cout<<"estimates "<<estimates.size()<<" "<<N_runs<<" "<<Nsamples.size()<<endl;
//      
        
		for ( vector< vector<int3> >::const_iterator i = estimates.begin(); i != estimates.end(); ++i)
			for (vector<int3>::const_iterator i_est = i->begin(); i_est!= i->end(); i_est++)
			{
      //          cout<<"i_estx "<<i_est->x<<" "<<in_target.size()<<endl;
                if ( in_target[i_est->x] == i_est->y )
                    ++pred_err[i_est->z];
//
//    cout<<"nsamples "<<i_est->z<<" "<<Nsamples[i_est->z]<<endl;
    //        //    Nsamples[i_est->z] = Nsamples[i_est->z] + 1;
                ++Nsamples[i_est->z];
		//	
		}
		//cout<<"class pred "<<pred_err[0]<<" "<<pred_err[1]<<endl;
		//cout<<"class predN "<<Nsamples[0]<<" "<<Nsamples[1]<<endl;
		
		divide<float,int>(pred_err,Nsamples);
		//cout<<"class pred2 "<<pred_err[0]<<" "<<pred_err[1]<<endl;
		
        //cout<<"pred"<<endl;
		//return mean(pred_err);
       // vector<float> blag;
		return pred_err;///Nsamples;
    //    return blag;
	}
	
	void Target::prediction_error( const std::vector<int3> & estimate, float & mean_out, float & std_err_out ) const 
	{
		
		int N_runs = estimate.back().z + 1;//plus one because zero indexing
		vector<float> pred_err(N_runs,0);
		vector<int> v_N_runs(N_runs,0);
		//unsigned int count = 0;
//		vector<float>::const_iterator i_targ = in_target.begin();
		//cout<<"Prediction/Acutal: "<<endl;
	//	for (int i = 0 ; i < N_runs; i ++)
	//		cout<<" "<<i<<"   ";
	//	cout<<endl;
		int index=0;
		for (vector<int3>::const_iterator i_est = estimate.begin(); i_est!= estimate.end(); i_est++,index++)
		{
				if ( in_target[i_est->x] == i_est->y )
				{
					pred_err[i_est->z]++;
				}
				v_N_runs[i_est->z]++;
		//	cout<<index<<":"<<i_est->y<<"/"<<in_target[i_est->x]<<"  ";
		}
		
		divide<float,int>(pred_err,v_N_runs);
		print<float>(pred_err,"Prediction Accuracy");
		//cout<<endl;
		
		mean_out = mean<float,float>(pred_err);
		std_err_out = std_err(pred_err);	
	
		
	}

	
}
