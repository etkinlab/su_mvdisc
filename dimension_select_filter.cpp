/*
 *  group_select_filter.cpp
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 11/12/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


//its own header file
#include "dimension_select_filter.h"


using namespace std;
using namespace misc_utils_name;


namespace su_mvdisc_name{
	DimensionSelect_Filter::DimensionSelect_Filter()
	{
		enum FILTER_MODE { INCLUDE, EXCLUDE };
		
		FILTER_MODE filt_mode;
		NodeName="DimensionSelect_Filter";
		filt_mode=INCLUDE;
        _dimTrim=0;
	}
	
	DimensionSelect_Filter::~DimensionSelect_Filter()
	{
	}
	
	
	void DimensionSelect_Filter::setOptions( std::stringstream & ss_options)
	{
		map<string,string> options = parse_sstream(ss_options);
		for ( map<string,string>::iterator it = options.begin(); it!=options.end();it++)
			if ( it->first == "dim")
			{
			//	cout<<"set groups"<<endl;
				_select_dimensions = csv_string2nums_uint(it->second);
				print<unsigned int>(_select_dimensions,"dimensions to keep ");
//				select_groups.push_back(groups);
			}else if ( it->first == "mode")
			{
			//	cout<<"set mode "<<endl;
				if (it->second.compare("INCLUDE") == 0)
					filt_mode=INCLUDE;
				else if (it->second.compare("EXCLUDE") == 0)
					filt_mode=EXCLUDE;
				else {
					throw ClassificationTreeNodeException("Unrecognized Filter Mode. Options are \"include\" and \"exclude\".");

				}

			}else {
				if (!setTreeNodeOptions(it->first, it->second))
					throw ClassificationTreeNodeException((NodeName + " : Invalid input argument : " + it->first).c_str());
			}
		
			if ((filt_mode == INCLUDE) && (_select_dimensions.size()<1))
				throw ("Dimension Select Mode = INCLUDE is not compatible with zero dimensions");
				
	}
    void DimensionSelect_Filter::setDimension( const unsigned int & dim  )
    {
        _select_dimensions.clear();
        _select_dimensions.push_back(dim);
    }

	
	void DimensionSelect_Filter::runSpecificFilter(  bool with_estimation )
	{		
		
		cout<<"Run dimension select filter "<<Dimensionality_<<" "<<NumberOfSamples_<<endl;
		if (out_data == NULL)
			out_data = new vector<float>();
		
		copyToSingleVector( in_data, out_data, NumberOfSamples_ );
        cout<<"Run dimension select filter - done copy "<<Dimensionality_<<" "<<NumberOfSamples_<<endl;

        _dim_mask.clear();
        if (filt_mode==INCLUDE)
        {
            _dim_mask.resize(Dimensionality_,0);
            for (vector<unsigned int>::iterator i = _select_dimensions.begin(); i != _select_dimensions.end(); ++i)
                _dim_mask[*i]=1;
        }else if(filt_mode==EXCLUDE)
        {
            _dim_mask.resize(Dimensionality_,1);
            for (vector<unsigned int>::iterator i = _select_dimensions.begin(); i != _select_dimensions.end(); ++i)
            _dim_mask[*i]=0;
        }else{
            throw ClassificationTreeNodeException("Exception. DimensionSelect_Filter invalid filt_mode");
        }

        unsigned int Dim_old=Dimensionality_;
        //apply feature filter will alter this
        print(_dim_mask,"_dim_mask");
        cerr<<"dim old "<<Dim_old<<" "<<in_data.size()<<" "<<in_data_names_.size()<<endl;
        cerr<<"dim old2 "<<Dim_old<<" "<<in_data[0]->size()<<" "<<in_data_names_[0]->size()<<endl;

        applyFeatureFilterToData( in_data, _dim_mask, out_data, NumberOfSamples_, Dimensionality_ );
        if ( ! in_data_names_.empty())
            if (! in_data_names_[0]->empty())
            {
                cerr<<"Apply feature selection to names"<<endl;
                applyFeatureFilterToData( in_data_names_ , _dim_mask, out_data_names_, 1, Dim_old );
            }
        _dimTrim = Dim_old - Dimensionality_;//apply this at the end of the test data application in case where there is no test data
		cout<<"Run dimension select filter end "<<Dimensionality_<<" "<<NumberOfSamples_<<" "<<Dim_old<<endl;

//        cerr<<"dim old2 "<<Dim_old<<endl;

//        print(*out_data,"dimSel out_data");
	}
	void DimensionSelect_Filter::applyToTestData()
	{
         cout<<"Run applytotestdaat select filter "<<in_test_data.size()<<" "<<NumberOfTestSamples_<<" "<<Dimensionality_<<endl;
        if (out_test_data == NULL)
			out_test_data = new vector<float>();
        //print(* (in_test_data[0]),"in_test_data");
        unsigned int Dim_old=Dimensionality_+_dimTrim;

        
        //final call will adjust 
        applyFeatureFilterToData( in_test_data, _dim_mask, out_test_data, NumberOfTestSamples_, Dim_old );
        //  print(*out_test_data,"out_test_data");
        cout<<"Run dimension select filter end "<<Dimensionality_<<" "<<NumberOfSamples_<<" "<<endl;

    
    
    }
	
	
	
}
