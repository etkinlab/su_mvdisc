/*
 *  GLM_filter.cpp
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 10/19/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
//its own header file
#include <cstdlib>

#include "glm_filter.h"


//STL includes
#include <algorithm>
//Custom includes
//#include <misc_utils/misc_utils.h>
#include <misc_utils.h>
#include <clapack_utils.h>
//#include <clapack_utils/clapack_utils.h>
#include <cmath>
#include <data_source.h>
//3rd party include 

//OSX include
//OSX include                                                                                                                                         
#ifdef ARCHLINUX
extern "C" {
#include "cblas.h"
#include "f2c.h"
  //typedef long int __CLPK_integer;                                                                                                                  
  //typedef float  __CLPK_real;                                                                                                                       

  typedef integer __CLPK_integer;
  typedef real __CLPK_real;
#include "clapack.h"
}

#else
#include <Accelerate/Accelerate.h>
#endif


using namespace std;
using namespace misc_utils_name;
using namespace clapack_utils_name;
namespace su_mvdisc_name{
	GLM_Filter::GLM_Filter()
	{
		NodeName="GLM_Filter";
		output_basename="output_"+NodeName;
		F_stats=NULL;
        estimated_betas=NULL;
		filt_mode=FTEST_ALL_PAIRS;
		threshold=0;
		order_threshold=0;
        preserveMean=0;
        //initialized in baseclass instead
    //    DimTrim=0;
	//	Dimensionality_=0;
	//	NumberOfSamples__=0;
	//	NumberOfTestSamples_=0;
//		Kclasses=0;
		
		
		stats_threshF=true;
		thresh_mode = ORDER_STATISTIC;
		max_F=0;
        B=1000;
	}
	
	GLM_Filter::~GLM_Filter()
	{
		if (F_stats!=NULL)
			delete[] F_stats;
        if (estimated_betas!=NULL)
            delete[] estimated_betas;
	}
	
	
	void GLM_Filter::setOptions( std::stringstream & ss_options)
	{
		map<string,string> options = parse_sstream(ss_options);
			for ( map<string,string>::iterator it = options.begin(); it!=options.end();it++)
				if ( it->first == "threshold")
				{
					stringstream ss; 
					ss<<it->second;
					ss>>threshold;
				}else if ( it->first == "threshold_mode")
				{
					if (it->second == "Absolute"){
						thresh_mode = ABSOLUTE;
					}else if (it->second == "Fractional") {
						thresh_mode = FRACTIONAL;
					}else if (it->second == "OrderStatistic") {
						thresh_mode = ORDER_STATISTIC;
					}else {
						throw ClassificationTreeNodeException(("GLM_Filter: Invalid input into threshold_mode, "+it->second+".").c_str());
					}


				}else if ( it->first == "filter_mode")
				{
					if (it->second == "Residuals"){
                        cout<<"set to residual mode"<<endl;
						filt_mode = RESIDUALS;
					}else if (it->second == "Residuals-NoApply"){
                        cout<<"set to residual mode without application to test set"<<endl;
						filt_mode = RESIDUALS_NOAPPLY;
					}else if (it->second == "Bootstrap") {
						filt_mode = BOOT_FTEST_ALL_PAIRS;
					}else if (it->second == "Ftest_all_pair") {
						filt_mode = FTEST_ALL_PAIRS;
					}else {
						throw ClassificationTreeNodeException(("GLM_FIlter: Invalid input into filter_mode, "+it->first+".").c_str());
					}
                    
                    
				}else if ( it->first == "BootSamples")
				{
					B=string2num<unsigned int>(it->second);
                    
                    
				}else if ( it->first == "preserveMean"){
					string s_preserveMean = it->second;
					if ((s_preserveMean == "true") || (s_preserveMean == "1"))
						preserveMean = false;
					else if ((s_preserveMean == "false") || (s_preserveMean == "0"))
						preserveMean = true;
					else 
						throw ClassificationTreeNodeException("Image Source: invalid input to NormalizeDataPreMask. ");
					
				}//else if ( it->first == "TrackStats"){
			//		if (it->second == "YES"){	
			//			PassDownOutput=true;
			//			trackStats=true;
			//		}else
			//			trackStats=false;
			//	}
                else if ( it->first == "TrackStats_useF"){
					string s_useF = it->second;
					if ((s_useF == "true") || (s_useF == "1"))
						stats_threshF = false;
					else if ((s_useF == "false") || (s_useF == "0"))
						stats_threshF = true;
					else 
						throw ClassificationTreeNodeException("Image Source: invalid input to NormalizeDataPreMask. ");
					
				}else {
					if (!setTreeNodeOptions(it->first, it->second))
						throw ClassificationTreeNodeException((NodeName + " : Invalid input argument : " + it->first).c_str());
				}
		
		
	}
	vector<float> GLM_Filter::getParameters()
	{
		unsigned int nparams = Dimensionality_+DimTrim;
		vector<float> params(nparams);
		unsigned int count=0;
		for (vector<float>::iterator i = params.begin(); i != params.end(); ++i,++count)
        {
			*i = F_stats[count];
           // cout<<"get params "<<count<<" "<<F_stats[count]<<endl;
        }
            
            return  params;
		
	}
	void GLM_Filter::getParameters( std::vector< std::list<float> > & all_params)
	{
		unsigned int nparams = Dimensionality_+DimTrim;

		if (all_params.empty())
		{
			list<float> params;
			all_params.assign(nparams,params);
		}else if (nparams != all_params.size())
		{
			throw ClassificationTreeNodeException("NumberOfExisting parameters does not match the number being added");
		}
			
		unsigned int count=0;
		for (vector< list<float> >::iterator i_p = all_params.begin(); i_p != all_params.end(); ++i_p,++count)
			i_p->push_back(F_stats[count]);

	}

	void GLM_Filter::setParameters( const std::vector< float > & params)
	{
		unsigned int count = 0;
		for (vector<float>::const_iterator i_p = params.begin(); i_p != params.end(); ++i_p,++count)
			F_stats[count] = *i_p; 
	}
	void GLM_Filter::setParameters( const std::vector<float> & params, const std::vector<unsigned int> & mask )
	{
		//--------------------
		if (Verbose)
			cout<<"Set GLM parameters (F-statistic) - Number of Parameters : "<<params.size()<<endl;
		//--------------------

		//mask out unused parameters
		unsigned int count = 0;
		vector<unsigned int>::const_iterator i_m = mask.begin();
		for (vector<float>::const_iterator i_p = params.begin(); i_p != params.end(); ++i_p,++count,++i_m)
		{	
			if ( *i_m == 0 )
			{
				F_stats[count] = -1;
			}else {
				F_stats[count] = *i_p; 
			}
		}
		}

	void GLM_Filter::setIOoutputSpecific()
	{
         if ((filt_mode != RESIDUALS) && (filt_mode != RESIDUALS_NOAPPLY))
         {
             setIOoutputSpecificFeatureSelect();
         }else{
             setIOoutputSpecificResiduals();
         }
             
	
	}

    void GLM_Filter::setIOoutputSpecificFeatureSelect()
    {
       // cout<<"setiooutput specific ot GLMfilter "<<Dimensionality_<<" "<<DimTrim<<endl;
		
		float thresh = threshold;
		
		//	cout<<"Threshmode "<<thresh_mode<<endl;
		if ( thresh_mode == FRACTIONAL )
		{
			thresh *= max_F;
		}else if ( thresh_mode == ORDER_STATISTIC )
		{
			thresh = order_threshold = orderStatistic(F_stats, Dimensionality_+DimTrim, threshold);
			if (Verbose)
				cout<<"Using Order Statistic... "<<endl;
			
		}
		
		//mask the features
		unsigned int index =0;
		for (vector<bool>::iterator i_mask = output_feature_mask_->begin(); i_mask != output_feature_mask_->end(); ++i_mask)
            if (*i_mask)
            {
                //need <= because used > for inclusion
                if (F_stats[index]<=thresh)//opposite of before, set to zero those whcih are masked out
                {
                    *i_mask=0;
                    
                }
                ++index;
            }
        
        // cout<<"trimmed "<<index<<" "<<thresh<<" "<<output_feature_mask->size()<<endl;
    }

    void GLM_Filter::setIOoutputSpecificResiduals()
    {
        
    }

	unsigned int GLM_Filter::getNumberOfParameters()
	{
		return  Dimensionality_+DimTrim;
	}

    void GLM_Filter::estimateParameters()
    {
        cout<<"GLM_Filter::estimateParameters"<<endl;
        if ((filt_mode == RESIDUALS) || (filt_mode == RESIDUALS_NOAPPLY) )
            estimateParametersResiduals();
        else if (filt_mode == BOOT_FTEST_ALL_PAIRS )
        {
          
            estimateParametersFeatureSelectBootStrap();
        }else 
            estimateParametersFeatureSelect();
//        cout<<"done estimate paramaters"<<endl;

    }
    void GLM_Filter::estimateParametersResiduals()
    {
        cerr<<"Estimating parameters for GLM_Filter (Compute Residuals)... "<<Dimensionality_<<endl;

		if (Verbose)
			cout<<"Estimating parameters for GLM_Filter (Compute Residuals)... "<<Dimensionality_<<endl;
		DimTrim=0;
		//not test for multiple contrasts,
		
		if (out_data == NULL)
			out_data = new vector<float>();
		//copyToSingleVector( in_data, out_data, NumberOfSamples_ );
        
        //the reason for the overkill is for future extensibility to any arbitrary design
	//	vector<float> X = training_target->getTargetData<float>(); //deisgn matrix
//		print<float>(X,"TARGET ");
	
     
        vector<float> Design;
        unsigned int N_regressors = training_target->getRegressorData(Design);
//        cerr<<"done getting regressors "<<Design.size()<<endl;
     //   unsigned int N_sub_reg = Design.size()/N_regressors;
        
        //cout<<"Design "<<N_regressors<<" "<<N_sub_reg<<endl;
    //    for (unsigned int j = 0 ; j < N_sub_reg; ++j)
      //  {
    //        cout<<"Row : "<<j<<" : ";
        //   for (unsigned int i = 0 ; i < N_regressors; ++i)
          //  {
            //    cout<<" "<<Design[j*N_regressors + i ]<<" ";
            //}
   //         cout<<endl;
       // }
    
        //l2_res not used
    	vector<float> l2_res(Dimensionality_);
        //betas are input and output
        vector<float> betas;
        copyToSingleVectorTranspose( in_data, &betas, NumberOfSamples_ );
//        cout<<"indata is NULL2 "<<in_data[0]->size()<<" "<<betas.size()<<" "<<NumberOfSamples_<<" "<<in_data.size()<<" "<<Dimensionality_<<endl;
       // print<float>(betas,"betas1");
      //  print(Design,"Design");
//        if (Verbose)
//            cout<<"Runnning GLM..."<<N_regressors<<" "<<NumberOfSamples_<<" "<<Dimensionality_<<endl;
        //MxN of transpose
         cout<<"Runnning GLM..."<<N_regressors<<" "<<NumberOfSamples_<<" "<<Dimensionality_<<endl;
//        print(Design, "Design-pre");
//       print(betas, "in_data-pre");
//        print(l2_res, "l2res-pre");
		__CLPK_integer infopinv = run_pinv_GLM( "T",N_regressors,NumberOfSamples_, Dimensionality_, Design, betas, l2_res);
//        cerr<<"done pinv GLM"<<endl;
        __CLPK_integer info =0;
		//__CLPK_integer info = run_sgels_GLM( "T",N_regressors,NumberOfSamples_, Dimensionality_, Design, betas, l2_res);
//        print(Design, "Design-post");
//        print(betas, "betas-post");
//      print(l2_res, "l2res-post");
//        cerr<<"GLM filter sgels info2 "<<info<<endl;
        if (info > 0 )
            cerr<<"GLM filter : "<<info<<" number of zero eignevalues when calculating SVD. "<<endl;
           // throw ClassificationTreeNodeException("GLM_Filter : estimateParametersResiduals : run_sgels_GLM ...failed.");
//        throw ClassificationTreeNodeException("info>0. Encountered in sgels_ in GLM_filter. \n The i-th diagonal element of the triangular factor of A is zero, \
                                              so that A does not have full rank; the least squares solution could not be computed");
//        cout<<"indata is NULL2.5 "<<endl;//in_data[0]->size()<<endl;
        if (Verbose)
            print<float>(betas,"GLM done. betas:");
        
        if (estimated_betas!=NULL)
            delete[] estimated_betas;
        estimated_betas = new float[N_regressors * Dimensionality_];
        memcpy(estimated_betas,&(betas[0]),N_regressors * Dimensionality_*sizeof(float));
        
   //*******THE COMMENTED OUT PORTION WAS TO DEAL WITH GELS SOLUTION FORMAT
        
//       
//        //leading dimensionality of betas is not nunber of regressors, some dummyies exists becasue of gels MAX(1,M,N)
//        //This was caluclated inthe same way in the GLM routine
//        __CLPK_integer LDB = (N_regressors>NumberOfSamples_) ? N_regressors : NumberOfSamples_;
//		if (LDB<1) LDB=1;
//        //cout<<"LDB "<<LDB<<endl;
//        //design is actually in row major, just manipulate with the transpose
//        //copy data into object saved betas, remove extra inbetween data
//      //  cout<<"est betas size "<<N_regressors * Dimensionality<<" "<<N_regressors<<" "<<Dimensionality<<" "<<LDB<<endl;
//        for (unsigned int i = 0 ; i < Dimensionality; ++i)
//        {
//      //      cout<<"copy "<<Dimensionality<<" "<<i*LDB<<" "<<i*N_regressors<<" "<<betas[i*LDB]<<endl;
//            cblas_scopy(N_regressors, &(betas[i*LDB]), 1, estimated_betas+i*N_regressors,1);
//        }
//        cout<<"indata is NULL3 "<<in_data[0]->size()<<endl;
//        cout<<"estimated betas"<<endl;
//        for (unsigned int i = 0 ; i < N_regressors;++i)
//        {            for (unsigned int j = 0 ; j < Dimensionality; ++j)
//            {
//                cout<<estimated_betas[j*N_regressors+i]<<" ";
//            }
//            cout<<endl;
//        }
    cout<<"done betas"<<endl;
    }
    void GLM_Filter::estimateParametersFeatureSelectBootStrap()
    {
cout<<"Start Bootstrapping (NumberOfSamples_): "<<NumberOfSamples_<<endl;
        
        if ( F_stats != NULL)
            delete[] F_stats;
        
        //		F_stats = new float[Ncontrasts*Nevs*Dimensionality];in estimateParametersFeatureSelect

		F_stats = new float[Dimensionality_];
        for (unsigned int i =0 ; i <Dimensionality_;++i) 
            F_stats[i]=0;
        //vector<float> cumFStats(Dimensionality_,0);
        if (Verbose)
			cout<<"Start Bootstrapping (NumberOfSamples): "<<NumberOfSamples_<<endl;

		
        //First we need to store the original training data, so we can set it back later
        DataSource* d_source  = new DataSource();
        d_source->setTarget(training_target);
       // cout<<"loop "<<endl;
        for ( vector< vector<float>* >::iterator i = in_data.begin(); i != in_data.end();i++)
        {
            vector<float>* data_src = new vector<float>((*i)->begin(),(*i)->end());
            d_source->addInputData(data_src,Dimensionality_,NumberOfSamples_);		
        }
        GLM_Filter *glm_boot = new GLM_Filter();
        glm_boot->addInput(d_source);
      //  glm_boot->setThresholdMode(this->getThresholdMode());
        //glm_boot->setVerbose(true);
      //  cout<<"Done creating data source"<<endl;
        
        //for (unsigned int i = 0 ; i < B; ++i)
		unsigned int i_B = 0 ;
        vector<int> test_mask(NumberOfSamples_,1);
        
		while (  i_B < B ) 
		{
			vector< int > one_mask(NumberOfSamples_,0);
			//allows multiple occurances, selection masks allow for repeated number of sample. Yeah for me for enabling this at the beginning :D
			for (unsigned int j = 0 ; j < NumberOfSamples_ ; ++j)
				++one_mask[rand() % NumberOfSamples_];
			
			
            //	if (Verbose)
            //	{
            //		cout<<"B "<<i_B<<" "<<NumberOfSamples_<<" "<<training_target->getNumberOfSamples_()<<endl;
            //		print< int>(one_mask, "A bootstrap sample");
            //		print<int>(training_target->getTargetData<int>(),"targetrf ");
            //	}
			//lets just make sure that we have at least one of each group, or else group tests will fail
			//this will be mostly for debugging
			bool valid_sample=true;
            
			{//test if vlaid sample
				map<int,int> label2count;
                //	for (unsigned int ii = 0 ; ii< NumberOfSamples_; ++ii)
                unsigned int t_index=0;
                for(vector<int>::iterator i_one_mask = one_mask.begin(); i_one_mask != one_mask.end(); ++i_one_mask,++t_index)
				{
					//if (one_mask[ii]>0)
					if (*i_one_mask > 0)
					{
						for (int samp = 0; samp < *i_one_mask; ++samp)
						{
                            //						label2count[training_target->getInTarget(ii)]++;
                            //						cout<<"ii "<<ii<<" "<<training_target->getInTarget(ii)<<endl;
							label2count[training_target->getInTarget(t_index)]++;
                            //	cout<<"ii "<<t_index<<" "<<*i_one_mask<<" "<<training_target->getInTarget(t_index)<<endl;
						}
					}
				}
				//enforce requirement that at least as 2 samples per class
				if (label2count.size() != Kclasses_ )
				{
                    //	cout<<"failed "<<label2count.size()<<" "<<Kclasses<<endl;
					//bootstrap must contain at least one sample from each class
					valid_sample=false;
				}								
				for (map<int,int>::iterator i_m = label2count.begin(); i_m != label2count.end(); ++i_m)
					if (i_m->second < 2)
					{
						//bootstrap must contain at least 2 samples from each class
						valid_sample=false;
					}
				
                
                
			}
			//cout<<"push back sel mask"<<endl;
			//valid if sample contains at least 2 samples from each class
			if (valid_sample)
			{
                //print(one_mask,"Valid bootstrap sample");
				
                d_source->applySelectionMask(one_mask, test_mask);
                glm_boot->setTarget();
                glm_boot->setInputData();
                glm_boot->estimateParameters();
//                glm_boot->runNode();
                //print(glm_boot->getParameters(),"FstatsBoot");
                vector<float> f_boot =  glm_boot->getParameters();

                unsigned int count=0;
                for (vector<float>::iterator i = f_boot.begin(); i != f_boot.end();++i,++count)
                    F_stats[count]+=*i;
                //cout<<"cum F, boot samp "<<i_B<<endl;
              //  for (unsigned int i =0 ; i < Dimensionality_;++i)
                  //  cout<<F_stats[i]<<" ";
                //cout<<endl;

                ++i_B;
			}
		}
        for (unsigned int i =0 ; i < Dimensionality_;++i)
            F_stats[i]/=B;
        if (Verbose)
        {
            cout<<"average F"<<endl;
            for (unsigned int i =0 ; i < Dimensionality_;++i)
                cout<<F_stats[i]<<" ";
        }
        delete glm_boot;
        delete d_source;
    }

	void GLM_Filter::estimateParametersFeatureSelect()
	{

		vector<float> betas;//store response variable in betas
		if (Verbose)
			cout<<"Estimating parameters for GLM_Filter... "<<Dimensionality_<<endl;
		DimTrim=0;
		//not test for multiple contrasts,
		copyToSingleVectorTranspose( in_data, &betas, NumberOfSamples_ );
		//print(betas,"input to est paaras");
		if (out_data == NULL)
			out_data = new vector<float>();
		copyToSingleVector( in_data, out_data, NumberOfSamples_ );
		
		//the reason for the overkill is for future extensibility to any arbitrary design
		vector<float> X = training_target->getTargetData<float>(); //deisgn matrix
                                                                   //print<float>(X,"TARGET ");
		
//		int Nevs = 2; //number of groups?
		int Ncontrasts=1;
		vector<float> Design;/// = X;//need a copy because will be written over 
		vector<float> Contrast;
		
		vector<int> class_labels = training_target->getClassLabels();
		
		Design.assign(NumberOfSamples_* class_labels.size(),0);
		vector<float>::iterator i_D = Design.begin();
        
        //The Design matrix is an EV per class, this loop creates this design
		for (vector<int>::iterator i_cl = class_labels.begin(); i_cl != class_labels.end(); ++i_cl)
		{
			for ( vector<float>::iterator i_X = X.begin(); i_X != X.end(); ++i_X,++i_D)
			{
				if (*i_X==*i_cl)
					*i_D=1;
			}			
		}
//      		print(Design,"design -feature ");

		int Nevs = static_cast<int> (class_labels.size());
      
		if ((filt_mode == FTEST_ALL_PAIRS) || (filt_mode == BOOT_FTEST_ALL_PAIRS))
		{
            if (Verbose)
                cout<<"Do all pair-wise tests"<<endl;
			//do all pair-wise combinations
			list< vector<int> > pairwise_combinations;

			vector<int> single_pair(class_labels.size(),0);
			unsigned int Ncl = class_labels.size();
			//binary_combinations(pairwise_combinations, single_pair, ss_labels, count, 0, remainder);
			for ( unsigned int i = 1 ; i< Ncl; ++i )
			{
				vector<int> single_pair(Ncl,0);
				single_pair[0]=1;
				single_pair[i]=1;
				pairwise_combinations.push_back(single_pair);
			}
							
			Ncontrasts=pairwise_combinations.size();
			Contrast.assign(Ncontrasts*Nevs,0);
			
			vector<float>::iterator i_C = Contrast.begin();
			for (list< vector<int> >::iterator i = pairwise_combinations.begin(); i != pairwise_combinations.end(); i++)
			{
				bool first=true;
				for (vector<int>::iterator ii = i->begin(); ii!=i->end(); ++ii,++i_C)
				{
					if ( *ii != 0)
                    {
						if (first){
							*i_C = 1.0;
							first=false;
						}else {
							*i_C = -1.0;
						}
                    }
				}
				
			}
			
			//--------------
			if (Verbose)
				print<float>(Contrast,"Contrast");
			//--------------
	
		}
            if (Verbose)//outputs in matlab code for validation
            {
                cout<<"%Design Matrix: "<<endl;
                vector<float>::iterator i_d = Design.begin();
                cout<<"G=[";
                
                for ( int ev = 0 ; ev < Nevs; ++ev)
                {
                    for ( int row = 0 ; row <NumberOfSamples_; ++row, ++i_d)
                        cout<<*i_d<<" ";
                    
                    
                    if (ev == (Nevs-1))
                        cout<<"]'"<<endl;
                    else
                        cout<<endl;
                    
                }
            
                cout<<"%Response "<<endl;
                  cout<<"Y=[";
                //betas store y-values
                vector<float>::iterator i_y = betas.begin();
                    for ( int dim = 0 ; dim < Dimensionality_; ++dim)
                {
                    for ( int row = 0 ; row <NumberOfSamples_; ++row, ++i_y)

                        cout<<*i_y<<" ";
                
                    if (dim == (Dimensionality_-1))
                        cout<<"]'"<<endl;
                    else
                        cout<<endl;
                }
                
                //output matlab code to calculate betas
                cout<<"betas=pinv(G'*G)*G'*Y"<<endl;
                
            }

		vector<float> l2_res(Dimensionality_);
        //gels takes in column-major
		__CLPK_integer info = run_sgels_GLM( "N", NumberOfSamples_, Nevs, Dimensionality_, Design, betas, l2_res);

        if (Verbose)
        {
            cout<<"Betas:"<<endl;
            vector<float>::iterator i_b = betas.begin();
            for (int dim = 0 ; dim < Dimensionality_ ; ++dim )
            {
                cout<<"Dim "<<dim<<" : ";
                i_b = betas.begin() + dim * NumberOfSamples_;//theres an offset of where the betas are stored
                for (int i = 0 ; i < Nevs ; ++i,++i_b )
                    cout<<*i_b<<" ";
            cout<<endl;
            }
                //            print(betas,"betas -feature ");
        }
		if (info<0)
		{
			throw ClassificationTreeNodeException("Inavlid argument into sgels_ in ttest filter ");
		}else if (info>0){
			throw ClassificationTreeNodeException("info>0. Encountered in sgels_ in GLM_filter. \n The i-th diagonal element of the triangular factor of A is zero, \
												  so that A does not have full rank; the least squares solution could not be computed");
		}
//		print<float>(betas,"betas");

		//scale residuals norm to get s
		cblas_sscal(Dimensionality_,1.0/sqrt(NumberOfSamples_-Nevs),&l2_res[0], 1);
		
		
		//------------------------Processing the Denominator -----------------------------//
		int LDXXT = (Nevs > Ncontrasts) ? Nevs : Ncontrasts;
//		float* XXT = new float[Ncontrasts*Ncontrasts];
		float* XXT = new float[LDXXT*LDXXT];
	//	float* XXTt = new float[LDXXT*LDXXT];

		float* XXTt = new float[Dimensionality_*LDXXT];

		//Design X Deisgn' matrix - used in denominator of T-statistic
		cblas_sgemm( CblasColMajor, CblasTrans, CblasNoTrans, Nevs, Nevs, NumberOfSamples_,1,&(Design[0]),NumberOfSamples_,&(Design[0]), NumberOfSamples_, 0, XXT, Nevs);

		//Calculate inverse of Design X Deisgn' matrix  - used in denominator of T-statistic
		//this is covariance of Betas
		info = run_sgetri_square(Nevs,XXT, Nevs);
		
		if (info<0)
		{
			throw ClassificationTreeNodeException("Inavlid argument into spotri_ in ttest filter ");
		}else if (info>0){
			throw ClassificationTreeNodeException("U(i,i) is exactly zero; the matrix is singular and its inverse could	not be computed. Encountered in sgetri_ in GLM_filter ");
			
		}
		
		//multiply xxt pre and post by contrast matrix 
		//float* XXTt = new float[Ncontrasts*Ncontrasts];
		cblas_sgemm( CblasColMajor, CblasTrans, CblasNoTrans, Ncontrasts, Nevs, Nevs,1, &Contrast[0], Nevs , XXT , Nevs, 0,XXTt , Ncontrasts);
		cblas_sgemm( CblasColMajor, CblasNoTrans, CblasNoTrans, Ncontrasts, Ncontrasts, Nevs,1, XXTt, Ncontrasts, &Contrast[0], Nevs, 0,XXT , Ncontrasts);


		//need to do sudo inverse, S is not guranteed to be full rank		
		unsigned int rank;
		info = run_pinv_dgelss_square( Ncontrasts, XXT, Ncontrasts, rank,-1 );	

		if (info<0)
		{
			throw ClassificationTreeNodeException("Inavlid argument into run_pinv_dgelss_square in ttest filter.  ");
		}else if (info>0){
			throw ClassificationTreeNodeException("error in pseudo inv iin GLM_filter ");
			
		}
		
		if (rank != static_cast<unsigned int> (Ncontrasts) )
		{
			
			throw ("Invalid contrast matrix for F-test. Rank does not equal Number of Contrast. Rank = "+ num2str<int>(rank));
		}
		//------------------------Processing theCOntarst x Betas ----------------//
	
		//scale factor on denominatic for T denom = s *sqrt(scalefactor))
		//numerator 
		//LDA and rows mismatch
		//calculat cBetas
		float *cBetas = new float[Ncontrasts*Nevs*Dimensionality_];		
		cblas_sgemm( CblasColMajor, CblasTrans, CblasNoTrans, Ncontrasts, Dimensionality_, Nevs,1, &Contrast[0], Nevs , &betas[0]  , NumberOfSamples_, 0,cBetas , Ncontrasts);
		
		//-------------Multiply Cbeta * precision 
		
		//throw rank scaling in here
		cblas_sgemm( CblasColMajor, CblasTrans, CblasNoTrans, Dimensionality_, Ncontrasts, Ncontrasts,1.0/static_cast<float>(rank), &cBetas[0], Ncontrasts, &XXT[0], Ncontrasts, 0,&XXTt[0] , Dimensionality_);

		if (F_stats != NULL)
			delete[] F_stats;
		
		F_stats = new float[Ncontrasts*Nevs*Dimensionality_];
		//do post-multiplication for cBeta
		//Do Ftest for each dimension
		for ( unsigned int nDim = 0 ; nDim < Dimensionality_; ++nDim )
		{
			float dot = cblas_sdot(Ncontrasts,&XXTt[nDim],Dimensionality_,&cBetas[nDim*Ncontrasts],1) ;
			F_stats[nDim]=dot / (l2_res[nDim] * l2_res[nDim]);
           // cout<<"Fstats "<<nDim<<" "<<F_stats[nDim]<<endl;
		}
		delete[] cBetas;
		delete [] XXTt;
		delete [] XXT;
		
		max_F=0;
		
		//need to pre-calculate max F if using fractional threshold
		if ( thresh_mode == FRACTIONAL )
		{
				unsigned int count = 0;
				for (unsigned int j = 0 ; j < Dimensionality_; ++j,++count)
				{
					if (F_stats[count] > max_F ) max_F = F_stats[count];					
				}

		}
		
		
		
	}
	
	void GLM_Filter::applyParametersToTrainingData()
	{
		//----------------------------filter training data being passed on to next node------------------------//-------------
		
		//--------------
		if (Verbose)
			cout<<"Applying GLM filter to training data..."<<endl;
		//--------------

		if (out_data == NULL) {
            cout<<"create out data"<<endl;
			out_data = new vector<float>();
		}
			
//        cout<<"applyParametersToTrainingData"<<endl;
        if ((filt_mode != RESIDUALS)  && (filt_mode != RESIDUALS_NOAPPLY))
            applyParametersToTrainingDataFeatureSelect();
        else 
            applyParametersToTrainingDataResiduals();
        //cout<<"done applyParametersToTrainingData"<<endl;

	}
    void GLM_Filter::applyParametersToTrainingDataFeatureSelect()
    {
        copyToSingleVector( in_data, out_data, NumberOfSamples_ );
       
     
        float thresh = threshold;
		float min=1e10;
		
        //	cout<<"Threshmode "<<thresh_mode<<endl;
		if ( thresh_mode == FRACTIONAL )
		{
			thresh *= max_F;
            if (Verbose)
				cout<<"Using Fractional Threshold... "<<thresh<<endl;
		}else if ( thresh_mode == ORDER_STATISTIC )
		{
			thresh = order_threshold = orderStatistic(F_stats, Dimensionality_, threshold);

			if (Verbose)
				cout<<"Using Order Statistic... "<<thresh<<endl;
            
		}
		
		//apply trheshodl 
		//--------------
		//if (Verbose)
		//	cout<<"F-threshold "<<thresh<<endl;
		//--------------
//		cout<<"features: ";
  //      for (unsigned int j = 0 ; j < Dimensionality_; ++j)
    //    {
      //   //   cout<<F_stats[j]<<" "<<thresh<<endl;
        //    cout<< ((F_stats[j]>thresh)? 1 : 0) <<" ";
       // }
       // cout<<endl;
        
        
		vector<float>::iterator i_out = out_data->begin();
		vector<float>::iterator i_out_write = out_data->begin();
		for (unsigned int i = 0 ; i < NumberOfSamples_; ++i)
		{
			unsigned int count = 0;
			for (unsigned int j = 0 ; j < Dimensionality_; ++j,++i_out,++count)
			{
                if (Verbose)
                   if (i==0) cout<<"Feature "<<j<<", F="<<F_stats[count]<<endl;

				if (F_stats[count] > max_F ) 
                { 
                    max_F = F_stats[count]; 
                    //cout<<"maxF "<<count<<" "<<max_F<<endl;
                }
				if (F_stats[count] < min ) min = F_stats[count];
                
				
				if ((F_stats[count]>thresh) && (F_stats[count]!= -1))//threshold)
				{
					*i_out_write=*i_out;
					++i_out_write;
				}else if (i==0)
					++DimTrim;
                
			}
		}
		
        
		
		
		if (Verbose)
			cout<<"Minimum/Maximum F "<<min<<"/"<<max_F<<endl;
		if (Dimensionality_==DimTrim)
			throw ClassificationTreeNodeException("GLM filter : Invalid threshold - no features survived threshold");
        
		Dimensionality_-=DimTrim;	
		
		if (Verbose)
			cout<<"Dimensionaltiy Reduction "<<Dimensionality_+DimTrim<<" ---> "<<Dimensionality_<<endl;
        
		
		out_data->resize(Dimensionality_*NumberOfSamples_);
        //print(*out_data,"glm filter outdata start filtered end ");

		if (Verbose)
			cout<<"Done applying "<<NodeName<<" to training data."<<endl;
        
    }
    
    void GLM_Filter::applyParametersToTrainingDataResiduals()
    {
     //   cout<<"copy data "<<NumberOfSamples_<<" "<<in_data.size()<<endl;
       // if (out_data == NULL)
       //     cout<<"outdata is NULL"<<endl;
       // if ( in_data[0] == NULL)
       // cout<<"access in data2 "<<endl;
       // cout<<"in_data size "<<in_data.size()<<endl;
         //   cout<<"indata is NULL "<<in_data[0]->size()<<endl;
        
        copyToSingleVector( in_data, out_data, NumberOfSamples_ );
        
        //cout<<"done copy"<<endl;
        vector<float> Design;
        unsigned int N_regressors = training_target->getRegressorData(Design);
       // cout<<"got design "<<endl;
        //calculate the residual
        //does data - betas*design
        //cout<<"do multiplication"<<endl;
        cout<<"regressors"<<endl;
         for (int i=0; i < N_regressors; ++i)
          cout<<estimated_betas[i]<<" ";
        cout<<endl;
       // print<float>(*out_data,"outdata-residualspre");

       cblas_sgemm( CblasColMajor, CblasTrans, CblasNoTrans,Dimensionality_,NumberOfSamples_, N_regressors,-1,estimated_betas,N_regressors,&(Design[0]), N_regressors, 1, &(out_data->at(0)), Dimensionality_);
        //cout<<"done multiply"<<endl;
        //    print<float>(*out_data,"outdata-residualspost");
        if (Verbose)
			cout<<"Done applying "<<NodeName<<" to training data."<<endl;
        //cout<<"Done applying "<<NodeName<<" to training data."<<endl;


        
    }
    

    void GLM_Filter::runTrackStats(){
        if ( (filt_mode != RESIDUALS) && (filt_mode != RESIDUALS_NOAPPLY) )
            runTrackStatsFeatureSelect();
		else {
			runTrackStatsResiduals();
		}

		
        
    }
	void GLM_Filter::runTrackStatsResiduals()
	{
		cerr<<"GLM_Filter::runTrackStatsResiduals"<<endl;

		if (Verbose) {
			cout<<"run track stats GLM - Residual Mode"<<endl;
		}
		
		//----------------------------start handling intializatioin----------------------//
		
		if (stats_sum_x == NULL)
		{
			N_stats=0;
			//	stats_sum_x = new vector<float>(Dimensionality_,0);
            //should be size of feature mask, accoutn for changes in feature selection
            stats_sum_x = new vector<float>(output_feature_mask_->size() ,0);
			
		}
		
		if (stats_sum_xx == NULL)
		{
			N_stats=0;
			//	stats_sum_xx = new vector<float>(Dimensionality_,0);
            stats_sum_xx = new vector<float>(output_feature_mask_->size(),0);
			
		}
        if (stats_sum_N == NULL)
		{
			N_stats=0;
            //	stats_sum_xx = new vector<float>(Dimensionality_,0);
            stats_sum_N = new vector<unsigned int>(output_feature_mask_->size(),0);
            
		}
		//----------------------------done handling intializatioin----------------------//
		
		
		//we're in residual mode no thresholding makes life simpler
        //	unsigned int Nd = stats_sum_x->size();
        //	unsigned int index = 0;
		vector<float>::iterator i_xx =stats_sum_xx->begin();
		vector<bool>::iterator i_mask =output_feature_mask_->begin();
		vector<unsigned int>::iterator i_N =stats_sum_N->begin();
		float* p_beta = estimated_betas;
		//   int count=0;
		for (vector<float>::iterator i_x= stats_sum_x->begin(); i_x != stats_sum_x->end(); ++i_x,++i_xx,++i_N, ++i_mask,++p_beta)
		{
			if ( *i_mask )
			{
				*i_x += *p_beta;
				*i_xx +=  (*p_beta) * (  *p_beta ); //don't swquare because square is still 1, yes redundant, but simplier for compatibility with other functions
				++(*i_N);
			}
			
			
		}
				
		
		
		++N_stats;//keep track  how how many tiems visited
	}
    
	void GLM_Filter::runTrackStatsFeatureSelect()
	{
		if (Verbose) {
			cout<<"run track stats GLM"<<endl;
		}
		
		//----------------------------start handling intializatioin----------------------//

		if (stats_sum_x == NULL)
		{
			N_stats=0;
		//	stats_sum_x = new vector<float>(Dimensionality_,0);
            //should be size of feature mask, accoutn for changes in feature selection
            stats_sum_x = new vector<float>(output_feature_mask_->size() ,0);

		}
		
		if (stats_sum_xx == NULL)
		{
			N_stats=0;
		//	stats_sum_xx = new vector<float>(Dimensionality_,0);
            stats_sum_xx = new vector<float>(output_feature_mask_->size(),0);

		}
        if (stats_sum_N == NULL)
		{
			N_stats=0;
            //	stats_sum_xx = new vector<float>(Dimensionality_,0);
            stats_sum_N = new vector<unsigned int>(output_feature_mask_->size(),0);
            
		}
		//----------------------------done handling intializatioin----------------------//
		
		
		//add Fstats to sum X
		//unsigned int Nd = stats_sum_x->size();
		//sum F-stats 
		//cout<<"track GLM "<<threshold<<" "<<stats_threshF<<" "<<Nd<<endl;
		if (stats_threshF)
		{//this option keep track of voxeks chosen.i.e. threshold teh voxels
			
			
			float thresh = threshold;
		//	float min=1e10;
			
			//	cout<<"Threshmode "<<thresh_mode<<endl;
			if ( thresh_mode == FRACTIONAL )
			{
				thresh *= max_F;
			}else if ( thresh_mode == ORDER_STATISTIC )
			{
				thresh = order_threshold = orderStatistic(F_stats, Dimensionality_+DimTrim, threshold);
				if (Verbose)
					cout<<"Using Order Statistic... "<<endl;
				
			}
			
		//	cout<<"track thresh "<<thresh<<endl;
			unsigned int index = 0;
			vector<float>::iterator i_xx =stats_sum_xx->begin();
            vector<bool>::iterator i_mask =output_feature_mask_->begin();
			vector<unsigned int>::iterator i_N =stats_sum_N->begin();

			
         //   int count=0;
            for (vector<float>::iterator i_x= stats_sum_x->begin(); i_x != stats_sum_x->end(); ++i_x,++i_xx,++i_N, ++i_mask)
			{
                if ( *i_mask )
                {
                    *i_x += ( F_stats[index] > thresh ) ? 1 : 0;
                    *i_xx += ( F_stats[index] > thresh ) ? 1 : 0; //don't swquare because square is still 1, yes redundant, but simplier for compatibility with other functions
                    if (F_stats[index] > thresh ) 
                    {
                        ++(*i_N);
                    //    ++count;
                    }
                    
                    
                    //else 
                        
                    ++index;
                }


			}
           // cout<<"GLM track "<<index<<" "<<count<<" nvoxesl"<<endl;

		}
        //Ftarckign is broken!!!!!!
        //else {//keep track of F
//
//			cblas_saxpy(Nd, 1.0,F_stats, 1, &(stats_sum_x->at(0)), 1);
//			//sum square of fstats
//			unsigned int index = 0;
//			for (vector<float>::iterator i = stats_sum_xx->begin(); i != stats_sum_xx->end(); ++i, ++index)
//				*i += F_stats[index] * F_stats[index];
  //          //increment all coutn
    //        stats_sum_N->assign(stats_sum_N->size(), stats_sum_N->at(0)+1);
//
//		}

		
					
		++N_stats;//keep track  how how many tiems visited
	}
//	void GLM_Filter::writeStats()
//	{
//		cout<<"write GLM stats "<<endl;
//	
//	}
	


	void GLM_Filter::applyToTestData()
	{
		if (out_test_data == NULL)
			out_test_data = new vector<float>();

        //cout<<"applyToTestData"<<endl;
        if ((filt_mode != RESIDUALS) && (filt_mode != RESIDUALS_NOAPPLY))
            applyToTestDataFeatureSelect();
        else 
            applyToTestDataResiduals();
        //cout<<"done applyToTestData"<<endl;

	}
	
    void GLM_Filter::applyToTestDataFeatureSelect()
    {
        //--------------------
		if (Verbose)
			cout<<"Applying GLM filter to test data..."<<endl<<"Number of Test Samples: "<<NumberOfTestSamples_<<"."<<endl;
		//--------------------
        
		
		copyToSingleVector( in_test_data, out_test_data, NumberOfTestSamples_ );
      /*  
        
        ofstream fout("gl_test.csv");
        fout<<0;
        for (unsigned int j = 1 ; j <Dimensionality_ ; ++j)
            fout<<","<<j;
        fout<<endl;
        vector<float>::iterator i_o=out_test_data->begin();
        for (unsigned int i = 0 ; i < NumberOfTestSamples_   ;++i)
            for (unsigned int j = 0 ; j <Dimensionality_ ; ++j,++i_o)
            {
                fout<<*i_o;
                if (j<(Dimensionality_-1))
                    fout<<",";
                else
                    fout<<endl;
            }
        fout.close();

        */
        
        
		vector<float>::iterator i_out = out_test_data->begin();
		vector<float>::iterator i_out_write = out_test_data->begin();
		float thresh = threshold;
        
		
		if ( thresh_mode == FRACTIONAL )
			thresh = threshold * max_F;
		else if ( thresh_mode == ORDER_STATISTIC )
		{
			//this is very redundant
			//can't wriote over threshold because of recalculation of thresh
			thresh = order_threshold;
            //			thresh = orderStatistic(F_stats, Dimensionality_, threshold);
		}
		
        
        
		//--------------------
		if (Verbose)
			cout<<"Maximnum F / F threshold : "<<max_F<<" / "<<thresh<<endl;
		//--------------------
        
        
		for (unsigned int i = 0 ; i < NumberOfTestSamples_; ++i)
		{
			unsigned int count = 0;
            //use DimTrim to retain original dimensiopnality
			for (unsigned int j = 0 ; j < Dimensionality_+DimTrim; ++j,++i_out,++count)
			{
				if ((F_stats[count]>thresh) && (F_stats[count]!= -1))//threshold)
				{
					*i_out_write=*i_out;
					++i_out_write;
				}
				
			}
		}
		out_test_data->resize(NumberOfTestSamples_*Dimensionality_);
        
       // print(*out_test_data,"glm filter out test data");
        
    }
    void GLM_Filter::applyToTestDataResiduals()
    {
        //cerr<<"applyToTestDataResiduals"<<endl;
        //--------------------
		if (Verbose)
			cout<<"Applying GLM filter to test data (residuals)..."<<endl<<"Number of Test Samples: "<<NumberOfTestSamples_<<"."<<endl;
		//--------------------
        vector<float> test_regressor_data;
		unsigned int N_regressors = training_target->getTestRegressorData(test_regressor_data);
		copyToSingleVector( in_test_data, out_test_data, NumberOfTestSamples_ );
//        cout<<"out data size "<<out_test_data->size()<<" "<<NumberOfTestSamples_<<" "<<Dimensionality_<<endl;
  //      cout<<"number of regressors "<<training_target->getNumberOfRegressors()<<endl;
    //    print<float>(*out_test_data,"test data");
   //     unsigned int N_regressors = training_target->getNumberOfRegressors();
      //  print<float>(test_regressor_data,"predictors");
      //  cerr<<"apply to residuals 1"<<endl;

        if ( filt_mode != RESIDUALS_NOAPPLY )
        {
            //    cerr<<"apply to residuals "<<endl;
        //multiply by betas then subtract from original data
        cblas_sgemm( CblasColMajor, CblasTrans, CblasNoTrans,Dimensionality_,NumberOfTestSamples_, N_regressors,-1,estimated_betas,N_regressors,&(test_regressor_data[0]), N_regressors, 1, &(out_test_data->at(0)), Dimensionality_);
        }
    //    print<float>(*out_test_data,"predicted");

        
    }
	
}
