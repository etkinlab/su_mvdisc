#ifndef ClassificationTreeNodeException_H
#define ClassificationTreeNodeException_H
/*
 *  ClassificationTreeNodeException.h
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 8/24/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include <exception>
#include <string>
#include <iostream>
class ClassificationTreeNodeException : public std::exception{
	
public:
	ClassificationTreeNodeException(const char* msg)
	{
        errmesg= msg;
    }
	
    virtual const char* what() const throw()
	{
		return errmesg;
	}
    private:

     const char* errmesg;
      //  char* errmesg;
//        std::string errmesg;
};


#endif
