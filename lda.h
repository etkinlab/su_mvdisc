/*
 *  lda.h
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 7/15/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


#ifndef lda_mvdisc_
#define lda_mvdisc_


#include <vector>
#include <string>
#include <sstream>
#include "base_mvdisc.h"

#include <clapack_utils.h>


namespace su_mvdisc_name{

//#pragma GCC visibility push(default)

class LDA_mvdisc : public base_mvdisc
{
public:
	
	enum PRIOR_TYPE {SAMPLE_PROB, EQUAL_PROB  };
	LDA_mvdisc();

	~LDA_mvdisc();
	void setOptions( std::stringstream & ss_options);

	 void estimateParameters();
	 void setParameters();
    void saveParameters();//this function writes to file as it goes.
    std::string getParameterFileName();
	//int classify(const std::vector<float> & vec) const ;
	int classifyTrainingData()  ;
	int classifyTestData()  ;
	
private:
		
	//std::string name;
    float ZERO_THRESH;
    float frac_var_thresh;
	//std::vector< std::vector<float> > class_means; //
//	std::vector< float > Sigma; //common covariance matrix 
	std::vector< float > class_priors;
     clapack_utils_name::smatrix uni_stdev_;
    clapack_utils_name::smatrix DxUt;                //is actually D^-0.5 Ut
  //  clapack_utils_name::smatrix Vt;
    clapack_utils_name::smatrix mu_sphere;
    std::vector<unsigned int> nperclass_;
//	unsigned int alloc_Dimensionality,alloc_NumberOfSamples;
	PRIOR_TYPE cl_prior_choice;

};

}
//#pragma GCC visibility pop
#endif
