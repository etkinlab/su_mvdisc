/*
 *  NormalizeFilter.cpp
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 8/12/10.
 *  Copyright 2010 _Stanford University__. All rights reserved.
 *
 */

#include "NormalizeFilter.h"
//#include <misc_utils/misc_utils.h>
#include <misc_utils.h>
#include <cmath>
using namespace std;
using namespace misc_utils_name;

namespace su_mvdisc_name{
	NormalizeFilter::NormalizeFilter()
	{
		NodeName="NormalizationFilter";
		
	}
	
	NormalizeFilter::~NormalizeFilter(){}
	
	void NormalizeFilter::setNormMode( NORM_MODE mode )
	{
		normalization_method=mode;
	//	mean=0;
	//	var=1;
		
		lower_limit = -1;
		upper_limit = +1;
		
	}
	
	void NormalizeFilter::setNormMode( const string & mode )
	{
		if		(	mode == "Variance_AS"	)
			setNormMode(VAR_NORM_AS);
		else if	(	mode == "Variance_WS"	)
			setNormMode(VAR_NORM_WS);
		else if (	mode == "Mean_AS"	)
			setNormMode(MEAN_NORM_AS);
		else if (	mode == "Mean_WS"	)
			setNormMode(MEAN_NORM_WS);
		else if (	mode == "Mean_And_Variance_AS"	)
			setNormMode(MEAN_VAR_NORM_AS);
		else if (	mode == "Mean_And_Variance_WS"	)
			setNormMode(MEAN_VAR_NORM_WS);
		else if (	mode == "MinMax_AS"	)
		setNormMode(MINMAX_AS);
		else if (	mode == "MinMax_WS"	)
			setNormMode(MINMAX_WS);
		else {
			throw ClassificationTreeNodeException( ("Unrecognized Normalization Mode Specified: "+mode).c_str());
		}

	}
	
	void NormalizeFilter::setOptions( std::stringstream & ss_options)
	{
		map<string,string> options = parse_sstream(ss_options);
		for ( map<string,string>::iterator it = options.begin(); it!=options.end();it++)
			if ( it->first == "normMode")
			{
				setNormMode( it->second );
			}else {
				if (!setTreeNodeOptions(it->first, it->second))
						throw ClassificationTreeNodeException((NodeName + " : Invalid input argument : " + it->first).c_str());
			}

		
	}
	void NormalizeFilter::estimateParameters()
	{
        
		if (out_data==NULL)
			out_data = new vector<float>();
		copyToSingleVector( in_data, out_data, NumberOfSamples_ );
		
		v_bias.clear();
		v_scale.clear();
		
		if ( normalization_method < NUMBER_OF_WS ) 
		{  ///choose between wihtin sample and across sample normalization
           
			for (unsigned int sample = 0 ; sample< NumberOfSamples_ ; sample++)
			{
				unsigned int cum_dim=0;
				for ( vector<unsigned int>::iterator i_dim = v_dims_.begin(); i_dim != v_dims_.end(); i_dim++)
				{
					
					normalize(out_data->begin()+ sample * Dimensionality_ + cum_dim,out_data->begin()+ sample * Dimensionality_ + cum_dim + *i_dim , 1 ) ;
					cum_dim += *i_dim;
				}
			}
			
		}else {
            //  print(*out_data,"outdata-prenormalize-train");
			for (unsigned int i = 0 ; i < Dimensionality_ ; i++)
			{
                //cout<<"normalize "<<i<<endl;
				//need the + 1 on end because vector does not include the end
				normalize(out_data->begin() + i , out_data->begin() + (NumberOfSamples_)*Dimensionality_ + i , Dimensionality_ );
			}
            //print(*out_data,"outdata-postnormalize-train");

			
		}
     
	}
	void NormalizeFilter::applyParametersToTrainingData()
	{
		copyToSingleVector( in_data, out_data, NumberOfSamples_ );
		if ( normalization_method < NUMBER_OF_WS ) 
		{  ///choose between wihtin sample and across sample normalization
			
			for (unsigned int sample = 0 ; sample< NumberOfSamples_ ; sample++)
			{
				unsigned int cum_dim=0;
				for ( vector<unsigned int>::iterator i_dim = v_dims_.begin(); i_dim != v_dims_.end(); i_dim++)
				{
					//the index in input does nothign for within subject because it does not use pre-computed means and varaicnes
					ApplyNormalize(out_data->begin()+ sample * Dimensionality_ + cum_dim,out_data->begin()+ sample * Dimensionality_ + cum_dim + *i_dim , 1, sample ) ;
					cum_dim += *i_dim;
				}
			}
			
		}else {
			for (unsigned int i = 0 ; i < Dimensionality_ ; i++)
			{
				//need the + 1 on end because vector does not include the end
				ApplyNormalize(out_data->begin() + i , out_data->begin() + (NumberOfSamples_)*Dimensionality_ + i , Dimensionality_, i );
			}
			
		}
		
		
	}
	
	void NormalizeFilter::runSpecificFilter()
	{		
		estimateParameters();//does both estimate and write at once
	}
	void NormalizeFilter::applyToTestData()
	{
        if (Verbose)
            cout<<"NormalizeFilter::applyToTestData"<<endl;
		if (out_test_data == NULL)
			out_test_data = new vector<float>();
		copyToSingleVector( in_test_data, out_test_data, NumberOfTestSamples_ );
	
		//do different slightly if normalizing across features (within subject) or accross subjects for a given feature.
		if ( normalization_method < NUMBER_OF_WS ) 
		{  ///choose between wihtin sample and across sample normalization
			
			for (unsigned int sample = 0 ; sample< NumberOfTestSamples_ ; sample++)
			{
				unsigned int cum_dim=0;
				for ( vector<unsigned int>::iterator i_dim = v_dims_.begin(); i_dim != v_dims_.end(); i_dim++)
				{
					//the index in input does nothign for within subject because it does not use pre-computed means and varaicnes
					ApplyNormalize(out_test_data->begin()+ sample * Dimensionality_ + cum_dim,out_test_data->begin()+ sample * Dimensionality_ + cum_dim + *i_dim , 1, sample ) ;
					cum_dim += *i_dim;
				}
			}
			
		}else {
			for (unsigned int i = 0 ; i < Dimensionality_ ; i++)
			{
				//need the + 1 on end because vector does not include the end
				ApplyNormalize(out_test_data->begin() + i , out_test_data->begin() + (NumberOfTestSamples_)*Dimensionality_ + i , Dimensionality_, i );
			}
			
		}
		
	}
	
	
	void NormalizeFilter::calculateBiasAndScale( float & bias, float & scale, vector<float>::iterator  i_begin, vector<float>::iterator  i_end,const unsigned int & inc)
	{
       
		if (( normalization_method == MINMAX_WS ) ||  ( normalization_method == MINMAX_AS ) ) 
		{
			vector<float>::iterator i= i_begin;
			float min = *i;
			float max = *i;
			
			for ( ;i != i_end; i+=inc)
			{
				if (*i > max) max=*i;
				if (*i < min) min=*i;						
				
			}
			float range = max-min;
			if (range>0)
			{
				scale = ( upper_limit - lower_limit ) / range;
				bias =  ( upper_limit * min - lower_limit * max  ) / range;
			}else {
				scale=0;
				bias=0;
			}
		}
		
		else if (( normalization_method == MEAN_VAR_NORM_WS ) ||  ( normalization_method == MEAN_VAR_NORM_AS ) ) 
		{
		         
			float sx = 0, sxx = 0;
			unsigned int N = 0;
			for (vector<float>::iterator i= i_begin; i != i_end; i+=inc)
			{
				sx  +=  *i;
				sxx += (*i)*(*i);	
				N++;
			}
			
			float variance = (sxx - sx*sx/N ) / (N-1); 
			scale = 1/sqrt(variance);
			bias = sx/N * scale ;
			
			
		}else if (( normalization_method == MEAN_NORM_WS ) ||  ( normalization_method == MEAN_NORM_AS ) ) 
		{
			
			
			float sx = 0;
			unsigned int N = 0;
			for (vector<float>::iterator i= i_begin; i != i_end; i+=inc)
			{
				sx  +=  *i;
				N++;
			}
			
			bias = sx/N;
			scale = 1.0;
			
		}else if (( normalization_method == VAR_NORM_WS ) ||  ( normalization_method == VAR_NORM_AS ) ) {
			
			
			float sx = 0, sxx = 0;
			unsigned int N = 0;
			for (vector<float>::iterator i= i_begin; i != i_end; i+=inc)
			{
				sx  +=  *i;
				sxx += (*i)*(*i);	
				N++;
			}
			
			bias = sx/N;
			float variance = (sxx - sx*sx/N ) / (N-1); 
			scale = 1.0/sqrt(variance);
			bias = sx/N * (scale - 1.0 );
			
			
		}
       

	}
	
	void NormalizeFilter::ApplyNormalize( vector<float>::iterator  i_begin, vector<float>::iterator  i_end, const unsigned int & inc, const unsigned int & index)
	{
		
		float bias=0;
		float scale=1;
		
		
		//do different slightly if normalizing across features (within subject) or accross subjects for a given feature.
		if ( normalization_method < NUMBER_OF_WS ) 
		{
			calculateBiasAndScale(bias,scale, i_begin, i_end,inc);
			
			
		}else if  (normalization_method > NUMBER_OF_WS) 
		{
			
			if ( ( v_bias.size() != Dimensionality_ ) || ( v_scale.size() != Dimensionality_ ))
				throw ClassificationTreeNodeException("Error: tried to normalize test data, but no or the wrong number of biass and vraiances have been computer");
			
            
			bias=v_bias[index];
			scale=v_scale[index];
			//cout<<"applyNromalize bias/scale "<<bias<<" "<<scale<<endl;
		}
		
		
		for (vector<float>::iterator i= i_begin; i != i_end; i+=inc)
			*i = (*i)*scale - bias;
		
		
		
	}	 
	void NormalizeFilter::normalize( vector<float>::iterator  i_begin, vector<float>::iterator  i_end, const unsigned int & inc)
	{
        if (Verbose)
            cout<<" NormalizeFilter::normalize - normalizing training data."<<endl;
		float bias = 0;
		float scale = 1; 
		calculateBiasAndScale(bias,scale, i_begin, i_end,inc);
        //cout<<bias<<" "<<scale<<endl;
		if ( normalization_method > NUMBER_OF_WS ) 
		{	
			v_bias.push_back(bias);
			v_scale.push_back(scale);
		}
		
		for (vector<float>::iterator i= i_begin; i != i_end; i+=inc)
			*i = (*i)*scale - bias;
		
		
		
	}	 
	
	
}
