#ifndef ClusterFilter_H
#define ClusterFilter_H
/*
 *  cluster_filter.h
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 8/12/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include <base_image_filter.h>
//#include <clapack_utils.h>

#include <vector>
namespace su_mvdisc_name{


enum CLUSTER_MODE { K_MEANS , SLINK, CLINK };
    
enum INIT_METHOD { FORGY, RNDM_PART , AGGLOM  };
//FORGY (for k-means) randomlyt selects 2 subjects to initialize means
//RNDM_PART (for k-means), randomly splits data
//AGGLOM is for agglommerative methods, assigns each subject to it's own cluster
    
    
class ClusterFilter : public base_filter
{
public:
	ClusterFilter();
	virtual ~ClusterFilter();
	void setOptions( std::stringstream & ss_options);
	void applyToTestData();
	void estimateParameters();
	void applyParametersToTrainingData();
	void runSpecificFilter();

    void useClustersAsOutput(const bool & state );
    
	//Set the number of nodes to partition the data into.
	void setK( const unsigned int & K );

	//Set the method by which to cluster. Currently this is only K-means, but may be extended
	//in the future
	void setClusterMode( CLUSTER_MODE mode );

	//Set the method by which to cluster. This however allows for the imput of a pre-specifed
	//string. This is for convenience, particulary when setup use a text configuration file
	void setClusterMode( const std::string & mode );

	//Depending on the clustering method, different strategies exist for initialization of the
	//means. For k-means, Forgy and Random Partition are available
	void setInitMethod( INIT_METHOD mode );

	//Set the initialization method using string as input.
	void setInitMethod( const std::string & mode );

	//Set the number of repitions of the clustering algorithm.
	void setRepititions( const unsigned int & R );

	clapack_utils_name::smatrix getClusterMeans() { return *k_means_; }




protected:

private:

	//This function actually runs the k-means algorithm. Reuires input data, an initial estimate of
	//the cluster means, a vector to store the final classifications, the dimensionality, the number
	//of samples and the number of clusters. k_means will be written over with the final cluster mean
	//estimates. k_final will store the final cluster labels for each sample. The function return the
	//total L2 distance from the point to it's cluster mean.
    
//    void doKmeans();
    static void doKmeans( const clapack_utils_name::smatrix & m_in , const unsigned int & NumberOfSamples, clapack_utils_name::smatrix & k_means, const unsigned int &  K , const unsigned int & cluster_reps, const INIT_METHOD & init_meth, std::vector<unsigned int> & k_final );

    void doAgglomerative();
    
    static float estimate_kmeans( const clapack_utils_name::smatrix & data_in , clapack_utils_name::smatrix & k_means,  std::vector<unsigned int> & k_final, const unsigned int & K );


	//This function initializae the cluster means uusing the indicated method.
	//It is separated out from estimate parameters for clarity in the code.
	//Particularly given that the algorithm is repeated cluster_reps_ times.
	static void initializeMeans(const clapack_utils_name::smatrix & data_in , clapack_utils_name::smatrix & k_means, std::vector<unsigned int> & k_init, const unsigned int & K,const INIT_METHOD & init_meth   );

	//return number of clusters in a given vector
	unsigned int getNumberOfClusters(const std::vector<unsigned int> & k_final);

	//Return nwith there is less than a given number of clusters
	//By using the less than, we can check size of set at every iteration and break out of loop when K is reach
	//Extra cost for the size call and < test, but save traversing whole vector
	static bool isNumberOfClustersLessThanK(const std::vector<unsigned int> & k_final, const unsigned int & K );

	unsigned int K_; //number of clusters to partition the data into
	unsigned int IterMax_; //A cap on the number of iteration it will allow for the algorithm to converge;
	unsigned int cluster_reps_;//Specifies the number of time to repeat the clustering algorithm. Differs due to randomness of the initialization method.

	std::vector<unsigned int> k_classes_final_;

	clapack_utils_name::smatrix*  k_means_,*data_in_;
	CLUSTER_MODE cluster_method_; //Specify what clustering algorithm to use. e.g, k-means
	INIT_METHOD init_meth_; //Specify which initialization to use. e.g. FORGY, RNDM_PART
    bool use_clusters_as_output_;

};

}

#endif
