/*
 *  base_mvdisc.cp
 *  base_mvdisc
 *
 *  Created by Brian Patenaude on 7/14/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include <iostream>
#include "base_mvdisc.h"
//#include "base_mvdiscPriv.h"
#include <misc_utils.h>
//#include <misc_utils/misc_utils.h>
#include <lda.h>
#include <qda.h>
#include <tree_sampler.h>
#include <smatrix.h>
using namespace std;
using namespace misc_utils_name;
using namespace clapack_utils_name;
namespace su_mvdisc_name{
	
	base_mvdisc::base_mvdisc()
	{
		training_target = NULL;
		//disc_distance=NULL;, doen at tree level
		NumberOfSamples_ = 0;
		nodeType = DISCRIMINANT;
		NodeName="MV_Discriminant";
		PassDownOutput=false;
		do_applyToTestData_ = true;
		do_applyToTrainingData_ = true;
		DimTrim=0;

       // DoGenAcc_=false;  //this has been left to the ClassificationTreeeNode for options reasons

        doKfold_=false;
	//	hasEstimateParameters=false;
        model_thresh_l_=0;
        model_thresh_u_=0;

	}
	base_mvdisc::~base_mvdisc()
	{
	//	cout<<"base_mvdisc : destructor"<<endl;
    //	if (disc_distance != NULL)
	//			delete disc_distance;
	}
    TpFpTnFn base_mvdisc::getGeneralizationData()
    {
        return genData_;
    }
    smatrix base_mvdisc::getGeneralizationDataThresh()
    {
        //cerr<<"genDataThesh_ "<<model_thresh_u_<<endl;
        return genData_BagThreshXBagModel_ ;
    }
    std::vector< TpFpTnFn > base_mvdisc::getGeneralizationDataLevel2()
    {
        return genData_l2_;
    }
	void base_mvdisc::runNode( bool with_estimation )
	{
//      cerr<<"base_mvdisc::runNode "<<nodeName()<<endl;
		for (vector< ClassificationTreeNode* >::iterator i = node_inputs.begin() ; i != node_inputs.end(); i++)
		{

			if (PassDownOutput)
				(*i)->enablePassDownOutput();//tells input to pass down output information
            cerr<<"base_mvdisc::runNode run input: "<<(*i)->nodeName()<<endl;
			(*i)->runNode();
		}


        cout<<"set target "<<endl;
		setTarget();
        cout<<"end target "<<endl;

		setInputData();
        cout<<"end input "<<endl;

        setTestData();
        cout<<"end test "<<endl;
		if (with_estimation) {
            cout<<"estinate params"<<endl;
			estimateParameters();
			            cout<<"base_mvdisc :: save aparameters "<<saveParams_<<endl;
            if (saveParams_)
                saveParameters();
        
        }

//        cout<<"base_mvdisc :: setIOoutput "<<PassDownOutput<<endl;

        if ( PassDownOutput )
			setIOoutput();
//        cout<<"base_mvdisc :: setIOoutputSpecific "<<PassDownOutput<<endl;

        
        if ( PassDownOutput )
            setIOoutputSpecific();
        
                //stats tracking
        if (trackStats) {
			runTrackStats();
//            cout<<"ran track stats"<<NumberOfTestSamples_<<endl;
        }
        
        if ( PassDownOutput ) {
			setIOoutput();
            //cout<<"passed down input "<<NumberOfTestSamples_<<endl;
        }
      
        if (NumberOfTestSamples_ == 0 ) //set either way to make sure it resets for the iteartion
            do_applyToTestData_=0;
        else
            do_applyToTestData_=1;
       // DoGenAcc_=false;
		if ((NumberOfTestSamples_>0)|| (NumberOfSamples_>0)) {
			cout<<"Run classify "<<endl;
			classify();//pretty much same as apply to test data
//                cerr<<"About to degenacc "<<DoGenAcc_<<endl;
            if (DoGenAcc_)
            {

                generalizationAccuracy();
            }
        
        
        }
        cout<<"delete output data "<<endl;
        deleteInputsOutputData();
        cout<<"delete output data done"<<endl;

	}


    void base_mvdisc::setDoGeneralizationAccuracy( const bool & state )
    {
        DoGenAcc_=state;
    }

	void base_mvdisc::setClassifyTrainingData( const bool & state )
	{
		do_applyToTrainingData_ = state;
	}
	void base_mvdisc::setClassifyTestData(  const bool & state )
	{
		do_applyToTestData_ = state;
	}



	
	void base_mvdisc::estimateParameters()
	{
		//cout<<"base_mvdisc : estimateParameters "<<endl;
	}
	
	
	void base_mvdisc::setParameters()
	{
		
	}

	int base_mvdisc::classifyTrainingData() 
	{
		copyToSingleVector( in_data, out_data, NumberOfTestSamples_ );
		return 0;
	}
	int base_mvdisc::classifyTestData() 
	{
      //  cout<<"classify test data"<<endl;
		copyToSingleVector( in_test_data, out_test_data, NumberOfTestSamples_ );
      //  cerr << "just classified test data" << endl;
		return 0;
	}
	int base_mvdisc::classify() 
	{
		clearOutData();


		if (disc_distance==NULL)
			disc_distance = new vector<float>(NumberOfTestSamples_,0);
		else {
			disc_distance->clear();
		}
		
		int succeed=0;
		if (do_applyToTrainingData_)
		{

            if (!out_data->empty())
                out_data->clear();

			//cout<<"apply to training data "<<endl;

			succeed=classifyTrainingData();
		}
		if (do_applyToTestData_) {

            if (!out_test_data->empty())
                out_test_data->clear();

			disc_distance->resize(NumberOfTestSamples_);
			succeed = classifyTestData();
            //cerr<<"done aplly to test "<<endl;


		}
        //cout<<"classify base disc end"<<endl;

		return succeed;
	}
    TpFpTnFn base_mvdisc::generalizationAccuracyKfold(  )
    {
        
   // cerr<<"base_mvdisc::generalizationAccuracyKfold - coded as LOO at moment "<<endl;
        base_mvdisc* my_disc;
        
        switch (nodeTypeSpec) {
            case LDA:
                my_disc = new LDA_mvdisc();
                break;
            case QDA:
                my_disc = new QDA_mvdisc();
                break;
                
            default:
                throw ClassificationTreeNodeException(("base_mvdisc::generalizationAccuracy :: Node Type Not Supported for this function :: " + NodeName).c_str() );
                break;
        }
        my_disc->setDoGeneralizationAccuracy(false);
        
        
        TreeSampler* tree_samp = new TreeSampler();
        tree_samp->setVerbose(Verbose);//turn off gets to be too much in innner loop
        ClassificationTree* TreeInner = new ClassificationTree;
        DataSource* d_src_innner = new DataSource();
        
        smatrix m_in_data(NumberOfSamples_,Dimensionality_);
        smatrix m_target;
        training_target->getTargetData(m_target);
        //         smatrix_print(m_target, "mtarget");        
        copyToSingleVectorTranspose(in_data, m_in_data.sdata, NumberOfSamples_);
        //        smatrix_print(m_in_data, "m_in_data");
        
        
        Target* target_inner = new Target(training_target);
        
        d_src_innner->setInputData(m_in_data);
        
        TreeInner->addRoot(my_disc);
        TreeInner->attachToRoot(d_src_innner);
        TreeInner->setTarget(target_inner);
        
        
        tree_samp->setTree(TreeInner);
        
        vector<int> v_mask(NumberOfSamples_,1);
        //        cerr<<"run k-fold"<<endl;
        unsigned int K = NumberOfSamples_;
        //         K=5;

                TreeSampleData t_sample_data(K,training_target->getStratifyLabels(),0);
//              t_sample_data.model_thresh_mode=2;
        
               tree_samp->run_Kfold(v_mask,0,t_sample_data);
        
                float tp(0),fp(0),fn(0),tn(0);
                for ( vector< pair<TpFpTnFn,TpFpTnFn> >::iterator i = t_sample_data.v_tp_fp_tn_fn.begin(); i != t_sample_data.v_tp_fp_tn_fn.end();++i)
                {        
                    tp+=i->first.tp;
                    tn+=i->first.tn;
                    fp+=i->first.fp;
                    fn+=i->first.fn;
                }
        
  
        delete tree_samp;

        return TpFpTnFn(tp,fp,tn,fn);
        
        
    }
    void base_mvdisc::generalizationAccuracy(  )
    {
//        cerr<<"base_mvdisc::generalizationAccuracy "<<doKfold_<<endl;
        base_mvdisc* my_disc;
        
        switch (nodeTypeSpec) {
            case LDA:
                my_disc = new LDA_mvdisc();
                break;
            case QDA:
                my_disc = new QDA_mvdisc();
                break;
                
            default:
                throw ClassificationTreeNodeException(("base_mvdisc::generalizationAccuracy :: Node Type Not Supported for this function :: " + NodeName).c_str() );
                break;
        }
        
        //go one more level to estimate inner gen Acc
        my_disc->setDoKFold(true);
        my_disc->setDoGeneralizationAccuracy(true);

        //determines whcihc loop level its in
        if (doKfold_)
        {//in the second loop it will just calculate the LOO gen Accuracy
            
            if (Verbose >= 2 )
                cerr<<"base_mvdisc::generalizationAccuracy --> Do Second Inner LOO... "<<endl;
            genData_ = generalizationAccuracyKfold();//this is need incase it is not a second level loop
            genData_l2_.push_back(genData_);
            return;
        }
        if (Verbose >= 1 )
         cerr<<"base_mvdisc::generalizationAccuracy --> Do First Inner Loop - minGroupSubSampling... "<<endl;
        TreeSampler* tree_samp = new TreeSampler();
        ClassificationTree* TreeInner = new ClassificationTree;
        DataSource* d_src_innner = new DataSource();
        Target* target_inner = new Target(training_target);

        //local copy of input data
        smatrix m_in_data(NumberOfSamples_,Dimensionality_);
        smatrix m_target;
          training_target->getTargetData(m_target);
        
        //copying in input data
        copyToSingleVectorTranspose(in_data, m_in_data.sdata, NumberOfSamples_);


        //Setting up the tree
        d_src_innner->setInputData(m_in_data);
        TreeInner->addRoot(my_disc);
        TreeInner->attachToRoot(d_src_innner);
        TreeInner->setTarget(target_inner);
        
        //assigns tree to tree sampler.
        tree_samp->setTree(TreeInner);
        
        vector<int> v_mask(NumberOfSamples_,1);

        TreeSampleData t_sample_data_mins(0,training_target->getStratifyLabels(),0);
        
        float keep_rate= 0.05;
        float k = 0.8;
        float leftout = (1-k) * NumberOfSamples_;
        float NperSubject=1;
        unsigned int Nsamps= (NumberOfSamples_ / leftout) * NperSubject / keep_rate ;
       // cerr<<"Verbose "<<Verbose<<endl;
        if (Verbose>=1)
            cout<<"base_mvdisc::generalizationAccuracy-->Nsamps "<<Nsamps<<endl;
        
        //runs the subsampling
        tree_samp->minGroupSubsampling(Nsamps, false,false, k, v_mask, t_sample_data_mins);
    
        {//calculate mean genAccc
            
//            float mean_acc=0.0;
            float tpm(0),fpm(0),fnm(0),tnm(0);

            unsigned int count = 1 ;
            for ( std::vector< std::pair<TpFpTnFn,TpFpTnFn> >::iterator i = t_sample_data_mins.v_tp_fp_tn_fn.begin(); i != t_sample_data_mins.v_tp_fp_tn_fn.end(); ++i,++count)
            {
                tpm+=i->first.tp;
                tnm+=i->first.tn;
                fpm+=i->first.fp;
                fnm+=i->first.fn;

            }
            genData_  = TpFpTnFn(tpm,fpm,tnm,fnm);
            // if (Verbose>=3)
                cerr<<"mean_genacc "<<" "<<genData_.acc()<<endl;
        }
        
        
        float max_modelRunThresh_l(0), max_modelRunThresh_u(0),max_modelRunThresh_acc(0);

        {//this uses bagging and generates different results based on threshold
            
            vector<int> targ_data = target_inner->getTargetData<int>();
            //genAcc2 stores the accuracies for each subsample of the innner loop
            vector<TpFpTnFn> gen_acc_l2 =  my_disc->getGeneralizationDataLevel2();
            //            for (vector<TpFpTnFn>::iterator i_acc = gen_acc_l2.begin(); i_acc != gen_acc_l2.end(); ++i_acc)
            //            {
            //                cerr<<"gen acc "<<i_acc->acc()<<endl;
            //            }
            //            list< pair<float,unsigned int > > ::iterator i_acc = t_sample_data_mins.l_accuracy_pergroup.begin();
            //l_acc is training accuracy
            
            //need generalization accouracy for inner estimates
            
            // DO BAGGING AND GRID SEARCH
            // float modelRunThresh=0.5;
            
            float Nb_res(0.05),Nm_res(0.05);
            
            unsigned int Nb= 0;// static_cast<unsigned int>((0.5 - Nb_res)/Nb_res) + 1;
            unsigned int Nm= 0;// 1/Nm_res;
            
            for ( float midBagThresh = Nb_res ; midBagThresh<= 0.501 ; midBagThresh += Nb_res)
            {
                //              cerr<<"midbagthresh "<<midBagThresh<<endl;
                
                ++Nb;
            }
            for ( float modelRunThresh = 0.0 ; modelRunThresh< 1 ; modelRunThresh += Nm_res)
                ++Nm;
            
            
            
            
            //            cerr<<"NbNm "<<Nb<<" "<<Nm<<endl;
            genData_BagThreshXBagModel_.resize(Nm,Nb);
            float* d_ptr=genData_BagThreshXBagModel_.sdata;
            for ( float midBagThresh = Nb_res ; midBagThresh<= 0.501 ; midBagThresh += Nb_res)
            {
                //                cerr<<"midbagthresh "<<midBagThresh<<endl;
                float bthresh_u = 1- midBagThresh;
                
                for ( float modelRunThresh = 0.0 ; modelRunThresh< 1 ; modelRunThresh += Nm_res,++d_ptr)
                {
                    
                    vector< vector<int>  > all_runs(NumberOfSamples_);//stores all the votes
                    vector<float> bagged_perc(NumberOfSamples_);//ore-threshold
                    vector<int> bagged(NumberOfSamples_); //post-threshold - binarized

                    //THIS LOOP CUMULATES ALL SAMPLES
                    for ( vector<int3>::iterator i_est = t_sample_data_mins.estimates_all.begin(); i_est != t_sample_data_mins.estimates_all.end(); ++i_est)
                    {
                        if (gen_acc_l2[i_est->z].acc() >= modelRunThresh )
                            all_runs[i_est->x].push_back(i_est->y);                        
                    }
                    
                    //THIS LOOP DOES THE BAGGING
                    TpFpTnFn overall_errrorData(0,0,0,0);
                    //for each Run.....
                    vector<float>::iterator i_bp = bagged_perc.begin();
                    vector<int>::iterator i_b = bagged.begin();
                    //            vector<bool>::iterator i_m = bagged_mask.begin();
                    vector<int>::iterator i_t = targ_data.begin();
                    for (vector< vector<int> >::iterator i_r = all_runs.begin(); i_r != all_runs.end(); ++i_r, ++i_b,++i_bp,++i_t)
                    {
                        if (Verbose>=3)
                                        print(*i_r, "predictions");
                        //requires 0/1 class at moment
                        if ( ! i_r->empty())//make sure it had some votes , i.e. non-zero
                        {
                            if (Verbose>=3)
                                cout<<"mean "<< mean<int,float>(*i_r)<<endl;
                            *i_bp = mean<int,float>(*i_r);
                            //                            cerr<<"midbagthresh  "<<*i_bp<<" "<<midBagThresh<<" "<<bthresh_u<<endl;
                            if (( *i_bp <= midBagThresh ) || (*i_bp >= bthresh_u))
                            {
                                
                                
                                
                                *i_b = ( *i_bp >0.5) ? 1 : 0 ;
                                if ((*i_b == 1) && (*i_t ==1 ))
                                    ++overall_errrorData.tp;
                                else if ((*i_b == 0) && (*i_t ==0 ))
                                    ++overall_errrorData.tn;
                                else if ((*i_b == 1) && (*i_t == 0 ))
                                    ++overall_errrorData.fp;
                                else if ((*i_b == 0) && (*i_t ==1 ))
                                    ++overall_errrorData.fn;
                            }
                        }//cerr<<mean<int,float>(*i_r)<<endl;
                        
                    }
                    //            print(bagged_perc,"bagged_perc");
                    //                cerr<<"bagged accuracy "<<modelRunThresh<<" "<<overall_errrorData.acc()<<endl;
                    if (overall_errrorData.acc()>= max_modelRunThresh_acc)
                    {
                        max_modelRunThresh_u = modelRunThresh;
                        if (overall_errrorData.acc() > max_modelRunThresh_acc)
                            max_modelRunThresh_l = modelRunThresh;
                        
                        
                        max_modelRunThresh_acc=overall_errrorData.acc();
                        
                    }
                    //  cerr<<"set accuracy "<<endl;
                    
                    if (overall_errrorData.N()==0)
                        *d_ptr=0;
                    else
                        *d_ptr= overall_errrorData.acc();
                    //cerr<<"is set"<<endl;
                    //                    cerr<<"test model Thresh "<<midBagThresh<<" "<<modelRunThresh<<" "<<overall_errrorData.acc()<<endl;
                    
                    
                    
                }
                model_thresh_l_=max_modelRunThresh_l;
                model_thresh_u_=max_modelRunThresh_u;
                
                
            }
            
            
        }
        
        
        //  delete TreeInner;
        delete tree_samp;
        
        //these should already be delete
//        if ( TreeInner != NULL)
//        {cerr<<"delete tree"<<endl;delete TreeInner;}
//        if (d_src_innner != NULL)
//        {   cerr<<"delete tree"<<endl; delete d_src_innner;}
//        if (target_inner != NULL)
//        {   cerr<<"delete tree"<<endl;delete target_inner;}

        
//        cerr<<"base_mvdisc::generalizationAccuracy - done"<<endl;

//        genData_  = TpFpTnFn(tp,fp,tn,fn);
//        cerr<<"genData "<<genData_.tp<<" "<<genData_.fp<<" "<<genData_.tn<<" "<<genData_.fn<<" "<<endl;

//        cerr<<"genData->acc "<<genData_.acc()<<" "<<genData_.sens()<<" "<<genData_.spec()<<endl;
        
        
    }
    
   




}
