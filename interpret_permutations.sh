#!/bin/sh

#  Script.sh
#  su_mvdisc
#
#  Created by Brian M. Patenaude on 8/6/12.
#  Copyright 2012 Stanford University - Etkin Lab. All rights reserved.
#CSVUTILSDIR=/Users/home/brianp/Library/Developer/Xcode/DerivedData/csv_utils-dofgnkakhxeqvfdycfjkezchspds/Build/Products/Debug/

CSVUTILSDIR=`dirname $0`

function Usage(){
    echo "interpret_permutations.sh <disc_dir> <p-value> \n "

}

if [ $# -ne 2 ]; then 
    Usage
    exit 1
fi
DISCDIR=$1
pval=$2
if [ ! -d $DISCDIR ] ; then 

    echo "\n Directory, $DISCDIR, does not exist \n"
    
fi


#N=`ls $DISCDIR/Leave-1-Out.perms/*errordata_ci.csv | wc | awk '{ print $1 }'`
#N=`ls $DISCDIR/Leave-0-Out.perms/*errordata.csv | wc | awk '{ print $1 }'`
N=0;
for i in $DISCDIR/Leave-0-Out.perms/*errordata.csv ; do 
    let N+=1
done


echo N $N

if [ $N -lt 10 ] ;then
    echo "Only found $N permutations $N"
    exit 1
fi

#grep -h mean $DISCDIR/Leave-1-Out.perms/*errordata_ci.csv | awk -F , '{ print $3,$4,$5,$6,$7 }' > $DISCDIR/Leave-1-Out.perms.results${N}.csv
if [ -f $DISCDIR/Leave-0-Out.perms.results${N}.csv ]; then
    rm $DISCDIR/Leave-0-Out.perms.results${N}.csv
fi

test_vals=`sed -n '2p' ${DISCDIR}/Leave-0-Out/cv_results_disc_errordata.csv | awk -F , '{ print $2" "$3" "$4" "$5" "$6 }'`
echo test_vals $test_vals

for i in $DISCDIR/Leave-0-Out.perms/*errordata.csv ; do

    sed -n '2p' $i| awk -F , '{ print $2","$3","$4","$5","$6 }' >> $DISCDIR/Leave-0-Out.perms.results${N}.csv
#sed -n '2p' $DISCDIR/Leave-1-Out.perms/*errordata.csv | awk -F , '{ print $2,$3,$4,$5,$6 }' > $DISCDIR/Leave-1-Out.perms.results${N}.csv
done
echo "vals "
echo "interpet results"
${CSVUTILSDIR}/csv_utils $DISCDIR/Leave-0-Out.perms.results${N}.csv -interpretAsPermutations $pval $test_vals $DISCDIR/Leave-0-Out.perms.results${N}_thresh${pval}.csv




