/*
 *  run_loo.cpp
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 7/19/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

//
//#include "lda.h"
//#include "svm_disc.h"
//#include "NormalizeFilter.h"
//#include "nifti_writer.h"
//FSL headers                                                                                                                             
#include "utils/options.h"

#include <fstream>
#include <iostream>
#include <vector>
#include <list>
#include <sys/stat.h> 
#include <sys/time.h> 
#include <cmath>


#include <miscmaths/t2z.h>

//brian headers
//#include <misc_utils.h>

//#include "image_source.h"
#include "ClassificationTree.h"
#include "tree_sampler.h"
#include <misc_utils.h>
#include "su_mvdisc_structs.h"

//FSL headers
// #include "utils/options.h"

//3rd party tools
//#include "cublas.h"

using namespace std;
//FSL namespaces

using namespace Utilities;
//our namespaces
using namespace misc_utils_name;
using namespace su_mvdisc_name;



string title="discriminate (Version 0.1) Stanford University (Brian Patenaude)";
string examples="discriminate -i data.tree -o output ";


Option<int> verbose(string("-v,--verbose"), 0,
		string("switch on diagnostic messages"), 
		false, requires_argument);
Option<bool> help(string("-h,--help"), false,
		string("display this message"),
		false, no_argument);
//Standard Inputs
Option<string> inname(string("-i,--in"), string(""),
		string("Filename of input discriminant tree."),
		true, requires_argument);
Option<string> intestname(string("-t,--intest"), string(""),
		string("Filename of input discriminant tree."),
		false, requires_argument);


Option<string> outname(string("-o,--outputName"), string(""),
		string("Output name"),
		true, requires_argument);
Option<string> sens_spec_class_inclusion (string("--sens_spec_class_inclusion,--sens_spec_class_inclusion"), "",
		string("Define the class the include for calculatation of sensitivity and specificity. Of the form \"0,1,\""),
		false, requires_argument);
Option<bool> bag(string("-b,--bag"), false,
		string("Bag Across Samples."),
		false, no_argument);

Option<bool> wBagByProb(string("--weightBaggingByProb"), false,
		string("Weight the smaples in the bagging by conditional probabilities."),
		false, no_argument);
Option<int> bag_threshMode(string("--BaggingThresholdMethod"), 0,
		string("Specify how it chooses to include samples when bagging. 0 = include everything, 1 = use a mixture of gaussians"),
		false, requires_argument);


Option<float> bag_mid_thresh(string("-bag_th,--bag_mid_thresh"), 0,
		string("Excludes application to people that lie between threshold."),
		false, requires_argument);
Option<string> selection_mask(string("-selection_mask,--selection_mask"), string(""),
		string("Use previously defined selection maks."),
		false, requires_argument);
Option<string> loadSamples(string("-loadSamples,--loadSamples"), string(""),
		string("Use previously defined subsamples. Used to replicate runs."),
		false, requires_argument);
Option<string> loadPermutedTarget(string("-loadPermutedTarget,--loadPermutedTarget"), string(""),
		string("Use previously defined target permutation. Used to replicate permutation runs."),
		false, requires_argument);

Option<bool> useSampleMask(string("-mask,--mask"), false,
		string("Use the selection mask to define sample, otherwise selection mask is performed prior to sampling"),
		false, no_argument);
Option<float> Moffset(string("-m,--minimumOffset"), 0,
		string("Samples less then minimum group size."),
		false, requires_argument);
Option<float> AccThr(string("-t,--accThresh"), 0,
		string("Minimum Within Group Accuracy required to retain sample."),
		false, requires_argument);
Option<float> NAccThr(string("-u,--NaccThresh"), 0,
		string("Percentage of sample to include from within Group Accuracy required to retain sample."),
		false, requires_argument);
Option<int> Nsub(string("-Ns,--Nsubsamples"), 0,
		string("Number of subsamples of the data to perform. If Nsub is 0, then will default to Leave-N-Out."),
		false, requires_argument);
Option<int> B(string("-B,--SubSampleSize"), 0,
		string("Number of samples for a givig subsamples."),
		false, requires_argument);
Option<int> bootstrap_opt(string("--bootstrap,--bootstrap"), 0,
		string("Number of samples for a givig subsamples."),
		false, requires_argument);


Option<int> K(string("-K,--Kfold"), -9,
		string("Which K-fold Cross-validation to perform"),
		false, requires_argument);
Option<int> N(string("-N,--LeaveNout"), 0,
		string("Which N-leave-one-out"),
		false, requires_argument);

Option<int> Npermutations(string("--Npermutations,--Npermutations"), 0,
		string("Number of permuations to run for permutation testing. Runs permuattion testing if greater than 0."),
		false, requires_argument);
Option<int> Npermutations_offset(string("--Npermutations_offset,--Npermutations_offset"), 0,
		string("Offsets the permutation index by value (default=0)."),
		false, requires_argument);


Option<int> write_level(string("--write_level"), 1,
		string("write_level is an integer [0-3] indicating how much of the results to write out"),
		false, requires_argument);

int nonoptarg;








//taken from http://www.techbytes.ca/techbyte103.html
bool FileExists( const string & strFilename) { 
	struct stat stFileInfo; 
	bool blnReturn; 
	int intStat; 

	// Attempt to get the file attributes 
	intStat = stat(strFilename.c_str(),&stFileInfo); 
	if(intStat == 0) { 
		// We were able to get the file attributes 
		// so the file obviously exists. 
		blnReturn = true; 
	} else { 
		// We were not able to get the file attributes. 
		// This may mean that we don't have permission to 
		// access the folder which contains this file. If you 
		// need to do that level of checking, lookup the 
		// return values of stat which will give you 
		// more details on why stat failed. 
		blnReturn = false; 
	} 

	return(blnReturn); 
}



int do_work(const int & verb, const string & filename, const string & filename_test, const unsigned int & K, const unsigned int & Nsub, const string & outname, const string & class_map_inc, const string & selection_mask,const string & load_samples_name, const string & loadPermutedTarget_name , const bool  & use_sample_mask, const unsigned int & Npermutations, const float & min_offset,const unsigned int & ss_size, const unsigned int & Npermutations_offset , const bool & doBagging, const bool & weightBagByProb, const float & tr_acc_thresh,const float & n_tr_acc_thresh, const float & bag_mid_th_in , const int & bootstrap, const int & bag_thresh_mode, const int & write_level_in)
{
	//    	cout<<"Do Work "<<tr_acc_thresh<<" "<<n_tr_acc_thresh<<endl;
	//argc must be equal to number of input variables +1 
	/*if ( ((argc!= 5 ) && (argc!=6)) ||  (!strcmp(argv[1],"--help")) ) 
	{
		cout<<"****************************"<<endl<<endl;
		cout<<"Usage: "<<endl<<endl;
		cout<<"run_loo tree_model.txt K Nsub output_base selection mask"<<endl<<endl;
		cout<<"----------------------------"<<endl<<endl;
		cout<<"K  : K-fold Cross-validation."<<endl<<endl;
		cout<<"Nsub : Nsub different subsamples of K-fold Cross-validation."<<endl<<endl;
		cout<<"output_base : base name for all output files."<<endl<<endl;
		cout<<"--doPermutations"<<endl<<endl;
		cout<<"****************************"<<endl<<endl;

		exit (EXIT_FAILURE);
	}
	 */
	//cublasInit();

	//string filename = argv[1];
	if (!FileExists(filename))
	{
		cerr<<"Model File does not exist : " << filename <<endl;
		exit (EXIT_FAILURE);
	}


	bool Permute = false;
	if (Npermutations>0)	
		Permute = true;

	list< vector<int> > sel_mask;
	vector<int> v_sel_mask;
	bool loadedSelMask=false;


	if (use_sample_mask)
	{
		if ( selection_mask != "" )
		{
			//  sel_mask=ClassificationTree::readSubSampleMask_from_SelMask(selection_mask);
			// if (sel_mask.size() != 1)
			//  throw ClassificationTreeException("Incompatible size of selection mask to use with sampling techniques");
			int itemp;
			ifstream fin(selection_mask.c_str());
			while ( fin >> itemp) {
				//cerr<<"mask read "<<itemp<<endl;
				v_sel_mask.push_back(itemp);
			}
			fin.close();
			loadedSelMask=true;

		}
	}

	TreeSampler* tree_samp = NULL;

	ClassificationTree *DiscTree = new ClassificationTree();
	DiscTree->setVerbose(verb);
    cout<<"---------------------"<<endl<<"Loading Tree "<<filename<<"..."<<endl;
	DiscTree->load(filename);	
    cout<<"Done Loading Tree."<<endl<<"---------------------"<<endl;
    //cout<<"Running "<<K<<"-Fold Cross-Validation "<<Nsub<<endl;
	//		DiscTree->run_KFold_CV(K,ClassificationTree::CLEAR);
	//		vector<int> labels(2,0);//these are the labels to stratify 
	//	labels[1]=1;
	vector<int> labels = DiscTree->getStratifyLabels();
	//DiscTree->run_subsampling(5,K,labels);
	//	DiscTree->run_subsampling(Nsub,K,labels,0, false);//number fo subsamples, K-fold CV, labels, number to recue gorup, whther to include left out subects in pred

	//try {
	if (Permute){
		//		if ((loadedSelMask)&&(use_sample_mask)){
		//			//void run_subsampling( outname,  unsigned int & N , const unsigned int & K,const std::vector<int> & stratified_labels ,   const unsigned int & minSizeOffset, bool USE_NON_STRATIFIED_SUBJECTS = true, bool USE_LEFTOUT_FOR_TEST =false , std::list< std::vector<int> >*  input_subsamplings = NULL );
		//			//	DiscTree->run_subsampling("lalal", Nsub,K,labels,0, true, false);
		//			
		//			
		//			
		//			DiscTree->run_permutationTest(Npermutations, outname, class_map_inc, Nsub,K,labels,min_offset, Npermutations_offset,true,false,&sel_mask);//number fo subsamples, K-fold CV, labels, number to recue gorup, whther to include left out subects in pred
		//			
		//		}else {

		cout<<"Run permutations"<<endl;
		// srand ( static_cast<unsigned int>(time(NULL)) );

		struct timeval time;
		gettimeofday(&time,NULL);
		// microsecond has 1 000 000
		// Assuming you did not need quite that accuracy
		// Also do not assume the system clock has that accuracy.
		//cout<<"tree seed "<<(time.tv_sec * 1000) + (time.tv_usec / 1000)<<endl;
		srand((time.tv_sec * 1000) + (time.tv_usec / 1000));


		vector< vector<float> > all_permtargs;
		bool use_prePerms=false;
		if (! loadPermutedTarget_name.empty())
		{

			ifstream f_perm(loadPermutedTarget_name.c_str());
			if (! f_perm.is_open())
				throw ClassificationTreeException(("Unable to read specifed permuted target file : "+ loadPermutedTarget_name).c_str());

			string line;
			while ( getline(f_perm, line) ) {
				all_permtargs.push_back(csv_string2nums_f(line) );
			}
			f_perm.close();
			use_prePerms=true;
		}

		vector< vector<float> >::iterator i_ptarg = all_permtargs.begin();

		ofstream f_ptarg((outname + "_permutedTarg.csv").c_str());
		//	DiscTree->run_permutationTest(Npermutations, outname,class_map_inc,  Nsub,K,labels,min_offset,Npermutations_offset, true, false);//number fo subsamples, K-fold CV, labels, number to recue gorup, whther to include left out subects in pred
		for (unsigned int i = 0 ; i < Npermutations; ++i)
		{
			cout<<"Permuation "<<i<<endl;
			//                vector< pair<float3,float3> > vAccSensSpec,vAccSensSpecOutMask;
			//                vector<float3> withinGroupErr;
			//                list< pair<float,unsigned int> > l_accuracy_pergroup;
			//                vector< map<int, pair<float, unsigned int> > > class_accuracies;
			//                vector< pair<TpFpTnFn,TpFpTnFn> > v_tp_fp_tn_fn,v_tp_fp_tn_fn_thresh;
			//              // vector<int2> predictions;
			//                vector< vector<int> > predictions;


			//                cerr<<"K "<<K<<endl;
			TreeSampleData tree_samp_data(K,labels,bag_mid_th_in);
			tree_samp_data.model_thresh_mode=bag_thresh_mode;
			vector<int> stratified_labels(2,0);// = labels;
			stratified_labels[1]=1;
			tree_samp_data.stratified_labels=stratified_labels;
			tree_samp_data.writeLevel=write_level_in;
			if (! load_samples_name.empty())
				tree_samp_data.subSamplesFilename = load_samples_name;


			if (use_prePerms)
			{
				cerr<<"load permuted target "<<i_ptarg->size()<<endl;
				print(*i_ptarg,"perm-target");
				DiscTree->loadPermutedTarget(*i_ptarg);
				++i_ptarg;

			}else
				DiscTree->permuteTarget(v_sel_mask);


			vector<float> targ = DiscTree->getTargetDataFromSrc<float>();
			cerr<<"targ size "<<targ.size()<<endl;
			print(targ,"target");
			vector<float>::iterator i_t = targ.begin();
			f_ptarg<<*i_t;
			++i_t;
			for ( ;i_t != targ.end(); ++i_t)
			{
				f_ptarg<<","<<*i_t;
			}
			f_ptarg<<endl;




			tree_samp = new TreeSampler();
			tree_samp->setVerbose( verb );
			tree_samp->setTree(DiscTree);
			//  cout<<"treesampe set thresholds "<<tr_acc_thresh<<" "<<n_tr_acc_thresh<<endl;

			tree_samp->setTrainingAccuracyThreshold(tr_acc_thresh);
			tree_samp->setTrainingAccuracyNThreshold(n_tr_acc_thresh);


			///change K afterwars
			//tree_samp->bag_absoluteSubsample( Nsub,K );
			if (Nsub > 0 ){
				cerr<<"Run Bagging with Sub Sampling "<<Nsub<<endl;
				//    tree_samp->minGroupSubsampling(Nsub,doBagging,min_offset,v_sel_mask, vAccSensSpec,predictions,class_accuracies,v_tp_fp_tn_fn,v_tp_fp_tn_fn_thresh, 0.25, stratified_labels,withinGroupErr,l_accuracy_pergroup,K,outname+"_subsamples");
				tree_samp->minGroupSubsampling(Nsub,doBagging,weightBagByProb, min_offset,v_sel_mask, tree_samp_data,outname+"_subsamples");


			}else{
				cerr<<"Run KFold2"<<endl;
				//                    tree_samp->run_Kfold(K,vAccSensSpec,predictions,class_accuracies,v_tp_fp_tn_fn,v_tp_fp_tn_fn_thresh,0.25,stratified_labels,withinGroupErr,l_accuracy_pergroup,outname+"_subsamples");
				tree_samp->run_Kfold(v_sel_mask,doBagging, tree_samp_data,outname+"_subsamples");


			}



			//                    cerr<<"Write output for permutation "<<i<<" : "<<outname<<endl;
			ofstream fout((outname+"_accuracy.csv").c_str());
			if ( ! fout.is_open() )
				throw ClassificationTreeNodeException(("Could not open "+ (outname+"_accuracy.csv") + " to write.").c_str());


			for (vector< pair<float3,float3> >::iterator i_acc = tree_samp_data.AccSensSpec.begin(); i_acc != tree_samp_data.AccSensSpec.end(); ++i_acc)
				fout<<i_acc->first.x<<","<<i_acc->first.y<<","<<i_acc->first.z<<endl;
			fout.close();

			ofstream fout_sens((outname+"_sensitivities.csv").c_str());

			fout_sens<<"class,number_of_samples,accuracy"<<endl;
			for ( vector< map<int, pair<float, unsigned int> > >::iterator ii = tree_samp_data.class_sensitivities.begin() ; ii != tree_samp_data.class_sensitivities.end(); ++ii)
				for ( map<int, pair<float, unsigned int> >::iterator i = ii->begin() ; i != ii->end(); ++i)
				{
					//  cout<<"Classout : "<<i->first<<" - Accuracy : "<<i->second.first<<" - N : "<<i->second.second<<endl;
					fout_sens<<i->first<<","<<i->second.second<<","<<i->second.first<<endl;

				}
			fout_sens.close();



			{
				//error data
				ofstream fout_errdata((outname+"_errordata.csv").c_str());
				fout_errdata<<"number_of_samples,accuracy,sensitivity,specificity,positive_predictive_value,negative_predictive_value,TruePositives,FalsePositives,TrueNegatives,FalseNegative"<<endl;

				for (vector< pair<TpFpTnFn,TpFpTnFn> >::iterator i_err = tree_samp_data.v_tp_fp_tn_fn.begin(); i_err != tree_samp_data.v_tp_fp_tn_fn.end();++i_err)
					fout_errdata<<i_err->first.N()<<","<<i_err->first.acc()<<","<<i_err->first.sens()<<","<<i_err->first.spec()<<","<<i_err->first.ppv()<<","<<i_err->first.npv()<<","<<i_err->first.tp<<","<<i_err->first.fp<<","<<i_err->first.tn<<","<<i_err->first.fn<<endl;
				fout_errdata.close();

				//                    cerr<<"write new eroro dara"<<endl;
				//error data
				ofstream fout_errdata_thresh((outname+"_errordata_thresh.csv").c_str());
				fout_errdata_thresh<<"number_of_samples,accuracy,sensitivity,specificity,positive_predictive_value,negative_predictive_value,TruePositives,FalsePositives,TrueNegatives,FalseNegative"<<endl;

				for (vector< pair<TpFpTnFn,TpFpTnFn> >::iterator i_err = tree_samp_data.v_tp_fp_tn_fn_thresh.begin(); i_err != tree_samp_data.v_tp_fp_tn_fn_thresh.end();++i_err)
					fout_errdata_thresh<<i_err->first.N()<<","<<i_err->first.acc()<<","<<i_err->first.sens()<<","<<i_err->first.spec()<<","<<i_err->first.ppv()<<","<<i_err->first.npv()<<","<<i_err->first.tp<<","<<i_err->first.fp<<","<<i_err->first.tn<<","<<i_err->first.fn<<endl;
				fout_errdata_thresh.close();

				ofstream fout_errdata_threshout((outname+"_errordata_thresh_out.csv").c_str());
				fout_errdata_thresh<<"number_of_samples,accuracy,sensitivity,specificity,positive_predictive_value,negative_predictive_val\
ue,TruePositives,FalsePositives,TrueNegatives,FalseNegative"<<endl;

				for (vector< pair<TpFpTnFn,TpFpTnFn> >::iterator i_err = tree_samp_data.v_tp_fp_tn_fn_thresh.begin(); i_err != tree_samp_data.v_tp_fp_tn_fn_thresh.end();++i_err)
					fout_errdata_threshout<<i_err->second.N()<<","<<i_err->second.acc()<<","<<i_err->second.sens()<<","<<i_err->second.spec()<<",\
"<<i_err->second.ppv()<<","<<i_err->second.npv()<<","<<i_err->second.tp<<","<<i_err->second.fp<<","<<i_err->second.tn<<","<<i_err->second.fn<<endl;
				fout_errdata_threshout.close();



			}
			if (tree_samp_data.v_tp_fp_tn_fn.size()>1)//case for non-bagging
			{                
				vector<float> v_sx(10,0), v_sxx(10,0),v_std(10,0),v_mean(10,0);
				unsigned int N=0;
				for (vector< pair<TpFpTnFn,TpFpTnFn> >::iterator i_err = tree_samp_data.v_tp_fp_tn_fn.begin(); i_err != tree_samp_data.v_tp_fp_tn_fn.end();++i_err,++N)
				{
					v_sx[0]+=i_err->first.N();
					v_sx[1]+=i_err->first.acc();
					v_sx[2]+=i_err->first.sens();
					v_sx[3]+=i_err->first.spec();
					v_sx[4]+=i_err->first.ppv();
					v_sx[5]+=i_err->first.npv();
					v_sx[6]+=i_err->first.tp;
					v_sx[7]+=i_err->first.fp;
					v_sx[8]+=i_err->first.tn;
					v_sx[9]+=i_err->first.fn;
					v_sxx[0]+=i_err->first.N()*i_err->first.N();
					v_sxx[1]+=i_err->first.acc()*i_err->first.acc();
					v_sxx[2]+=i_err->first.sens()*i_err->first.sens();
					v_sxx[3]+=i_err->first.spec()*i_err->first.spec();
					v_sxx[4]+=i_err->first.ppv()*i_err->first.ppv();
					v_sxx[5]+=i_err->first.npv()*i_err->first.npv();
					v_sxx[6]+=i_err->first.tp*i_err->first.tp;
					v_sxx[7]+=i_err->first.fp*i_err->first.fp;
					v_sxx[8]+=i_err->first.tn*i_err->first.tn;
					v_sxx[9]+=i_err->first.fn*i_err->first.fn;





				}

				ofstream fconf((outname+"_errordata_ci.csv").c_str());
				fconf<<",number_of_samples,accuracy,sensitivity,specificity,positive_predictive_value,negative_predictive_value,TruePositives,FalsePositives,TrueNegatives,FalseNegative"<<endl;
				fconf<<"mean";
				for ( unsigned int i = 0 ; i < v_sx.size(); ++i)
				{
					v_mean[i]=v_sx[i]/N;
					fconf<<","<<v_sx[i]/N;
					v_std[i]= sqrtf((v_sxx[i]-v_sx[i]*v_sx[i]/N)/(N-1));
					//                    cerr<<"std "<<v_std[i]<<" "
				}
				fconf<<endl;
				fconf<<"95%CI-LowerBound";
				for ( unsigned int i = 0 ; i < v_sx.size(); ++i)                    
					fconf<<","<<v_mean[i]-1.96*v_std[i]/sqrtf(N);
				fconf<<endl;
				fconf<<"95%CI-UpperBound";
				for ( unsigned int i = 0 ; i < v_sx.size(); ++i)                    
					fconf<<","<<v_mean[i]+1.96*v_std[i]/sqrtf(N);
				fconf<<endl;

				fconf.close();
			}



			//                ofstream fpred((outname+"_predictions.csv").c_str());
			//                int count=0;
			//                //predictions is sample count,decision
			//                fpred<<"Truth,Predictions..."<<endl;
			//                   for (vector< vector<int> >::iterator i = tree_samp_data.predictions.begin(); i!=tree_samp_data.predictions.end();++i,++count)                
			//                   {
			//                       fpred<<targ[count];
			//                       for (vector<int>::iterator ii = i->begin(); ii != i->end(); ++ii)
			//                            fpred<<","<<*ii;
			//                       fpred<<endl;
			//                   }
			//                fpred.close();


			// delete tree_samp;

		}
		f_ptarg.close();
		//}
	}else{
		if (verb >= 2 )
            cout<<endl<<"---------------------"<<endl<<"Setting up the Tree Sampler..."<<endl;

		
		//TreeSampleData struct contains the parameters to pass onto the tree sampler. 
		//The tree sampler runs the classification tree (flow), mannaging the appropriate sampling
		//e.g. Cross-Validation, Bootstrap sampling
		
		TreeSampleData tree_samp_data(K,labels,bag_mid_th_in);
		tree_samp_data.model_thresh_mode=bag_thresh_mode;  //Whether to bag across samples

		vector<int> stratified_labels(2,0);// = labels;
		stratified_labels[1]=1;
		//labels for which to sample/stratify from. 
		//The tree sampler may try to take equal number of samples across each label
		tree_samp_data.stratified_labels=stratified_labels; 
		tree_samp_data.bootstrap=bootstrap; 
		//Provides an indidication of how much data should be written to file.
		//Similar to verbosity except for files. The is implement due to space considerations 
		//when running a large number of samples
		tree_samp_data.writeLevel=write_level_in;

		//Indicates whether to load previously specified samples. Usefull for replicating analysis 
		//which depends on random sampling.
		if (! load_samples_name.empty())	tree_samp_data.subSamplesFilename = load_samples_name;

		//Output to terminal at runtime.
		if (verb >= 2 )
		{
			cout<<"     Number of Subsamples : "<<Nsub<<endl ;
			cout<<"     Minimum offset relative to smallest group size : "<<min_offset<<endl;
		}

		if (verb >= 2 ) cout<<"     Creating Sampler..."<<endl;
		tree_samp = new TreeSampler();
		
		if (verb >= 2 ) cout<<"     Initializing tree... "<<endl;

		//Sets the verboseness of the tree. This will be passed down to the nodes, it provides
		//information of the nodes running
		tree_samp->setTree(DiscTree);

		if (verb >= 2 ) cout<<"     Setting Verboseness to "<<verb<<endl;

		//Set the verboseness for the tree sampler. Provides information regarding the sampling. 
		tree_samp->setVerbose( verb );

		
		//These next few lines set acccuracy thresholds. These threshold are used in the post-processing
		//to look at performance after excluding subject with low estimated accuracies.
		if (verb >= 2 )	cout<<"Set accuracy thresholds : "<<tr_acc_thresh<<" "<<n_tr_acc_thresh<<endl;
		
		tree_samp->setTrainingAccuracyThreshold(tr_acc_thresh);
		tree_samp->setTrainingAccuracyNThreshold(n_tr_acc_thresh);

		//Whether the sampler is running sub samples.
		if (verb >= 2 )	cout<<"Running Sub Samples : "<< ((Nsub>0) ? "yes" : "no") <<endl;

        
        cout<<"Done setting up the Tree Sampler."<<endl<<"---------------------"<<endl;

        cout<<"Running models..."<<endl;

		//This is where it will run the multiple subsampling (bootstrapping) or whether it will run cross-validation
		//The decision is made based on whether the number of subsamples (Nsub) is greater then zero.
		if (Nsub > 0 )
		{
            cout<<"Running subsampling..."<<endl;
			//minGrooupSubsamping subsamples the data into equal group over many iteration. The number of samples is based on 
			//the size of the smallest group. It will samples <= the size of the smallest group, the offset from which is min_offset
			//Nsub indicates the number of random samples, doBagging whether the bag across samples, weightBagByProb inidicates to weight 
			//the contribution of a given subject by its estimated probability. v_sel_mask selects a subset of the subjects form the outset
			//this is useful so as not to have to regenerate data for different subject selection criteria.
			tree_samp->minGroupSubsampling(Nsub,doBagging, weightBagByProb , min_offset,v_sel_mask,tree_samp_data,outname+"_subsamples");
		}else{
            cout<<"Running K-fold... "<<endl;
			//run_Kfold runs K-fold cross-validation. doBagging whether the bag across samples, v_sel_mask selects a subset of the subjects form the outset
			//this is useful so as not to have to regenerate data for different subject selection criteria.
			tree_samp->run_Kfold(v_sel_mask,doBagging, tree_samp_data,outname+"_subsamples");
		}

        cout<<"done runnning models."<<endl<<"---------------------"<<endl;
        
			
		//From here on, we write out results to file.
		ofstream fwgr((outname+"_withinGrp.csv").c_str());
		for ( vector<float3>::iterator i = tree_samp_data.v_withinGrErr.begin(); i != tree_samp_data.v_withinGrErr.end(); ++i)
		{
			fwgr<<i->x<<","<<i->y<<","<<i->z<<endl;
		}
		fwgr<<endl;

		ofstream fout((outname+"_accuracy.csv").c_str());
		for (vector< pair<float3,float3> >::iterator i_acc = tree_samp_data.AccSensSpec.begin(); i_acc != tree_samp_data.AccSensSpec.end(); ++i_acc)
			fout<<i_acc->first.x<<","<<i_acc->first.y<<","<<i_acc->first.z<<endl;
		fout.close();

		ofstream fout2((outname+"_accuracy_out.csv").c_str());
		for (vector< pair<float3,float3> >::iterator i_acc = tree_samp_data.AccSensSpec.begin(); i_acc != tree_samp_data.AccSensSpec.end(); ++i_acc)
			fout2<<i_acc->second.x<<","<<i_acc->second.y<<","<<i_acc->second.z<<endl;
		fout2.close();

		cerr<<"open sensistivities.csv  "<<endl;
		ofstream fout_sens((outname+"_sensitivities.csv").c_str());
		fout_sens<<"class,number_of_samples,accuracy"<<endl;


		map<int, float>  m_sx,m_sxx,m_mean,m_std;
		//initialize for each class to zero
		if ( ! tree_samp_data.class_sensitivities.empty())
		{
			for ( map<int, pair<float, unsigned int> >::iterator i = tree_samp_data.class_sensitivities[0].begin() ; i != tree_samp_data.class_sensitivities[0].end(); ++i)
			{
				m_sx[i->first]=0;
				m_sxx[i->first]=0;
				m_mean[i->first]=0;
				m_std[i->first]=0;

			}
		}


		for ( vector< map<int, pair<float, unsigned int> > >::iterator ii = tree_samp_data.class_sensitivities.begin() ; ii != tree_samp_data.class_sensitivities.end(); ++ii)
			for ( map<int, pair<float, unsigned int> >::iterator i = ii->begin() ; i != ii->end(); ++i)
			{
				//cout<<"Classout : "<<i->first<<" - Accuracy : "<<i->second.first<<" - N : "<<i->second.second<<endl;
				fout_sens<<i->first<<","<<i->second.second<<","<<i->second.first<<endl;
				m_sx[i->first]+=i->second.first;
				m_sxx[i->first]+=i->second.first * i->second.first;

			}
		// fout_sens<<accuracy<<","<<sensitivity<<","<<specificity<<endl;
		fout_sens.close();
		cerr<<"write out confidence intervals per class"<<endl;
		//write out confidence intervals per class
		if ( ! tree_samp_data.class_sensitivities.empty())
		{
			//            cerr<<"write confidence intervals"<<endl;
			unsigned int Nsamp=tree_samp_data.class_sensitivities.size();
			ofstream fconf((outname+"_sensitivities_ci.csv").c_str());
			fconf<<",number_of_samples"<<endl;
			//do header
			for ( map<int,float>::iterator i_sx = m_sx.begin(); i_sx != m_sx.end(); ++i_sx)
			{
				stringstream ss;
				ss<<i_sx->first;
				string stemp;
				ss>>stemp;
				fconf<<",class_"<<stemp;
			}
			fconf<<endl;
			fconf<<"mean";
			{
				map<int,float>::iterator i_sxx = m_sxx.begin();
				map<int,float>::iterator i_sx = m_sx.begin();
				map<int,float>::iterator i_std = m_std.begin();
				for ( map<int,float>::iterator i_mean = m_mean.begin(); i_mean != m_mean.end(); ++i_mean,++i_sx,++i_sxx,++i_std)
				{
					if (i_sx->first != i_sxx->first )
						throw ClassificationTreeNodeException("Error: Tried to create Confidence Intervals but Sx and Sxx don't have corresponding labels" );
					if (i_sx->first != i_mean->first )
						throw ClassificationTreeNodeException("Error: Tried to create Confidence Intervals but Sx and Mean don't have corresponding labels" );
					if (i_sx->first != i_std->first )
						throw ClassificationTreeNodeException("Error: Tried to create Confidence Intervals but Sx and Mean don't have corresponding labels" );

					i_mean->second = i_sx->second / Nsamp; 
					i_std->second = sqrtf( ( i_sxx->second - ( i_sx->second *  i_sx->second )/Nsamp ) / ( Nsamp -1 ));

					fconf<<","<<i_mean->second;
				}
				fconf<<endl;
			}

			if (Nsamp > 1 )
			{
				cerr<<"MISCMATHS::Z2t::getInstance() "<<Nsamp<<endl;
				float t = MISCMATHS::Z2t::getInstance().convert(1.96,Nsamp-1);
				cerr<<"done eman "<<endl;

				{
					fconf<<"95%CI-LowerBound";
					map<int,float>::iterator i_std = m_std.begin();
					for ( map<int,float>::iterator i_mean = m_mean.begin(); i_mean != m_mean.end(); ++i_mean,++i_std)
						fconf<<","<< (i_mean->second - t * (i_std->second) / sqrtf(Nsamp));

					//    fconf<<","<< (i_mean->second - 1.96 * (i_std->second) / sqrtf(Nsamp));
					fconf<<endl;
				}
				{
					fconf<<"95%CI-UpperBound";
					map<int,float>::iterator i_std = m_std.begin();
					for ( map<int,float>::iterator i_mean = m_mean.begin(); i_mean != m_mean.end(); ++i_mean,++i_std)
						fconf<<","<<i_mean->second + t * i_std->second /sqrtf(Nsamp);

					fconf.close();
				}
			}
		}
		cerr<<"calculate accuracies,sens,spec,ppv,npv"<<endl;

		//calculate accuracies,sens,spec,ppv,npv
		{
			//error data
			{

				ofstream fout_errdata((outname+"_errordata.csv").c_str());
				fout_errdata<<"number_of_samples,accuracy,sensitivity,specificity,positive_predictive_value,negative_predictive_value,TruePositives,FalsePositives,TrueNegatives,FalseNegative"<<endl;
				for (vector< pair<TpFpTnFn,TpFpTnFn> >::iterator i_err = tree_samp_data.v_tp_fp_tn_fn.begin(); i_err != tree_samp_data.v_tp_fp_tn_fn.end();++i_err)
					fout_errdata<<i_err->first.N()<<","<<i_err->first.acc()<<","<<i_err->first.sens()<<","<<i_err->first.spec()<<","<<i_err->first.ppv()<<","<<i_err->first.npv()<<","<<i_err->first.tp<<","<<i_err->first.fp<<","<<i_err->first.tn<<","<<i_err->first.fn<<endl;
				fout_errdata.close();


				ofstream fout_errdata_th((outname+"_errordata_thresh.csv").c_str());
				fout_errdata<<"number_of_samples,accuracy,sensitivity,specificity,positive_predictive_value,negative_predictive_value,TruePositives,FalsePositives,TrueNegatives,FalseNegative"<<endl;
				for (vector<pair<TpFpTnFn,TpFpTnFn> >::iterator i_err = tree_samp_data.v_tp_fp_tn_fn_thresh.begin(); i_err != tree_samp_data.v_tp_fp_tn_fn_thresh.end();++i_err)
					fout_errdata_th<<i_err->first.N()<<","<<i_err->first.acc()<<","<<i_err->first.sens()<<","<<i_err->first.spec()<<","<<i_err->first.ppv()<<","<<i_err->first.npv()<<","<<i_err->first.tp<<","<<i_err->first.fp<<","<<i_err->first.tn<<","<<i_err->first.fn<<endl;
				fout_errdata_th.close();



			}
			//            {
			//per run accuracy
			//                for (list< pair<float,unsigned int> >::iterator i_acc = tree_samp_data.l_accuracy_pergroup.begin(); i_acc != tree_samp_data.l_accuracy_pergroup.end(); ++i_acc)
			//                    cerr<<"cv acc - "<<i_acc->first<<" "<<i_acc->second<<endl;


			//            }

			if (tree_samp_data.v_tp_fp_tn_fn.size()>1)//case for non-bagging
			{                
				vector<float> v_sx(10,0), v_sxx(10,0),v_std(10,0),v_mean(10,0);
				unsigned int N=0;
				for (vector<pair<TpFpTnFn,TpFpTnFn> >::iterator i_err = tree_samp_data.v_tp_fp_tn_fn.begin(); i_err != tree_samp_data.v_tp_fp_tn_fn.end();++i_err,++N)
				{
					v_sx[0]+=i_err->first.N();
					v_sx[1]+=i_err->first.acc();
					v_sx[2]+=i_err->first.sens();
					v_sx[3]+=i_err->first.spec();
					v_sx[4]+=i_err->first.ppv();
					v_sx[5]+=i_err->first.npv();
					v_sx[6]+=i_err->first.tp;
					v_sx[7]+=i_err->first.fp;
					v_sx[8]+=i_err->first.tn;
					v_sx[9]+=i_err->first.fn;
					v_sxx[0]+=i_err->first.N()*i_err->first.N();
					v_sxx[1]+=i_err->first.acc()*i_err->first.acc();
					v_sxx[2]+=i_err->first.sens()*i_err->first.sens();
					v_sxx[3]+=i_err->first.spec()*i_err->first.spec();
					v_sxx[4]+=i_err->first.ppv()*i_err->first.ppv();
					v_sxx[5]+=i_err->first.npv()*i_err->first.npv();
					v_sxx[6]+=i_err->first.tp*i_err->first.tp;
					v_sxx[7]+=i_err->first.fp*i_err->first.fp;
					v_sxx[8]+=i_err->first.tn*i_err->first.tn;
					v_sxx[9]+=i_err->first.fn*i_err->first.fn;





				}

				ofstream fconf((outname+"_errordata_ci.csv").c_str());
				fconf<<",number_of_samples,accuracy,sensitivity,specificity,positive_predictive_value,negative_predictive_value,TruePositives,FalsePositives,TrueNegatives,FalseNegative"<<endl;
				fconf<<"mean";
				for ( unsigned int i = 0 ; i < v_sx.size(); ++i)
				{
					v_mean[i]=v_sx[i]/N;
					fconf<<","<<v_sx[i]/N;
					v_std[i]= sqrtf((v_sxx[i]-v_sx[i]*v_sx[i]/N)/(N-1));
					//                    cerr<<"std "<<v_std[i]<<" "
				}
				fconf<<endl;
				if (N>1)
				{
					float t = MISCMATHS::Z2t::getInstance().convert(1.96,N-1);


					fconf<<"95%CI-LowerBound";
					for ( unsigned int i = 0 ; i < v_sx.size(); ++i)
						fconf<<","<<v_mean[i]-t*v_std[i]/sqrtf(N);
					fconf<<endl;
					fconf<<"95%CI-UpperBound";
					for ( unsigned int i = 0 ; i < v_sx.size(); ++i)
						fconf<<","<<v_mean[i]+t*v_std[i]/sqrtf(N);
					fconf<<endl;
				}
				fconf.close();
			}

		}

		cout<<"gettarget data"<<endl;
		vector<float> targ = DiscTree->getTargetData<float>();
		//print(targ,"taregt");
		//        cout<<"write predictions"<<endl;
		ofstream fpred((outname+"_predictions.csv").c_str());
		int count=0;
		//predictions is sample count,decision

		fpred<<"Truth,Predictions..."<<endl;
		for (vector< vector<int> >::iterator i = tree_samp_data.predictions.begin(); i!=tree_samp_data.predictions.end();++i,++count)                
		{
			fpred<<targ[count];
			for (vector<int>::iterator ii = i->begin(); ii != i->end(); ++ii)
				fpred<<","<<*ii;
			fpred<<endl;

		}





		fpred.close();

		//        DiscTree->outputPerSubjectStats( outname+"_per_subject_stats.csv");

		DiscTree->writeOutput();
	}
	cerr<<"delete tree sampler"<<endl;
	if (tree_samp != NULL)
		delete tree_samp;

	return 0;
}


int main( int argc, char * argv[])
{


	//Tracer tr("main");
	OptionParser options(title, examples);

	try {
		// must include all wanted options here (the order determines how
		//  the help message is printed)
		options.add(verbose);
		options.add(help);
		options.add(inname);
		options.add(outname);
		options.add(Nsub); 
		options.add(K);
		options.add(N);
		options.add(selection_mask);
		options.add(Npermutations);
		options.add(Npermutations_offset);
		options.add(sens_spec_class_inclusion);
		options.add(Moffset);
		options.add(B);
		options.add(bag);
		options.add(wBagByProb);
		options.add(AccThr);
		options.add(NAccThr);
		options.add(useSampleMask);
		options.add(bag_mid_thresh);
		options.add(loadSamples);
		options.add(loadPermutedTarget);
		options.add(bootstrap_opt);
		options.add(bag_threshMode);
		options.add(write_level);
		nonoptarg = options.parse_command_line(argc, argv);

		// line below stops the program if the help was requested or 
		//  a compulsory option was not set
		if (  (!options.check_compulsory_arguments(true) ))
		{
			options.usage();
			exit(EXIT_FAILURE);
		}
		unsigned Kin = K.value();

		if ((( N.value() ==0 ) && ( K.value() < -1 )) && (Nsub.value()==0) )
		{
			cerr<<"Didn't set either N or K."<<endl;
			cerr<<"N : "<<N.value()<<endl;
			cerr<<"K : "<<Nsub.value()<<endl;
			cerr<<"Nsub : "<<Nsub.value()<<endl;

			options.usage();
			exit(EXIT_FAILURE);
		}else if ( N.value() == 1 ){
			Kin=1;
		}//else if (K.value() == 0 ){
		//    cout<<"LeaveNout options only currently supports 1"<<endl;
		//    exit (EXIT_FAILURE);
		//  }
		//    cout<<"KN "<<K.value()<<" "<<N.value()<<endl;
		// Call the local functions
		//do_work(inname.value(),1,Nsub.value(),outname.value(),selection_mask.value(),1);
//        cout<<"Running workflow..."<<endl;
		do_work(verbose.value(), inname.value(),intestname.value(), Kin, Nsub.value(), outname.value(), sens_spec_class_inclusion.value(),selection_mask.value(),loadSamples.value(),loadPermutedTarget.value(),useSampleMask.value(), Npermutations.value(), Moffset.value(),B.value(),Npermutations_offset.value(), bag.value(),wBagByProb.value(),AccThr.value(),NAccThr.value(),bag_mid_thresh.value(),bootstrap_opt.value(),bag_threshMode.value(),write_level.value());


	}  catch(X_OptionError& e) {
		options.usage();
		cerr << endl << e.what() << endl;
		exit(EXIT_FAILURE);
	} catch(std::exception &e) {
		cerr << e.what() << endl;
		exit(EXIT_FAILURE);

	}

	return 0;// do_work(argc,argv);

}



