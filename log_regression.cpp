//
//  log_regression.cpp
//  su_mvdisc
//
//  Created by Natasha Parikh on 6/5/12.
//  Copyright (c) 2012 Stanford University - Etkin Lab. All rights reserved.
//
#include <algorithm>

#include "log_regression.h"
//#include <algorithm>
#include <cmath>
#include <iostream>

#include <misc_utils.h>
#include <clapack_utils.h>



#ifdef ARCHLINUX
extern "C" {
#include "cblas.h"
#include "f2c.h"
  
  typedef integer __CLPK_integer;
  typedef real __CLPK_real;
#include "clapack.h"
}

//#endif
//#ifdef ARCHDARWIN
#else
#include <Accelerate/Accelerate.h>

#endif



#include <tree_sampler.h>
#include <ClassificationTree.h>
//STL includes


using namespace std;
using namespace misc_utils_name;
using namespace clapack_utils_name;


namespace su_mvdisc_name
{
    
    log_regression_mvdisc::log_regression_mvdisc()
        : ZERO_THRESH(1e-04), fracVarThresh_(0.1), chiSquaredCV_(2.71), backwardStepwiseLR_(false), backwardStepwiseMaxAcc_(false), backwardStepwiseGenAcc_(false),
          forwardStepwiseLR_(false), forwardStepwiseMaxAcc_(false), forwardStepwiseGenAcc_(false), kFold_(1)
    {
       // cout << "log_regression_mvdisc : constructor" << endl;
        NodeName = "Logistic_Regression";
    }
	
	
	log_regression_mvdisc::~log_regression_mvdisc()
	{
        // Nothing to do here!  
    }

    void log_regression_mvdisc::setOptions(std::stringstream & ss_options)
	{
        // first reset to defaults
        setForwardStepWiseMaxAccOn(false);
        setBackStepWiseOn(false);
        
        map<string,string> options = parse_sstream(ss_options);
        for ( map<string,string>::iterator it = options.begin(); it != options.end(); it++) {
            if (it->first == "FracVariance") {
                stringstream ss; 
                ss << it->second;
                ss >> fracVarThresh_;
            } else if (it->first == "BackwardStepwiseLikelihoodRatio") {
                backwardStepwiseLR_ = true;
                stringstream ss; 
                ss << it->second;
                ss >> chiSquaredCV_;
            } else if (it->first == "BackwardStepwiseWithinAcc") {
                backwardStepwiseMaxAcc_ = true;
            } else if (it->first == "BackwardStepwiseGenAcc") {
                backwardStepwiseGenAcc_ = true;
                stringstream ss; 
                ss << it->second;
                ss >> kFold_;
            } else if (it->first == "ForwardStepwiseLikelihoodRatio") {
                forwardStepwiseLR_ = true;
                stringstream ss; 
                ss << it->second;
                ss >> chiSquaredCV_;
            } else if (it->first == "ForwardStepwiseWithinAcc") {
                forwardStepwiseMaxAcc_ = true;
            } else if (it->first == "ForwardStepwiseGenAcc") {
                forwardStepwiseGenAcc_ = true;
                stringstream ss; 
                ss << it->second;
                ss >> kFold_;
            } else {
                if (!setTreeNodeOptions(it->first, it->second))
                    throw ClassificationTreeNodeException((NodeName + " : Invalid input argument : " + it->first).c_str());
            }
        }
    }
    
    
    void log_regression_mvdisc::estimateParameters()
    {
        // create the observed class vector
        // all of these must be either 0 or 1 (formulas only work for Kclasses = 2)
        smatrix yMatrix(NumberOfSamples_, 1);
        unsigned int count = 0;
        
        for (vector<int>::iterator class_iter = training_target->class_index_begin(); 
             class_iter != training_target->class_index_end(); class_iter++, count++) {
            yMatrix.sdata[count] = abs(*class_iter - 1);    // in order to make 1->0 and 0->1
        }

//        smatrix_print(yMatrix, "ymatrix");
//        cout << endl << endl << endl << endl << endl << endl;
        
        // find the parameters for the null model
        smatrix data(NumberOfSamples_, 1);
        smatrix_set(data, 1);
        //smatrix_print(data, "data only one col");
        
        // estimate betaNull_, where data is just the intercepts
        logLikeliNull_ = newtonAlgorithm(data, yMatrix, betaNull_);
        
        if (Verbose) {
	  cout << "null log_likelihood: " << logLikeliNull_ << endl;
            //smatrix_print(betaNull_, "final null beta"); 
        }
        
        // copy training data to device, in column major format
        data.resize(NumberOfSamples_, Dimensionality_);                       // create the data matrix (aka X)
		
        //print(*(in_data[0]),"estpars indata");
        //cout<<"genacc "<<backwardStepwiseGenAcc_<<endl;
        copyToSingleVectorTranspose(in_data, data.sdata, NumberOfSamples_);
        
        if ((Dimensionality_ != 1) || (data.sdata[0] != 1))
            smatrix_insertColumn(data, 0, 1);                               // make each x_i start with 1 to account for the intercepts
        
    //    smatrix_print(data, "initial data, with added vector");
        
        //{//tetsing
        
	//cerr<<"initial data"<<endl;
	//smatrix_print(data, "initial data, with added vector");
	//	ofstream fdata("sampledata_for_spss.csv");
	//for (unsigned int i = 0 ; i <data.MRows;++i){
	// for (unsigned int j = 0; j <data.NCols;++j)
	//    fdata<<data.sdata[i + j * data.MRows]<<",";
	// fdata<<training_target->getOutTarget(i)<<endl;
	//}
        //for (unsigned int i = 0 ; i <data.MRows;++i){
	//   for (unsigned int j = 0; j <data.NCols;++j)
	//    fdata<<data.sdata[i + j * data.MRows]<<",";
	//  fdata<<-1<<endl;
        //}
        //fdata.close();
        //}

        // estimate the parameter beta_ from the full model 
        logLikelihood_ = newtonAlgorithm(data, yMatrix, beta_);
        
        smatrix nullModel(NumberOfSamples_, 1); 
        smatrix_set(nullModel, 1);
        
        if (backwardStepwiseGenAcc_) {
            vector<int> colsDeleted;
            //cout << "about to do backwardStepping_genAcc" << endl;
            
            estParamsBackwardStep_maxAcc(data, yMatrix, colsDeleted, findGenAcc(nullModel, kFold_), 1);
        } 
        if (backwardStepwiseMaxAcc_) {
            vector<int> colsDeleted;
            //cout << "about to do backwardStepping_maxAcc" << endl;
            
            estParamsBackwardStep_maxAcc(data, yMatrix, colsDeleted, findTrainingAccuracy(nullModel, beta_), 0);
        } 
        if (backwardStepwiseLR_) {
            vector<int> colsDeleted;
            estParamsBackwardStepLR(data, yMatrix, colsDeleted);            
        }        
        if (forwardStepwiseGenAcc_) {
            vector<int> colsAdded;
            //cout << "about to do forwardStepping_genAcc" << endl;
            
            estParamsForwardStep_maxAcc(nullModel, data, yMatrix, colsAdded, findGenAcc(nullModel, kFold_), 1);
        } 
        if (forwardStepwiseMaxAcc_) {
            vector<int> colsAdded;
            //cout << "about to do forwardStepping_maxAcc" << endl;
            
            //float oldAcc = findTrainingAccuracy(nullModel, beta_);
            estParamsForwardStep_maxAcc(nullModel, data, yMatrix, colsAdded, findTrainingAccuracy(nullModel, beta_), 0);
            
           // smatrix_print(beta_, "beta after maxAcc: ");
        } 
        if (forwardStepwiseLR_) {
            vector<int> colsAdded, colsAdded2;
            smatrix  nullModel2; 
            nullModel2 = nullModel;
            
            //cout << "about to do forwardStepping_LR" << endl;
            estParamsForwardStepLR(nullModel, data, yMatrix, colsAdded, logLikeliNull_);            
            estParamsForwardStepScoreStat(nullModel2, data, yMatrix, colsAdded2, betaNull_);
        }
        
        //smatrix_print(beta_, "final beta"); 
                
        if (Verbose) {
            cout << "final log_likelihood: " << logLikelihood_ << endl;
            smatrix_print(beta_, "final beta"); 
            cout << "Cox & Snell: " << coxAndSnell() << endl;
            cout << "Nagelkerke: " << nagelkerke() << endl;
        }
        //cout<<"end est LR "<<endl;
    }
    
    
    void log_regression_mvdisc::estParamsBackwardStep_maxAcc(smatrix& data, const smatrix& yMatrix, vector<int>& colsDeleted, float oldAcc, bool useGenAcc)
    {
        //cerr << "top of function, data.Ncols: " << data.NCols << endl;
        if (data.NCols == 1) {
            //cerr << "base case, data.Ncols: " << data.NCols << endl;
            throw ClassificationTreeNodeException("Removed all variables except the intercepts. Consider changing p threshold.");
        } else {    
            //cerr << "else case, data.Ncols: " << data.NCols << endl;
            //cerr << "Old Accuracy: " << oldAcc << endl;
            
            // next, find the reduced model with the greatest accuracy
            // go through all eligible variables, delete them one at a time
            float tempLikelihood, bestAccuracy(0.0f), bestLikelihood, acc;
            smatrix copy, beta, bestBeta, bestData, tempBeta;
            __CLPK_integer bestCol;
            for (__CLPK_integer i = 1; i < data.NCols; ++i) {
                copy = data;
                smatrix_deleteColumn(copy, i);

                //smatrix_print(copy, "data with col deleted");
                
                // adjust the col index so that it reflects the col deleted from the original data matrix
                __CLPK_integer row = i; // copy so that it does not mess up the iterations
                for (int j = 0; j <= row; ++j) {
                    if (find(colsDeleted.begin(), colsDeleted.end(), j) != colsDeleted.end()) 
                        ++row;
                }
                //cout << "row: " << row << endl;
                
                tempLikelihood = newtonAlgorithm(copy, yMatrix, beta);
                
                if (useGenAcc) {
                    // find the generalization accuracy for the reduced model
                    acc = findGenAcc(copy, kFold_);
                } else {
                    // find the training accuracy for the reduced model
                    acc = findTrainingAccuracy(copy, beta);
                }
                
                tempBeta.resize(Dimensionality_ + 1, 1);
                smatrix_set(tempBeta, 0);
                
                // adjust beta so that it has zeroes in the deleted rows
                int count = 0;
                for (unsigned int pos = 0; pos < Dimensionality_ + 1; ++pos) {
                    if ((find(colsDeleted.begin(), colsDeleted.end(), pos) == colsDeleted.end()) && (pos != static_cast<unsigned int>(row))) {
                        smatrix_setRow(tempBeta, pos, beta, count);
                        ++count;
                    }
                }
                
                // find the best accuracy of the remaining features
                if (acc >= bestAccuracy) {
                    bestAccuracy = acc;
                    bestBeta = tempBeta;
                    bestLikelihood = tempLikelihood;
                    bestData = copy;
                    bestCol = row;
                }
            }
            
            //cerr << "Old Accuracy: " << oldAcc << endl;
            //cerr << "New Accuracy: " << bestAccuracy << endl;
            
            // check if the best accuracy found by decreasing by one feature increases the
            // accuracy when compared to the old model. If so, delete the feature and recurse.
            if (bestAccuracy >= oldAcc) {                 
                colsDeleted.push_back(bestCol);                 // keep track of which columns are deleted
                beta_ = bestBeta;
                logLikelihood_ = bestLikelihood;
                //cerr << "before recursion, bestData Ncols: " << bestData.NCols << endl;
                estParamsBackwardStep_maxAcc(bestData, yMatrix, colsDeleted, bestAccuracy, useGenAcc);
            } else {                                            // if accuracy decreases, end the recursion
                cerr << "number of cols deleted: " << Dimensionality_ + 1 - data.NCols << endl;
                cout << "Max accuracy : useGen: " << useGenAcc << ", accuracy: " << oldAcc << endl;
                //smatrix_print(data, "final data matrix");                
                return; 
            }
        }
    }
    
    
    void log_regression_mvdisc::estParamsBackwardStepLR(smatrix& data, const smatrix& yMatrix, vector<int>& colsDeleted)
    {
        if (data.NCols == 1) {
            throw ClassificationTreeNodeException("Removed all variables except the intercepts. Consider changing p threshold.");
        } else {
            
            // find the likelihood ratio of each reduced model to the full model
            float tempLikelihood, LR, leastSig(1e12), bestLikelihood, bestCol;
            smatrix copy, beta, bestBeta, bestData; 
            for (int i = 1; i < data.NCols; ++i) {
                copy = data;
                smatrix_deleteColumn(copy, i);
                tempLikelihood = newtonAlgorithm(copy, yMatrix, beta);
                
                //cout << "last likelihood: " << logLikelihood_ << endl;
                //cout << "temp likelihood: " << tempLikelihood << endl;
                
                LR = likelihoodRatio(tempLikelihood, logLikelihood_);
                //cout << "LR: " << LR << endl;
                
                // the reduced model gives a lower likelihood than the full model. 
                // thus, extract information for the model that differs the least from the full model
                if (LR <= leastSig) {
                    leastSig = LR;
                    bestBeta = beta;
                    bestLikelihood = tempLikelihood;
                    bestData = copy;
                    bestCol = i;
                }
            }
            
            // check if significance is low enough here! then, either recurse or end
            // need critical value for x^2 test here! also, choose p threshold!!!
            if (leastSig <= chiSquaredCV_) {    
                
                // edit bestCol so it reflects the col from the original data matrix
                for (int j = 0; j <= bestCol; ++j) {
                    if (find(colsDeleted.begin(), colsDeleted.end(), j) != colsDeleted.end()) 
                        ++bestCol;
                }
                
                colsDeleted.push_back(bestCol); // then, store it
                
                // add zeroes into the appropriate rows for beta_
                beta_.resize(Dimensionality_ + 1, 1);
                smatrix_set(beta_, 0);
                int count = 0;
                for (unsigned int pos = 0; pos < Dimensionality_ + 1; ++pos) {
                    if (find(colsDeleted.begin(), colsDeleted.end(), pos) == colsDeleted.end()) {
                        smatrix_setRow(beta_, pos, bestBeta, count);
                        ++count;
                    }
                }
                
                logLikelihood_ = bestLikelihood;
                estParamsBackwardStepLR(bestData, yMatrix, colsDeleted);
            } else {
                cerr << "number of cols deleted, LR: " << Dimensionality_ + 1 - data.NCols << endl;
                //smatrix_print(data, "final data matrix");                
                return; 
            }
        }
    }
    
    
    void log_regression_mvdisc::estParamsForwardStep_maxAcc(smatrix& currData, const smatrix& fullData, const smatrix& yMatrix, 
                                                            std::vector<int>& colsAdded, float oldAcc, bool useGenAcc)
    {
        if (currData.NCols == fullData.NCols) {
            
            if (Verbose) {
                cerr << "All cols added into the model." << endl;
                cout << "Max accuracy : useGen: " << useGenAcc << ", accuracy: " << oldAcc << endl;
                //smatrix_print(currData, "final data matrix"); 
            }
                
            return;
        } else {                
            // next, find the increased model with the greatest accuracy
            // go through all eligible variables, add them one at a time
            float tempLikelihood, bestAccuracy(0.0f), bestLikelihood, acc;
            smatrix copy, beta, bestBeta, bestData, tempBeta;
            __CLPK_integer bestCol, insertCol(1);
            for (__CLPK_integer i = 1; i < fullData.NCols; ++i) {
            
                if (find(colsAdded.begin(), colsAdded.end(), i) != colsAdded.end()) {
                    ++insertCol;
                    continue;
                }
                
                // copy the current data, and insert the appropriate column of data
                copy = currData;
                smatrix_insertColumn(copy, insertCol, 0);
                smatrix_setCol(copy, insertCol, fullData, i);
                
                //smatrix_print(copy, "data with col added");
                
                tempLikelihood = newtonAlgorithm(copy, yMatrix, beta);
                
                if (useGenAcc) {
                    // find the generalization accuracy for the reduced model
                    acc = findGenAcc(copy, kFold_);
                } else {
                    // find the training accuracy for the reduced model
                    acc = findTrainingAccuracy(copy, beta);
                }
                
                tempBeta.resize(Dimensionality_ + 1, 1);
                smatrix_set(tempBeta, 0);
                
                tempBeta.sdata[0] = beta.sdata[0];
                
                // adjust beta so that it has zeroes in the deleted rows
                int count = 1;
                for (unsigned int pos = 1; pos < Dimensionality_ + 1; ++pos) {
                    if ((find(colsAdded.begin(), colsAdded.end(), pos) != colsAdded.end()) || (pos == static_cast<unsigned int>(i))) {
                        smatrix_setRow(tempBeta, pos, beta, count);
                        ++count;
                    }
                }

                // find the best accuracy of the remaining features
                if (acc > bestAccuracy) {
                    bestAccuracy = acc;
                    bestBeta = tempBeta;
                    bestLikelihood = tempLikelihood;
                    bestData = copy;
                    bestCol = i;
                }
            }
            
         //   cerr << "Old Accuracy: " << oldAcc << endl;
        //    cerr << "New Accuracy: " << bestAccuracy << endl;
            
            // check if the best accuracy found by decreasing by one feature increases the
            // accuracy when compared to the old model. If so, delete the feature and recurse.
            if (bestAccuracy > oldAcc) {                 
                colsAdded.push_back(bestCol);                 // keep track of which columns are deleted
                beta_ = bestBeta;
                logLikelihood_ = bestLikelihood;
                //cerr << "before recursion, bestData Ncols: " << bestData.NCols << endl;
                estParamsForwardStep_maxAcc(bestData, fullData, yMatrix, colsAdded, bestAccuracy, useGenAcc);
            } else {                                            // if accuracy decreases, end the recursion
                
                if (Verbose) {
                    cerr << "Number of cols in the final model: " << currData.NCols << endl;
                    cout << "Max accuracy : useGen: " << useGenAcc << ", accuracy: " << oldAcc << endl;
                    //smatrix_print(data, "final data matrix");                
                }
                
                return; 
            }
        }
    }
    
    
    void log_regression_mvdisc::estParamsForwardStepLR(smatrix& currData, const smatrix& fullData, const smatrix& yMatrix, std::vector<int>& colsAdded, float oldLikelihood)
    {
        if (currData.NCols == fullData.NCols) {
            cerr << "All columns added into the model, forward LR." << endl;
            //smatrix_print(data, "final data matrix");
            return;
        } else {
            
            // find the likelihood ratio of each increased model compared to the last model
            float tempLikelihood, LR, mostSig(1e-12), bestLikelihood, bestCol;
            smatrix copy, beta, bestBeta, bestData; 
            int insertCol(1);
            for (int i = 1; i < fullData.NCols; ++i) {
            
                if (find(colsAdded.begin(), colsAdded.end(), i) != colsAdded.end()) {
                    ++insertCol;
                    continue;
                }
                
                // copy the current data, and insert the appropriate column of data
                copy = currData;
                smatrix_insertColumn(copy, insertCol, 0);
                smatrix_setCol(copy, insertCol, fullData, i);
                
                //smatrix_print(copy, "data with col added");
                
                tempLikelihood = newtonAlgorithm(copy, yMatrix, beta);
                
                LR = likelihoodRatio(oldLikelihood, tempLikelihood);
                
          //      cout << "last likelihood: " << oldLikelihood << endl;
            //    cout << "temp likelihood: " << tempLikelihood << endl;
                
              //  cout << "LR: " << LR << endl;
                
                // the reduced model gives a lower likelihood than the full model. 
                // thus, extract information for the model that differs the least from the full model
                if (LR > mostSig) {
                    mostSig = LR;
                    bestBeta = beta;
                    bestLikelihood = tempLikelihood;
                    bestData = copy;
                    bestCol = i;
                }
            }
            
            // check if significance is low enough here! then, either recurse or end
            // need critical value for x^2 test here! also, choose p threshold!!!
            if (mostSig > chiSquaredCV_) {    
                
                colsAdded.push_back(bestCol); // then, store it
                
              //  print(colsAdded, "colsAdded");
                
                // add zeroes into the appropriate rows for beta_
                beta_.resize(Dimensionality_ + 1, 1);
                smatrix_set(beta_, 0);
                beta_.sdata[0] = bestBeta.sdata[0];
                int count = 1;
                for (unsigned int pos = 1; pos < Dimensionality_ + 1; ++pos) {
                    if (find(colsAdded.begin(), colsAdded.end(), pos) != colsAdded.end()) {
                        smatrix_setRow(beta_, pos, bestBeta, count);
                        ++count;
                    }
                }
                
                logLikelihood_ = bestLikelihood;
                estParamsForwardStepLR(bestData, fullData, yMatrix, colsAdded, logLikelihood_);
            } else {
                cerr << "Number of cols added, forward LR: " << currData.NCols - 1 << endl;
                //smatrix_print(data, "final data matrix");                
                return; 
            } 
        }
    }
    
    
    void log_regression_mvdisc::estParamsForwardStepScoreStat(smatrix& currData, const smatrix& fullData, const smatrix& yMatrix, 
                                                              std::vector<int>& colsAdded, const smatrix& oldBeta)
    {
        if (currData.NCols == fullData.NCols) {
            cerr << "All columns added into the model, forward ScoreStats." << endl;
            //smatrix_print(data, "final data matrix");
            return;
        } else {
            
            // find the likelihood ratio of each increased model compared to the last model
            float tempLikelihood, score, mostSig(1e12), bestCol;
            smatrix copy, bestBeta, bestData, beta; 
            int insertCol(1);
            for (int i = 1; i < fullData.NCols; ++i) {
                
                if (find(colsAdded.begin(), colsAdded.end(), i) != colsAdded.end()) {
                    ++insertCol;
                    continue;
                }
                
                // copy the current data, and insert the appropriate column of data
                copy = currData;
                smatrix_insertColumn(copy, insertCol, 0);
                smatrix_setCol(copy, insertCol, fullData, i);
                
                //smatrix_print(copy, "data with col added");
                beta = oldBeta;
                
                smatrix_transpose1Dim(beta);
                smatrix_insertColumn(beta, insertCol, 0);
                smatrix_transpose1Dim(beta);
                
                //cout << "finding score " << endl;
                
                score = findScoreStatistic(currData, beta, yMatrix);
                
                //cout << "last score: " << oldScore << endl;
//                cout << "temp score: " << score << endl;
                
                // the reduced model gives a lower likelihood than the full model. 
                // thus, extract information for the model that differs the least from the full model
                if (score < mostSig) {
                    mostSig = score;
                    bestData = copy;
                    bestCol = i;
                }
            }
            
            // check if significance is low enough here! then, either recurse or end
            // need critical value for x^2 test here! also, choose p threshold!!!
            if (mostSig < chiSquaredCV_) {   
                
                tempLikelihood = newtonAlgorithm(bestData, yMatrix, bestBeta);
                colsAdded.push_back(bestCol); // then, store it
                
//                print(colsAdded, "colsAdded");
                
                // add zeroes into the appropriate rows for beta_
                beta_.resize(Dimensionality_ + 1, 1);
                smatrix_set(beta_, 0);
                beta_.sdata[0] = bestBeta.sdata[0];
                int count = 1;
                for (unsigned int pos = 1; pos < Dimensionality_ + 1; ++pos) {
                    if (find(colsAdded.begin(), colsAdded.end(), pos) != colsAdded.end()) {
                        smatrix_setRow(beta_, pos, bestBeta, count);
                        ++count;
                    }
                }
                
                logLikelihood_ = tempLikelihood;
                estParamsForwardStepScoreStat(bestData, fullData, yMatrix, colsAdded, bestBeta);
            } else {
                cerr << "Number of cols added, forward score stat: " << currData.NCols - 1 << endl;
                //smatrix_print(data, "final data matrix");                
                return; 
            } 
        }
    }
    
    
    void log_regression_mvdisc::setParameters()
    {
        // Nothing to do here!
    }
    
    
    int log_regression_mvdisc::classifyTrainingData()
    {
     //   cout<<"LR classify Training "<<endl;
        if (in_data.empty())
            throw ClassificationTreeNodeException("Trying to classify using Logistic Regression, but no data to classify exists.");
        
        smatrix target(NumberOfSamples_, Dimensionality_); // target to classify
        
        // load test data into target matrix 
        //print(*(in_data[0]),"indata");
        copyToSingleVectorTranspose(in_data, target.sdata, NumberOfSamples_);
        smatrix_insertColumn(target, 0, 1);
        
        // class labels
        vector<int> cl_labels = training_target->getClassLabels();
        pair<unsigned int,float> base;
        vector< pair<unsigned int,float> > v_prob(target.MRows, base);
        
        //cerr << "retrieved class labels" << endl;
        
        //smatrix_print(target, "target");
        smatrix_AtxBt(beta_, target);
        //smatrix_print(target, "beta x targetRow");
        
        for (__CLPK_integer count = 0; count < target.NCols; ++count) {

            float prob = exp(target.sdata[count]) / (1 + exp(target.sdata[count]));
            
          //  cout << "training prob: " << count << " "<< prob << endl;
            
            if (prob > 0.5) {
                v_prob[count].first = 0;
                v_prob[count].second = prob;
            } else {
                v_prob[count].first = 1;
                v_prob[count].second = 1 - prob;
            }
        }
                
        float train_err=0;
        vector<int>::iterator class_iter = training_target->class_index_begin();
        for (vector< pair<unsigned int, float> >::iterator i_prob = v_prob.begin(); i_prob != v_prob.end(); ++i_prob, ++class_iter) {
            out_data->push_back(static_cast<float> (cl_labels[i_prob->first]));
            train_err += (cl_labels[i_prob->first] == *class_iter) ? 1 : 0 ; 
        }
        
//        cout<<"class "<<endl;
//        for (vector<int>::iterator class_iter = training_target->class_index_begin(); class_iter != training_target->class_index_end(); class_iter++)
//            cout << *class_iter << " ";    
//        cout << endl;
        
        train_err /= NumberOfSamples_;
       //cout << "training accuracy " << train_err << endl;
         
//        print(*out_data, "classify training");
        
        //cout << "Logistic Regression : classify test end" << endl;
         
        return 0;
    }
    

    int log_regression_mvdisc::classifyTestData()
    {
        if (Verbose)
            cerr << "log_regression: classifyTestData: just starting" << endl;
        
        //smatrix_print(beta_, "beta that is classifying test data");
        
        if (in_test_data.empty())
            throw ClassificationTreeNodeException("Trying to classify using Logistic Regression, but no test data to classify exists.");
        
        smatrix target(NumberOfTestSamples_, Dimensionality_); // target to classify
        
        // load test data into target matrix 
        copyToSingleVectorTranspose(in_test_data, target.sdata, NumberOfTestSamples_);
        
        //cerr<<"done copy"<<endl;
        smatrix_insertColumn(target, 0, 1);
        //cerr<<"done append"<<endl;
        
        //smatrix_print(target, "test data");
        
        //class labels
        vector<int> cl_labels = training_target->getClassLabels();
        pair<unsigned int,float> base;
        vector< pair<unsigned int,float> > v_prob(NumberOfTestSamples_,base);
        
        
        smatrix_AtxBt(beta_, target);
        
        //smatrix_print(target, "beta x test data");
        
        for (unsigned int count = 0; count < NumberOfTestSamples_; ++count) {
            
            float prob = exp(target.sdata[count]) / (1 + exp(target.sdata[count]));
            
            if (Verbose) 
                cerr << "probabilities: " << prob << endl;
            
            if (prob > 0.5) {
                v_prob[count].first = 0;
                v_prob[count].second = prob;
            } else {
                v_prob[count].first = 1;
                v_prob[count].second = 1 - prob;
            }
        }
        
        for (vector< pair<unsigned int, float> >::iterator i_prob = v_prob.begin(); i_prob != v_prob.end(); ++i_prob)
            out_test_data->push_back(static_cast<float> (cl_labels[i_prob->first])); 
        
//        print(*out_test_data, "classify test");
        //exit (EXIT_FAILURE);
        //cout << "Logistic Regression : classify test end" << endl;
        return 0;
    }
    
    
    float log_regression_mvdisc::coxAndSnell() const
    {
        // must take e^(log_likelihood) to get the simple likelihood
        return 1 - pow(exp(logLikeliNull_ - logLikelihood_), 2.0f / NumberOfSamples_);
    }
    
    
    float log_regression_mvdisc::nagelkerke() const
    {
        float numerator = coxAndSnell();
        return numerator / (1 - exp(logLikeliNull_ * 2.0f / NumberOfSamples_));
    }
    
    
    float log_regression_mvdisc::newtonAlgorithm(const smatrix& data, const smatrix& yMatrix, smatrix& beta) 
    {
      //  cout<<"start Newton"<<endl;
        
        // initialize the newBeta to use as a comparison
        smatrix newBeta(data.NCols, 1);
        smatrix_set(newBeta, 0);
        
        // create/set variables needed for iterations
        beta.resize(data.NCols, 1);
        smatrix_set(beta, 1);  // set to 1 so it's significantly different than newBeta
        smatrix probMatrix(data.MRows, 1), weightMatrix(data.MRows, 1), D, y_p(data.MRows, 1), XxBeta, dataRow, 
                Beta_X, Winv, z, XTxW, XtwX_inv, Winvxy_p, zeroes(data.MRows, 1);
        float min_prev(-1000.0f), stepsize(1), min_new;
        smatrix_set(zeroes, 0);
        smatrix_set(y_p, 1);
         
        while ((beta != newBeta) && (y_p != zeroes)) {
  
            
            beta = newBeta;
            
            // find the new probMatrix_
            // min new is log-likelihood which must be negative
            min_new = 0.0f;
            
            dataRow = data;
            
            //smatrix_print(beta, "beta");
            //smatrix_print(dataRow, "data");
            
            smatrix_AtxBt(beta, dataRow);   // get the matrix that is beta transpose times the row of data transpose, save it in dataRow
            
            //smatrix_print(dataRow, "beta' * dataRow'");
            
            for (__CLPK_integer count = 0; count < data.MRows; ++count) {
                
                float expDif = exp(dataRow.sdata[count]);
                
                //cout << "expDif: " << expDif << endl;
            
                probMatrix.sdata[count] = expDif / (1 + expDif);
                min_new += yMatrix.sdata[count] * dataRow.sdata[count] - logf(1 + expDif);
            }
            
            //smatrix_print(probMatrix, "probabilities");
            
            // reduce the step size if log likelihood is not increasing to ensure convergence
            if (min_new < min_prev) {
                stepsize /= 2.0f;
                //return min_prev;
            }
            
            min_prev = min_new;
//            cout << " log likelihood: " << min_prev << endl;
            
            // the new weights are calculated using each probability times its conjugate
            // weight matrix should be a diagonal NxN matrix, but it is being stored as a Nx1 matrix
            for (__CLPK_integer i = 0; i < weightMatrix.MRows; ++i)
                weightMatrix.sdata[i] = probMatrix.sdata[i] * (1 - probMatrix.sdata[i]); // + fracVarThresh_;


            //smatrix_print(weightMatrix, "W");
            
            // find W ^ (-1), with a threshold value
            Winv = weightMatrix;
            for (__CLPK_integer i = 0 ; i < Winv.MRows; ++i) 
                //Winv.sdata[i] = (weightMatrix.sdata[i] > ZERO_THRESH) ? 1.0f / weightMatrix.sdata[i] : 0; 
                Winv.sdata[i] = 1.0f / weightMatrix.sdata[i];
            
            //smatrix_print(Winv, "winv");
            
            // calculate the new beta in a series of steps
            // end result is the equation: betaNew = (X' * W * X) ^ (-1) * X' * W * (X * beta + W ^ (-1) * (y - p))
            // notes: the expression (X * beta + W ^ (-1) * (y - p)) is denoted as z
            //        (XT * W * X) ^ (-1) is computed by finding the appropriate SVD and computing U * (D * W) ^ (-1) * U'
            
            
            // the first half of the equation  
            smatrix_AtxDiagD(data, weightMatrix, XTxW);                     // X' * W  
            
//            smatrix_print(XTxW, "X'*W");
//            cout << endl;
//            cout << endl;
//            cout << endl;
//            cout << endl;
//            cout << endl;
            
            //cout << "rows: " << data.MRows << ", cols: " << data.NCols << endl;
            
            // because we are taking the pseudo inverse, there are different conditions for if N > M and vice versa
             smatrix U;
         //   smatrix_set(U, 0 );
            if (data.MRows >= data.NCols) {

                smatrix_AxB(XTxW, data, U);                                 // X' * W * X
//                smatrix_print(U, "XWX");
//                cout << endl;
//                cout << endl;
//                cout << endl;
//                cout << endl;
//                cout << endl;
                
                //z = U;
                
                smatrix_svd(U, D);                                          // SVD(X' * W * X) gives U, D * W
                /*smatrix_print(U, "U");
                cout << endl;
                cout << endl;
                cout << endl;
                cout << endl;
                cout << endl;
                
                smatrix_print(D, "D");
                cout << endl;
                cout << endl;
                cout << endl;
                cout << endl;
                cout << endl;*/
                
                
                // find (D * W) ^ (-1), with a threshold value
                //   unsigned int count = D.MRows;
                for (__CLPK_integer row = 0; row < D.MRows; ++row) {
                    //D.sdata[row] += 0.01;
                    D.sdata[row] = (D.sdata[row] > ZERO_THRESH) ? 1.0f / D.sdata[row] : 0;
                    //(D.sdata[row] > ZERO_THRESH) ? ++count : 0;
                    //D.sdata[row] = 1.0f / D.sdata[row];
                }
                
                //smatrix_print(D, "inverted singular values");
                
                smatrix_AxDiagD(U, D);                                      // U * (D * W) ^ (-1)
                
                //smatrix_print(D, "U*D");
                
                smatrix_BxAt(D, U);                                         // U * (D * W) ^ (-1) * U' = (X' * W * X) ^ (-1)
            
//                smatrix_print(D, "(X' * W * X) ^ (-1)");
//                cout << endl;
//                cout << endl;
//                cout << endl;
//                cout << endl;
//                cout << endl;
                
            } else {
                cerr << "in else case" << endl;
                
                U = data;
                smatrix_svd_of_transpose(U, D);                             // SVD(X') gives U', D ^ 0.5

                float total_var = cblas_sdot(NumberOfSamples_, D.sdata, 1, D.sdata, 1);
                
                if (Verbose)
                    cout << "Total Variance " << total_var << endl;
                
                //float var_thresh = (total_var * fracVarThresh_);
                
                //cout << "variance threshold: " << var_thresh << endl; 
                
                // find D ^ (-1), with a threshold value
                for (__CLPK_integer row = 0; row < D.MRows; ++row) {
                    //D.sdata[row] += fracVarThresh_*sqrt(total_var)*0.1;
                    D.sdata[row] = (D.sdata[row] > ZERO_THRESH) ? ( 1.0f / (pow(D.sdata[row], 2) ) * Winv.sdata[row] ) : 0; // apply weight too
                }                                                                                                           // (D * W) ^ (-1)
                
                //smatrix_print(D, "inverted singular values");
                
                smatrix_AtxDiagD(U, D, XtwX_inv);                           // U * (D * W) ^ (-1)

               // smatrix_print(XtwX_inv, "U*D*W -1");
              //  cout << endl;
               // cout << endl;
                //cout << endl;
                //cout << endl;
                //cout << endl;
                
                smatrix_AxB(XtwX_inv, U, D);                                // U * (D * W) ^ (-1) * U' = (X' * W * X) ^ (-1)
                
                //smatrix_print(D, "(X' * W * X) ^ (-1)");
                //cout << endl<<;
                //cout << endl;
                //cout << endl;
                //cout << endl;
               // cout << endl;
            } 
            
            
            smatrix_AxB(D, XTxW);                                           // (X' * W * X) ^ (-1) * X' * W

            //smatrix_print(XTxW, "(X'WX)^-1 * X'W");
            
            smatrix Identity;
            smatrix_AxB(XTxW, data, Identity);                              // (X' * W * X) ^ (-1) * X' * W * X ----FOR DEBUGGGING
            //cout << "Identity[1,1]: " << Identity.sdata[0] << endl;
            
            
            
            // second half, calculating z
            smatrix_scaleAndAddMatrices(probMatrix, -1, yMatrix, y_p);      // y - p
            
            smatrix_scale(y_p, stepsize);                                   // stepsize * (y - p), to ensure convergence

            
            smatrix_DiagDxA(Winv, y_p, Winvxy_p);                           // W ^ (-1) * (y - p)
            
            smatrix_AxB(data, beta, XxBeta);                                // X * beta

            smatrix_scaleAndAddMatrices(XxBeta, 1.0, Winvxy_p, z);          // z = X * beta + W ^ (-1) * (y - p)
            smatrix_AxB(XTxW, z, newBeta);                                  // combine to create newBeta


        }
        
        //beta = newBeta;
        
        //smatrix_print(beta, "beta_ at end of newtonAlg");
        
        if (Verbose)
            cout << "stepsize: " << stepsize << endl;
        
      //  cerr<<"End Newton"<<endl;
        return min_new;
    }    
    
    
    float log_regression_mvdisc::findTrainingAccuracy(const smatrix& data, const smatrix& beta)
    {
        // class labels
        vector<int> cl_labels = training_target->getClassLabels();
        pair<unsigned int,float> base;
        vector< pair<unsigned int,float> > v_prob(data.MRows, base);
        
        //cerr << "retrieved class labels" << endl;
        
        smatrix targetRow;
        targetRow = data;
        smatrix_AtxBt(beta, targetRow);
        
        for (__CLPK_integer count = 0; count < data.MRows; ++count) {
            
            // extract each row of data from the data
            //targetRow.resize(1, data.NCols);
            //for (unsigned int pos = 0; pos < data.NCols; ++pos)
            //    targetRow.sdata[pos] = data.sdata[data.MRows * pos + count];
            
            //smatrix_print(targetRow, "row of data");
            
            //smatrix_AtxBt(beta, targetRow);
            float prob = exp(targetRow.sdata[count]) / (1 + exp(targetRow.sdata[count]));
            
            if (prob > 0.5) {
                v_prob[count].first = 0;
                v_prob[count].second = prob;
            } else {
                v_prob[count].first = 1;
                v_prob[count].second = 1 - prob;
            }
        }
        
        //cerr << "out of the for loop" << endl;
        
        float train_err=0;
        vector<int>::iterator class_iter = training_target->class_index_begin();
        for (vector< pair<unsigned int, float> >::iterator i_prob = v_prob.begin(); i_prob != v_prob.end(); ++i_prob, ++class_iter) {
            train_err += (cl_labels[i_prob->first] == *class_iter) ? 1 : 0 ; 
        }
        
        train_err /= NumberOfSamples_;
        //cout << "findTrainingAccuracy: training accuracy " << train_err << endl;
        return train_err;
    }
    
    
    float log_regression_mvdisc::findGenAcc(const smatrix& data_in, const unsigned int& Kin)
    {
        //always deleting first column, it's leading ones, which will be added on again later
        
       // cerr << "findGenAcc start" << endl;
        //smatrix_print(data_in, "find genAcc in ");
        
        smatrix data;
        if (data_in.NCols == 1) {
            data = data_in;
        } else {
            data = sub_matrix(data_in, 0, data_in.MRows, 1, data_in.NCols-1);
        }

        Target* targ = new Target();
        targ->setTargetData(training_target->getTargetData<float>());
        
        //cerr << "created target, set data " <<training_target->getTargetData<float>().size()<< endl;
        
        DataSource* d_src = new DataSource();
        
        //cerr << "created data source" << endl;
        
        d_src->setTarget(targ);
        
        //cerr << "set target of data source" << endl;
        
        d_src->addInputData(data);
        
        //cerr << "added data to data source" << endl;
        
        log_regression_mvdisc* lr_inter = new log_regression_mvdisc();
        lr_inter->setBackStepWiseOn(false);
        lr_inter->addInput(d_src);
        
        //cerr << "created logReg object, added input" << endl;
        
        vector<int> targ_true = targ->getTargetData<int>();
        vector<float> output;
        vector<int> sel_test(targ->getNumberOfSamples(), 1);
        
        
        /****************************************** K-FOLD STUFF ***********************************************/
        
        //check_Ns_Targ_eq_Ns_Src();
        //cout<<"Enter run Kfold "<<sum<int>(mask)<<endl;
        
        unsigned int NumberOfSamples = d_src->getNumberOfSamples();
        
        /*
         unsigned int NumberOfSamples_masked = 0;
         for (vector<int>::const_iterator i = mask.begin(); i!=mask.end();i++)
         {
         //cout<<"mask "<<*i<<endl;
         if (*i > 0 ) NumberOfSamples_masked++;
         } */
        
        // If K=1 run leave-one-out
        //cout<<"Kin1 "<<Kin <<" "<<NumberOfSamples_masked<<" "<<K<<endl;
        
        unsigned int K =  ( Kin == 1 ) ? NumberOfSamples : Kin;
        
        //cout << "Kin " << Kin << " " << NumberOfSamples << " " << K << endl;
        if (Verbose)
            cout << "Running " << K << "-fold cross-validation. Number of samples " << NumberOfSamples << endl;
        
        if (K > NumberOfSamples)
            throw ClassificationTreeNodeException("Invalid choice of K in K-fold cross-validation. It exceeds number of training samples");
        
        unsigned int N_per_class = NumberOfSamples / K;
        unsigned int left_over = NumberOfSamples - N_per_class * K;
        
        srand ( time(NULL) );
        //srand ( seed );
        //seed+=rand();
        vector< vector< unsigned int > > group_samples;
        
        
        list< unsigned int > available_samples;
        for ( unsigned int i = 0; i < NumberOfSamples; i++ )
            available_samples.push_back(i);
        
        // print<unsigned int>(available_samples,"available_samples");
        
        unsigned int N_available = available_samples.size();
        //cout<<"Number of availabel sampels "<<N_available<<endl;
        
        // this sets up group samples
        // randomly samples from available data then remove chosen
        // group samples will be the sample subjects in each class
        for ( unsigned int gr = 0; gr < K; gr++ ) {
            // for each group
            vector< unsigned int > group;
            
            // spread out extras
            unsigned int left_over_one = 0;
            if (left_over > 0) {
                left_over_one = 1;
                --left_over;
            }
            
            for (unsigned int i = 0; i < N_per_class + left_over_one; i++) {
                unsigned int sample = rand() % N_available;
                list< unsigned int >::iterator i_element = available_samples.begin();
                advance(i_element, sample);
                
                group.push_back(*i_element);
                available_samples.erase(i_element);
                N_available--;
                
            }
            
            group_samples.push_back(group);
            
        }
        
        //---------------set up selection mask--------------//
        // this and above is the heart of the cross-validation
        // i.e. convert data into binary mask vector so that it's compatible with apply selection mask,
        vector< vector<int> > selection_mask;
        
        //group samples		
        for (unsigned int gr = 0 ; gr < K ; gr++) {
            
            // initialize mask to all zero, then select included
            // mask size differs from number for sample because of subsampling?
            vector<int>* one_mask = new vector<int>(targ->getNumberOfSamples(), 0);
            
            // create one selection mask for each left out group
            for (unsigned int gr_sub = 0 ; gr_sub < K ; gr_sub++) {
                
                if ( gr_sub != gr ) { 
                    
                    // include every group that is not which is being left out
                    // this get trickier because of mask changing of indcies are mapped
                    for ( vector< unsigned int >::iterator i = group_samples[gr_sub].begin(); i != group_samples[gr_sub].end(); i++) {
                        // set the appropriate subject to one
                        // then input mask can trump any samples
                        one_mask->at(*i) = 1;
                    }
                }
            }
            
            selection_mask.push_back(*one_mask);
            delete one_mask;
        }
            
        
        //cerr << "looping through number of samples" << endl;
        // leave one out (loo) sel mask
        for (unsigned int i = 0; i < K; ++i) {
            // create your loo selection mask
            
            //print(sel,"selection mask");
            //cerr << "set selection mask " << i << " to 0" << endl;
            
            d_src->applySelectionMask(selection_mask[i], sel_test); // this adds the left out subjects to the test data
            
            //cerr << "applied selection mask, now running node!" << endl;
         //   cerr << "GOING INTO RUN NODE." << endl;
            lr_inter->runNode();
         //   cerr << "DONE WITH RUN NODE." << endl;

            //cout << "size should be 1: " << lr_inter->getOutputTestData<float>().size() << endl;
            //print(lr_inter->getOutputTestData<float>(),"getoutput");
            output.push_back(lr_inter->getOutputTestData<float>()[0]);
            //cout<<"genAcc Sample "<<i<<" "<<output.back()<<endl;
            
        }
        
        //print(output,"output");
        //print(targ_true,"truth");
        float accuracy = 0;
        vector<int>::iterator iter = targ_true.begin();
        for (vector<float>::iterator i_pred = output.begin(); i_pred != output.end(); ++i_pred, ++iter) {
            accuracy += (*i_pred == *iter) ? 1 : 0 ; 
        }
        
        accuracy /= NumberOfSamples;
        //cout << "findGenAcc: generalization accuracy " << accuracy << endl;
        
        delete targ;
        delete d_src;
        delete lr_inter;
        
       // cerr << "findGenAcc end" << endl;
        
        return accuracy; 
    }

    
    float log_regression_mvdisc::findScoreStatistic(const smatrix& data, const smatrix& beta, const smatrix& yMatrix)
    {
        smatrix dataRow, probMatrix(data.MRows, 1), weightMatrix(data.MRows, 1), Winv, XTxW, U, D;
        
        // data Row is written over each time
        dataRow = data;
        smatrix_AtxBt(beta, dataRow);  // get the matrix that is beta transpose times each row of data transpose, save it in dataRow
        
        for (__CLPK_integer count = 0; count < data.MRows; ++count) {
            
            //dataRow.resize(1, data.NCols);
            //cblas_scopy(data.NCols, data.sdata + count, data.MRows, dataRow.sdata, 1);
            //smatrix_AtxBt(beta, dataRow);  // get the matrix that is beta transpose times the row of data transpose, save it in dataRow
            
            float expDif = exp(dataRow.sdata[count]);
            probMatrix.sdata[count] = expDif / (1 + expDif);
        }
        
        //smatrix_print(probMatrix, "probMatrix");
        
        // the new weights are calculated using each probability times its conjugate
        // weight matrix should be a diagonal NxN matrix, but it is being stored as a Nx1 matrix
        for(__CLPK_integer i = 0; i < weightMatrix.MRows; ++i)
            weightMatrix.sdata[i] = probMatrix.sdata[i] * (1 - probMatrix.sdata[i]);
        
        //smatrix_print(weightMatrix, "weights");
        
        // because we are taking the pseudo inverse, there are different conditions for if N > M and vice versa
        if (data.MRows >= data.NCols) {
            smatrix_AtxDiagD(data, weightMatrix, XTxW);                 // X' * W  
            smatrix_AxB(XTxW, data, U);
            smatrix_svd(U, D);                                          // SVD(X' * W * X) gives U, D * W
            
            // find (D * W) ^ (-1), with a threshold value
            for (__CLPK_integer row = 0; row < D.MRows; ++row)
                D.sdata[row] = (D.sdata[row] > ZERO_THRESH) ? 1.0f / D.sdata[row] : 0; 
            
            smatrix_AxDiagD(U, D);                                      // U * (D * W) ^ (-1)
            smatrix_BxAt(D, U);                                         // U * (D * W) ^ (-1) * U' = (X' * W * X) ^ (-1)
        } else {
            // find W ^ (-1), with a threshold value
            Winv = weightMatrix;
            for (__CLPK_integer i = 0 ; i < Winv.MRows; ++i) 
                Winv.sdata[i] = (weightMatrix.sdata[i] > ZERO_THRESH) ? 1.0f / weightMatrix.sdata[i] : 0; 
            
            smatrix XtwX_inv;
            U = data;
            smatrix_svd_of_transpose(U, D);                             // SVD(X') gives U', D ^ 0.5
            float total_var = cblas_sdot(NumberOfSamples_, D.sdata, 1, D.sdata, 1);
            
            if (Verbose)
                cout << "Total Variance " << total_var << endl;
            
            float var_thresh = (total_var * fracVarThresh_);
            
            // find D ^ (-1), with a threshold value
            for (__CLPK_integer row = 0; row < D.MRows; ++row) {
                D.sdata[row] += var_thresh;
                D.sdata[row] = (D.sdata[row] > ZERO_THRESH) ? ( 1.0f / (pow(D.sdata[row], 2) ) * Winv.sdata[row] ) : 0; // apply weight too
            }                                                                                                           // (D * W) ^ (-1)
            
            smatrix_AtxDiagD(U, D, XtwX_inv);                           // (D * W) ^ (-1)
            smatrix_AxB(XtwX_inv, U, D);                                // U * (D * W) ^ (-1) * U' = (X' * W * X) ^ (-1)
        } 

        smatrix hessianInv, score, y_p;
        hessianInv = D;
        smatrix_scaleAndAddMatrices(probMatrix, -1, yMatrix, y_p);
        smatrix_AtxB(data, y_p, score);
        smatrix scoretxHinv;
        smatrix_AtxB(score, hessianInv, scoretxHinv);
        smatrix_BxA(scoretxHinv, score);
        return scoretxHinv.sdata[0];
    }

    
    
    
    
    
    // Non member functions:
    
    float likelihoodRatio(const float redLikelihood, const float fullLikelihood)
    {
        return -2 * (redLikelihood - fullLikelihood); 
    }


}

