#!/bin/sh

#  discriminate_groups.sh
#  su_mvdisc
#
#  Created by Brian Patenaude on 9/13/11.
#  Copyright 2011 Stanford University - Etkin Lab. All rights reserved.

#This script will create the desired tree given different flags

Usage(){
    echo "discriminate_groups.sh"
    echo ""
    echo ""
    echo ""
    echo "--------Discriminant Options-----------"
    echo ""
    echo "-LDA : Use LDA instead of SVM."
    echo "-QDA : Use QDA instead of SVM."
    echo "-DecTree : Use Decision Tree."

    echo "-SVM : Use SVM (Default)."
    echo "-QuantileRegression : Use QuantileRegression."

    echo "-LR : Use Logistic Regression instead of SVM."
    echo "-LR_Fwrd_Elim_Gen <Kfold> : Use Logistic Regression instead of SVM."

    echo "-RF_SVM <B>: Use reliability filter with SVM."
    echo "-AB : Use Ada Boost."
    echo "-svm_options <options>: options to SVM ."


    echo "-CLF : Use Classifier Fusion instead of SVM."

    echo "-svm_options <options>: options to SVM ."
    echo "-save_parameters : write to file the model parameters. *** may not be implemented for all discriminants"
    echo ""
    echo "--------Filter Options-----------"
    echo ""
    echo "***These options are added in this order. If you want more control, manipulate the tree file manually."
    echo ""
    echo "-exclude_groups <1,2,3> " 
    echo "-include_groups <1,2,3> "
    echo "-include_groups_post_glm <1,2,3>"
    echo "-fill_missing_data <method> : method=zero,mean"

    echo ""
    echo "-glm_resdiuals <regressors.txt> : Pass on the residuals to of the data" 
    echo ""
    echo "-norm_feats_as <0 or 1>: Normalize feature so that each feature ranges from [-1,+1]. 0 = off, 1 =  on (default=1). Done before SVM "
    echo "-norm_feats_ws <0 or 1>: Normalize feature so that mean/variance is 0/1 (default=1); done after image read "

    echo ""
    echo "-glm_thresh <type> <threshold> : Feature Selection based on any pair-group difference(F-test). Theshold is fractional of voxels to retain. Type = OrderStatistic, Fractional, Absolute"
    echo "-glm_bootstrap <NumberOfSamples>: using bootstrap for above method"
    echo "-RV_Transform <mode> : transform Random Variables" 
    echo ""
    echo "-pca_filter <threshold> : threshold is specified as fraction of total varieance 0<th<1"
    echo ""
    echo "--------------Input Options------------------------------"
    echo ""

    echo "-input_mask <mask.nii.gz <0 or 1>: Input mask. Second argument: 0 = no normalization of data, 1 = normalize data"
    echo "-input_mask_txt <mask.txt>: Input mask. "
    echo "-dimSel <dimension>"
    echo "-dimExclude <dimension>"
    echo "                                   before intensity normalization (mean=0,var=1). (Default=0)"
    echo "-i : input data. A series of space spearate 4D images.  1 image for each group."
    echo "-icsv : input data as a csv."
    echo "-inames  : input feature names."

    echo "-ix : input data to apply discriminant towards but not used to train."
    echo "-itarget : input target file "
    echo ""
    echo "-------------------Output Options-------------------------"
    echo ""
    echo "-o : output data"

    echo "--------CrossValidation Options-----------"

    echo "-bagging <0 or 1> <thresh>: weather to use bagging or not. 0= no. 1=yes. (Default=0)"
    echo "-Kfold  <K> : Kfold cross-validation (1 is special case LOO)"

    echo ""
    echo "----------------------------------------------------------"
    echo ""
    echo "-class_map 0,1,2.... (class to map each group into)"
#echo "-stratify_classes 0,1,2.... (class to map each group into)"
    echo ""
    echo "---------------------Cross-validation/Sampling Options-------------------------------"
    echo ""
    echo "-bootstrap_subsamples"
    echo "-Nsubsamples <Nsub> <cl> <0,1,2,3...>: Specifies number of subsamples to take from the data. The samples are such that it is sampled evenly from each group."
    echo "-SubsampleOffset <Nsubjects>: Specifies number of samples to exclude from teh minimum group size"
    echo "-loadSubSamples <file>"
    echo "-ModelThresh <mode> <threshold> : Mode (Acc/FracN) refers to setting an aboslute cutoff based on accuracy vs fraction of models, ythreshodl is the value "
    echo "--weightBaggingByProb : Weight Samples in Bagging by conditional probabilities from generalization accruacy esttimation"
    echo "--rerunDiscriminate"
    echo ""


    exit 1
}

NGROUPS=0;


if [ $# = 0 ] ; then 
    Usage
    exit 1
fi

USE_CLF=0;
USE_LDA=0;
USE_QDA=0;
USE_LR=0;
LR_Gen_K=0;
USE_GP=0;
USE_RF_SVM=0;
USE_AB=0;
USE_DECTREE=0;
USE_FILL=0
USE_RERUN=0;

OUTPUTDIR="output"
QUANTREG=0

#PCA filter
PCA_FILTER=0
PCA_FILTER_THRESH=0
#NODE ON/OFF SWITCH+PAPRAMETERS
#GLM filter (for feature selection)
GLM_FEATURE_SELECT=0
GLM_FEATURE_SELECT_TH=0

INPUT_TARGET=""
SELMASK_ARGS=""
#Normlalization of features
NORM_FEATS_AS=0
NORM_FEATS_WS=0
ModelThresh_mode="_"
LeaveN=0
KFOLD=0
DECTREE_MAXLEVEL=0;
#Image Source Attrributes
INPUT_MASK=""
INPUT_MASK_PRE=0
MAPPED_CLASSES=""
WEIGHT_BY_PROB=""
CMAPFILE=""
STRAT_CLASSES=""
BAGGING_ARGS=""
SUBSAMPLESIZE=0
VERBOSE=""
VERBOSEN=""

#text file Stuff
CSV_HEADER=0
INPUT_MASK_TXT=""
RVT_mode=""
GLM_FEATURE_SELECT_BOOT=0;
USE_DIMSEL=0
DIMSEL=0
USE_DIMEXCL=0;
DIMEXCL=0
FEATURENAMES=""

#nuissance regressors to remove from input data.
ORIG_REG_FILES=""
#Dicriminate exceutbale parameters (mostly related to cross-validation)
NSUBSAMPLES=0
MINOFFSET=0
BAG_MID_TH=0.5

LOAD_ARGS=""
FILL_METH="none"
SAVE_PARAMS=0
BOOTOPTS=""


# if zero will run standard cross-validation
echo "NSUBSAMPLES $NSUBSAMPLES"
SVMOPTS=""
#while [ _${1:0:1} = _- ] ; do 
while [ $# != 0 ] ; do 
    echo "Next input $1"
    #read in group data 
    if [ ${1} = -i ] ; then 
#dont use comma separate values, so that you can use tab-completions on command line
        shift 1
        echo $1 ${1:0:1}
        while [ ! ${1:0:1} = - ] ; do
            if [ `${FSLDIR}/bin/imtest $1` = 0 ] ; then 
                echo "Invalid Image"
                exit 1
            fi
            GROUP_IMAGES="${GROUP_IMAGES} $1"
            let NGROUPS+=1
            shift 1
            if [ $# = 0 ] ; then 
                break
            fi
        done
        echo GROUP_IMAGES $GROUP_IMAGES
    elif [ ${1} = -dimSel ] ; then
        USE_DIMSEL=1
        DIMSEL=$2
        shift 2
    elif [ ${1} = -dimExclude ]  ; then
        USE_DIMEXCL=1;
        DIMEXCL=$2
	shift 2
    elif [ ${1} = -header ] ; then
        CSV_HEADER=1
        shift 1
    elif [ ${1} = -icsv ] ; then
        #dont use comma separate values, so that you can use tab-completions on command line
        shift 1
        echo $1 ${1:0:1}
        while [ ! ${1:0:1} = - ] ; do
            echo "csv file : $1 "
            if [ ! -f $1 ] ; then 
                echo "Invalid CSV file"
                exit 1
            fi
            GROUP_CSVS="${GROUP_CSVS} $1"
            let NGROUPS+=1
            shift 1
            if [ $# = 0 ] ; then 
		break
            fi
        done
        echo GROUP_CSVS $GROUP_CSVS
    elif [ ${1} = -inames ]; then
	FEATURENAMES=$2
	shift 2
    elif [ ${1} = -itarget ] ; then
        INPUT_TARGET=$2 
        shift 2
    elif [ ${1} = -fill_missing_data ] ; then

	USE_FILL=1;
	FILL_METH=$2
	shift 2
    elif [ ${1} = -save_parameters ]; then 
	SAVE_PARAMS=1
	shift 1
    elif [ ${1} = -svm_options ] ; then
        SVMOPTS=$2
        shift 2
    elif [ $1 = -QuantileRegression ] ; then
        QUANTREG=1
        shift 1
    elif [ ${1} = -pca_filter ] ; then
        PCA_FILTER=1
        PCA_FILTER_THRESH=$2
        shift 2
    elif [ ${1} = -glm_thresh ] ; then 
        GLM_FEATURE_SELECT=1
        GLM_THRESH_TYPE=$2
        GLM_FEATURE_SELECT_TH=$3
        shift 3
    elif [ ${1} = -glm_bootstrap ] ; then 
        GLM_FEATURE_SELECT_BOOT=$2
        shift 2
    elif [ $1 =  --weightBaggingByProb ]; then
        WEIGHT_BY_PROB="--weightBaggingByProb"
	shift 1
    elif [ ${1} = -glm_residuals ] ; then
	echo "glm_residuals "
        GLMNOAPPLY=0

        shift 1
        echo $1 ${1:0:1}
        while [ ! ${1:0:1} = - ] ; do
            if [ ! -f $1 ] ; then 
                echo "File does not exist: $1"
                exit 1
            fi
            ORIG_REG_FILES="${ORIG_REG_FILES} $1"               
            shift 1
            if [ $# = 0 ] ; then 
		break
            fi
        done
        echo ORIG_REG_FILES $ORIG_REG_FILES
    elif [ ${1} = -glm_residuals-NoApply ] ; then
#THIS IS COPY AND PASTED FROM ABOVE EXCEPTS SETS GLMNOAPPLY
	echo "glm_residuals noapply"

	GLMNOAPPLY=1
	shift 1
	echo $1 ${1:0:1}
	while [ ! ${1:0:1} = - ] ; do
	    if [ ! -f $1 ] ; then
		echo "File does not exist: $1"
		exit 1
	    fi
	    ORIG_REG_FILES="${ORIG_REG_FILES} $1"
	    shift 1
	    if [ $# = 0 ] ; then
		break
	    fi
	done
	echo ORIG_REG_FILES $ORIG_REG_FILES

    elif [ ${1} = -norm_feats_as ] ; then
        NORM_FEATS_AS=$2
        shift 2
    elif [ ${1} = -norm_feats_ws ] ; then
        NORM_FEATS_WS=$2
        shift 2
    elif [ ${1} = -exclude_groups ] ; then
        EXCGROUPS=$2
        shift 2
    elif [ ${1} = -include_groups ] ; then
        INCGROUPS=$2
        shift 2
    elif [ ${1} = -include_groups_post_glm ] ; then
        INCGROUPS_POSTGLM=$2
        shift 2
    elif [ ${1} = -input_mask ] ; then 
        INPUT_MASK=`readlink -f $2`
        INPUT_MASK_PRE=$3
        shift 3
    elif [ ${1} = -input_mask_txt ] ; then
        INPUT_MASK_TXT="$INPUT_MASK_TXT `readlink -f $2`"
	shift 2
    elif [ ${1} = -o ] ; then 
        OUTPUTDIR=`readlink -f $2`
        shift 2
    elif [ ${1} = -class_map ] ; then 
        if [ -f $2 ] ; then 
            CMAPFILE=$2
        else
            MAPPED_CLASSES=$2
        fi
        shift 2
    elif [ ${1} = -SubsampleOffset ] ; then 
        MINOFFSET=$2
        shift 2
    elif [ ${1} = -loadSubSamples ] ; then
        LOAD_ARGS="--loadSamples=$2"
        shift 2
    elif [ ${1} = -Nsubsamples ] ; then 
        NSUBSAMPLES=$2
        STRAT_CLASSES=$3
        shift 3
    elif [ ${1} = -bootstrap_subsamples ]; then     
        BOOTOPTS="--bootstrap=1"
        shift 1
    elif [ ${1} = -SubSampleSize ] ; then
        SUBSAMPLESIZE=$2
        shift 2
    elif [ ${1} = -RV_Transform ]; then
	RVT_mode=$2
	shift 2
    elif [ ${1} = -bagging ] ; then
        if [ $2 = 1 ] ; then
            BAG_MID_TH=$3
            BAGGING_ARGS="--bag --bag_mid_thresh=$BAG_MID_TH"
        fi
        shift 3 
    elif [ ${1} = -SVM ] ; then
	shift 1
    elif [ ${1} = -LDA ] ; then
        USE_LDA=1
        shift 1
    elif [ ${1} = -QDA ] ; then
        USE_QDA=1
        shift 1
    elif [ ${1} = -LR ] ; then
        USE_LR=1
        shift 1
    elif [ ${1} = -LR_Fwrd_Elim_Gen ] ; then
        USE_LR=1
        LR_Gen_K=$2
        shift 2
    elif [ ${1} = -GP ] ; then
        USE_GP=1
        shift 1
    elif [ ${1} = -CLF ] ; then 
        USE_CLF=1
        shift 1
    elif [ ${1} = -RF_SVM ] ; then
        USE_RF_SVM=1
        Bsamples=$2
        shift 2
    elif [ ${1} = -AB ] ; then
        USE_AB=1
        shift 1
    elif [ ${1} = -DecTree ]; then
	USE_DECTREE=1
	shift 1
    elif [ ${1} = -DecTreeMaxLevel ]; then
	DECTREE_MAXLEVEL=$2
	shift 2
    elif [ ${1} = -RF_GLM ] ; then
	USE_RF_GLM=1
	BGLMsamples=$2
	shift 2
    elif [ ${1} = -Kfold ] ; then 
        KFOLD=$2
        shift 2
    elif [ ${1} = -selMask ] ; then
	SELMASK_ARGS="--mask --selection_mask=`readlink -f $2`"
	shift 2
    elif [ ${1} = -ModelThresh ]; then
	ModelThresh_mode=$2
	ModelThresh_th=$3
	shift 3
    elif [ ${1} = -verbose ] ; then
        VERBOSE="--verbose=$2"
        shift 2
    elif [ ${1} = -verbose_nodes ] ; then
        VERBOSEN="Verbose=$2"
        shift 2
    elif [ ${1} = "-rerunDiscriminate" ]; then
	USE_RERUN=1
        RERUN_NUM=$2
        shift 2
    else
        echo "Unrecognized option: ${1}"
        Usage
        exit 1
    fi

done




if [ $USE_RERUN = 1 ]; then #copy over data the run
    OUTPUTDIR=${OUTPUTDIR}.disc
    DISCOUT_DIR=${OUTPUTDIR}/Leave-${LeaveN}-Out-${RERUN_NUM}
    #Files taht will output
    #A general information file
    INFO_FILE=${OUTPUTDIR}/info.txt
    echo "Discriminant Information File" >  $INFO_FILE
    DISC_CONFIG_FILE=${OUTPUTDIR}/config.txt
    #target file (i.e. class labels)
    TARGET_FILE=${OUTPUTDIR}/target.txt

    #group map file
    GROUP_MAP=${OUTPUTDIR}/group_map.txt

    #CLASS MAP
    CLASS_MAP=${OUTPUTDIR}/class_map.txt
    INTERP_MAP=${OUTPUTDIR}/interp_class_map.txt
    SENS_FILE=${OUTPUTDIR}/sens.txt
    #disriminant tree
    TREE_FILE=${OUTPUTDIR}/tree.txt
    LOG_FILE=${OUTPUTDIR}/run.log
    REG_FILE=${OUTPUTDIR}/regressors.txt


    MODEL_THRESH_OPTS=""

    if [ $ModelThresh_mode = Acc ]; then
        MODEL_THRESH_OPTS="--accThresh=${ModelThresh_th}"

    elif [ $ModelThresh_mode = FracN ]; then
        MODEL_THRESH_OPTS="--NaccThresh=${ModelThresh_th}"
    fi
    echo "MODEL_THRESH_OPTS $MODEL_THRESH_OPTS"




    ${DISCDIR}/discriminate -i $TREE_FILE -o ${DISCOUT_DIR}/cv_results_disc --minimumOffset=${MINOFFSET} --Nsubsamples=${NSUBSAMPLES} --sens_spec_class_inclusion=${SENS_FILE} -K ${KFOLD} ${VERBOSE} $BAGGING_ARGS $MODEL_THRESH_OPTS $SELMASK_ARGS $WEIGHT_BY_PROB $LOAD_ARGS




fi



echo NSUBSAMPLES $NSUBSAMPLES

#create output director
while [ -d ${OUTPUTDIR}.disc ] ; do
    OUTPUTDIR="${OUTPUTDIR}+"
done
OUTPUTDIR="${OUTPUTDIR}.disc"
/bin/mkdir ${OUTPUTDIR}





#########################
#Files taht will output 
#A general information file
INFO_FILE=${OUTPUTDIR}/info.txt
echo "Discriminant Information File" >  $INFO_FILE
DISC_CONFIG_FILE=${OUTPUTDIR}/config.txt
#target file (i.e. class labels)
TARGET_FILE=${OUTPUTDIR}/target.txt 

#group map file
GROUP_MAP=${OUTPUTDIR}/group_map.txt

#CLASS MAP
CLASS_MAP=${OUTPUTDIR}/class_map.txt
INTERP_MAP=${OUTPUTDIR}/interp_class_map.txt
SENS_FILE=${OUTPUTDIR}/sens.txt
#disriminant tree
TREE_FILE=${OUTPUTDIR}/tree.txt
LOG_FILE=${OUTPUTDIR}/run.log
REG_FILE=${OUTPUTDIR}/regressors.txt

echo "#The Discriminant tree was created using discriminate_group.sh" >> $TREE_FILE

#######################



#main running part 




#lets build the tree

echo $GROUP_IMAGES
echo $NGROUPS groups

echo "Creating Class Labels Files"
echo "Number of Groups : ${NGROUPS}" >> $INFO_FILE
if [ ! "_${GROUP_IMAGES}" = "_" ] ; then 
    echo "Input Group Images: ${GROUP_IMAGES}">> $INFO_FILE
else
    echo "Input Group CSVs: ${GROUP_CSVS}">> $INFO_FILE
fi
echo "GroupImage ClassLabel NumberOfSamples" > $GROUP_MAP
NperGroup=""

#lets start at 100, makes mapping easier
group=100;
class_map=0
class_map_index=0
if [ ! "_${GROUP_IMAGES}" = "_" ] ; then 
    for gr in $GROUP_IMAGES ; do
        echo "group $gr $group"
        index=$group;
        nsubs=`${FSLDIR}/bin/fslnvols $gr`
        echo "Nsubs: $nsubs"
        NperGroup="${NperGroup} $nsubs"
        
        #Create target files
        count=0    
        while [ $count -lt $nsubs ] ; do 

            echo $group >> $TARGET_FILE
            let count+=1
        done
        
        #maps classes into 0->N
        if [ ! "${MAPPED_CLASSES}_" = "_" ] ; then
            #indexing into awk starts at 1 
            if [ $class_map_index = 0 ] ; then 
                let class_map_index+=1
            fi
            class_map=`echo $MAPPED_CLASSES | awk -F , "{ print \$$class_map_index }"`
    #need to redirect into file, doesn't work wihtin ``
            echo $MAPPED_CLASSES | awk -F , "{ print \$$class_map_index }" > ${OUTPUTDIR}/grot
            class_map=`cat ${OUTPUTDIR}/grot`
            /bin/rm ${OUTPUTDIR}/grot
        else
            class_map=$class_map_index
        fi

        echo "${group} ${class_map}" >> $CLASS_MAP

        echo "${gr} ${group} ${nsubs}" >> $GROUP_MAP

        let class_map_index+=1
        let group+=1
    done
else
    if [ $NGROUPS -gt 1 ] ; then 
        for gr in $GROUP_CSVS ; do
            echo "group $gr $group"
            index=$group;
            nsubs=`wc $gr | awk '{ print $1 }'`
            echo "Nsubs: $nsubs"
            NperGroup="${NperGroup} $nsubs"

            #Create target files
            count=0    
            while [ $count -lt $nsubs ] ; do 

                echo $group >> $TARGET_FILE
                let count+=1
            done

                #maps classes into 0->N
            if [ ! "${MAPPED_CLASSES}_" = "_" ] ; then
                #indexing into awk starts at 1 
                if [ $class_map_index = 0 ] ; then 
                    let class_map_index+=1
                fi
                class_map=`echo $MAPPED_CLASSES | awk -F , "{ print \$$class_map_index }"`
                    #need to redirect into file, doesn't work wihtin ``
                echo $MAPPED_CLASSES | awk -F , "{ print \$$class_map_index }" > ${OUTPUTDIR}/grot
                class_map=`cat ${OUTPUTDIR}/grot`
                /bin/rm ${OUTPUTDIR}/grot
            else
                class_map=$class_map_index
            fi

            echo "${group} ${class_map}" >> $CLASS_MAP

            echo "${gr} ${group} ${nsubs}" >> $GROUP_MAP

            let class_map_index+=1
            let group+=1
        done

    elif [ $NGROUPS = 1 ] ; then 
        echo "Only one csv found, going to assume a binary classfication scheme"


        cp $INPUT_TARGET $TARGET_FILE

        if [ "_" = "_${CMAPFILE}" ]; then  
            NGROUPS=2
            nsubs=`wc $TARGET_FILE | awk '{ print $1 }'`
            for i in 0 1 ; do 
                ngrot=`grep $i $TARGET_FILE | wc | awk '{ print $1 }'`
                NperGroup="${NperGroup} $ngrot"
                echo "$i $i" >> $CLASS_MAP
                echo "${gr} $i $ngrot" >> $GROUP_MAP

            done
        else
            cp $CMAPFILE $CLASS_MAP
        fi
        echo Nsubject : $nsubs
        echo NsubjectPerGroup : $NperGroup



    else
        echo "Found no groups. Exiting..."
        exit 1
    fi

fi




DISCOUT_DIR=${OUTPUTDIR}/Leave-${LeaveN}-Out

echo "Number of Samples per Group : ${NperGroup}" >> $INFO_FILE

#lets process target arguments
#some are blank until option is added
CLASS_MAP_ARGS="class_map=${CLASS_MAP}"
if [ "${STRAT_CLASSES}_" = "_" ] ; then 
    STRATIFY_ARGS="StratifyLabels=0,1"
else
    STRATIFY_ARGS="StratifyLabels=${STRAT_CLASSES}"
fi

#computer whether to regress out variables of no interest
REGRESSOR_ARGS=""
if [ ! "${ORIG_REG_FILES}_" = "_" ] ; then 
    Nreg_files=`echo $ORIG_REG_FILES | wc | awk '{ print $2 }'`

    N_images=`echo $GROUP_IMAGES | wc | awk '{ print $2 }'`
    N_csvs=`echo $GROUP_CSVS | wc | awk '{ print $2 }'`

#check to make sure that regressors were given for each group, these should not be correlated
    echo "nregs ${Nreg_files}"

#check to make sure the number per group is the same

    if [ ! "_${GROUP_IMAGES}" = "_" ] ; then 
        if [ ! $Nreg_files = $N_images ] ; then
            echo "Error : Number of Input Images is not equal to number of regressor files"
            exit 1
        fi
        c=1
        for gr in $GROUP_IMAGES ; do
            Nim=`${FSLDIR}/bin/fslnvols $gr`
            file=`echo $ORIG_REG_FILES | awk '{ print $1 }'`
            Nregs=`wc  $file | awk '{ print $1 }'`
            if [ ! $Nreg_files = $N_images ] ; then
                echo "Error : Number of Subjects in Regresors and data do not match"
                exit 1
            fi


            let c+=1
        done
    else
        if [ ! $Nreg_files = $N_csvs ] ; then
            echo "Error : Number of Input Images is not equal to number of regressor files"
            exit 1
        fi
        c=1
        for gr in $GROUP_CSVS ; do
            echo "READ CSVS"
            Nim=`wc $gr | awk '{ print $1 }'`
            file=`echo $ORIG_REG_FILES | awk '{ print $1 }'`
            Nregs=`wc  $file | awk '{ print $1 }'`
            ns_p0=$nsubs
            let nsubs+=1
            if [ $Nim = $ns_p0 ]; then
                echo "$gr has NO header"
                CSV_HEADER=0
            elif [ $Nim = $nsubs ]; then
                echo "$gr HAS no header"
                CSV_HEADER=1
            else
                echo "mismatch in input number subject and number in target"
                exit 1
            fi


            let c+=1
        done


    fi
    cat $ORIG_REG_FILES > ${REG_FILE}
    REGRESSOR_ARGS="regressors=${REG_FILE}"
fi 


NODE=0
NODE_PREV=0
if [ ! _ = "_${INPUT_MASK_TXT}" ] ; then 
    #allow multiple masks
#  SUBSELECT_MASK_ARGS="maskname=$INPUT_MASK_TXT"
    SUBSELECT_MASK_ARGS="maskname="
    mcount=1
    for i in $INPUT_MASK_TXT ; do 
	if [ $mcount = 1 ] ; then  
	    SUBSELECT_MASK_ARGS="${SUBSELECT_MASK_ARGS}$i"

	else
	    SUBSELECT_MASK_ARGS="$SUBSELECT_MASK_ARGS,$i"

	fi

	let mcount+=1
    done
fi
#create tree target
echo "ClassLabels   Target  filename=$TARGET_FILE $SUBSELECT_MASK_ARGS $CLASS_MAP_ARGS $STRATIFY_ARGS $REGRESSOR_ARGS" >> $TREE_FILE

#add in discirminant

if [ $USE_LDA = 1 ] ; then 
    echo "node${NODE}   LDA	None $VERBOSEN SaveParameters=$SAVE_PARAMS  Output_Name=${DISCOUT_DIR}/lda calcGenAccuracy=false" >> $TREE_FILE
    echo "Discriminant Used : LDA ">> $INFO_FILE
elif [ $USE_QDA = 1 ] ; then 
    echo "node${NODE}   QDA	None $VERBOSEN" >> $TREE_FILE
    echo "Discriminant Used : QDA ">> $INFO_FILE
elif [ $USE_LR = 1 ] ; then 

    if [ $LR_Gen_K = 0 ] ; then 
	echo "node${NODE}   LogisticRegression	None $VERBOSEN " >> $TREE_FILE
	echo "Discriminant Used : Logistic Regression ">> $INFO_FILE

    else
	echo "node${NODE}   LogisticRegression	None $VERBOSEN ForwardStepwiseGenAcc=$LR_Gen_K " >> $TREE_FILE
	echo "Discriminant Used : Logistic Regression - Forward Feature Elimination  - ${LR_Gen_K}-fold (">> $INFO_FILE

    fi

    echo "Discriminant Used : LogisticRegression ">> $INFO_FILE
elif [ $USE_CLF = 1 ] ; then 

    echo "node${NODE}   ClassifierFusion	None $VERBOSEN " >> $TREE_FILE


    echo "Discriminant Used : ClassifierFusion ">> $INFO_FILE

elif [ $USE_GP = 1 ] ; then 
    echo "node${NODE}   GaussianProcessFilter	None $VERBOSEN Mode=LSClassifier" >> $TREE_FILE
    echo "Discriminant Used : Gaussian_Process ">> $INFO_FILE
elif [ $USE_AB = 1 ] ; then 
    echo "node${NODE}   Ada_Boost   None $VERBOSEN nodeType=QDA mRuns=5 univariate=true" >> $TREE_FILE
    echo "Discriminant Used : Ada_Boost : nodeType=QDA" >> $INFO_FILE
elif [ $USE_DECTREE = 1 ];then
    DECOPTS=""
    if [ $DECTREE_MAXLEVEL -ne 0 ] ; then
        DECOPTS="MaxLevel=$DECTREE_MAXLEVEL"
    fi
    echo "node${NODE}   DecisionTree	None $VERBOSEN ${DECOPTS}" >> $TREE_FILE
    echo "Discriminant Used : DecisionTree ">> $INFO_FILE
else
    echo "node${NODE}   SVM	None svm_type=C-SVC kernel_type=linear $VERBOSEN TrackStats=NO Output_Name=${DISCOUT_DIR}/SVM_lin_weights ${SVMOPTS}" >> $TREE_FILE
    echo "Discriminant Used : SVM ">> $INFO_FILE

fi
let NODE+=1


if [ $QUANTREG = 1 ] ; then
    echo "QuantileRegression : Yes" >>$INFO_FILE
    echo "node${NODE}	QuantileRegression	node${NODE_PREV}   " >> $TREE_FILE
    let NODE+=1
    let NODE_PREV+=1
else
    echo "QuantileRegression : No" >>$INFO_FILE
fi


#NORMALIZATION
if [ $NORM_FEATS_AS = 1 ] ; then 
    echo "Normalization each feature : Yes" >>$INFO_FILE

    echo "node${NODE}	NormalizeFilter	node${NODE_PREV}  normMode=MinMax_AS " >> $TREE_FILE
    let NODE+=1
    let NODE_PREV+=1
else
    echo "Normalization each feature across subjects : No" >>$INFO_FILE
fi

#RELIABILITY FILTER

if [ $USE_RF_SVM = 1 ] ; then 
    if [ ! "_$Bsamples" = "_" ]; then  
	echo "node${NODE}  Reliability_Filter	node${NODE_PREV}			nodeType=SVM Inc_Criteria=NotInBounds threshold=0.0 useAbsoluteValue=0 FilterMode=FeatureSelection $VERBOSEN  Bsamples=$Bsamples" >> $TREE_FILE
	echo "Discriminant Used : RF_SVM : nodeType=SVM Inc_Criteria=NotInBounds threshold=0.0 useAbsoluteValue=0 FilterMode=FeatureSelection Bsamples=$Bsamples">> $INFO_FILE

    else
	echo "node${NODE}  Reliability_Filter	node${NODE_PREV}			nodeType=SVM Inc_Criteria=NotInBounds threshold=0.0 useAbsoluteValue=0 FilterMode=FeatureSelection  $VERBOSEN " >> $TREE_FILE
	echo "Discriminant Used : RF_SVM : nodeType=SVM Inc_Criteria=NotInBounds threshold=0.0 useAbsoluteValue=0 FilterMode=FeatureSelection ">> $INFO_FILE

    fi
    let NODE+=1
    let NODE_PREV+=1
else
    echo "Reliability_Filter SVM : No ">> $INFO_FILE
fi 



##---------END DISCRIMINANT PORTION-------------------


if [ $PCA_FILTER = 1 ] ; then 

    echo "Using PCA for Dimensionality Reduction: Yes" >>$INFO_FILE
    echo "Fraction of variance retained by the PCA: $PCA_FILTER_THRESH " >>$INFO_FILE

    echo "node${NODE}		PCA_Filter		node${NODE_PREV}	threshold=${PCA_FILTER_THRESH} " >> $TREE_FILE
    let NODE+=1
    let NODE_PREV+=1
else
    echo "Using PCA for Dimensionality Reduction: No" >>$INFO_FILE
fi

if [ ! "${INCGROUPS_POSTGLM}_" = "_" ] ; then 
    echo "Only Include Groups: ${INCGROUPS_POSTGLM}" >>$INFO_FILE
    echo "node${NODE}   GroupSelect_Filter	node${NODE_PREV} mode=INCLUDE groups=${INCGROUPS_POSTGLM}" >> $TREE_FILE
    let NODE+=1
    let NODE_PREV+=1

fi





#REGRESSORS
if [ ! "${ORIG_REG_FILES}_" = "_" ] ; then 
    echo "Regress out covariates : Yes" >>$INFO_FILE
    echo "Original Regressor file : ${ORIG_REG_FILES}" >>$INFO_FILE
    if [ $GLMNOAPPLY = 0  ] ; then
        echo "node${NODE}	GLM_Filter	node${NODE_PREV}  filter_mode=Residuals $VERBOSEN" >> $TREE_FILE
    else
        echo "node${NODE}	GLM_Filter	node${NODE_PREV}  filter_mode=Residuals-NoApply $VERBOSEN " >> $TREE_FILE
    fi

    let NODE+=1
    let NODE_PREV+=1
else
    echo "Regress out covariates : No" >>$INFO_FILE
fi

    #GLM FILTER 
if [ $GLM_FEATURE_SELECT = 1 ] ; then 
    
    echo "Using univariate GLM-based feature selection: Yes" >>$INFO_FILE
    echo "GLM-based feature selection: $GLM_THRESH_TYPE : ${GLM_FEATURE_SELECT_TH} (include everything above this percentile)" >>$INFO_FILE
    echo "GLM-based feature selection: Number of Bootstrap Samples : $GLM_FEATURE_SELECT_BOOT (include everything above this percentile)" >>$INFO_FILE

    if [ $GLM_FEATURE_SELECT_BOOT -gt 0 ] ; then 
        echo "node${NODE}		GLM_Filter			node${NODE_PREV}	threshold=${GLM_FEATURE_SELECT_TH} TrackStats=YES filter_mode=Bootstrap BootSamples=$GLM_FEATURE_SELECT_BOOT threshold_mode=${GLM_THRESH_TYPE} Output_Name=${DISCOUT_DIR}/GLM_feature_select $VERBOSEN" >> $TREE_FILE

    else

        echo "node${NODE}		GLM_Filter			node${NODE_PREV}	threshold=${GLM_FEATURE_SELECT_TH} TrackStats=YES threshold_mode=${GLM_THRESH_TYPE} Output_Name=${DISCOUT_DIR}/GLM_feature_select $VERBOSEN" >> $TREE_FILE
    fi

    let NODE+=1
    let NODE_PREV+=1
else
    echo "Using univariate GLM-based feature selection: No" >>$INFO_FILE
fi





#select specific groups to include in the training
if [ ! "_${INCGROUPS}" = "_" ]; then 
    echo "Only Include Groups: ${INCGROUPS}" >>$INFO_FILE
    echo "node${NODE}   GroupSelect_Filter	node${NODE_PREV} mode=INCLUDE groups=${INCGROUPS}" >> $TREE_FILE
    let NODE+=1
    let NODE_PREV+=1
fi
if [ ! "_${EXCGROUPS}" = "_" ]; then 
    echo "Exclude Groups : ${EXCGROUPS} ">> $INFO_FILE
    echo "node${NODE}   GroupSelect_Filter	node${NODE_PREV} mode=EXCLUDE groups=${EXCGROUPS}" >> $TREE_FILE
    let NODE+=1
    let NODE_PREV+=1
fi

#NORMALIZATION
if [ $NORM_FEATS_WS = 1 ] ; then 
    echo "Normalization each feature : Yes" >>$INFO_FILE

    echo "node${NODE}	NormalizeFilter	node${NODE_PREV}  normMode=Mean_And_Variance_WS " >> $TREE_FILE
    let NODE+=1
    let NODE_PREV+=1
else
    echo "Normalization each feature within subjects : No" >>$INFO_FILE
fi

if [ ! "_$RVT_mode" = "_" ] ; then

    echo "Random Variable Transform : Yes $RVT_mode " >>$INFO_FILE

    echo "node${NODE}   RV_Transform  node${NODE_PREV}  xfmMode=$RVT_mode " >> $TREE_FILE
    let NODE+=1
    let NODE_PREV+=1
else
    echo "Random Variable Transform : No" >>$INFO_FILE


fi


if [ $USE_FILL = 1 ]; then
    echo "FillMissingData : $USE_FILL - $FILL_METH" >> $INFO_FILE

    echo "node${NODE} FillDataFilter node${NODE_PREV} fillMode=$FILL_METH">> $TREE_FILE

    let NODE+=1
    let NODE_PREV+=1
else
    echo "FillMissingData : $USE_FILL" >> $INFO_FILE
    echo "FillMissingData ${USE_FILL}  $FILL_METH" >> $DISC_CONFIG_FILE

fi




if [ $USE_DIMSEL = 1 ] ; then
    echo "DimensionSelect : $DIMSEL" >> $INFO_FILE
    echo "node${NODE} DimensionSelect_Filter node${NODE_PREV}  $VERBOSEN mode=INCLUDE dim=$DIMSEL ">> $TREE_FILE
    let NODE+=1
    let NODE_PREV+=1

fi
if [ $USE_DIMEXCL = 1 ] ; then
    echo "DimensioExclude : $DIMEXCL" >> $INFO_FILE
    echo "node${NODE} DimensionSelect_Filter node${NODE_PREV}  $VERBOSEN mode=EXCLUDE dim=$DIMEXCL ">> $TREE_FILE
    let NODE+=1
    let NODE_PREV+=1

fi

#lets handle the image source + masks
if [ ! "_${GROUP_IMAGES}" = "_" ] ; then 

    echo "merging  $GROUP_IMAGES"
    ${FSLDIR}/bin/fslmerge -t ${OUTPUTDIR}/training_data $GROUP_IMAGES
    echo "done merge"
else
    cat $GROUP_CSVS > ${OUTPUTDIR}/training_data.csv
fi
#input data
MASK_ARGS=""
if [ ! "${INPUT_MASK}_" = "_" ] ; then     
#MASK_ARGS="${MASK_ARGS} maskname=${INPUT_MASK}"
    MASK_ARGS="${MASK_ARGS} external_mask=${INPUT_MASK} Normalize_Pre_Mask=${INPUT_MASK_PRE}"
fi

FNAMES_ARGS=""
if [ ! _$FEATURENAMES = _ ] ; then
    FNAMES_ARGS="featureNames=$FEATURENAMES"
fi


if [ ! "_${GROUP_IMAGES}" = "_" ] ; then 

    echo "node${NODE}		ImageSource	node${NODE_PREV}  $VERBOSEN filename=${OUTPUTDIR}/training_data ${FNAMES_ARGS} ${MASK_ARGS} mask_mode=INTERSECTION">> $TREE_FILE
else

    echo "node${NODE}	TextSource	node${NODE_PREV}  $VERBOSEN filename=${OUTPUTDIR}/training_data.csv header=$CSV_HEADER ${FNAMES_ARGS} ${MASK_ARGS} $SUBSELECT_MASK_ARGS ">> $TREE_FILE

fi

mkdir ${DISCOUT_DIR}/
echo NSUBSAMPLES $NSUBSAMPLES
if [ $NSUBSAMPLES -gt 0 ] ; then 
    echo "Using Even Group Subsampling: Yes " >> $INFO_FILE
    echo "Strafying across $STRAT_CLASSES." >> $INFO_FILE
    echo "Samples size is $MINOFFSET less than minimum group size." >> $INFO_FILE
else
    echo "Using Even Group Subsampling: No " >> $INFO_FILE
fi


#echo "${ETKINLAB_DIR}/discriminate -i $TREE_FILE -o ${DISCOUT_DIR}/cv_results_disc --minimumOffset=${MINOFFSET} --Nsubsamples=${NSUBSAMPLES} --sens_spec_class_inclusion=${CLASS_MAP} --LeaveNout=${LeaveN}" > $LOG_FILE 


echo "NSUBSAMPLES ${NSUBSAMPLES}" >> $DISC_CONFIG_FILE
echo "MINOFFSET ${MINOFFSET}" >> $DISC_CONFIG_FILE
echo "LeaveN ${LeaveN}" >> $DISC_CONFIG_FILE
if [ _${BAGGING_ARGS:0:5} = "_--bag" ] ; then
    echo "BAGGING 1" >> $DISC_CONFIG_FILE
else
    echo "BAGGING 0" >> $DISC_CONFIG_FILE
fi


if [ ! "_${SELMASK_ARGS}" = "" ] ; then
    echo "$SELMASK_ARGS" >> $DISC_CONFIG_FILE
fi

echo "KFOLD ${KFOLD}"  >> $DISC_CONFIG_FILE
echo WEIGHT_BY_PROB $WEIGHT_BY_PROB >> $DISC_CONFIG_FILE    

DISCDIR=`readlink -f  $0`
DISCDIR=`dirname $DISCDIR`

DIRB=/Volumes/Smurf-Village/SoftwareRepository/etkinlab/git/su_mvdisc/build/Release
DIRB2=//Users/brianpatenaude//susrc/git/su_mvdisc/build/Release

MODEL_THRESH_OPTS=""

if [ $ModelThresh_mode = Acc ]; then
    MODEL_THRESH_OPTS="--accThresh=${ModelThresh_th}"

elif [ $ModelThresh_mode = FracN ]; then
    MODEL_THRESH_OPTS="--NaccThresh=${ModelThresh_th}"
fi
echo "MODEL_THRESH_OPTS $MODEL_THRESH_OPTS"



echo "${DISCDIR}/discriminate -i $TREE_FILE -o ${DISCOUT_DIR}/cv_results_disc --minimumOffset=${MINOFFSET} --Nsubsamples=${NSUBSAMPLES} --sens_spec_class_inclusion=${SENS_FILE}  -K ${KFOLD} --LeaveNout=${LeaveN} $BAGGING_ARGS $MODEL_THRESH_OPTS  $SELMASK_ARGS $WEIGHT_BY_PROB $LOAD_ARGS"  > $LOG_FILE


echo "Running ... "
echo "${DISCDIR}/discriminate -i $TREE_FILE -o ${DISCOUT_DIR}/cv_results_disc --minimumOffset=${MINOFFSET} --Nsubsamples=${NSUBSAMPLES} --sens_spec_class_inclusion=${SENS_FILE} -K ${KFOLD} ${VERBOSE} $BAGGING_ARGS $MODEL_THRESH_OPTS $SELMASK_ARGS $WEIGHT_BY_PROB $LOAD_ARGS"

${DISCDIR}/discriminate -i $TREE_FILE -o ${DISCOUT_DIR}/cv_results_disc --minimumOffset=${MINOFFSET} --Nsubsamples=${NSUBSAMPLES} --sens_spec_class_inclusion=${SENS_FILE} -K ${KFOLD} ${VERBOSE} $BAGGING_ARGS $MODEL_THRESH_OPTS $SELMASK_ARGS $WEIGHT_BY_PROB $LOAD_ARGS $BOOTOPTS



#this cna quickly lead to an explosion of disk space if not delete
${FSLDIR}/bin/imrm ${OUTPUTDIR}/training_data



