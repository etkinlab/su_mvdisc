/*
 *  GLM_filter.h
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 10/19/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


#include <base_image_filter.h>

#include <vector>
namespace su_mvdisc_name{
	
	//WS = within sample
	//AS = across sample
	//enum NORM_MODE { VAR_NORM_WS,MEAN_VAR_NORM_WS,MINMAX_WS, MEAN_NORM_WS,NUMBER_OF_WS,VAR_NORM_AS,MEAN_VAR_NORM_AS,MEAN_NORM_AS, MINMAX_AS  };
	
    
   
    
	class GLM_Filter : public base_image_filter
	{
        enum FILTER_MODE { FTEST_ALL_PAIRS,RESIDUALS,RESIDUALS_NOAPPLY, BOOT_FTEST_ALL_PAIRS };
        enum GLM_THRESH_MODE { ABSOLUTE, FRACTIONAL, ORDER_STATISTIC };
        
	public:
		GLM_Filter();
		virtual ~GLM_Filter();
		void setOptions( std::stringstream & ss_options);
		void applyToTestData();


		void estimateParameters();
        
        
		std::vector<float> getParameters();
		void getParameters( std::vector< std::list<float> > & all_params);
		void setParameters( const std::vector< float > & params);
		void setParameters( const std::vector<float> & params, const std::vector<unsigned int> & mask );
		void setIOoutputSpecific();

		unsigned int getNumberOfParameters();

		void applyParametersToTrainingData();

        GLM_THRESH_MODE getThresholdMode() { return thresh_mode; }
        void setThresholdMode( GLM_THRESH_MODE mode ){ thresh_mode = mode; }

		//virtual void writeStats();

	//	void runSpecificFilter();
		//		void setNormMode( NORM_MODE mode );
		//	void setNormMode( const std::string & mode );
		
	protected:
				void runTrackStats();
	private:
        void estimateParametersFeatureSelectBootStrap();
        void estimateParametersFeatureSelect();
        void estimateParametersResiduals();
        
        void applyToTestDataFeatureSelect();
		void applyToTestDataResiduals();
        void applyParametersToTrainingDataFeatureSelect();
        void applyParametersToTrainingDataResiduals();
        
        void setIOoutputSpecificFeatureSelect();
		void setIOoutputSpecificResiduals();

        
        void runTrackStatsFeatureSelect();
		void runTrackStatsResiduals();
       

		float *F_stats;
		float threshold, order_threshold;
		float max_F;
        float *estimated_betas;
        //when regressing out data, preserve the means (i.e. add it back)
        bool preserveMean;
		//void normalize( std::vector<float>::iterator  i_begin, std::vector<float>::iterator  i_end, const unsigned int & inc);
		//void ApplyNormalize( std::vector<float>::iterator  i_begin, std::vector<float>::iterator  i_end, const unsigned int & inc,const unsigned int & index);
//		int min(const int & m, const int & n );
//		float *D,*U,*VT,*work;
		//void calculateBiasAndScale( float & bias, float & scale, std::vector<float>::iterator  i_begin, std::vector<float>::iterator  i_end, const unsigned int & inc);
		FILTER_MODE filt_mode;
		GLM_THRESH_MODE thresh_mode;
		
        unsigned int B;
		//-----Stats Tracking options--------//
		bool stats_threshF;
		
		//----------------------------//
		
		//	NORM_MODE normalization_method;
		//	std::vector<float> v_bias, v_scale;
		//	float lower_limit, upper_limit;
	};
	
}
