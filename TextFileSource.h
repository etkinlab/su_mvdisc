#ifndef TextFileSource_H
#define TextFileSource_H


/*
 *  TextFileSource.h
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 8/10/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "data_source.h"

//STL include
#include <iostream>


namespace su_mvdisc_name{
	
	class TextFileSource : public DataSource 
	{
        enum TextSrcfileTypes { CSV, SSV};

	public:
		TextFileSource();
		~TextFileSource();
        //setOptions is implemented for each node to give node specific options
		void setOptions( std::stringstream & ss_options);

        //Set file type for reading. Options are CSV and SSV
        void setFileType( const TextSrcfileTypes & ftype );
        void setFileType( const std::string & ftype );
        
        void setMaskName(const std::string & filename );
        void selectDimension( const unsigned int & dim );
        //Inidicate whether or not the input file has a header
        void setHeader( const bool & hasHeader) { hasHeader_=hasHeader; }
		void readFile( );
		
	protected:
		
	private:
        
        bool hasHeader_;
        std::string maskname;
        std::vector<bool> dim_mask_;
        std::vector<std::string> header_;
		TextSrcfileTypes filetype_;
	};
	
}

#endif
