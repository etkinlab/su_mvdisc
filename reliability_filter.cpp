/*
 *  reliability_filter.cpp
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 11/11/10.
 *  Copyright 2010 NRI. All rights reserved.
 *
 */
#include <cstdlib>

#include "reliability_filter.h"

#include <cmath>
#include <vector>
#include <misc_utils.h>
//#include <misc_utils/misc_utils.h>
//#include <cstdlib>
//base nodes
#include <data_source.h>
#include <BaseFilter.h>


//compatible nodes
#include <glm_filter.h>
#include <svm_disc.h>

using namespace std;
using namespace misc_utils_name;

namespace su_mvdisc_name{
    
    
    
    Reliability_Filter::Reliability_Filter()
    {
        nodeType=FILTER;
        filter_t = nt_Unspecified;
        NodeName="Reliability Filter";
        //RequireSelectionMask=false;
        //filter_object=NULL;
        seed_init=RANDOM;
        useAbsoluteValue=false;
        accu_thresh=0.65;
        filterMode = FEATURE_SELECTION;
        rf_inc_crit = NoExclusions;
        testMethod = None;
        sampleMethod=BOOTSTRAP;
        rf_thresh=0;
        ci_alpha=0.05;
        B=1000;
    }
    
    
    
    Reliability_Filter::~Reliability_Filter()
    {
        //	if ( filter_object != NULL )
        //		delete filter_object;
    }
    
    
    void Reliability_Filter::setOptions( std::stringstream & ss_options)
    {
        map<string,string> options = parse_sstream(ss_options);
        for ( map<string,string>::iterator it = options.begin(); it!=options.end();it++)
        if ( it->first == "sampleMethod")
        {
            if (it->second == "bootstrap")
            sampleMethod = BOOTSTRAP;
            
        }else if (it->first == "Bsamples" )
        {
            stringstream ss;
            ss<<it->second;
            ss>>B;
        }else if (it->first == "threshold" )
        {
            stringstream ss;
            ss<<it->second;
            ss>>rf_thresh;
        }else if ( it->first == "nodeType" )
        {
            if ( it->second == "GLM_Filter")
            {
                filter_t = nt_GLM_Filter;
                //	GLM_Filter* node = new GLM_Filter();
                //	setNode(node);
            }else if (it->second == "SVM" ) {
                filter_t = nt_SVM;
                //					SVM_mvdisc* node = new SVM_mvdisc();
                //					setNode(node);
                //	RequireSelectionMask=true;
                
            }else {
                throw ClassificationTreeNodeException(("Reliability Filter: Tried to load invalid node type. " + it->second + ".").c_str());
                
            }
        }else if ( it->first == "FilterMode" )
        {
            if ( it->second == "FeatureSelection")
            {
                filterMode = FEATURE_SELECTION;
            }else if (it->second == "SubjectSelection" ) {
                filterMode = SUBJECT_SELECTION;
            }else {
                throw ClassificationTreeNodeException(("Reliability Filter: Invalid FilterMode " + it->second + ".").c_str());
                
            }
        }else if (it->first == "Inc_Criteria")
        {
            string s_inc = it->second;
            if (s_inc == "InBounds")
            rf_inc_crit = InBounds;
            else if(s_inc == "NotInBounds")
            rf_inc_crit = NotInBounds;
            else if(s_inc == "GreaterThanMin")
            rf_inc_crit = GreaterThanMin;
            else if(s_inc == "LessThanMax")
            rf_inc_crit = LessThanMax;
            else if(s_inc == "LessThanMin")
            rf_inc_crit = LessThanMin;
            else if(s_inc == "GreaterThanMax")
            rf_inc_crit = GreaterThanMax;
            //count<<"setting inc criteria "<<rf_inc_crit<<endl;
        }else if (it->first == "useAbsoluteValue")
        {
            string s_abs = it->second;
            if ((s_abs == "true") || (s_abs == "1"))
            useAbsoluteValue = true;
            else if ((s_abs == "false") || (s_abs == "0"))
            useAbsoluteValue = false;
            else
            throw ClassificationTreeNodeException("Reliability Filter: invalid input to useAbsoluteValue. ");
            
            
        }else if (it->first == "Initial_Seed")
        {
            if ( it->second == "Zero")
            {
                seed_init = ZERO;
            }else if (it->second == "Random" ) {
                seed_init =	RANDOM;
            }else {
                throw ClassificationTreeNodeException(("Reliability Filter: Invalid Initial_Seed " + it->second + ".").c_str());
                
            }
            
        }else if (it->first == "nodeOptions" )
        {
            node_options<<it->second;
        }else {
            if (!setTreeNodeOptions(it->first, it->second))
            throw ClassificationTreeNodeException(("Reliability Filter: Option, "+it->first+", does not exist.").c_str());
            
        }
        
        
        
        
        //else {
        //			throw ClassificationTreeNodeException("Invalid input argument");
        //		}
        
    }
    
    
    void Reliability_Filter::runNode( bool with_estimation )
    {
        
        //reliability doesn't use the with estimation
        if (Verbose)
        cout<<"Running Relaibility Filter"<<endl;
        
        cout<<"Running Relaibility Filter "<<filter_t<<endl;
        
        
        if (filter_t == nt_Unspecified)
        throw ClassificationTreeNodeException("Tried to run Reliability filter without setting filter type.");
        
        ClassificationTreeNode* filter_object = nullptr;
        
        switch (filter_t) {
                case nt_GLM_Filter:
                //count<<"creating GLM_Filter"<<endl;
                filter_object = new GLM_Filter();
                break;
                case nt_SVM:
                filter_object = new SVM_mvdisc();
                break;
            default:
                break;
        }
        
        filter_object->setOptions(node_options);
        for (vector< ClassificationTreeNode* >::iterator i = node_inputs.begin() ; i != node_inputs.end(); i++)
        {
            (*i)->runNode();
        }
        
        
        setTarget();
        
        setInputData();
        
        setTestData();
        
        //----------------------------------- LETS SET UP OUR TREE NODE THAT WERE GOING TO DO RELIABILITY FILTER FOR----------------------------//
        //First we need to store the original training data, so we can set it back later
        DataSource* d_source  = new DataSource();
        d_source->setTarget(training_target);
        for ( vector< vector<float>* >::iterator i = in_data.begin(); i != in_data.end();i++)
        {
            vector<float>* data_src = new vector<float>((*i)->begin(),(*i)->end());
            d_source->addInputData(data_src,Dimensionality_,NumberOfSamples_);
        }
        
        filter_object->addInput(d_source);
        
        //generate selection mask, here is where you allows options for bootstrap etc...
        if ( seed_init == ZERO )
        srand ( 0 );
        else if (seed_init == RANDOM) {
            srand ( time(NULL) );
        }
        
        //
        vector< vector< int> > selection_masks;
        cout<<"start sampling Reliability filter "<<endl;
        if ( sampleMethod == BOOTSTRAP )
        {
            if (Verbose)
            cout<<"Start Bootstrapping (NumberOfSamples_): "<<NumberOfSamples_<<endl;
            //for (unsigned int i = 0 ; i < B; ++i)
            unsigned int i_B = 0 ;
            while (  i_B < B )
            {
                vector< int > one_mask(NumberOfSamples_,0);
                //allows multiple occurances, selection masks allow for repeated number of sample. Yeah for me for enabling this at the beginning :D
                for (unsigned int j = 0 ; j < NumberOfSamples_ ; ++j)
                ++one_mask[rand() % NumberOfSamples_];
                
                
                //	if (Verbose)
                //	{
                //		cout<<"B "<<i_B<<" "<<NumberOfSamples_<<" "<<training_target->getNumberOfSamples_()<<endl;
                //		print< int>(one_mask, "A bootstrap sample");
                //		print<int>(training_target->getTargetData<int>(),"targetrf ");
                //	}
                //lets just make sure that we have at least one of each group, or else group tests will fail
                //this will be mostly for debugging
                bool valid_sample=true;
                
                {//test if vlaid sample
                    map<int,int> label2count;
                    //	for (unsigned int ii = 0 ; ii< NumberOfSamples_; ++ii)
                    unsigned int t_index=0;
                    for(vector<int>::iterator i_one_mask = one_mask.begin(); i_one_mask != one_mask.end(); ++i_one_mask,++t_index)
                    {
                        //if (one_mask[ii]>0)
                        if (*i_one_mask > 0)
                        {
                            for (int samp = 0; samp < *i_one_mask; ++samp)
                            {
                                //						label2count[training_target->getInTarget(ii)]++;
                                //						cout<<"ii "<<ii<<" "<<training_target->getInTarget(ii)<<endl;
                                label2count[training_target->getInTarget(t_index)]++;
                                //	cout<<"ii "<<t_index<<" "<<*i_one_mask<<" "<<training_target->getInTarget(t_index)<<endl;
                            }
                        }
                    }
                    //enforce requirement that at least as 2 samples per class
                    if (label2count.size() != Kclasses_ )
                    {
                        //	cout<<"failed "<<label2count.size()<<" "<<Kclasses<<endl;
                        //bootstrap must contain at least one sample from each class
                        valid_sample=false;
                    }
                    for (map<int,int>::iterator i_m = label2count.begin(); i_m != label2count.end(); ++i_m)
                    if (i_m->second < 2)
                    {
                        //bootstrap must contain at least 2 samples from each class
                        valid_sample=false;
                    }
                    
                }
                //cout<<"push back sel mask"<<endl;
                //valid if sample contains at least 2 samples from each class
                if (valid_sample)
                {
                    selection_masks.push_back(one_mask);
                    ++i_B;
                }
            }
        }
        //count<<"Number of Sample in RF filter "<<NumberOfSamples_<<endl;
        //count<<"NNs3 "<<d_source->getNumberOfSamples_()<<endl;
        //------------------------------START RUNNING THE BOOTSTRAP OR OTHER SAMPLE-----------------------------------------------------//
        cout<<"START RUNNING THE BOOTSTRAP OR OTHER SAMPLE"<<endl;
        
        //this has the original data
        //	vector<float> sx_params,sxx_params;
        vector< list<float> > all_theta_est;// = new vector< list<float> >();//(l_theta_est,B);
        vector<float> all_accu;//= new vector<float>();
        vector< vector<float> > all_sub_accu(NumberOfSamples_);// = new vector< vector<float> >( NumberOfSamples_ );//this will store the accuracies
        //	bool first=true;
        //	cout<<"start selection mask loop"<<endl;
        unsigned int count=0;
        for (vector< vector< int> >::iterator i = selection_masks.begin() ; i != selection_masks.end(); ++i,++count)//loop around different combinations of input data, and restimate paramteres
        {
            if (Verbose)
            if ( (count%100) == 0)
            cerr<<"bootstrap "<<count<<endl;
            
            vector<int> test_mask(NumberOfSamples_,1);
            //apply selection mask to data source, the data source has a copy of the current data
            //the apply selection mask will be applied to the train target
            d_source->applySelectionMask(*i,test_mask);
            
            //If we wish to use iwithin sample error rates then need to copy data to test data
            //By default the apply selection mask does teh out-of-sample case
            switch (testMethod) {
                    case None:
                    d_source->copyInData_to_TestData();
                    break;
                    case WihtinSample:
                    d_source->copyInData_to_TestData();
                    break;
                    case OutOfSample:
                    break;
                default:
                    break;
            }
            
            
            
            //run the node, this ecompasses both parameter estimation and its application,
            //could get discriminant accuracy info from here as well
            //Potentially slightly less efficient than just parameters because it does application to data as well as estimation
            //Is simplier than managing when and when not to estimate parameters
            // cerr<<"runn node"<<endl;
            filter_object->runNode();
            //cerr<<"done run node"<<endl;
            
            
            
            if (testMethod != None){//calculate accuracies of left out data
                cout<<"getoutputdata"<<endl;
                vector<int> cl_pred = filter_object->getOutputData<int>();
                cout<<"got clpred "<<cl_pred.size()<<endl;
                //	print<int>(cl_pred,"RF filter classes");
                
                //	print<int>( training_target->getTargetData<int>(),"Target data RF");
                //
                //	cout<<"test sizes " << training_target->getTargetData<int>().size()<<" "<<cl_pred.size()<<endl;
                
                int count=0, index=0;
                float accu = 0.0;
                
                //need to treat the accuracy calcualtion differently, depending on what test data method using
                //print<int>(*i,"selmask");
                
                if (testMethod == WihtinSample )
                {
                    //in_data has been copied to test data
                    accu = accuracy<int>(cl_pred,training_target->getTargetData<int>());
                    vector< vector<float> >::iterator i_sub_acc = all_sub_accu.begin();
                    unsigned int index=0;
                    for ( vector<int>::iterator i_pred = cl_pred.begin(); i_pred != cl_pred.end(); ++i_pred, ++i_sub_acc,++index)
                    if ( *i_pred ==  training_target->getInTarget(index) ){
                        i_sub_acc->push_back(1);
                    }else {
                        i_sub_acc->push_back(0);
                        
                    }
                    
                    
                    
                    
                }else if (testMethod == OutOfSample) {
                    //	for (vector<int>::iterator i_pred = cl_pred.begin(); i_pred != cl_pred.end(); ++i_pred,++index)
                    vector<int>::iterator i_pred = cl_pred.begin();
                    //keep track of accuracies for filtering subjects
                    vector< vector<float> >::iterator i_sub_acc = all_sub_accu.begin();
                    for ( vector<int>::iterator i_selmask = i->begin(); i_selmask != i->end(); ++i_selmask, ++i_sub_acc, ++index)
                    if  (*i_selmask == 0)
                    {
                        if ( *i_pred ==  training_target->getInTarget(index) ){
                            i_sub_acc->push_back(1);
                            accu += 1.0;
                        }else {
                            i_sub_acc->push_back(0);
                        }
                        
                        ++count;
                        ++i_pred;
                    }
                    accu/=count;
                    
                }else{
                    throw ClassificationTreeNodeException("Unrecognized Test sampling method in Reliability Filter");
                }
                all_accu.push_back(accu);
            }
            //	cout<<"get parameters "<<endl;
            //can match accuracies with parameter estimates
            filter_object->getParameters(all_theta_est);//use selection mask
        }
        
        cout<<"DONE BOOTSTRAP AND HAVEGOTTEN PARAMS"<<endl;
        //------------------------------DONE RUNNING THE BOOTSTRAP OR OTHER SAMPLE-----------------------------------------------------//
        
        if ( filterMode == SUBJECT_SELECTION ){
            if (Verbose)
            cout<<"Reliability Filter : Subject Selection Mode."<<endl;
            
            //		for (vector< vector<float>* >::iterator i = in_data.begin(); i != in_data.end(); ++i)
            //			print<float>(*(*i),"in_data: ");
            
            vector<float> per_subject_accu(NumberOfSamples_);
            vector<short> sub_sel_mask(NumberOfSamples_);
            
            vector<float>::iterator i_per = per_subject_accu.begin();
            vector<short>::iterator i_sub_sel = sub_sel_mask.begin();
            for ( vector< vector<float> >::iterator i = all_sub_accu.begin(); i != all_sub_accu.end(); ++i, ++i_per, ++i_sub_sel)
            {
                *i_per = mean<float,float>(*i);
                *i_sub_sel = ( *i_per > rf_thresh ) ? 1 : 0 ;
            }
            
            if (Verbose)
            print<short>(sub_sel_mask, "sub_sel_mask");
            
            
            //create output data
            if (out_data == NULL) {
                out_data = new vector<float>(NumberOfSamples_*Dimensionality_);
            }
            //	copyToSingleVector( in_data, out_data, NumberOfSamples_ );
            //now apply mask
            cout<<"Dimensionality_ before : "<<in_data.size()<<" "<<NumberOfSamples_<<" "<<Dimensionality_<<endl;
            //unsigned int Dimensionality__temp = Dimensionality_;
            //it be trimmed again at next stage
            base_filter::applySubjectFilterToData(in_data,sub_sel_mask,out_data, Dimensionality_, NumberOfSamples_);
            training_target->applySelectionMask(sub_sel_mask,sub_sel_mask);
            cout<<"out "<<out_data->size()<<" "<<NumberOfSamples_<<endl;
            cout<<"Ntarget "<<training_target->getNumberOfSamples()<<endl;
            //print<float>(*out_data,"out_data");
            
            
        }else if ( filterMode == FEATURE_SELECTION ) {
            if (Verbose)
            cout<<"Reliability Filter : Feature Selection Mode."<<endl;
            
            //this is going to calculate Confidence Intervals for Bootstrap, See Effron, Rice, etc...
            vector< pair<float,float> > CIs;
            //these are always positive
            
            float f_upper = B*(1-ci_alpha/2.0);
            float f_lower = B*( ci_alpha/2.0);
            unsigned int i_upper = static_cast<unsigned int> (f_upper);
            unsigned int i_lower = static_cast<unsigned int> (f_lower);
            float rem_up = f_upper - i_upper;
            float rem_low = f_lower - i_lower;
            
            //cout<<"rem "<<rem_up<<" "<<rem_low<<" "<<f_lower<<" "<<f_upper<<" "<<i_upper<<" "<<i_lower<<endl;
            //Lets maks out any parameters esimatimates we don't desire
            //	vector< list<float> > all_theta_est;//(l_theta_est,B);
            //print<float> (all_accu, "all_accuracies");
            if (testMethod != None)
            apply_mask(all_theta_est, all_accu,accu_thresh );
            
            //print<float>(all_accu,"trimmed accuracies");
            
            float s_mean =0.0;
            
            if (useAbsoluteValue)
            {
                if (Verbose)
                cout<<"Using absolute value of parameters."<<endl;
                for (vector< list<float> >::iterator i = all_theta_est.begin(); i != all_theta_est.end(); ++i)
                for ( list<float>::iterator ii = i->begin(); ii != i->end(); ++i)
                *ii = fabs(*ii);
                //			abs<float>(all_theta_est);
            }
            
            
            vector<float> param_means;
            
            for (vector< list<float> >::iterator i_theta = all_theta_est.begin(); i_theta!= all_theta_est.end(); ++i_theta)
            {
                
                
                s_mean = mean( *i_theta );
                
                param_means.push_back(s_mean);
                
                i_theta->sort();
                
                pair<float,float> ci;
                
                list<float>::iterator i_theta_begin_low = i_theta->begin();
                for (unsigned int i =0 ; i < i_lower ; ++i)
                ++i_theta_begin_low;
                
                
                //need the = because of reverse traversal
                list<float>::reverse_iterator i_theta_begin_up = i_theta->rbegin();
                for (unsigned int i =0 ; i <= i_lower ; ++i)
                ++i_theta_begin_up;
                
                
                //theta percentile estimates interpolate between points if not interger
                if ( rem_up == 0 )
                {
                    ci.first = 2*s_mean - *(i_theta_begin_up);
                }else {
                    ci.first = 2*s_mean - (*i_theta_begin_up) * (1.0 - rem_up );
                    //		cout<<"theta upper1 "<<" "<<*i_theta_begin_up<<endl;
                    --i_theta_begin_up;//decrement pointer (reverse iterator)
                    //		cout<<"theta upper2 "<<" "<<*i_theta_begin_up<<endl;
                    
                    ci.first -=			  (*i_theta_begin_up) *     rem_up     ;	
                }
                if ( rem_low == 0 )
                {
                    ci.second = 2*s_mean - *(i_theta_begin_low); 
                }else {
                    //		cout<<"theta lower1 "<<" "<<*i_theta_begin_low<<endl;
                    ci.second = 2*s_mean -  (*i_theta_begin_low) * (1.0 - rem_low );
                    ++i_theta_begin_low;//inccrement pointer (forward iterator)
                    //		cout<<"theta lower1 "<<" "<<*i_theta_begin_low<<endl;
                    
                    ci.second -=          	(*i_theta_begin_low) *    rem_low    ;	
                    
                }
                
                //	cout<<"confidence interval "<<ci.first<<" "<<ci.second<<endl;
                CIs.push_back(ci);
                
            }
            
            //let do some confidence interval calculations
            //calculate means
            //need to add in thresholding 
            
            //alter means, i.e. set to zero based on Confidence Inetrval
            vector<short> dim_sel_mask;
            
            switch (rf_inc_crit) {
                    case InBounds:
                    for (vector< pair<float,float> >::iterator i = CIs.begin(); i != CIs.end(); ++i)
                    if ( ( rf_thresh > i->first   )  && ( rf_thresh < i->second ) )
                    dim_sel_mask.push_back(1);
                    else {
                        dim_sel_mask.push_back(0);
                    }
                    break; 
                    case NotInBounds:
                    for (vector< pair<float,float> >::iterator i = CIs.begin(); i != CIs.end(); ++i)
                {
                    //	cout<<"Testing not in bounds "<<i->first<<" -> "<<rf_thresh<<" <- "<<i->second<<endl;
                    if (!( ( rf_thresh > i->first   )  && ( rf_thresh < i->second ) ))
                    dim_sel_mask.push_back(1);
                    else {
                        //		cout<<"exclude"<<endl;
                        dim_sel_mask.push_back(0);
                    }
                }
                    break; 
                    case GreaterThanMin:
                    for (vector< pair<float,float> >::iterator i = CIs.begin(); i != CIs.end(); ++i)
                    if (  rf_thresh > i->first   )
                {
                    //cout<<"CImask 1 "<<i->first<<endl;
                    dim_sel_mask.push_back(1);
                }else {
                    //cout<<"CImask 0 "<<i->first<<endl;
                    dim_sel_mask.push_back(0);
                    
                }
                    break;
                    case LessThanMax:
                    for (vector< pair<float,float> >::iterator i = CIs.begin(); i != CIs.end(); ++i)
                    if ( rf_thresh < i->second ) 
                    dim_sel_mask.push_back(1);
                    else {
                        dim_sel_mask.push_back(0);
                        
                    }
                    break;
                    case LessThanMin:
                    for (vector< pair<float,float> >::iterator i = CIs.begin(); i != CIs.end(); ++i)
                    if ( rf_thresh < i->first   )
                    dim_sel_mask.push_back(1);
                    else {
                        dim_sel_mask.push_back(0);
                        
                    }
                    break;
                    case GreaterThanMax:
                    for (vector< pair<float,float> >::iterator i = CIs.begin(); i != CIs.end(); ++i)
                    if  ( rf_thresh > i->second ) 
                    dim_sel_mask.push_back(1);
                    else {
                        dim_sel_mask.push_back(0);
                        
                    }
                    break;
                    case NoExclusions:
                    for (vector< pair<float,float> >::iterator i = CIs.begin(); i != CIs.end(); ++i)
                    dim_sel_mask.push_back(1);
                    break;
                default:
                    
                    throw ClassificationTreeNodeException("Could not find a valid Inclusion Criteria. Not sure how you accomplished this?");
                    break;
            }
            
            //dont think next 2 lines do anything
            //	vector<unsigned int> all_mask(NumberOfSamples_,1);
            //	d_source->applySelectionMask(all_mask,all_mask);
            
            
            if (Verbose)
            print<short>(dim_sel_mask,"Reliability Filter : Feature Selection Mask:");
            //need to chop parameters that do not correspond to a feature
            //This may have to do with generalization of get prarameter function?
            cout<<"Reliability Filter : "<<Dimensionality_<<" "<<dim_sel_mask.size()<<endl;
            dim_sel_mask.resize(Dimensionality_);
            //apply to training data 
            //----------------------------filter training data being passed on to next node------------------------//-------------
            
            //if nto feature survive keep all of them (i.e. disbale filter)
            if (sum<short>(dim_sel_mask)==0)
            dim_sel_mask.assign(Dimensionality_,1);
            
            //	cout<<"Ns "<<d_source->getNumberOfSamples_()<<" "<<filter_object->getNumberOfSamples_()<<endl;
            //print<float>(param_means,"final parameters");
            
            
            //create output data
            if (out_data == nullptr) {
                out_data = new vector<float>(NumberOfSamples_*Dimensionality_);
            }
            //	copyToSingleVector( in_data, out_data, NumberOfSamples_ );
            //now apply mask
            unsigned int Dimensionality__temp = Dimensionality_;
            //it be trimmed again at next stage
            base_filter::applyFeatureFilterToData(in_data,dim_sel_mask,out_data, NumberOfSamples_, Dimensionality__temp);
            
            
            if (out_test_data == nullptr) {
                out_test_data = new vector<float>(NumberOfSamples_*Dimensionality_);
            }
            
            if (NumberOfTestSamples_ == 0)
            throw ClassificationTreeNodeException("Reliability Filters has 0 test smaples");
            
            base_filter::applyFeatureFilterToData(in_test_data,dim_sel_mask,out_test_data, NumberOfTestSamples_, Dimensionality_);
            
        }
        
        
        
        delete filter_object;
        delete	d_source;
        //	delete all_theta_est;//(l_theta_est,B);
        //	delete all_accu;
        //	delete all_sub_accu;
        cout<<" ****************************end reliabity filter ****************************"<<endl;
        
        
        for (vector< ClassificationTreeNode* >::iterator i = node_inputs.begin() ; i != node_inputs.end(); i++)
        {
            (*i)->deleteOutData();
        }
    }
}
