#ifndef nifti_writer_H
#define nifti_writer_H
/*
 *  nifti_writer.h
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 8/19/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include <vector>
#include <DataWriter.h>
#include <vector_structs.h>
//#include <image_source.h>
namespace su_mvdisc_name
{

	class nifti_Writer : public DataWriter
	{
	public:
		nifti_Writer();
		 ~nifti_Writer();
		
		
		//----static write functions----------------------------------//
		//easier than creating ImageSources and feeding into writer objects
		static void write( const std::vector<float> & data, const su_mvdisc_name::uint3 & dims, const su_mvdisc_name::float3 & res, const std::string & outname );
		
		//----static write functions----------------------------------//

//		 void write( const std::string & filename );
	//	void setImageProperties(const image_src * im );
//		void setMask(  ImageSource * im_src ) ;
		
//		void setImageDimensions( const uint3 & dims_in );
//		void setImageDimensions(const unsigned int & x, const unsigned int & y , const unsigned int & z );
//		void setImageResolution( const float4 & res );
//		void setImageResolution(const float & x, const float & y , const float & z);
//		void setImageResolution(const float & x, const float & y , const float & z, const float & t );
		
		
	protected:
	private:
//		bool useMask;
//		ImageSource* im_src;
//		uint3 dims;
//		float4 res;
		
		
	};
}

#endif
