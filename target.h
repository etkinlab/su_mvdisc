#ifndef Target_H
#define Target_H
/*
 *  target.h
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 7/27/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


#include <misc_utils.h>

#include "ClassificationTreeNodeException.h"


//#include <misc_utils/misc_utils.h>
//STL includes
#include <vector>
//##include <vector_functions.h>
#include <su_mvdisc_structs.h>
#include <string>
#include <smatrix.h>
namespace su_mvdisc_name{

	class Target {
		
	public:
		Target();
		Target( Target *targ);
		~Target();

		void copyTarget( Target * targ );
		void setOptions( std::stringstream & ss_options);

		void setTargetData(  std::vector<float>  target);		void setTargetData(  const clapack_utils_name::smatrix &  target);		

    //    void setTargetDataToOrig() { in_target = in_target_orig; };		

		template<class T>
		std::vector<T> getTargetData() const; 
        
        template<class T>
		std::vector<T> getTargetData( const std::vector<int> & mask ) const; 
        
		void getTargetData(  clapack_utils_name::smatrix & data ) const;
        
        
        
        unsigned int getRegressorData( std::vector<float> & regs) const;
        unsigned int getTestRegressorData( std::vector<float> & regs) const;

		int getInTarget( const unsigned int & index );
		int getOutTarget( const unsigned int & index );
		      
        void permuteTargetData();
        void permuteTargetData( const std::vector<int> & v_sel_mask );

		unsigned int getNumberOfClasses() ;
		std::vector<unsigned int> getNumberOfSubjectsPerClass() const ;
        std::vector<unsigned int> getNumberOfSubjectsPerClass(const std::vector<int> & v_mask ) const ;
		void clear();
		void calculateClassInfo();
		void setFileName( const std::string & s );	
        void setMaskName( const std::string & s );
        void setTestMaskName( const std::string & s );

        void setRegressorsFileName( const std::string & s );

		void readFile();
		void addClassLabel( const int & cl );

		
		
		//void calculateClassLabelInfo();
		unsigned int getNumberOfSamples() { return misc_utils_name::sum(NperClass_); } 
        unsigned int getNumberOfRegressors();
		std::vector<int> getClassLabels( ) const { return vClasses_; } 
        std::vector<int> getStratifyLabels() const { return strat_labels; }
       
		int getClassLabel( const unsigned int & index );//get label for a given class index
		std::vector<float>::iterator begin();
		std::vector<float>::iterator end();
		void erase( std::vector<float>::iterator & it );

		unsigned int getClassIndex( const int & label ) const ;
		

		std::vector<int>::iterator class_index_begin();
		std::vector<int>::iterator class_index_end();
		
		template<class T>
		void applySelectionMask( const std::vector<T> & selection_mask ,  const std::vector<T> & selection_test_mask, bool clearTestRegressors=true );
		
		//lets get out some stats from the taregt 
		float expected_prediction_error( const std::vector<int3> & estimate ) const ;
		std::vector<float> perclass_expected_prediction_error( const std::vector<int3> & estimate ) const ;
		std::vector<float> perrun_expected_prediction_error( const std::vector< std::vector<int3> > & estimate ) const ;

		
		void prediction_error( const std::vector<int3> & estimate, float & mean, float & std_err ) const ;


        void  printData();
        
	protected:
        //unsigned int seed;//used for permutations
		unsigned int Kclasses;
		std::vector<float> in_target;
      //  std::vector<float> in_target_orig;

		std::vector<float> out_target;
        
         std::vector< std::vector<float> > in_regressors;
         std::vector< std::vector<float> > out_regressors;
        std::vector< std::vector<float> > in_test_regressors;
        std::vector< std::vector<float> > out_test_regressors;
        
        std::vector<int> strat_labels;//which labels to stratify across
		std::vector<int> target_class_index;//stores index where label is stored
		std::vector<unsigned int> NperClass_; //store number of smaple for each class
		std::vector<int> vClasses_; //store class labels
		std::string filename, reg_filename;//, maskname;
        std::vector< std::string > v_masknames;
        std::vector< std::string > v_test_masknames;

        std::string classmap_name;
	private:
		bool FileHasBeenRead, reg_FileHasBeenRead;
		bool hasFileNameBeenSet, reg_hasFileNameBeenSet;
		bool TargetEdited;
		
	};
	
}

#endif
