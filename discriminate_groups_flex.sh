#!/bin/sh

#  discriminate_groups.sh
#  su_mvdisc
#
#  Created by Brian Patenaude on 9/13/11.
#  Copyright 2011 Stanford University - Etkin Lab. All rights reserved.

#This script will create the desired tree given different flags

Usage(){
    echo "discriminate_groups.sh"
    echo "This version allows for the felixibility of the tree based algorithm used to constructed the data"
    echo ""
    echo ""
    echo "--------Discriminant Options-----------"
    echo ""
    echo "-LDA : Use LDA instead of SVM."
    echo "-GP : Use Gaussian Process instead of SVM."
    echo ""
    echo "--------Filter Options-----------"
    echo ""
    echo "***These options are added in this order. If you want more control, manipulate the tree file manually."
echo ""
    echo "-glm_resdiuals <regressors.txt> : Pass on the residuals to of the data" 
echo ""
    echo "-norm_feats <0 or 1>: Normalize feature so that each feature ranges from [-1,+1]. 0 = off, 1 =  on (default=1) "
echo ""
    echo "-glm_thresh <threshold> : Feature Selection based on any pair-group difference(F-test). Theshold is fractional of voxels to retain"
echo ""
    echo "-pca_filter <threshold> : threshold is specified as fraction of total varieance 0<th<1"
echo ""
    echo "--------------Input Options------------------------------"
    echo ""

    echo "-input_mask <mask.nii.gz <0 or 1>: Input mask. Second argument: 0 = no normalization of data, 1 = normalize data"
    echo "                                   before intensity normalization (mean=0,var=1). (Default=0)"
    echo "-i : input data. A series of space spearate 4D images.  1 image for each group."
    echo "-ix : input data to apply discriminant towards but not used to train."
    echo ""
    echo "-------------------Output Options-------------------------"
echo ""
    echo "-o : output data"
    echo "-bagging <0 or 1> : weather to use bagging or not. 0= no. 1=yes. (Default=0)"
echo ""
    echo "----------------------------------------------------------"
echo ""
    echo "-class_map 0,1,2.... (class to map each group into)"
#echo "-stratify_classes 0,1,2.... (class to map each group into)"
echo ""
    echo "---------------------Cross-validation/Sampling Options-------------------------------"
echo ""
    echo "-Nsubsamples <Nsub> <cl> <0,1,2,3...>: Specifies number of subsamples to take from the data. The samples are such that it is sampled evenly from each group."
    echo "-SubsampleOffset <Nsubjects>: Specifies number of samples to exclude from teh minimum group size"
echo ""


    exit 1
}

NGROUPS=0;


if [ $# = 0 ] ; then 
Usage
exit 1
fi

#FIRST TIME THROUGH JUST EXTRACT TARGET,INPUT AND OUTPUT INFO 

while [ $# != 0 ] ; do 

#read in group data 
if [ ${1} = -i ] ; then 
#dont use comma separate values, so that you can use tab-completions on command line
shift 1
echo $1 ${1:0:1}
while [ ! ${1:0:1} = - ] ; do
if [ `${FSLDIR}/bin/imtest $1` = 0 ] ; then 
echo "Invalid Image"
exit 1
fi
GROUP_IMAGES="${GROUP_IMAGES} $1"
let NGROUPS+=1
shift 1
if [ $# = 0 ] ; then 
break
fi
done
echo GROUP_IMAGES $GROUP_IMAGES
elif [ ${1} = -pca_filter ] ; then 
PCA_FILTER=1
PCA_FILTER_THRESH=$2
shift 2
elif [ ${1} = -glm_thresh ] ; then 
GLM_FEATURE_SELECT=1
GLM_FEATURE_SELECT_TH=$2
shift 2
elif [ ${1} = -glm_residuals ] ; then 
shift 1
echo $1 ${1:0:1}
while [ ! ${1:0:1} = - ] ; do
if [ ! -f $1 ] ; then 
echo "File does not exist: $1"
exit 1
fi
ORIG_REG_FILES="${ORIG_REG_FILES} $1"               
shift 1
if [ $# = 0 ] ; then 
break
fi
done
echo ORIG_REG_FILES $ORIG_REG_FILES
elif [ ${1} = -norm_feats ] ; then 
NORM_FEATS=$2
shift 2
elif [ ${1} = -input_mask ] ; then 
INPUT_MASK=$2
INPUT_MASK_PRE=$3
shift 3
elif [ ${1} = -o ] ; then 
OUTPUTDIR=$2
shift 2
elif [ ${1} = -class_map ] ; then 
MAPPED_CLASSES=$2
shift 2
elif [ ${1} = -SubsampleOffset ] ; then 
MINOFFSET=$2
shift 2
elif [ ${1} = -Nsubsamples ] ; then 
NSUBSAMPLES=$2
STRAT_CLASSES=$3
shift 3
elif [ ${1} = -bagging ] ; then
if [ $2 = 1 ] ; then 
BAGGING_ARGS="--useBagging"
fi
shift 2
elif [ ${1} = -LDA ] ; then
USE_LDA=1
shift 1
elif [ ${1} = -GP ] ; then
USE_GP=1
shift 1
else
echo "Unrecognized option: ${1}"
Usage
exit 1
fi

done



USE_LDA=0;
USE_GP=0;

OUTPUTDIR="output"

#PCA filter
PCA_FILTER=0
PCA_FILTER_THRESH=0
#NODE ON/OFF SWITCH+PAPRAMETERS
#GLM filter (for feature selection)
GLM_FEATURE_SELECT=0
GLM_FEATURE_SELECT_TH=0

#Normlalization of features
NORM_FEATS=1

LeaveN=1
KFOLD=0

#Image Source Attrributes
INPUT_MASK=""
INPUT_MASK_PRE=0
MAPPED_CLASSES=""
STRAT_CLASSES=""
BAGGING_ARGS=""

#nuissance regressors to remove from input data.
ORIG_REG_FILES=""
#Dicriminate exceutbale parameters (mostly related to cross-validation)
NSUBSAMPLES=0
MINOFFSET=0
# if zero will run standard cross-validation
echo "NSUBSAMPLES $NSUBSAMPLES"

#while [ _${1:0:1} = _- ] ; do 
while [ $# != 0 ] ; do 

    #read in group data 
    if [ ${1} = -i ] ; then 
#dont use comma separate values, so that you can use tab-completions on command line
        shift 1
        echo $1 ${1:0:1}
        while [ ! ${1:0:1} = - ] ; do
            if [ `${FSLDIR}/bin/imtest $1` = 0 ] ; then 
                echo "Invalid Image"
                exit 1
            fi
            GROUP_IMAGES="${GROUP_IMAGES} $1"
            let NGROUPS+=1
            shift 1
            if [ $# = 0 ] ; then 
                break
            fi
        done
        echo GROUP_IMAGES $GROUP_IMAGES
    elif [ ${1} = -pca_filter ] ; then 
            PCA_FILTER=1
            PCA_FILTER_THRESH=$2
            shift 2
    elif [ ${1} = -glm_thresh ] ; then 
        GLM_FEATURE_SELECT=1
        GLM_FEATURE_SELECT_TH=$2
        shift 2
    elif [ ${1} = -glm_residuals ] ; then 
        shift 1
        echo $1 ${1:0:1}
        while [ ! ${1:0:1} = - ] ; do
            if [ ! -f $1 ] ; then 
                echo "File does not exist: $1"
                exit 1
            fi
            ORIG_REG_FILES="${ORIG_REG_FILES} $1"               
        shift 1
        if [ $# = 0 ] ; then 
            break
        fi
        done
        echo ORIG_REG_FILES $ORIG_REG_FILES
    elif [ ${1} = -norm_feats ] ; then 
        NORM_FEATS=$2
        shift 2
    elif [ ${1} = -input_mask ] ; then 
        INPUT_MASK=$2
        INPUT_MASK_PRE=$3
        shift 3
    elif [ ${1} = -o ] ; then 
        OUTPUTDIR=$2
        shift 2
    elif [ ${1} = -class_map ] ; then 
        MAPPED_CLASSES=$2
        shift 2
   elif [ ${1} = -SubsampleOffset ] ; then 
        MINOFFSET=$2
        shift 2
    elif [ ${1} = -Nsubsamples ] ; then 
        NSUBSAMPLES=$2
        STRAT_CLASSES=$3
        shift 3
    elif [ ${1} = -bagging ] ; then
        if [ $2 = 1 ] ; then 
            BAGGING_ARGS="--useBagging"
        fi
        shift 2
elif [ ${1} = -LDA ] ; then
USE_LDA=1
shift 1
else
        echo "Unrecognized option: ${1}"
        Usage
        exit 1
    fi

done

echo NSUBSAMPLES $NSUBSAMPLES

#create output director
while [ -d ${OUTPUTDIR}.disc ] ; do
    OUTPUTDIR="${OUTPUTDIR}+"
done
OUTPUTDIR="${OUTPUTDIR}.disc"
/bin/mkdir ${OUTPUTDIR}





#########################
#Files taht will output 
#A general information file
INFO_FILE=${OUTPUTDIR}/info.txt
echo "Discriminant Information File" >  $INFO_FILE

#target file (i.e. class labels)
TARGET_FILE=${OUTPUTDIR}/target.txt 

#group map file
GROUP_MAP=${OUTPUTDIR}/group_map.txt

#CLASS MAP
CLASS_MAP=${OUTPUTDIR}/class_map.txt
INTERP_MAP=${OUTPUTDIR}/interp_class_map.txt
SENS_FILE=${OUTPUTDIR}/sens.txt
#disriminant tree
TREE_FILE=${OUTPUTDIR}/tree.txt
LOG_FILE=${OUTPUTDIR}/run.log
REG_FILE=${OUTPUTDIR}/regressors.txt

echo "#The Discriminant tree was created using discriminate_group.sh" >> $TREE_FILE

#######################



#main running part 




#lets build the tree

echo $GROUP_IMAGES
echo $NGROUPS groups

echo "Creating Class Labels Files"
echo "Number of Groups : ${NGROUPS}" >> $INFO_FILE
echo "Input Group Images: ${GROUP_IMAGES}">> $INFO_FILE
echo "GroupImage ClassLabel NumberOfSamples" > $GROUP_MAP
NperGroup=""

#lets start at 100, makes mapping easier
group=100;
class_map=0
class_map_index=0
for gr in $GROUP_IMAGES ; do
    echo "group $gr $group"
    index=$group;
    nsubs=`${FSLDIR}/bin/fslnvols $gr`
    NperGroup="${NperGroup} $nsubs"
    
    #Create target files
    count=0    
    while [ $count -lt $nsubs ] ; do 

        echo $group >> $TARGET_FILE
        let count+=1
    done
    
    #maps classes into 0->N
    if [ ! "${MAPPED_CLASSES}_" = "_" ] ; then
        #indexing into awk starts at 1 
        if [ $class_map_index = 0 ] ; then 
            let class_map_index+=1
        fi
        class_map=`echo $MAPPED_CLASSES | awk -F , "{ print \$$class_map_index }"`
#need to redirect into file, doesn't work wihtin ``
        echo $MAPPED_CLASSES | awk -F , "{ print \$$class_map_index }" > ${OUTPUTDIR}/grot
        class_map=`cat ${OUTPUTDIR}/grot`
        /bin/rm ${OUTPUTDIR}/grot
    else
        class_map=$class_map_index
    fi

    echo "${group} ${class_map}" >> $CLASS_MAP

    echo "${gr} ${group} ${nsubs}" >> $GROUP_MAP

    let class_map_index+=1
    let group+=1
done






DISCOUT_DIR=${OUTPUTDIR}/Leave-${LeaveN}-Out

echo "Number of Samples per Group : ${NperGroup}" >> $INFO_FILE

#lets process target arguments
#some are blank until option is added
CLASS_MAP_ARGS="class_map=${CLASS_MAP}"
if [ "${STRAT_CLASSES}_" = "_" ] ; then 
    STRATIFY_ARGS="StratifyLabels=0,1"
else
    STRATIFY_ARGS="StratifyLabels=${STRAT_CLASSES}"
fi

#computer whether to regress out variables of no interest
REGRESSOR_ARGS=""
if [ ! "${ORIG_REG_FILES}_" = "_" ] ; then 
    Nreg_files=`echo $ORIG_REG_FILES | wc | awk '{ print $2 }'`
    N_images=`echo $GROUP_IMAGES | wc | awk '{ print $2 }'`
    
#check to make sure that regressors were given for each group, these should not be correlated
    echo "nregs ${Nreg_files}"
    if [ ! $Nreg_files = $N_images ] ; then
        echo "Error : Number of Input Images is not equal to number of regressor files"
        exit 1
    fi
#check to make sure the number per group is the same
    c=1
    for gr in $GROUP_IMAGES ; do
        Nim=`${FSLDIR}/bin/fslnvols $gr`
        file=`echo $ORIG_REG_FILES | awk '{ print $1 }'`
        Nregs=`wc  $file | awk '{ print $1 }'`
        if [ ! $Nreg_files = $N_images ] ; then
            echo "Error : Number of Subjects in Regresors and data do not match"
            exit 1
        fi


        let c+=1
    done
    cat $ORIG_REG_FILES > ${REG_FILE}
    REGRESSOR_ARGS="regressors=${REG_FILE}"
fi 


NODE=0
NODE_PREV=0
#create tree target
echo "ClassLabels   Target  filename=$TARGET_FILE $CLASS_MAP_ARGS $STRATIFY_ARGS $REGRESSOR_ARGS" >> $TREE_FILE

#add in discirminant

if [ $USE_LDA = 1 ] ; then 
    echo "node${NODE}   LDA	None Verbose=false" >> $TREE_FILE
    echo "Discriminant Used : LDA ">> $INFO_FILE
elif [ $USE_GP = 1 ] ; then 
echo "node${NODE}   Gaussian_Process	None Verbose=false Mode=LSClassifier" >> $TREE_FILE
echo "Discriminant Used : Gaussian_Process ">> $INFO_FILE
else 
echo "node${NODE}   SVM	None svm_type=C-SVC kernel_type=linear Verbose=false TrackStats=YES Output_Name=${DISCOUT_DIR}/SVM_lin_weights" >> $TREE_FILE
echo "Discriminant Used : SVM ">> $INFO_FILE

fi
let NODE+=1
##---------END DISCRIMINANT PORTION-------------------


    if [ $PCA_FILTER = 1 ] ; then 

        echo "Using PCA for Dimensionality Reduction: Yes" >>$INFO_FILE
        echo "Fraction of variance retained by the PCA: $PCA_FILTER_THRESH " >>$INFO_FILE

        echo "node${NODE}		PCA_Filter		node${NODE_PREV}	threshold=${PCA_FILTER_THRESH} " >> $TREE_FILE
        let NODE+=1
        let NODE_PREV+=1
    else
        echo "Using PCA for Dimensionality Reduction: No" >>$INFO_FILE
    fi



    #GLM FILTER 
    if [ $GLM_FEATURE_SELECT = 1 ] ; then 
        echo "Using univariate GLM-based feature selection: Yes" >>$INFO_FILE
        echo "GLM-based feature selection: ${GLM_FEATURE_SELECT_TH} (include everything above this percentile)" >>$INFO_FILE

        echo "node${NODE}		GLM_Filter			node${NODE_PREV}	threshold=${GLM_FEATURE_SELECT_TH} TrackStats=YES threshold_mode=OrderStatistic Output_Name=${DISCOUT_DIR}/GLM_feature_select " >> $TREE_FILE
        let NODE+=1
        let NODE_PREV+=1
    else
        echo "Using univariate GLM-based feature selection: No" >>$INFO_FILE
    fi





#NORMALIZATION
if [ $NORM_FEATS = 1 ] ; then 
    echo "Normalization each feature : Yes" >>$INFO_FILE

    echo "node${NODE}	NormalizeFilter	node${NODE_PREV}  normMode=MinMax_AS " >> $TREE_FILE
    let NODE+=1
    let NODE_PREV+=1
else
    echo "Normalization each feature : No" >>$INFO_FILE
fi

#REGRESSORS
if [ ! "${ORIG_REG_FILES}_" = "_" ] ; then 
    echo "Regress out covariates : Yes" >>$INFO_FILE
    echo "Original Regressor file : ${ORIG_REG_FILES}" >>$INFO_FILE
    echo "node${NODE}	GLM_Filter	node${NODE_PREV}  filter_mode=Residuals Verbose=true" >> $TREE_FILE
    let NODE+=1
    let NODE_PREV+=1
else
    echo "Regress out covariates : No" >>$INFO_FILE
fi



#GroupSelect			GroupSelect_Filter	NormAS 				mode=INCLUDE groups=0,1 

#lets handle the image source + masks 
${FSLDIR}/bin/fslmerge -t ${OUTPUTDIR}/training_data $GROUP_IMAGES

#input data 
MASK_ARGS=""
if [ ! "${INPUT_MASK}_" = "_" ] ; then     
    MASK_ARGS="${MASK_ARGS} external_mask=${INPUT_MASK} Normalize_Pre_Mask=${INPUT_MASK_PRE}"
fi


   
 
echo "node${NODE}		ImageSource	node${NODE_PREV} filename=${OUTPUTDIR}/training_data ${MASK_ARGS} mask_mode=INTERSECTION">> $TREE_FILE


mkdir ${DISCOUT_DIR}/
echo NSUBSAMPLES $NSUBSAMPLES
if [ $NSUBSAMPLES -gt 0 ] ; then 
    echo "Using Even Group Subsampling: Yes " >> $INFO_FILE
    echo "Strafying across $STRAT_CLASSES." >> $INFO_FILE
    echo "Samples size is $MINOFFSET less than minimum group size." >> $INFO_FILE
else
    echo "Using Even Group Subsampling: No " >> $INFO_FILE
fi


echo "${ETKINLAB_DIR}/bin/discriminate -i $TREE_FILE -o ${DISCOUT_DIR}/cv_results_disc --minimumOffset=${MINOFFSET} --Nsubsamples=${NSUBSAMPLES} --sens_spec_class_inclusion=${CLASS_MAP} --LeaveNout=${LeaveN}" > $LOG_FILE 

${ETKINLAB_DIR}/bin/discriminate -i $TREE_FILE -o ${DISCOUT_DIR}/cv_results_disc --minimumOffset=${MINOFFSET} --Nsubsamples=${NSUBSAMPLES} --sens_spec_class_inclusion=${CLASS_MAP} --LeaveNout=${LeaveN}

#mkdir ${OUTPUTDIR}/Leave-${LeaveN}-Out/summary
####----------------INTERPRET EXISITING RAW DATA, at this point need this to do bagging-------------------###
#create class map for interpreter, need to get labels that were in original and map onto itself

for i in `cat ${CLASS_MAP} | awk '{ print $2 }'` ; do 
    echo "INTERPMAP " $i 
    EXIST=0
    for j in `cat ${CLASS_MAP} | awk '{ print $1 }'` ; do 
        if [ $j = $i ] ; then 
            EXIST=1
        fi
    done
#included class that are not in the first column for sensitivity calcualtions. This assumes, that classes of ionterests are never in the first colums
        if [ $EXIST = 0 ] ; then
            echo $i $i >> $INTERP_MAP
            echo "$i 1">> $SENS_FILE
        #Calculation are done after mapping classes
        else 
            echo "$i 0">> $SENS_FILE
        fi
done
#go back and add existing ones that exist but aren't included
#for i in `cat ${CLASS_MAP} | awk '{ print $1 }'` ; do 
#     echo "$i 0">> $SENS_FILE
#done




/bin/cat $INTERP_MAP $CLASS_MAP > ${OUTPUTDIR}/temp
/bin/mv ${OUTPUTDIR}/temp $INTERP_MAP

#this defines which classes are used for sensitivityspeciificty calcualtion]

echo "${ETKINLAB_DIR}/bin/discriminate_interpreter -i ${DISCOUT_DIR}/cv_results_disc_per_subject_stats_predX.csv -o ${DISCOUT_DIR}.results --class_map=$INTERP_MAP --sens_spec_class_inclusion=$SENS_FILE" >> $LOG_FILE
${ETKINLAB_DIR}/bin/discriminate_interpreter -i ${DISCOUT_DIR}/cv_results_disc_per_subject_stats_predX.csv -o ${DISCOUT_DIR}.results --class_map=$INTERP_MAP --sens_spec_class_inclusion=$SENS_FILE $BAGGING_ARGS

#-v -o summary_results/${fmap}/{output}.results -i ${fmap}/${output}.disc/${Kfold}-Fold_runs_${RUNS}_subsampling_th${th}_per_subject_stats_predX.csv --class_map=${class_map} --useBagging --sens_spec_class_inclusion=sens.txt | grep Acc/sensitivity/ | sed 's/Acc\/sensitivity\/specificity//g' | sed 's/ /,/g'`,`~/src/etkinlab_utils/src/su_mvdisc/build/Release/discriminate_interpreter  -v -o summary_results/${fmap}/{output}.results -i ${fmap}/${output}.disc/${Kfold}-Fold_runs_${RUNS}_subsampling_th${th}_per_subject_stats_predX.csv --class_map=${class_map} --useBagging --sens_spec_class_inclusion=sens.txt | grep Accu | sed 's/Accuracies,//g'`  >> $OUTPUT

