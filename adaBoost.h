//
//  adaBoost.h
//  su_mvdisc
//
//  Created by Natasha Parikh on 6/25/12.
//  Copyright (c) 2012 Stanford University - Etkin Lab. All rights reserved.
//

#ifndef su_mvdisc_adaBoost_h
#define su_mvdisc_adaBoost_h 1

#include <ClassificationTreeNode.h>
#include <sstream>
#include <base_mvdisc.h>

namespace su_mvdisc_name{
    
    class Ada_Boost : public ClassificationTreeNode
	{
	public:
        
        // constructor and destructor
		Ada_Boost();
		virtual ~Ada_Boost();
        
        // setOptions
        // takes user input and sets the type of classifier, number of 
        // iterations, and other input options 
		void setOptions( std::stringstream & ss_options);
        
        // runNode
        // runs the tree up till the current node, prepares this node, 
        // and calls private member functions estimate parameters 
        // (if bool is true) and classify test data
		void runNode(bool with_estimation = true);
        
        // setMIterations
        // a way for the user to change the number of weighted
        // iterations Ada Boost will complete before ending
        void setMIterations(int M);
        
	private:
        // makes it easier to manipulate classifier type
		enum Node_Type { nt_Unspecified, nt_LDA, nt_QDA, nt_LR, nt_SVM };
        
        // estimateParameters
        // use the training data to perform several weighted 
        // classifications and fit alpha parameters to each. also,
        // determine the final training classifications
        void estimateParameters();
        
        // classifyTestData
        // using the alphas for each discriminant classifier, find
        // the test classifications
        int classifyTestData();
		
        std::stringstream node_options;         
        Node_Type discriminant_t;               // stores the type of classifier being used
        bool univariateClassifier_;             // toggle for univariate classifiers vs. the entire dataset
        int mRuns_;                             // the number of iterations of Ada Boost
        std::vector<float> alphas_;             // save the alpha values for each classifier 
        std::vector< base_mvdisc* > discObjs_;  // save the associated classifiers as well
        std::vector<int> bestDimensionVec_;     // save the features selected from each iteration
	};
    
}


#endif // su_mvdisc_adaBoost_h
