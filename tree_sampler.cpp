//
//  tree_sampler.cpp
//  su_mvdisc
//
//  Created by Brian M. Patenaude on 4/27/12.
//  Copyright 2012 Stanford University - Etkin Lab. All rights reserved.
//
#include "tree_sampler.h"

//STL includes
#include <string.h>
#include <cstdlib>
#include <stdio.h>
#include <set>
#include <limits>
#include <iostream>
#include <fstream>
//#include <assert.h>     /* assert */

//su_mvdisc includes
#include "misc_utils.h"
#include <clapack_utils.h>
using namespace std;
using namespace clapack_utils_name;
//using namespace su_mvdisc_name;
using namespace misc_utils_name;
namespace su_mvdisc_name{
    
    //constructor
    TreeSampler::TreeSampler(){
        //this is the main tree
        gaia_=nullptr;
        //don't output information to the terminal
        Verbose=false;
        //thresholds are used at post procesing to segment results 
        tr_acc_thresh_=0.0;
        n_tr_acc_thresh_=0.0;
        
    }
    
    TreeSampler::~TreeSampler(){
        //Delete the classification tree if it exists
        if (gaia_!= nullptr){
            delete gaia_;
            gaia_=nullptr;
        }    
    }
    
    //Load the configuration file specified by the filename
    void TreeSampler::loadTree( const std::string & filename ){
        //Calls the Classification Tree constructor with the file name.
        gaia_=new ClassificationTree(filename);
        
    }
    
    //This sets the tree by passing in a previously contructed object
    void TreeSampler::setTree( ClassificationTree* tree ){
        
        if (Verbose>=2)	cout<<"Entering TreeSampler::setTree "<<endl;
        
        //set tree to that specified
        gaia_=tree;
        
        //Do a check to make sure the number of samples in the source equal that in the target.
        gaia_->check_Ns_Targ_eq_Ns_Src();
                
        if (Verbose>=2)	cout<<"Leaving TreeSampler::setTree - Number of Samples match between predictors and outcomes."<<endl;
        
    }
    
    //Take vector of selection masks (binary vectors) and apply cross-valildation to create
    //a new set of masks. i.e. It performs (creates a vector of vinary vectors) cross-
    //validation for each set of selected sample specified in sel_masks. K-fold secifies the
    //K-fold cross-validation to use. Special case include:
    //Chunk specfies samples that should be tied together. e.g. in 4D timeseries.
    vector< vector<int> > TreeSampler::applyCrossValidation( const vector< vector<int> > & sel_masks, const unsigned int & Kfold, const vector<unsigned int> & chunks ){
        //Kfold does NOT allow for multiple sample Leave-One-Out does not currently
        //for LOO (Kfold=1) needs to be binary
        // Kfold == 0, does not chnage the input, return the input.
        //        cerr<<"TreeSampler::applyCrossValidation "<<Kfold<<endl;
        //        cerr<<"TreeSampler::applyCrossValidation sel_masks1 "<<sel_masks.size()<<endl;
        //        cerr<<"TreeSampler::applyCrossValidation sel_masks2 "<<sel_masks[0].size()<<endl;
        //        print(sel_masks[0],"selection mask");
        //        print(chunks,"chunks");
        
        //Checks to see whether chucks are present in the data.
        bool useChunks = (chunks.empty()) ? false : true;
        if (Kfold == 0) return sel_masks; //return what was entered if K=0 (i.e. no CV)
        
        //Seed the randomization ate a fine scale so that you do not see repeat use of seeds
        srand_fine();
        
        //lets figure out how many there will be in all
        
        //Define the numberer of samples. It is assumed that the length of each selection vector (number of samples)
        //are all equal.
        unsigned int Nsamps=0;
        if (!sel_masks.empty()) //make sure there are selection vectors
            Nsamps=sel_masks[0].size();
        
        //cv_total stores how many CV iterations exists
        unsigned int cv_total=0;
        
        //Handles the generation of the samples (binary vectors) for chunks separately. Once the binary vectors
        //are setup the operation is the same.
        
        //Find and store all the chunks in the data
        set<unsigned int> chunks_set;
        if (useChunks)
            for (vector<unsigned int>::const_iterator i_ch = chunks.begin(); i_ch != chunks.end(); ++i_ch)
                chunks_set.insert(*i_ch);

        //Chunks group data together. For example, Leave-One-Out will be a Leave-One-Chunk-Out
        if (useChunks)
        {
            if (Kfold == 1 )
            {   //for each input, the number of samples created equal to the number of chunks
                //apply K-fold for each sample
                for (vector< vector<int> >::const_iterator i_s = sel_masks.begin(); i_s != sel_masks.end();++i_s)
                {
                    cv_total+=chunks_set.size(); //Accumulating the number of samples
                }
            }else{
                //for each input, K-fold generates K samples.
                //This does not differ from non-chunck version below
                for (vector< vector<int> >::const_iterator i_s = sel_masks.begin(); i_s != sel_masks.end();++i_s)
                {
                    cv_total+=Kfold; //accumulating the number of samples
                }
            }
        }else{
            
            if (Kfold == 1 )
            {//for each input, Leave-One-Out generates N samples
                for (vector< vector<int> >::const_iterator i_s = sel_masks.begin(); i_s != sel_masks.end();++i_s) //apply K-fold for each sample
                {
                    cv_total+=sum(*i_s);
                }
            }else{
                //for each input, K-fold generates K samples.
                for (vector< vector<int> >::const_iterator i_s = sel_masks.begin(); i_s != sel_masks.end();++i_s) //apply K-fold for each sample
                {
                    cv_total+=Kfold;
                }
            }
            
        }
        
        //v_sel_masks_cv is the new set samples. It is the augmented version of v_sel_masks.
        //Augmented, in that a cross-validation has been applied.
        //Initialize data structure to store all samples.
        vector< vector<int> > v_sel_masks_cv(cv_total,vector<int>(Nsamps,1));
        //The code will now populate the CV samples
        //The CV allows for chunking of data. i.e. data the should be grouped together
        //in the CV. En example, all time points for a single subject in an fMRI acquisition
                 	
        if (useChunks)
        {//Case for chunking i.e. grouping of dimensions.
            if (Kfold == 1 )//Special Leave-One-Out case with chunks, assumes binary
            {
                //Apply K-fold for each chunk, 
                vector< vector<int> >::iterator i_s_cv = v_sel_masks_cv.begin();
                for (vector< vector<int> >::const_iterator i_s = sel_masks.begin(); i_s != sel_masks.end();++i_s)
                {
                    unsigned int N = chunks_set.size(); //count number of samples
                    set<unsigned int>::iterator i_sch = chunks_set.begin();
                    for ( unsigned count = 0 ; count < N ; ++count,++i_s_cv,++i_sch) //toggle off each sample and push back
                    {
                        {//for each chunk
                            //                        cerr<<"count "<<count<<" "<<Nsamps<<" "<<i_s_cv->size()<<" "<<i_s->size()<<endl;
                            //                        print(*i_s_cv,"copy base ");
                            memcpy(&(i_s_cv->at(0)), &(i_s->at(0)), Nsamps*sizeof(int));// starts as copy of base
                            //   unsigned int count2=0; //keeps track of
                            //go through each sample and set to zero (alternating)
                            
                            vector<unsigned int>::const_iterator i_c = chunks.begin();
                            for (vector<int>::iterator ii_s_cv = i_s_cv->begin(); ii_s_cv != i_s_cv->end(); ++ii_s_cv,++i_c)
                            {
                                if (*i_c == *i_sch )
                                    *ii_s_cv=0;
                            }
                        }
                        
                    }
                    
                    
                }
                
            }else{
            	//Do generic K-fold cross-validation
             
            	vector< vector<int> >::iterator i_s_cv = v_sel_masks_cv.begin();
                for (vector< vector<int> >::const_iterator i_s = sel_masks.begin(); i_s != sel_masks.end();++i_s)
                {
                    unsigned int N = sum(*i_s);///accomdate left out ad multiple smaples
                    //                cerr<<"Samples "<<N<<" "<<i_s->size()<<endl;
                    vector<unsigned int> NperGroup(Kfold,static_cast<unsigned int>(N/Kfold));
                    unsigned int Nrem=N%Kfold;
                    {
                        unsigned int count=0;
                        while (Nrem > 0)
                        {
                            ++NperGroup[count];
                            --Nrem;
                        }
                    }
                    //   cerr<<"initializa sample "<<endl;
                    //inititialize samples
                    list<unsigned int> * samples = new list<unsigned int>(N,0);
                    {
                        unsigned int count=0;
                        
                        list<unsigned int>::iterator i_samp = samples->begin();//this is incremenented in the lopp
                        for (vector<int>::const_iterator i_sel = i_s->begin(); i_sel != i_s->end();++i_sel,++count)  //initialize samples than remove
                        {
                            if (*i_sel>0)
                            {
                                *i_samp=count;
                                ++i_samp;//increments sample spot, but not index
                                
                            }
                        }
                    }
                    //print(samples,"samples");
                    //cout<<"done sample initialization "<<endl;
                    //actually smaple groups
                    //Npergroup is numbver per left out group
                    for (unsigned int i = 0 ; i < Kfold ; ++i,++i_s_cv)//increament output as well
                    {
                        // cout<<"Group "<<i<<endl;
                        //                    vector<int> sel_mask_one = vector<int>(i_s->size(),1);
                        while (NperGroup[i]>0)
                        {
                            unsigned int rsamp = rand() % samples->size();
                            list<unsigned int>::iterator i_stemp=samples->begin();
                            advance(i_stemp, rsamp);
                            //                        sel_mask_one[*i_stemp]=0;
                            i_s_cv->at(*i_stemp)=0;
                            
                            //++sel_mask_one[*i_stemp];
                            samples->erase(i_stemp);
                            --NperGroup[i];
                        }
                        //                    v_sel_masks_cv.push_back(sel_mask_one);
                        
                    }
                    
                    delete samples;
                    
                }
                //            cerr<<"done genereic cross-validation "<<endl;
            }
        }else{
            if (Kfold == 1 )//special LOO case, assumes binary
            {
                //            cerr<<"TreeSampler::applyCrossValidation :: LOO CASE"<<endl;
                vector< vector<int> >::iterator i_s_cv = v_sel_masks_cv.begin();
                for (vector< vector<int> >::const_iterator i_s = sel_masks.begin(); i_s != sel_masks.end();++i_s) //apply K-fold for each sample
                {
                    unsigned int N = sum(*i_s); //count number of samples
                    for ( unsigned count = 0 ; count < N ; ++count,++i_s_cv) //toggle off each sample and push back
                    {
                        //                    cerr<<"count "<<count<<" "<<Nsamps<<" "<<i_s_cv->size()<<" "<<i_s->size()<<endl;
                        memcpy(&(i_s_cv->at(0)), &(i_s->at(0)), Nsamps*sizeof(int));// starts as copy of base
                        unsigned int count2=0; //keeps track of
                        //go through each sample and set to zero (alternating)
                        for (vector<int>::iterator ii_s_cv = i_s_cv->begin(); ii_s_cv != i_s_cv->end(); ++ii_s_cv)
                        {
                            if (*ii_s_cv ==1 ) {//set smaple to zero if it is its turn
                                if (count == count2)
                                {
                                    *ii_s_cv=0;
                                    break;
                                }
                                ++count2;
                            }
                        }
                    }
                    
                    
                }
                
            }else{//do generic K-fold cross-validation
                
                //            cerr<<"do egenric cross-validation"<<endl;
                vector< vector<int> >::iterator i_s_cv = v_sel_masks_cv.begin();
                
                for (vector< vector<int> >::const_iterator i_s = sel_masks.begin(); i_s != sel_masks.end();++i_s)
                {
                    unsigned int N = sum(*i_s);///accomdate left out ad multiple smaples
                    //                cerr<<"Samples "<<N<<" "<<i_s->size()<<endl;
                    vector<unsigned int> NperGroup(Kfold,static_cast<unsigned int>(N/Kfold));
                    unsigned int Nrem=N%Kfold;
                    {
                        unsigned int count=0;
                        while (Nrem > 0)
                        {
                            ++NperGroup[count];
                            --Nrem;
                        }
                    }
                    //   cerr<<"initializa sample "<<endl;
                    //inititialize samples
                    list<unsigned int> * samples = new list<unsigned int>(N,0);
                    {
                        unsigned int count=0;
                        
                        list<unsigned int>::iterator i_samp = samples->begin();//this is incremenented in the lopp
                        for (vector<int>::const_iterator i_sel = i_s->begin(); i_sel != i_s->end();++i_sel,++count)  //initialize samples than remove
                        {
                            if (*i_sel>0)
                            {
                                *i_samp=count;
                                ++i_samp;//increments sample spot, but not index
                                
                            }
                        }
                    }
                    //print(samples,"samples");
                    //cout<<"done sample initialization "<<endl;
                    //actually smaple groups
                    //Npergroup is numbver per left out group
                    for (unsigned int i = 0 ; i < Kfold ; ++i,++i_s_cv)//increament output as well
                    {
                        // cout<<"Group "<<i<<endl;
                        //                    vector<int> sel_mask_one = vector<int>(i_s->size(),1);
                        while (NperGroup[i]>0)
                        {
                            unsigned int rsamp = rand() % samples->size();
                            list<unsigned int>::iterator i_stemp=samples->begin();
                            advance(i_stemp, rsamp);
                            //                        sel_mask_one[*i_stemp]=0;
                            i_s_cv->at(*i_stemp)=0;
                            
                            //++sel_mask_one[*i_stemp];
                            samples->erase(i_stemp);
                            --NperGroup[i];
                        }
                        //                    v_sel_masks_cv.push_back(sel_mask_one);
                        
                    }
                    
                    delete samples;
                    
                }
                //            cerr<<"done genereic cross-validation "<<endl;
            }
        }
        //                cerr<<"v_sel_masks_cv "<<Kfold<<" "<<v_sel_masks_cv.size()<<" "<<cv_total<<endl;
        //                cerr<<"done apply cross validation "<<endl;
        //                for (vector< vector<int> >::iterator i = v_sel_masks_cv.begin() ; i != v_sel_masks_cv.end();++i)
        //                    print(*i,"sel_masks_cv");
        //                cerr<<"done cross-validation Kfold : "<<Kfold<<" "<<Nsamps<<endl;
        return v_sel_masks_cv;
    }
    
 
    void TreeSampler::postProcess( const std::vector<int> & v_mask, const bool & do_bagging,const bool & weightBagByProb, TreeSampleData & tsamp_data  , const std::string & output_name )
    {
        //      fselect.close();
        //output
        //lets look at per subsample accuracy
        //cout<<"Do Post Processing of Data... "<<tsamp_data.genData_.size()<<endl;
        
        if (Verbose)
        {
            cout<<"Training estimates : "<<tsamp_data.estimates_train_all.size()<<endl;
        }
        //        cerr<<"Do Post Processing of Data..."<<endl;
        
        bool doWrite= (output_name.empty())? 0 : 1;
        
        int sample_group=0;
        unsigned int TP(0),FP(0),FN(0),TN(0);
        
        float accuracy_pergroup=0.0;
        unsigned int Npred = 0 ;
        //group refers to sample group
        //this calculates the training accuracy per run
        for (vector<int4>::const_iterator i_tpred = tsamp_data.estimates_train_all.begin(); i_tpred != tsamp_data.estimates_train_all.end();++i_tpred)
        {
            
            if ( sample_group == i_tpred->z)
            {
                int targ=gaia_->getTargetData(i_tpred->x);//stored as variable for looking at TP<FNetc later
                
                if (i_tpred->y ==  targ)
                    ++accuracy_pergroup;
                ++Npred;
                
            }else{
                tsamp_data.l_accuracy_pergroup.push_back( pair<float,unsigned int> (accuracy_pergroup/Npred, sample_group) );
                //cout<<"Group Accuracy "<<sample_group<<" "<<accuracy_pergroup<<" "<<Npred<<" "<<accuracy_pergroup/Npred<<endl;
                sample_group = i_tpred->z;//next sample
                
                accuracy_pergroup=0;
                Npred=0;
                TP=FP=FN=TN=0;
                --i_tpred;//don't use until in next group
            }
        }
        //need to push back the final one
        //cout<<"Group Accuracy "<<sample_group<<" "<<accuracy_pergroup<<" "<<Npred<<" "<<accuracy_pergroup/Npred<<endl;
        
        tsamp_data.l_accuracy_pergroup.push_back( pair<float,unsigned int> (accuracy_pergroup/Npred, sample_group) );
        
        //  cout<<"Do Post Processing of Data1..."<<endl;
        
        int2 base;
        base.x=0;
        base.y=0;
        vector<int2> within_bagged( gaia_->getNumberOfSamples(), base );
        vector<float> within_bagged_perc( gaia_->getNumberOfSamples() );
        
        //within group bagging
        {
            //x is the count, y is the number of votes
            for (vector<int4>::const_iterator i_est = tsamp_data.estimates_train_all.begin(); i_est != tsamp_data.estimates_train_all.end(); ++i_est)
            {
                //increment the count for that subject
                ++within_bagged[i_est->x].x;
                //add the vote 0/1 for that subject
                within_bagged[i_est->x].y+=i_est->y;
                //add the vote 0/1 for that subject
                within_bagged_perc[i_est->x]+=i_est->y;
                //cout<<"outputbagged "<<i_est->x<<" "<<i_est->y<<" "<<i_est->z<<endl;
            }
            
            unsigned int count=0;
            vector<float>::iterator i_b2 = within_bagged_perc.begin();
            for (vector<int2>::iterator i_b = within_bagged.begin(); i_b != within_bagged.end(); ++i_b,++i_b2,++count)
            {
                //cout<<count<<" "<<i_b->x<<" "<<i_b->y<<endl;
                i_b->y = ( i_b->y > (i_b->x/2.0)) ? 1 : 0 ;
                *i_b2 /= i_b->x;
                
                //cout<<"voted "<<i_b->y<<endl;
            }
            
        }
        
        //cout<<"Do Post Processing of Data2..."<<endl;
        
        ///----------------------LETS WRITE OUT TRAINING PREDICTIONS ----------------------
        //process clusters as well as predictions
        
        
        vector<int> train_target = gaia_->getTargetData<int>();
        
        vector< vector< pair<int,int> > > train_all_transpose(gaia_->getNumberOfSamples());
        vector< vector< pair<int,int> > > train_clusters_all_transpose(gaia_->getNumberOfSamples());
        cout<<"training estimates "<<tsamp_data.estimates_train_all.size()<<endl;
        int colmax=0;
        for (vector<int4>::const_iterator i_est = tsamp_data.estimates_train_all.begin(); i_est != tsamp_data.estimates_train_all.end(); ++i_est)
        {
            train_all_transpose[i_est->x].push_back( pair<int, int>(i_est->w,i_est->y) );
            train_clusters_all_transpose[i_est->x].push_back( pair<int, int>(i_est->w,i_est->z) );
            if (i_est->w > colmax) colmax = i_est->w;
        }
        
        
        if ((doWrite )&& (tsamp_data.writeLevel>1))
        {
            //        cerr<<"open file "<<(output_name+"_training_pred.csv")<<endl;
            
            
            ofstream ftrain_all,ftrain_clusters_all;
            ftrain_all.open((output_name+"_training_pred.csv").c_str());
            //        cerr<<"done open file "<<endl;
            for ( vector< vector< pair<int, int> > >::iterator i_all = train_all_transpose.begin(); i_all != train_all_transpose.end(); ++i_all)
            {
                if (i_all->empty())
                {
                    ftrain_all<<endl;
                }else
                {//fill in row
                    
                    vector< pair<int, int> >::iterator i_all2 = i_all->begin();

                    //handle first column
                    if (i_all2->first ==0){
                        ftrain_all<<i_all2->second;
                        ++i_all2;
                    }
                    
                    int col=1;
                    
                    for ( ; i_all2 != i_all->end(); ++i_all2)
                    {
                        for (;col < i_all2->first; ++col) //fill in blanks
                        {
                            ftrain_all<<",";
                        }
                        ftrain_all<<","<<i_all2->second;

                        ++col;
                    }
                    
                    //need the <= because of the final plus within previous loop
                    for (;col < colmax; ++col) //fill in end of row
                    {
                        ftrain_all<<",";
                    }
                    
                    ftrain_all<<endl;
                }
            }
            
            ftrain_all.close();
            
            //        cerr<<"done open file "<<endl;
            ftrain_clusters_all.open((output_name+"_training_clusters.csv").c_str());
            for ( vector< vector< pair<int, int> > >::iterator c_all = train_clusters_all_transpose.begin(); c_all != train_clusters_all_transpose.end(); ++c_all)
            {
                cout<<"call size "<<c_all->size()<<endl;
                if (c_all->empty())
                {
                    ftrain_clusters_all<<endl;
                }else
                {//fill in row
                    
                    vector< pair<int, int> >::iterator c_all2 = c_all->begin();
                    
                    //handle first column
                    if (c_all2->first ==0){
                        ftrain_clusters_all<<c_all2->second;
                        ++c_all2;
                    }
                    
                    int col=1;
                    
                    for ( ; c_all2 != c_all->end(); ++c_all2)
                    {
                        for (;col < c_all2->first; ++col) //fill in blanks
                        {
                            ftrain_clusters_all<<",";
                        }
                        ftrain_clusters_all<<","<<c_all2->second;
                        
                        ++col;
                    }
                    
                    //need the <= because of the final plus within previous loop
                    for (;col < colmax; ++col) //fill in end of row
                    {
                        ftrain_clusters_all<<",";
                    }
                    
                    ftrain_clusters_all<<endl;
                    
                }
            }

            
            ftrain_clusters_all.close();
            //        cerr<<"done write  file "<<endl;
        }
        ///----------------------DONE WRITE OUT OF TRAINING PREDICTIONS ----------------------
        //******************************
        bool useGenAcc=false;//true;//THIS IS HARD SET AT MOMEMTN
        ///lets see if gen accuracy was used
        
        for (vector<TpFpTnFn>::iterator i_genacc = tsamp_data.genData_.begin(); i_genacc != tsamp_data.genData_.end();++i_genacc)
        {
            //            cerr<<"gendata "<<i_genacc->N()<<endl;
            if (i_genacc->N() > 0 ) useGenAcc=true;
        }
        
        vector<bool> run_mask(tsamp_data.l_accuracy_pergroup.size(),1);//default to 1, that way if unchanged will include all
        //  cerr<<"thresholds "<<tr_acc_thresh_<<" "<<n_tr_acc_thresh_<<" "<<tsamp_data.l_accuracy_pergroup.size()<<" "<<tsamp_data.genData_.size()<<endl;
        //        cerr<<"Do Post Processing of Data2.25..."<<endl;
        //        cerr<<"Do Post Processing of Data2.5..."<<endl;
        if (useGenAcc)
        {
            //           cerr<<"Do Post Processing of Data...GenAcc2.1 "<<tsamp_data.genData_.size()<<" "<<tr_acc_thresh_<<" "<<n_tr_acc_thresh_<<endl;
            
            if ((tr_acc_thresh_ > 0.0 ) || (n_tr_acc_thresh_ > 0.0))
            {//this part allows removal of models based on training accuracies
                cerr<<"LETS CALCULATE TRAINING ACCURACIES "<<tr_acc_thresh_<<" "<<n_tr_acc_thresh_<<endl;
                
                float thresh=0.5;
                if ( n_tr_acc_thresh_ > 0.0 )
                {
                    list< pair<float,unsigned int> > l_accuracy_pergroup_sorted;
                    unsigned int ind=0;
                    for (vector<TpFpTnFn>::iterator i_genacc = tsamp_data.genData_.begin(); i_genacc != tsamp_data.genData_.end();++i_genacc,++ind)
                    {
                        l_accuracy_pergroup_sorted.push_back( pair<float,unsigned int>(i_genacc->acc(),ind));
                        //                   cerr<<"postproc "<<i_genacc->tp<<" "<<i_genacc->fp<<" "<<i_genacc->tn<<" "<<i_genacc->fn<<endl;
                    }
                    l_accuracy_pergroup_sorted.sort();
                    
                    //               cerr<<"Calculate threshold based on top "<<n_tr_acc_thresh_ * 100<<"% of samples."<<endl;
                    //  print( l_accuracy_pergroup, "l_accuracy_pergroup");
                    // grab top 5%
                    list< pair<float,unsigned int> >::iterator i_group=l_accuracy_pergroup_sorted.begin();
                    advance(i_group, tsamp_data.l_accuracy_pergroup.size() * ( 1- n_tr_acc_thresh_ ) );
                    thresh=i_group->first;
                    
                    
                    
                }else{
                    //               cerr<<"thresh "<<thresh<<endl;
                    
                    thresh=tr_acc_thresh_;
                    //               cerr<<"thresh "<<thresh<<endl;
                    
                }
                //           cerr<<"mask size / number of groups "<<endl;//run_mask.size()<<" "<<l_accuracy_pergroup.size()<<" "<<l_accuracy_pergroup.size() * ( 1- 0.05 )<< thresh<<endl;
                //           if (thresh < 0.6 ) thresh =0.6;
                
                
                
                //           for (vector<TpFpTnFn>::iterator i_genacc = tsamp_data.genData_.begin(); i_genacc != tsamp_data.genData_.end();++i_genacc)
                //           {
                //               cerr<<"postproc "<<i_genacc->tp<<" "<<i_genacc->fp<<" "<<i_genacc->tn<<" "<<i_genacc->fn<<endl;
                //           }
                
                
                
                
                
                vector<bool>::iterator i_mask = run_mask.begin();
                
                cerr<<"Threshold "<<thresh<<" "<<tsamp_data.genData_.size()<<endl;
                
                for (  vector<TpFpTnFn>::iterator i_acc = tsamp_data.genData_.begin(); i_acc != tsamp_data.genData_.end(); ++i_acc,++i_mask)
                {
                    cerr<<"m " << i_acc->acc() << " " <<thresh<<endl;
                    *i_mask = ( i_acc->acc() >= thresh ) ? 1 : 0 ;
                }
                
            }
            cerr<<"Do Post Processing of Data...GenAcc2.2 "<<tr_acc_thresh_<<" "<<n_tr_acc_thresh_<<endl;
            
        }else{
            //       cerr<<"Do Post Processing of Data...4"<<endl;
            cerr<<"Do Post Processing of Data...GenAcc4 "<<tr_acc_thresh_<<" "<<n_tr_acc_thresh_<<endl;
            //tr_acc_thresh_=0.6;
            if ((tr_acc_thresh_ > 0.0 ) || (n_tr_acc_thresh_ > 0.0))
            {//this part allows removal of models based on training accuracies
                //            cerr<<"LETS CALCULATE TRAINING ACCURACIES2 "<<tr_acc_thresh_<<" "<<n_tr_acc_thresh_<<endl;
                
                list< pair<float,unsigned int> > l_accuracy_pergroup_sorted = tsamp_data.l_accuracy_pergroup;
                l_accuracy_pergroup_sorted.sort();
                float thresh=0.5;
                if ( n_tr_acc_thresh_ > 0.0 )
                {
                    //                cerr<<"Calculate threshold based on top "<<n_tr_acc_thresh_ * 100<<"% of samples."<<endl;
                    //  print( l_accuracy_pergroup, "l_accuracy_pergroup");
                    // grab top 5%
                    list< pair<float,unsigned int> >::iterator i_group=l_accuracy_pergroup_sorted.begin();
                    advance(i_group, tsamp_data.l_accuracy_pergroup.size() * ( 1- n_tr_acc_thresh_ ) );
                    thresh=i_group->first;
                    
                    
                    
                }else{
                    thresh=tr_acc_thresh_;
                }
                
                vector<bool>::iterator i_mask = run_mask.begin();
                
                // cerr<<"Threshold "<<thresh<<endl;
                
                for (  list< pair<float,unsigned int> >::iterator i_acc = tsamp_data.l_accuracy_pergroup.begin(); i_acc != tsamp_data.l_accuracy_pergroup.end(); ++i_acc,++i_mask)
                {
                    cerr<<"m " << i_acc->first << " " <<thresh<<endl;
                    *i_mask = ( i_acc->first >= thresh ) ? 1 : 0 ;
                }
                
            }
        }
        //        cerr<<"Do Post Processing of Data...5"<<endl;
        //if (Verbose>=1)
        print(run_mask,"model mask ");
        
        ///------------------------------
        
        //  bool do_ppv_npv_ensemble
        if (do_bagging)
        {
            if (Verbose>=1){
                cerr<<"Do Bagging : Yes"<<endl;
                cerr<<"Weight by Probabilities: "<<((weightBagByProb == 0 )? "No" : "Yes")<<endl;
            }
            
            //GENERALIZATON BAGGING
            vector<int2> bagged( gaia_->getNumberOfSamples(), base );
            vector<float> bagged_perc( gaia_->getNumberOfSamples(),0 );
            vector<float> bagged_perc0( gaia_->getNumberOfSamples(),0 );
            
            cerr<<"baggedsizes "<<bagged.size()<<" "<<bagged_perc.size()<<" "<<run_mask.size()<<endl;
            
            {
                //x is the count, y is the number of votes
                cout<<"Generalization Data Size : "<<tsamp_data.genData_.size()<<endl;
                //  cout<<"Number of Estimates : "<<tsamp_data.estimates_all.size()<<endl;
                
                if (weightBagByProb)
                {
                    vector<bool>::iterator i_runm = run_mask.begin();
                    vector<TpFpTnFn>::iterator i_gen = tsamp_data.genData_.begin();
                    if (tsamp_data.genData_.empty())
                        throw ClassificationTreeException("TreeSampler::postProcess - No Generalization Accuracy found to Weight Probabilities.");
                    for (vector<int3>::const_iterator i_est = tsamp_data.estimates_all.begin(); i_est != tsamp_data.estimates_all.end(); ++i_est)
                    {
                        //                        cerr<<"estimates_all with weightByProb "<<i_est->x<<" "<<i_est->y<<" "<<i_est->z<<endl;
                        //increment the count for that subject
                        if (*i_runm)
                        {
                            ++bagged[i_est->x].x;
                            bagged[i_est->x].y+=i_est->y;//add the vote 0/1 for that subject
                            
                            //bagged_perc is what stores the weighted
                            //lets accumulate prob
                            //only comp[atible with binary classes at the mmoment
                            //                            cerr<<"estimate "<<i_est->y<<" "<<i_gen->ppv()<<" "<<i_gen->npv()<<endl;
                            if (i_est->y == 0 )
                            {
                                bagged_perc0[i_est->x]+= i_gen->npv();
                                bagged_perc[i_est->x]+= 1 - i_gen->npv();
                                
                            }
                            else{
                                bagged_perc0[i_est->x]+= 1- i_gen->ppv();
                                bagged_perc[i_est->x]+= i_gen->ppv();
                                
                            }
                        }
                        
                        if ( (i_est+1) != tsamp_data.estimates_all.end() ) //if there is a next sample
                            if ((i_est+1)->z != i_est->z ) //increment mask if not same group
                            {
                                ++i_runm;
                                ++i_gen;
                            }
                    }
                    
                    
                    
                    unsigned int count=0;
                    vector<float>::iterator i_b2 = bagged_perc.begin();
                    vector<float>::iterator i_b2_0 = bagged_perc0.begin();
                    
                    for (vector<int2>::iterator i_b = bagged.begin(); i_b != bagged.end(); ++i_b,++i_b2,++i_b2_0,++count)
                    {
                        //                        cerr<<"compare "<<*i_b2<<" "<<*i_b2_0<<endl;
                        i_b->y = ((*i_b2) > (*i_b2_0)) ? 1 : 0;
                        //i_b->y = ( i_b->y > (i_b->x/2.0)) ? 1 : 0 ;
                        //*i_b2 /= i_b->x;
                    }
                    //                    cerr<<"endif "<<endl;
                }else{
                    
                    vector<bool>::iterator i_runm = run_mask.begin();
                    for (vector<int3>::const_iterator i_est = tsamp_data.estimates_all.begin(); i_est != tsamp_data.estimates_all.end(); ++i_est)
                    {
                        //                        cerr<<"estimates_all no weightByProb "<<i_est->x<<" "<<i_est->y<<" "<<i_est->z<<endl;
                        //increment the count for that subject
                        if (*i_runm)
                        {
                            ++bagged[i_est->x].x;
                            bagged[i_est->x].y+=i_est->y;//add the vote 0/1 for that subject
                            bagged_perc[i_est->x]+=i_est->y;
                        }
                        
                        if ( (i_est+1) != tsamp_data.estimates_all.end() ) //if there is a next sample
                            if ((i_est+1)->z != i_est->z ) //increment mask if not same group
                                ++i_runm;
                        
                    }
                    
                    
                    
                    unsigned int count=0;
                    vector<float>::iterator i_b2 = bagged_perc.begin();
                    for (vector<int2>::iterator i_b = bagged.begin(); i_b != bagged.end(); ++i_b,++i_b2,++count)
                    {
                        i_b->y = ( i_b->y > (i_b->x/2.0)) ? 1 : 0 ;
                        *i_b2 /= i_b->x;
                    }
                }
            }
            
            //compile all distances
            vector< vector<float> > all_dists( gaia_->getNumberOfSamples());
            for ( vector< vector< pair<int, float> > >::const_iterator i = tsamp_data.estimated_distance_scores.begin(); i != tsamp_data.estimated_distance_scores.end(); ++i)
            {
                for ( vector< pair<int, float> >::const_iterator ii = i->begin(); ii!=i->end();++ii)
                {
                    all_dists[ii->first].push_back(ii->second);
                }
            }
            ///oif output name has been set then wirte out data
            cout<<"distance write "<<doWrite<<" "<<tsamp_data.writeLevel<<endl;
            if  ((doWrite )&& (tsamp_data.writeLevel>1))
            {
                //------------------within--group
                {
                    vector< vector<int> > estall_train( gaia_->getNumberOfSamples());
                    for (vector<int4>::const_iterator i_est = tsamp_data.estimates_train_all.begin(); i_est != tsamp_data.estimates_train_all.end(); ++i_est)
                        estall_train[i_est->x].push_back( i_est->y );
                    
                    ofstream fall;
                    fall.open((output_name+"_withGr_pred.csv").c_str());
                    unsigned int index=0;
                    for (vector< vector<int> >::iterator i = estall_train.begin(); i!=estall_train.end();++i,++index)
                    {
                        fall<<index<<","<<gaia_->getTargetData<float>()[index]<<","<<within_bagged_perc[index];
                        for ( vector<int>::iterator ii = i->begin(); ii!=i->end();++ii)
                            fall<<","<<*ii;
                        fall<<endl;
                    }
                    fall.close();
                }
                
                //-------------generalization- output - Distance file---------------//
                {
                    cout<<"wirityn idstance file"<<endl;
                    ofstream fout_dist((output_name+"_dist.csv").c_str());
                    fout_dist<<"Subject_Index,True_Class,NumberOfPredictions,Accuracy,MeanSignedDistance,NumberOfPositiveDistances,MeanPositiveDistance,NumberOfNegativeDistance,MeanNegativeDistance,DistanceScores..."<<endl;
                    
                    vector< vector<int> > estall( gaia_->getNumberOfSamples());
                    for (vector<int3>::const_iterator i_est = tsamp_data.estimates_all.begin(); i_est != tsamp_data.estimates_all.end(); ++i_est)
                        estall[i_est->x].push_back( i_est->y );
                    
                    ofstream fall;
                    fall.open((output_name+"_pred.csv").c_str());
                    vector< vector<float> >::iterator i_d = all_dists.begin();
                    unsigned int index=0;
                    for (vector< vector<int> >::iterator i = estall.begin(); i!=estall.end();++i,++index,++i_d)
                    {
                        float acc = (gaia_->getTargetData<float>()[index]==0) ? bagged_perc[index] : (1-bagged_perc[index] );
                        
                        fall<<index<<","<<gaia_->getTargetData<float>()[index]<<","<<acc;
                        
                        fout_dist<<index<<","<<gaia_->getTargetData<float>()[index]<<","<<all_dists[index].size()<<","<<acc;
                        
                        
                        for ( vector<int>::iterator ii = i->begin(); ii!=i->end();++ii)
                            fall<<","<<*ii;
                        fall<<endl;
                        
                        
                        //output mean distance and mean absolute
                        //                        fout_dist<<","<<mean<float>(*i_d)<<","<<mean_abs<float>(*i_d);
                        fout_dist<<","<<mean<float>(*i_d);
                        
                        //mean-unsigned distance
                        //Assume distances run from -1 to +1, 0 is directrly between two classes
                        float dist_positive(0.0), dist_negative(0.0);
                        unsigned int n_pos(0), n_neg(0);
                        for ( vector<float>::iterator ii_d = i_d->begin(); ii_d!=i_d->end();++ii_d)
                        {
                            if (*ii_d >  0 )
                            {
                                dist_positive+= *ii_d;
                                n_pos++;
                            }else{
                                dist_negative += *ii_d;
                                n_neg++;
                            }
                        }
                        fout_dist<<","<<","<<n_pos<<","<<dist_positive/n_pos<<","<<n_neg<<dist_negative/n_neg;
                        //out individual distances
                        for ( vector<float>::iterator ii_d = i_d->begin(); ii_d!=i_d->end();++ii_d)
                            fout_dist<<","<<*ii_d;
                        fout_dist<<endl;
                    }
                    fall.close();
                    fout_dist.close();
                }
            }
            
            float3 wGrErr(gaia_->getAccuracy(within_bagged,tsamp_data.stratified_labels),gaia_->getSensitivity(within_bagged,tsamp_data.stratified_labels),gaia_->getSpecificity(within_bagged,tsamp_data.stratified_labels) );
            tsamp_data.v_withinGrErr.push_back(wGrErr);
            // cerr<<"VMASK "<<v_mask.size()<<endl;
            if (v_mask.empty())
            {
                tsamp_data.AccSensSpec.push_back( pair<float3,float3> ( \
                                                                       float3(gaia_->getAccuracy(bagged,tsamp_data.stratified_labels), \
                                                                              gaia_->getSensitivity(bagged,tsamp_data.stratified_labels), \
                                                                              gaia_->getSpecificity(bagged,tsamp_data.stratified_labels) ), \
                                                                       float3(0,0,0)\
                                                                       ));
                tsamp_data.class_sensitivities.push_back(gaia_->getClassSensitivities(bagged,tsamp_data.stratified_labels));
                
                tsamp_data.v_tp_fp_tn_fn.push_back(pair<TpFpTnFn,TpFpTnFn>(gaia_->getErrorData( bagged,tsamp_data.stratified_labels ), TpFpTnFn(0,0,0,0)));
                
            }else{
                tsamp_data.class_sensitivities.push_back(gaia_->getClassSensitivities(bagged,tsamp_data.stratified_labels,v_mask,1));
                
                tsamp_data.AccSensSpec.push_back( pair<float3,float3> ( \
                                                                       float3(gaia_->getAccuracy(bagged,tsamp_data.stratified_labels,v_mask,1), \
                                                                              gaia_->getSensitivity(bagged,tsamp_data.stratified_labels,v_mask,1), \
                                                                              gaia_->getSpecificity(bagged,tsamp_data.stratified_labels,v_mask,1) ), \
                                                                       float3(gaia_->getAccuracy(bagged,tsamp_data.stratified_labels,v_mask,0), \
                                                                              gaia_->getSensitivity(bagged,tsamp_data.stratified_labels,v_mask,0), \
                                                                              gaia_->getSpecificity(bagged,tsamp_data.stratified_labels,v_mask,0) ) \
                                                                       )
                                                 );
                tsamp_data.v_tp_fp_tn_fn.push_back( pair<TpFpTnFn,TpFpTnFn>( \
                                                                            gaia_->getErrorData( bagged,tsamp_data.stratified_labels,v_mask,1 ), \
                                                                            gaia_->getErrorData( bagged,tsamp_data.stratified_labels,v_mask,0 ) \
                                                                            )\
                                                   );
                
                
            }
            
            
            ///---------This part masks out middle folks based on votes (estimated posteriro probabilities)
            
            vector<int> bag_mask(bagged_perc.size(),0);
            vector<int> bag_mask_out(bagged_perc.size(),0);
            
            //            print(bagged_perc,"bagged_perc");
            //            cerr<<"threshold "<<tsamp_data.acc_thresh<<endl;
            vector<int>::iterator i_m = bag_mask.begin();
            for ( vector<float>::iterator i_perc = bagged_perc.begin(); i_perc != bagged_perc.end(); ++i_perc,++i_m)
                *i_m = ((*i_perc <= tsamp_data.acc_thresh) || ( *i_perc >= (1-tsamp_data.acc_thresh))) ? 1 : 0 ;
            //            print(bag_mask,"bagmask");
            
            
            
            if (v_mask.empty())
            {
                
                tsamp_data.v_tp_fp_tn_fn_thresh.push_back( pair<TpFpTnFn,TpFpTnFn>( gaia_->getErrorData( bagged,tsamp_data.stratified_labels,bag_mask ),TpFpTnFn(0,0,0,0)) );
                
            }else{
                if (bag_mask.size() != v_mask.size())
                    throw ClassificationTreeNodeException(("TreeSample: Incompatible masks sizes (threshold/selection) : "+ num2str(bag_mask.size())+ " / " + num2str(v_mask.size())).c_str());
                
                
                {//first lets make sure all the thresholded excluded superthresh subjects are excluded.
                    vector<int>::iterator i_mb = bag_mask.begin();
                    for (vector<int>::const_iterator i_m2 = v_mask.begin(); i_m2 != v_mask.end();++i_m2,++i_mb)
                        if (*i_m2 == 0)
                            *i_mb = 0 ;
                }
                { //calculate for super threshold, excluded subjects
                    //                    print(bagged_perc,"bagged_perc");
                    //                    print(v_mask,"v_mask");
                    //                    print(bag_mask_out,"bagmask_out");
                    
                    vector<int>::const_iterator i_m2 = v_mask.begin();
                    vector<int>::iterator i_m = bag_mask_out.begin();
                    
                    for ( vector<float>::iterator i_perc = bagged_perc.begin(); i_perc != bagged_perc.end(); ++i_perc,++i_m,++i_m2)
                        if (*i_m2 == 0 )//the threshod will exlcude any that were exlcuded prior
                            *i_m = ((*i_perc <= tsamp_data.acc_thresh) || ( *i_perc >= (1-tsamp_data.acc_thresh))) ? 1 : 0 ;
                        else
                            *i_m = 0 ;
                }
                //                print(v_mask,"v_mask");
                
                //                print(bag_mask,"bagmask");
                //                print(bag_mask_out,"bagmask_out");
                
                //the second parameter is meant to have 1 as well because of the mask that was create set excluded to 1.
                tsamp_data.v_tp_fp_tn_fn_thresh.push_back( pair<TpFpTnFn,TpFpTnFn>( gaia_->getErrorData( bagged,tsamp_data.stratified_labels,bag_mask ,1),gaia_->getErrorData( bagged,tsamp_data.stratified_labels,bag_mask_out ,1)) );
                
            }
            
            
            
            
            
            
            for (vector<int2>::iterator i_b = bagged.begin(); i_b != bagged.end();++i_b)
            {
                // cout<<"push back to predictions "<<i_b->x<<" "<<i_b->y<<endl;
                if (i_b->x > 0 )
                {
                    tsamp_data.predictions.push_back( vector<int>(1,i_b->y) );
                }else{
                    tsamp_data.predictions.push_back( vector<int>() );
                    
                }
            }
            //  predictions=bagged;
        }else{
            //                cerr<<"No Bagging"<<endl;
            {
                
                vector<int2> bagged( gaia_->getNumberOfSamples(), base );
                
                
                vector< vector<int2> > all_runs;//this vector will store all runs
                //x is the count, y is the number of votes
                tsamp_data.predictions.clear();
                unsigned int Ns = gaia_->getNumberOfSamples();
                tsamp_data.predictions.resize(Ns);
                vector<bool>::iterator i_mask = run_mask.begin();
                int sample_index_prev=-1; ///this is used to properly trim runs, if all runs are included shoudl corrspond to i_est->z
                int sample_index=-1;//entry index of all runs
                for (vector<int3>::const_iterator i_est = tsamp_data.estimates_all.begin(); i_est != tsamp_data.estimates_all.end(); ++i_est)
                {
                    //                     cout<<"NS "<< tsamp_data.predictions.size()<<" "<<i_est->x<<" "<<i_est->y<<" "<<i_est->z<<endl;
                    if ( sample_index_prev != i_est->z )//reisze for each additional group
                    {
                        
                        if (*i_mask)
                        {
                            all_runs.resize(all_runs.size()+1);
                            all_runs.back().resize(Ns);
                            sample_index_prev= i_est->z;
                            ++sample_index;
                        }
                    }
                    if (*i_mask)
                    {
                        tsamp_data.predictions[i_est->x].push_back(i_est->y);
                        
                        ++all_runs[sample_index][i_est->x].x;//tried to make as generic as possible
                        all_runs[sample_index][i_est->x].y=i_est->y;
                    }
                    
                    if ( (i_est+1) != tsamp_data.estimates_all.end() ) //if there is a next sample
                        if ((i_est+1)->z != i_est->z ) //increment mask if not same group
                            ++i_mask;
                    
                }
                
                //            cerr<<"All_runs "<<all_runs.size()<<endl;
                for (vector< vector<int2> >::iterator i = all_runs.begin(); i != all_runs.end(); ++i )
                {
                    //                       cerr<<"new sample "<<i->size()<<endl;
                    //for (vector<int2>::iterator ii = i->begin(); ii != i->end(); ++ii )
                    //    cerr<<ii->x<<" "<<ii->y<<endl;
                    
                }
                for (vector< vector<int2> >::iterator i_all = all_runs.begin(); i_all != all_runs.end(); ++i_all)
                {
                    
                    if (v_mask.empty())
                    {
                        //                        cerr<<"empty mask"<<endl;
                        tsamp_data.AccSensSpec.push_back( pair<float3,float3> ( \
                                                                               float3(gaia_->getAccuracy(*i_all,tsamp_data.stratified_labels), \
                                                                                      gaia_->getSensitivity(*i_all,tsamp_data.stratified_labels), \
                                                                                      gaia_->getSpecificity(*i_all,tsamp_data.stratified_labels) ), \
                                                                               float3(0,0,0)\
                                                                               ));
                        tsamp_data.class_sensitivities.push_back(gaia_->getClassSensitivities(*i_all,tsamp_data.stratified_labels));
                        
                        tsamp_data.v_tp_fp_tn_fn.push_back(pair<TpFpTnFn,TpFpTnFn>(gaia_->getErrorData( *i_all,tsamp_data.stratified_labels ), TpFpTnFn(0,0,0,0)));
                        //   tsamp_data.v_tp_fp_tn_fn_thresh.push_back( pair<TpFpTnFn,TpFpTnFn>( gaia_->getErrorData( bagged,tsamp_data.stratified_labels,bag_mask ,1),gaia_->getErrorData( bagged,tsamp_data.stratified_labels,bag_mask_out ,1)) );
                        
                        
                    }else{
                        //                        cerr<<"non empty mask "<<v_mask.size()<<endl;
                        //print(v_mask,"v_mask");
                        //  print(*i_all,"*i_all");
                        tsamp_data.class_sensitivities.push_back(gaia_->getClassSensitivities(*i_all,tsamp_data.stratified_labels,v_mask,1));
                        
                        
                        //cerr<<"accuracy "<<gaia_->getAccuracy(*i_all,tsamp_data.stratified_labels,v_mask,1)<<endl;
                        tsamp_data.AccSensSpec.push_back( pair<float3,float3> ( \
                                                                               float3(gaia_->getAccuracy(*i_all,tsamp_data.stratified_labels,v_mask,1), \
                                                                                      gaia_->getSensitivity(*i_all,tsamp_data.stratified_labels,v_mask,1), \
                                                                                      gaia_->getSpecificity(*i_all,tsamp_data.stratified_labels,v_mask,1) ), \
                                                                               float3(gaia_->getAccuracy(*i_all,tsamp_data.stratified_labels,v_mask,0), \
                                                                                      gaia_->getSensitivity(*i_all,tsamp_data.stratified_labels,v_mask,0), \
                                                                                      gaia_->getSpecificity(*i_all,tsamp_data.stratified_labels,v_mask,0) ) \
                                                                               )
                                                         );
                        tsamp_data.v_tp_fp_tn_fn.push_back( pair<TpFpTnFn,TpFpTnFn>( \
                                                                                    gaia_->getErrorData( *i_all,tsamp_data.stratified_labels,v_mask,1 ), \
                                                                                    gaia_->getErrorData( *i_all,tsamp_data.stratified_labels,v_mask,0 ) \
                                                                                    )\
                                                           );
                        
                        
                    }
                    
                    
                    
                    
                }
                
                
                
                
            }
            
        }
        //        cerr<<"done Do Post Processing of Data..."<<endl;
        
    }
    
    // int TreeSampler::run_Kfold(const unsigned int & K, std::vector< pair<float3,float3> >  & AccSensSpec, vector< vector<int> > & predictions  , std::vector< std::map<int,std::pair<float,unsigned int> > > & class_sensitivities, std::vector< pair<TpFpTnFn,TpFpTnFn> > & v_tp_fp_tn_fn, std::vector< pair<TpFpTnFn,TpFpTnFn> > & v_tp_fp_tn_fn_thresh, const float & acc_thresh, const vector<int> & stratified_labels , std::vector< float3 > & v_withinGrErr, list< pair<float,unsigned int> > & l_accuracy_pergroup ,string output_name  )
    int TreeSampler::run_Kfold( std::vector<int> &v_mask, const bool & do_bagging, TreeSampleData & tsamp_data, std::string output_name)
    {
        if (Verbose)
        {
            cout<<"Run K-Fold Cross-validation..."<<endl;
            cout<<"     Bagging : "<<( (do_bagging==0) ? "no" : "yes" )<<endl;
            cout<<"     K : "<<tsamp_data.Kfold<<endl;
            cout<<"     Number of Samples : "<<gaia_->getNumberOfSamples()<<" "<<endl;

        }
        

        //set whether it goes to 1 or 2 inner loop
        //        if (tsamp_data.model_thresh_mode == 2 )
        if (Verbose)
        {
            cout<<"Turning off within-loop K-fold estimates of generalization accuracy... "<<endl;
        }
        cout<<"setDoGenAccKFold..."<<endl;
        cout<<gaia_->getNumberOfSamples()<<endl;
        gaia_->setDoGenAccKFold(false);
        cout<<"Done setDoGenAccKFold..."<<endl;

        //        else
        //            gaia_->setDoGenAccKFold(true);
    
        
        //Selects all subjects if no mask is provided
        if (v_mask.empty())//if, then select all
        {
            cout<<"Creating subject mask..."<<endl;
            v_mask= vector<int>(gaia_->getNumberOfSamples(),1);
        }
        cout<<"done creating mask"<<endl;
        //selection masks are used to subsampling the data. In this fashion, cross-validation is implemented
        vector< vector<int> > sel_mask(1, v_mask );
        //        print(sel_mask.front(),"sel_mask");
        
        unsigned int offset=0;
        //Create a list of subsamples corresponding to CV
        //Allows chunks to group samples together (e.g. imaging timeseries)
        if (Verbose)
        {
            cout<<"Calculating Cross-Validation samples..."<<endl;
        }
        vector< vector<int> > all_ss_combinations=applyCrossValidation(sel_mask,tsamp_data.Kfold,gaia_->getInputChunks());
        
        if (Verbose)
        {
            cout<<"Looping over Cross-Validation samples..."<<endl;
        }
        
        for ( vector< vector<int> >::iterator i = all_ss_combinations.begin(); i != all_ss_combinations.end();++i,++offset)
        {
            //print subject selection mask
//            if (Verbose > 0 )
//            {
//                print(*i, "Sample Mask");
//            }
            
            //structures to store output
            vector< pair<int,float> > distance_scores;// = new vector< pair<int,float> >();
            vector<int> one_test_mask(i->size(),1);
            
            vector<int4> estimates_train_temp;

            if (Verbose){
                cout<<"     Classify sample "<<offset<<endl;
                cout<<"     ------------------------------"<<endl;
            }
            vector<int3> estimates_temp  = gaia_->classify(distance_scores, *i,one_test_mask, estimates_train_temp, offset);
            if (Verbose){
                cout<<"     Done Classify. "<<offset<<endl;

            }
            cout<<"estimates_train_temp "<<estimates_train_temp.size()<<endl;
            
            { //store the classification results
                tsamp_data.genData_.push_back(gaia_->getGeneralizationData());
                tsamp_data.estimated_distance_scores.push_back(distance_scores);
                tsamp_data.estimates_all.insert(tsamp_data.estimates_all.end(),estimates_temp.begin(), estimates_temp.end());
                tsamp_data.estimates_train_all.insert(tsamp_data.estimates_train_all.end(),estimates_train_temp.begin(), estimates_train_temp.end());
            }
            
        }
        
        if (Verbose>0)
        {
            cout<<"processing the output data...";
        }
        //if bagging is 0 then weight by prob is 0
        postProcess(v_mask, do_bagging,0, tsamp_data , output_name );
        
        if (Verbose>0)
        {
            cout<<"     ------------------------------"<<endl;

        }
        
        return 0;
        
    }
    
    
    int TreeSampler::absoluteSubsample(unsigned int B, const bool & doBagging, const bool & weightBagByProb, const unsigned int & NperGroup, const std::vector<int> & v_mask, TreeSampleData & tsamp_data, std::string output_name)
    {
        //******************THE SUBSAMPLING IS ONLY AVAILABLE FOR 2 CLASSES*********************//
        //start by implementing for binary
        //N is the size of subsample
        //B is the number of bootstrap samples.
        //The boostrapping is can be at either the individual subsample level or at the larger sample, from which we sample from. Let's try both!
        
        
        if ((tsamp_data.bootstrap) && (tsamp_data.Kfold != 0 ))
        {
            cerr<<"Currently, Bootstrapping is only supported with Kfold=0"<<endl;
            throw ClassificationTreeNodeException("Currently, Bootstrapping is only supported with Kfold=0");
        }
        
        
        //        cerr<<"Baging using SubSampling2 ..."<<endl;
        //std::vector< std::vector< std::pair<int,float> > > estimated_distance_scores;
        //        if (tsamp_data.model_thresh_mode == 2 )
        gaia_->setDoGenAccKFold(false);
        //        else
        //            gaia_->setDoGenAccKFold(true);
        
        bool doWrite = (output_name.empty())? 0:1;
        
        if (Verbose)
            cout<<"absoluteSubSample ..."<<endl;
        //done like this in case it exceeds maximum number of samples
        vector< pair<unsigned int,float> > predicted_values;
        unsigned int Kclasses = gaia_->getNumberOfClasses();
        vector<int> labels = gaia_->getClassLabels();
        //   print(labels,"tree sampler labels");
        vector<unsigned int> N_per_class;// = gaia_->getNumberOfObservationsPerClass();
        //        print(N_per_class,"N_per_class");
        if (v_mask.empty())
            N_per_class = gaia_->getNumberOfObservationsPerClass();
        else
            N_per_class = gaia_->getNumberOfObservationsPerClass( v_mask );
        //        print(N_per_class,"N_per_class_wMask");
        
        
        vector<float> targ_data = gaia_->getTargetData<float>();
        // print<float>(targ_data,"targ_data");
        //mapping between class label and index in in labels
        map<int, unsigned int> label_2_ind;
        for (unsigned int i=0; i< Kclasses ;  i++)
            label_2_ind.insert(pair<int,unsigned int>(labels[i],i));
        
        
        //For now lets just set stratification to happen for all labels
        //nonstrat_labels should remain empty at moment
        //assume 0/1 labels of interest
        // vector<int> stratified_labels(2,0);// = labels;
        //stratified_labels[1]=1;
        
        vector<int> nonstrat_labels;
        
        //Total number of observations (samples), just the sum across number of obs. per class
        //		unsigned int Nobs = sum<unsigned int>(N_per_class);
        unsigned int NperClass_min=static_cast<unsigned int>(1e12);
        unsigned max_combinations=1;//initialize to 1, change if necessary
        //keeps tracj of number picked from each class, count down
        vector<unsigned int> v_Nselect(Kclasses);
        
        
        //---------------------------------DO RANDOM SUBSAMPLING------------------------------------------------//
        //subsample labels, only two (only support for 2-class subsampling)
        //set number of samples in excluded groups to 0
        {
            vector<int>::iterator i_l = labels.begin();
            for ( vector<unsigned int>::iterator i = N_per_class.begin() ; i != N_per_class.end(); ++i, ++i_l)
            {
                bool select=false;
                //check to see if the label is one of the specified interest ones
                for (vector<int>::const_iterator ii = tsamp_data.stratified_labels.begin(); ii != tsamp_data.stratified_labels.end(); ++ii)
                {
                    if (*ii == *i_l) //is the label been startified one of the specified labels
                        select=true;
                }
                if (!select)//if its not erase the entry
                {
                    nonstrat_labels.push_back(*i_l);
                    *i=0;
                }
            }
        }
        //      print(stratified_labels,"Strat labels");
        //    print(nonstrat_labels,"nonStrat labels");
        //    print(N_per_class,"N_per_class");
        
        //the order of these is important, need to take out samples before calculating min per group
        
        //maximum combinations is number of combinations from each class multiplied
        //also set the number of samples for each class to NgroupMin
        NperClass_min=NperGroup;
        
        //Here we decide to sample with or without bootstrap. The true bootstrap samples with replacement, I still enforce the equal sample size.
        if ( tsamp_data.bootstrap == 0 )
        {
            
            
            vector<unsigned int>::iterator ii = N_per_class.begin();
            for (vector<unsigned int>::iterator i = v_Nselect.begin(); i != v_Nselect.end(); ++i, ++ii)
            {
                //set to min if greater than zero
                *i= (*ii > 0 ) ? NperClass_min : 0;
                double comb = combination(*ii,NperClass_min);
                //  cout<<"max combinations "<<comb<<endl;
                
                if ((comb > std::numeric_limits<unsigned int>::max() ) || ( max_combinations*comb >= std::numeric_limits<unsigned int>::max()))
                    max_combinations = std::numeric_limits<unsigned int>::max();
                else
                    max_combinations *= comb;
            }
            
            if (Verbose)
                cerr<<"Maximum number of samples "<<max_combinations<<endl;
            
            //        print(v_Nselect,"v_Nselect");
            //Check to make sure that all class size are greater than specfied
            for (vector<unsigned int>::iterator i = v_Nselect.begin(); i != v_Nselect.end();++i)
            {
                // if ( (*i) < NperClass_min ) NperClass_min=*i;
                
                if ((*i>0)&&( NperGroup > (*i)))
                {
                    throw ClassificationTreeException("In run subsampling, absolute group sample size is larger than an inidividual group size.");
                    return 1;
                }
            }
            
            //check to make sure that you haven't chosen to many samples
            if (Verbose)
                cout<<"Maximum number of possible unique subsamples "<<max_combinations<<endl;
            if (B>max_combinations)//make sure input was not selected higher than number permissble
            {
                //instead of throw push throgh with max
                B = max_combinations;
                cout<<"Warning: You have chosen too many samples. Setting to "<<B<<endl;
            }
            
        }else if ( tsamp_data.bootstrap == 1 ){
            
            max_combinations=0;
            //because of replacement, may not really need a struct max on bootstrap samples. This should however provide a guideline and warning if actually surpassing.
            for ( vector<unsigned int>::iterator i = N_per_class.begin() ; i != N_per_class.end(); ++i)
            {
                cout<<"max combinations "<<max_combinations<<" "<<*i<<" "<<powf(*i,*i)<<" "<<B<<" "<<(B>max_combinations)<<endl;
                max_combinations+=powf(*i,*i);
                if (max_combinations > std::numeric_limits<float>::max())
                {
                    max_combinations=std::numeric_limits<float>::max();
                }
                cout<<"max "<<max_combinations<<" "<<std::numeric_limits<float>::max()<<endl;
                
            }
            vector<unsigned int>::iterator ii = N_per_class.begin();
            for (vector<unsigned int>::iterator i = v_Nselect.begin(); i != v_Nselect.end(); ++i, ++ii)
            {
                //set to min if greater than zero
                *i= (*ii > 0 ) ? NperClass_min : 0;
            }
            
            //Check to make sure that all class size are greater than specfied
            for (vector<unsigned int>::iterator i = v_Nselect.begin(); i != v_Nselect.end();++i)
            {
                if ((*i>0)&&( NperGroup > (*i)))
                {
                    throw ClassificationTreeException("In run subsampling, absolute group sample size is larger than an inidividual group size.");
                    return 1;
                }
            }
        }else{
            
            
        }
        
        //cout<<"next step "<<endl;
        //This is the bit that ensures equal sample sizes
        //		unsigned Nobs_ss = NperClass_min * Kclasses;
        //		unsigned int remainder = Nobs - Nobs_ss; //number of left out samples
        
        //create vector saying what to smaple off each class, make sure to not select more than what exists (particuylarly for sample by which you are not stratifying)
        
        
        cout<<"max "<<max_combinations<<" "<<std::numeric_limits<float>::max()<<" "<<B<<endl;
        
        
        
        //now find all the binary combinations of sampling the data
        //only select the desired amount, contained in binary_combinations
        if (Verbose)
        {
            cout<<"Runnning "<<NperGroup<<" subsamples per group."<<endl;
            print<unsigned int>(v_Nselect,"v_Nselect");
        }
        
        //ensure equal smaples sizes
        vector< vector<int> > all_ss_combinations;//all possible ways of randomly sampling the data to exclude desired amount
        if (tsamp_data.subSamplesFilename.empty()){
            vector<int> targ_int(gaia_->getNumberOfSamples());
            vector<int>::iterator i_ti = targ_int.begin();
            for ( vector<float>::iterator i_t = targ_data.begin(); i_t != targ_data.end(); ++i_t,++i_ti)
            {
                *i_ti = static_cast<int>((*i_t)+0.5);//rounds to prevent numerical errors
            }
            
            if ( tsamp_data.bootstrap == 0 )
            {
                if (v_mask.empty())
                    binary_combinations(all_ss_combinations,targ_int, B,v_Nselect, labels );
                else
                    binary_combinations(all_ss_combinations,targ_int, B,v_Nselect, labels, v_mask );
                
                
                
            }else if ( tsamp_data.bootstrap == 1 )
            {
                if (v_mask.empty())
                    bootstrap_combinations(all_ss_combinations,targ_int, B,v_Nselect, labels );
                else
                    bootstrap_combinations(all_ss_combinations,targ_int, B,v_Nselect, labels, v_mask );
                
                
            }
        } else{
            
            ifstream fsel_in(tsamp_data.subSamplesFilename.c_str());
            string line;
            while ( getline(fsel_in,line)  ) {
                all_ss_combinations.push_back(csv_string2nums_i(line));
                
            }
            fsel_in.close();
        }
        
        //---------Samples are generated by generating sampling possibilities then trimming non-equal cases
        //---------remove all configurations that doesn't preserve equal sample sizes
        //this should already be handled in the binary combinations call
        
        ofstream fsel;
        if (doWrite ){
            fsel.open((output_name+"_sel_mask.csv").c_str());
        }
        
        if (Verbose>=1)
            cout<<"Completed binary combinations."<<endl;
        unsigned int offset=0;
        
        
        ofstream fprederr;
        if ((doWrite ) && (tsamp_data.writeLevel>1))
        {
            fprederr.open((output_name+"_perSubSample.csv").c_str());
            fprederr<<"Within-Class_Accuracy,Generalization-Accuracy"<<endl;
        }
        if (Verbose>=1)
            cerr<<"About to run discriminant..."<<endl;
        
        unsigned int ss_count=0;
        for (vector< vector<int> >::iterator i = all_ss_combinations.begin(); i != all_ss_combinations.end(); ++i,++ss_count)//,++offset)
        {//cross-validation has not been applied yet, will do on the fly because of memory issues
            //lets try a compromise and just generate cross-validation for each sample and store these, decrease memory imprint by factor of N
            if (Verbose>=1)
            {
                if (tsamp_data.Kfold == 1)
                    cout<<"Next subsample "<<ss_count<<"/"<<all_ss_combinations.size()<<". Run Leave-One-Out within this sample. Number of Subsamples is "<<B<<"."<<endl;
                else
                    cout<<"Next subsample "<<ss_count<<"/"<<all_ss_combinations.size()<<". Run "<<tsamp_data.Kfold<<"-fold within this sample. Number of Subsamples is "<<B<<"."<<endl;
            }
            vector< vector<int> > all_ss_combinations_cv(1,*i);//copy in that specific subsample
            if (tsamp_data.subSamplesFilename.empty()) //only apply cross-validation if not loaded samples.
                all_ss_combinations_cv  = applyCrossValidation(all_ss_combinations_cv,tsamp_data.Kfold,gaia_->getInputChunks());
            
            if (Verbose>=1)
            {
                cout<<"Done applying CV to binary combinations. The are "<<all_ss_combinations_cv.size()<<" models to run."<<endl;
            }
            
            for (vector< vector<int> >::iterator i_cv = all_ss_combinations_cv.begin(); i_cv != all_ss_combinations_cv.end(); ++i_cv,++offset)
            {
                //                cerr<<"first iteration "<<i_cv->size()<<endl;
                //                print(*i_cv,"do classify one selection _mask");
                
                if (doWrite )
                {
                    vector<int>::iterator ii_cv = i_cv->begin();
                    fsel<<*ii_cv;
                    ++ii_cv;
                    for ( ; ii_cv != i_cv->end(); ++ii_cv)
                        fsel<<","<<*ii_cv;
                    fsel<<endl;
                    
                }
                
                //                cerr<<"done output "<<endl;
                
                vector< pair<int,float> > distance_scores;// = new vector< pair<int,float> >();
                vector<int> one_test_mask(i_cv->size(),1);
                vector<int4> estimates_train_temp;
                
                
                if ( ( (offset+1) % 100) == 0 )
                    cout<<"TreeSample::absoluteSubsample:: running classify() :: "<<offset<<endl;
                
                vector<int3> estimates_temp  = gaia_->classify(distance_scores, *i_cv,one_test_mask, estimates_train_temp, offset);
                
                
                tsamp_data.estimated_distance_scores.push_back(distance_scores);
                
                if (gaia_->storeGenData())
                    tsamp_data.genData_.push_back(gaia_->getGeneralizationData());
                
                if (gaia_->storeGenDataThresh())
                {
                    tsamp_data.genDataThresh_.push_back(gaia_->getGeneralizationDataThresh());
                    //  cerr<<"GenDataThresh "<<offset<<" "<<tsamp_data.genDataThresh_.back()<<endl;
                    
                    
                    
                }
                float withinAcc = gaia_->getAccuracy(estimates_train_temp);
                
                float genAcc = gaia_->getAccuracy(estimates_temp);//do not use this at irsk of bias
                if ((doWrite )&& (tsamp_data.writeLevel>1))
                    fprederr<<withinAcc<<","<<genAcc<<endl;
                //            if (withinAcc > 0.75)
                //  if (withinAcc > 0.60)
                {
                    //cout<<"USE THIS CLASSIFEIR"<<endl;
                    tsamp_data.estimates_all.insert(tsamp_data.estimates_all.end(),estimates_temp.begin(), estimates_temp.end());
                    tsamp_data.estimates_train_all.insert(tsamp_data.estimates_train_all.end(),estimates_train_temp.begin(), estimates_train_temp.end());
                    
                    
                }
                
                //<<"estimate all size "<<estimates_all.size()<<" "<<estimates_temp.size()<<endl;
            }
        }
        
        if (doWrite )
        {
            fsel.close();
            if  ((tsamp_data.writeLevel>1))
                fprederr.close();
        }
        cerr<<"average gen data accs "<<tsamp_data.genDataThresh_.size()<<" "<<tsamp_data.model_thresh_mode<<endl;
        // float avg_genThres=mean<float,float>(tsamp_data.genDataThresh_);
        
        if ((!tsamp_data.genDataThresh_.empty()  ) && (tsamp_data.model_thresh_mode == 1 ))
        {
            cerr<<"do MOG "<<tsamp_data.model_thresh_mode<<endl;
            
            unsigned int rmax,cmax;
            float rres(0.05),cres(0.05);
            //            float max_acc=0;
            {//lets try some MOG modelling
                unsigned int N,M;
                N=tsamp_data.genDataThresh_.back().MRows;
                M=tsamp_data.genDataThresh_.back().NCols;
                cerr<<"MN "<<N<<" "<<M<<endl;
                vector<float> v_mu1,v_mu2;
                float mu_max=0;
                
                for ( unsigned int col = (M-1) ; col < M ; ++col)
                {
                    cerr<<"new col "<<endl;
                    for (unsigned int row = 0 ; row < N ; ++row)
                    {
                        vector<float> v_data(tsamp_data.genDataThresh_.size());
                        unsigned int index = 0 ;
                        for (vector<float>::iterator i_d = v_data.begin() ; i_d != v_data.end(); ++i_d,++index)
                        {
                            *i_d = tsamp_data.genDataThresh_[index].value(row,col);
                        }
                        //                        cerr<<"row/col "<<row<<" "<<col<<endl;
                        //                        print(v_data,"v_data");
                        float mu1,mu2,var1,var2;
                        GaussianMixtureModel_2cl(v_data, 100, mu1, mu2, var1, var2);
                        
                        float grot = (mu1>mu2) ? mu1 : mu2;
                        if ((mu_max < grot) || ((mu_max==grot)&&(col>cmax)))
                        {
                            mu_max=grot;
                            rmax=row;
                            cmax=col;
                        }
                        //                        cerr<<"MOG "<<mu1<<" "<<mu2<<" "<<var1<<" "<<var2<<endl;
                        v_mu1.push_back(mu1);
                        v_mu2.push_back(mu2);
                        
                    }
                    {
                        print(v_mu1,"v_mu1");
                        print(v_mu2,"v_mu2");
                        cerr<<"max "<<rmax*rres<<" "<<(cres+1)*cmax<<" "<<endl;
                        
                        //                        cerr<<"max "<<rmax<<" "<<cmax<<" "<<mu_max<<endl;
                        
                        tr_acc_thresh_=rmax*rres;
                        //                        tr_acc_thresh_=rmax*rres;
                        //                        tr_acc_thresh_=0.65;
                        n_tr_acc_thresh_=0;
                        tsamp_data.acc_thresh=cres*(cmax+1);
                        
                        
                    }
                    
                }
                
                
                
            }
        }
        //       cerr<<"Mean Gen Data Thresh "<<avg_genThres<<endl;
        //        if (doWrite )
        cerr<<"thresholds "<<tr_acc_thresh_<<" "<<tsamp_data.acc_thresh<<endl;
        //        postProcess(v_mask, doBagging, AccSensSpec,predictions  ,v_withinGrErr,estimates_train_all, estimates_all,estimated_distance_scores,class_sensitivities, v_tp_fp_tn_fn,v_tp_fp_tn_fn_thresh, acc_thresh,stratified_labels ,l_accuracy_pergroup, output_name );
        postProcess(v_mask, doBagging,weightBagByProb, tsamp_data, output_name );
        
        return 0;
        
    }
    
    
    //    int TreeSampler::minGroupSubsampling(  unsigned int  B,  const bool & doBagging, const float & Noffset ,const std::vector<int> & v_mask, std::vector< pair<float3,float3> > & AccSensSpec, vector< vector<int> > & predictions, std::vector< std::map<int,std::pair<float,unsigned int> > > & class_sensitivities, std::vector< pair<TpFpTnFn,TpFpTnFn> > & v_tp_fp_tn_fn, std::vector< pair<TpFpTnFn,TpFpTnFn> > & v_tp_fp_tn_fn_thresh, const float & acc_thresh, const vector<int> & stratified_labels ,std::vector< float3 > & v_withinGrErr,std::list< std::pair<float,unsigned int> > & l_accuracy_pergroup,  int Kfold_CV, string output_name )
    int TreeSampler::minGroupSubsampling( unsigned int B ,  const bool & doBagging, const bool & weightBagByProb, const float & Noffset, const std::vector<int> & v_mask,  TreeSampleData & tsamp_data, std::string output_name )
    {
        //Noffset is the number of subjects per group less the min group sizesize of subsample
        //B is the number of bootstrap samples.
        
        //    if (tsamp_data.model_thresh_mode == 2 )
        gaia_->setDoGenAccKFold(false);
        //    else
        //        gaia_->setDoGenAccKFold(true);
        
        
        
        if (Verbose)
        {
            cout<<"TreeSampler::minGroupSubsampling ..."<<endl;
            
            cerr<<"Output : "<<output_name<<endl;
        }
        //        cerr<<"treesampler mask size"<<v_mask.size()<<endl;
        
        vector<unsigned int> N_per_class;
        if (v_mask.empty())
            N_per_class= gaia_->getNumberOfObservationsPerClass();
        else
            N_per_class= gaia_->getNumberOfObservationsPerClass(v_mask);
        
        print(N_per_class,"N_per_class");
        
        unsigned int NperClass_min=N_per_class[0];
        vector<int> labels= gaia_->getClassLabels();
        //Check to make sure that all class size are greater than specfied
        vector<int>::iterator i_lb=labels.begin();
        for (vector<unsigned int>::iterator i = N_per_class.begin(); i != N_per_class.end();++i,++i_lb)
        {
            for (vector<int>::const_iterator i_strat=tsamp_data.stratified_labels.begin();i_strat != tsamp_data.stratified_labels.end();++i_strat)
                if (*i_strat == *i_lb)
                {
                    if ( (*i) < NperClass_min ) NperClass_min=*i;
                    
                    break;
                }
            
        }
        if (Verbose)
            cout<<"Sampling from each group "<<NperClass_min<<endl;
        // print(gaia_->getTargetData(),"tree target data");
        //        print(N_per_class,"NperClass");
        unsigned int sample_size=NperClass_min;
        if ((Noffset>0) && (Noffset<1))//use fractional version
        {
            sample_size*=Noffset;
        }else if (Noffset >= 1){   //specific number of subjects less than min group size
            sample_size-=Noffset;
        }else if ( Noffset != -1 ) //-1 is special condition to use whole sample, i.e. no cross-validation
        {
            throw ClassificationTreeException("Invalid value for Subsample Offset. Valid values are -1, 0<N<1, N>1");
            
        }
        
        //        absoluteSubsample( B,doBagging, sample_size, v_mask,AccSensSpec,  predictions,class_sensitivities, v_tp_fp_tn_fn,v_tp_fp_tn_fn_thresh, acc_thresh, stratified_labels, v_withinGrErr, l_accuracy_pergroup, Kfold_CV, output_name);
        if (Verbose)
            cerr<<"Run "<<tsamp_data.Kfold<<"-Fold :: "<<B<<" :: "<<doBagging<<" :: "<<sample_size<<endl;
        
        cerr<<"subsamplefileName "<<tsamp_data.subSamplesFilename<<endl;
        //        if (tsamp_data.subSamplesFilename.empty())
        absoluteSubsample( B,doBagging, weightBagByProb, sample_size, v_mask,tsamp_data, output_name);
        //else
        //    absoluteSubsamplePreSelect( B,doBagging, weightBagByProb, sample_size, v_mask,tsamp_data, output_name);
        
        return 0;
    }
    
    
    
}
