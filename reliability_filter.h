#ifndef Reliability_Filter_H
#define Reliability_Filter_H
/*
 *  reliability_filter.h
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 11/11/10.
 *  Copyright 2010 NRI. All rights reserved.
 *
 */
#include <ClassificationTreeNode.h>
#include <sstream>
namespace su_mvdisc_name{
	enum Inc_Criteria { NoExclusions,InBounds, NotInBounds, GreaterThanMin, LessThanMax, LessThanMin, GreaterThanMax };
	enum TestDataMethod { None, WihtinSample, OutOfSample };//used to incoprate accuracy masking
	enum RF_SampleMethod { BOOTSTRAP };
	enum RF_mode { FEATURE_SELECTION, SUBJECT_SELECTION };
	//enum RF_Output_Mode { FILTER_MODE, DISCRIMINANT_MODE };

	class Reliability_Filter : public ClassificationTreeNode
	{
	public:
		Reliability_Filter();
	//	Reliability_Filter( ClassificationTreeNode* data_filter );
		virtual ~Reliability_Filter();

	//	void setNode( ClassificationTreeNode* data_filter );
		void runNode(bool with_estimation = true);
	//	virtual void runSpecificFilter();
		void setOptions( std::stringstream & ss_options);
        //void setIOoutputSpecificFeatureSelect()

	protected:
		
	private:
		enum Node_Type { nt_Unspecified, nt_GLM_Filter, nt_SVM };
		enum RandSeedInit { ZERO, RANDOM };
	
		RF_mode filterMode;
		RF_SampleMethod sampleMethod;
		TestDataMethod testMethod;
		unsigned int B;
		float rf_thresh;
		float ci_alpha;
		float accu_thresh;
		bool useAbsoluteValue;
		//ClassificationTreeNode* filter_object;
		std::stringstream node_options;
		Inc_Criteria rf_inc_crit;
		Node_Type filter_t;
		RandSeedInit seed_init;
		
	};
}


#endif
