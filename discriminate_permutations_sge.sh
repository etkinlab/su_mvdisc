#!/bin/sh

#  discriminate_permutations.sh
#  su_mvdisc
#
#  Created by Brian Patenaude on 9/26/11.
#  Copyright 2011 Stanford University - Etkin Lab. All rights reserved.

Usage(){
    echo ""
    echo "discriminate_permutations [-no_sge] [-loadSubSamples PermsDirectory ][-run_missing] <output directory> <Number of Permutations>"
    echo ""

    exit 1
}

#total number of permuations
NPERMS=0
#what number to start at
NPERMS_OFFSET=0
RUN_MISSING=0
LOADSAMP=0
LOADSAMP_DIR=""
LOADSAMP_ARGS=""

RUN_SGE=1;
DISC_DIR=""
BAGGING_ARGS=""

scount=0
valid_input=1
while [ $valid_input = 1 ]; do
    valid_input=0
    if [ $1 = -run_missing ] ; then
        valid_input=1

        RUN_MISSING=1
        let scount+=1
        shift 1
    elif [ $1 = -loadSubSamples ] ; then
        valid_input=1
        LOADSAMP=1
        LOADSAMP_DIR=$2
        let scount+=1
        shift 2
    elif [ $1 = -no_sge ] ; then
        valid_input=1
        RUN_SGE=0
        let scount+=1
        shift 1
    fi

done


if [ $# -ne 2 ] ; then 
    echo ""
    echo "Invalid number of input : $#"
    echo "\n\n"
    echo "-run_missing : reruns missing permutations"
    Usage
fi



DISC_DIR=$1
shift 1
NPERMS=$1
shift 1


if [ ! -d $DISC_DIR ] ; then 
    echo ""
    echo "$DISC_DIR does not exist"
    Usage  
fi

if [ $NPERMS -le 0 ] ; then
    echo ""
    echo "Invalid number of permutations : $#"
    Usage
fi

DISC_CONFIG_FILE=${DISC_DIR}/config.txt
NSUBSAMPLES=`grep NSUBSAMPLES $DISC_CONFIG_FILE  | awk '{ print $2 }'`
MINOFFSET=`grep MINOFFSET $DISC_CONFIG_FILE | awk '{ print $2 }'`
LeaveN=`grep LeaveN $DISC_CONFIG_FILE  | awk '{ print $2 }'`
BAGGING=`grep BAGGING $DISC_CONFIG_FILE | awk '{ print $2 }'`
USE_FILL=`grep FillMissingData $DISC_CONFIG_FILE | awk '{ print $2 }'`
FILL_METH=`grep FillMissingData $DISC_CONFIG_FILE | awk '{ print $3 }'`
WEIGHT_BY_PROB=`grep WEIGHT_BY_PROB $DISC_CONFIG_FILE | awk '{ print $2 }'`

if [ $BAGGING = 1 ] ; then 
    BAGGING_ARGS="--bag --bag_mid_thresh=0.5"
#--bag --bag_mid_thresh=$BAG_MID_TH
fi



INFO_FILE=${DISC_DIR}/info.txt
echo "Discriminant Information File" >  $INFO_FILE

#target file (i.e. class labels)
TARGET_FILE=${DISC_DIR}/target.txt 

#group map file
GROUP_MAP=${DISC_DIR}/group_map.txt

#CLASS MAP
CLASS_MAP=${DISC_DIR}/class_map.txt

ls $DISC_DIR $CLASS_MAP

INTERP_MAP=${DISC_DIR}/interp_class_map.txt
SENS_FILE=${DISC_DIR}/sens.txt
#disriminant tree
TREE_FILE=${DISC_DIR}/tree.txt
LOG_FILE=${DISC_DIR}/perms.log
SGECOM_FILE=${DISC_DIR}/perms_sge

#want a new a unique command file
while [ -f ${SGECOM_FILE}.com ] ; do 
    SGECOM_FILE=${SGECOM_FILE}+
done
SGECOM_FILE=${SGECOM_FILE}.com


REG_FILE=${DISC_DIR}/regressors.txt


DISCOUT_DIR=${DISC_DIR}/Leave-${LeaveN}-Out.perms
SELMASKARGS=`grep selection_mask ${DISC_DIR}/config.txt `



DISCBINDIR=`readlink -f  $0`
DISCBINDIR=`dirname $DISCBINDIR`




if [ $RUN_MISSING = 0 ] ; then 

    if [ ! -d ${DISC_DIR}/Leave-${LeaveN}-Out.perms ] ; then 
        /bin/mkdir ${DISC_DIR}/Leave-${LeaveN}-Out.perms
    else

    #FOR NOW ONLY WORKS FOR SUBSAMPLING WITH BAGGING

        NExist=`ls ${DISCOUT_DIR}/cv_results_disc_perm*_errordata.csv| wc | awk '{ print $1 }'`
        echo "Permutations already exist : $NExist"
        count=0
    #check to make sure everything is in right order
        while [ $count -lt $NExist ] ; do
            if [ ! -f ${DISCOUT_DIR}/cv_results_disc_perm${count}_errordata.csv ] ; then 
                echo "Permutations exist but they are not sequential. Strange things are afoot at the Circle-K."
                echo "P.S. That was a Bill & Ted's Excellent Adventure reference"
                exit 1
            fi
            let count+=1
        done

        NPERMS_OFFSET=$NExist
        if [ $NPERMS_OFFSET -ge $NPERMS ] ; then 
            echo "Exiting, you have already run the desired number of permutations"
            exit 1
        fi
        NPERMSDIF=`echo "$NPERMS - $NPERMS_OFFSET" | bc`

    fi
    echo "I have $NPERMSDIF permutations to run "

    PERM_LIST=${DISCOUT_DIR}/perms.list

    PERM_COUNT=$NPERMS_OFFSET;
echo "Running subjects..."

    while [ $PERM_COUNT -ne $NPERMS ] ; do 
        echo $PERM_COUNT
    # echo "${ETKINLAB_DIR}/bin/discriminate -i $TREE_FILE -o ${DISCOUT_DIR}/cv_results_disc --minimumOffset=${MINOFFSET} --Nsubsamples=${NSUBSAMPLES} --sens_spec_class_inclusion=${SENS_FILE} --LeaveNout=${LeaveN} --Npermutations=${NPERMS} --Npermutations_offset=${NPERMS_OFFSET}" >> $LOG_FILE
LEAVE_N_ARG=""
if [ ! $LeaveN = 0 ] ; then
LEAVE_N_ARG="--LeaveNout=${LeaveN}"
fi

LOADSAMP_ARGS=""
if [ $LOADSAMP = 1 ]; then
#    if [ -f ${LOADSAMP_DIR}/cv_results_disc_perm${PERM_COUNT}_subsamples_sel_mask.csv ] ; then
        LOADSAMP_ARGS="--loadSamples=${LOADSAMP_DIR}/cv_results_disc_perm${PERM_COUNT}_subsamples_sel_mask.csv --loadPermutedTarget=${LOADSAMP_DIR}/cv_results_disc_perm${PERM_COUNT}_permutedTarg.csv"
#   else
#       echo "Could not load ${LOADSAMP_DIR}/cv_results_disc_perm${PERM_COUNT}_subsamples_sel_mask.csv, does not exist."
#       exit 1
#   fi
fi

        echo "${DISCBINDIR}/discriminate  -i $TREE_FILE -o ${DISCOUT_DIR}/cv_results_disc_perm${PERM_COUNT} --minimumOffset=${MINOFFSET} --Nsubsamples=${NSUBSAMPLES} --sens_spec_class_inclusion=${SENS_FILE} ${LEAVE_N_ARG} --Npermutations=1 --Npermutations_offset=${PERM_COUNT} ${SELMASKARGS} ${BAGGING_ARGS} -K 0 $LOADSAMP_ARGS " >> ${SGECOM_FILE}
#/Volumes/EtkinLab_Data/SoftwareRepository/etkinlab/developer/git/su_mvdisc/build/Release/discriminate -i $TREE_FILE -o ${DISCOUT_DIR}/cv_results_disc_perm${PERM_COUNT} --minimumOffset=${MINOFFSET} --Nsubsamples=${NSUBSAMPLES} --sens_spec_class_inclusion=${SENS_FILE} --LeaveNout=${LeaveN} --Npermutations=1 --Npermutations_offset=${PERM_COUNT} 
#echo ${DISCOUT_DIR}/cv_results_disc_perm${PERM_COUNT}_perm_per_subject_stats_predX.csv >> $PERM_LIST

#${DISCDIR}/discriminate -i $TREE_FILE -o ${DISCOUT_DIR}/cv_results_disc --minimumOffset=${MINOFFSET} --Nsubsamples=${NSUBSAMPLES} --sens_spec_class_inclusion=${SENS_FILE} -K ${KFOLD} ${VERBOSE} $BAGGING_ARGS $MODEL_THRESH_OPTS $SELMASK_ARGS $WEIGHT_BY_PROB $LOAD_ARGS



        let PERM_COUNT+=1
    done
else 
    if [ ! -d ${DISC_DIR}/Leave-${LeaveN}-Out.perms ] ; then 
        echo "trying to run missing permutation but output directory,${DISC_DIR}/Leave-${LeaveN}-Out.perms, does not exist!"
        exit 1
    fi

    PERM_COUNT=0;
    echo "Searching for subject..."

    while [ $PERM_COUNT -ne $NPERMS ] ; do 
        echo $PERM_COUNT ...
        # echo "${ETKINLAB_DIR}/bin/discriminate -i $TREE_FILE -o ${DISCOUT_DIR}/cv_results_disc --minimumOffset=${MINOFFSET} --Nsubsamples=${NSUBSAMPLES} --sens_spec_class_inclusion=${SENS_FILE} --LeaveNout=${LeaveN} --Npermutations=${NPERMS} --Npermutations_offset=${NPERMS_OFFSET}" >> $LOG_FILE
#        if [ ! -f ${DISCOUT_DIR}/cv_results_disc_perm${PERM_COUNT}_perm${PERM_COUNT}_per_subject_stats_predX.csv ] ; then

        if [ ! -f ${DISCOUT_DIR}/cv_results_disc_perm${PERM_COUNT}_errordata.csv ] ; then 
#echo "/Volumes/Smurf-Village/SoftwareRepository/etkinlab/git/su_mvdisc/build/Release/discriminate  -i $TREE_FILE -o ${DISCOUT_DIR}/cv_results_disc_perm${PERM_COUNT} --minimumOffset=${MINOFFSET} --Nsubsamples=${NSUBSAMPLES} --sens_spec_class_inclusion=${SENS_FILE} --LeaveNout=${LeaveN} --Npermutations=1 --Npermutations_offset=${PERM_COUNT} ${SELMASKARGS} ${BAGGING_ARGS} -K 0" >> ${SGECOM_FILE}
LEAVE_N_ARG=""
if [ ! $LeaveN = 0 ] ; then
    LEAVE_N_ARG="--LeaveNout=${LeaveN}"
fi

LOADSAMP_ARGS=""
if [ $LOADSAMP = 1 ]; then
#   if [ -f ${LOADSAMP_DIR}/cv_results_disc_perm${PERM_COUNT}_subsamples_sel_mask.csv ] ; then
        LOADSAMP_ARGS="--loadSamples=${LOADSAMP_DIR}/cv_results_disc_perm${PERM_COUNT}_subsamples_sel_mask.csv --loadPermutedTarget=${LOADSAMP_DIR}/cv_results_disc_perm${PERM_COUNT}_permutedTarg.csv"
#   else
#       echo "Could not load ${LOADSAMP_DIR}/cv_results_disc_perm${PERM_COUNT}_subsamples_sel_mask.csv, does not exist."
#       exit 1
#   fi
fi
echo "${DISCBINDIR}/discriminate  -i $TREE_FILE -o ${DISCOUT_DIR}/cv_results_disc_perm${PERM_COUNT} --minimumOffset=${MINOFFSET} --Nsubsamples=${NSUBSAMPLES} --sens_spec_class_inclusion=${SENS_FILE}  ${LEAVE_N_ARG} --Npermutations=1 --Npermutations_offset=${PERM_COUNT} ${SELMASKARGS} ${BAGGING_ARGS} -K 0 $LOADSAMP_ARGS" >> ${SGECOM_FILE}
            echo "Need to rerun"
        else
            echo "Found"
        fi

        let PERM_COUNT+=1
    done



fi

ID=`fsl_sub -l logs -N CV-perms -q short.q  -t ${SGECOM_FILE}`

echo "JOBID : $ID"
#ls ${DISCOUT_DIR}/cv_results_disc_perm*_per_subject_stats_predX.csv > $PERM_LIST
#N=`wc $PERM_LIST | awk '{ print $1 }'`
#fsl_sub -N Interpret-Perms -q matlab.q -j $ID ~/src/etkinlab_utils/src/su_mvdisc/build/Release/discriminate_interpreter -i ${PERM_LIST} --inputType=1 ${BAGGING_ARGS} --class_map=$INTERP_MAP --sens_spec_class_inclusion=$SENS_FILE --p_sig=0.05 -o ${DISCOUT_DIR}/results_${N}perms_p0.05.csv


#   echo "~/src/etkinlab_utils/src/su_mvdisc/build/Release/discriminate_interpreter -i ${PERM_LIST} --inputType=1 ${BAGGING_ARGS} --class_map=$INTERP_MAP --sens_spec_class_inclusion=$SENS_FILE --p_sig=0.05 -o ${DISCOUT_DIR}/results_${N}perms_p0.05.csv" >> $LOG_FILE
 
#   ~/src/etkinlab_utils/src/su_mvdisc/build/Release/discriminate_interpreter -i ${PERM_LIST} --inputType=1 ${BAGGING_ARGS} --class_map=$INTERP_MAP --sens_spec_class_inclusion=$SENS_FILE --p_sig=0.05 -o ${DISCOUT_DIR}/results_${N}perms_p0.05.csv


