/*
 *  svm_disc.h
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 7/16/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef svm_mvdisc_
#define svm_mvdisc_


#include <vector>
#include <string>
#include "base_mvdisc.h"
#include "svm.h"
#include "nifti_writer.h"


namespace su_mvdisc_name {
	


//#pragma GCC visibility push(default)

class SVM_mvdisc : public base_mvdisc
{
public:
	SVM_mvdisc();
	~SVM_mvdisc();
	enum kernelType { LINEAR , POLYNOMIAL, RBF, SIGMOID };

	void estimateParameters();
    void displaySupportVector();

	//void setParameters();
	void setParameters( const std::vector<float> & params );
	void setParameters( const std::vector<float> & params, const std::vector<unsigned int> & mask );
	
	std::vector<float> getParameters();
    void saveParameters( );

	//this adds parameters to 
	void getParameters( std::vector< std::list<float> > & all_params );
	
	int classifyTrainingData()  ;
	int classifyTestData()  ;
    void writeStats() const;


	void setOptions( std::stringstream & ss_options);

	int set_SVM_type( const std::string & type );	
	int set_kernel_type( const std::string & type );
	
protected:
   // void setIOoutputSpecific();
    void runTrackStats();

	
private:
	
	std::vector<svm_node> vectorToSVMnodes( const std::vector<float> & vec ) const ;
	std::vector<svm_node> vectorToSVMnodes( const std::vector<float> * vec ) const ;

	
	svm_problem libsvm_data;
	svm_parameter libsvm_parameter;
	svm_model* libsvm_model;

	//used for entering data into libsvm format
		
	//std::vector< std::vector<svm_node>* > svm_training_data_tracker;
	void delete_svmTrainingData();
	std::vector<svm_node*> svm_training_data;

    std::vector<float> *feature_w_sx_, *feature_w_sxx_;
    std::vector<float> *featureAbs_w_sx_, *featureAbs_w_sxx_;


    
    
    
	bool UseAsFilter;
	std::string name;	
	std::vector< std::vector<float> > means; //
	std::vector< float > Sigma; //common covariance matrix 
	
	
};
}
//#pragma GCC visibility pop
#endif
