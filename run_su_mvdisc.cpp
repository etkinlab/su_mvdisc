open ru/*
 *  run_su_mvdisc.cpp
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 7/15/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#include "lda.h"
#include "svm_disc.h"
#include <iostream>
#include <vector>
using namespace std;
using namespace su_mvdisc_name;
int main( int argc, const char * argv[])
{
	//LDA_mvdisc* mydisc = new LDA_mvdisc();
	SVM_mvdisc* mydisc = new SVM_mvdisc();

//	mydisc->HelloWorld("blah");

	vector<float> test_vec;
	
	vector<float> v1, v2;
	vector<unsigned int> Mdims;
	int M=5;
	for ( int i=1; i<3; i++)
		for ( int j=0; j<M; j++)
		{
			v1.push_back(i*i + static_cast<float>(rand())/RAND_MAX*0.1);
		}
	Mdims.push_back(M);
	
	M=7;
	for ( int i=1; i<3; i++)
		for ( int j=0; j<M; j++)
		{
			v2.push_back(i*i + static_cast<float>(rand())/RAND_MAX*0.1);
		}
	Mdims.push_back(M);
	vector<int> target;
	target.push_back(0);
	target.push_back(1);

	vector< vector<float> > all_sources;
	all_sources.push_back(v1);
	all_sources.push_back(v2);
	
	mydisc->setTrainingData(all_sources,target,Mdims,2);
	mydisc->displayTrainingData();
	mydisc->estimateParameters();

	//set up test vector
	for (vector<unsigned int>::iterator i=Mdims.begin();i!=Mdims.end();i++)
		for (unsigned int j=0;j<*i;j++)
			test_vec.push_back(3.5);
	
	int cl=mydisc->classify(test_vec);

	cout<<"test vector: "<<endl;
	for (vector<float>::iterator i=test_vec.begin();i!=test_vec.end();i++)
		cout<<*i<<" ";
	cout<<endl;
	cout<<"Classified as: "<<cl<<endl;
	
	
	delete mydisc;
}
