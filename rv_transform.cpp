//
//  rv_transform.cpp
//  su_mvdisc
//
//  Created by Brian M. Patenaude on 7/23/12.
//  Copyright 2012 Stanford University - Etkin Lab. All rights reserved.
//

#include "rv_transform.h"
#include <misc_utils.h>
#include <cmath>
using namespace std;
using namespace misc_utils_name;

namespace su_mvdisc_name{
	RV_Transform::RV_Transform()
	{
		NodeName="RV_Transform";
		_xfm_targ=FEATURES;
	}
	
	RV_Transform::~RV_Transform(){}
	
	void RV_Transform::setXFMMode( XFM_MODE mode )
	{
		transformation_method = mode;
		
	}
	
	void RV_Transform::setXFMMode( const string & mode )
	{
		if		(	mode == "square"	)
			setXFMMode(SQUARE);
		else if	(	mode == "sqrt"	)
			setXFMMode(SQRT);
		else if (	mode == "log"	)
			setXFMMode(LOG);
		else if (	mode == "invlog"	)
			setXFMMode(INVLOG);
		else {
			throw ClassificationTreeNodeException( ("Unrecognized Transformation Mode Specified: "+mode).c_str());
		}
        
	}
    void RV_Transform::setXFMTarg( const string & mode )
	{
		if		(	mode == "features"	)
            _xfm_targ=FEATURES;
		else if	(	mode == "target"	)
            _xfm_targ=TARGET;
        else {
			throw ClassificationTreeNodeException( ("Unrecognized Transformation Target Specified: "+mode).c_str());
		}
        
	}
	
	void RV_Transform::setOptions( std::stringstream & ss_options)
	{
		map<string,string> options = parse_sstream(ss_options);
		for ( map<string,string>::iterator it = options.begin(); it!=options.end();it++)
			if ( it->first == "xfmMode")
			{
				setXFMMode( it->second );
			}else if (	it->first == "thresh"	){
                setXFMMode(THRESH);
                if (xfm_params.empty())
                    xfm_params.clear();
                float val;
                stringstream ss;
                ss<<it->second;
                ss>>val;
                xfm_params.push_back(val);
            }else if ( it->first == "xfmTarg")
                    {
                        setXFMTarg( it->second );
            }else {
				if (!setTreeNodeOptions(it->first, it->second))
                    throw ClassificationTreeNodeException((NodeName + " : Invalid input argument : " + it->first).c_str());
			}
        
		
	}
	void RV_Transform::applyParametersToTrainingData()
	{
        //cerr<<"RV_Transform::applyParametersToTrainingData - begin"<<endl;
        
		if (out_data == NULL)
			out_data = new vector<float>();
        else
        {
            out_data->clear();
            out_data->resize(NumberOfSamples_);

        }
		copyToSingleVector( in_data, out_data, NumberOfSamples_ );
        
        //  print<float>(*out_data,"RV-out_data");
        
        
        if (_xfm_targ == FEATURES)
        {
        if (transformation_method == SQUARE)
        {
            for (vector<float>::iterator i = out_data->begin(); i != out_data->end(); ++i)
                *i=(*i)*(*i);
        }else  if (transformation_method == SQRT)
        {
            for (vector<float>::iterator i = out_data->begin(); i != out_data->end(); ++i)
            {
                cerr<<"sqrttrain "<<*i<<" "<<sqrtf(*i)<<endl;
                *i=sqrtf(*i);
            }
        }else  if (transformation_method == LOG)
        {
            for (vector<float>::iterator i = out_data->begin(); i != out_data->end(); ++i)
                *i=logf(*i);
        }else  if (transformation_method == INVLOG)
        {
            for (vector<float>::iterator i = out_data->begin(); i != out_data->end(); ++i)
                *i=expf(*i);
        }else  if (transformation_method == THRESH)
        {
            float val = xfm_params[0];
            for (vector<float>::iterator i = out_data->begin(); i != out_data->end(); ++i)
                *i= (*i > val) ? 1 : 0 ; 
        }else{
            throw ClassificationTreeNodeException("RV_Transform invalid transform specified");
        }
        }else if (_xfm_targ == TARGET){
            if (transformation_method == THRESH)
            {
                float val = xfm_params[0];
                for (vector<float>::iterator i = training_target->begin(); i != training_target->end(); ++i)
                {
                    //   cerr<<"i1 "<<*i<<" "<<val<<endl;
                    *i= (*i > val) ? 1 : 0 ;
                    //cerr<<"i2 "<<*i<<" "<<val<<endl;
                
                }
                    training_target->calculateClassInfo();
            }else{
                throw ClassificationTreeNodeException("XFM not compatible for application to target");
            }
        }
       // cerr<<"RV_Transform::applyParametersToTrainingData - end"<<endl;
        
		
	}
    void RV_Transform::applyToTestData()
	{
        if (Verbose)
            cout<<"RV_Transform::applyToTestData"<<endl;
		if (out_test_data == NULL)
			out_test_data = new vector<float>();
        else{
            out_test_data->clear();
            out_test_data->resize(NumberOfTestSamples_);

        }
        
        
		copyToSingleVector( in_test_data, out_test_data, NumberOfTestSamples_ );
       // print<float>(*out_test_data,"RV-out_test_data_data");

        if (_xfm_targ == FEATURES){
            if (transformation_method == SQUARE)
            {
                for (vector<float>::iterator i = out_test_data->begin(); i != out_test_data->end(); ++i)
                    *i=(*i)*(*i);
            }else  if (transformation_method == SQRT)
            {
                for (vector<float>::iterator i = out_test_data->begin(); i != out_test_data->end(); ++i)
                {
                    cerr<<"sqrttest "<<*i<<" "<<sqrtf(*i)<<endl;
                    *i=sqrtf(*i);
                }
            }else  if (transformation_method == LOG)
            {
                for (vector<float>::iterator i = out_test_data->begin(); i != out_test_data->end(); ++i)
                    *i=logf(*i);
            }else  if (transformation_method == INVLOG)
            {
                for (vector<float>::iterator i = out_test_data->begin(); i != out_test_data->end(); ++i)
                    *i=expf(*i);
            }else  if (transformation_method == THRESH)
            {
                float val = xfm_params[0];
                for (vector<float>::iterator i = out_test_data->begin(); i != out_test_data->end(); ++i)
                    *i= (*i > val) ? 1 : 0 ;
            }
            
        }else if (_xfm_targ == TARGET){
         
                //dont do anything
           //     float val = xfm_params[0];
             //   for (vector<float>::iterator i = training_target->begin(); i != training_target->end(); ++i)
               //     *i= (*i > val) ? 1 : 0 ;
           // }else{
            if (transformation_method != THRESH)
            {
                throw ClassificationTreeNodeException("XFM not compatible for application to target");
            }
        }
       // print<float>(*out_test_data,"RV-out_test_data_data-end");


	}
	
	
}
