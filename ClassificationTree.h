#ifndef ClassificationTree_H
#define ClassificationTree_H

/*
 *  ClassificationTree.h
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 7/20/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "ClassificationTreeNode.h"
#include "target.h"
#include "base_mvdisc.h"
#include <vector>
#include <string>
#include <sstream>
#include <map>
#include <list>
//#include <map>
#include <vector_structs.h>
#include <treeSampleData.h>
//#include <vector_functions.h>
namespace su_mvdisc_name{
	
	//Define exception for reporting errors.
	class ClassificationTreeException : public std::exception{
		
	public:
		ClassificationTreeException(const char* msg)
		{
			errmesg= msg;
		}
		
        const char* what() const throw()
		{
			//std::cout<<errmesg<<std::endl;
			return errmesg;
		}
    private:

        const char* errmesg;

	};
	
    //ClassificationTree is the processing pipeline. It uses a tree structure to model the pipeline. It starts at the top and recursivly
    //traverse to Tree to run each node in the pipeline. Typically a classifier at the top and data source(s) at the bottom.
    //The tree stucture allows spliting and merge of paths.
    class ClassificationTree
	{
		

	public:
 
		//Constructor
		ClassificationTree();
        //Constructor that takes a configuration file name as input.
        ClassificationTree( const std::string & filename );
        //Destructor
        ~ClassificationTree();
		
        //Indicator to clear old data or not
        enum clear_flag { NO_CLEAR, CLEAR };
        
        //Set the verbosenss of the tree. 0 is quiet. v = 3 id most verbose.
        void setVerbose( const int & v ) { Verbose_=v; }
        
        
        //-----------Access to proprties of the target----------//
        //Return the number of classes found in the target
        unsigned int getNumberOfClasses() const { return target->getNumberOfClasses(); }
    
        //Return a vector containing the true class labels. The size of vector is equal to the number of subjects
        std::vector<int> getClassLabels() const { return target->getClassLabels(); }
        
        //Returns a vector with counts of how many time a class is sampled. The size of the vector is equal to the number of classes.
        //The second version provide the same information given a specific subject selection specified by v_mask.
        std::vector<unsigned int> getNumberOfObservationsPerClass() const ;
        std::vector<unsigned int> getNumberOfObservationsPerClass( const  std::vector<int> & v_mask ) const;

        //Returns the total number of samples
        unsigned int getNumberOfSamples(){ return target->getNumberOfSamples(); }

        //Returns the target data in format class T. Internally, the targets are stored as floating point so that it can
        //accomodate regression method.
        template <class T>
        std::vector<T> getTargetData() { return target->getTargetData<T>(); }
        
        //Return an iterator to the target data (outcomes).
        std::vector<float>::iterator getTargetDataBegin() { return target->begin(); }
        std::vector<float>::iterator getTargetDataEnd() { return target->end(); }
        
        
        //------------------Access to Tree Data--------------------//
        
        //Access chunks. Chunks are grouping of the samples, particularly used for sampling/ cross-validation definitions
        std::vector<unsigned int> getInputChunks();
        
        
        //-------function that accesss output data--------
        //Set the estimate distance scores. This interprestation may change depending on the classifier. e.g. SVM is distance to the hyperplane
        void setDistanceScores(std::vector< std::vector< std::pair<int,float> > > &estimated_distance_scores_in ) { estimated_distance_scores = estimated_distance_scores_in; }
        
       
        
      
        template <class T>
        std::vector<T> getTargetDataFromSrc();
   
        //Get the TpFpTnFn structure that stores TP/FP/TN/FN. It also provide a method for returning accuracy,
        //sensitivity, specificty, positive prediction value, negative prediction value
        TpFpTnFn getGeneralizationData();
        
        
        
        clapack_utils_name::smatrix getGeneralizationDataThresh();

        //-------Tree Creation and Manipulation---------
        
        
        //-------Classification tree parameters
        //these control the behavior at the tree level
        
        bool storeGenData();
        bool storeGenDataThresh();

        
        //-------error checking -------/
        //calling this will envoke loading/reading of source data
        void check_Ns_Targ_eq_Ns_Src();

        //-----tree info -----//
        void setLevels();
        std::list< std::pair<unsigned int, std::string> > getLevelsAndNames();

        //-------------------//
        
		void load( const std::string & filename );
		void save( const std::string & filename );
		void writeOutput();
		
		void addDataSource( DataSource * src );
		void setTreeTop( const unsigned int & index );
        TreeNodeType getTreeTopType();
        TreeNodeType getTreeTopTypeSpec();
        std::string getTreeTopParameterFileName();
        
		void attachInputToLeaf( const std::vector<int> & index,  ClassificationTreeNode * new_leaf);
        void addRoot( ClassificationTreeNode * new_leaf);
        void attachToRoot( ClassificationTreeNode * new_leaf);
        
		void setTarget( Target * targ_in );
		void setNodeTargets( Target * targ_in );//used for subsampling, sets the targets of all the nodes without changinthe tree's target
		void setTargets();
        
        
        void setDoGenAccKFold(const bool & enabled );
        int getTargetData( const unsigned int & index ){ return target->getOutTarget( index ); }
        
        
		void run_leave_N_out(const unsigned int & N);
		void run_randomSubSampling_NoRep( const unsigned int & N );
		unsigned int run_KFold_CV( const unsigned int & N , const clear_flag & clear );
		unsigned int run_KFold_CV( const unsigned int & K , const std::vector<int> & mask, const std::vector<int> & test_mask, const clear_flag & clear, std::ofstream & flog_mask, int run_offset = 0 );
        
        void permuteTarget();
        void loadPermutedTarget(const std::vector<float> & v_targ );
        void permuteTarget( const std::vector<int> & v_sel_mask );

		void run_permutationTest( const unsigned int & Nperms, const std::string & output_base, const std::string & class_inc_name, unsigned int N , const unsigned int & K,const std::vector<int> & stratified_labels ,   const unsigned int & minSizeOffset, const unsigned int & perms_offset, bool USE_NON_STRATIFIED_SUBJECTS = true, bool USE_LEFTOUT_FOR_TEST =false , std::list< std::vector<int> >*  input_subsamplings = NULL );
        
        
        //if K=1 is entered it will run leave 1 out
		void run_subsampling( const std::string & output_base,  unsigned int  N , const unsigned int & K, const unsigned int & minSizeOffset, const unsigned int & sampleSizePerGroup );
		void run_subsampling( const std::string & output_base,  unsigned int  N , const unsigned int & K,const std::vector<int> & stratified_labels ,   const unsigned int & minSizeOffset,  unsigned int sampleSizeAbs = 0 , bool USE_NON_STRATIFIED_SUBJECTS = true, bool USE_LEFTOUT_FOR_TEST =false , std::list< std::vector<int> >*  input_subsamplings = NULL );

        std::vector<int> getStratifyLabels(); 
		
//		permutateLabels();
		std::vector< std::vector<float> > getAllClassAccuracies();
      //  std::map<int,std::pair<float,unsigned int> > getWithinClassAccuracies(const std::vector<int2> & estimates);
        std::map<int,std::pair<float,unsigned int> > getClassSensitivities(const std::vector<int2> & estimates,const std::vector<int> & strat_labels);
        std::map<int,std::pair<float,unsigned int> > getClassSensitivities(const std::vector<int2> & estimates,const std::vector<int> & strat_labels ,  const std::vector<int> & mask  , bool m_in = 1);

		std::vector<float> getAllAccuracies();
		float getAccuracy( const unsigned int & index );
        float getAccuracy( const std::vector<int3> & index );
        float getAccuracy( const std::vector<int4> & index );

        float getAccuracy( const std::vector<int2> & estimates );
        float getAccuracy( const std::vector<int2> & estimates ,const std::vector<int> & strat_labels );

        //use selection mask
        // float getAccuracy( const std::vector<int2> & estimates , const std::vector<bool> & mask );
        float getAccuracy( const std::vector<int2> & estimates ,const std::vector<int> & strat_labels , const std::vector<int> & mask , bool m_in = 1 );
        
        
        
        float getSensitivity( const std::vector<int2> & estimates );
        float getSensitivity( const std::vector<int2> & estimates,const std::vector<int> & strat_labels );
        //float getSensitivity( const std::vector<int2> & estimates, const std::vector<bool> & mask  );
        float getSensitivity( const std::vector<int2> & estimates,const std::vector<int> & strat_labels, const std::vector<int> & mask , bool m_in = 1 );

        float getSpecificity( const std::vector<int2> & estimates );
        float getSpecificity( const std::vector<int2> & estimates,const std::vector<int> & strat_labels );
        //        float getSpecificityMask( const std::vector<int2> & estimates, const std::vector<int> & mask  );
        float getSpecificity( const std::vector<int2> & estimates,const std::vector<int> & strat_labels, const std::vector<int> & mask  , bool m_in = 1 );
        
        
        
        //return TP,FP,TN,FN
        TpFpTnFn getErrorData( const std::vector<int2> & estimates,const std::vector<int> & strat_labels );
       TpFpTnFn getErrorData( const std::vector<int2> & estimates,const std::vector<int> & strat_labels , const std::vector<int> & mask , bool m_in = 1 );

		void predictionError( const unsigned int & index );
		std::vector<int> getPredictionsForClass( const int & cl );
		void outputPerSubjectStats(const std::string & filename);
		void outputPerSessionStats(const std::string & filename);

		
		static std::list< std::vector<int> > readSelectionMask( const std::string & filename  );
		static std::list< std::vector<int> > readSubSampleMask_from_SelMask( const std::string & filename  );

		
		        std::vector<int3> classify();

        std::vector<int3> classify(std::vector< std::pair<int,float> > & distance_scores,  const std::vector<int> selection_mask , const std::vector<int> selection_test_mask , std::vector<int4> & withinGroup, unsigned int offset);

		std::vector<int3> classify(  std::vector< std::pair<int,float> > & distance_scores, const std::vector< std::vector<int> >  selection_mask, const std::vector< std::vector<int> >  selection_test_mask , std::vector<int4> & withinGroup , unsigned int offset = 0);
	//	void classifyLeftOutData( const std::vector< std::vector<unsigned int> > & selection_mask  );
    protected:

		
	private:
        
        //Verbosity level. v >=0, where 0 is the minimum verbosity
		int Verbose_;

        //data sources are the data readers. These are the leaf nodes in our pipeline
		std::vector< DataSource* > data_sources;
        
        //data readers for test data. i.e. data that we wish to apply our classifiers to
		std::vector< DataSource* > test_data_sources;
        
        //all the nodes in the tree, except for the sources
		std::vector< ClassificationTreeNode* > tree_nodes;
		//store the pointer (same as in )

        //target contains the classification labels (or outcome values) of the training data
		Target *target;
        
        //test_target contains the classification labels (or outcome values) for the data that is being tested.
        //This is only being used for evaluation of the accuracy, its is not passed down the processing pipeline.
		Target *test_target;

        //Stores the estimated class outcomes. Depending on parameter settings this may contain outcomes for several
        //iterations
		std::vector< std::vector<int3> > estimated_class;

        //Stores the estimated distance associated with the outcome. it's meaning differs with the model.
		std::vector< std::vector< std::pair<int,float> > > estimated_distance_scores;
        
 
        
        //Create a node. ss_node would be a line from a configuration file read into a stringstream.
        void createNode( std::stringstream & ss_node );
        
        
        
		//recursively calculates, good for combinatorials, calculates all combinations
	//	void binary_combinations( std::list< std::vector<int> > & all_combinations, std::vector<int> & v1,  const std::vector< int > & labels,  int & count, const unsigned int & index, const int & k ) const ;
		
		//randomly chooses Nsamples different ways of selecting Nselect from Nobs
	//	void binary_combinations( std::list< std::vector<int> > & all_combinations, const std::vector<int> & labels, const unsigned int & Nsamples, const std::vector<unsigned int> & Nselect, const std::vector<int> & cl_labels ) const; 

		void clearAll();
		void clearOutputData();

		unsigned int treeTop;   //store index into tree_nodes where the top is located.
		unsigned int treeTop_count; //check are inplace to ensure that only one top exists (i.e. final classifier).
		unsigned int totalNumberOfIterations;
		std::map<std::string,int> map_node_names;

		
		
	};
	
}

#endif
