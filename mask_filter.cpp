//
//  mask_filter.cpp
//  su_mvdisc
//
//  Created by Brian Patenaude on 5/27/11.
//  Copyright 2011 Stanford University - Etkin Lab. All rights reserved.
//
#include <cstdlib>

#include "image_source.h"


#include "mask_filter.h"


//STL includes
#include <algorithm>
//#include <iostream>;
//#include <sstream>

//Custom includes
//#include <misc_utils/misc_utils.h>
//#include <misc_utils.h>
//#include <clapack_utils.h>
//#include <clapack_utils/clapack_utils.h>


//3rd party include 

//OSX include
//#include <vecLib/cblas.h>
//#include <vecLib/clapack.h>

//using namespace su_mvdisc_name;
using namespace std;
using namespace misc_utils_name;
//using namespace clapack_utils_name;
namespace su_mvdisc_name{
	Mask_Filter::Mask_Filter()
	{
		NodeName="Mask_Filter";
//		output_basename="output_"+NodeName;
		filt_mode=LABEL_SELECT;
		threshold=0;
        mask = NULL;
        bool readData;

		
		
	}
	
	Mask_Filter::~Mask_Filter()
	{
			}
	
	
	void Mask_Filter::setOptions( std::stringstream & ss_options)
	{
		map<string,string> options = parse_sstream(ss_options);
        for ( map<string,string>::iterator it = options.begin(); it!=options.end();it++)
            if ( it->first == "filename")
			{
				filename = it->second ;
                have_readData=false;
			}else if ( it->first == "threshold")
            {
                stringstream ss; 
                ss<<it->second;
                ss>>threshold;
            }else if ( it->first == "filter_mode")
            {
                if (it->second == "Label_Select"){
                    filt_mode = LABEL_SELECT;
                }else if (it->second == "Threshold") {
                    filt_mode = THRESHOLD;
                }else {
                    throw ClassificationTreeNodeException(("Mask_Filter: Invalid input into filter_mode, "+it->first+".").c_str());
                }
                
                
            }else if ( it->first == "label"){

                stringstream ss; 
                ss<<it->second;
                ss>>label_select;
                
            }else {
                if (!setTreeNodeOptions(it->first, it->second))
                    throw ClassificationTreeNodeException( (NodeName + " : Invalid input argument : " + it->first).c_str());
            }
		
		
	}

    void Mask_Filter::estimateParameters()
	{
		vector<float> betas;
//		if (Verbose)
//			cout<<"Estimating parameters for GLM_Filter... "<<Dimensionality_<<endl;
		DimTrim=0;

		if (out_data == NULL)
			out_data = new vector<float>();
		copyToSingleVector( in_data, out_data, NumberOfSamples_ );
	
        //don't want to re-read/applymask unless the mask has changed 
		if ((mask != NULL) && (!have_readData))
			delete[] mask;
		
		mask = new bool[Dimensionality_];
		//do post-multiplication for cBeta
        
        //ImageSource* imMask = new ImageSource();
        //imMask->setFileName(filename);
        
       // delete imMask;
		
        
        
        
		
	}

	void Mask_Filter::applyParametersToTrainingData()
	{
		//----------------------------filter training data being passed on to next node------------------------//-------------
		/*
		//--------------
		if (Verbose)
			cout<<"Applying Mask filter to training data...";
		//--------------
        
		if (out_data == NULL) {
			out_data = new vector<float>();
		}
		copyToSingleVector( in_data, out_data, NumberOfSamples_ );
        
		float thresh = threshold;
		float min=1e10;
		
        //	cout<<"Threshmode "<<thresh_mode<<endl;
		if ( thresh_mode == FRACTIONAL )
		{
			thresh *= max_F;
		}else if ( thresh_mode == ORDER_STATISTIC )
		{
			thresh = order_threshold = orderStatistic(F_stats, Dimensionality_, threshold);
			if (Verbose)
				cout<<"Using Order Statistic... "<<endl;
            
		}
		
		//apply trheshodl 
		//--------------
		if (Verbose)
			cout<<"F-threshold "<<thresh<<endl;
		//--------------
		
		vector<float>::iterator i_out = out_data->begin();
		vector<float>::iterator i_out_write = out_data->begin();
		for (unsigned int i = 0 ; i < NumberOfSamples_; ++i)
		{
			unsigned int count = 0;
			for (unsigned int j = 0 ; j < Dimensionality_; ++j,++i_out,++count)
			{
				if (F_stats[count] > max_F ) max_F = F_stats[count];
				if (F_stats[count] < min ) min = F_stats[count];
                
				
				if ((F_stats[count]>thresh) && (F_stats[count]!= -1))//threshold)
				{
					//cout<<"write dim "<<endl;
					*i_out_write=*i_out;
					++i_out_write;
				}else if (i==0)
					++DimTrim;
                
			}
		}
		
        
		
		
		if (Verbose)
			cout<<"Minimum/Maximum F "<<min<<"/"<<max_F<<endl;
		if (Dimensionality_==DimTrim)
			throw ClassificationTreeNodeException("GLM filter : Invalid threshold - no features survived threshold");
        
		Dimensionality_-=DimTrim;	
		
		if (Verbose)
			cout<<"Dimensionaltiy Reduction "<<Dimensionality_+DimTrim<<" ---> "<<Dimensionality_<<endl;
        
		
		out_data->resize(Dimensionality_*NumberOfSamples_);
        
		if (Verbose)
			cout<<"Done applying "<<NodeName<<" to training data."<<endl;
         */
	}
	
		
    
    
	void Mask_Filter::applyToTestData()
	{
        /*
		if (out_test_data == NULL)
			out_test_data = new vector<float>();
        
		//--------------------
		if (Verbose)
			cout<<"Applying GLM filter to test data..."<<endl<<"Number of Test Samples: "<<NumberOfTestSamples<<"."<<endl;
		//--------------------
        
		
		copyToSingleVector( in_test_data, out_test_data, NumberOfTestSamples );
        
		vector<float>::iterator i_out = out_test_data->begin();
		vector<float>::iterator i_out_write = out_test_data->begin();
		float thresh = threshold;
        
		
		if ( thresh_mode == FRACTIONAL )
			thresh = threshold * max_F;
		else if ( thresh_mode == ORDER_STATISTIC )
		{
			//this is very redundant
			//can't wriote over threshold because of recalculation of thresh
			thresh = order_threshold;
            //			thresh = orderStatistic(F_stats, Dimensionality_, threshold);
		}
		
        
        
		//--------------------
		if (Verbose)
			cout<<"Maximnum F / F threshold : "<<max_F<<" / "<<thresh<<endl;
		//--------------------
        
        
		for (unsigned int i = 0 ; i < NumberOfTestSamples; ++i)
		{
			unsigned int count = 0;
			for (unsigned int j = 0 ; j < Dimensionality_+DimTrim; ++j,++i_out,++count)
			{
				if ((F_stats[count]>thresh) && (F_stats[count]!= -1))//threshold)
				{
					*i_out_write=*i_out;
					++i_out_write;
				}
				
			}
		}
		out_test_data->resize(NumberOfTestSamples*Dimensionality_);
         */
	}
	
	
	
}
