#include <quantile_regression.h>

#include <cmath>
#include <misc_utils.h>
//#include <pair>
#include <stdio.h>
#include <list>
#include <smatrix.h>
#include <clapack_utils.h>
#include <string.h>

#ifdef ARCHLINUX
extern "C" {
#include "cblas.h"
#include "f2c.h"
    typedef integer __CLPK_integer;
    typedef real __CLPK_real;
#include "clapack.h"
}

#else
#include <Accelerate/Accelerate.h>
#endif

using namespace std;
using namespace misc_utils_name;
using namespace clapack_utils_name;



float machine_eps( float & mach_eps)
{
    mach_eps = 1.0f;
    float prec_fac=1.01f;
    //printf( "current Epsilon, 1 + current Epsilon\n" );
    do {
       // printf( "%G\t%.20f\n", machEps, (1.0f + machEps) );
  //      mach_eps /= 2.0f;
        mach_eps /= prec_fac;

        // If next epsilon yields 1, then break, because current
        // epsilon is the machine epsilon.
    }
//    while ((float)(1.0 + (mach_eps/2.0)) != 1.0);
    while ((float)(1.0 + (mach_eps/prec_fac)) != 1.0);

    printf( "\nCalculated Machine epsilon: %G\n", mach_eps );

    
    
    return 0;
}



#define TOL 1e-4

namespace su_mvdisc_name{

    
    QuantileRegression::QuantileRegression(){
        machine_eps(_MACH_EPS);
        NodeName="QuantileRegressionFilter";

        _InvertModel=false;
        _binarize_output=false;
        _threshold=0;
        _out_format = ALL_QUANTILES;
        _Ntau=9;
        _tau_min=0.1;
        _tau_max=0.9;
        _tau_res=0.1;
        _useOLS=false;
    }
    
    QuantileRegression::~QuantileRegression(){
        cerr<<"QuantileRegression::~QuantileRegression()"<<endl;
    }
    float quantile(const vector<float> & v_data,const float &  tau){
        
        cerr<<"quantie tau2 "<<tau<<endl;
        
        list<float> all_data(v_data.begin(),v_data.end());
        all_data.sort();
        float quant_opt;
        float cost_min=0;
        unsigned int index=0;
        for ( list<float>::iterator i_d = all_data.begin(); i_d != all_data.end(); ++i_d , ++index)
        {
            
            float cost=0;
            for ( list<float>::iterator i_d2 = all_data.begin(); i_d2 != all_data.end(); ++i_d2 )
            {
                if (*i_d2 > *i_d)
                    cost+=( *i_d2 - *i_d) * tau;
                else
                    cost+=( *i_d2 - *i_d) * (tau-1);
            }
            cerr<<"quant cost "<<cost<<endl;
            if (index==0)
            {
                cost_min=cost;
                quant_opt=*i_d;
            }else if  ( cost < cost_min ){
                cost_min = cost;
                quant_opt=*i_d;
            }
            
        }
                
        return quant_opt;
    }
//    float quantile(const vector<float> & v_data,const float &  tau){
//        cerr<<"quantie tau "<<tau<<endl;
//        float min=v_data.front();
//        float max=v_data.front();
//        
//        for (vector<float>::const_iterator i = v_data.begin(); i != v_data.end();++i)
//        {
//            if (*i <min) min=*i;
//            if (*i >max) max=*i;
//        }
//        float range=max-min;
//        unsigned int Nres=1000;
//        float res=range/Nres;
//        float quant_opt;
//        float cost_min=1e16;
//        for ( float quant = min; quant < max ; quant+=res)
//        {
//            float cost=0;
//            for (vector<float>::const_iterator i = v_data.begin(); i != v_data.end();++i)
//            {
//                if (*i > quant)
//                    cost+=( *i - quant) * tau;
//                else
//                     cost+=( *i - quant) * (tau-1);
//            }
//            if (cost<cost_min)
//            {
//                quant_opt=quant;
//                cost_min=cost;
//            }
//        }
//        
//        return quant_opt;
//    }
    
    void QuantileRegression::estimateTargetQuantiles(const std::vector<float> & targ_data )
    {
        cerr<<"estimate target quantiles2 "<<endl;
        print(_tau,"_tau2");
        _targ_quantiles.clear();
        for (vector<float>::iterator i_t = _tau.begin(); i_t != _tau.end(); ++i_t)
        {
            cerr<<"_tau2 "<<*i_t<<endl;
            _targ_quantiles.push_back(quantile(targ_data, *i_t));
        }
        print(_targ_quantiles,"_targ_quantiles");
    }
    
    
    float quant_cost( const float & tau, const vector<float> & v_x, const vector<float> & v_y, const float & B0, const float & B1 ){
    
        float res;
        float cost = 0;
        vector<float>::const_iterator i_x = v_x.begin();
        vector<float>::const_iterator i_y = v_y.begin();
      //  print(v_x,"v_x v_res");
    //    cerr<<"vres"<<endl;
        for (; i_x != v_x.end(); ++i_x,++i_y)
        {
            // *i_res = *i_y -B0 - B1*(*i_x); //model is (B0+B1x)               
            res= *i_y -B0 - B1*(*i_x); //model is (B0+B1x)                      
           //cerr<<"res "<<B0<<" "<<B1<<" "<<*i_y<<" "<<res<<" "<<*i_x<<endl;                                                   
    //        cerr<<res<<endl;
            cost+= ( res > 0 ) ? tau * res : -1*(1-tau)*res;//  absf(res);   
         //   cerr<<"cumcost "<<cost<<endl;
        }
      //  cerr<<endl;
        return cost;
  }
    float quant_cost( const float & tau, const smatrix & x, const smatrix & y, const smatrix & b){
        //cerr<<"do quantcost m "<<x.MRows<<" "<<x.NCols<<" "<<y.MRows<<" "<<y.NCols<<endl;
       // smatrix_print(b, "cost b ");
        unsigned int N  = x.MRows;
        smatrix m_res;
        smatrix_AxB(x,b, m_res);
        //cerr<<"done multiply"<<endl;
        m_res*=-1;
        //cerr<<"m_res "<<m_res.MRows<<" "<<m_res.NCols<<endl;
        
        m_res+=y;
        //cerr<<"m_res i "<<endl;
        float cost=0;
        for (unsigned int i = 0 ; i < N ; ++i)
        {
      //      cerr<<"m_res i "<<i<<" "<<x(i,0)<<" "<<y(i,0)<<" "<<m_res(i,0)<<endl;
            cost+= ( m_res(i,0) > 0 ) ? tau * m_res(i,0) : -1*(1-tau)*m_res(i,0);
            //cerr<<m_res(i,0)<<endl;
        }
        //cerr<<endl;
        //cerr<<"do quantcost m end "<<endl;
    
        return cost;
    }

float QuantileRegression::gridSearch( const vector<float> & v_x, const vector<float> & v_y, const float & tau, const float & B0min, const float & B0max, const float & B1min, const float & B1max, const float & stepB0, const float & stepB1, float & B0opt, float & B1opt )
    {
        
        float cost_min =  quant_cost( tau,v_x ,v_y, B0opt, B1opt  );
      //  cerr<<"costmin "<<cost_min<<" "<<stepB0<<" "<<stepB1<<endl;
        int count=0;
        for ( float b0 = B0min; b0<B0max; b0+=stepB0)
            for (float b1 = B1min; b1<B1max; b1+=stepB1,++count) 
        {
          //  cerr<<"bbb "<<b1<<" "<<stepB1<<endl;
           // if (count%100 ==0) cerr<<"grid search "<<b0<<" "<<b1<<" "<<B0max<<" "<<B1max<<endl; 
            float cost =  quant_cost( tau,v_x ,v_y, b0,  b1 );
            if (cost < cost_min)
            {
              //  cerr<<"new costmin "<<cost_min<<endl;

                cost_min=cost;
                B0opt=b0;
                B1opt=b1;
            }
            
            
        }
      //  cout<<"end grid search "<<endl;
        return cost_min;
    }
    
    pair<float,unsigned int>  QuantileRegression::lineSearch1D( const smatrix & x, const smatrix & y ,const smatrix & point_indices , const float & tau) const {
        ///x are predictors, y is response, point_indicies and points already include 
       // cerr<<"QuantileRegression::lineSearch1D "<<endl;
        unsigned int Nsamples=x.MRows;
        list< pair<float,unsigned int> > l_b_index;
        // cerr<<"linessearch1D"<<endl;

        for ( unsigned int i = 0 ; i < Nsamples; ++i)
        {
            if (fabs(y(i,0)) > _MACH_EPS )//this is here because we want to exclude 0 residuals
            {
                //            cerr<<i<<","<<y(i,0)/x(i,0)<<" ";
                if (fabs(x(i,0)) > _MACH_EPS )
                    l_b_index.push_back( pair<float,unsigned int>(y(i,0)/x(i,0),i) );
  
            }//else{
//                cerr<<"less than machine epsilon |"<<(y(i,0))<<"| < "<<_MACH_EPS<<endl;

  //          }//else
        }
        //        cerr<<endl;
        //      cerr<<"skip"<<endl;
        l_b_index.sort();
//           int count=0;
//        for (list< pair<float, unsigned int> >::iterator i = l_b_index.begin(); i != l_b_index.end(); ++i,++count)
//            cerr<<"initisorted "<<count<<"/"<<l_b_index.size()<<" "<<i->first<<" "<<i->second<<endl;

        float grad1 = -1* ( tau-0.5 )*smatrix_sum(x) +0.5 *smatrix_sum_abs(x) ;

        float dif_prev=0;
        pair<float,unsigned int> b_prev= pair<float,unsigned int>(0,0);
        for (  list< pair<float,unsigned int> >::iterator j = l_b_index.begin(); j != l_b_index.end(); ++j)
        {
            float grad2=0.0;
            
            for ( unsigned int i = 0 ; i < Nsamples; ++i)
                if ( y(i,0)/x(i,0) >= j->first ) 
                    grad2 += fabs(x(i,0));
            
            float cost=0;
            for ( unsigned int i = 0 ; i < Nsamples; ++i)
            {
                float grot=y(i,0)-(j->first)*x(i,0);
                if (grot<0) 
                    grot=fabs(grot)*(1-tau);
                else 
                    grot*=tau;
                cost+=grot;
            }
            //
         //   cerr<<"grad 1/2 "<<j->second<<" "<<dif_prev<<" "<<grad1<<" "<<grad2<<" "<<cost<<" "<<b_prev.first<<" "<<b_prev.second<<" "<<j->first<<" "<<j->second<<endl;
            
            if (j != l_b_index.begin())
            {
                //check to see if used.
                //        smatrix_print(point_indices, "point indices");
                bool found=false;
                for (__CLPK_integer i = 0 ; i < point_indices.MRows; ++i)
                    if (j->second == point_indices(i,0))
                    {
                        found=true;
                        break;
                    }
               if (found)//skip this one
                   continue;
                   //    break;
                
                if ((grad2 > grad1) && (dif_prev<=0))
                {//transition
                    //lets calculte cost for understanding algorithim
                   // cerr<<"found b ng/pos transition "<<j->first<<" "<<j->second<<" "<<" cost : "<<cost<<endl;
                    
                    //        cerr<<"return 1 "<<j->first<<" "<<j->second<<endl;
                    return *j;
                }else if ((grad2 < grad1) && (dif_prev>=0))
                {
                    //  cerr<<"return 2 "<<b_prev.first<<" "<<b_prev.second<<endl;

                    return b_prev;
                }
                
                if (dif_prev != (grad2-grad1 )) //take first of multiple identical, i.e. dont write over
                {
                  // cerr<<"set b_prev - firts ident"<<endl;
                    dif_prev=grad2-grad1;
                    b_prev=*j;
                }//else
                // cerr<<"donm't set bprev "<<grad2-grad1<<endl;

                
            }else{
                dif_prev=grad2-grad1;
                b_prev=*j;
            }
      //      cerr<<"set bprev 1 "<<b_prev.first<<" "<<b_prev.second<<endl;
         //   cerr<<"set bprev 2 "<<j->first<<" "<<j->second<<endl;

            //cerr<<"set bprev 3 "<<b_prev.first<<" "<<b_prev.second<<endl;

            
        }
        //this will be last ident  or last item
         //throw ClassificationTreeNodeException("QuantileRegression : LineSearch1D : never found min cost.");
        return b_prev;
//        cerr<<"done line search"<<endl;
  //      throw ClassificationTreeNodeException("QuantileRegression : LineSearch1D : never found min cost.");
        //float dh = -1 * m_dh(0,0);
        
    //    return pair<float,unsigned int>(0,0);
    }
    
    
    
    
    
    void QuantileRegression::estimateParametersSimplex( const vector<float> & v_indata, const vector<float> & targ_data ){
        //need to flip predcitor and target for binary response variables
              //let's do a unviariate model but the proper way!!!
        
        //lets pick N choose p vertices, calculate cost and choose min (for now ignoring interpolation between)
        //this copiies data venetual ELIMINATE vectors being used.
        smatrix m_in_data(&v_indata[0],NumberOfSamples_,Dimensionality_);
        smatrix_insertColumn(m_in_data,0,1); //augment design to include intercept
        Dimensionality_+=1;
        smatrix m_target(&targ_data[0],NumberOfSamples_,1);
        
        //   smatrix_print(m_in_data, "Est Simplex :  m_in_data");
       // smatrix_print(m_target, "Est Simplex :  m_target_data");
                
        //NOW WE ARE ASSUMING THAT P=2, IE X=[ 4X 4] MATRIX
        
        //search through everypair 
      //  ofstream fparams("linear_model.txt");
      //  int Ntau = 10 ;
    //    float tau_res = 1.0f/Ntau;
        for (unsigned int itau =0 ; itau < _Ntau; ++itau)
        {   
            //initilialize parameters for the simplex search
         //   float costmin;
            smatrix m_Xh(Dimensionality_, Dimensionality_);
            smatrix m_Xhinv(Dimensionality_,Dimensionality_);
            smatrix m_Y(Dimensionality_,1);
            smatrix m_bh(Dimensionality_,1);
            smatrix point_indices(Dimensionality_,1);
            smatrix m_bh_prev(Dimensionality_,1);
             smatrix m_bh_prev2(Dimensionality_,1);

            float tau = _tau[itau];
            //float tau=itau*tau_res;
            cerr<<"tau : "<<tau<<endl;
          //  _tau.push_back(tau);//keep track of quantiles calculated for
    
            //----------search for initial vertex-------------------//
            
            bool first=true;//firstis used to break out of main search loop once first valid vertex is found
            for (unsigned int i = 0 ; i < NumberOfSamples_; ++i)
            {
                for (unsigned int j = i ; j < NumberOfSamples_; ++j)//need to be diefferent i,j
                {
                    //  smatrix_setRow(m_in_data, i, m_Xh, 0);
                    //smatrix_setRow(m_in_data, j, m_Xh, 1);
                    smatrix_setRow( m_Xh, 0, m_in_data, i);
                    smatrix_setRow(m_Xh, 1, m_in_data, j);
                    point_indices(0,0)=i;
                    
                    point_indices(1,0)=j;

                    //only 1 dimensional output
                    m_Y(0,0)=m_target(i,0);
                    m_Y(1,0)=m_target(j,0);

                    int info_inv = smatrix_inverse(m_Xh, m_Xhinv);
                    if (info_inv == 0 ) //not valid iuf no inverse
                    {
                        first=false;
                        break;
                    }
                }
                if (!first) //breaks overall search once first vertex is found
                    break;
            }//doing exhaustive search for debugging
  

            int info_inv = smatrix_inverse(m_Xh, m_Xhinv);
            if (info_inv != 0 )
                throw ClassificationTreeNodeException("QuantileRegression::estimateParametersSimplex : failed to perform matrix inversion.");
            
            smatrix_AxB(m_Xhinv, m_Y,m_bh);
           // smatrix_print(m_Xh, "m_Xh-init");

            //----------END search for initial vertex-------------------//
            
            //-----------Now move to next stage of simplex---------------------//
            //first need to check if optimal solution in gradient direction
            {
                //calculate d(h) for all direction delta_j
                //delta j = colums of X(h)^-1

                float cost=quant_cost(tau, m_in_data,m_target,m_bh);
                //need to exclude chosen h, set of constraints that are equal to zero
                //DECLARE MATRICES
                smatrix dhSum(1,Dimensionality_); //this is summation portion of d(h) queation (p183 in QR book)   
                smatrix m_dh(1,Dimensionality_);
                smatrix m_res(NumberOfSamples_,1);
                smatrix m_pred(NumberOfSamples_,1);
                smatrix xi(1,Dimensionality_);//stores inididual row, will copy in each new row
                float min_dR=0;
                vector< smatrix > all_bhs;
                do {

                    all_bhs.push_back(m_bh);
                    //Initialize Matrices
                    dhSum=0;//set sum to zero
                    m_res=m_target; //set residuals to observed values
                    smatrix_AxB(m_in_data, m_bh, m_pred);//produces ypred (predications)
                    m_res-=m_pred; //subtract predictions from observed (i.e. residuals)

                    ///calculating d(h) portiion of the gradient  
                    //need to multiply Xi by tau/tau-1 depending on sign of resiudals
                    for (unsigned int i = 0 ; i < NumberOfSamples_; ++i)
                    {
                        
                        float res=m_res(i,0);                    
                        if (fabs(res) > _MACH_EPS )//this is here because we want to exclude 0 residuals
                        {
                            //smatrix_setRow(m_in_data, i, xi, 0);
                            smatrix_setRow( xi, 0, m_in_data, i);

                            if (res<0)
                                dhSum+=xi * (tau - 1 );
                            else
                                dhSum+=xi * tau;
                        }
                    }//done summation, now multiplpy by m_Xh^-1

                    smatrix_AxB(dhSum,m_Xhinv,m_dh); //writes over dhSum
                    m_dh*=-1.0;
                    //we now have d(h)
                    
                    //-------Checking to see if reacheed optimal solution, need to check both direction -di an d+dj

                    list< pair<float,unsigned int> > grads_pos,grads_neg;
                    min_dR=0;
                    bool min_dR_pos=true;
                    unsigned int min_dR_j=0;            
                    for ( unsigned int j = 0 ; j < Dimensionality_; ++j)
                    {
                        float dj_pos=m_dh(0,j)+1-tau; //positivr dj 
                        float dj_neg=-1*m_dh(0,j)+tau; //negative dj
                     //   cerr<<"dereivatives "<<m_dh(0,j)+1-tau<<" "<<-1*m_dh(0,j)+tau<<endl;
                        if (dj_pos<min_dR)//dR is negative
                        {
                            min_dR=dj_pos;
                            min_dR_j=j;
                            min_dR_pos=true;
                        }
                        if (dj_neg<min_dR)//dR is negative
                        {
                            min_dR=dj_neg;
                            min_dR_j=j;
                            min_dR_pos=false;
                            
                        }
                        
                        grads_pos.push_back( pair<float,unsigned int>(dj_pos,j));
                        grads_neg.push_back( pair<float,unsigned int>(dj_neg,j));
                    }
                    grads_pos.sort();
                    grads_neg.sort();

                    bool isNegGrad=true;
                    if (min_dR<0)//not reach optimality
                    {
                        smatrix djstar(Dimensionality_,1);//direction of descent
                        smatrix design(1,Dimensionality_);
                        bool do_break=false;
                        pair<float,unsigned int> bmin;
                        
                        list< pair<float,unsigned int> >::iterator i_pos=grads_pos.begin();
                        list< pair<float,unsigned int> >::iterator i_neg=grads_neg.begin();
                        int loop=0;
                        do{//this is to deal with special degenrative case when ossicllations occur
                           //    cerr<<"looping around derivatives until not falling back on itself "<<loop<<" "<<i_pos->first<<" "<<i_neg->first<<endl;
                           // cerr<<"looping around derivatives until not falling back on itself "<<loop<<" "<<i_pos->second<<" "<<i_neg->second<<endl;
                            ++loop;
                            do_break=false;
                            if ( (i_pos == grads_pos.end()) && (i_neg == grads_neg.end()) )
                            {
                            //    cerr<<"no neg gradient left2"<<endl;
                                isNegGrad=false;
                                break;
                            }
                            else if (i_pos == grads_pos.end())
                            {
                                min_dR_j=i_neg->second;
                                min_dR_pos=0;
                                min_dR=i_neg->first;
                                
                                //increment position
                                ++i_neg;

                            }else if (i_neg == grads_neg.end())
                            {
                                min_dR_j=i_pos->second;
                                min_dR_pos=1;
                                min_dR=i_pos->first;
                                //increment position
                                ++i_pos;
                            }else if (i_pos->first < i_neg->first)
                            {
                                min_dR_j=i_pos->second;
                                min_dR_pos=1;
                                min_dR=i_pos->first;
                                //increment position
                                ++i_pos;
                            }else{
                                min_dR_j=i_neg->second;
                                min_dR_pos=0;
                                min_dR=i_neg->first;

                                //increment position
                                ++i_neg;


                            }
                     //       cerr<<"mins "<<min_dR_j<<" "<<min_dR_pos<<" "<<min_dR<<endl;
                            //set direction of max descent (column of X(h)^-1)
                            //smatrix_setCol(m_Xhinv, min_dR_j, djstar, 0);
                            smatrix_setCol(djstar, 0, m_Xhinv, min_dR_j);

                            if (!min_dR_pos)
                                djstar*=-1.0f;
                       
                            //  smatrix_print(m_in_data, "mindata");
                            //set design matrix , this is always a 1D optimization because design is X*dBetas
                            smatrix_AxB(m_in_data, djstar, design);
                            //need to omit identical vertices
                            bmin=lineSearch1D(design, m_res,point_indices, tau); //do line search, this will set a new vertex' residual to zero (bmin.second)                                                  
                                                                                 //  cerr<<"lineSearch "<<bmin.second<<endl;
                            
                            cost=quant_cost(tau, m_in_data,m_target,m_bh);
                         
                            smatrix next_bh=m_bh;
                            next_bh+=djstar*bmin.first;
          
                            float cost_next=quant_cost(tau, m_in_data,m_target,next_bh);
//                             smatrix_print(m_bh  , "m_bh");
//                            smatrix_print(next_bh  , "m_bh_next");
//                            smatrix_print(m_Xh,"m_Xh");
//                            smatrix_print(point_indices,"point_indices");

                          //  cerr<<"cost+next "<<cost<<" "<<cost_next<<endl;
                            if (cost_next >= cost)//make sure its a dscent, think maybe sometime not due to multiple identical values
                            {
                            //    cerr<<"enter cost test "<<i_pos->first<<" "<<i_neg->first<<endl;
                                //doesn't quite make sense with gradient
//                                if ( (i_pos == grads_pos.end()) && (i_neg == grads_neg.end()) )
//                                {
//                                    cerr<<"no neg gradient left3"<<endl;
//                                    isNegGrad=false;
//                                    break;
//                                }else
                                
                                if (i_pos != grads_pos.end())// && ( i_neg->first < 0 ))
                                {
                                    if ( i_pos->first < 0 )
                                        do_break=true;                            
                                }else if (i_neg != grads_neg.end()) // && ( i_pos->first < 0 ) )
                                {
                                     if ( i_pos->first < 0 )
                                         do_break=true;
                                }
                                
                                if (!do_break){
                                   // cerr<<"no neg gradient left"<<endl;
                                    isNegGrad=false;
                                    break;
                                }
                                
                              //  else if ( ( i_pos->first < 0 ) || ( i_neg->first < 0 ))
                                //{
                                  //  do_break=true;
                                //}
                            }
//                            }else{
//                                cerr<<"passed cost test "<<cost<<" "<<cost_next<<endl;
//                                
//                            }
                        
                        }while (do_break); ///loopp back if we're going back on ourselves
                        
                      //  if (quant_cost(tau, m_in_data,m_target,next_bh)<cost)//only do pivot if decreasing cost
                        if(isNegGrad){
                            m_bh+=djstar*bmin.first; //update betas
                            // smatrix_setRow(m_in_data, bmin.second, m_Xh, min_dR_j); ///change row in X corresponding to directional derivative, pivot point
                            smatrix_setRow(m_Xh, min_dR_j, m_in_data, bmin.second); ///change row in X corresponding to directional derivative, pivot point

                                                                                    //  smatrix_print(m_in_data, "mindata2");
                           // smatrix_print(point_indices,"point_indices2");
                                                          //       smatrix_print(point_indices,"point_indices2");

                            point_indices(min_dR_j,0)=bmin.second;//keep track of point indices
                            m_Y(min_dR_j,0)=m_target(bmin.second,0);

                            int info_inv = smatrix_inverse(m_Xh, m_Xhinv);    
                            if (info_inv != 0)
                            {
                                cerr<<"--------not invertable--------"<<endl;
                                smatrix_print(m_Xh,"m_Xh2");
                                cerr<<"--------not invertable---------"<<endl;                                
                            }
                            if (info_inv != 0)
                                throw ClassificationTreeNodeException("QuantileRegression: Simplex Search reach non invertible X");
                        }
                        
                    }
                    if ( !isNegGrad )
                        break;
                }while (min_dR<0);
                
                
            }
               smatrix_print(m_bh  , "m_bh");

            _vbh.push_back(m_bh);
            
        }
        //next need to solve line optimization which becomes a weighted quantile problem.
        //LETS ESTIMATE LEAST SQUARE PARAMETERS for comparison
        
        smatrix Blq(Dimensionality_,1);
        smatrix grot;
        smatrix grot_inv;
        smatrix_AtxA(m_in_data, grot);
        smatrix_inverse(grot, grot_inv);
        smatrix_AxBt(grot_inv, m_in_data, grot);
        smatrix_AxB(grot, m_target, Blq);
        smatrix_print(Blq, "Blq");
        _ols_params=Blq;
        
    }

    void QuantileRegression::estimateParameters(){
        // cerr<<"QuantileRegression::estimateParameters : begin "<<endl;
        //   print(*(in_data[0]),"quant_in_data - est");
        //print(training_target->getTargetData<float>(),"quant_in_data - est");
        //clear member variables
        _tau.clear();
        _Ntau=(_tau_max-_tau_min)/_tau_res + 1;
        _tau.resize(_Ntau);
        unsigned int ind_tau=0;
        for (vector<float>::iterator i_t = _tau.begin(); i_t != _tau.end(); ++i_t,++ind_tau)
            *i_t = _tau_min + ind_tau *_tau_res;
        print(_tau,"_tau");
        _vbh.clear();
		if (out_data == NULL)
			out_data = new vector<float>();
        
        
        if (( Dimensionality_ != 1) && (_InvertModel)) //forced unit demnsionality when inverting model.
            throw ClassificationTreeNodeException("Currently Dimensionality_ of the data must be equal to 1");
        
               vector<float> v_indata;//load data
                vector<float> targ_data;      //targ data ios that which is quantized over
   //     if (_InvertModel)//switch around inpout and target...effects what is being quantized
     //   {
   //         v_indata=training_target->getTargetData<float>();
     //       copyToSingleVectorTranspose( in_data, &targ_data, NumberOfSamples_ );
       // }else {
            targ_data = training_target->getTargetData<float>();
            copyToSingleVectorTranspose( in_data, &v_indata, NumberOfSamples_ );
        //}
        
        if (v_indata.empty())
            throw ClassificationTreeNodeException("Attempted to run quantile regression, but there appears to be no data");
        
     //   cerr<<"estimateParameters - in_data "<<NumberOfSamples_<<" "<<Dimensionality_<<endl;
//        for (unsigned int i = 0 ; i < NumberOfSamples_;++i)
//        {
//            for (unsigned int j = 0 ; j < Dimensionality_;++j)
//               cerr<<v_indata[i*Dimensionality_+j]<<" ";
//                // if (j!=0)
//         //           cerr<<in_data[0]->at(i*Dimensionality_+j)<<" ";
//            cerr<<endl;
//
//        }
        print(v_indata,"quantref in_data");
    //    cerr<<"estimateParameters - targ_data "<<endl;
       // print(targ_data,"targ_data");
//        for (unsigned int i = 0 ; i < NumberOfSamples_;++i)
        //            cerr<<targ_data[i]<<endl;
        //
        if (_InvertModel)//switch around inpout and target...effects what is being quantized
        {
            cerr<<"invert model est"<<endl;
            estimateParametersSimplex(targ_data,v_indata);
            cerr<<"invert model dione simplex2"<<endl;

            estimateTargetQuantiles(v_indata);
            cerr<<"invert model dione quan"<<endl;


        }else{
            estimateParametersSimplex(v_indata,targ_data);
            estimateTargetQuantiles(targ_data);


        }
        //   print(_targ_quantiles,"_targ_quantiles");
        
        //cerr<<"QuantileRegression::estimateParameters : end"<<endl;
    }
    
  
  void quant_neggrad( const float & tau, const vector<float> & v_x, const vector<float> & v_y, const float & B0, const float & B1, const float & stepB0, const float & stepB1, float & dB0, float & dB1 ){
    float c_B0B1=quant_cost(tau,v_x,v_y,B0,B1);
    float c_pB1= quant_cost(tau,v_x,v_y,B0,B1+stepB1);
    // float c_mB1=quant_cost(tau,v_indata,targ_data,b0,b1-stepB1);
    float c_pB0= quant_cost(tau,v_x,v_y,B0+stepB0,B1);
    //float c_mB0=quant_cost(tau,v_indata,targ_data,b0-stepB0,b1);

    //dB0=0;
    // dB1=0;
    //    dB0=(c_pB0 + c_mB0 - 2*c_B0B1);
    //dB1=(c_pB1 - c_mB1);
    dB0= -1 * (c_pB0 - c_B0B1)/stepB0;
    dB1= -1 * (c_pB1 -c_B0B1)/stepB1;

      float mag = sqrtf(dB0*dB0+dB1*dB1);
      dB0/=mag;
      dB1/=mag;
    //if (( c_mB1 < cB0B1 ) ||  ( c_pB1 < cB0B1 ))
      

  }
    
    vector<float> QuantileRegression::getParameters(){
        //cerr<<"Get params "<<_vbh.size()<<endl;
        if (_vbh.empty())
            return vector<float>();
    
        unsigned int msize = _vbh[0].size();
        vector<float> all_params(_tau.size()*msize);
        unsigned int index = 0 ; 
        for ( vector<smatrix>::iterator i_bh = _vbh.begin(); i_bh != _vbh.end(); ++i_bh, ++index)
            memcpy(&(all_params[msize*index]), i_bh->sdata, sizeof(float)*msize);
    
        return all_params;
    }
    vector<float> QuantileRegression::getOLSParameters(){
        //cerr<<"Get params "<<_vbh.size()<<endl;
        if (_vbh.empty())
            return vector<float>();
        
        unsigned int msize = _ols_params.size();
        vector<float> all_params(msize);

            memcpy(&(all_params[0]), _ols_params.sdata, sizeof(float)*msize);
        
        return all_params;
    }

    void QuantileRegression::setOptions( std::stringstream & ss_options)
    {
        map<string,string> options = parse_sstream(ss_options);
		for ( map<string,string>::iterator it = options.begin(); it!=options.end();it++)
		{
			cout<<"set svm option "<<it->first<<" "<<it->second<<endl;
			
			if ( it->first == "InvertModel")
			{
                  if ((it->second=="false") || (it->second=="0"))
                  {
                      _InvertModel=false;
                  }else if ((it->second=="true") || (it->second=="1")){
                      _InvertModel=true;
                  }
                      //nooptions yet
			}else if ( it->first == "OutputFormat")
			{
                if (it->second=="ClosestQuantile")
                _out_format=CLOSEST_QUANTILE;
             
			}else if ( it->first == "useOLS")
			{
                if ((it->second=="false") || (it->second=="0"))
                {
                    _useOLS=false;
                }else if ((it->second=="true") || (it->second=="1")){
                    _useOLS=true;
                }
                //nooptions yet
			}else if( it->first == "threshold")
			{
                stringstream ss; 
                ss<<it->second;
                ss>>_threshold;
                _binarize_output=true;
                //nooptions yet
			}else if (it->first == "TauMin")
            {
                stringstream ss;
                ss<<it->second;
                ss>>_tau_min;                
            }else if (it->first == "TauMax")
            {
                stringstream ss;
                ss<<it->second;
                ss>>_tau_max;
            }else if (it->first == "TauRes")
            {
                stringstream ss;
                ss<<it->second;
                ss>>_tau_res;
            }else {
                
				//if not in node specific implementation, check to see if option exists in Tree node.
				//by including at the end, can replace option handling of parent (ClassificationTreeNode);
				if (!setTreeNodeOptions(it->first, it->second))
					throw ClassificationTreeNodeException("Quantile Regression: Invalid option into QuantileRegression node.");
			}
            
		}

    }
    float QuantileRegression::quantile( const std::vector<float> & data, const float & tau )
    {
        
        cerr<<"quantie tau2 "<<tau<<endl;
        
        list<float> all_data(data.begin(),data.end());
        all_data.sort();
        float quant_opt=0;
        //float cost_min=0;
        unsigned int index=0;
        float q_prev=0;
        float cost=0;

        for ( list<float>::iterator i_d = all_data.begin(); i_d != all_data.end(); ++i_d , ++index)
        {
            q_prev=cost;
            cost=0;
            for ( list<float>::iterator i_d2 = all_data.begin(); i_d2 != all_data.end(); ++i_d2 )
            {
                if (*i_d2 > *i_d)
                    cost+=( *i_d2 - *i_d) * tau;
                else
                    cost+=( *i_d2 - *i_d) * (tau-1);
            }
            // cerr<<"quant cost "<<*i_d<<" "<<cost<<endl;
            //   if ((index>0)&&(cost>q_prev)){
            //  break;
            //}
            if ((index>0))
            if (cost > q_prev)
            {
                //quant_opt = q_prev;
                --i_d;
                quant_opt = *i_d;
                break;
                // cerr<<"Quantile Opt "<<quant_opt<<" "<<q_prev<<endl;
            }
//            if (index==0)
//            {
//                cost_min=cost;
//                quant_opt=*i_d;
//            }else if  ( cost < cost_min ){
//                cost_min = cost;
//                
//                quant_opt=*i_d;
//            }
            
        }
        
        return quant_opt;
        
    }
//    float QuantileRegression::quantile( const std::vector<float> & data, const float & tau )
//    {
//        if (data.empty())
//            return 0;
//        float min=data[0];
//        float max=data[0];
//        float search_res=0.01;
//        for (vector<float>::const_iterator i_d = data.begin()+1; i_d != data.end(); ++i_d)
//        {
//            if (*i_d < min) min=*i_d;
//            if (*i_d > max) max=*i_d;
//        }
//        
//        unsigned int count=0;
//        float costmin,quant_min;
//        
//        for  ( float i = min ; i < max ; i+=search_res,++count)
//        {
//            float cost=0;
//            for (vector<float>::const_iterator i_d = data.begin()+1; i_d != data.end(); ++i_d)
//            {
//                float dif = *i_d  - i;
//                if (dif < 0 ) {
//                    cost += (tau-1) * (dif);
//                    //  cerr<<"aug1 "<<cost<<" "<<tau<<" "<<dif<<" "<<(tau-1) * (dif)<<endl;
//                }else{
//                    cost += tau * (dif);
//                    //  cerr<<"aug2 "<<cost<<" "<<tau<<" "<<dif<<" "<< tau * (dif)<<endl;
//                    
//                }
//            }
//            if (count==0)
//            {
//                costmin=cost;
//                quant_min=i;
//            }else if (cost < costmin)
//            {
//                costmin=cost;
//                quant_min=i;
//            }
//        }
//        return quant_min;
//        
//    }

    void QuantileRegression::runNode( bool with_estimation )
	{
		cerr<<"run Node name "<<NodeName<<endl;
        
        
		for (vector< ClassificationTreeNode* >::iterator i = node_inputs.begin() ; i != node_inputs.end(); i++)
		{
			
			if (PassDownOutput)
			{
				(*i)->enablePassDownOutput();//tells input to pass down output information
			}
			
			(*i)->runNode(true);
			
		}
        //cout<<"done running inputs : base filter "<<endl;
        
        //loop above setups the passdown
      
		setTarget();
 
		setInputData();

		runSpecificFilter(with_estimation);

        if ( PassDownOutput )
			setIOoutput();
        
        
        
        if ( PassDownOutput )
            setIOoutputSpecific();
        
        if (trackStats)
			runTrackStats();
     //   cerr<<"set test data"<<endl;
		setTestData();
		
   //     cerr<<"done set Test Data"<<endl;
        //cout<<"filetr delete prior data"<<endl;
        
        if ( _out_format == CLOSEST_QUANTILE ){//lets find the appropriate tau
            Dimensionality_=1;
        }else{
            
            
            //REIMPLEMENTED FOR THIS CHANGE
            Dimensionality_=_tau.size();
            
            
            //this poriton removes dimension for which there were no slope, i.e. un-invertible
            if (_InvertModel)
            {
                //trim un invertiables
                vector<int> dim_mask(Dimensionality_,0);
                unsigned int tau_index=0;
                unsigned int dimTrim=0;
                for (std::vector<clapack_utils_name::smatrix>::iterator i_bh = _vbh.begin(); i_bh != _vbh.end();++i_bh,++tau_index)
                    if (_vbh[tau_index](1,0)==0)
                    {
                        dim_mask[tau_index]=1;
                        ++dimTrim;
                    }
                
                vector<float>::iterator i_out = out_data->begin();
                vector<float>::iterator i_out_write = out_data->begin();
                for ( unsigned int i = 0 ; i < NumberOfSamples_; ++i)
                    for ( vector<int>::iterator i_m = dim_mask.begin(); i_m != dim_mask.end(); ++i_m,++i_out)
                    {
                        if (*i_m == 0)
                        {
                            *i_out_write=*i_out;
                            ++i_out_write;
                        }
                    }
                
                out_data->resize((Dimensionality_-dimTrim)*NumberOfSamples_);
                
                
                i_out = out_test_data->begin();
                i_out_write = out_test_data->begin();
                for ( unsigned int i = 0 ; i < NumberOfTestSamples_; ++i)
                    for ( vector<int>::iterator i_m = dim_mask.begin(); i_m != dim_mask.end(); ++i_m,++i_out)
                    {
                        if (*i_m == 0)
                        {
                            *i_out_write=*i_out;
                            ++i_out_write;
                        }
                    }
                
                out_test_data->resize((Dimensionality_-dimTrim)*NumberOfTestSamples_);
                Dimensionality_-=dimTrim;
                
                
            }
        }
        deleteInputsOutputData();
		
	}
	
    
    
    
    
    
    void QuantileRegression::applyToTestData()
    {
        //output is predcited repsonse for each input
        // cerr<<"QuantileRegression::applyToTestData : "<<NumberOfTestSamples_<<endl;
        if (disc_distance!=NULL)
            cerr<<"disc_distance "<<disc_distance->size()<<endl;
     //   vector<float> v_test_data;
        
        
        smatrix m_in_test(NumberOfTestSamples_,Dimensionality_-1);
        copyToSingleVector( in_test_data, m_in_test.sdata, NumberOfTestSamples_ );//copy directly into matrix
        smatrix_insertColumn(m_in_test,0,1); //augment design to include intercept
        

        if (out_test_data == NULL)
        {
			//out_test_data = new vector<float>(_vb0_opt.size()*v_test_data.size());
            
            if (( _out_format == ALL_QUANTILES)  ) ///output predictions for all quantiles
                out_test_data = new vector<float>(NumberOfTestSamples_*_tau.size());
                else
            out_test_data = new vector<float>(NumberOfTestSamples_);
        }
        else
        {
            out_test_data->clear();
            if (( _out_format == ALL_QUANTILES) )
                            out_test_data->resize(NumberOfTestSamples_*_tau.size());
            else
                out_test_data->resize(NumberOfTestSamples_);
            // out_test_data->resize(_vb0_opt.size()*v_test_data.size());
        }
        
        if (disc_distance==NULL)
            disc_distance = new vector<float>(NumberOfTestSamples_);
        else {
            disc_distance->clear();
            disc_distance->resize(NumberOfTestSamples_);
            
        }
        //if (disc_distance!=NULL)
        //            cerr<<"disc_distance2 "<<disc_distance->size()<<endl;
        //STILL ASSUMING 1 DIMESNION
        
        //---------------------this bit of code searches for median
        //        unsigned int med_index=0;
        //        bool found=false;
        //        for (vector<float>::iterator i_tau = _tau.begin(); i_tau != _tau.end();++i_tau,++med_index)
        //        {
        //            cerr<<"i_tau "<<*i_tau<<endl;
        //            if (*i_tau==0.5)
        //            {
        //                found=true;
        //                break;
        //            }
        //        }
        //        cerr<<"med_index "<<med_index<<endl;
        //
        //------------------------
        
        //    if (!found)
        //        throw ClassificationTreeNodeException("QuantileRegression : ApplyToTestData : Cannot find 0.5 quantile");
        
        
        if ( _out_format == CLOSEST_QUANTILE ){//lets find the appropriate tau
            cerr<<"dim "<<Dimensionality_<<endl;
            if ( Dimensionality_ !=2 )
                throw ClassificationTreeNodeException(" Dimensionality_ must be equal to 2 for Closest Quantile. " );
            //  for ( vector<float>::iterator i_d
            
            //invert modek uses unviariate assumption
            //  cerr<<"invert model fo rTau "<<*i_tau<<endl;
            float pred;
            vector<float> out_grot(NumberOfSamples_);
            vector<float>::iterator i_out= out_grot.begin();
            
            for (unsigned int i = 0 ; i < NumberOfSamples_; ++i,++i_out)//,++i_dist)
            {
                float val =  m_in_test(i,1);
                unsigned int min_ind = 0;
                float min_dif = 0;
                unsigned int ind=0;
                for ( vector<float>::iterator i_q = _targ_quantiles.begin(); i_q != _targ_quantiles.end(); ++i_q ,++ind)
                {
                    // cerr<<"tquants "<<*i_q<<" / "<<val<<endl;
                    
                    float dif = fabs(*i_q - val );
                    if (ind==0)
                    {
                        min_dif=dif;
                        min_ind=ind;
                    }else if ( dif < min_dif )
                    {
                        min_dif=dif;
                        min_ind=ind;
                    }
                    
                }
                //cerr<<"MINMINMIN "<<min_ind<<" "<<val<<endl;
                
                
                if (_InvertModel){
                    
                    if (_vbh[min_ind](1,0)==0)
                    {
                        *i_out = 0;
                        //   _dim_trims.push_back(i);
                        
                    }else{
                        
                        pred = 1.0f/_vbh[min_ind](1,0) * m_in_test(i,1) - _vbh[min_ind](0,0)/_vbh[min_ind](1,0);
                        // cerr<<"x="<<- _vbh[tau_index](0,0)/_vbh[tau_index](1,0)<<" + "<<1.0f/_vbh[tau_index](1,0)<<"y = "<<- _vbh[tau_index](0,0)/_vbh[tau_index](1,0)<<" + "<<1.0f/_vbh[tau_index](1,0) <<" * "<<m_indata(i,1)<<" = "<<pred<<endl;
                        
                        if (_binarize_output) {
                            *i_out =  ((pred<_threshold)? 0 :  1 );
                        }else{
                            *i_out = pred;
                        }
                        
                    }
                    
                }
            }
            cblas_scopy(NumberOfTestSamples_, &(out_grot[0]), 1, &(out_test_data->at(0)), 1);

        }else{
            
            
            
            
            //LETS loop over all quantiles
            //   ofstream fout("pred_vals.ssv");
            unsigned int tau_index=0;//overrides previous
            for (vector<float>::iterator i_tau = _tau.begin(); i_tau != _tau.end();++i_tau,++tau_index)
            {
                
                //   smatrix_print(_vbh[med_index], "Contional median Betas");
                
                if (_InvertModel){    //invert modek uses unviariate assumption
                                      //   cerr<<"Inverted Model"<<endl;
                    
                    float pred;
                    vector<float> out_grot(NumberOfTestSamples_);
                    vector<float>::iterator i_out= out_grot.begin();
                    //  vector<float>::iterator i_out= out_test_data->begin()+tau_index;
                    // vector<float>::iterator i_dist= disc_distance->begin();
                    for (unsigned int i = 0 ; i < NumberOfTestSamples_; ++i,++i_out)
                    {
                        pred = 1.0f/_vbh[tau_index](1,0) * m_in_test(i,1) - _vbh[tau_index](0,0)/_vbh[tau_index](1,0);
                        
                        if (_vbh[tau_index](1,0)==0)
                        {
                            //cerr<<"ZEROZERO"<<endl;
                            *i_out = 0;
                            
                        }else{
                            if (_binarize_output) {
                                *i_out =  ((pred<_threshold)? 0 :  1 );
                                //   *i_dist = pred-_threshold;
                            }else{
                                // cerr<<"invert model sum residual error "<<pred<<" "<< training_target->getInTarget(i)<<endl;
                                *i_out = pred;
                                // *i_dist = (pred -  training_target->getInTarget(i));
                            }
                        }
                    }
                    cblas_scopy(NumberOfTestSamples_, &(out_grot[0]), 1, &(out_test_data->at(tau_index)), _tau.size());
                    
                }else{//normal non-inverted model
                      //want to copy to out_test_data, so data is not deleted
                      //        cerr<<"Standard MNodel"<<endl;
                    smatrix m_test_out( NumberOfTestSamples_, 1 );
                    //          cerr<<"sizes "<<m_in_test.MRows<<" "<<m_in_test.NCols<<" "<<_vbh[tau_index].MRows<<" "<<_vbh[tau_index].NCols<<" "<<_vbh.size()<<" "<<tau_index<<endl;
                    smatrix_AxB(m_in_test , _vbh[tau_index] , m_test_out);
                    //                smatrix_print(m_in_test, "m_test_in ");
                    //                smatrix_print( _vbh[tau_index], " _vbh[tau_index] ");
                    //                smatrix_print(m_test_out, "m_test_out ");
                    //                cerr<<"offset "<<
                    
                    cblas_scopy(NumberOfTestSamples_, m_test_out.sdata, 1, &(out_test_data->at(tau_index)), _tau.size());
                    
                    //  memcpy(&(out_test_data->at( NumberOfTestSamples_*tau_index)) , m_test_out.sdata, m_test_out.size()*sizeof(float));
                }
                
            }
        }
        
   //    print(*out_test_data,"QuantReg- out_test_data");
//        for ( vector<float>::iterator i_out = out_test_data->begin() ; i_out != out_test_data->end();++i_out  )
//        {
//            fout<<*i_out<<" ";
//        }
//        fout<<endl;
//
//        fout.close();
       // float res2 = 0.0f;
//        cerr<<"MNumber of training targets"training_target->getNumberOfSamples_()
      //  for ( vector<float>::iterator i_pred )
        
        
        //print output data for testing
    //    cerr<<"done out data size : "<<out_test_data->size()<<" "<<NumberOfTestSamples_<<" "<<endl;

        
    }

    void QuantileRegression::applyParametersToTrainingData()
    {
        //vector<float> v_indata;
       // copyToSingleVectorTranspose( in_data, &v_indata, NumberOfSamples_ );
        smatrix m_indata(NumberOfSamples_,Dimensionality_-1);   //account for appended intercept     
        copyToSingleVectorTranspose( in_data, m_indata.sdata, NumberOfSamples_ );
        smatrix_insertColumn(m_indata,0,1); //augment design to include intercept

        if (out_data == NULL)
        {
            if ( ( _out_format == ALL_QUANTILES)  )
                out_data = new vector<float>(NumberOfSamples_*_tau.size());
            else
                out_data = new vector<float>(NumberOfSamples_);

        }else
        {
            out_data->clear();//dont need though will mean vector starts empty
            if ( ( _out_format == ALL_QUANTILES) )
                out_data->resize(NumberOfSamples_*_tau.size());
            else
                out_data->resize(NumberOfSamples_);

        }
          //----------------------lets try just median quantiles
        
            //---------------------this bit of code searches for median
//            unsigned int med_index=0;
//            bool found=false;
//            for (vector<float>::iterator i_tau = _tau.begin(); i_tau != _tau.end();++i_tau,++med_index)
//            {
//                cerr<<"i_tau "<<*i_tau<<endl;
//                if (*i_tau==0.5)
//                {
//                    found=true;
//                    break;
//                }
//            }
//            cerr<<"med_index "<<med_index<<endl;
        
        if ( _out_format == CLOSEST_QUANTILE ){//lets find the appropriate tau
         //   cerr<<"dim "<<Dimensionality_<<endl;
            if ( Dimensionality_ !=2 )
                throw ClassificationTreeNodeException(" Dimensionality must be equal to 2 for Closest Quantile. " );
            //  for ( vector<float>::iterator i_d
            
              //invert modek uses unviariate assumption
                                  //  cerr<<"invert model fo rTau "<<*i_tau<<endl;
                float pred;
                vector<float> out_grot(NumberOfSamples_);
                vector<float>::iterator i_out= out_grot.begin();
            
            for (unsigned int i = 0 ; i < NumberOfSamples_; ++i,++i_out)//,++i_dist)
            {
                float val =  m_indata(i,1);
                unsigned int min_ind = 0;
                float min_dif = 0;
                unsigned int ind=0;
                for ( vector<float>::iterator i_q = _targ_quantiles.begin(); i_q != _targ_quantiles.end(); ++i_q ,++ind)
                {
                    // cerr<<"tquants "<<*i_q<<" / "<<val<<endl;
                    
                    float dif = fabs(*i_q - val );
                    if (ind==0)
                    {
                        min_dif=dif;
                        min_ind=ind;
                    }else if ( dif < min_dif )
                    {
                        min_dif=dif;
                        min_ind=ind;
                    }
                    
                }
             //   cerr<<"MINMINMIN "<<min_ind<<" "<<val<<endl;

                
                if (_InvertModel){
                    
                    if (_vbh[min_ind](1,0)==0)
                    {
                        *i_out = 0;
                        //   _dim_trims.push_back(i);
                        
                    }else{
                        
                        pred = 1.0f/_vbh[min_ind](1,0) * m_indata(i,1) - _vbh[min_ind](0,0)/_vbh[min_ind](1,0);
                      //  cerr<<"x="<<- _vbh[min_ind](0,0)/_vbh[min_ind](1,0)<<" + "<<1.0f/_vbh[min_ind](1,0)<<"y = "<<- _vbh[min_ind](0,0)/_vbh[min_ind](1,0)<<" + "<<1.0f/_vbh[min_ind](1,0) <<" * "<<m_indata(i,1)<<" = "<<pred<<endl;
                        
                        if (_binarize_output) {
                            *i_out =  ((pred<_threshold)? 0 :  1 );
                        }else{
                            *i_out = pred;
                        }
                        
                    }
                    
                }
            }
          //  print(out_grot, "out_grot");
            cblas_scopy(NumberOfSamples_, &(out_grot[0]), 1, &(out_data->at(0)), 1);
            
        }else{
            
            
            
            
            
            
            //------------------------
            unsigned int tau_index=0;//overrides previous
            for (vector<float>::iterator i_tau = _tau.begin(); i_tau != _tau.end();++i_tau,++tau_index)
            {
                
                
                if (_InvertModel){    //invert modek uses unviariate assumption
                                      //  cerr<<"invert model fo rTau "<<*i_tau<<endl;
                    float pred;
                    vector<float> out_grot(NumberOfSamples_);
                    vector<float>::iterator i_out= out_grot.begin();
                    
                    //vector<float>::iterator i_out= out_data->begin();
                    for (unsigned int i = 0 ; i < NumberOfSamples_; ++i,++i_out)//,++i_dist)
                    {
                        //  cerr<<"y="<<_vbh[tau_index](0,0)<<"+"<<_vbh[tau_index](1,0)<<"x"<<endl;
                        
                        
                        if (_vbh[tau_index](1,0)==0)
                        {
                            // cerr<<"ZEROZERO"<<endl;
                            *i_out = 0;
                            //   _dim_trims.push_back(i);
                            
                        }else{
                            
                            pred = 1.0f/_vbh[tau_index](1,0) * m_indata(i,1) - _vbh[tau_index](0,0)/_vbh[tau_index](1,0);
                            // cerr<<"x="<<- _vbh[tau_index](0,0)/_vbh[tau_index](1,0)<<" + "<<1.0f/_vbh[tau_index](1,0)<<"y = "<<- _vbh[tau_index](0,0)/_vbh[tau_index](1,0)<<" + "<<1.0f/_vbh[tau_index](1,0) <<" * "<<m_indata(i,1)<<" = "<<pred<<endl;
                            
                            if (_binarize_output) {
                                *i_out =  ((pred<_threshold)? 0 :  1 );
                            }else{
                                *i_out = pred;
                            }
                            
                        }
                    }
                    cblas_scopy(NumberOfSamples_, &(out_grot[0]), 1, &(out_data->at(tau_index)), _tau.size());
                    
                }else{//normal non-inverted model
                      //want to copy to out_test_data, so data is not deleted
                    smatrix m_out( NumberOfSamples_, 1 );
                    smatrix_AxB(m_indata , _vbh[tau_index] , m_out);
                    //      smatrix_print(m_indata, "m_test_in ");
                    //smatrix_print(m_out, "m_out ");
                    
                    // smatrix_print( _vbh[tau_index], " _vbh[tau_index] ");
                    //    smatrix_print(m_indata, "m_test_out ");
                    // cerr<<"memcpy "<<NumberOfSamples_<<" "<<tau_index<<" "<<NumberOfSamples_*tau_index<<" "<<m_out.size()<<endl;
                    
                    cblas_scopy(NumberOfSamples_, m_out.sdata, 1, &(out_data->at(tau_index)), _tau.size());
                    // memcpy(&(out_data->at(NumberOfSamples_*tau_index)), m_out.sdata, sizeof(float)*m_out.size());
                }
                
            }
            
        }
       // print(*out_data,"QuantReg- out_data");
            

        
       // cerr<<"Quantile regression aplplied ot training data"<<endl;
//        
//        int count=0;
//        for (vector<float>::iterator i_out = out_data->begin(); i_out != out_data->end(); ++i_out,++count)
//        {
//            if (count==9)
//            {
//                cerr<<endl;
//                count=0;
//            }
//            cerr<<*i_out<<" ";
//            
//        }
       // print(*out_data,"QuantReg- outdata");
       // cerr<<"tarining done out data"<<endl;
        
    }
    
    void QuantileRegression::runTrackStats(){
        
    }

}
