#!/bin/sh 
function Usage(){
    echo "\n\n rerun_discriminate.sh [options] <disc_dir> <n_iterations>  <run_file> \n\n"
    echo "\n\n"
    echo "-N <number of subsamples> : this overrides that found in the file"
    echo "-save_parameters : save aparameters for discriminant function"
    echo "\n"
    exit 1
}

if [ $# -lt 3 ] ; then
    Usage
fi
NSUBSAMPLES=""
SETN=0
OPTS=""
DOPTS=""
echo Number of Inputs: $#
while [ $# -gt 3 ]; do
    echo "Parse : $1"
    if [ $1 = "-N" ]; then
        NSUBSAMPLES=$2
        SETN=1
        shift 2
    elif [ $1 = "-save_parameters" ]; then
        DOPTS="${DOPTS} -save_parameters"
        shift 1
    else
        echo "Invalid option : $1 "
        Usage
        exit 1
    fi 
done

INDIR=$1
DISCOUT_DIR=$INDIR
NITER=$2
RUN_FILE=$3
echo $DISCOUT_DIR
echo $NITER
echo $RUN_FILE

if [  _$NSUBSAMPLES = _ ] ; then 
    NSUBSAMPLES=`grep NSUBSAMPLES ${INDIR}/config.txt | awk '{ print $2 }'`
fi
MINOFFSET=`grep MINOFFSET ${INDIR}/config.txt | awk '{ print $2 }'`
KFOLD=`grep KFOLD ${INDIR}/config.txt | awk '{ print $2 }'`


#NEED TO CHANGE THESE
#MASKARGS=`grep MASKARGS  $INDIR | awk -F , '{ print $2 }'`
SELMASK_ARGS=`sed -n '5p'  ${INDIR}/config.txt`


TREE_FILE=${INDIR}/tree.txt


#BAGGING_ARGS=--bag --bag_mid_thresh=$BAG_MID_TH 
BAGGING_ARGS="--bag --bag_mid_thresh=0.5" 

#FNAME=`basename $DISCOUTDIR .disc`
#DISCOUT_DIR=`dirname $DISCOUT_DIR`
RUN=0; 

DISCOUT=${DISCOUT_DIR}/Leave-0-Out_${RUN}
EXEDIR=`dirname $0`
RUN=0
#append options to name for clarity 
OTPS=""
if [ $SETN = 1 ] ; then 
    OPTS="_N${NSUBSAMPLES}"
fi

while [ $RUN -lt $NITER ]; do 
    DISCOUT=${DISCOUT_DIR}/Leave-0-Out_${RUN}${OPTS}
    echo DISCOUT $DISCOUT
    if [ ! -d ${DISCOUT} ] ; then 
	mkdir ${DISCOUT}
	echo  ${EXEDIR}/discriminate -i $TREE_FILE -o ${DISCOUT}/cv_results_disc --minimumOffset=${MINOFFSET} --Nsubsamples=${NSUBSAMPLES} -K ${KFOLD} ${VERBOSE} $BAGGING_ARGS $MODEL_THRESH_OPTS $SELMASK_ARGS $WEIGHT_BY_PROB $LOAD_ARGS ${DOPTS}>> $RUN_FILE
    fi
    let RUN+=1
done

fsl_sub -q short.q -l logs -N rerun_disc -t $RUN_FILE
