\hrule
\hypertarget{AdaBoostSpec}{}
%\hypertarget{AdaBoostSpec}{}
\vspace{0.5cm}
\begin{section}{AdaBoost}

NodeType: {\bf Ada\_Boost}\\

Adaptive Boosting (AdaBoost) is an algorithm that creates a strong classifier by combining many weak classifiers. A weak classifier can 
be identified as any classifier that classifies near chance. AdaBoost takes each classification, increases the dependencies upon the 
incorrect prediction, and uses these changes to reclassify the data. At the end, each set of classifications is weighted according to 
error rate and combined to create final predictions for the dataset.  \\

This is a wrapper node, which means it is used with another node as input (specifically, a discriminant). The input node must have certain 
virtual functions re-implemented form its parent class.  The input node defines the set of parameters for which AdaBoost is run on.

\begin{subsection}{AdaBoost Options}
    	\begin{description}

	\item[nodeType] \hfill \\ \\
		Specifies the node (weak classifier) to wrap into AdaBoost.\\
		Available nodes are:
			
		\begin{enumerate}
		\item {\hyperlink{LDA_Spec}{LDA}}
		\item{\hyperlink{QDA_Spec}{QDA}}
		\item{\hyperlink{SVM_Spec}{SVM}}
		\item{\hyperlink{LR_Spec}{Logistic Regression}}
		\end{enumerate}
			
	\item[nodeOptions] \hfill \\ \\
		Comma separated inputs to pass down to the input filter.
			
	\item[mRuns] \hfill \\ \\
		Used to specify the number of iterations of AdaBoost to run. Default is 100.
			
	\item[univariate] \hfill \\ \\
		If this is set to true, AdaBoost will run a univariate boosting algorithm, acting as a feature selector as well. 
		If false, the entire dataset is used in boosting and creating predictions of test data.
			 
   	\end{description}
\end{subsection}

\begin{subsection}{Overall Algorithm} 
A version of Hastie, Tibshirani and Friedman's (2009), Freunde and Schapire's (1996), and Viola and Jones' (2003) AdaBoost algorithm 
was adapted. For a 2 class data set, let $y$ and $x$ represent the true and predicted training classifications as \{-1, 1\}, and M be the 
total number of iterations of the boosting algorithm, so each iterations' classifications are $G_m(x)$. $N$ is the number of samples being 
used to train on. The full algorithm for the univariate method is explained below.

	\begin{center}
	\bf AdaBoost Algorithm
	\end{center}
	
%\hline

	\begin{enumerate}
	\item{Initialize the weights $w_i=1/N,\ i=1,2,...,N$.}		
	\item{For $m=1$ to $M$:}
		
		\begin{enumerate}
		\item{Fit a classifier to each feature of the training data using weights $w_i$.}
		\item{Compute} $err_m =$ {\Large $\frac{\sum_{i=1}^N w_i I (y_i \neq G_m(x_i))}{\sum_{i=1}^N w_i}$} for each feature.
		\item{Select the feature with the lowest error to represent this iteration ($G_m(x)$).}
		\item{Compute} $\alpha_m = log${\Large$(\frac{1-err_m}{err_m})$}.
		\item{Set $w_i = w_i * exp(\alpha_m I(y_i \neq G_m(x_i)))$.}
		\item{Store the feature index and $\alpha_m$ for future use.}
		\end{enumerate}
					
	\item{Output $G(x) = sign[\sum_{m=1}^M \ \alpha_m G_m(x)]$.}
	\end{enumerate}
%	\hline
	\begin{description}
	\item[Note:] $I$ is the indicator function, and $sign$ is the signum function.
	\end{description} 
	
\vspace{1in}

To classify future (test) data, skip steps 1 and 2 from the algorithm. Perform step 3 by selecting only the desired feature for each iteration, 
classifying it, and summing the product of the classifications and the associated alphas. \\  

For the non-univariate option, the algorithm becomes simpler. Each iteration only involves fitting a classifier to the entire data set, so a single 
feature is not selected. Test data is similarly classified without selection of a specific feature. 
\end{subsection}

\begin{subsection}{Major Functions}
	\begin{description}
	
	\item[setMIterations(M)] \hfill \\ \\ 
		Lets the user change the number of weighted iterations AdaBoost will complete. \\ 
	
	\item[runNode()] \hfill \\ \\ 
		Connects AdaBoost to the rest of the tree node by running through previous nodes before estimating parameters, classifying 
		training data, and predicting on test data.
	
	\end{description}
\end{subsection}

\begin{subsection}{Data Structures}
Internally, the alphas and feature indices are stored in vectors of floats. In addition, the class has a vector of pointers to base\_mvdisc 
(\hyperlink{baseDisc_Spec}{basic multivariate discriminants}) in order to save the specific parameters for each iteration that are estimated 
during the training process. Lastly, there are also a bool and an int to keep track of the univariate option and iteration number, respectively.
\end{subsection}

\begin{subsection}{Limitations}
Currently, test data is correctly classified in LDA, but there is an error for other node types. This error is believed to be a cause of parameters 
that are estimated using weighted (and thus reduced) data, while test data is not weighted in any way. The result - much larger predicted 
probabilities, creating misclassifications. 
\end{subsection}


\end{section}
\vspace{0.5cm}
\hrule