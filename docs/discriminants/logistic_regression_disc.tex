\hrule
\vspace{0.5cm}
\hypertarget{LR_Spec}{}
\begin{section}{Logistic Regression}

NodeType: Logistic\_Regression \\

%\label{NormFilterSpec}
Performs standard logistic regression.

\begin{subsection}{Logistic Regression Options}
	\begin{description} 	
   	\item [FracVariance] \hfill \\
        		The fractional amount of the total variance to be added in to stabilize the inverse calculations 
		(especially in the reduced models). Default is set to 0.1 
	\item[BackwardStepwiseLikelihoodRatio] \hfill \\
		The option to run backwards stepwise elimination using the likelihood ratio as a score criterion. 
		This option simultaneously sets a chi squared value to compare the model to.
	\item[BackwardStepwiseWithinAcc] \hfill \\
                	The option to run backwards stepwise elimination using the training accuracy as a score criterion.
	\item[BackwardStepwiseGenAcc] \hfill \\
               	The option to run backwards stepwise elimination using the generalization accuracy as a score 
		criterion. Also, it saves the value representing the number of groups to split the data into for cross 
		validation.
	\item[ForwardStepwiseLikelihoodRatio] \hfill \\
                	The option to run backwards stepwise elimination using the score statistic as a score criterion. 
		This option simultaneously sets a chi squared value to compare the model to.
	\item[ForwardStepwiseWithinAcc] \hfill \\
                	The option to run backwards stepwise elimination using the training accuracy as a score criterion.
         \item[ForwardStepwiseGenAcc] \hfill \\
		The option to run backwards stepwise elimination using the generalization accuracy as a score 
		criterion. Also, it saves the value representing the number of groups to split the data into for cross 
		validation.
	\end{description}
\end{subsection}

\begin{subsection}{Algorithms}
	\begin{description}
	\item[General Algorithm and Math] \hfill \\ 
		Logistic Regression aims to model two classes $(K = 2)$ with linear probability functions. The model has form: 
		\begin{align}
		\begin{split}
		log {\Large \frac{Pr(G=1|X=x)}{Pr(G=2|X=x)}} &= \beta_0 + \beta^T X^T \\ 
		& \ \ \vdots
		\end{split} \\ 
		Pr(G=1|X=x) & = {\Large \frac{exp(\beta_0 + \beta^T X^T)}{1 + exp(\beta_0 + \beta^T X^T)}} \\
		Pr(G=2|X=x) & = {\Large \frac{1}{1 + exp(\beta_0 + \beta^T X^T)}}
		\end{align}
		
		where $G$ represents the classification for a sample and $X$ is a data matrix with dimensions [number of 
		samples (N) x features (D)]. It is easy to see that equations 11.3 and 11.4 sum up to one, as expected. Furthermore, 
		these equations can be further simplified by adding a column of ones to the data matrix to account for the 
		intercept. Then, $\beta_0 + \beta^T x^T$ dissolves down to $\beta_f^T X_f^T$. From now on, $\beta_f$ will 
		be referred to as simply $\beta$, and $X_f \equiv X$. \\ 
		
		The goal of logistic regression is to find a $\beta$ value that maximizes the log-likelihood of the predictions. 
		To calculate the log-likelihood, let $g_i$ be the specific class of each sample, and let $y_i$ represent the 
		response, where $y = 1$ when $g = 1$ and $y = 0$ when $g = 2$. Also, let $p_k(x_i, \beta) = 
		Pr(G=k|X=x_i, \beta)$. Since $p_2 = 1 - p_1$, all equations will be in terms of $p_1$. Then, the log-likelihood 
		can be written as: 
		\begin{equation}
		\begin{split}
		l(\beta) &= \sum_{i=1}^N \{y_i \ log(p) + (1-y_i) log(1-p)\} \\
		&= \sum_{i=1}^N \{y_i \beta^T x_i - log(1 + e^{\beta^T x_i^T})\}
		\end{split}
		\end{equation} \\ 
		
		In order to calculate the optimal $\beta$ vector for the log-likelihood, the Newton-Raphson algorithm is used. 
		Here it is, in simplified matrix form:
		
			\begin{center}
			\bf Newton Algorithm
			\end{center}
			
	%	\hline

			\begin{enumerate}
			\item{Initialize $\beta = \vec{0}$ and probabilities, ${\bf p} = \vec{0}$.}		
			\item{While the $\beta^{new} \neq \beta^{old}$ AND ${\bf y-p} \neq \vec{0}$:}
				\begin{enumerate}
				\item{Update the probabilities using equation 11.3}
				\item{Compute the weights, $w_i$, as $p_i * (1 - p_i)$ and store them along the diagonal of the 
				square matrix} ${\bf W}$.
				\item{Then, calculate $\beta^{new}$:}
					\begin{equation}
					\begin{split}
					\beta^{new} &= \beta^{old} + ({\bf X^T W X})^{-1} {\bf X^T} ({\bf y -p}) \\
					&= ({\bf X^T W X})^{-1} {\bf X^T W} \ ({\bf X} \beta^{old} + ({\bf y -p}))
					\end{split}
					\end{equation}
				\end{enumerate}
					
		\item{Calculate the final log-likelihood with equation 11.5.}
		\end{enumerate}
	
		%\hline
	
		Unfortunately, this algorithm produces some errors when ${\bf X^T W X}$ has eigenvalues near zero. 
		Thus, the inverse is calculated 	using the singular value decomposition. For the case in which N > D, 
		use the conversion ${\bf U (D W)}^{-1} {\bf U^T = (X^T W  X)}^{-1}$.
	
	\item[Backward Stepwise]
		Start with the full data set. Iterate through each feature and remove it from the model. Calculate the 
		new score (see the various score criteria below). For each iteration, keep track of the feature which, 
		when removed, has the best score. At the end of each iteration, see if this score is better than the score 
		of the model with that feature. If it is, permanently remove that feature and repeat the process unless 
		only one feature remains. In this case, throw an error message. If the likelihood is worse, stop.  
	
	\item[Forward Selection]
		Start with just the null model, a data set consisting of a column of ones. In each iteration, go through 
		each feature, adding them into the model one at a time. Calculate the new score, and store the feature 
		that produces the best score. If it is better than the previous model, add in the feature permanently and 
		repeat if other features exist. If not, stop. 
	
	\item[Log-Likelihood Ratio]
		This is the most common score used for backwards stepwise iterations. The equation takes the form 
		\begin{equation}
		        -2 (l(\beta)_{reduced} - l(\beta)_{full}),
		\end{equation} 
		where the reduced model is one feature less than the full model. 
	
	\item[Score Statistic]
		This is commonly the score used in forward selection. It is calculated as: 
		\begin{equation}
		\begin{split}
		score &= (\frac{\partial l}{\partial \beta})^T (\frac{\partial^2 l}{\partial \beta\ \partial \beta^T})^{-1} (\frac{\partial l}{\partial \beta}) \\
		&= [{\bf X^T (y -p)}]^T [{\bf X^T W X}]^{-1} [{\bf X^T (y -p)}].
		\end{split}
		\end{equation}
	
	\item[Training Accuracy]
	\item[Generalization Accuracy]
	\end{description}

\end{subsection}

\begin{subsection}{Important Member Functions}
\end{subsection}

\begin{subsection}{Data Representation}
\end{subsection}

\begin{subsection}{Bugs, Limitations, and Improvements}
Currently, the case in which number of features is greater than the number of samples does not work correctly. Though 
the math is all correct, the identity matrix has an incorrect value at the first position as a cause of the imperfect pseudo-
inverse. One way to correct this error may be to stabilize the inverse with a fraction of the variance, but still needs some 
more work before it completely works. 

One improvement may be to condense the backward and forward stepwise options so that there is one toggle for the 
type of scoring criterion and one for backward vs. forward. This addition would eliminate 4 of the 6 options that currently 
exist. 
\end{subsection}

\end{section}
\vspace{0.5cm}
\hrule
