/*
 *  image_source.cpp
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 7/20/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
//#include <misc_utils.h>
#include "newimage/newimageio.h"
#include "newimage/newimage.h"

#include "image_source.h"
//#include <misc_utils/misc_utils.h>

#include <fstream>
//#include "newimage/newimageall.h"

//FSL libraries
//FSL includes
//#include "newimage/newimageall.h"


using namespace std;
using namespace NEWIMAGE;
using namespace misc_utils_name;

namespace su_mvdisc_name{

	ImageSource::ImageSource()
	{
		
	//	USE_ONLY_NONZERO=1;
		mask_mode = INTERSECTION;
		NodeName="ImageSource";
		im_res = float4(0,0,0,0);
		im_dims = uint3(0,0,0);
		mask=NULL;
		ApplyExternalMask = false;
		NormalizeDataPreMask=false;
        use3Dimages=false;
        use4Dimages=false;
        useMultiImages_t_eq_s=false;
        useAtlas = false;
        dim_include_ = NULL;

		
	}
	ImageSource::~ImageSource()
	{
		//cout<<"ImageSource : Desctructor"<<endl;
		if (mask != NULL)
			delete mask;
        if (dim_include_ != NULL)
            delete dim_include_;
        
	}
	
	void ImageSource::setOptions( std::stringstream & ss_options)
	{
		map<string,string> options = parse_sstream(ss_options);
		
		for ( map<string,string>::iterator it = options.begin(); it!=options.end();it++)
			if ( it->first == "filename")
			{
				setFileName( it->second );
			}else if ( it->first == "files_time_eq_subject")
            {
                setFileNames(  csv_2_vector(it->second) );
                useMultiImages_t_eq_s=true;
            }else if ( it->first == "filelist")
			{
				setFileName( it->second );
                use3Dimages=true;
			}else if ( it->first == "filelist4D")
			{
				setFileName( it->second );
                use4Dimages=true;
			}else if ( it->first == "mask_mode")
			{
				if (it->second =="Intersection")
					mask_mode = INTERSECTION;
				else if (it->second =="Union")
					mask_mode = UNION;
				else if (it->second =="None")
					mask_mode = NONE;
				else 
					throw ClassificationTreeNodeException("Invalid value for mask_mode. Options are: INTERSECTION, UNION,NONE");
				
				
			}else if (it->first == "Normalize_Pre_Mask")
			{
				//cout<<"reached norm pre-mask"<<endl;
				string s_abs = it->second;
				if ((s_abs == "true") || (s_abs == "1"))
					NormalizeDataPreMask = true;
				else if ((s_abs == "false") || (s_abs == "0"))
					NormalizeDataPreMask = false;
				else 
					throw ClassificationTreeNodeException("Image Source: invalid input to NormalizeDataPreMask. ");
				
				
			}else if ( it->first == "external_mask")
			{
				ext_mask_filename = it->second;
				ApplyExternalMask = true;
			
			}else if ( it->first == "atlas")
        {
            atlas_filename = it->second;
            useAtlas = true;
            //            ApplyExternalMask = true;
			
        }else if (it->first == "atlas_dim_include")
        {
            cout<<"foudn atlas include "<<endl;
            dim_include_ = new list<int>();
            vector<int> dims = csv_string2nums_i(it->second);
            for (vector<int>::iterator i = dims.begin(); i != dims.end(); ++i)
                dim_include_->push_back(*i);
            dim_include_->sort();
            
        }else if ( it->first == "SourceType")
			{
				if (it->second == "Training" ) {
					src_type=TRAINING;
				}else if (it->second == "Test" ) {
					src_type=TEST;
				}
				else 
					throw ClassificationTreeNodeException("Image Source: invalid input to SourceType. ");
				
			}else {
				if (!setTreeNodeOptions(it->first, it->second))
					throw ClassificationTreeNodeException((NodeName + " : Invalid input argument : " + it->first).c_str());
			}
		
	}
	
	
	
	vector<int>::iterator ImageSource::mask_begin() 
	{ 
		if (mask == NULL)
			throw ClassificationTreeNodeException("Tried to use mask in image source without setting a masl");
		return mask->begin(); 
	}
	vector<int>::iterator ImageSource::mask_end() { 
		if (mask == NULL)
			throw ClassificationTreeNodeException("Tried to use mask in image source without setting a masl");
		
		return mask->end();
	}
	
	
	float4 ImageSource::res() { return im_res; }

	float ImageSource::xres() { return im_res.x; }
	float ImageSource::yres() { return im_res.y; }
	float ImageSource::zres() { return im_res.z; }
	float ImageSource::tres() { return im_res.t; }

	uint3 ImageSource::size() { return im_dims; }

	unsigned int ImageSource::xsize() { return im_dims.x; }
	unsigned int ImageSource::ysize() { return im_dims.y; }
	unsigned int ImageSource::zsize() { return im_dims.z; }
	
	
    bool ImageSource::includeFeature( const int & cl)
    {
//        cout<<"includeFeature "<<dim_include_->size()<<endl;

        bool inc=1;//default include

        if ( dim_include_ != NULL)
        {
//            cout<<"includeFeature "<<dim_include_->size()<<endl;
        //decide whether to include feature or not
        if (! dim_include_->empty())//if not setting , include all
        {
            inc = 0 ;
            for (list<int>::iterator il = dim_include_->begin(); il != dim_include_->end(); ++il )
                if (*il == cl)
                {
                    inc=1;
                    break;
                }
//            cout<<"Include class "<<cl<<" "<<inc<<endl;
        }
        }
        return inc;
    }

	void ImageSource::readFile( )
	{
        if (Verbose>0)
        {
            cout<<"     ImageSource : readFile "<<filename<<endl;
        }

        
		volume4D<float>* image= new volume4D<float>();
        
        if (use3Dimages)//3D images is acutally timepoint per subject
        {
            // vector<string> im_names;
            ifstream f_in(filename.c_str());
            if (f_in.is_open())
            {
                string name;
                bool first=true;
                while (f_in>>name)
                {
                    if (Verbose>0)
                        cout<<"         Reading image : "<<name<<endl;
                    volume<float> * im3d = new volume<float>();
                    read_volume(*im3d,name);
                    if (first)
                    {
                        image->copyproperties(*im3d);
                        first=false;
                    }
                    
                    image->addvolume(*im3d);
                    delete im3d;
                        
                }
                
//                save_volume4D(*image,"imagegrot4D");
            }else{
                throw ClassificationTreeNodeException(("ImageSource: Unable to read "+filename).c_str());
            }
            
        }else if (useMultiImages_t_eq_s){
            
            for (vector<string>::iterator i_f = v_filenames.begin(); i_f != v_filenames.end(); ++i_f)
            {
                cout<<"         Reading "<<*i_f<<"..."<<endl;
                volume4D<float>* im_grot= new volume4D<float>();
                read_volume4D(*im_grot, *i_f);
                if ( i_f == v_filenames.begin())
                    *image=*im_grot;
                else
                    image->addvolume(*im_grot);
                //  /              for (int t =  ; t < image->tsize(); ++t)
//                image->addvolume((*im_grot)[t]);
            }
//            save_volume4D(*image,"imsrc_combined");
        }else if (use4Dimages){
            if (  out_chunks_ != NULL )
                out_chunks_->clear();
            else
                out_chunks_ = new vector<unsigned int>();

            ifstream f_in(filename.c_str());
            NumberOfChunks_=0;
            if (f_in.is_open())
            {
                vector<unsigned int> * chunks = new vector<unsigned int>();

                string name;
                bool first=true;
                while (f_in>>name)
                {
                    if (Verbose>0)
                    {
                        cout<<"         Reading image : "<<name<<"..."<<endl;
                    }
                    volume4D<float> * im4d_tmp = new volume4D<float>();
                    read_volume4D(*im4d_tmp,name);
                    
                    chunks->insert(chunks->end(), im4d_tmp->tsize(), NumberOfChunks_);
                    out_chunks_->insert(out_chunks_->end(), im4d_tmp->tsize(), NumberOfChunks_);
                    ++NumberOfChunks_;

                    image->addvolume(*im4d_tmp);

                    if (first)
                    {
                        image->copyproperties(*im4d_tmp);
                        first=false;
                    }

                    delete im4d_tmp;
                    
                }
                if (chunks->empty())
                {
                    delete chunks;
                }else{
                    in_chunks_.push_back(chunks);
                }
                
            }
        }else{
            read_volume4D(*image,filename);
        }
        //Done reading files
        
        
        
        vector<float> v_mean,v_var;
		//hanfle stuff for track stats, need to do now because of max

		if (output_feature_mask_ == NULL)
			output_feature_mask_ = new vector<bool>(image->nvoxels(),1);
		else {
			output_feature_mask_->clear();
			output_feature_mask_->resize(image->nvoxels(), 1);
		}
		
		int xsize=image->xsize();
		int ysize=image->ysize();
		int zsize=image->zsize();
		
		im_dims=uint3(xsize,ysize,zsize);
		im_res = float4( image->xdim(),image->ydim(),image->zdim(), image->tdim());

//
//		stats_image_dims.x = im_dims.x;
//		stats_image_dims.y = im_dims.y;
//		stats_image_dims.z = im_dims.z;
//
//		stats_image_res.x = im_res.x;
//		stats_image_res.y = im_res.y;
//		stats_image_res.z = im_res.z;

		//external mask is only used / crated on read in
		volume<short>* ext_mask=NULL;
		if ( ApplyExternalMask )
		{
			cout<<"     Read in mask : "<<ext_mask_filename<<endl;
				ext_mask = new volume<short>();
			read_volume(*ext_mask,ext_mask_filename);
			
			if ( ( ext_mask->xsize() != xsize ) || ( ext_mask->zsize() != zsize ) ||  \
				( ext_mask->xdim() != im_res.x ) || ( ext_mask->ydim() != im_res.y ) ||( ext_mask->zdim() != im_res.z ) )
            {
                cerr<<"ImageSource: Unable to mask image, incompatible dimensions"<<endl;
				throw ClassificationTreeNodeException("ImageSource: Unable to mask image, incompatible dimensions");
		
            }
		}
		int nvoxels=xsize*ysize*zsize;
		
		int tsize=image->tsize();

	
		in_data.clear();

		vector<float>* data_src = new vector<float>();
		
		if (mask_mode != NONE )
		{	
			//create non-zero mask, must ensure its zero overall all time points
			//cout<<"USE ONLY NON-ZERO"<<endl;
			mask = new vector<int>(nvoxels,0);
	
			if ( mask_mode == UNION )
			{
				for (int t=0;t<tsize;t++)
				{
				const float* ptr = (*image)[t].fbegin();
				vector<int>::iterator i_mask=mask->begin();
                    for (int i=0; i<nvoxels;i++, ptr++, i_mask++)
                    {
                        
                        *i_mask+=static_cast<int> ( (*ptr != 0) || (! isnan(*ptr)) );
                    }
				}
                //mask out nan , is implciit to intersecvtion but not union
                for (int t=0;t<tsize;t++)
				{
                    const float* ptr = (*image)[t].fbegin();
                    vector<int>::iterator i_mask=mask->begin();
                    for (int i=0; i<nvoxels;i++, ptr++, i_mask++)
                    {
                        if (isnan(*ptr))
                        *i_mask = 0;
                    }
				}
                
                
			}else if (mask_mode == INTERSECTION) 
			{
				for (int t=0;t<tsize;t++)
				{
					const float* ptr = (*image)[t].fbegin();
					vector<int>::iterator i_mask=mask->begin();
					for (int i=0; i<nvoxels;i++, ptr++, i_mask++)
                    {
                  //          if (*ptr == 0)
                    //        {
                      //        cout<<"found zero "<<t<<" "<<i<<" "<<*ptr<<endl;
                               
                        //  }
						*i_mask+=static_cast<int> ( (*ptr != 0) || (! isnan(*ptr))  );
                    }
                    }	
				vector<int>::iterator i_mask=mask->begin();
				for (int i=0; i<nvoxels;++i, ++i_mask)
					if (*i_mask != tsize) 
                    {
                       // *i_out_feat = 0;
						*i_mask = 0 ;
                       // cout<<"inplicit mask"<<endl;
                    }
            }
            
            
            
            
			//----------mask the implicit mask-----------------//
			if (NormalizeDataPreMask)
			{//need to calculate normalization prior to external mask
				//cout<<"DO NORMALZIATIONO PRE_MASK "<<endl;
				for (int t=0;t<tsize;t++)
				{
					float sx=0, sxx=0;
					unsigned int N_mask = 0 ; 
					const float* ptr = (*image)[t].fbegin();
					vector<int>::iterator i_mask=mask->begin();
					for (int i=0; i<nvoxels;i++, ptr++, i_mask++)
						if (*i_mask)
						{
							++N_mask;
							sx+=*ptr;
							sxx+=(*ptr)*(*ptr);
						}
				//	cout<<"mean "<<sx/N_mask<<" "<<N_mask<<" "<<sqrt((sxx - sx*sx/N_mask)/(N_mask-1))<<endl;
					v_mean.push_back(sx/N_mask);
					v_var.push_back( sqrt((sxx - sx*sx/N_mask)/(N_mask-1)) );
					
					//does the nromalization using new image
					(*image)[t] -= sx/N_mask;
					(*image)[t] /= sqrt((sxx - sx*sx/N_mask)/(N_mask-1));

				}
				
			}
			
			
			//mask the mask
			if (	ApplyExternalMask )
			{	
				vector<bool>::iterator i_out_feat = output_feature_mask_->begin();
				const short* ptr = ext_mask->fbegin();
                //int count=0;
				for (vector<int>::iterator i_m = mask->begin(); i_m!= mask->end();++i_m,++ptr,++i_out_feat)
                {
                   // cout<<"mask "<<count<<" "<<*ptr<<endl;
                  //  ++count;
                    if (*ptr == 0 ) {
						*i_m = 0;
						*i_out_feat=0;
					}
                }
				
			}
            
			//----------done mask the implicit mask-----------------//
//			cout<<"useAtlas "<<useAtlas<<" "<<atlas_filename<<endl;
            if ( useAtlas )//Going to average within atlas regions
            {
                cout<<"     Averaging over regions of Atlas..."<<endl;
                volume4D<float>* im_atlas = new volume4D<float>();
                read_volume4D(*im_atlas,atlas_filename);
                int Nt = im_atlas->tsize();
                vector< vector<float> > vdata;

                if (Nt == 1 )
                {//assume multiple labels in single volume, will work for 1 label
                    delete im_atlas;
                    volume<int>* im_atlas3D = new volume<int>();
                    read_volume(*im_atlas3D,atlas_filename);
                    
                    map<int,int> class_2_count;
                    
                    {
                        const int* ptr = im_atlas3D->fbegin();
                        vector<int>::iterator i_mask=mask->begin();
                        //mask conatins explicit and implict mask
                        for (int i=0; i<nvoxels;i++, ++ptr,++i_mask)
                            if ( ( *i_mask ) && ( *ptr !=0 ) )
                            {
                                class_2_count[*ptr]++;
                            }
                    }
                    map<int,int> class_2_index;
                    int index=0;
                    for (map<int,int>::iterator i = class_2_count.begin(); i != class_2_count.end(); ++i,++index )
                    {
                        cout<<"     ATLAS class found "<<i->first<<endl;
                        class_2_index[i->first]=index;
                    }
                    for (map<int,int>::iterator i = class_2_index.begin(); i != class_2_index.end(); ++i)
                        cout<<"     ATLAS class 2 index found "<<i->first<<" "<<i->second<<endl;
                    
                    vdata.resize(tsize,vector<float>(class_2_count.size(),0));
                    {
                        const int* ptr = im_atlas3D->fbegin();
                        vector<int>::iterator i_mask = mask->begin();
                        for (int i=0; i<nvoxels;i++, ++ptr,++i_mask)
                        {
//                            cout<<"voxel "<<i<<" "<<*ptr<<" "<<*i_mask<<endl;
                        if ( ( *i_mask ) && ( *ptr !=0 ) )
                            {
                            
                                for (int t=0;t<tsize;t++)
                                {
//                                    cout<<"vdata "<<vdata.size()<<" "<<class_2_index[*ptr]<<endl;
//                                    cout<<"vdata "<<vdata[0].size()<<endl;
//
                                    vdata[t].at(class_2_index[*ptr]) += *((*image)[t].fbegin() + i);

//                                    vdata[ class_2_index[*ptr] ].at(t) += *((*image)[t].fbegin() + i);
                                }
                            }
                        }
                        
                    }
//                    cout<<"normalize vdata "<<vdata.size()<<endl;
                    index=0;
                    for (map<int,int>::iterator i = class_2_count.begin(); i != class_2_count.end(); ++i,++index )
                    {
//                        cout<<"normalize sum to create average - ATLAS class found "<<i->first<<" "<<i->second<<endl;
//                        class_2_index[i->first]=index;
        
//                        if (includeFeature(index)) //index vs i->first is differnece between using actual labels or indices
                        {
//                            cout<<"I'm including this feature :D "<<i->first<<endl;
                            
                            for (int t=0;t<tsize;t++)
                            {
                                if (i->second >0)
                                    
                                    vdata[ t ].at(index) /= i->second;
                                else
                                    vdata[ t ].at(index) /= 0;
                                
                            }
                        }
                        
                    }
                    
                    
                    
                }else
                {
                    //Nt = number of classes
                    vdata.resize(tsize,vector<float>(Nt,0));
                    vector<float> class_count(Nt,0);
                    //sum voxels values for each ROI, normalization constant in mean
                    
//                    cout<<"Calculating sum of voxels for each ROI..."<<endl;
                    for (int cl = 0 ; cl < Nt ; ++cl)
                    {
                        vector<int>::iterator i_mask = mask->begin();
                        const float* ptr = (*im_atlas)[cl].fbegin();
                        for (int i=0; i<nvoxels;i++, ++ptr,++i_mask)
                        {
                            if ( *i_mask )
                            {
                                class_count[cl] += (*ptr);
                            }
                        }

                        
                    }
                    
                    {
                        
//averaging within rois
                        for (int cl = 0 ; cl < Nt ; ++cl)
                        {
//                            cout<<"ROI "<<cl<<"..."<<endl;
                            const float* val = (*im_atlas)[cl].fbegin();
                            vector<int>::iterator i_mask = mask->begin();
                            for (int i=0; i<nvoxels;i++, ++i_mask,++val)
                            {
                                if ( (*val) > 0 )
                                {
                                    for (int t=0;t<tsize;t++)
                                    {
                                  
                                        const float* ptr = (*image)[t].fbegin() + i ;
                                      
                                        vdata[t].at(cl)+= (*val) * (*ptr);
                                    }
                                }
                            }
                            
                        }

                        
                        
                        
                        
//                        cout<<"Creating average from sums..."<<endl;
                        for (int t=0;t<tsize;t++)
                        for (int cl = 0 ; cl < Nt ; ++cl)
                        {
//                            cout<<"clavg "<<cl<<" "<<vdata[t].at(cl)<<" "<<class_count[cl]<<endl;
//                            cout<<"include feature "<<cl<<" "<<includeFeature(cl)<<endl;
//                            if (includeFeature(cl))
                            {
//                                cout<<"I'm including this feature :D "<<cl<<endl;
                                //                            cout<<"clavgpost "<<cl<<" "<<vdata[t].at(cl)<<" "<<class_count[cl]<<endl;
                                if (class_count[cl] >0)
                                    vdata[t].at(cl) /= class_count[cl];
                                else
                                    vdata[t].at(cl) /= class_count[cl];
                            }
                        }
                        
                    }

                    delete im_atlas;
                }
                
                //add to data src
                int index = 0 ;
                for (vector< vector<float> >::iterator i = vdata.begin(); i != vdata.end(); ++i,++index)
                {
//                    if (includeFeature(index))
                        data_src->insert(data_src->end(),i->begin(), i->end());
                }

                cout<<"     done atlas processing..."<<endl;
                
            }else{
                
              
                for (int t=0;t<tsize;t++)
                {
                    const float* ptr = (*image)[t].fbegin();
                    vector<int>::iterator i_mask=mask->begin();
                    vector<bool>::iterator i_out_feat = output_feature_mask_->begin();
                    for (int i=0; i<nvoxels;i++, ptr++, i_mask++,++i_out_feat)
                        if (*i_mask)
                            data_src->push_back(*ptr);
                    //alreadyt done above
                    //                    else
                    //                        *i_out_feat = 0;
                }
            }
            
            //clean up implicit mask
//           delete mask;
            
		}else {
			if (NormalizeDataPreMask)
			{//need to calculate normalization prior to external mask
				for (int t=0;t<tsize;t++)
				{
					float sx=0, sxx=0;
					unsigned int N_mask = 0 ; 
					const float* ptr = (*image)[t].fbegin();
					vector<int>::iterator i_mask=mask->begin();
					for (int i=0; i<nvoxels;i++, ptr++, i_mask++)
						if (*i_mask)
						{
							++N_mask;
							sx+=*ptr;
							sxx+=(*ptr)*(*ptr);
						}
					
					v_mean.push_back(sx/N_mask);
					v_var.push_back( (sxx - sx*sx/N_mask)/(N_mask-1) );
					//does the nromalization using new image
					(*image)[t] -= sx/N_mask;
					(*image)[t] /= (sxx - sx*sx/N_mask)/(N_mask-1);
					
				}
				
			}
            
          

			for (int t=0;t<tsize;t++)
			{
				
				//wheher to apply an external mask
				if (	ApplyExternalMask )
				{	
					const short* ext_mask_ptr = ext_mask->fbegin();
					const float* ptr = (*image)[t].fbegin();
                    vector<bool>::iterator i_out_feat = output_feature_mask_->begin();//i_out_feat is the feat selection, for mapping voxels later
                    
					for (int i=0; i<nvoxels;++i, ++ptr,++ext_mask_ptr,++i_out_feat)
                        if (ext_mask_ptr != 0)
                            data_src->push_back(*ptr);
                        else
                            *i_out_feat = 0;
                    
					
				}else {
										
					const float* ptr = (*image)[t].fbegin();
					for (int i=0; i<nvoxels;i++, ptr++)
						data_src->push_back(*ptr);
					
				}
				
			}
			
			
			
			
		}
       
        
		NumberOfSources_=1;
		Dimensionality_=data_src->size()/tsize;
		NumberOfSamples_=tsize;
		DimTrim=static_cast<unsigned int>(image->nvoxels())-Dimensionality_;
//		cout<<"Dims and stuff "<<Dimensionality_<<" "<<data_src->size()<<endl;
		in_data.push_back(data_src);
		v_dims_.push_back(Dimensionality_);
        
        if (src_image_res_ == NULL)
            src_image_res_ = new vector<float4>();
        
        if (src_image_dims_ == NULL)
            src_image_dims_ = new vector<uint3>();
        
        if (src_names_ == NULL)
            src_names_ = new vector<string>();
        
        
        if (src_dims_ == NULL)
            src_dims_ = new vector<unsigned int>();
        
        src_image_res_->push_back(im_res);
        src_image_dims_->push_back(im_dims);
        src_names_->push_back(NodeName);
        src_dims_->push_back(image->nvoxels());

//        src_dims_->push_back(image->nvoxels()/tsize);

        
        if (Verbose>0)
            cout<<"ImageSource Dimensionaltiy/Number of Samples "<<Dimensionality_<<" "<<NumberOfSamples_<<" "<<endl;
		
		if ( ApplyExternalMask )
			delete ext_mask;
		
		delete image;
        

	}


}
