//
//  File.cpp
//  su_mvdisc
//
//  Created by Brian Patenaude on 12/6/12.
//  Copyright (c) 2012 Stanford University - Etkin Lab. All rights reserved.
//


//STL includes
#include <map>
#include "decisionTree.h"
#include "misc_utils.h"
#include "tree_sampler.h"
#include <cmath>
#include <fstream>
//#include <unordered_set>
using namespace std;
using namespace misc_utils_name;
using namespace clapack_utils_name;

namespace su_mvdisc_name{

    DecisionTreeLeaf::DecisionTreeLeaf(){
        parent_ = NULL;
        child0_ = NULL;
        child1_ = NULL;
        isTerminal_ = true;
       // m_data_ = new smatrix();
       // m_target_ = new smatrix();
        split_feature_ = 0;
        split_threshold_ = 0;
        split_cost_ = 0; 
        split_levels_max_=10;
        split_level_=0;
        majority_lb_=0;
        imp_criteria_=GINI_INDEX;
        visited_=0;
//        imp_criteria_=DEVIANCE;

        collapsed_=false;
    }
    DecisionTreeLeaf::DecisionTreeLeaf( DecisionTreeLeaf* leaf )
    {
        parent_ = NULL;
        child0_ = NULL;
        child1_ = NULL;
        isTerminal_=leaf->isTerminal_;
        split_feature_ = leaf->split_feature_;
        split_threshold_ = leaf->split_threshold_;
        split_cost_ = leaf->split_cost_;
        split_levels_max_=leaf->split_levels_max_;
        split_level_=leaf->split_level_;
        majority_lb_=leaf->majority_lb_;
        imp_criteria_=leaf->imp_criteria_;
//        //recursive copy constructor
        m_data_ = leaf->m_data_;
        m_target_ = leaf->m_target_;
        v_lb_ = leaf->v_lb_;
        collapsed_= leaf->collapsed_;

        if (leaf->child0_ != NULL){
            child0_ = new DecisionTreeLeaf( leaf->child0_);
            child0_->parent_ = this;
        }
        if (leaf->child1_ != NULL){
            child1_ = new DecisionTreeLeaf( leaf->child1_);
            child1_->parent_ = this;
        }
        visited_ =leaf->hasBeenVisited();
    }
    DecisionTreeLeaf::DecisionTreeLeaf( DecisionTreeLeaf* leaf, list<DecisionTreeLeaf*> & l_leaves )
    {
//        cerr<<"create leaf "<<split_threshold_<<endl;

        parent_ = NULL;
        child0_ = NULL;
        child1_ = NULL;
//        cerr<<"create leaf 1 "<<split_threshold_<<endl;

        isTerminal_=leaf->isTerminal_;
//        cerr<<"create leaf 2 "<<split_threshold_<<endl;
        visited_ =leaf->hasBeenVisited();

        split_feature_ = leaf->split_feature_;
        split_threshold_ = leaf->split_threshold_;
        split_cost_ = leaf->split_cost_;
        split_levels_max_=leaf->split_levels_max_;
        split_level_=leaf->split_level_;
        majority_lb_=leaf->majority_lb_;
        imp_criteria_=leaf->imp_criteria_;
//        cerr<<"create leaf 3 "<<split_threshold_<<endl;

        l_leaves.push_back(this);
//        cerr<<"create leaf 4 "<<split_threshold_<<endl;
        
        m_data_ = leaf->m_data_;
//        cerr<<"create leaf 5 "<<split_threshold_<<endl;

        m_target_ = leaf->m_target_;
//        cerr<<"create leaf 6 "<<split_threshold_<<endl;

        v_lb_ = leaf->v_lb_;
        collapsed_= leaf->collapsed_;

        
        //recursive copy constructor
//        cerr<<"create leaf chold0 "<<split_threshold_<<endl;

        if (leaf->child0_ != NULL){
            child0_ = new DecisionTreeLeaf( leaf->child0_, l_leaves);
            child0_->parent_ = this;
        }
        
//        cerr<<"create leaf chgiuld  1"<<split_threshold_<<endl;

        if (leaf->child1_ != NULL){
            child1_ = new DecisionTreeLeaf( leaf->child1_, l_leaves);
            child1_->parent_ = this;
        }

    }

    void DecisionTreeLeaf::getLeaves( list<DecisionTreeLeaf*> & leaves )
    {
//        cerr<<"*****DecisionTreeLeaf::getleave**** "<<leaves.size()<<endl;

        leaves.push_back(this);
//        cerr<<"*****DecisionTreeLeaf::getleave push back*******"<<endl;
//        cerr<<"getleave child 01"<<endl;

           if (child0_ != NULL)
        {
//            cerr<<"getleave child 0"<<endl;
            child0_->getLeaves(leaves);
        }

//        cerr<<"*****DecisionTreeLeaf::getleave move one to child 1*******"<<endl;

         if (child1_ != NULL)
        {
//            cerr<<"getleave child 1"<<endl;
            child1_->getLeaves(leaves);
        }
//        cerr<<"done *****DecisionTreeLeaf::getleave****"<<endl;

    }

    
       DecisionTreeLeaf::DecisionTreeLeaf(const unsigned int & NSamples, const unsigned int & NDimensions )
    {

        parent_ = NULL;
        child0_ = NULL;
        child1_ = NULL;
        isTerminal_ = true;

        m_data_.resize(NSamples, NDimensions);

        for (unsigned int i = 0 ; i < NDimensions; ++i )
            l_feat_inds_.push_back(i);

        m_target_.resize(NSamples,1);
        visited_ = 0;
        split_feature_ = 0;
        split_threshold_ = 0;
        split_cost_ = 0;
        split_levels_max_=10;
        split_level_=0;
        majority_lb_=0;
        imp_criteria_=GINI_INDEX;
        collapsed_= false;
    }

    DecisionTreeLeaf::~DecisionTreeLeaf(){
        
      
        
 
    }
    
    
    void DecisionTreeLeaf::getDataAndTarget( DecisionTreeLeaf * leaf ){
        int child = 0 ;
        
        child = ( child0_ == leaf ) ? 0 : (child1_ == leaf) ? 1 : throw ClassificationTreeNodeException("There is a discrpency in your tree, parent/child relationship is not reciprocated");
       // cerr<<"is Child "<<child<<endl;
        
        
        unsigned int Nsamples= m_data_.MRows;
        unsigned int Nfeatures = m_data_.NCols;
        unsigned int N0(0),N1(0);
        
        float* f_ptr = m_data_.sdata + split_feature_ * Nsamples;
        vector<bool> m_0(Nsamples,0);//select child 0 data
        if (collapsed_){//setup selection mask , inlclude all if collapsed, but depends on which child for reuse of later code
            N0=N1=Nsamples;
            if (child==0)
                m_0.assign(Nsamples, 1);
            else if (child ==1 )
                m_0.assign(Nsamples, 0);
            
        }else{
            for ( vector<bool>::iterator i_m_0 = m_0.begin(); i_m_0 != m_0.end(); ++i_m_0,++f_ptr)
            {
                if ( *f_ptr > split_threshold_ )
                    ++N1;
                else{
                    ++N0;
                    *i_m_0=1;
                }
            }
        }
        
        //no more collapsed_ dependent code past here
        
        if (child == 0 ) leaf->setSize(N0,Nfeatures-1);
        else if (child == 1 ) leaf->setSize(N1, Nfeatures-1);
        
        
        float* l_ptr = leaf->getDataPointer();//to copy data to
        f_ptr = m_data_.sdata;//to copy data from, reset pointer
        
        for (unsigned int feat = 0 ; feat < Nfeatures; ++feat)
        {//copy in data
            if (feat == split_feature_)
            {
                f_ptr += Nsamples;//skip the column
                continue;
            }
            
            vector<bool>::iterator i_m0 = m_0.begin();
            //  copy in feature
            for (; i_m0 != m_0.end(); ++i_m0,++f_ptr)
            {
                if ((*i_m0 == 1) && (child == 0 )){
                    *l_ptr = *f_ptr;
                    ++l_ptr;
                }else if ((*i_m0 == 0) && (child == 1 ))
                {
                    *l_ptr = *f_ptr;
                    ++l_ptr;
                }
            }
            
        }
        
        //process target
        {
            
            float* l_tptr = leaf->getTargetPointer();//to copy data to
            float* t_ptr = m_target_.sdata;
            vector<bool>::iterator i_m0 = m_0.begin();
            for (; i_m0 != m_0.end(); ++i_m0,++t_ptr)
            {
                if ((*i_m0 == 1) && (child == 0 )){
                    *l_tptr = *t_ptr;
                    ++l_tptr;
                }else if ((*i_m0 == 0) && (child == 1 ))
                {
                    *l_tptr = *t_ptr;
                    ++l_tptr;
                }

              }
            
            
        }

        
        
        
          //
//            float* f_ptr = m_data_.sdata + split_feature_ * Nsamples;
//            unsigned int N0(0),N1(0);
//            
//            //lets create some masks
//            vector<bool> m_0(Nsamples,0);//,m_1(Nsamples,0);
//
//            //create leaves
//            
//            //only proceed if there is data left
////            if ( ( N0==0) || (N1==0) )
////                return 0;
//            
//
//            //   done copying in the data
//            cerr<<" //setuop connections"<<endl;
    }
    
    void  DecisionTreeLeaf::getDataAndTarget( )
    {
        // DecisionTreeLeaf* current_leaf;
//        cerr<<"Is collapsed -pre : "<<collapsed_<<" -Feature "<<split_feature_<<" - Threshold : "<<split_threshold_<<" --level "<<split_level_<<endl;
//        smatrix_print(m_data_, "m_data_pre");

        if ( parent_ != NULL )//are we not at root
        {
            parent_->getDataAndTarget();
                        parent_->getDataAndTarget(this);//this will do threshold or just pass data, depending if collapsed, not a recursive call
        
        }
//        cerr<<"Is collapsed : "<<collapsed_<<" -Feature "<<split_feature_<<" - Threshold : "<<split_threshold_<<" --level "<<split_level_<<endl;
//
//        smatrix_print(m_data_, "m_data_");
//        smatrix_print(m_target_, "m_target_");
//
//        else{
//            //handle root case
//            
//            if ( collapsed_ )
//            {
//                cerr<<"root is collapsed "<<endl;
//            }else{
//                cerr<<"root is not collapsed"<<endl;
//            }
//
//        }
        
    }
    void DecisionTreeLeaf::trim() {
         cerr<<"DecisionTreeLeaf::trim  "<<split_threshold_<<" "<<split_level_<<endl;
        if ( child0_ != NULL )
        {
//            cerr<<"trim recursive0"<<endl;
            child0_->trim();
            delete child0_;
            child0_=NULL;
//            cerr<<"delete chld 0"<<endl;
        }
        if ( child1_ != NULL )
        {
//            cerr<<"trim recursive1"<<endl;

            child1_->trim();
            delete child1_;
            child1_=NULL;
//            cerr<<"delete chld 1"<<endl;

        }
        
        isTerminal_=true;
    }
    
    void DecisionTreeLeaf::setSize(const unsigned int & NSamples, const unsigned int & NDimensions ) {
        m_data_.resize(NSamples, NDimensions);
        m_target_.resize(NSamples, 1);
    }
    void DecisionTreeLeaf::setFeatureNames( const vector<string> & names)
    {
        feature_names_=names;
    }

    void DecisionTreeLeaf::setData(const clapack_utils_name::smatrix & m_data_in)
    {
        m_data_=m_data_in;// = new smatrix(m_data_in);
        l_feat_inds_.clear();
        for (__CLPK_integer i = 0 ; i < m_data_.NCols; ++i )
            l_feat_inds_.push_back(i);
    }
    void DecisionTreeLeaf::setTarget(const clapack_utils_name::smatrix & m_target_in)
    {
        m_target_ = m_target_in;
    }
    
    void DecisionTreeLeaf::setClassLabels( const std::vector<int> & v_lb_in )
    {
        v_lb_ = v_lb_in;
    }
    std::string DecisionTreeLeaf::getSplitFeatureName()
    {
        if ( split_feature_ < l_feat_inds_.size() )
        {
            list<unsigned int>::iterator i_f = l_feat_inds_.begin();
            advance(i_f,split_feature_);
        return feature_names_[*i_f];
            //l_feat_inds_.at(split_feature_);
            //feature_names_[split_feature_];
        }
        return ("feature_"+num2str(split_feature_));
    }

    void DecisionTreeLeaf::setAndRemoveFeatureIndices(const std::list<unsigned int> & l_feat_inds , const unsigned int & split_feature )
    {
            if (l_feat_inds_.empty())//dont remove anything  if no features left
                return ;
        l_feat_inds_ = l_feat_inds;
    
        list<unsigned int>::iterator i = l_feat_inds_.begin();
        advance(i, split_feature);
        l_feat_inds_.erase(i);
    }
    unsigned int DecisionTreeLeaf::getFeatureIndex( const unsigned int & feat ){
        list<unsigned int>::iterator i = l_feat_inds_.begin();
        advance(i, feat);
        return *i;

    }
    void DecisionTreeLeaf::printFeatureIndices(const std::string & name ){
        cerr<<name<<" : ";
        for (std::list<unsigned int>::iterator i_l = l_feat_inds_.begin(); i_l != l_feat_inds_.end(); ++i_l )
        {
            cerr<<*i_l<<" ";
        }
        cerr<<endl;
    }
    
    int DecisionTreeLeaf::isChild( DecisionTreeLeaf* leaf )
    {
        if ( leaf == child0_ )
            return 0;
        else if ( leaf == child1_ )
            return 1;
        
        return -1;
    }

    
    DecisionTreeLeaf* DecisionTreeLeaf::getChild( const int & index ){
         if (index==0)
             return child0_;
         else if (index ==1 )
             return child1_;
         else
             throw ClassificationTreeException("Invalid child number");
     }
    DecisionTreeLeaf* DecisionTreeLeaf::getParent( )
    {
        return parent_;
    }

    
    float DecisionTreeLeaf::impurity( )
    {
//        float impurity_val = 0 ; 
//        if ( imp_criteria_ == GINI_INDEX )
//        {
//            //const float * p_m_col = m_col.sdata;
//            //const float * p_targ = targ.sdata;
////         smatrix_print(m_target_,"m_target");
//            smatrix p_mk_0(v_lb_.size(),1);
//            p_mk_0 = 0 ;
//            //unsigned int N0(0);
//            //vector<unsigned int> v_N(v_lb_.size(),0);
//            unsigned int Nele = m_data_.MRows;
//          //  cerr<<"Calculate gini index "<<Nele<<endl;
//
//            float* p_targ = m_target_.sdata;
//            for (unsigned int i  = 0 ; i < Nele ; ++i, ++p_targ)
//            {
//             //   cerr<<"element " << i <<" / "<<Nele<<" -- " << *p_targ<<endl;
//                unsigned int index=0;
//                for ( vector<int>::iterator i_lb = v_lb_.begin(); i_lb != v_lb_.end(); ++i_lb,++index)
//                    if (*p_targ == *i_lb)
//                        *(p_mk_0.sdata + index) = *(p_mk_0.sdata + index) +1;
//                
//            }
//            
//            
//           // smatrix_print(p_mk_0, "p_mk_0-pre");
//           // print(v_N,"v_N");
//            float* ptr_p_mk_0 = p_mk_0.sdata;
//            for (unsigned int i  = 0 ; i < v_lb_.size() ; ++i, ++ptr_p_mk_0)
//            {
//                    *ptr_p_mk_0  /= Nele;
//            }
//            
//            
//            //cerr<<"N9N1 "<<N0<<" "<<N1<<endl;
//            
////            smatrix_print(p_mk_0, "p_mk_0");
//            //smatrix_print(p_mk_1, "p_mk_1");
//            impurity_val = gini_index(p_mk_0);
//
//            
////            cerr<<"imp val "<<impurity_val<<endl;
//            
//        }
//        return impurity_val;
        unsigned int Nele= m_target_.MRows;
//        cout<<"Nele "<<Nele<<" "<<endl;
        float imp=1e6;
        unsigned int N(0);
        float* p_targ = m_target_.sdata;
        map<int,int> lb_count;
        for (vector<int>::iterator i_lb = v_lb_.begin(); i_lb != v_lb_.end(); ++i_lb )
            lb_count[*i_lb]=0;
        
        for (unsigned int i  = 0 ; i < Nele ; ++i,++p_targ)
        {
//            cout<<"impurity target "<<*p_targ<<endl;
            lb_count[*p_targ]++;
        }

        if (imp_criteria_ == GINI_INDEX )
        {
            imp = gini_index(lb_count);
        }else if (imp_criteria_ == DEVIANCE )
        {
            imp = cross_entropy(lb_count);
        }
        //cerr<<"gini index "<<gini_0<<" "<<gini_1<<endl;
        return imp;


    }
    float DecisionTreeLeaf::cross_entropy( const smatrix & m_p )
    {
        float cr_ent=0;
        for ( __CLPK_integer i = 0 ; i < m_p.MRows; ++i )
        {
            float p = *( m_p.sdata + i );
            cr_ent+= -1*p * log( p );
        }
        return cr_ent;
    }
    float DecisionTreeLeaf::cross_entropy( const map<int,int> & m_p  )
    {
        float cr_ent=0;
        unsigned int Ntotal = 0 ;
        for (map<int,int>::const_iterator i = m_p.begin(); i != m_p.end(); ++i )
        {
            Ntotal+=i->second;
        }
        for (map<int,int>::const_iterator i = m_p.begin(); i != m_p.end(); ++i )
        {
            float p = static_cast<float>(i->second)/Ntotal;
            cr_ent+= -1*p * log( p );
        }
        return cr_ent;
    }

    float DecisionTreeLeaf::gini_index( const map<int,int> & m_p )
    {
        float gini=0;
        unsigned int Ntotal = 0 ;
        for (map<int,int>::const_iterator i = m_p.begin(); i != m_p.end(); ++i )
        {
            
            Ntotal+=i->second;
        }
        for (map<int,int>::const_iterator i = m_p.begin(); i != m_p.end(); ++i )
        {
            float p = static_cast<float>(i->second)/Ntotal;
//            cout<<"p "<<p<<endl;
            gini+= p * ( 1 -p );
        }
        return gini;
    }
    
    float DecisionTreeLeaf::gini_index( const smatrix & m_p )
    {
        float gini=0;
        for ( __CLPK_integer i = 0 ; i < m_p.MRows; ++i )
        {
            float p = *( m_p.sdata + i );
            gini+= p * ( 1 -p );
        }
        return gini;
    }
//    void DecisionTreeLeaf::split( DecisionTreeLeaf* leaf0, DecisionTreeLeaf* leaf1 )
    bool DecisionTreeLeaf::isPure()
    {
        unsigned int n_samples = m_target_.MRows;
        const float* t_ptr = m_target_.sdata;
        float val=*t_ptr;
        ++t_ptr;

        for (unsigned int i = 1 ;i <n_samples; ++i,++t_ptr)
        {
            if (val != *t_ptr)
                return 0;
        }
        return 1;
        
    }

    
    int DecisionTreeLeaf::split( std::list<DecisionTreeLeaf*> & leaves , unsigned int split_level )
    {
	cout<<"DecisionTreeLeaf::split"<<endl;
       split_level_=split_level;
        unsigned int Nsamples= m_data_.MRows;
        unsigned int Nfeatures = m_data_.NCols;
//    cout<<"feature names size "<<feature_names_.size()<<endl;
//        cerr<<"SPLIT_LEVELa "<<split_level_<<" / "<<split_levels_max_<<" "<<Nsamples<<" "<<Nfeatures<<" "<<split_feature_<<" "<<feature_names_[split_feature_]<<" "<<split_threshold_<<endl;
//                cout<<"return if not "<<endl;

        if (split_level_ > split_levels_max_)//not sure i need this, but it's an extra safe guard
            return 0;
//          cout<<"calculate purity "<<endl;

//cout<<"isPure "<<isPure()<<endl;
        if (isPure()) //dont continue to split if it is a pure node.
        {
            // node is pure...leaving
            return 0;
        }
        
        
//        cout<<"not returning "<<endl;


        float* f_ptr = m_data_.sdata + split_feature_ * Nsamples;
        unsigned int N0(0),N1(0);
        //lets create some masks
        vector<bool> m_0(Nsamples,0);//,m_1(Nsamples,0);
      //  vector<bool>::iterator i_m_1 = m_1.begin();

        for ( vector<bool>::iterator i_m_0 = m_0.begin(); i_m_0 != m_0.end(); ++i_m_0,++f_ptr)
        {
            if ( *f_ptr > split_threshold_ )
                ++N1;
            else{
                ++N0;
                *i_m_0 = 1;
            }
        }

        //create leaves
        
        //only proceed if there is data left
        if ( ( N0==0) || (N1==0) )
            return 0;
            
        
        
        if ( (split_level_+1) >= split_levels_max_)
            return 0;//don't want to do estimation unless i have to

//       cout<<"create leaves "<<endl;
        DecisionTreeLeaf *leaf0 = new DecisionTreeLeaf(N0,Nfeatures-1);
        leaf0->setSplitLevelMax(split_levels_max_);
        DecisionTreeLeaf *leaf1 = new DecisionTreeLeaf(N1,Nfeatures-1);
        leaf1->setSplitLevelMax(split_levels_max_);

//        cerr<<"done creating leaves"<<endl;
        
        leaf0->setAndRemoveFeatureIndices(l_feat_inds_,split_feature_);
       leaf1->setAndRemoveFeatureIndices(l_feat_inds_,split_feature_);
        leaf0->setSplitLevel(split_level_+1);
        leaf1->setSplitLevel(split_level_+1);
     //   cerr<<"done passing on feature info"<<endl;

        leaves.push_back(leaf0);
        leaves.push_back(leaf1);
        isTerminal_=false;

        //remove and pass down feature names
        vector<string> names_split(l_feat_inds_.size());
//        print(feature_names_,"split-feature_names");
        vector<string>::iterator i_split = names_split.begin();
        for (list<unsigned int>::iterator i = l_feat_inds_.begin(); i != l_feat_inds_.end(); ++i,++i_split)
        {
//            cerr<<"index "<<*i<<" "<<names_split.size()<<" "<<l_feat_inds_.size()<<endl;
        *i_split = feature_names_[*i];
        }
//        leaf0->setFeatureNames(names_split);
//        leaf1->setFeatureNames(names_split);
        leaf0->setFeatureNames(feature_names_);
        leaf1->setFeatureNames(feature_names_);

        
        
        float* l0_ptr = leaf0->getDataPointer();//to copy data to
        float* l1_ptr = leaf1->getDataPointer();//to copy data to
       
        f_ptr = m_data_.sdata;//to copy data from, reset pointer

        for (unsigned int feat = 0 ; feat < Nfeatures; ++feat)
        {//copy in data
            if (feat == split_feature_)
            {
                f_ptr += Nsamples;//skip the column
                continue;
            }
            
            vector<bool>::iterator i_m0 = m_0.begin();
            //  copy in feature 
            for (; i_m0 != m_0.end(); ++i_m0,++f_ptr)
            {
                if (*i_m0 == 1){
                    *l0_ptr = *f_ptr;
                    ++l0_ptr;
                }else{
                    *l1_ptr = *f_ptr;
                    ++l1_ptr;
                }
            }
            
        }

     //   done copying in the data
//        smatrix_print(m_data_,"m_data");
        
        //process target
        {
            
            float* l0_tptr = leaf0->getTargetPointer();//to copy data to
            float* l1_tptr = leaf1->getTargetPointer();//to copy data to
            float* t_ptr = m_target_.sdata;
            vector<bool>::iterator i_m0 = m_0.begin();
            // vector<bool>::iterator i_m1 = m_1.begin();
            for (; i_m0 != m_0.end(); ++i_m0,++t_ptr)
            {
                if (*i_m0 == 1){
                    *l0_tptr = *t_ptr;
                    ++l0_tptr;
                }else{
                    *l1_tptr = *t_ptr;
                    ++l1_tptr;
                }
                }
            
            
        }
//        smatrix_print(m_target_,"m_target_");

//        cerr<<" //setuop connections"<<endl;
//        print(*out_data_names_,"feature names");
        //setuop connections
        child0_=leaf0;
        child1_=leaf1;
        child0_->setClassLabels(v_lb_);
        child1_->setClassLabels(v_lb_);
        child0_->est_binary_split();
        child1_->est_binary_split();
        child0_->setParent(this);
        child1_->setParent(this);
        if (Nfeatures == 1 )//stop condition because no more feature to divide with, childreb have zero
            return 0;
        
        
//        cerr<<"start recursive call"<<endl;
   
                child0_->split(leaves,split_level_+1);

                child1_->split(leaves,split_level_+1);
        
        
        return 1; //succesful split
        

    }

    
    float DecisionTreeLeaf::split_cost(const float *p_m_col, const float * p_targ, const vector<int> & v_lb, const unsigned int Nele, float thresh )
    {//implemented for binary only
        smatrix p_mk_0(v_lb.size(),1);
        p_mk_0 = 0 ;
        smatrix p_mk_1(v_lb.size(),1);
        p_mk_1 = 0 ;
        float imp0(0),imp1(0);
        unsigned int N0(0),N1(0);
        for (unsigned int i  = 0 ; i < Nele ; ++i,++p_m_col,++p_targ)
        {
            //  cerr<<"ele "<<*p_m_col<<" "<<thresh<<" "<<*p_targ;
            if ( *p_m_col > thresh )
            {
                //  cerr<<" g0"<<endl;
                ++N1;
                if (*p_targ == v_lb[0])
                    ++( *(p_mk_1.sdata) ) ;
                else if (*p_targ == v_lb[1])
                    ++( *(p_mk_1.sdata+1) ) ;
            }else{
                //  cerr<<" g1"<<endl;
                
                ++N0;
                if (*p_targ == v_lb[0])
                    ++( *(p_mk_0.sdata) ) ;
                else if (*p_targ == v_lb[1])
                    ++( *(p_mk_0.sdata+1) ) ;
            }
            
        }
        p_mk_0/=N0;
        p_mk_1/=N1;

        if (imp_criteria_ == GINI_INDEX )
        {
           
  
            // cerr<<"N9N1 "<<N0<<" "<<N1<<endl;
            
            //  smatrix_print(p_mk_0, "p_mk_0");
            //smatrix_print(p_mk_1, "p_mk_1");
            imp0 = gini_index(p_mk_0);
            imp1 = gini_index(p_mk_1);

        }else if (imp_criteria_ == DEVIANCE )
        {
            imp0 = cross_entropy(p_mk_0);
            imp1 = cross_entropy(p_mk_1);
        }
        //cerr<<"gini index "<<gini_0<<" "<<gini_1<<endl;
        return N0*imp0+N1*imp1;

    }
    
    
    void DecisionTreeLeaf::est_majority_lb(){
        const float * t_ptr =m_target_.sdata;
        unsigned int n_samples = m_target_.MRows;
        unsigned int N0(0), N1(0);
        for (unsigned int i = 0 ; i < n_samples; ++i,++t_ptr)
        {
            if ( *t_ptr == v_lb_[0] )
                ++N0;
            else if ( *t_ptr == v_lb_[1] )
                ++N1;
            else
                throw ClassificationTreeException("DecisionTreeLeaf::est_majority_lb() : Invalid class label found.");
        }
        majority_lb_ = (N0 > N1) ? v_lb_[0] : v_lb_[1];
      //  cerr<<"majoritry label calc "<<N0<<" "<<N1<<" "<<v_lb_[0] <<" "<< v_lb_[1]<<" "<<majority_lb_<<endl;

    }
    

    
    int DecisionTreeLeaf::est_binary_split( )//const smatrix & m_col, const smatrix & targ, const vector<int> & v_lb )
    {
        est_majority_lb();
        //these keep track of the variable specfic threshholds and cost minimum
        float f_thresh_min=0;
        float f_cost_min=0;
        unsigned int i_feat_min=0;

        //need to find best variable, given best split for each variable.
        unsigned int Nfeatures = m_data_.NCols;
        unsigned int Nsamples = m_data_.MRows;
        for (unsigned int feature =0 ; feature < Nfeatures; ++ feature)
        {
//          cerr<<"est_bin_split feature "<<feature<<" "<<Nsamples<<" "<<Nfeatures<<endl;
            //copy data into list to sort
            list<float> l_feat(Nsamples);
            float min = m_data_.value(0,feature);
            float max = m_data_.value(0,feature);
            
            {//load column into STL list to sort
                float* f_ptr=m_data_.sdata + feature*Nsamples; //start at appropriate column
                for (list<float>::iterator i_d = l_feat.begin(); i_d != l_feat.end();++i_d,++f_ptr)//now lets copy
                {
                    if ( *f_ptr < min ) min = *f_ptr;
                    if ( *f_ptr > max ) max = *f_ptr;
                    *i_d = *f_ptr;
                }
            }
            
            l_feat.sort(); //sorting the list to use incremental thresholds
      //      for (list<float>::iterator i_d = l_feat.begin(); i_d != l_feat.end();++i_d)//now lets copy
        //        cerr<<*i_d<<" ";
          //  cerr<<endl;
            //threshold will be set as mean between 2 adjacent ordered points
            list<float>::iterator i_f = l_feat.begin();
            list<float>::iterator i_f2 = l_feat.begin();
            ++i_f2;
         //   unsigned int i_feat=0;
            float thresh_min=0;// 0.5*(*i_f + *i_f2);
            float cost_min=0;
            bool first_entry=true;
            float val_dif_prev=0;
            for ( ;i_f2 != l_feat.end();++i_f,++i_f2)
            {
                
                
                float thresh= 0.5*(*i_f + *i_f2);
                
                if ((thresh > min) && (thresh < max)) //inclusion of max/min would
                {
                    float cost  = split_cost(m_data_.sdata + feature*Nsamples, m_target_.sdata, v_lb_,Nsamples,thresh);
                   //cerr<<"cost "<<thresh<<" "<<cost<<endl;
                    if ( ( first_entry ) ||  (cost < cost_min) || ((cost == cost_min) && (val_dif_prev< fabs(*i_f - *i_f2))) )                    //going to chose threshold that correspond to maximum difference between adjacent points , e.g. prefer 3.7(1.3->6.1), vs 1.3 (1.3->1.3)
                    {
                        thresh_min=thresh;
                        cost_min=cost;
                        first_entry=false;
                        val_dif_prev= fabs(*i_f - *i_f2);
                    }
                }
            }
//            cerr<<"feature "<<feature<<" "<<thresh_min<<" "<<cost_min<<" "<<endl;
            if ( (feature==0) || ( cost_min < f_cost_min) )
            {
                i_feat_min = feature;
                f_thresh_min = thresh_min;
                f_cost_min = cost_min;
            }
            
        }

//cout<<"done loop"<<endl;
//        cerr<<"min feature "<<i_feat_min<<" "<<f_thresh_min<<" "<<f_cost_min<<" "<<endl;
        split_feature_ = i_feat_min;
        split_threshold_ = f_thresh_min;
        split_cost_ = f_cost_min;
//        cerr<<"done est split "<<endl;
        return 1;//sucessful split
        //        return f_cost_min;
    }
    

    
    
    
    
    
    
    //---------------------------------END OF LEAF---------------------------//
DecisionTree::DecisionTree()
{
    cerr<<"DecisionTree::DecisionTree()";
    NodeName="DecisionTree";
    root_ = new DecisionTreeLeaf();
    imp_criteria_ = GINI_INDEX;
    cost_comp_alpha_=0;
    alpha_est_=true;
    nodeTypeSpec=DECISION_TREE;


}
    DecisionTree::DecisionTree( DecisionTree* tree ){
      
        //this should recursively create all leaves

        root_ = new DecisionTreeLeaf(tree->root_, nodes_ );

        nodes_.pop_front();//remove root from this
         updateLeaves();

			Dimensionality_ = tree->Dimensionality_;
        cost_comp_alpha_ = tree->cost_comp_alpha_;
        alpha_est_=tree->alpha_est_;

    }

    
    
DecisionTree::~DecisionTree(){
    //delete nimloth_;
    delete root_;
    
    //cerr<<"root deleted2 " <<nodes_.size()<<endl;
    
    // display()  ;
    for (list<DecisionTreeLeaf*>::iterator i_l = nodes_.begin(); i_l != nodes_.end();++i_l)
    {
        //   cerr<<"delete node X / "<<nodes_.size()<<endl;
       delete *i_l;
    }
    //cerr<<"done deleting leaves"<<endl;
    
}
    
    
    void DecisionTree::writeTree(const string & filename){
        
        ofstream ftree(filename.c_str());
        
        if (ftree.is_open()  )
        {

            unsigned int index=0;
            //do root now.
            {
                int index2=1; //need to start at 1 because of addition of root
                int child0_index=-1;
                int child1_index=-1;
                for (list<DecisionTreeLeaf*>::iterator i_l2 = nodes_.begin(); i_l2 != nodes_.end();++i_l2,++index2)
                {
                    if ( (*i_l2) == root_->getChild(0) ) child0_index=index2;
                    if ( (*i_l2) == root_->getChild(1) ) child1_index=index2;
                }
            ftree<<index<<" "<<0<<" "<<root_->getSplitFeatureName()<<" "<<root_->getSplitThreshold()<<" "<<child0_index<<" "<<child1_index<<" "<<-1<<endl;

            }
            ++index;
            
            for (list<DecisionTreeLeaf*>::iterator i_l = nodes_.begin(); i_l != nodes_.end();++i_l,++index)
            {
                int parent=-1;
                //search for childrean
                int index2=1;
                int child0_index=-1;
                int child1_index=-1;
                if (root_->isChild(*i_l)>=0) parent=0;
                for (list<DecisionTreeLeaf*>::iterator i_l2 = nodes_.begin(); i_l2 != nodes_.end();++i_l2,++index2)
                {
                            if ( (*i_l2)->isChild(*i_l) >=0 ) parent=index2;
                    if ( (*i_l2) == (*i_l)->getChild(0) ) child0_index=index2;
                    if ( (*i_l2) == (*i_l)->getChild(1) ) child1_index=index2;
                    
                }
                
                ftree<<index<<" "<<(*i_l)->getSplitLevel()<<" "<<(*i_l)->getSplitFeatureName()<<" "<<(*i_l)->getSplitThreshold()<<" "<<child0_index<<" "<<child1_index<<" "<<parent<<endl;
                           }
            ftree.close();
        }
    }
    
    
    
    void DecisionTree::copy( DecisionTree* tree )
    {
        delete root_;
        for (list<DecisionTreeLeaf*>::iterator i_l = nodes_.begin(); i_l != nodes_.end();++i_l)
            delete *i_l;
//        cerr<<"clearv"<<endl;
        nodes_.clear();
        nodes_terminal_.clear();
        
//        cerr<<"new root"<<endl;

        root_ = new DecisionTreeLeaf(tree->root_, nodes_ );
//        cerr<<"done leaves root"<<endl;

        nodes_.pop_front();//remove root from this
        
        updateLeaves();
        
    }


    void DecisionTree::passDownTrainingData()
    {
        if (Verbose)
            cout<<"Decision Trees : Passing down training data..."<<endl;
        
        if (in_data.empty())
            throw ClassificationTreeNodeException("Trying to classify training using Decision Trees , but no data to classify exists.");
        
        smatrix m_in_data(NumberOfSamples_,Dimensionality_);
        copyToSingleVectorTranspose(in_data, m_in_data.sdata, NumberOfSamples_);
        
        DecisionTreeLeaf* current_leaf;
        for ( unsigned int sample = 0 ; sample < NumberOfSamples_; ++sample)
        {
            current_leaf = root_;
            //  cerr<<"Classify suject "<<sample<<endl;
            while (!current_leaf->isTerminal())
            {
                //  cerr<<"new node"<<endl;
                unsigned int feat_ind = current_leaf->getSplitFeature();
                float feat_thresh = current_leaf->getSplitThreshold();
                //    cerr<<"make decision "<<feat_ind<<" "<<feat_thresh<<" < "<<m_in_data.value(sample, feat_ind) <<endl;
                // current_leaf = current_leaf->getChild(m_in_datasample);
                current_leaf = (m_in_data.value(sample, current_leaf->getFeatureIndex(feat_ind)) > feat_thresh) ? current_leaf->getChild(1) : current_leaf->getChild(0) ;
            }
            //cerr<<"CLassify "<<sample<<" "<<current_leaf->getMajorityClass()<<endl;
            out_data->push_back(current_leaf->getMajorityClass() );
            
        }
        // print(*out_data,"training classification");
        

        
        
    }

    

    void DecisionTree::setOptions( std::stringstream & ss_options){
        
		map<string,string> options = parse_sstream(ss_options);
		for ( map<string,string>::iterator it = options.begin(); it!=options.end();it++)
		{
			cout<<"set DecisionTree option "<<it->first<<" "<<it->second<<endl;
			
			if ( it->first == "SplitCriteria")
			{
				if (it->second == "Mean")
                    imp_criteria_=MEAN;
                else if (it->second == "Median")
                    imp_criteria_=MEDIAN;
                else if (it->second == "GINI_index")
                    imp_criteria_=GINI_INDEX;
            }else if ( it->first == "MaxLevel")
			{
                root_->setSplitLevelMax( string2num<unsigned int>(it->second));
            }else {
				if (!setTreeNodeOptions(it->first, it->second))
					throw ClassificationTreeNodeException((NodeName + " : Invalid input argument : " + it->first).c_str());
			}
		}

    }
    void DecisionTree::trim( const unsigned int & index ) {
        list< DecisionTreeLeaf* >::iterator i = nodes_.begin();
        advance(i, index);
        (*i)->trim();
        updateLeaves();
        
    }
    float  DecisionTree::cost_complexity()
    {
     //   unsigned int absT = nodes_terminal_.size();
        // unsigned int Nm = nodes_terminal_.size();
//        cerr<<"cost_complexity: "<< nodes_terminal_.size()<<endl;
        
        float cost=0.0f;
        unsigned int i = 0 ; 
        for (list<DecisionTreeLeaf*>::iterator i_tm = nodes_terminal_.begin(); i_tm != nodes_terminal_.end(); ++i_tm,++i)
        {
            cost+=(*i_tm)->getNumberOfSamples()*(*i_tm)->impurity();
        }
        
//       cerr<<"cost total "<<cost<<endl;
        return cost;
    }
    
    void DecisionTree::updateLeaves()
    {
//              cerr<<" DecisionTree::updateLeaves "<<nodes_terminal_.size()<<endl;
        nodes_.clear();
        //cerr<<"get leaves "<<endl;
        root_->getLeaves(nodes_);
        //cerr<<"done get leaves"<<endl;
        nodes_.pop_front();
        //cerr<<"moving on 1 "<<endl;
        nodes_terminal_.clear();
        
               if (root_->isTerminal())
                    nodes_terminal_.push_back(root_);

        
        for (list<DecisionTreeLeaf*>::iterator ii_l = nodes_.begin(); ii_l != nodes_.end(); ++ii_l )
            if ((*ii_l)->isTerminal())
                nodes_terminal_.push_back(*ii_l);
//        cerr<<"end  DecisionTree::updateLeaves "<<nodes_terminal_.size()<<endl;

    }
    
    void DecisionTree::prune( const float & alpha  )
    {
//        cerr<<"DecisionTree::prune2"<<endl;
//        cerr<<"Orig tree "<<endl;
// display();
        //doing weakest link prunning
        //first lets copy the tree
        float cost_comp_full= cost_complexity();
        unsigned int Tfull =  nodes_terminal_.size();
        
        vector<bool> leavesVisited(nodes_.size(),0);
        
        vector<bool>::iterator i_visit = leavesVisited.begin();
        
        // unsigned int Nnonterm_nodes = nodes_.size() - nodes_terminal_.size();
        DecisionTree* prunedTree2 = new DecisionTree(this);
        list< pair<float, DecisionTree*> > v_trees;
        list< pair<float, float> > v_alpha;
        v_trees.push_back( pair<float, DecisionTree*>(prunedTree2->cost_complexity()   + nodes_terminal_.size()*alpha ,new DecisionTree(prunedTree2)) );

        
        
        //unsigned int Nsplits=0;
//        cerr<<"Nsplits "<<Nnonterm_nodes<<" "<<Nnonterm_nodes<<endl;
        //   while (Nsplits != Nnonterm_nodes)
        unsigned int Nsplits=0;

        do {
//           cerr<<"Construct tree"<<endl;
//            cerr<<"done construct tree"<<endl;
//            cerr<<"Try triming at each non-terminal node"<<endl;
            unsigned int index=0;
            unsigned int index_min=0;
            float cost_min=0;
            float alpha_min=0;
             Nsplits=0;

            for (list<DecisionTreeLeaf*>::iterator ii_l = prunedTree2->nodes_.begin(); ii_l != prunedTree2->nodes_.end(); ++ii_l, ++i_visit,++index)
            {
//                cerr<<"-------------------------"<<endl;
//                cerr<<"Delete next node"<<endl;
//                cerr<<"construct tree"<<endl;
//                cerr<<"done construct tree"<<endl;
                
                cerr<<"do trim "<<(*ii_l)->getSplitThreshold() <<" -level- "<<(*ii_l)->getSplitLevel() <<endl;
                if ((!(*ii_l)->isTerminal()) && (! *i_visit))
                {
                    DecisionTree* prunedTree = new DecisionTree(prunedTree2);

                    *i_visit=1;
                    cerr<<"DO TRIM"<<endl;
//                    (*ii_l)->trim();
                    prunedTree->trim(index);
//                    cerr<<"DONE TRIM "<<endl;
//                    cerr<<"Update leaves after trim"<<endl;
                    prunedTree->updateLeaves();
//                    cerr<<"DONE UPDATE LEAVES"<<endl;
//                    cerr<<"number of leaves "<< prunedTree->nodes_.size()<<" "<< prunedTree->nodes_terminal_.size()<<endl;
                    //
                    
                    // prunedTree->display();
                    
                    //                for ( list<DecisionTreeLeaf*>::iterator i_l = prunedTree->nodes_.begin(); i_l != prunedTree->nodes_.end(); ++i_l)
                    //                {
                    //                    cerr<<"seraching leaves..."<<endl;
                    //                    if (*i_l == NULL)
                    //                    {
                    //                        cerr<<"LEAF = NULL "<<endl;
                    //                    }
                    //                }
//                    cerr<<"do cost trim start"<<endl;
                    unsigned int Tpruned = prunedTree->nodes_terminal_.size();//number of terminal nodes
                    float cost_trim = prunedTree->cost_complexity();
                                       cerr<<"Cost trim " <<cost_trim<<endl;
                    if ( (cost_trim < cost_min) || (Nsplits==0) )
                    {
                        cost_min = cost_trim;
                        index_min = index;
                        alpha_min = (cost_comp_full - cost_trim)/(Tfull-Tpruned);
                    }
                  //  cerr<<"break"<<endl;
                    //break;
                    delete prunedTree;
                    ++Nsplits;

                }
                //break;
               

            }
            cerr<<"trimTree "<<Nsplits<<" / "<<index_min<<" "<<alpha_min<<" "<<endl;
            prunedTree2->trim(index_min);

            cost_comp_full = cost_min;// prunedTree2->cost_complexity();
            
            if (Nsplits>0)
            {
                v_alpha.push_back( pair<float,float>(cost_min, alpha_min));

                        v_trees.push_back( pair<float, DecisionTree*>(cost_min ,new DecisionTree(prunedTree2)) );

            }
//            v_trees.push_back( pair<float, DecisionTree*>(cost_min+ prunedTree2->nodes_terminal_.size()*alpha ,new DecisionTree(prunedTree2)) );
        }while (Nsplits>0);
        v_alpha.sort();
        v_trees.sort();
//        cerr<<"done triming "<<endl;
//        v_trees.front().second->display();
//        for (list< pair<float, DecisionTree*> >::iterator i = v_trees.begin(); i != v_trees.end();++i)
//        {
////             cerr<<"tree cost "<<i->first<<endl;
//            i->second->display();
//        }
        cerr<<"pruning alphas "<<endl;
                for (list< pair<float, float> >::iterator i = v_alpha.begin(); i != v_alpha.end();++i)
               {
                     cerr<<"tree cost /alpha "<<i->first<<" "<<i->second<<endl;
                   // i->second->display();
                }

        
        
//        cerr<<"Copy TREE"<<endl;
        this->copy(v_trees.front().second);

        delete prunedTree2;
//             cerr<<"deleted pruned tree"<<endl;
        for (list< pair<float, DecisionTree*> >::iterator i = v_trees.begin(); i != v_trees.end();++i)
        {
            // cerr<<"tree cost "<<i->first<<endl;
            //  i->second->display();
            delete i->second;
        }
 
        
        cerr<<"done DecisionTree::prune2"<<endl;


    }
    void DecisionTree::estimateParameters(){
        if (out_data_names_ != NULL)
            delete out_data_names_;
        out_data_names_ = new vector<string>();

        cerr<<"DecisionTree::estimateParameters "<<in_data_names_.size()<<" "<<in_data_names_[0]->size()<<endl;
        copyToSingleVector(in_data_names_, out_data_names_,1);
        //print(out_data_names_,"features_names");
        cerr<<"feature_names"<<endl;
        for (vector<string>::iterator i = out_data_names_->begin(); i != out_data_names_->end(); ++i)
            cerr<<*i<<" ";
        cerr<<endl;
        
        smatrix m_in_data(NumberOfSamples_,Dimensionality_);
        smatrix m_target;
        training_target->getTargetData(m_target);
        copyToSingleVectorTranspose(in_data, m_in_data.sdata, NumberOfSamples_);
        cerr<<"estimateParametersGrow"<<endl;
        estimateParametersGrow(m_in_data,m_target);
        writeTree("treeGrown.txt");

        cerr<<"estimateParametersPrune"<<endl;

        estimateParametersPrune(m_in_data,m_target,alpha_est_);
        writeTree("treeGrown-pruned.txt");

        
    }
    void DecisionTree::estimateParametersGrow( const clapack_utils_name::smatrix & m_in_data, const clapack_utils_name::smatrix & m_target){
               cerr<<"DecisionTree::estimateParameters "<<out_data_names_->size()<<endl;
        
        //clear tree data
        if (! nodes_.empty()){
            for (list< DecisionTreeLeaf* >::iterator i_l = nodes_.begin(); i_l != nodes_.end(); ++i_l)
                delete *i_l;
            nodes_.clear();
        }
        
        //       cerr<<"DecisionTree::estimateParameters "<<NumberOfSamples<<" "<<Dimensionality_<<endl;
          //  smatrix_print(m_in_data,"m_in_data" );
        //   smatrix_print(m_target, "m_target");
        
        
        vector<int> v_lb =  training_target->getClassLabels();
        unsigned int Ncl = training_target->getNumberOfClasses();
        
        if (Ncl != 2)
            throw ClassificationTreeNodeException(("DecisionTree must have only 2 classes. " + num2str<int>(Ncl) + " classes were found").c_str() );
        
        //        cerr<<"DecisionTree::estimateParameters::set root data"<<endl;
        root_->setData(m_in_data);
        root_->setFeatureNames(*out_data_names_);
        root_->setTarget(m_target);
        root_->setClassLabels(v_lb);
        
        //this portion grows the trees the tree
        root_->est_binary_split();
//        cout<<"rub split"<<endl;
        root_->split(nodes_,0); //root is level 0
//        cerr<<"update leave preprune"<<endl;
        //this also ensure a level of consistency when creating deep copies
        updateLeaves();
        
        
        //RIGHT HERE IS WHERE WE WANT TO EMBED CROSS-VALIDATION to estimate alpha for the pruning
        cerr<<" Done Growing Tree "<<endl;

    }
    
    float DecisionTree::g_subtree( DecisionTreeLeaf* subroot ){
        //this is difference between R(t) at node vs R(t) of subrooted tree.
        //current RT
//        cout<<"---------calculate impurity diff for "<<subroot->getSplitFeatureName()<<endl;
        float RT_t = subroot->getNumberOfSamples() * subroot->impurity();
//        float RT_t =  subroot->impurity();

//        cout<<"subroot "<<subroot->impurity()<<" "<<subroot->getNumberOfSamples()<<endl;
        //lets find the leaves
        float RT_Tt = 0.0f;
        int size_t = 1;
        int size_Tt = 0;
        list< DecisionTreeLeaf* > nodes_to_go;
        nodes_to_go.push_back(subroot->getChild(0));
        nodes_to_go.push_back(subroot->getChild(1));
        while (! nodes_to_go.empty()) {
            DecisionTreeLeaf * leaf = nodes_to_go.front();
            nodes_to_go.pop_front();
            if (leaf->isTerminal())
            {
                RT_Tt +=leaf->getNumberOfSamples() * leaf->impurity();
//                RT_Tt += leaf->impurity();

//                cout<<"g_subtree - found temrinal node "<<leaf->getSplitFeatureName()<<" "<<leaf->impurity()<<" "<<RT_Tt<<" "<<leaf->getNumberOfSamples()<<endl;
                ++size_Tt;
//                cout<<"alpha "<<leaf->getNumberOfSamples() * leaf->impurity()<<endl;
            }else{
                nodes_to_go.push_back(leaf->getChild(0));
                nodes_to_go.push_back(leaf->getChild(1));
            }
        }
//        cout<<"gt "<<RT_t<<" "<<RT_Tt<<" "<<size_Tt<<" "<<size_t<<endl;
        return ((RT_t - RT_Tt)/(size_Tt -size_t));
        
    }
    void DecisionTree::estimateParametersPrune(  const clapack_utils_name::smatrix & m_in_data, const clapack_utils_name::smatrix & m_target, const bool & withAlphaEst ){
	//To processing flows, need for alpha estiattion
	// and the inner loop for which it is actually estimated.
        //first things, first creates ordered set of subtreess with various alphas
        vector<float> alpha_k;
        
         DecisionTree* prunedTree = new DecisionTree(this);
			if (!alpha_est_)
			{
				cout<<"Copy in test Data "<<endl;
				prunedTree->copyInTestData( in_test_data);
			        cout<<"done copy"<<endl;
			}else{//don't write inner trees
		        prunedTree->writeTree("treeGrown-copy.txt");
			}
//        for (int pind = 0 ; pind < 5 ; ++pind)

	//produces sequnce of tree correspinding to alpha thresholds. 
        int pind=0;
        while (prunedTree->nodes_.size()>1)
        {
            //        map< DecisionTreeLeaf*, bool> bottom_nonterminal;
            list< DecisionTreeLeaf* > bottom_nonterminal;
            //        DecisionTree* prunedTree2 = new DecisionTree(this);
            //        unordered_set bt
            //should need to clear but much safer coding
            prunedTree->clearVisited();
            list< pair<float,DecisionTreeLeaf* > > l_gt;
            for (list< DecisionTreeLeaf* >::iterator i_leaves = prunedTree->nodes_terminal_.begin(); i_leaves != prunedTree->nodes_terminal_.end();++i_leaves)
            {
//                cout<<"check out "<<(*i_leaves)->getSplitFeatureName()<<" "<<(*i_leaves)->getParent()->hasBeenVisited()<<endl;
                if ( (*i_leaves)->getParent()->hasBeenVisited() == 0 )
                {
//                    cout<<"addin parent "<<(*i_leaves)->getParent()->getSplitFeatureName()<<endl;                    
                    (*i_leaves)->getParent()->setVisited(1);
                    bottom_nonterminal.push_back((*i_leaves)->getParent());
                    l_gt.push_back(pair<float,DecisionTreeLeaf*>(g_subtree(bottom_nonterminal.back()),(*i_leaves)->getParent()));
                }
            }
//            cout<<"Size of bottom nonterminals "<<bottom_nonterminal.size()<<endl;
//            for (list< pair<float,DecisionTreeLeaf* > >::iterator i = l_gt.begin(); i != l_gt.end();++i)
//                cout<<"gt "<<i->first<<endl;
            
            l_gt.sort();
//            for (list< pair<float,DecisionTreeLeaf* > >::iterator i = l_gt.begin(); i != l_gt.end();++i)
//                cout<<"gtsort "<<i->first<<endl;
            alpha_k.push_back(l_gt.front().first);
            l_gt.front().second->trim();
            prunedTree->updateLeaves();
				if (!alpha_est_)
				{//calculate generalization accuracy for sequence if using to estimate alpha
					// this is slightly more complicated than standard because of sequence
//					cout<<"Copy in test Data "<<endl;
//					prunedTree->copyInTestData( in_test_data);
//				        cout<<"done copy"<<endl;
					cout<<"Classify test data"<<endl;
//					vector<int3> ests = prunedTree->classify();
						
					prunedTree->classifyTestDataInner();
					
					cout<<"inner tree classify "<<endl;
				}else{
            		//only write when outer loop 
					prunedTree->writeTree("treeGrown-p"+num2str(pind)+".txt");
				}
            ++pind;
        }
        print(alpha_k,"alphas");
        //for each alpha run cross-validation
        cout<<"Do I set up inner loop for alpha estimation? "<<alpha_est_<<endl;

		if (alpha_est_)//do cross_validation to estimate alpha
		{
				cout<<"set up inner loop for alpha estimation"<<endl;
				//        smatrix m_target;
				// training_target->getTargetData(m_target);
				 Target* target_inner = new Target();
				target_inner->setTargetData(m_target);
				//smatrix_print(m_target,"m_target_inner");
		        //estimate alpha
		        TreeSampler* tree_samp = new TreeSampler();
		        
				ClassificationTree *TreeInner = new ClassificationTree();
				TreeInner->setTarget(target_inner);
				cout<<"Target "<<target_inner->getNumberOfSamples()<<endl;
		        DataSource* d_src_inner = new DataSource();
		//		smatrix_print(m_in_data,"m_in_data");
				d_src_inner->setInputData(m_in_data);    
				cout<<"number of test samples "<<NumberOfTestSamples_<<endl;
				//shoudl not use test data in inner loop
				cout<<"innerK "<<out_data_names_->size()<<endl;

					d_src_inner->setFeatureNames(*out_data_names_);
				DecisionTree* decTree = new DecisionTree();
				decTree->setAlphaEst(false);

		//		decTree->setFeatureNames(getFeatureNames());
				TreeInner->addRoot(decTree);
				TreeInner->attachToRoot(d_src_inner);
				TreeInner->setTarget(target_inner);
				
				 tree_samp->setTree(TreeInner);
				int K=5;
				vector<int> stratified_labels(2,0);
					stratified_labels[1]=1;
				   TreeSampleData tree_samp_data(K,stratified_labels,0.5);
				        tree_samp_data.model_thresh_mode=0;
			
				        tree_samp_data.stratified_labels=stratified_labels;
				        tree_samp_data.bootstrap=0;
				vector<int> v_mask(target_inner->getNumberOfSamples(),1);
				    tree_samp->run_Kfold( v_mask, 0,  tree_samp_data, "inner-dec-tree-out");
//				cout<<"Accuracy "<<TreeInner->getAccuracy
cout<<"tsamp_data "<<tree_samp_data.estimates_all.size()<<endl;
				cout<<"Generalization Data "<<tree_samp_data.genData_.size()<<endl;
		for (vector<TpFpTnFn>::iterator i_cv = tree_samp_data.genData_.begin(); i_cv != tree_samp_data.genData_.end(); ++i_cv)
			cout<<i_cv->acc()<<" "<<i_cv->N()<<" "<<i_cv->tp<<endl;
			

			
	
	}	else{
		cout<<"classification estimates "<<endl;
		prunedTree->displayOutDataInner();
		//estimate accuracy for each alpha, piecewise linear function of alpha
		
		
	}
		        delete prunedTree;


    }

	
	void DecisionTree::displayOutDataInner()
	{
		cout<<"DecisionTree::displayOutDataInner "<<inner_out_test_data_.size()<<endl;
		int index=0;
		for ( vector< vector<float> >::iterator i = inner_out_test_data_.begin(); i != inner_out_test_data_.end(); ++i,++index ) {
			cout<<"index "<<index<<" : "<<endl;
			for (vector<float>::iterator ii = i->begin(); ii != i->end(); ++ii)
				cout<<" "<<*ii;
			cout<<endl;	
		}
		
	}



    void DecisionTree::setParameters(){
        
    }
    void DecisionTree::display(){
        cerr<<"-----------------Tree-----------------"<<endl;
        cerr<<"       feat_"<<root_->getSplitFeature()<<" > "<<root_->getSplitThreshold()<<" :: "<<root_->getSplitLevel()<<" :: "<<root_->getMajorityClass()<<" :: "<<root_->isTerminal()<<endl;
        // cerr<<"           /     \                    "<<endl;
        
        
        for (unsigned int level =1; level <= root_->getSplitLevelMax();++level)
        {
            cerr<<"Level "<<level<<endl;
            for (list<DecisionTreeLeaf*>::iterator i_l = nodes_.begin() ; i_l != nodes_.end(); ++i_l)
            {
                //cerr<<"level "<<(*i_l)->getSplitLevel()<<endl;
                if ( (*i_l)->getSplitLevel() == level)
                    cerr<<"feat_"<<(*i_l)->getSplitFeature()<<" > "<<(*i_l)->getSplitThreshold()<<" :: "<<(*i_l)->getSplitLevel()<<" :: "<<(*i_l)->getMajorityClass()<<" :: "<<(*i_l)->isTerminal()<<endl;
                // (*i_l)->printFeatureIndices("feature indices");
            }
        }
        cerr<<"--------------------------------------"<<endl;
        //  cerr<<"done display"<<endl;
    }


    int DecisionTree::classifyTrainingData(){
           if (Verbose)
        cout<<"Decision Trees : Classifying training data..."<<endl;
        cout<<"Decision Trees : Classifying training data..."<<endl;

        if (in_data.empty())
            throw ClassificationTreeNodeException("Trying to classify training using LDA , but no data to classify exists.");
                
        smatrix m_in_data(NumberOfSamples_,Dimensionality_);
        copyToSingleVectorTranspose(in_data, m_in_data.sdata, NumberOfSamples_);
        
        DecisionTreeLeaf* current_leaf;
        for ( unsigned int sample = 0 ; sample < NumberOfSamples_; ++sample)
        {
            current_leaf = root_;
          //  cerr<<"Classify suject "<<sample<<endl;
            while (!current_leaf->isTerminal())
            {
              //  cerr<<"new node"<<endl;
                unsigned int feat_ind = current_leaf->getSplitFeature();
                float feat_thresh = current_leaf->getSplitThreshold();
            //    cerr<<"make decision "<<feat_ind<<" "<<feat_thresh<<" < "<<m_in_data.value(sample, feat_ind) <<endl;
               // current_leaf = current_leaf->getChild(m_in_datasample);
                current_leaf = (m_in_data.value(sample, current_leaf->getFeatureIndex(feat_ind)) > feat_thresh) ? current_leaf->getChild(1) : current_leaf->getChild(0) ;
            }
            //cerr<<"CLassify "<<sample<<" "<<current_leaf->getMajorityClass()<<endl;
            out_data->push_back(current_leaf->getMajorityClass() );

        }
       // print(*out_data,"training classification");

        
        
        return 0;
    }
    void DecisionTree::clearVisited()
    {
        root_->setVisited(0);
        for ( std::list< DecisionTreeLeaf* >::iterator i_n = nodes_.begin(); i_n != nodes_.end(); ++i_n)
            (*i_n)->setVisited(0);

    }
        int DecisionTree::classifyTestDataInner()
{
	this->classifyTestData();
	cout<<"push_back classifyTestDataInner "<<inner_out_test_data_.size()<<endl;
	
	
	
	inner_out_test_data_.push_back(*out_test_data);
}

    int DecisionTree::classifyTestData() {
			if (out_test_data == NULL)
				out_test_data = new vector< float > ();
				else 
				out_test_data->clear();

        if (Verbose)
            cout<<"Decision Trees : Classifying test data..."<<endl;
        cout<<"Decision Trees : Classifying test data..."<<endl;

        if (in_test_data.empty())
{
	cerr<<"Trying to classify using Decision Trees  , but no data to classify exists."<<endl;
            throw ClassificationTreeNodeException("Trying to classify using Decision Trees  , but no data to classify exists.");
}

        smatrix m_in_data(NumberOfTestSamples_,Dimensionality_);
//cout<<"NumberOfTestSamples_ "<<NumberOfTestSamples_<<" "<<Dimensionality_<<endl;
        copyToSingleVectorTranspose(in_test_data, m_in_data.sdata, NumberOfTestSamples_);
//        cout<<"done copying data"<<endl;
        DecisionTreeLeaf* current_leaf;
        for ( unsigned int sample = 0 ; sample < NumberOfTestSamples_; ++sample)
        {            
            current_leaf = root_;
//             cerr<<"Classify suject "<<sample<<" / "<<NumberOfTestSamples_<<endl;
            while (!current_leaf->isTerminal())
            {
					
                unsigned int feat_ind = current_leaf->getSplitFeature();
                float feat_thresh = current_leaf->getSplitThreshold();

                current_leaf = (m_in_data.value(sample, current_leaf->getFeatureIndex(feat_ind)) > feat_thresh) ? current_leaf->getChild(1) : current_leaf->getChild(0) ;
            }

            out_test_data->push_back(current_leaf->getMajorityClass() );

        }
        
        print(*out_test_data,"test classification");
        
        
        return 0;
    }
    
    
}
