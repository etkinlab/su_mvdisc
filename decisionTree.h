//
//  decisionTree.h
//  su_mvdisc
//
//  Created by Brian Patenaude on 12/6/12.
//  Copyright (c) 2012 Stanford University - Etkin Lab. All rights reserved.
//

#ifndef __su_mvdisc__decisionTree__
#define __su_mvdisc__decisionTree__

#include <iostream>

//su_mvdisc nodes
//#include <ClassificationTreeNode.h>

#include <clapack_utils.h>
#include <smatrix.h>
#include <base_mvdisc.h>
//
#include <ClassificationTree.h>
#include <list>
#include <map>
namespace su_mvdisc_name{
    //DEVIANCE IS CROSS ENTROPY
    enum Impurity_CRIT { GINI_INDEX, DEVIANCE, MEAN, MEDIAN };

    
    class DecisionTreeLeaf {
        class DecisionTree;
    public:
        DecisionTreeLeaf();
        DecisionTreeLeaf( DecisionTreeLeaf* leaf );
        DecisionTreeLeaf( DecisionTreeLeaf* leaf, std::list<DecisionTreeLeaf*> & l_leaves );


        DecisionTreeLeaf(const unsigned int & NSamples, const unsigned int & NDimensions );
        void getLeaves( std::list<DecisionTreeLeaf*> & leaves );

        void trim();
        void collapse( bool state = true ) { collapsed_=true;}
        void uncollapse() { collapsed_=false;}

        ~DecisionTreeLeaf();
        float* getDataPointer() { return m_data_.sdata ; }
        float* getTargetPointer() { return m_target_.sdata ; }

        void setSize(const unsigned int & NSamples, const unsigned int & NDimensions );
        void setData(const clapack_utils_name::smatrix & m_data_in);
        void setTarget(const clapack_utils_name::smatrix & m_target_in);
        void setFeatureNames( const std::vector<std::string> & names);
        void  getDataAndTarget();
        void getDataAndTarget( DecisionTreeLeaf* leaf );
        
        void setClassLabels( const std::vector<int> & v_lb_in );

        unsigned int getNumberOfSamples() { return m_data_.MRows; }
        unsigned int getNumberOfFeatures() { return m_data_.NCols; }

        int est_binary_split( );//const clapack_utils_name::smatrix & m_col, const clapack_utils_name::smatrix & targ, const std::vector<int> & v_lb  );
        //void split( DecisionTreeLeaf* leaf0, DecisionTreeLeaf* leaf1 );
        int split( std::list<DecisionTreeLeaf*> & leaves , unsigned int split_level = 0  );

        int classifyTrainingData()  ;

        void setAndRemoveFeatureIndices(const std::list<unsigned int> & l_feat_inds , const unsigned int & split_feature );

        void setParent( DecisionTreeLeaf* parent) { parent_=parent; }
        void setIsTerminal( const bool & is_terminal_node ) { isTerminal_ = is_terminal_node; }
        
        void printData(const std::string & name ){ smatrix_print(m_data_, name); }
        void printTarget(const std::string & name ){ smatrix_print(m_target_, name); }
        void printFeatureIndices(const std::string & name );
        
        
        //access split info
        unsigned int getSplitFeature(){ return split_feature_; }
        std::string getSplitFeatureName(); 
        std::vector<std::string> getFeatureNames(){ return feature_names_;}
        
        float getSplitThreshold(){ return split_threshold_ ; }
        unsigned int getSplitLevel(){ return split_level_ ; }
        void setSplitLevel( const unsigned int & level ){ split_level_ = level ; }
        void setSplitLevelMax( const unsigned int & max_level ){ split_levels_max_ = max_level ; }

        unsigned int getSplitLevelMax(){ return split_levels_max_ ; }
        int getMajorityClass() { return majority_lb_;}
        unsigned int getFeatureIndex( const unsigned int & feat );
        bool isPure();
        bool isTerminal(){ return isTerminal_; }
        bool isCollapsed(){ return collapsed_; }

        int hasBeenVisited(){ return visited_; }
        void setVisited( const bool & visit ){ visited_ = visit ; }
        
        int isChild( DecisionTreeLeaf* leaf );

        DecisionTreeLeaf* getChild( const int & index );//index is child 0/1
        DecisionTreeLeaf* getParent( );

        float impurity( );

    protected:
        static float cross_entropy( const std::map<int,int> & m_p  );
        static float gini_index( const std::map<int,int> & m_p );
        static float cross_entropy( const clapack_utils_name::smatrix & m_p );
        static float gini_index( const clapack_utils_name::smatrix & m_p );
        float split_cost(const float *p_m_col, const float * p_targ, const std::vector<int> & v_lb, const unsigned int Nele, float thresh );
        void est_majority_lb();
        std::list<unsigned int> l_feat_inds_;


        //used internally for algorithms.
        int visited_;

        
    private:
        
        std::vector<std::string> feature_names_;
        unsigned int split_level_,split_levels_max_;
        
        unsigned int split_feature_;
        float split_threshold_, split_cost_;
        
        std::vector<int> v_lb_;//vector of class labels;
        int majority_lb_;
        clapack_utils_name::smatrix m_data_; //input data, columns are featrures, rows subjects
        clapack_utils_name::smatrix m_target_;//classifications for training data
//        std::vector< DecisionTreeLeaf* > children_;
        Impurity_CRIT imp_criteria_;

        DecisionTreeLeaf *child0_, *child1_, *parent_;
        bool isTerminal_;
        bool collapsed_;
        
    };
    
    class DecisionTree : public base_mvdisc {
        
    public:
        
        //enum SPLIT_CRIT { GINI_INDEX, MEAN, MEDIAN };
        
        DecisionTree();
        DecisionTree( DecisionTree* tree );
        void writeTree(const std::string & filename);
        
        void copy( DecisionTree* tree );
        
        virtual ~DecisionTree();
        void updateLeaves();
        void trim( const unsigned int & index );
        void setOptions( std::stringstream & ss_options);
        void estimateParameters();
        void estimateParametersGrow( const clapack_utils_name::smatrix & m_in_data, const clapack_utils_name::smatrix & m_target );
        void estimateParametersPrune( const clapack_utils_name::smatrix & m_in_data, const clapack_utils_name::smatrix & m_target, const bool & withAlphaEst );
        void setAlphaEst( const bool & est ) { alpha_est_ = est; }
        void setAlpha( const float & alpha_in ) { cost_comp_alpha_ = alpha_in; }

        void prune( const float & alpha );
        void passDownTrainingData();
        void display();
        
        void setParameters();
        
        int classifyTrainingData()  ;
        int classifyTestData();
		void displayOutDataInner();
    protected:
        float  cost_complexity();
        float g_subtree( DecisionTreeLeaf* subroot );
        void clearVisited();

		//user for inner CV to estimate alphas
        int classifyTestDataInner();

    private:
        
        //ClassificationTree* nimloth_;
        
        Impurity_CRIT imp_criteria_;
        DecisionTreeLeaf* root_;
        std::list< DecisionTreeLeaf* >  nodes_;
        std::list< DecisionTreeLeaf* > nodes_terminal_;
        float cost_comp_alpha_;
        bool alpha_est_;
std::vector< std::vector< float>  > inner_out_test_data_;

//		std::vector< std::vector< pair<int,float> > > inner_out_test_data_;


    };
}


#endif /* defined(__su_mvdisc__decisionTree__) */
