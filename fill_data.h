#ifndef FillDataFilter_H
#define FillDataFilter_H
/*
 *  FillDataFilter.h
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 8/12/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include <BaseFilter.h>

#include <vector>
namespace su_mvdisc_name{

	enum FILL_MODE { FILL_ZERO , FILL_MEAN  };

	
class FillDataFilter : public base_filter
{
	public:
		FillDataFilter();
		virtual ~FillDataFilter();
		void setOptions( std::stringstream & ss_options);
	void applyToTestData();
	void estimateParameters();
	void applyParametersToTrainingData();
		void runSpecificFilter();
		void setFillMode( FILL_MODE mode );
	void setFillMode( const std::string & mode );

protected:
	
private:
	
	
	FILL_MODE fill_method;
    std::vector<float> fill_values_;
    
    
};

}
	
#endif
