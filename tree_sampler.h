//
//  tree_sampler.h
//  su_mvdisc
//
//  Created by Brian M. Patenaude on 4/27/12.
//  Copyright 2012 Stanford University - Etkin Lab. All rights reserved.
//

#include "ClassificationTree.h"
#include <string>
#include <vector_structs.h>
#include <list>
#include <treeSampleData.h>

namespace su_mvdisc_name{
    
    //This class is used to sample the processing tree. The sampler effectively attributes sample
    //to out_data and out_test_data in the data source. The sampling is internalling specified as
    //binary selection vectors. Each sampling technique requires the setting up of the appropriate
    //sequence of binary vectors
    class TreeSampler{
        
    public:
        
        TreeSampler(); //constructor
        ~TreeSampler(); //destructor
        
        //Load the configuration file specified by the filename
        void loadTree( const std::string & filename );
        
        //Classification tree is the processing pipeline. Sets the processing programmatically
        void setTree( ClassificationTree* tree );
        
        //Set the verbose levels for the tree. 0 is least verbose, v >2  is most verbose.
        void setVerbose( const int & v ) { Verbose = v ; }
        
        //Take vector of selection masks (binary vectors) and apply cross-valildation to create
        //a new set of masks. i.e. It performs (creates a vector of vinary vectors) cross-
        //validation for each set of selected sample specified in sel_masks. K-fold secifies the
        //K-fold cross-validation to use. Special case include:
        //Chunk specfies samples that should be tied together. e.g. in 4D timeseries.
        static std::vector< std::vector<int> > applyCrossValidation( const std::vector< std::vector<int> > & sel_masks, const unsigned int & Kfold ,const std::vector<unsigned int> & chunks);
        
        //Run k-fold cross-validation. do_bagging specifies whether to bag the results. For example
        //majority vote rule. If bagged, a single output will exist. If not, results for each CV step
        //will be reported. tsample_data stores the configuration parameters as well as results.
        int run_Kfold(  std::vector<int> & v_mask, const bool & do_bagging, TreeSampleData & tsamp_data, std::string output_name="");
        
        //
        int absoluteSubsample(unsigned int B, const bool & doBagging, const bool & weightBagByProb, const unsigned int & NperGroup, const std::vector<int> & v_mask, TreeSampleData & tsamp_data, std::string output_name="");
        
        //
        int absoluteSubsamplePreSelect(unsigned int B, const bool & doBagging, const bool & weightBagByProb, const unsigned int & NperGroup, const std::vector<int> & v_mask, TreeSampleData & tsamp_data, std::string output_name);
        
        //This sampling techniques whereby the samples are repeatedly sampled. The number of samples selected
        //from each group is equal. The number selected is equal to the minimun group size minus Noffset.
        //v_mask allows for selection of subset of the data
        int minGroupSubsampling( unsigned int B ,  const bool & doBagging, const bool  & weightBagByProb, const float & Noffset, const std::vector<int> & v_mask,  TreeSampleData & tsamp_data, std::string output_name="" );
        
        
        void setTrainingAccuracyThreshold(const float & val) { tr_acc_thresh_=val; }
        void setTrainingAccuracyNThreshold(const float & val) { n_tr_acc_thresh_=val; }
        
    protected:
   
        //This function computes statistics and outputs of interest/
        void postProcess( const std::vector<int> & v_mask, const bool & do_bagging, const bool & weightBagByProb, TreeSampleData & tsamp_data  , const std::string & output_name );
        
        
        
        
        
    private:
        //The classification tree to run each sampling through.
        //The tree is just the process pipeline
        ClassificationTree* gaia_;
        
        //Specifies whether to output information to standard out
        //Verbosity is also specified for each node
        int Verbose;
        
        //
        float tr_acc_thresh_,n_tr_acc_thresh_;
        
        
    };
    
}
