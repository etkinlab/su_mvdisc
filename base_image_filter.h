#ifndef BASEIMAGEFILER_H
#define BASEIMAGEFILER_H
/*
 *  base_image_filter.h
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 12/8/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


#include <BaseFilter.h>

#include <vector>
namespace su_mvdisc_name{
	
	class base_image_filter : public base_filter
	{
	public:
		base_image_filter();
		virtual ~base_image_filter();
			
	protected:
		
	private:
	
	};
	
}

#endif
