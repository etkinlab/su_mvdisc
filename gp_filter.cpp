//
//  gauss_proc.cpp
//  su_mvdisc
//
//  Created by Brian Patenaude on 7/6/11.
//  Copyright 2011 Stanford University - Etkin Lab. All rights reserved.
//

#include <map>
#include <string.h>
//su mvdisc 
#include "gp_filter.h"
#include <misc_utils.h>
#include <clapack_utils.h>
//OSX include
//OSX include                                                                                                                                         
#ifdef ARCHLINUX
extern "C" {
#include "cblas.h"
#include "f2c.h"
  typedef integer __CLPK_integer;
  typedef real __CLPK_real;
#include "clapack.h"
}

#else
#include <Accelerate/Accelerate.h>
#endif
#include <cmath>
//#include <vecLib/vblas.h>

//#include <vecLib/clapack.h>
//STIL includes
//#include <iostream>
using namespace std;
using namespace misc_utils_name;
using namespace clapack_utils_name;

#define GRAD_DIM 0
#define HYP_CONV_THRESH 0.001
#define CONJ_GRAD_BREAK_CRIT 0.001
namespace su_mvdisc_name {
    //these are not class funciton hence reuse of K 
    //not using at moment
    int lineSearch_GPLogLikelihood(void (*Kfunc)(const float *, float*,const std::vector<float> &,const unsigned int & , const unsigned int & ), const float* data,const float* y, std::vector<float> & hyperparams, const float* deriv, const unsigned int & NumberOfSamples, const unsigned int & Ndim, float step_size)
    {
        
        //copy hyperparameters to edit
        vector<float>  h_params=hyperparams;
        
        float prev_loglike=GPregression_logLikelihood(Kfunc,data,y,h_params, NumberOfSamples,Ndim);
       // cout<<"initial log likelihood "<<prev_loglike<<endl;
        float next_loglike=prev_loglike;
        int count=0;
         int max_iter=50;
        float minthresh=0.001;
     //   int min_iter=20;
        int index_max=0;
       float max_like=prev_loglike;
    //    float var_step=step_size;
   //     bool notfirst=false;
     //   while ((var_step>0.00001))//&&(index_max==0))
       // {
         //   if (notfirst)
           //     var_step/=2;
            //cout<<"NEWSTEPSIZE "<<var_step<<endl;
        while (   count<max_iter ){//at least move once
            int h_count=0;
            //update hyperparameters
          //  cout<<"hyp: "<<h_params[0]<<" "<<h_params[1]<<" "<<h_params[2]<<endl;
            
            bool BR=false;
            for (vector<float>::iterator i_h = h_params.begin(); i_h != h_params.end(); ++i_h,++h_count)
            {
                //if(h_count==GRAD_DIM)
           //     cout<<"hyp "<<(*i_h+step_size*deriv[h_count])<<endl;
                if ((*i_h+step_size*deriv[h_count])<=minthresh)
                    BR=true;
            }
            //break out of loop
           // cout<<"break "<<BR<<endl;
            if (BR) break;
            h_count=0;
            for (vector<float>::iterator i_h = h_params.begin(); i_h != h_params.end(); ++i_h,++h_count)
            {
               
            //    if(h_count==GRAD_DIM)
                    *i_h+=step_size*deriv[h_count];
            //    cout<<*i_h<<" ";
            }
            //cout<<endl;
            next_loglike = GPregression_logLikelihood(Kfunc,data,y,h_params, NumberOfSamples,Ndim);
         //  cout<<"next log like2 "<<count<<" "<<" "<<next_loglike<<endl;     
            ++count;

            if (next_loglike> max_like)
            {
                max_like=next_loglike;
                index_max=count;
                break;
            }
            
           
            
            
            if ( next_loglike > prev_loglike )
                break;
        }
     //       cout<<"varstep "<<var_step<<endl;
          //  var_step/=2;
   //         notfirst=true;
       // }
        //take back the last step
        int h_count=0;
        //update hyperparameters
        vector<float>::iterator i_h2 = h_params.begin();
        for (vector<float>::iterator i_h = hyperparams.begin(); i_h != hyperparams.end(); ++i_h,++i_h2,++h_count)
        {
          //  if(h_count==GRAD_DIM)
            cout<<"update hyperparams "<<h_count<<" "<<index_max<<" "<<step_size<<" "<<deriv[h_count]<<endl;
                *i_h += index_max*step_size*deriv[h_count];  
        }
        
        return index_max;
        
        
    }
    void dLikelihood_dHyp(void (*Kfunc)(const float *, float*,const std::vector<float> &,const unsigned int & , const unsigned int & ),const float * data , const float* y, std::vector<float> & hyperparams, const unsigned int & NumberOfSamples, const unsigned int & Ndim,float* grad){
        
        unsigned int Nhyper=hyperparams.size();
        
        //assume grad has been allocated properly
        //  if ( grad == NULL ) 
        //    delete[] grad;
        //   grad = new float[Nhyper];    
        
        float* K = new float[NumberOfSamples*NumberOfSamples];
        float *invKy = new float[NumberOfSamples];
        float *invKy_dTheta = new float[NumberOfSamples];
        //   float *dK_sc = new float[Nhyper];
        vector<float*> dK(Nhyper);
        for (vector<float*>::iterator i_dK = dK.begin(); i_dK != dK.end(); ++i_dK)
            *i_dK = new float[NumberOfSamples*NumberOfSamples];
        
        
        //calulate K
        Kfunc(data,K,hyperparams, NumberOfSamples,Ndim);
        
        //calculate K^-1
        __CLPK_integer info = run_choleskySymmetricInverse("L", NumberOfSamples, K, info);
        //inverse is Lower  traingle only !!!!
        //multiple matrix K^1 by vector y 
        cblas_ssymv( CblasRowMajor, CblasUpper, NumberOfSamples, 1.0f, K, NumberOfSamples, y, 1,0.0f, invKy, 1);
        
        //calculate elementwise dK/Dtheta for each theta
        squaredExponentialCovariance_dK(data,y,dK,hyperparams, NumberOfSamples,Ndim);
        //going to loop aorud and calculate derivative for each parameter
        float deriv_norm=0.0;//normalization constant
        int count = 0 ; 
        for (vector<float*>::iterator i_dK=dK.begin(); i_dK != dK.end(); ++i_dK,++count)
        {
            //multiply 1xN by NxN 
            //K^-1*y*dTheta
            cblas_sgemv( CblasRowMajor, CblasNoTrans, NumberOfSamples, NumberOfSamples, 1.0f, *i_dK, NumberOfSamples, invKy, 1,0, invKy_dTheta, 1);
            
            //multiply 1xN by Nx1 vector
            //store first term, then subtarct second from it
            grad[count]=cblas_sdot( NumberOfSamples,invKy_dTheta, 1, invKy, 1);
            
            //only calculates main diagoal using dot product
            float trace=0;
            unsigned int offset=0;
            unsigned int Nelements=NumberOfSamples;
            for (unsigned int i_row = 0 ; i_row < NumberOfSamples; ++i_row,offset+=NumberOfSamples,--Nelements)
            {
                //kinv is a diagonal matrix
                //use row
                trace+=cblas_sdot( Nelements,K+offset+i_row,1,(*i_dK)+i_row*(1+NumberOfSamples),NumberOfSamples )+cblas_sdot( i_row,K+i_row,NumberOfSamples,(*i_dK)+ i_row ,NumberOfSamples );                    
            }
            cout<<"Trace "<<trace<<" "<<grad[count]<<endl;
            grad[count]=0.5*(grad[count]-trace);
            //add square to calculate magnitude
            deriv_norm+=grad[count]*grad[count];
            
        }
        deriv_norm=sqrtf(deriv_norm);    
        //make unit vector
        for (unsigned int i =0 ; i < Nhyper;++i)
        {
            grad[i] = grad[i] / deriv_norm ;
          //  cout<<"grad "<<i<<" "<<grad[i]<<endl;
        }
        
        //  delete[] dK_sc;
        delete[] invKy;
        delete[] invKy_dTheta;
        //clean up deriavtive matrices
        for (vector<float*>::iterator i_dK = dK.begin(); i_dK != dK.end(); ++i_dK)
            delete[] *i_dK;
        delete[] K;
    }
    
    
    float GPregression_logLikelihood(void (*Kfunc)(const float *, float*,const std::vector<float> &,const unsigned int & , const unsigned int & ), const float* data,const float* y, std::vector<float> & hyperparams, const unsigned int & NumberOfSamples, const unsigned int & Ndim)
    {
       // cout<<"calculate K for y for marginal likelihood"<<endl;
        float* K = new float[NumberOfSamples*NumberOfSamples];
        Kfunc(data,K,hyperparams,NumberOfSamples, Ndim);
        float logDetL;
        
 //       cout<<"hyp "<<endl;
   //     for (vector<float>::iterator i_h = hyperparams.begin(); i_h != hyperparams.end();++i_h)
     //       cout<<*i_h<<" ";
     //   cout<<endl;

        __CLPK_integer info = run_choleskySymmetricInverse("L", NumberOfSamples, K,logDetL, info);
   //     cout<<"info "<<info<<endl;
        if (info>0)//positive definite
        {
            cout<<"not positive definite"<<endl;
            return -1.0e32;
        }   
        float* alpha = new float[NumberOfSamples];
        cblas_ssymm(CblasRowMajor, CblasLeft, CblasUpper, NumberOfSamples,1, 1.0f,K , NumberOfSamples, y,1, 0.0f, alpha, 1);
   //     cout<<"alpha"<<endl;
      //  for (int i = 0 ; i < NumberOfSamples   ;++i)
     //       cout<<alpha[i]<<" ";
     //   cout<<endl;
        
     //   cout<<"logDetl "<<logDetL<<endl;
        float likelihood = -0.5*( cblas_sdot(NumberOfSamples, alpha, 1, y, 1) + logDetL + NumberOfSamples*logf(2*PI));
        
        delete[] K;
        delete[] alpha;
        
        return likelihood;
    }
    void GPregression_conjugateGradientSearch( void (*Kfunc)(const float *, float*,const std::vector<float> &,const unsigned int & , const unsigned int & ),const float * data , const float* y, std::vector<float> & hyperparams, const unsigned int & NumberOfSamples, const unsigned int & Ndim  )
    {
        
        int break_count_max=20;
        cout<<"running conjugate gradient"<<endl;
        unsigned int Nhyp=hyperparams.size();
        float break_crit = CONJ_GRAD_BREAK_CRIT;// * CONJ_GRAD_BREAK_CRIT;
        
        float* grad = new float[Nhyp];        
        float *g= new float[Nhyp];
        float *h = new float[Nhyp];
        //calculate initial gradient
        dLikelihood_dHyp(Kfunc,data,y,hyperparams, NumberOfSamples,Ndim,grad);
        cout<<"done creating stuff "<<endl;
        vector<float> hyp_prev(Nhyp);
        //float loglike_prev=0;
        int break_count=0;
        for (unsigned int i = 0 ; i < Nhyp;++i)
        {
            //not doing neg grad because were maximizing prob
            g[i]=grad[i];
            h[i]=grad[i];//grad=h=g
        }
        
        
            float dgg=0.0;
            float gg=0.0;
            float gam;
        float loglike;//fp;
            
        //    float gamma=1;//step for gradient search
            
            
            cout<<"Starting Hyperparameters ";
            for (vector<float>::iterator i_h = hyperparams.begin(); i_h != hyperparams.end();++i_h)
                cout<<*i_h<<" ";
            cout<<endl;
            
            
            int iter_max=1000;
            int iter=0;
            while (iter <iter_max)
            {
                 cout<<"-----Iteration "<<iter<<endl; 
                // cblas_saxpy(Ncols, -1.0f, Data + r_loc , 1, tempK + r2*Ncols, 1);

         /*       cout<<"g "<<endl;
                for (unsigned int i = 0 ; i < Nhyp;++i){
                    cout<<g[i]<<" ";
                }
                cout<<endl;
                cout<<"h "<<endl;
                for (unsigned int i = 0 ; i < Nhyp;++i){
                    cout<<h[i]<<" ";
                }
                cout<<endl;
                cout<<"grad "<<endl;
                for (unsigned int i = 0 ; i < Nhyp;++i){
                    cout<<grad[i]<<" ";
                }
                cout<<endl;
 */
                float step=0.1;//max_grad_hyp;
                int sum_index=0;
                while (step > 0.0000001) {
             //       cout<<"step "<<step<<endl;
                    int index= lineSearch_GPLogLikelihood(Kfunc,data,y,hyperparams,grad, NumberOfSamples,Ndim,step);
               //     cout<<"min at index : "<<index<<endl;
                    sum_index+=index;
                   step/=10.0;
                }
              //  float break_crit=0.0f;
               // for (int i=0; i < Nhyp;++i)
               // {
                 //   break_crit+=
               // }
                //if (sum_index==0) break;
                //    cout<<"sumindex "<<sum_index<<endl;
                //evaluarte probability
                loglike=GPregression_logLikelihood(squaredExponentialCovariance_K,data,y,hyperparams, NumberOfSamples,Ndim);
             //   cout<<"log liklihood2 "<<iter<<" : "<<loglike<<" diff "<<(loglike-loglike_prev)<<endl;

           //  //   if ((fabs(loglike-loglike_prev)<break_crit)&&(iter!=0)) {
                  //  cout<<"breaK "<<fabs(loglike-loglike_prev)<<" "<<break_crit<<endl;
               //     break;
               // }
                //loglike_prev=loglike;
          //      float hyp_dist2=0;
                float max_disp=0;
                vector<float>::iterator i_h = hyperparams.begin();
                for (vector<float>::iterator i_hp = hyp_prev.begin(); i_hp != hyp_prev.end();++i_hp,++i_h)
                {
                    if ( fabs(*i_h - *i_hp) > max_disp ) max_disp = fabs(*i_h - *i_hp);
                   // hyp_dist2+=(*i_h - *i_hp) * (*i_h - *i_hp);
                   *i_hp=*i_h;
                }
                    
                //   cout<<"break? "<<max_disp<<endl;

                if ((max_disp < break_crit))
                {
                    ++break_count;
                }else{
                    break_count=0;
                }
                if (break_count> break_count_max)
                {
                   // cout<<"breakcount "<<break_count<<endl;

                    break;
                }
                    //leave condition 
               // cout<<"hyp_dist 2 "<<hyp_dist2<<" "<<break_crit<<endl;
               // if ( (hyp_dist2 < break_crit) && (iter!=0))
                 //   break;
                
                
                //calculate gradient 
                dLikelihood_dHyp(Kfunc,data,y,hyperparams, NumberOfSamples,Ndim,grad);
                dgg=gg=0.0f;

                for (unsigned int i = 0 ; i < Nhyp;++i)
                {
                    gg+=g[i]*g[i];
                    dgg+=(grad[i]-g[i])*grad[i];
                 //    dgg+=(grad[i])*grad[i];
                }
                if (gg==0.0)
                    break;
                gam=dgg/gg;
                if (gam < 0)gam=0;
                //            cout<<"dgg/gg/gamma "<<dgg<<" "<<gg<<" "<<gam<<endl;                 
                //cout<<"gradient ";
                for (unsigned int i = 0 ; i < Nhyp;++i)
                {
                    //set to new gradient, again no ngeative
                    g[i]=grad[i];
                    grad[i]=h[i]=g[i]+gam*h[i];
                    // cout<<grad[i]<<" ";
                }
                //cout<<endl;
                //let try a line search 
              
                
                
                //      int h_count=0;
                //    for (vector<float>::iterator i_hyp = hyperparams.begin(); i_hyp !=hyperparams.end(); ++i_hyp,++h_count) {
                //       cout<<"hcount "<<h_count<<" "<<dK_sc[h_count]<<endl;
                //only maximize first for now
                // if (h_count<=2)
                // {
                //         *i_hyp+=gamma*dK_sc[h_count];
                //   *i_hyp+=0;
                //}
                // }
                
                cout<<"Estimating Hyperparameters : iter "<<iter<<"  : "<<hyperparams[0]<<" "<<hyperparams[1]<<" "<<hyperparams[2]<<endl;
                // cout<<"log liklihood "<<iter<<" : "<<GPregression_logLikelihood(squaredExponentialCovariance_K,data,y,hyperparams, NumberOfSamples,Ndim)<<endl;
                
                //no movement
                ++iter;
                
            }
            
            delete grad;

        
        
    }
    

    void GPregression_gradientSearch( void (*Kfunc)(const float *, float*,const std::vector<float> &,const unsigned int & , const unsigned int & ),const float * data , const float* y, std::vector<float> & hyperparams, const unsigned int & NumberOfSamples, const unsigned int & Ndim  )
    {
        //want o maximize marginal likelihood
      //  float max_grad_hyp=1;

        // float gamma=1;//step for gradient search
        
        
        cout<<"Starting Hyperparameters ";
        for (vector<float>::iterator i_h = hyperparams.begin(); i_h != hyperparams.end();++i_h)
            cout<<*i_h<<" ";
        cout<<endl;

        unsigned int Nhyp=hyperparams.size();
 
        float* grad = new float[Nhyp];
        int iter_max=1000;
        int iter=0;
        while (iter <iter_max)
        {
          //  cout<<"-----Iteration "<<iter<<endl; 
           // cblas_saxpy(Ncols, -1.0f, Data + r_loc , 1, tempK + r2*Ncols, 1);
            
            cout<<"log liklihood "<<iter<<" : "<<GPregression_logLikelihood(squaredExponentialCovariance_K,data,y,hyperparams, NumberOfSamples,Ndim)<<endl;

            //calculate gradient 
            dLikelihood_dHyp(Kfunc,data,y,hyperparams, NumberOfSamples,Ndim,grad);
            
            cout<<"gradient ";
            for (unsigned int i = 0 ; i < Nhyp;++i)
            {
                cout<<grad[i]<<" ";
            }
            cout<<endl;
                 //let try a line search 
            float step=0.1;//max_grad_hyp;
            int sum_index=0;
            while (step > 0.0000001) {
                cout<<"step "<<step<<endl;
                int index= lineSearch_GPLogLikelihood(Kfunc,data,y,hyperparams,grad, NumberOfSamples,Ndim,step);
                cout<<"min at index : "<<index<<endl;
                sum_index+=index;
                step/=10.0;
            }
            if (sum_index==0) break;


      //      int h_count=0;
        //    for (vector<float>::iterator i_hyp = hyperparams.begin(); i_hyp !=hyperparams.end(); ++i_hyp,++h_count) {
         //       cout<<"hcount "<<h_count<<" "<<dK_sc[h_count]<<endl;
                //only maximize first for now
               // if (h_count<=2)
               // {
          //         *i_hyp+=gamma*dK_sc[h_count];
                 //   *i_hyp+=0;
                //}
           // }
            
            cout<<"Estimating Hyperparameters : iter "<<iter<<"  : "<<hyperparams[0]<<" "<<hyperparams[1]<<" "<<hyperparams[2]<<endl;
            cout<<"log liklihood "<<iter<<" : "<<GPregression_logLikelihood(squaredExponentialCovariance_K,data,y,hyperparams, NumberOfSamples,Ndim)<<endl;

            //no movement
            ++iter;
            
        }
       
        delete grad;
    }
    
    //----------------------------------------------------START OF KERNEL FUNCTIONS--------------------------//
    void squaredExponentialCovariance_dK( const float * Data, const float* y,  vector<float*> dK , const vector<float> & hyperparams, const unsigned int & Nrows, const unsigned int & Ncols )
    {
       // cout<<"calculating derivatives for squared exponential"<<endl;
        //Estomates K - ocvariance, this is only a function of the training data
        
        float lengScale=hyperparams[0];
        float var_f=hyperparams[1];
      //  float var_n=hyperparams[2];

        
        
        //assumes that K has Nrows
        //switch below
        //assumme 1 feature per column, rows are Dimensionality_
        //now let get to work
        //find covariance for evey columen (feature) with every other column
        float * tempK = new float[Nrows*Ncols];
        
        //ionly need to copy features after the base one
		size_t row_size = Ncols*sizeof(float);
        // K should be appropriately allocated
        //loop over dimensions
        for ( unsigned int r = 0 ; r < Nrows; ++r)
		{
			unsigned int r_loc = r*Ncols;
			//copy data over it will write over
          //  if ( (r+1) != Nrows )
            {//wouldn't go into the loop anyways but need to stop out of bvounds memcpy
              //  cout<<"copy "<<Nrows*Ncols<<" "<<r_loc<<" "<<(Nrows-r)<<" "<<row_size<<endl;
                memcpy(tempK + r_loc, Data + r_loc, (Nrows-r)*row_size);
              //  cout<<"memcpy "<<tempK[0]<<" "<<Data[0]<<endl;
              //  cout<<"memcpy "<<tempK[1]<<" "<<Data[1]<<endl;
                
                for ( unsigned int r2 = r ; r2 < Nrows; ++r2)
                {
                    // cblas_saxpy = constant * vector + vector
                    //lets print both roows for debuggging
                //    cout<<"-----------Claculate difference vector---- " <<r<<" "<<r2<<"----------"<<endl;
             //       cout<<r<<" : ";
               //     for (unsigned int i =0 ; i < Ncols;++i)
              //          cout<<*(Data + r_loc + i )<<" ";
              //      cout<<endl;
              //      cout<<r2<<" : ";
               //     for (unsigned int i =0 ; i < Ncols;++i)
                //        cout<<*(tempK +  r2*Ncols + i )<<" ";
                 //   cout<<endl;
                    
                    cblas_saxpy(Ncols, -1.0f, Data + r_loc , 1, tempK + r2*Ncols, 1);
               //     cout<<" r2 - r : ";
                 //   for (unsigned int i =0 ; i < Ncols;++i)
                   //     cout<<*(tempK +  r2*Ncols + i )<<" ";
               //     cout<<endl;
                 //   cout<<*(tempK + r2*Ncols + 1)<<endl;
                //    cout<<"norm "<<cblas_scnrm2(Ncols, tempK + r2*Ncols + 1, 1)<<endl;;
                    float norm=cblas_snrm2(Ncols, tempK + r2*Ncols, 1);
                 //   cout<<"K : "<<r<<" "<<r2<<" "<<norm<<" "<<expf(-0.5*norm*norm)<<endl;
                   
                    float exp_par=norm*norm / (lengScale*lengScale);
                    
                    //use symmetry property
                    //length scale derivatie
                    //exp_par/lengScale * 
                    
                    *( dK[0] + r*Nrows + r2 ) = *( dK[0] + r2*Nrows + r ) = exp_par/lengScale * var_f * expf(-0.5*exp_par); 
                    
                    //varf derivatie
                    *( dK[1] + r*Nrows + r2 ) = *( dK[1] + r2*Nrows + r ) = expf(-0.5*exp_par); 
                    //varn scale derivatie
                    //set to identity later
                    
                    //delata function but actually just Itentity
                    *( dK[2] + r*Nrows + r2 ) = *( dK[2] + r2*Nrows + r )  = (r == r2) ? 1 : 0; // *( K + r2*Nrows + r ) = var_f * expf(-0.5*norm*norm / (lengScale*lengScale)); 
                                    
                }
            }
            
		}
        
              
        delete[] tempK;
       /* cout<<"l,varf,mvarn "<<lengScale<<" "<<var_f<<" "<<hyperparams[2]<<endl;
        cout<<"dK/dl "<<Nrows<<" x "<<Nrows<<endl;
        
        for ( unsigned int r = 0 ; r < Nrows; ++r)
        {
            for ( unsigned int r2 = 0 ; r2 < Nrows; ++r2)
                cout<< *( dK[0] + r*Nrows + r2 )<<" ";
            cout<<endl;
        }
        cout<<"dK/dvarf "<<Nrows<<" x "<<Nrows<<endl;
        
        for ( unsigned int r = 0 ; r < Nrows; ++r)
        {
            for ( unsigned int r2 = 0 ; r2 < Nrows; ++r2)
                cout<< *( dK[1] + r*Nrows + r2 )<<" ";
            cout<<endl;
        }
     
        cout<<"dK/dvarn "<<Nrows<<" x "<<Nrows<<endl;
        
        for ( unsigned int r = 0 ; r < Nrows; ++r)
        {
        for ( unsigned int r2 = 0 ; r2 < Nrows; ++r2)
        cout<< *( dK[2] + r*Nrows + r2 )<<" ";
        cout<<endl;
        }
         */ 
    }
   
    
void squaredExponentialCovariance_K( const float * Data, float* K , const std::vector<float> & hyperparams, const unsigned int & Nrows, const unsigned int & Ncols )
    {
        //Estomates K - ocvariance, this is only a function of the training data
        float lengScale=hyperparams[0];
        float var_f=hyperparams[1];
        float var_n=hyperparams[2];

        //assumes that K has Nrows
        //switch below
        //assumme 1 feature per column, rows are dimensionality
        //now let get to work
        //find covariance for evey columen (feature) with every other column
        float * tempK = new float[Nrows*Ncols];
        
        //ionly need to copy features after the base one
		size_t row_size = Ncols*sizeof(float);
        // K should be appropriately allocated
        //loop over dimensions
        for ( unsigned int r = 0 ; r < Nrows; ++r)
		{
			unsigned int r_loc = r*Ncols;
			//copy data over it will write over
            if ( (r+1) != Nrows )
            {//wouldn't go into the loop anyways but need to stop out of bvounds memcpy
     //           cout<<"copy "<<Nrows*Ncols<<" "<<r_loc<<" "<<(Nrows-r)<<" "<<row_size<<endl;
                memcpy(tempK + r_loc, Data + r_loc, (Nrows-r)*row_size);
       //         cout<<"memcpy "<<tempK[0]<<" "<<Data[0]<<endl;
         //       cout<<"memcpy "<<tempK[1]<<" "<<Data[1]<<endl;
                
                for ( unsigned int r2 = r + 1; r2 < Nrows; ++r2)
                {
                    // cblas_saxpy = constant * vector + vector
                    //lets print both roows for debuggging
     //               cout<<"-----------Claculate difference vector---- " <<r<<" "<<r2<<"----------"<<endl;
       //             cout<<r<<" : ";
        //            for (unsigned int i =0 ; i < Ncols;++i)
        //                cout<<*(Data + r_loc + i )<<" ";
      //              cout<<endl;
       //             cout<<r2<<" : ";
       //             for (unsigned int i =0 ; i < Ncols;++i)
       //                 cout<<*(tempK +  r2*Ncols + i )<<" ";
       //             cout<<endl;
                    
                    cblas_saxpy(Ncols, -1.0f, Data + r_loc , 1, tempK + r2*Ncols, 1);
       //             cout<<" r2 - r : ";
        //            for (unsigned int i =0 ; i < Ncols;++i)
        //                cout<<*(tempK +  r2*Ncols + i )<<" ";
         //           cout<<endl;
          //          cout<<*(tempK + r2*Ncols + 1)<<endl;
           //         cout<<"norm "<<cblas_scnrm2(Ncols, tempK + r2*Ncols + 1, 1)<<endl;;
                    float norm=cblas_snrm2(Ncols, tempK + r2*Ncols, 1);
                  //  cout<<"K : "<<r<<" "<<r2<<" "<<norm<<" "<<expf(-0.5*norm*norm)<<endl;
                    //use symmetry property
                    *( K + r*Nrows + r2 ) = *( K + r2*Nrows + r ) = var_f * expf(-0.5*norm*norm / (lengScale*lengScale)); 
              //   add in log likelihood here   
                }
            }
    
		}
         
        //1 because (exp(0))
        for ( unsigned int r = 0 ; r < Nrows; ++r)
            *(K + r*Nrows +r) = var_f + var_n;
        
        delete[] tempK;
        
   //     cout<<"K "<<Nrows<<" x "<<Nrows<<endl;
        
     //   for ( unsigned int r = 0 ; r < Nrows; ++r)
      //  {
        //    for ( unsigned int r2 = 0 ; r2 < Nrows; ++r2)
          //      cout<< *( K + r*Nrows + r2 )<<" ";
      //      cout<<endl;
       // }
		
    }
    
    void squaredExponentialCovariance_Kstar( const float * Data,const float * DataStar, float* Kstar, const std::vector<float> & hyperparams, const unsigned int & Nrows, const unsigned int & Ncols , const unsigned int & Ntest)
    {
        float lengScale=hyperparams[0];
        float var_f=hyperparams[1];
        //float var_n=hyperparams[2];

        //----------Process Kstar----------//
        //does not yet take into account multiple test samples
        float * tempKstar = new float[Nrows * Ncols];
        
        //copy over the test point
        //        float* tempDstar = new float[NumberOfTestSamples_*Ncols];
        //       memcpy(tempDstar, DataStar,NumberOfTestSamples_ * Ncols*sizeof(float) );
        
        //copy over all of training data data
        for ( unsigned int i_test = 0 ; i_test < Ntest; ++i_test)
        { 
            //need to recopy the data for each test subject
            memcpy(tempKstar, Data,Nrows * Ncols*sizeof(float) );
            unsigned int test_loc = i_test*Ncols;
            for ( unsigned int r = 0 ; r < Nrows; ++r)
            {//loop over each training subject
                unsigned int r_loc = r*Ncols;
                
                cblas_saxpy(Ncols, -1.0f, DataStar + test_loc, 1, tempKstar + r_loc, 1);
                float norm=cblas_snrm2(Ncols, tempKstar + r_loc, 1);
                *( Kstar + r + i_test*Nrows) =  var_f * expf(-0.5*norm*norm / (lengScale*lengScale)); 
                
            }
        }
        //  cblas_sgemv(CblasRowMajor, CblasNoTrans, M, N , -1.0f, DataStar, , Data, 1, 1.0f, Kstar, 1)
        delete[] tempKstar;
        
        //--------------------//
//        for ( unsigned int r = 0 ; r < Nrows; ++r)
  //          *(Kstar + r*Nrows +r) += var_n;

    /*    
        cout<<"K* "<<Ntest<<endl;
        for ( unsigned int r2 = 0 ; r2 < Ntest; ++r2)
        {
            for ( unsigned int r = 0 ; r < Nrows; ++r)
            {
                cout<< *( Kstar + r2*Nrows + r )<<" ";
            }
            cout<<endl;
        }
        cout<<endl;
	*/	
    }
    
    
    void squaredExponentialCovariance( const float * Data,const float * DataStar, float* K , float* Kstar, const std::vector<float> & hyperparams, const unsigned int & Nrows, const unsigned int & Ncols , const unsigned int & Ntest)
    {
     
        squaredExponentialCovariance_K(Data, K, hyperparams, Nrows, Ncols);
        squaredExponentialCovariance_Kstar(Data, DataStar, Kstar, hyperparams, Nrows, Ncols,Ntest);

    }
//----------------------------------------------------END OF KERNEL FUNCTIONS--------------------------//
    
    
    
    
    
    
    GaussianProcessFilter::GaussianProcessFilter(){
        GP_mode = LSClassifier;
        NodeName="GaussianProcess";
        alpha=NULL;
        K=NULL;
        cov_func=Exponential;
    }
    GaussianProcessFilter::~GaussianProcessFilter(){
        if (alpha!=NULL)
            delete[] alpha;
    }
    
    void GaussianProcessFilter::setOptions( std::stringstream & ss_options)
    {
        //\---------------SET CUTOM TOPTIONS ------------------//
		
		
		map<string,string> options = parse_sstream(ss_options);
		for ( map<string,string>::iterator it = options.begin(); it!=options.end();it++)
		{
			//cout<<"set svm option "<<it->first<<" "<<it->second<<endl;
			
			if ( it->first == "Mode")
			{
                if (it->second == "Regression")
                {
                    GP_mode=Regression;
                }else if (it->second == "LSClassifier") {
                    GP_mode=LSClassifier;
                }else{
                    throw ClassificationTreeNodeException("Gaussian Process: Invalid options into GP mode");
                }
                    
                    
                

			}else {
				//if not in node specific implementation, check to see if option exists in Tree node.
				//by including at the end, can replace option handling of parent (ClassificationTreeNode);
				if (!setTreeNodeOptions(it->first, it->second))
					throw ClassificationTreeNodeException("Gaussian Process: Invalid option into GP node.");
			}
     
		}

    }
       void GaussianProcessFilter::estimateParameters( )
    {
        cout<<"GaussianProcessFilter :: estimateParameters"<<endl;
        cout<<"Gaussian process estimate aoparmeters "<<endl;
        //going to start with a generic kimplementation that doesn't exploit the undersampling of the data
        //first calculate covariance 
        cov_hyperpars.clear();
        cov_hyperpars.push_back(0.8f);
      //  cov_hyperpars.push_back(1.0*1.0);
//        cov_hyperpars.push_back(1.1*1.1);
        cov_hyperpars.push_back(1.1*1.1);

        cov_hyperpars.push_back(0.5*0.5);

        
        
        if ( K ==NULL)
            delete[] K;
        
        
        
        vector<float> * all_in_data = new vector<float>();
        
        vector<float> * targ_data = new vector<float>();
        *targ_data = training_target->getTargetData<float>();
        print(*targ_data,"GP estiate params: targetdata");
        //vector<float> * all_test_data = new vector<float>();

        //transpose is to order all the sampes for a feature continuously// for kernel claulation
        cout<<"Number of input sources "<<in_data.size()<<endl;
        cout<<"Number of data points at source 0 "<<in_data[0]->size()<<endl;
       // copyToSingleVectorTranspose( in_data, all_in_data, NumberOfSamples_ );
        copyToSingleVector( in_data, all_in_data, NumberOfSamples_ );

        print<float>(*all_in_data,"all_in_data");
  
        cout<<"All in data "<<Dimensionality_<<" "<<NumberOfSamples_<<endl;
        //--------------------OUTPUT DEMEANED MATRIX----------------------//
        for (unsigned int dim = 0 ; dim < Dimensionality_; ++dim)
        {
            
        for ( unsigned int sample = 0 ; sample < NumberOfSamples_; ++sample)//,i_allin += Dimensionality_)
        {
    
                cout<<all_in_data->at(dim*NumberOfSamples_ + sample )<<" ";
        }
            cout<<endl;
        }
        //cblas_stpmv 
        //cblas_strmv
        //Multiplies a triangular matrix by a vector.
	//	float * Kstar;
        cout<<"claculate K"<<endl;
		switch (cov_func){
                
            case Exponential :
                cout<<"calcExp"<<endl;
                cout<<"Sqaured Exponential "<<Dimensionality_<<endl;
				K = new float[NumberOfSamples_*NumberOfSamples_];
//                Kstar = new float[NumberOfTestSamples_*NumberOfSamples_];
                //plus 1 is there for testing
             //   Kstar = new float[(NumberOfTestSamples_+1)*NumberOfSamples_];
             //   all_test_data->push_back(-0.3);
				//data, covariance
                //value of hyperparameters currently take from a tutorial for testing
//                squaredExponentialCovariance(&(all_in_data->at(0)),&(all_test_data->at(0)), K,Kstar,1.0f, 1.27*1.27, 0.3*0.3,NumberOfSamples_, Dimensionality_, NumberOfTestSamples_+1);
                //input data, K=covariance, description length, var_f,var_n
                //estimate hyperpara
                cout<<"do grad search"<<endl;
             //   GPregression_gradientSearch(squaredExponentialCovariance_K,&(all_in_data->at(0)), &(targ_data->at(0)),cov_hyperpars,NumberOfSamples_, Dimensionality_);
                GPregression_conjugateGradientSearch(squaredExponentialCovariance_K,&(all_in_data->at(0)), &(targ_data->at(0)),cov_hyperpars,NumberOfSamples_, Dimensionality_);

    
                squaredExponentialCovariance_K(&(all_in_data->at(0)), K,cov_hyperpars,NumberOfSamples_, Dimensionality_);
                break;
            default:
                break;
        }
        
      		//the matrix is now Column Major
        // cout<<"hmmm"<<endl;
		__CLPK_integer info = run_choleskySymmetricInverse("L", NumberOfSamples_, K, info);
		//cout<<"done colesky inv "<<endl;
    //    __CLPK_integer info=0;
        
        if (info == 0)
			cout<<"Cholesky-based Inverse Successful"<<endl;
        else if (info>0)
			cout<<"Cholesky-based Inverse Failed : "<<info<<"th argument was illegal"<<endl;
        else if (info<0)
            cout<<"Cholesky-based Inverse Failed :not positive definite"<<endl;

        
    /*    cout<<"cholesky factorization matrix"<<endl;
       for (unsigned int dim = 0 ; dim < NumberOfSamples_; ++dim)
        {
            for (unsigned int dim2 = 0 ; dim2 < NumberOfSamples; ++dim2)
            {
           //         if (dim==dim2)
             //           K[dim2+dim*NumberOfSamples_]=1;
               //     else if (dim>dim2)
                 //       K[dim2+dim*NumberOfSamples_]=0;                       
                 //  else
                   //     K[dim2+dim*NumberOfSamples_]=0;                       
            
               //     if (dim==0) if (dim2<4) K[dim2+dim*NumberOfSamples_]=1; 
                cout<<K[dim2+dim*NumberOfSamples_]<<" ";
                
            }
            cout<<endl;

       }
     */
        //calculate alphas , i.e K^-1y
        if (alpha==NULL)
            delete[] alpha;
        
       alpha = new float[NumberOfSamples_];
       // memset(alpha, 0, NumberOfSamples_*sizeof(float));
       //For our puposes, y is the input data, and x is the regressor data.
        //
        
        vector<float> y_data = training_target->getTargetData<float>();
       // vector<float> y_data;
       // training_target->getRegressorData(y_data);
       // print<float>(y_data,"regressor data");
        
        ///THis is K^-1 * y

//choesky, onky stores upper triangle of inverse, use blas symmetric multiplication
       // A*B = K^-1*y
        //Kis symmetric
        cblas_ssymm(CblasRowMajor, CblasLeft, CblasUpper, NumberOfSamples_,1, 1.0f,K , NumberOfSamples_, &(y_data[0]),1, 0.0f, alpha, 1);

        cout<<"alpha "<<NumberOfSamples_<<endl;
        
        for (unsigned int i =0; i < NumberOfSamples_;++i)
            cout<<alpha[i]<<" ";
        cout<<endl;
		
        
     ///////-------------alpha are the parameters to estimate, the rest depends on test data ----------------//   
        
        delete all_in_data;
        delete[] K;
    //    delete[] Kstar;
     //   delete[] alpha;
      //  delete[] predicted_mean;
   //    delete[] v_mean;
    //    delete[] v_mean_kernel;
        cout<<"done clean"<<endl;
        //delete[] Sigma;
        
    }
    void GaussianProcessFilter::applyParametersToTrainingData()
    {
        
        /*
        cout<<"copy data "<<NumberOfSamples_<<" "<<in_data.size()<<endl;
        // if (out_data == NULL)
        //     cout<<"outdata is NULL"<<endl;
        // if ( in_data[0] == NULL)
        cout<<"access in data2 "<<endl;
        cout<<"in_data size "<<in_data.size()<<endl;
        cout<<"indata is NULL "<<in_data[0]->size()<<endl;
        
        copyToSingleVector( in_data, out_data, NumberOfSamples_ );
        
        cout<<"done copy"<<endl;
        vector<float> Design;
        unsigned int N_regressors = training_target->getRegressorData(Design);
        cout<<"got design "<<endl;
        //calculate the residual
        //does data - betas*design
        cout<<"do multiplication"<<endl;
        // print<float>(*out_data,"outdata1");
        
        cblas_sgemm( CblasColMajor, CblasTrans, CblasNoTrans,Dimensionality_,NumberOfSamples_, N_regressors,-1,estimated_betas,N_regressors,&(Design[0]), N_regressors, 1, &(out_data->at(0)), Dimensionality_);
        cout<<"done multiply"<<endl;
        //  print<float>(*out_data,"outdata2");
        if (Verbose)
			cout<<"Done applying "<<NodeName<<" to training data."<<endl;
        cout<<"Done applying "<<NodeName<<" to training data."<<endl;
*/
        
        
    }
    void GaussianProcessFilter::applyToTestData(){
        //--------------------
		if (Verbose)
			cout<<"Applying Gaussian Process filter to test data (residuals)..."<<endl<<"Number of Test Samples: "<<NumberOfTestSamples_<<"."<<endl;
		//--------------------
        clearOutData();

        vector<float> * all_test_data = new vector<float>();
        vector<float> * all_in_data = new vector<float>();
        cout<<"copy test data "<<in_test_data.size()<<endl;
        copyToSingleVector( in_test_data, all_test_data, NumberOfTestSamples_ );
        cout<<"copy in data"<<endl;
        copyToSingleVector( in_data, all_in_data, NumberOfSamples_ );
        print<float>(*all_test_data,"all_test_data");

        //float * K;
        float * Kstar;
		switch (cov_func){
                
            case Exponential :
                cout<<"Sqaured Exponential "<<Dimensionality_<<endl;
                //K = new float[NumberOfSamples_*NumberOfSamples_];
                //                Kstar = new float[NumberOfTestSamples_*NumberOfSamples_];
                //plus 1 is there for testing
                Kstar = new float[(NumberOfTestSamples_+1)*NumberOfSamples_];
                //   all_test_data->push_back(-0.3);
				//data, covariance
                //value of hyperparameters currently take from a tutorial for testing
                //                squaredExponentialCovariance(&(all_in_data->at(0)),&(all_test_data->at(0)), K,Kstar,1.0f, 1.27*1.27, 0.3*0.3,NumberOfSamples_, Dimensionality_, NumberOfTestSamples_+1);
                //input data, K=covariance, description length, var_f,var_n
                squaredExponentialCovariance_Kstar(&(all_in_data->at(0)), &(all_test_data->at(0)),Kstar,cov_hyperpars,NumberOfSamples_, Dimensionality_,NumberOfTestSamples_);
              

                break;
            default:
                break;
        }

        
        cout<<"predicted mean stuff"<<endl;
        
        //Now just need to multiply alpha by the appropriate input 
        
        float* predicted_mean = new float[NumberOfSamples_*NumberOfTestSamples_+1];
        // memset(predicted_mean, 0, NumberOfSamples_*sizeof(float));
        //MAY HAVE TO ADJUST SOME SIZES WHEN GOING MULTIVVARIA?TE
        for ( unsigned int i_test = 0 ; i_test < NumberOfTestSamples_+1; ++i_test)
        {
            cblas_sgemm( CblasRowMajor, CblasNoTrans, CblasNoTrans, 1, 1, NumberOfSamples_, 1.0f, Kstar + NumberOfSamples_*i_test, NumberOfSamples_, alpha, 1, 0.0, predicted_mean + i_test, NumberOfSamples_);
        }
        if (out_test_data == NULL)
			out_test_data = new vector<float>();
      
        
        if (GP_mode == Regression)
        {
            for (unsigned int i =0; i < NumberOfTestSamples_;++i)
                out_test_data->push_back(predicted_mean[i]);
        }else if (GP_mode == LSClassifier)
        {
            for (unsigned int i =0; i < NumberOfTestSamples_;++i)
            {   //threshold to 0-1
                cout<<"outprethresh "<<i<<" "<<predicted_mean[i]<<endl;
                out_test_data->push_back((predicted_mean[i]>0.5));
            }
        }
        cout<<"outsize "<<out_test_data->size()<<endl;
        print<float>(*out_test_data,"out_test_data");
        
		cout<<"clean up stuff"<<endl;
        delete[] predicted_mean;
        delete[] Kstar;
        delete all_test_data;
/*        
        
        vector<float> test_regressor_data;
		unsigned int N_regressors = training_target->getTestRegressorData(test_regressor_data);
		copyToSingleVector( in_test_data, out_test_data, NumberOfTestSamples_ );
        //        cout<<"out data size "<<out_test_data->size()<<" "<<NumberOfTestSamples_<<" "<<Dimensionality_<<endl;
        //      cout<<"number of regressors "<<training_target->getNumberOfRegressors()<<endl;
        //    print<float>(*out_test_data,"test data");
        //     unsigned int N_regressors = training_target->getNumberOfRegressors();
        print<float>(test_regressor_data,"predictors");
        
            
        float* predicted_mean = new float[NumberOfSamples_*NumberOfTestSamples_+1];
        // memset(predicted_mean, 0, NumberOfSamples_*sizeof(float));
        //MAY HAVE TO ADJUST SOME SIZES WHEN GOING MULTIVVARIA?TE
        for ( unsigned int i_test = 0 ; i_test < NumberOfTestSamples_+1; ++i_test)
        {
            cblas_sgemm( CblasRowMajor, CblasNoTrans, CblasNoTrans, 1, 1, NumberOfSamples_, 1.0f, Kstar + NumberOfSamples_*i_test, NumberOfSamples_, alpha, 1, 0.0, predicted_mean + i_test, NumberOfSamples_);
        }
        cout<<"predicted means "<<endl;
        for (unsigned int i =0; i < NumberOfTestSamples_+1;++i)
            cout<<predicted_mean[i]<<" ";
        cout<<endl;
        
        
        print<float>(*out_test_data,"predicted");
 */

    }
    
    
}
