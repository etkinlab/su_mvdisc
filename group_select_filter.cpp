/*
 *  group_select_filter.cpp
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 11/12/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


//its own header file
#include "group_select_filter.h"


using namespace std;
using namespace misc_utils_name;


namespace su_mvdisc_name{
	GroupSelect_Filter::GroupSelect_Filter()
	{
				NodeName="GroupSelect_Filter";
		filt_mode_=EXCLUDE;
	}
	
	GroupSelect_Filter::~GroupSelect_Filter()
	{
	}
	
	
	void GroupSelect_Filter::setOptions( std::stringstream & ss_options)
	{
		map<string,string> options = parse_sstream(ss_options);
		for ( map<string,string>::iterator it = options.begin(); it!=options.end();it++)
			if ( it->first == "groups")
			{
			//	cout<<"set groups"<<endl;
				select_groups = csv_string2nums_i(it->second);
				print<int>(select_groups,"groups");
//				select_groups.push_back(groups);
			}else if ( it->first == "mode")
			{
			//	cout<<"set mode "<<endl;
				if (it->second.compare("INCLUDE") == 0)
					filt_mode_=INCLUDE;
				else if (it->second.compare("EXCLUDE") == 0)
					filt_mode_=EXCLUDE;
				else {
					throw ClassificationTreeNodeException("Unrecognized Filter Mode. Options are \"include\" and \"exclude\".");

				}

			}else {
				if (!setTreeNodeOptions(it->first, it->second))
					throw ClassificationTreeNodeException((NodeName + " : Invalid input argument : " + it->first).c_str());
			}
		
			if ((filt_mode_ == INCLUDE) && (select_groups.size()<1))
				throw ("Group Select Mode = INCLUDE is not compatible iwth zerto groups");
				
	}
	
	
	void GroupSelect_Filter::runSpecificFilter(  bool with_estimation )
	{		
		
		//cout<<"Run group select filter "<<endl;
		if (out_data == NULL)
			out_data = new vector<float>();
		
		copyToSingleVector( in_data, out_data, NumberOfSamples_ );
//		print<float>(*out_data,"out_data - pretrimmed inner ");

		//----------------------------filter training data being passed on to next node------------------------//-------------
		//this will be used to filter the data using basefilter and target functions
		vector<short> sub_sel_mask(NumberOfSamples_,1);
			//loops over number of samples 
		vector<short>::iterator i_sub_sel = sub_sel_mask.begin();
		//	vector<float>::iterator i_out = out_data->begin();
			for ( vector<float>::iterator i_targ = training_target->begin() ; i_targ != training_target->end() ; ++i_targ , ++i_sub_sel)
			{
		//		cout<<"targ "<<*i_targ<<endl;
				
				//first check to see if group is in list
				bool foundGroup = false;
				for ( vector<int>::iterator i_gs = select_groups.begin() ; i_gs != select_groups.end() ; ++i_gs)
				{
					if ( *i_targ == *i_gs )
					{
						foundGroup=true;
					}
						
				}
				
				//if it wasn't in the group list and you wnat to include (then set to zero, or in group list + want to exclude
				if (	((filt_mode_ == INCLUDE ) && (!foundGroup)) ||  ((filt_mode_ == EXCLUDE ) && (foundGroup))	)
					*i_sub_sel=0;
			}

		if ( sum<short>(sub_sel_mask) == 0 )
			throw ClassificationTreeNodeException("Group Select filter : No Training Subjects left ! ");
		
	
		//cout<<"Dimensionality_/smaple size "<<Dimensionality_<<" "<<NumberOfSamples_<<" "<<in_data[0]->size()<<endl;
		base_filter::applySubjectFilterToData(in_data,sub_sel_mask,out_data, Dimensionality_, NumberOfSamples_);
        //the selection mask is repeated twice, so that the left out subjects are not included in the test sample
		training_target->applySelectionMask(sub_sel_mask,sub_sel_mask,false);
		//cout<<"Dimensionality_/smaple size post "<<Dimensionality_<<" "<<NumberOfSamples<<" "<<in_data[0]->size()<<endl;

		
		//print<float>(*out_data,"out_data - trimmed ");
//		cout<<"target "<<endl;
//		for ( vector<int>::iterator i_targ = training_target->begin() ; i_targ != training_target->end() ; ++i_targ , i_out += Dimensionality_)
//		{
//			cout<<*i_targ<<" ";
//		}
//		cout<<endl;
		
		//cout<<"done target"<<endl;
	}
//	void GroupSelect_Filter::applyToTestData()
//	{
		//don't need to do anytingn but pass on data because it is a subject filter
//		if (out_test_data == NULL)
//			out_test_data = new vector<float>();
//		cout<<"applyToTestDataTarget Group Select "<<NumberOfTestSamples<<endl;
		//don't filter the test data, will classify into one of remainign groups
//		copyToSingleVector( in_test_data, out_test_data, NumberOfTestSamples );
//		
//		cout<<"done apply filter"<<endl;
//	}
	
	
	
}
