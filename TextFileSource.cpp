/*
 *  TextFileSource.cpp
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 8/10/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include <misc_utils.h>

#include "TextFileSource.h"
#include <fstream>
#include <vector>
#include <ClassificationTreeNodeException.h>
#include <map>



//#include <misc_utils/misc_utils.h>

#define MAX_LINE_SIZE 136192
using namespace std;
using namespace misc_utils_name;

namespace su_mvdisc_name{
	
	TextFileSource::TextFileSource()
	{
		NumberOfSources_=1;
		NodeName="TextFileSource";
        filetype_=CSV;
		//cout<<"create TextFileSource "<<endl;
        string mask_name="";
        hasHeader_=false;

	}
	TextFileSource::~TextFileSource()
	{
		
	}
    void TextFileSource::setMaskName(const std::string & filename )
    {
        maskname = filename;
    }
    
    //Set file type for reading. Options are CSV and SSV
    void TextFileSource::setFileType( const TextSrcfileTypes & ftype )
    {
        filetype_ = ftype;
    }
    
    //Set file type for reading. Options are CSV and SSV
    void TextFileSource::setFileType( const std::string & ftype )
    {
        if ( ftype == "csv" ) filetype_ = CSV;
        else if ( ftype == "ssv" ) filetype_ = SSV;
    }
    
	/*
	vector<string> parse_line_to_strings( char* s, char* delim )
	{
		
	}
	
	vector<string> parse_line_to_floats( char* s, char* delim )
	{
		
	}
	*/
	
	void TextFileSource::setOptions( std::stringstream & ss_options)
	{
		map<string,string> options = parse_sstream(ss_options);
		//cout<<"retrieved options"<<endl;
		
		for ( map<string,string>::iterator it = options.begin(); it!=options.end();it++)
			if ( it->first == "filename")
			{
				setFileName( it->second );
			}else if ( it->first == "maskname")
			{
				setMaskName( it->second );
			}else if ( it->first == "header")
			{
                string shead = it->second;
                if ((shead == "true") || (shead == "1"))
                    hasHeader_ = true;
                else if ((shead == "false") || (shead == "0"))
                    hasHeader_ = false;
                else
                    throw ClassificationTreeNodeException("Image Source: invalid input to header. ");
			}else {
				if (!setTreeNodeOptions(it->first, it->second))
                    //cerr << "tree node options: " << it->first << ", " << it->second << endl;
					throw ClassificationTreeNodeException((NodeName + " : Invalid input argument : " + it->first).c_str());
			}
	}
	
	
	void TextFileSource::readFile( )
	{
        //mask at read in level, make things easier for computing accuracies and such
       cerr<<"read "<<filename.c_str()<<endl;
        vector<short> mask;
        if (maskname != "")
        {
            ifstream fmask(maskname.c_str());
            short val;
            while (fmask>>val)//,MAX_LINE_SIZE))
            {
                mask.push_back(val);
            }
            fmask.close();
        }
        
        
		ifstream f_in ( filename.c_str() );
		
        if ( f_in.is_open() )
        {
            if (hasHeader_){
                string header;
                getline(f_in, header);
                
                if (filetype_ == SSV)
                    header_ = ssv_2_vector(header);
                else  if (filetype_ == CSV)
                    header_ = csv_2_vector(header);
            }
        }
		if ( f_in.is_open() )
		{
			in_data.clear();
            vector<float>* data_src = new vector<float>();

            if (filetype_ == SSV)
            {
                //string type_of_node;
               // char* line = new char[MAX_LINE_SIZE];
                string line;
                NumberOfSamples_=0;
                Dimensionality_=0;
                if (mask.empty()){

                    while ( getline(f_in, line) ) //f_in.getline(line,MAX_LINE_SIZE))
                    {
                        stringstream ss(line);
                        string temp;
                        ss>>temp;
                        // cout<<"temp ."<<temp<<"."<<endl;
                        if (temp.length() > 0 ) //exclude blank lines (particularly the last one
                        {
//                               	cout<<"Read Line "<<line<<endl;
                            stringstream ss_line;
                            
                            ss_line <<line;
                            float ftemp;
                            while (ss_line>>ftemp) {
                                //     	cout<<ftemp<<" ";
                                data_src->push_back(ftemp);
                            }
                            //  cout<<endl;
                            NumberOfSamples_++;
                        }
                    }
                }else
                {
                    vector<short>::iterator i_mask = mask.begin();

                    while ( getline(f_in, line) ) //f_in.getline(line,MAX_LINE_SIZE))
                    {
                        if ( *i_mask > 0 ){

                            stringstream ss(line);
                            string temp;
                            ss>>temp;
                          //  cout<<"temp ."<<temp<<"."<<endl;
                            if (temp.length() > 0 ) //exclude blank lines (particularly the last one
                            {
                                //	cout<<"Read Line "<<line<<endl;
                                stringstream ss_line;
                                
                                ss_line <<line;
                                float ftemp;
                                while (ss_line>>ftemp) {
                                    //		cout<<ftemp<<" ";
                                    data_src->push_back(ftemp);
                                }
                                //	cout<<endl;
                                NumberOfSamples_++;
                            }
                            ++i_mask;
                        }
                    }
                        
                    
                }
            }else if (filetype_ == CSV)
            {
                cerr<<"csv file"<<endl;
                string line;
                //check if doing masking
                if (mask.empty()){
                    cout<<"empty mask"<<endl;
                    while (getline(f_in,line))//,MAX_LINE_SIZE))
                    {
		           cerr<<"readline "<<endl;
                        if (line.empty())//account for leadin g black line
                            break;
                        //line=csv_string_replace_all(line,"NA",0);
                            vector<float> vftemp = csv_string2nums_f(line);
			                            print(vftemp,"vftemp");
                     
                        data_src->insert(data_src->end(), vftemp.begin(), vftemp.end());
                            NumberOfSamples_++;
			    //cout<<"insert doen"<<endl;
                    }
                }else{
                    cout<<"non-empty mask"<<endl;

                    vector<short>::iterator i_mask = mask.begin();
                    while (getline(f_in,line))//,MAX_LINE_SIZE))
                    {   
                        if ( *i_mask > 0 ){
                            //cerr<<"readline mask"<<endl;
//                            line=csv_string_replace_all(line,"NA",0);
                            vector<float> vftemp = csv_string2nums_f(line);
                            //    print(vftemp,"vftemp");

                            
                            
                            data_src->insert(data_src->end(), vftemp.begin(), vftemp.end());
                           NumberOfSamples_++;
                        }
                        ++i_mask;
                        
                    }
                }

            }
            cout<<"done reading file "<<Verbose<<endl;

            if (Verbose >=1 )
                cout<<"done reading file"<<endl;
			NumberOfSources_=1;
			Dimensionality_=data_src->size()/NumberOfSamples_;
            if (Verbose >=1 )
            {
                print<float>(*data_src,"TextFileSource:: data_src");
                cout<<"Dimensionality_ "<<Dimensionality_<<endl;
                cout<<"NumberOfSamples_ "<<NumberOfSamples_<<endl;
            }
	

			in_data.push_back(data_src);
			v_dims_.push_back(Dimensionality_);
            cout<<"set src image dims "<<endl;
            
            
            
            if (src_image_res_ == NULL)
                src_image_res_ = new vector<float4>();            
            if (src_image_dims_ == NULL)
                src_image_dims_ = new vector<uint3>();
            if (src_names_ == NULL)
                src_names_ = new vector<string>();
            if (src_dims_ == NULL)
                src_dims_ = new vector<unsigned int>();
            
            
            src_image_res_->push_back(float4(0,0,0,0));
            src_image_dims_->push_back(uint3(0,0,0));
            src_names_->push_back(NodeName);
            cout<<"set mask"<<endl;

            if (maskname != "")
            {
                src_dims_->push_back(mask.size());

            }else{
                src_dims_->push_back(Dimensionality_);

            }
            cout<<"end text read"<<endl;
			
		}else{
			
			cerr<<"Could not open file : "<<filename<<endl;
			throw ClassificationTreeNodeException("Could not open file");
		}
		
			
    }
	
	
}
