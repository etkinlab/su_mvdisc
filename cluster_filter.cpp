
/*
 *  ClusterFilter.cpp
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 8/12/10.
 *  Copyright 2010 _Stanford University__. All rights reserved.
 *
 */

#include "cluster_filter.h"
//#include <misc_utils/misc_utils.h>
#include <misc_utils.h>
#include <clapack_utils.h>
#include <cmath>

#include <set>

//namespace limf = std::numeric_limits<float>;

using namespace std;
using namespace misc_utils_name;
using namespace clapack_utils_name;

namespace su_mvdisc_name{
    ClusterFilter::ClusterFilter()
    {
        NodeName="ClusterFilter";
        K_=2;
        IterMax_=1e6;
        cluster_reps_=100;
        init_meth_=FORGY;
        cluster_method_=K_MEANS;
        srand (time(nullptr));
        k_means_=nullptr;
        
        data_in_=nullptr;
        use_clusters_as_output_=false;
        
    }
    
    ClusterFilter::~ClusterFilter(){
        if (k_means_ == nullptr ){
            delete k_means_;
        }
        if (data_in_ == nullptr ){
            delete data_in_;
            
        }
    }
    
    
    //Set the method by which to cluster. Currently this is only K-means, but may be extended
    //in the future
    void ClusterFilter::setClusterMode( CLUSTER_MODE mode )
    {
        cluster_method_=mode;
    }
    
    void ClusterFilter::useClustersAsOutput(const bool & state )
    {
        use_clusters_as_output_ = state;
    }
    //Set the number of repitions of the clustering algorithm.
    void ClusterFilter::setRepititions( const unsigned int & R )
    {
        cluster_reps_ = R;
    }
    
    
    //Set the method by which to cluster. This however allows for the imput of a pre-specifed
    //string. This is for convenience, particulary when setup use a text configuration file
    void ClusterFilter::setClusterMode( const string & mode )
    {
        if	(	mode == "k-means"	)
        {
            setClusterMode(K_MEANS);
        }else if	(	mode == "slink"	)
        {
            setClusterMode(SLINK);
        }else if	(	mode == "clink"	)
        {
            setClusterMode(CLINK);
        }else {
            throw ClassificationTreeNodeException( ("Unrecognized Clustering Method Specified: "+mode).c_str());
        }
        
    }
    
    //Depending on the clustering method, different strategies exist for initialization of the
    //means. For k-means, Forgy and Random Partition are available
    void ClusterFilter::setInitMethod( INIT_METHOD mode )
    {
        init_meth_ = mode;
    }
    
    //Set the initialization method using string as input.
    void ClusterFilter::setInitMethod( const std::string & mode )
    {
        if		(	mode == "Forgy"	)
        {
            setInitMethod(FORGY);
        }else if (	mode == "RandomPartitions"	)
        {
            setInitMethod(RNDM_PART);
        }else
        {
            throw ClassificationTreeNodeException( ("Unrecognized Initialization Method Specified: "+mode).c_str());
        }
    }
    
    //Set the number of nodes to partition the data into.
    void ClusterFilter::setK( const unsigned int & K )
    {
        K_=K;
    }
    
    //SetOptions is implemented in all nodes in order to interpret node option from
    //the configuration file.
    void ClusterFilter::setOptions( std::stringstream & ss_options)
    {
        map<string,string> options = parse_sstream(ss_options);
        for ( map<string,string>::iterator it = options.begin(); it!=options.end();it++)
        if ( it->first == "method")
        {
            setClusterMode( it->second );
        }else if ( it->first == "init")
        {
            setInitMethod( it->second );
        }else if ( it->first == "repitions" )
        {
            cluster_reps_=stol(it->second);
        }else if ( it->first == "clustersAsOutput")
        {
            if ((it->second == "0" ) || (it->second == "false"))
            {
                useClustersAsOutput(0);
            }else if ((it->second == "1" ) || (it->second == "true")){
                useClustersAsOutput(1);
            }else{
                throw ClassificationTreeNodeException((NodeName + " : Invalid value for clustersAsOutput : " + it->second).c_str());
                
            }
        }else {
            if (!setTreeNodeOptions(it->first, it->second))
            throw ClassificationTreeNodeException((NodeName + " : Invalid input argument : " + it->first).c_str());
        }
        
        
    }
    
    void ClusterFilter::doKmeans( const smatrix & m_in , const unsigned int & NumberOfSamples,smatrix & k_means, const unsigned int &  K , const unsigned int & cluster_reps, const INIT_METHOD & init_meth , std::vector<unsigned int> & k_final )
    {
        
        
        vector<unsigned int> k_classes(NumberOfSamples,0);
        k_final.assign(NumberOfSamples,0); // Class labels for each sample.
        
        
        //Repeat the clustering method multiple times. The initialization is stochastic and a global solution is
        //not guaranteed. The method is repeated multiple times to stabilize the solution and to approach
        //the global solution.
        float L2min; //keep track of the solution that minimizes the L2 norm. L2 will always be greater than 0.
        
        //create outside loop to avoid multiple allocations
        smatrix* k_mean_local = new smatrix(k_means.MRows,k_means.NCols);
        for (unsigned int i = 0 ; i < cluster_reps; ++i)
        {
            cout<<"Repition : "<<i<<endl;
            //Initialize mean given specified method
            initializeMeans(m_in,*k_mean_local,k_classes,K,init_meth);
//                        print(k_classes,"initial clusters");
            //            smatrix_print(*k_mean_local,"initial k_means");
            
            //Estimate the k-means solution
            float L2 =  estimate_kmeans(m_in, *k_mean_local, k_classes,K);
            
            //Save the minimum solution.
            //For the first iteration through, store results. Otherwise, if the L2 norm for the solution
            //is less than that for the currently stored soltuions, replace it.
            if ( ( i == 0 ) ||   (L2 < L2min) )
            {
                k_final = k_classes;
                L2min = L2;
                k_means = *k_mean_local;
            }
        }
        delete k_mean_local;
    }
    
    
    void ClusterFilter::doAgglomerative()
    {
        //start by initializing to N clusters (i.e. one per subejct)
        
        vector<unsigned int> clusters(NumberOfSamples_,0);
        set<unsigned int> s_clusts; 
        unsigned int index = 0 ;
        for (auto i_c = clusters.begin(); i_c != clusters.end(); ++i_c, ++index)
        {
            *i_c = index;
            s_clusts.insert(index);
        }
        
        //stores dissimilarity between samples
        //This is inefficient in terms of memory due to not storing as a triangular matrix
//        smatrix Dissimilarity(NumberOfSamples_,NumberOfSamples_);
        smatrix Dissimilarity(NumberOfSamples_,NumberOfSamples_);

        //initialize all entry to -1, simmilarity metrics are assumed to be >0
        Dissimilarity=-1;
        vector<float> A_d;//distance to closest
        vector<unsigned int> A_v;
        //assume squared  euclidean distance SQ_EUCLID distance
        //Calculate the euclidean distance to each mean.
        smatrix_print(*data_in_,"doAgglomerative-data_iin ");
        smatrix_dissimilarityEucSq(*data_in_, Dissimilarity,A_v,A_d);
        smatrix_print(Dissimilarity,"doAgglomerative-Dissimilarity ");

        print(A_d,"A_d");
        print(A_v,"A_v");
        for (int hier_iter = 0 ; hier_iter < 7; ++hier_iter)
        	while ( s_clusts.size() > 1)
        {
        	//find minimum distance, i.e. closest cluster based on A_d
        	pair<unsigned int,float> min = pair<unsigned int,float>(0,A_d[0]);
        	//only take pairs to merge, likelihood of multiple identical is small given our data
        	{
        		unsigned int index = 1; //index start at 1 because initialize min to first element
        		for (auto i_d = A_d.begin()+index; i_d != A_d.end(); ++i_d,++index )
        		{
        			if (*i_d < min.second)
        			{
        				min.first = index;
        				min.second = *i_d;
        			}
        		}
        	}
        	cout<<"min "<<min.first<<" "<<min.second<<endl;

        	//---merge to samples---//
        	//merge min.first and A_v[min.first]
        	unsigned int index_i = min.first;
        	unsigned int index_j = A_v[min.first];

        	//Need to seach Di,k Dj,k for all i,j != k
        	//the closest vertex is the closest of what is stored in A_d and the new addition to the cluster
        	pair<unsigned int,float> new_min = pair<unsigned int,float>(0,std::numeric_limits<float>::infinity());
        	//initialize new_min
        	//        while ( (new_min.first == index_i) || (new_min.first == index_j)  )
        	//        {
        	//        	new_min.first++;
        	//        }

        	//Search i
        	float* i_row = Dissimilarity.sdata + index_i;
        	for (unsigned int i = 0 ; i < NumberOfSamples_ ; ++i, i_row += NumberOfSamples_ )
        	{
        		if ((i != index_j ) && (i != index_i) && (!isinf(A_d[i])) )
        		{
        			if ((*i_row) < new_min.second )
        			{
        				new_min.first = i;
        				new_min.second = *i_row;
        				cout<<"i new min "<<new_min.first<<" "<<new_min.second<<endl;
        			}
        		}
        	}
        	//Search j 
        	float* j_row = Dissimilarity.sdata + index_j;
        	for (unsigned int j = 0 ; j < NumberOfSamples_ ; ++j, j_row += NumberOfSamples_ )
        	{
        		if ((j != index_j ) && (j != index_i) && (!isinf(A_d[j])) )
        		{
        			cout<<"test "<<(*j_row)<<" "<<new_min.second <<endl;
        			if ((*j_row) < new_min.second )
        			{
        				new_min.first = j;
        				new_min.second = *j_row;
        				cout<<"j new min "<<new_min.first<<" "<<new_min.second<<endl;
        			}
        		}
        	}

        	smatrix_dissimilarityMergeSlink(Dissimilarity,index_i,index_j);
        	
        	//set clusters
        	unsigned int cl_i = clusters[index_i];

        	unsigned int cl_j = clusters[index_j];
        	for (auto i_cl = clusters.begin(); i_cl != clusters.end();  ++i_cl)
        	{
        		if (*i_cl == cl_j) *i_cl = cl_i;
        	}
        	
        	s_clusts.erase(cl_j);

        	
//        	clusters[index_j]=clusters[index_i];
        	smatrix_print(Dissimilarity,"doAgglomerative-Merge ");



        	//        if ( A_d[index_j] < A_d[min.first]  )
        	//        {
        	//            A_d[index_i] = A_d[index_j];
        	//            A_v[index_i] = index_j;
        	//
        	//        }
        	A_d[index_i] = new_min.second;
        	A_v[index_i] = new_min.first;
        	A_d[index_j] = std::numeric_limits<float>::infinity();
        	
        	
        	print(A_d,"A_d");
        	print(A_v,"A_v");
        	print(clusters,"clusters");
        }
    }
    
    
    //As with all nodes, estimateParameters estimate the given model parameters. In the case of k-means, it
    //estimates cluster labels for each sample and the cluster means.
    void ClusterFilter::estimateParameters()
    {
        cout<<"ClusterFilter::estimateParameters"<<endl;
        cout<<"Number of Cluster Repitions : "<<cluster_reps_<<endl;
        
        
        if (Verbose>2){
            
            cout<<"ClusterFilter::estimateParameters"<<endl;
            
        }
        //one cannot set the number of nodes to a value greater than number of samples
        if (NumberOfSamples_ < K_)	throw ClassificationTreeNodeException("Cluster Filter Error : Number of Classes > Number of Samples");
        
        
        if (out_data==nullptr)
        {
            out_data = new vector<float>(NumberOfSamples_*Dimensionality_);
        }
        copyToSingleVector( in_data, out_data, NumberOfSamples_ );
        
        //Create data matrix
        
        if (data_in_ == nullptr)
        {
            data_in_ = new smatrix(NumberOfSamples_, Dimensionality_);
        }
        //Do not resize matrix as it will be handled by the setData function
        
        //copy data from all sources into data_in matrix.
        //Required to transpose the data into Row Major format
        data_in_->setData(*out_data,NumberOfSamples_, Dimensionality_);
        //        smatrix_print(*data_in_,"data_in");
        //        cout<<"done setting data "<<K_<<" "<<Dimensionality_<<endl;
        if (k_means_== nullptr)
        {
            k_means_ = new smatrix(K_,Dimensionality_); //This will store initial class means as well as the final estimates.
            
        }else{
            k_means_->resize(K_,Dimensionality_);
        }
  
        
        
        if (cluster_method_ == K_MEANS )
        {//----------------KMEANS IMPLEMENTATION-----------------------//
            doKmeans(*data_in_, NumberOfSamples_, *k_means_, K_,cluster_reps_,init_meth_, k_classes_final_);
        }else if ( ( cluster_method_ == SLINK ) || ( cluster_method_ == CLINK ))
        {
            doAgglomerative();
        }
        //--------------------------------------------------------------//
        cout<<"end ClusterFilter::estimateParameters"<<endl;
        
    }
    void ClusterFilter::applyParametersToTrainingData()
    {
        //    	cout<<"apply parameters to training data "<<endl;
        //if out data has not been created, do so now
        if (out_data == nullptr)
        {
            out_data = new vector<float>(NumberOfSamples_);
        }
        if (out_clusters_ == nullptr)
        {
            out_clusters_ = new vector<unsigned int>(NumberOfSamples_);
        }
        //otherwise resize the output
        //    	 else	out_data->resize(NumberOfSamples_);
        
        //copying and cast clusters labels
        //    	 vector<float>::iterator i_out = out_data->begin();
        auto i_out = out_clusters_->begin();
        for (auto i_cluster = k_classes_final_.begin(); i_cluster != k_classes_final_.end(); ++i_cluster, ++i_out)
        {
            cout<<"cluster "<<(*i_cluster)<<endl;
            *i_out = (*i_cluster);
        }
        
        cout<<"copy cluster data to outdata "<<out_clusters_->size()<<endl;
        
        
        //just passed on data
        if (use_clusters_as_output_)
        {
            out_data->resize(out_clusters_->size());
            auto i_out = out_data->begin();
            for (auto i_c = out_clusters_->begin(); i_c != out_clusters_->end(); ++i_c, ++i_out)
            *i_out = *i_c;
        }else{
            copyToSingleVector( in_data, out_data, NumberOfSamples_ );
            
        }
        
        
        cout<<"copy name "<<in_data_names_.size()<<endl;
        cout<<"copy name "<<in_data_names_[0]->size()<<endl;
        
        //        copyToSingleVector( in_data_names_, out_data_names_, 1);
        
        //    	cout<<"end - apply parameters to training data "<<endl;
        
        
    }
    
    void ClusterFilter::runSpecificFilter()
    {
        estimateParameters();//does both estimate and write at once
        applyParametersToTrainingData();
        
    }
    
    
    
    
    
    void ClusterFilter::applyToTestData()
    {
        cout<<"applyToTestData" <<endl;
        if (Verbose)
        cout<<"ClusterFilter::applyToTestData"<<endl;
        if (out_test_data == nullptr)
        out_test_data = new vector<float>();
        copyToSingleVector( in_test_data, out_test_data, NumberOfTestSamples_ );
        cout<<"end - applyToTestData" <<endl;
        
    }
    
    
    
    
    
    
    //This function initializae the cluster means using the indicated method.
    //It is separated out from estimate parameters for clarity in the code.
    //Particularly given that the algorithm is repeated cluster_reps_ times.
    void ClusterFilter::initializeMeans(const smatrix & data_in , smatrix & k_means, std::vector<unsigned int> & k_init,const unsigned int & K , const INIT_METHOD & init_meth )
    {
        unsigned int ns = data_in.MRows;
        
        
        ///------INITIALIZATION------------//
        
        if ( init_meth == FORGY )
        {
            //Initializing mean to two instances; Forgy method.
            //Use set since it will continue to add selections until there are K_ unique selections.
            //The set container will not insert a value that already exists
                       cout<<"FORGY "<<K<<endl;
            set<unsigned int> selected;
            while (selected.size() < K )//need to make sure don't select same subject
            {
                unsigned int val = rand() % ns;
                selected.insert(val);
                
            }
            //Copy the selection into the k_means matrix
            //Row index will refer to the class.
            unsigned int count=0;
            for ( set<unsigned int>::iterator i = selected.begin(); i != selected.end(); ++i, ++count)
            {
                smatrix_setRow(k_means, count , data_in, *i);
            }
//                    smatrix_print(k_means,"k_menas");
        }else if ( init_meth == RNDM_PART )
        {
            cout<<"do randmom partitions"<<endl;
            //Initialize means by randomly partitioning the data into K groups
            //Initialize list with each subject index, then proceed to pick from the list
            list<unsigned int> l_subjects;
            for (unsigned int i = 0 ; i < ns; ++i)
            l_subjects.push_back(i);
            cout<<"do randmom partitions:1"<<endl;
            
            //Pull subject indices from the list. Use count % K to altenate the class assignment
            unsigned int count=0;
            while (! l_subjects.empty() ){
                list<unsigned int>::iterator i_s = l_subjects.begin();//initialize iterator to beginning of list
                advance(i_s,rand() % l_subjects.size()); //Randomly selected subject out of those remaing
                l_subjects.erase(i_s); //Remove the subject from the list
                k_init[*i_s] = count % K ; //cycle allocation to each group, simplifies equal number of samples per group.
                ++count;
            }
          cout<<"do randmom partitions:2"<<endl;
            
            //Calculate class means given the new classifications.
            smatrix_mean_by_row(data_in,k_means, k_init);
            
            
        }else {
            cerr<<"Clustering : no iniatlization method set."<<endl;
            throw ClassificationTreeNodeException("Clustering : no iniatlization method set.");
        }
        ///------DONE INITIALIZATION------------//
    }
    
    
    //Return nwith there is less than a given number of clusters
    //By using the less than, we can check size of set at every iteration and break out of loop when K is reach
    //Extra cost for the size call and < test, but save traversing whole vector
    bool ClusterFilter::isNumberOfClustersLessThanK(const vector<unsigned int> & k_final, const unsigned int & K )
    {
        //Since only contain unique values, size of set will be number of clusters
        set<unsigned int> k_find;
        for ( vector<unsigned int>::const_iterator i = k_final.begin(); i != k_final.end(); ++i)
        {
            k_find.insert(*i);
            if (k_find.size() == K ) return false;
        }
        
        return true;
        
    }
    
    //return number of clusters in a given vector
    unsigned int ClusterFilter::getNumberOfClusters(const vector<unsigned int> & k_final)
    {
        //Since only contain unique values, size of set will be number of clusters
        set<unsigned int> k_find;
        for ( vector<unsigned int>::const_iterator i = k_final.begin(); i != k_final.end(); ++i)
        {
            k_find.insert(*i);
        }
        
        return k_find.size();
        
    }
    
    
    //This function actually runs the k-means algorithm. Reuires input data, an initial estimate of
    //the cluster means, a vector to store the final classifications, the dimensionality, the number
    //of samples and the number of clusters. k_means will be written over with the final cluster mean
    //estimates. k_final will store the final cluster labels for each sample. The function return the
    //total L2 distance from the point to it's cluster mean.
    //k_final should also contain initialization
    float ClusterFilter::estimate_kmeans( const smatrix & data_in , smatrix & k_means,  vector<unsigned int> & k_final ,  const unsigned int & K)
    {
        //minimal distance for clustering search
        smatrix minDistance;
        
        unsigned int ns = data_in.MRows;
        vector<unsigned int> k_class_prev = k_final; //Will need to store the previous classifications to server as stop criteria
        
        smatrix distance(ns,K); //distance is a matrix containing distances to each centroid, column indexs class
        if (k_final.size() != data_in.MRows)
        {
            cerr<<"ClusterFilter : initilized class labels not equal to the number fo subjects."<<endl;
            throw ClassificationTreeNodeException("ClusterFilter : initilized class labels not equal to the number fo subjects.");
        }
        
        //Calculate the Euclidean distance from each sample to each centroid (class mean)
        //        smatrix_print(distance,"distance1");
//         smatrix_print(k_means,"k-means");
        distance=0;
        smatrix_distEucSq(k_means, data_in, distance);
//                smatrix_print(distance,"distance2");
        
        //classify each sample to belong to the classes of which the centroid is closest.
        //        smatrix_print(k_means,"k-means");
        //
        //        print(k_final,"k_final-init1");
        //
        smatrix_minColumnIndex(distance,k_final);
        
//        print(k_final,"k_final-init2");
        
        //Deals with the case where theres randomly only one cluster
        if (isNumberOfClustersLessThanK(k_final,K))
        {
            k_final = k_class_prev;
            //need to make sure we recalculate means
            smatrix_mean_by_row(data_in,k_means, k_final);
            smatrix_min_byColumn(distance,minDistance);
            return smatrix_sum(minDistance);
            
            
        }
        
        //Start iterative refinement of cluster definitions
        unsigned int iter=0; //used for testing
        //if there's been a change in classification, repeat
        while ( k_final != k_class_prev )
        {
            //Set previous classificaion to current classification.
            //Copying vector contents to avoid reallocation
            memcpy( &(k_class_prev[0]) , &(k_final[0]), sizeof(unsigned int)*ns );
//                        print(k_final,"k-est ");
            
            //check to make sure all labels are present
            
            
            //if the number of unique lables is not equal to K , than proceed to next iteration.
            //also want o revert to the previous class. By doing this current will equal previous
            //thus breaking the loop, whilst still calcuting metrics.
            //            if (k_find.size() < K_ ) {cout<<"conitue "<<endl; k_final = k_class_prev;  }
            if (isNumberOfClustersLessThanK(k_final,K))    k_final = k_class_prev;
            
            
            
            //Calculate class means given the new classifications.
            //            print(k_final,"k_final");
            //            smatrix_print(data_in,"datain");

            smatrix_mean_by_row(data_in,k_means, k_final);

            //Calculate the euclidean distance to each mean.
            smatrix_distEucSq(k_means, data_in, distance);
            //            smatrix_print(distance,"distance ");
            //Update classifications based on closest centroid


            smatrix_minColumnIndex(distance,k_final);
            //            print(k_final,"k-est2 ");
            
            ++iter;
        }
        
        //for final classification calculate means and distance for final clasiciation
        //handles cases that it never enters the loop.
        
        smatrix_mean_by_row(data_in,k_means, k_final);
        //Calculate the euclidean distance to each mean.
        smatrix_distEucSq(k_means, data_in, distance);
        
        //Calculate the distance of the solution
        //need to sum the minimum distances
        //Store distances to each sample's class' centroid.
        smatrix_min_byColumn(distance,minDistance);
        
        //Return the sum of the distances.
        return smatrix_sum(minDistance);
        
    }
    
}
