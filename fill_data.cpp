/*
 *  NormalizeFilter.cpp
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 8/12/10.
 *  Copyright 2010 _Stanford University__. All rights reserved.
 *
 */

#include "fill_data.h"
//#include <misc_utils/misc_utils.h>
#include <misc_utils.h>
#include <cmath>
//#include <math.h>
using namespace std;
using namespace misc_utils_name;

namespace su_mvdisc_name{
	FillDataFilter::FillDataFilter()
	{
		NodeName="FillDataFilter";
		
	}
	
	FillDataFilter::~FillDataFilter(){}
	
	void FillDataFilter::setFillMode( FILL_MODE mode )
	{
		fill_method=mode;
	}
	
	void FillDataFilter::setFillMode( const string & mode )
	{
		if		(	mode == "zero"	)
			setFillMode(FILL_ZERO);
		else if	(	mode == "mean"	)
			setFillMode(FILL_MEAN);
		else {
			throw ClassificationTreeNodeException( ("Unrecognized Fill Mode Specified: "+mode).c_str());
		}

	}
	
	void FillDataFilter::setOptions( std::stringstream & ss_options)
	{
		map<string,string> options = parse_sstream(ss_options);
		for ( map<string,string>::iterator it = options.begin(); it!=options.end();it++)
			if ( it->first == "fillMode")
			{
				setFillMode( it->second );
			}else {
				if (!setTreeNodeOptions(it->first, it->second))
						throw ClassificationTreeNodeException((NodeName + " : Invalid input argument : " + it->first).c_str());
			}

		
	}
	void FillDataFilter::estimateParameters()
	{
        
        if (out_data==NULL)
			out_data = new vector<float>();
		copyToSingleVector( in_data, out_data, NumberOfSamples_ );

        
        if (fill_method == FILL_ZERO)
            fill_values_.assign(Dimensionality_,0);
        else if (fill_method == FILL_MEAN)
        {
            fill_values_.assign(Dimensionality_,0);
            vector<unsigned int> vN(Dimensionality_,0);
	    //            cerr<<"FillDataFilter::estimateParameters - out_data"<<endl;
            unsigned int dim=0;

            vector<float>::iterator i_fill = fill_values_.begin();
            vector<unsigned int>::iterator i_N = vN.begin();
            for (vector<float>::iterator i_out = out_data->begin(); i_out != out_data->end();++i_out,++i_fill,++dim,++i_N)
            {
                if (dim==Dimensionality_)
                {//reset for new sample
                    i_fill = fill_values_.begin();
                    i_N = vN.begin();
                    dim=0;
                }
                //cerr<<*i_out<<endl;
                if ( ! isnan(*i_out)){
                    *i_fill += *i_out;
                    ++(*i_N);
                    
                }//else
		// cerr<<"It is nan "<<endl;

                
            }
            //print(fill_values_,"fill_values ");
	    // print(vN,"vN");

            i_N = vN.begin();
            i_fill = fill_values_.begin();
            for ( ; i_fill != fill_values_.end();++i_fill,++i_N )
            {
                *i_fill /= *i_N;
            }
	    //     print(fill_values_,"fill_values ");
        
        }else
            throw ClassificationTreeNodeException("FillDataFilter::estimateParameters unrecognized fill method");
            
        
        
    }
	void FillDataFilter::applyParametersToTrainingData()
	{
        if (Verbose)
            cout<<"FillDataFilter::applyParametersToTrainingData"<<endl;
	//        print<float>(*out_data,"FillDataFilter::applyParametersToTrainingData-out_data-preFill");
        
        unsigned int dim=0; 
        vector<float>::iterator i_fill = fill_values_.begin();
        for (vector<float>::iterator i_out = out_data->begin(); i_out != out_data->end(); ++i_out,++i_fill,++dim)
        {
            if (dim==Dimensionality_)
            {
                dim=0;
                i_fill = fill_values_.begin();
            }
            if (isnan(*i_out)) *i_out = *i_fill;
        }
	//        print<float>(*out_data,"FillDataFilter::applyParametersToTrainingData-out_data-postFill");

	}
	
	void FillDataFilter::runSpecificFilter()
	{		
		estimateParameters();//does both estimate and write at once
	}
	void FillDataFilter::applyToTestData()
	{
        if (Verbose)
            cout<<"FillDataFilter::applyToTestData"<<endl;
		if (out_test_data == NULL)
			out_test_data = new vector<float>();
		copyToSingleVector( in_test_data, out_test_data, NumberOfTestSamples_ );
        
        
		//        print<float>(*out_test_data,"FillDataFilter::applyToTestData-out_data-preFill");
        
        unsigned int dim=0;
        vector<float>::iterator i_fill = fill_values_.begin();
        for (vector<float>::iterator i_out = out_test_data->begin(); i_out != out_test_data->end(); ++i_out,++i_fill,++dim)
        {
            if (dim==Dimensionality_)
            {
                dim=0;
                i_fill = fill_values_.begin();
            }
            if (isnan(*i_out)) *i_out = *i_fill;
        }
        
	//        print<float>(*out_test_data,"FillDataFilter::applyToTestData-out_data-postFill");


	}
	
    
	
}
