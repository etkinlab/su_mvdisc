#ifndef base_mvdisc_H
#define base_mvdisc_H

/*
 *  base_mvdisc.h
 *  base_mvdisc
 *
 *  Created by Brian Patenaude on 7/14/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


#include <vector>
//#include "Accelerate/Accelerate.h"
//#include "Accelerate/cblas.h"
#include "ClassificationTreeNode.h"

//brian's headers
//#include "misc_utils.h"

#include "target.h"
#include "data_source.h"
#include <clapack_utils.h>
namespace su_mvdisc_name{


/* The classes below are exported */
#pragma GCC visibility push(default)

	class Target;

	class base_mvdisc : public ClassificationTreeNode
{
	
	
	public:
		base_mvdisc();
		virtual ~base_mvdisc();
		virtual void estimateParameters();
		virtual void runNode( bool with_estimation = true);
    // virtual void setOptions(  std::stringstream & ss_options );
	//std::vector<float> getDistanceScores();
    void setDoGeneralizationAccuracy( const bool & state );


    bool getDoGeneralizationAccuracy() { return DoGenAcc_; }
    
    
	void setClassifyTrainingData( const bool & state );
	void setClassifyTestData(  const bool & state );
	//protected:
	//void setInputData( ClassificationTreeNode & data_input );
//	void setInputAndLeftOutData( );

	//void setTrainingData( const std::vector< const std::vector<float>* > & data , const std::vector<int> & target, const std::vector<unsigned int> & M, const unsigned int & N );
	//void setTrainingData( const std::vector< const std::vector<float>* > & data , const std::vector<int> & target, const std::vector<unsigned int> & M, const unsigned int & N , const std::vector<unsigned int> & selection_mask);

//	void displayTrainingData();
    void generalizationAccuracy( );
    TpFpTnFn generalizationAccuracyKfold(  );

	virtual void setParameters();
//	virtual int classify( const std::vector<float> & test_vec ) const ;
	virtual int classifyTrainingData( ) ;
	virtual int classifyTestData() ;

	int classify() ;
	//int classify(const std::vector<float> & vec);

	//virtual std::vector<int> classifyLeftOutData();

    //intended for discriminants
    TpFpTnFn getGeneralizationData();
    clapack_utils_name::smatrix getGeneralizationDataThresh();

    std::vector< TpFpTnFn > getGeneralizationDataLevel2();
    
protected:
	//std::vector<float>* training_data;

	//bool hasEstimateParameters;
	
	//std::vector<int> training_target;
	//, target_class_index,NperClass;
	//std::vector<int> vClasses;
	//std::vector<unsigned int> v_D;//stores dimensionality of each source
	//std::vector<float>* disc_distance; 

private:
	//float genAcc_;
    std::vector<TpFpTnFn> genData_l2_;
    TpFpTnFn genData_;
//    std::vector< std::vector<float> >
    clapack_utils_name::smatrix genData_BagThreshXBagModel_;
    
    float model_thresh_l_, model_thresh_u_;
    bool do_applyToTestData_, do_applyToTrainingData_;

	
};

#pragma GCC visibility pop
	
}
	
#endif
