/*
 * corrp.cpp
 *
 *  Created on: Aug 21, 2015
 *      Author: Brian Patenaude
 *      Calculates Pearson's correlation from two input text files.
 *
 */

//STL includes
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <misc_utils.h>
using namespace std;

int Usage(){
	cout<<"\n corrp <file1> <file2> \n "<<endl;
	cout<<"           file1 and file1 are single row/column ASCII files. They should be the same length. \n "<<endl;
	return 1;
}

//read in vector of data from a file
vector<double> readVector( char* filename )
{
	vector<double> data;
		ifstream fin(filename);
		if (! fin.is_open()) return data;

		string line;
		while (getline(fin,line))
		{
			stringstream ss(line);
			double fval;
			while (ss >> fval)
			{
				data.push_back(fval);
			}
		}


		fin.close();

		return data;

}

int main(int argc, char* argv[])
{
	if ( argc != 3 ) return Usage();

	vector<double> data1 = readVector(argv[1]);
	vector<double> data2 = readVector(argv[2]);

	if ( (data1.size()) != (data2.size()))
	{
		cerr<<"Vector sizes do not match!"<<endl;
		exit (EXIT_FAILURE);
	}

//	auto i_1 = data1.begin();
//	for (auto i_2 = data2.begin(); i_2 != data2.end(); ++i_2,++i_1)
//	{
//		cout<<(*i_1)<<" "<<(*i_2)<<endl;
//
//	}
	//compute Pearson's correlation
	double r = misc_utils_name::correlation(&(data1[0]), &(data2[0]), data1.size(),1);

	cout<<r<<endl;
}



