/*
 *  quantile_regression.h
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 10/19/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

//only does bivariate at the moment
#include <base_image_filter.h>
#include <smatrix.h>

#include <vector>
namespace su_mvdisc_name{
	
    
    
    
	class QuantileRegression : public base_image_filter
	{
        enum QUANT_OUTPUT { ALL_QUANTILES, CLOSEST_QUANTILE, MEDIAN };
    
	public:
		QuantileRegression();
		virtual ~QuantileRegression();
		void setOptions( std::stringstream & ss_options);
        void setThreshold( const float & thresh ){_threshold=thresh; _binarize_output=true; }
        void setBinarize( const bool & binarize) { _binarize_output = binarize;}
        void setTau( const float & tau ) { _tau.clear(); _tau.push_back(tau); }
        void setOutputFormat( const QUANT_OUTPUT & qout ) { _out_format = qout; }
		void applyToTestData();
        float gridSearch( const std::vector<float> & v_x, const std::vector<float> & v_y,const float & tau, const float & B0min, const float & B0max, const float & B1min, const float & B1max, const float & stepB0, const float & stepB1, float & B0opt, float & B1opt  );
        void runNode( bool with_estimation=true );

        std::vector<float> getParameters();
        std::vector<float> getOLSParameters();
        std::vector<float> getQuantiles() { return _tau; } 
		void estimateParameters();
        void estimateTargetQuantiles( const std::vector<float> & targ_data );
        void estimateParametersSimplex(const std::vector<float> & v_indata, const std::vector<float> & targ_data);
        void estimateParametersGradient();

		void applyParametersToTrainingData();
		
	protected:
        void runTrackStats();
        static float quantile( const std::vector<float> & data, const float & tau );
        
	private:
        
        
        std::pair<float,unsigned int> lineSearch1D( const clapack_utils_name::smatrix & x, const clapack_utils_name::smatrix & y ,const clapack_utils_name::smatrix & point_indices , const float & tau) const ;
        float _MACH_EPS; //machine epsilon
		bool _InvertModel;
        bool _binarize_output;
        bool _useOLS;
        float _threshold;
        bool _out_format;
//        float _tau;
        std::vector<clapack_utils_name::smatrix> _vbh;

        clapack_utils_name::smatrix _ols_params;
        float _tau_res,_tau_min,_tau_max;

        unsigned int _Ntau;
        
        std::vector<float> _tau,_targ_quantiles;//, _vb0_opt, _vb1_opt;
    };
	
}
