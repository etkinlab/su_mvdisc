/*
 *  NormalizeFilter.cpp
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 8/12/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
//its own header
#include <cstdlib>

#include "pca_filter.h"


#include <string.h>
//STL includes
#include <algorithm>


//Custom includes
//#include <misc_utils/misc_utils.h>
//#include <clapack_utils/clapack_utils.h>
#include <misc_utils.h>
#include <clapack_utils.h>
//3rd party include 

//OSX include
#ifdef ARCHLINUX
extern "C" {
#include "cblas.h"
#include "f2c.h"
  //typedef long int __CLPK_integer;                                                                                                                
  //typedef float  __CLPK_real;                                                                                                                     

  typedef integer __CLPK_integer;
  typedef real __CLPK_real;
#include "clapack.h"
}

#else

#include <Accelerate/Accelerate.h>
#endif

using namespace std;
using namespace misc_utils_name;
using namespace clapack_utils_name;

namespace su_mvdisc_name{
	PCA_Filter::PCA_Filter()
	{
		NodeName="PCA_Filter";
		D	 =	NULL;
		cumD2 =	NULL;
		U	 =	NULL;
		VT	 =	NULL;
		work =	NULL;
		mean = NULL;
		totalD2=0.0;
		DimTrim=0;
		varThresh=0.95; //specified as percentage of total variance
	}
	
	PCA_Filter::~PCA_Filter()
	{
		if (work != NULL)
			delete[] work;
		if (D != NULL)
			delete[] D;
		if (cumD2 != NULL)
			delete[] cumD2;
		if (U != NULL)
			delete[] U;
		if (VT != NULL)
			delete[] VT;
		if (mean != NULL)
			delete[] mean;
	}
	
			
	void PCA_Filter::setOptions( std::stringstream & ss_options)
	{
		map<string,string> options = parse_sstream(ss_options);
		for ( map<string,string>::iterator it = options.begin(); it!=options.end();it++)
			if ( it->first == "threshold")
			{
				stringstream ss;
				ss<<it->second;
				ss>> varThresh;
			}else {
				if (!setTreeNodeOptions(it->first, it->second))
					throw ClassificationTreeNodeException((NodeName + " : Invalid input argument : " + it->first).c_str());
			}

	}
	
	void PCA_Filter::estimateParameters()
	{
	//	cout<<"estimate PCA"<<endl;
		
		//get data from all sources
		if (out_data == NULL)
			out_data = new vector<float>();
		
		copyToSingleVector( in_data, out_data, NumberOfSamples_ );
		//print<float>(*out_data,"input to pca");
		char jobu = 'S';
		char jobvt = 'O';
        //I think assume Row-Major order hence swapping M and N
		__CLPK_integer M = static_cast<__CLPK_integer> (Dimensionality_);
		__CLPK_integer N = static_cast<__CLPK_integer> (NumberOfSamples_);
		__CLPK_integer lwork =	calculate_lworkGESVD(M,N);
		__CLPK_integer minMN = (M<N) ? M : N ; 
		__CLPK_integer ldvt = minMN ;
		__CLPK_integer ldu = M ;
		__CLPK_integer lda = M ;
		__CLPK_integer info;
		
		if (work != NULL)
			delete[] work;
		work = new float[lwork];
		
		if (D != NULL)
			delete[] D;
		D = new float[minMN];
		
		if (cumD2 != NULL)
			delete[] cumD2;
		cumD2 = new float[minMN];
		
		if (U != NULL)
			delete[] U;
		U = new float[M*minMN];
		
		if (mean != NULL)
			delete[] mean;
		mean = new float[Dimensionality_];
		memset(mean, 0, Dimensionality_*sizeof(float));
		
		
		//calculate summation over subjects 
		for ( unsigned int samp = 0 ; samp < NumberOfSamples_; ++samp )
			cblas_saxpy(Dimensionality_, 1.0, &(out_data->at(0))+Dimensionality_*samp, 1, mean, 1);

		//divide by number of samples (to calculate mean
		cblas_sscal(Dimensionality_, 1.0/static_cast<float>(NumberOfSamples_), mean, 1);
		
		//subtract teh mean from the data 
		for ( unsigned int samp = 0 ; samp < NumberOfSamples_; ++samp )
			cblas_saxpy(Dimensionality_, -1.0, mean, 1, &(out_data->at(0))+Dimensionality_*samp, 1);

//		cout<<"demean pca"<<endl;
//		for ( unsigned int samp = 0 ; samp < NumberOfSamples_; ++samp )
//		{	
///			for (unsigned int dim = 0 ; dim<Dimensionality_; ++dim)
	//	{
	//			cout<<out_data->at(Dimensionality_*samp + dim)<<" ";
	//	}
	//		cout<<endl;
	//	}
		
		//calculate SVD
		sgesvd_( &jobu, &jobvt, &M , &N,  (&(out_data->at(0))), &lda, D, U, &ldu, VT, &ldvt, work, &lwork, &info );
	
		//claculate the total variance, and evalute D*V' 
		totalD2=0;
		for (__CLPK_integer i =0 ; i<minMN ; i++)
		{
			//This implents D*V'
			cblas_sscal(N, D[i], &(out_data->at(0))+i, minMN);
			
			totalD2+=D[i]*D[i];
			//cumulate the variance
			cumD2[i] = totalD2;
		}
		 if (totalD2==0)
			 throw ClassificationTreeNodeException("Zero Variance in the Training Data Input to the PCA filter.");
		
		
		//by scaling becomes the fraction of variance for each eigenvector
		cblas_sscal(minMN, 1.0/totalD2, cumD2, 1);

	}
	
	void PCA_Filter::applyParametersToTrainingData()
	{
		//estimates 
		//cout<<"apply PCAto training "<<endl;
		vector<float> temp_data;
		copyToSingleVector( in_data, &temp_data, NumberOfSamples_ );
		out_data->resize(temp_data.size());
		
		unsigned int minMN = (Dimensionality_<NumberOfSamples_) ? Dimensionality_ : NumberOfSamples_ ; 
		
		unsigned int NumEigs=0;
		for (unsigned int i =0 ; i<minMN; ++i)
		{
			//need at leats one component			

			NumEigs++;
			if ( cumD2[i] > varThresh )
				break;
		}
	
		//------DVt is already stored in output, eleminate truncated eigvectors----------//	
		float* out_reduced = new float[NumEigs*NumberOfSamples_];
		for ( unsigned int i = 0 ; i < NumEigs ; ++i)
			cblas_scopy(NumberOfSamples_, &(out_data->at(0))+i,minMN, out_reduced+i, NumEigs);
		
		
		memcpy(&(out_data->at(0)), out_reduced, NumEigs*NumberOfSamples_*sizeof(float));
		out_data->resize(NumEigs*NumberOfSamples_);

		

		delete[] out_reduced;
		
		//adjust the Dimensionality_, dimTrim used for apply to test data
		DimTrim = Dimensionality_ - NumEigs;
		Dimensionality_ = NumEigs;

		cout<<"PCA applied to training data. Retained "<<NumEigs<<" eigenvectors. Retained "<<cumD2[NumEigs-1]*100<<"% of the total variance."<<endl;
	}

	
	void PCA_Filter::applyToTestData()
	{
	//	cout<<"apply PCA to test "<<NumberOfTestSamples_<<endl;

		vector<float> test_data;
		//this will allocate 
		copyToSingleVector( in_test_data, &test_data, NumberOfTestSamples_ );
	
		//needs to know the original Dimensionality_ of teh data
		unsigned int DimOrg = Dimensionality_+DimTrim;
		//----------DEMEAN TEST DATA -------------------//
		
		//-------Do demeaning, using mean calulated from training data 
		for ( unsigned int samp = 0 ; samp < NumberOfTestSamples_; ++samp )
		{
			cblas_saxpy(DimOrg, -1.0, mean, 1, &(test_data[0])+DimOrg*samp, 1);
		}
	//	print<float>(test_data,"PCA demeaned ");
		
		
		//----------end DEMEAN TEST DATA -------------------//

		
		
		//potentially reduced Dimensionality_
		
		if (out_test_data == NULL)
			out_test_data = new vector<float>();
		out_test_data->resize(NumberOfTestSamples_*Dimensionality_);

		
		
		
		cblas_sgemm(CblasColMajor, CblasTrans , CblasNoTrans, Dimensionality_, NumberOfTestSamples_, DimOrg, 1.0, \
					U , DimOrg, &test_data[0], DimOrg, \
					0.0, &out_test_data->at(0),Dimensionality_);
		
	//	print<float>(*out_test_data,"PCA applied to test data");
		
	}
	
		
	
}
