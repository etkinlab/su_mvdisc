//
//  qda.cpp
//  su_mvdisc
//
//  Created by Natasha Parikh on 5/30/12.
//  Copyright (c) 2012 Stanford University - Etkin Lab. All rights reserved.
//

#include <iostream>

#include "qda.h"
#include <misc_utils.h>

//OSX include
//#include <vecLib/cblas.h>
//#include <vecLib/clapack.h>
#ifdef ARCHLINUX
extern "C" {
#include "cblas.h"
#include "f2c.h"
    typedef integer __CLPK_integer;
    typedef real __CLPK_real;
#include "clapack.h"
}
#else
//#endif
//#ifdef ARCHDARWIN

#include <Accelerate/Accelerate.h>

#endif


//STL includes
#include <cmath>

using namespace std;
using namespace misc_utils_name;
using namespace clapack_utils_name;


namespace su_mvdisc_name
{
	
	QDA_mvdisc::QDA_mvdisc()
        : ZERO_THRESH(3e-6), frac_var_thresh(1), cl_prior_choice(SAMPLE_PROB)
	{
        NodeName="QDA_Discriminant";
        nodeTypeSpec=QDA;

	}
	
	
	QDA_mvdisc::~QDA_mvdisc()
	{
      // Nothing to do here!  
    }
    
    void QDA_mvdisc::setOptions( std::stringstream & ss_options)
	{
		
		//-------BEFORE SETTING CUSTOM OPTIONS RESET TO DEFAULT----------//
        
		//---------------SET CUSTOM OPTIONS ------------------//
		
		
		map<string,string> options = parse_sstream(ss_options);
		for ( map<string,string>::iterator it = options.begin(); it!=options.end(); it++)
		{
			//cout<<"set svm option "<<it->first<<" "<<it->second<<endl;
			
			if ( it->first == "Prior") {
				if (it->second == "SampleProb")
					cl_prior_choice = SAMPLE_PROB;
				else if (it->second == "EqualProb")
					cl_prior_choice = EQUAL_PROB;
				//cout<<"svm_type "<<libsvm_parameter.svm_type<<endl;
			} 
            
            if ( it->first == "FracVariance") {
                stringstream ss; 
                ss<<it->second;
                ss>>frac_var_thresh;
            } else {
				if (!setTreeNodeOptions(it->first, it->second))
                    throw ClassificationTreeNodeException((NodeName + " : Invalid input argument : " + it->first).c_str());
			}
		}
	}

    
    void QDA_mvdisc::estimateParameters() 
    {
//        cerr << "**********************************ULTIMATE TEST*************************************" << endl;
//        float* sdata = new float[1*1];
//        if (sdata != NULL) {
//            cerr << "are you in here?" << endl;
//            delete sdata;
//        }
//        cerr << "*********************************WAS THERE AN ERROR?****************************************" << endl;
        
        if (Verbose)
            cout<<"estimate QDA PARAMS"<<endl;
        
        // need to set data on device		
		vector<unsigned int> NperClass = training_target->getNumberOfSubjectsPerClass();
        
        //cerr<<"QDA: estimate params: got subjects per class"<<endl;

        
		// scaling factor for calculating mean
        
        class_priors.resize(Kclasses_);
        
        //cerr << "QDA: estimate params: just resized class priors" << endl;

        dataVec_.resize(Kclasses_);

        
        //cerr << "QDA: estimate params: resized dataVector" << endl;
        
        eigenSums_.resize(Kclasses_);
        //cerr << "QDA: estimate params: resized vectors" << endl;
        
		vector<float> scale;	
        vector< smatrix >::iterator i_data_vec = dataVec_.begin();
     //   print(NperClass,"Nperclass");
        unsigned int class_ind=0;
		for (vector<unsigned int>::iterator i = NperClass.begin(); i != NperClass.end(); ++i, ++i_data_vec,++class_ind) 
        {
			scale.push_back(1.0 / static_cast<float>(*i));
//            cerr<<"QDA: estimate params: added to scale vector"<<endl;

            if (Verbose)
            {
                cout << "NperClass: " << *i << endl;
                cout << "Number of samples: " << NumberOfSamples_ << endl;
            }            
			if (cl_prior_choice == EQUAL_PROB ) {
				class_priors[class_ind]=(1.0 / Kclasses_);
			} else {                                    // default to SAMPLE_PROB
				class_priors[class_ind]=(static_cast<float>(*i) / NumberOfSamples_ );
			}
            //  cerr<<"resize data"<<endl;
            // create the matrices in the data vector to the correct size
            //  *i_data_vec = smatrix(*i, Dimensionality_);
            i_data_vec->resize(*i,Dimensionality_);
            
		}
//        print(class_priors,"class priors");
 
        // copy training data to device
        // use column major format
        smatrix data(NumberOfSamples_, Dimensionality_);
		copyToSingleVectorTranspose(in_data, data.sdata, NumberOfSamples_);

        // create the matrix that keeps track of the means
        smatrix mu_sphere(Kclasses_, Dimensionality_);
        smatrix_set(mu_sphere,0);   
        
        vector<int> rowsInserted(Kclasses_, 0);  // keeps track of how many rows have been inserted into each class's matrix
        unsigned int count = 0;                 // keeps track of the row of data we are looking at now
        
        // create the matrices of data, separated by class
        // also, update the means of each class
        
        for (vector<int>::iterator class_iter = training_target->class_index_begin(); 
             class_iter != training_target->class_index_end(); class_iter++, count++) 
        {
            //smatrix_setRow(dataVec_[*class_iter], rowsInserted[*class_iter], data, count);
            smatrix_setRow(dataVec_[*class_iter], rowsInserted[*class_iter], data, count);

            smatrix_addRow2Row(data, count, mu_sphere, *class_iter);

            ++rowsInserted[*class_iter];
        }
        //cout<<endl;
               
		// divide by number of sample, scale is not used as accumulating means because of precision issues
		for (unsigned int i = 0; i < Kclasses_; i++)
            smatrix_rowScale(mu_sphere, i, 1.0f / NperClass[i]);
               
        // display class means
        // demean the data (by class specific means)
		for (count = 0; count < Kclasses_; count++) {
            smatrix_scaleAndAddRow2Matrix(mu_sphere, count, -1, dataVec_[count]);
        }
       
        if (Verbose)
            cout<<"QDA: estimate params: about to find svd for each class"<<endl;

        // find the SVD for each class's data individually
        __CLPK_integer minMN;
        smatrix DxUt, D, U, Vt, singleClassMu;
        count = 0;
        DxUtVec_.resize(Kclasses_);
        //cout<<"DxUtVecSize "<<DxUtVec_.size()<<endl;
        muVec_.resize(Kclasses_);
        //cout<<"muvecsizeest "<<muVec_.size()<<endl;
        for (vector<smatrix>::iterator i_mat = dataVec_.begin(); i_mat != dataVec_.end(); i_mat++, count++)
        {
            //cout<<"QDA estimate class "<<count<<" info..."<<endl;
            minMN = (static_cast<unsigned int>(i_mat->MRows) < Dimensionality_) ? i_mat->MRows : Dimensionality_;
            DxUt.resize(minMN, Dimensionality_);
                        
            //After svd DxUt store Ut
            __CLPK_integer info = smatrix_svd_of_transpose(*i_mat, DxUt, D, 'S');
                       
            if (! (info==0) ) {
                stringstream ss; 
                ss<<info;
                string sinfo;
                ss>>sinfo;
                throw ClassificationTreeNodeException( ("SVD error in estimating QDA a parameters " + sinfo).c_str() );
            }
//           / smatrix_print(D, "D");
            float sumD = 0.0f;  // find the sum of the logs of the (positive) eigenvalues
            
            // threshold
            // do if keeping less than 100% of variance
            if ( frac_var_thresh < 1 ) {
                float total_var  =  cblas_sdot(minMN, D.sdata, 1, D.sdata, 1);
               
                if (Verbose)
                    cout << "Total Variance " << total_var << endl;
                
                //zero thresh is actually on singular values
                float var_thresh = (total_var * frac_var_thresh);
                
                // D is singular values not eigenvalues
                // invert D
                float cum_var=0;
                for (__CLPK_integer row = 0; row < minMN; ++row)
                {
                    cum_var += D.sdata[row] * D.sdata[row];
                    
                    if (D.sdata[row] != 0.0f)
                        sumD += (cum_var < var_thresh) ? ( (D.sdata[row] > ZERO_THRESH) ? logf(pow(D.sdata[row],2)) : 0 ) : 0;
                    
                    //if cumulative variance is less then total being kept and singular value is above zero threshold
                    D.sdata[row]  = (cum_var < var_thresh) ? ( (D.sdata[row] > ZERO_THRESH) ? 1.0f/D.sdata[row] : 0 ) : 0; 
                }
                
            } else {
                // D is singular values not eigenvalues
                // invert D
                for (__CLPK_integer row = 0 ; row < minMN; ++row) 
                {
                    if (D.sdata[row] != 0.0f)
                        sumD += (D.sdata[row] > ZERO_THRESH) ? logf(pow(D.sdata[row],2)) : 0;
                    
                    D.sdata[row]  = (D.sdata[row] > ZERO_THRESH) ? 1.0f / D.sdata[row] : 0; 
                }
                                    
            }
                     
            eigenSums_[count]=(sumD);
//            print(eigenSums_,"eigensums");
         
            for (__CLPK_integer row = 0 ; row < D.MRows; ++row)
                smatrix_rowScale(DxUt, row, D.sdata[row]);
                        
            DxUtVec_[count] = DxUt;
            
            //cerr << "does this exist? " << DxUtVec_[count].sdata[0] << endl;
            
            singleClassMu.resize(1, Dimensionality_);
            for (unsigned int pos = 0; pos < Dimensionality_; pos++)
                singleClassMu.sdata[pos] = mu_sphere.sdata[Kclasses_ * pos + count];
                        
            smatrix_BxAt(singleClassMu, DxUt);
//            smatrix_print(singleClassMu, "singleClassMu");
            muVec_[count] = singleClassMu;

            
        } 
        
        if (Verbose)
            cout << "estimate QDA PARAMS end" << endl;
//        cerr << "estimate QDA PARAMS end" << endl;

    }
    
    
    
    void QDA_mvdisc::setParameters()
    {
        // No need to do anything here!
    }
	
    
	int QDA_mvdisc::classifyTrainingData()
	{
        
		//DimTrim = Dimensionality_ -1;
		//Dimensionality_ = 1;
		if (Verbose)
            cout << "QDA_mvdisc : classify training start" << endl;
        
        if (in_data.empty())
            throw ClassificationTreeNodeException("Trying to classify training using QDA , but no data to classify exists.");
        
        smatrix target(NumberOfSamples_,Dimensionality_); // target to classify
     //   smatrix dist(NumberOfSamples_,Dimensionality_);   // distance in each dimension from sphered mean
        
        // load training data into target matrix 
        copyToSingleVectorTranspose(in_data, target.sdata, NumberOfSamples_);
   //     smatrix_print(target, "training_data_to_classify");
     //   cout<<"muVecsize "<<muVec_.size()<<endl;

        //class labels
        vector<int> cl_labels = training_target->getClassLabels();
//        pair<unsigned int,float> base(0,2e10);
        vector< pair<unsigned int,float> > v_dist(NumberOfSamples_);
        
        for (unsigned int class_ind = 0; class_ind < Kclasses_; class_ind++)
        {
            // copy the training data
            smatrix diff(target.MRows, target.NCols); // f=target
            memcpy(diff.sdata, target.sdata, target.MRows * target.NCols * sizeof(float));
            
            // sphere the copied data
            smatrix_BxAt(diff, DxUtVec_[class_ind]);
        //    smatrix_print(diff, "Sphered training target");
            
            smatrix_scaleAndAddRow2Matrix(muVec_[class_ind], 0, -1.0, diff);
            
       //     smatrix_print(muVec_[class_ind], "mu's");
         //   smatrix_print(diff, "Scaled training");
            
            for (__CLPK_integer i = 0; i < diff.MRows; ++i)
            {
                // distance modulo prior
                //cerr << "class priors " << class_priors[i] << endl;
                //cerr << "size " << class_priors.size() << endl;
                float dist = eigenSums_[class_ind] + cblas_sdot(diff.NCols, diff.sdata + i, diff.MRows, diff.sdata + i, diff.MRows) - 2.0 * logf(class_priors[class_ind]);
             //   dist = cblas_sdot(diff.NCols, diff.sdata + i, diff.MRows, diff.sdata + i, diff.MRows) - 2.0 * logf(class_priors[class_ind]);
                
        //        cerr<<"dcompare1 "<<class_ind<<" "<<i<<" "<<v_dist[i].second<<" "<<dist<<endl;

                if ( class_ind == 0 )
                {
          //          cout<<"domcpare0 "<<endl;
                    v_dist[i].first = class_ind;
                    v_dist[i].second = dist;
                }else if (v_dist[i].second > dist) 
                {
                    v_dist[i].first = class_ind;
                    v_dist[i].second = dist;

                }
            //    cerr<<"dcompare2 "<<class_ind<<" "<<i<<" "<<v_dist[i].second<<" "<<dist<<endl;

            }
        }
        
        
        for (vector< pair<unsigned int, float> >::iterator i_dist = v_dist.begin(); i_dist != v_dist.end();++i_dist)
            out_data->push_back(static_cast<float> (cl_labels[i_dist->first]));
        
       // print(*out_data,"training classification");
        
        if (Verbose)
            cout << "QDA_mvdisc : classify training end" << endl;
        
        return 0;
	}
    
	
    
    
    int QDA_mvdisc::classifyTestData()  
    {
        
        if (Verbose)
        cout << "QDA_mvdisc : classify test start" << endl;
        
        if (in_test_data.empty())
            throw ClassificationTreeNodeException("Trying to classify using QDA , but no data to classify exists.");
        
        smatrix target(NumberOfTestSamples_,Dimensionality_); //target to classify

        // load test data into target matrix 
        copyToSingleVectorTranspose(in_test_data, target.sdata, NumberOfTestSamples_);
      //  smatrix_print(target, "target_to_classify");
        
        //class labels
        vector<int> cl_labels = training_target->getClassLabels();
       // pair<unsigned int,float> base(0,2e10);
        vector< pair<unsigned int,float> > v_dist(NumberOfTestSamples_);
     //   cout<<"Kclasses "<<Kclasses<<endl;
        for (unsigned int class_ind = 0; class_ind < Kclasses_; class_ind++)
        {
           // cout<<"class ind "<<class_ind<<endl;
            smatrix diff(target.MRows, target.NCols); // f=target
            memcpy(diff.sdata, target.sdata, target.MRows * target.NCols * sizeof(float));
    //        cout<<"class "<<class_ind<<endl;
      //      smatrix_print(DxUtVec_[class_ind],"DxUT");
            //sphere test data
            smatrix_BxAt(diff, DxUtVec_[class_ind]);
     //       smatrix_print(diff, "Sphered target");
        //    cout<<"class "<<class_ind<<endl;
        //    smatrix_print(muVec_[class_ind], "mu");
            
            smatrix_scaleAndAddRow2Matrix(muVec_[class_ind], 0, -1.0, diff);
//cout<<"class "<<class_ind<<endl;
//
            //smatrix_print(muVec_[class_ind], "mu's");
        //    smatrix_print(diff, "Scaled target diff");
            
            for (__CLPK_integer i = 0; i < diff.MRows; ++i)
            {
                // distance modulo prior
                float dist = eigenSums_[class_ind] + cblas_sdot(diff.NCols, diff.sdata + i, diff.MRows, diff.sdata + i, diff.MRows) - 2.0 * logf(class_priors[class_ind]);
                
               // cerr << "test distances " << dist << eigenSums_[class_ind] << " "<< cblas_sdot(diff.NCols, diff.sdata + i, diff.MRows, diff.sdata + i, diff.MRows) <<" "<< - 2.0 * logf(class_priors[class_ind]) <<endl;
               // cerr<<"dcomparetest1 "<<i<<" "<<v_dist[i].second<<" "<<dist<<endl;

                if ( class_ind == 0 )
                {
                //    cout<<"domcpare0 "<<endl;
                    v_dist[i].first = class_ind;
                    v_dist[i].second = dist;
                }else if (v_dist[i].second > dist) {
                    //cerr << "in the if statement!" << endl;
                    v_dist[i].first = class_ind;
                    v_dist[i].second = dist;
                }
              //  cerr<<"dcomparetest2 "<<i<<" "<<v_dist[i].second<<" "<<dist<<endl;

            }
            
        }
               
        for (vector< pair<unsigned int, float> >::iterator i_dist = v_dist.begin(); i_dist != v_dist.end();++i_dist)
            out_test_data->push_back(static_cast<float> (cl_labels[i_dist->first]));
        
     //   print(*out_test_data,"QDA out test data");
        
       if (Verbose)
        cout<<"QDA_mvdisc : classify test end"<<endl;
        
       //exit (EXIT_FAILURE);

        return 0;
        
    }    
    
}

