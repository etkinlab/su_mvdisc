-#!/bin/sh

#  discriminate_permutations.sh
#  su_mvdisc
#
#  Created by Brian Patenaude on 9/26/11.
#  Copyright 2011 Stanford University - Etkin Lab. All rights reserved.

Usage(){
echo ""
echo "discriminate_permutations [-run_missing] <output directory> <Number of Permutations>"
echo ""

exit 1
}

#total number of permuations
NPERMS=0
#what number to start at
NPERMS_OFFSET=0
RUN_MISSING=0
DISC_DIR=""
BAGGING_ARGS=""

if [ $1 = -run_missing ] ; then 
RUN_MISSING=1
shift 1
fi

if [ $# -ne 2 ] ; then 
echo ""
echo "Invalid number of input : $#"
echo "\n\n"
echo "-run_missing : reruns missing permutations"
Usage
fi



DISC_DIR=$1
shift 1
NPERMS=$1
shift 1


if [ ! -d $DISC_DIR ] ; then 
echo ""
echo "$DISC_DIR does not exist"
Usage  
fi

if [ $NPERMS -le 0 ] ; then
echo ""
echo "Invalid number of permutations : $#"
Usage
fi

DISC_CONFIG_FILE=${DISC_DIR}/config.txt
NSUBSAMPLES=`cat $DISC_CONFIG_FILE | sed -n '1p' | awk '{ print $2 }'`
MINOFFSET=`cat $DISC_CONFIG_FILE | sed -n '2p' | awk '{ print $2 }'`
LeaveN=`cat $DISC_CONFIG_FILE | sed -n '3p' | awk '{ print $2 }'`
BAGGING=`cat $DISC_CONFIG_FILE | sed -n '4p' | awk '{ print $2 }'`
if [ $BAGGING = 1 ] ; then 
BAGGING_ARGS="--useBagging"
fi

INFO_FILE=${DISC_DIR}/info.txt
echo "Discriminant Information File" >  $INFO_FILE

#target file (i.e. class labels)
TARGET_FILE=${DISC_DIR}/target.txt 

#group map file
GROUP_MAP=${DISC_DIR}/group_map.txt

#CLASS MAP
CLASS_MAP=${DISC_DIR}/class_map.txt
INTERP_MAP=${DISC_DIR}/interp_class_map.txt
SENS_FILE=${DISC_DIR}/sens.txt
#disriminant tree
TREE_FILE=${DISC_DIR}/tree.txt
LOG_FILE=${DISC_DIR}/perms.log
SGECOM_FILE=${DISC_DIR}/perms_interpret_sge

#want a new a unique command file
#echo "start while"
#while [ -f ${SGECOM_FILE}.com ] ; do 
#SGECOM_FILE=${SGECOM_FILE}+
#done

#echo "end while $SGECOM_FILE"
#SGECOM_FILE=${SGECOM_FILE}.com


REG_FILE=${DISC_DIR}/regressors.txt


DISCOUT_DIR=${DISC_DIR}/Leave-${LeaveN}-Out.perms


PERM_LIST=${DISCOUT_DIR}/perms.list




echo "JOBID : $ID"
ls ${DISCOUT_DIR}/cv_results_disc_perm*_per_subject_stats_predX.csv > $PERM_LIST
echo "done perms list"
N=`wc $PERM_LIST | awk '{ print $1 }'`
fsl_sub -N Interpret-Perms${N} -q matlab.q ${ETKINLAB_DIR}/bin//discriminate_interpreter -i ${PERM_LIST} --inputType=1 ${BAGGING_ARGS} --class_map=$INTERP_MAP --sens_spec_class_inclusion=$SENS_FILE --p_sig=0.05 -o ${DISCOUT_DIR}/results_${N}perms_p0.05.csv


#   echo "~/src/etkinlab_utils/src/su_mvdisc/build/Release/discriminate_interpreter -i ${PERM_LIST} --inputType=1 ${BAGGING_ARGS} --class_map=$INTERP_MAP --sens_spec_class_inclusion=$SENS_FILE --p_sig=0.05 -o ${DISCOUT_DIR}/results_${N}perms_p0.05.csv" >> $LOG_FILE

#   ~/src/etkinlab_utils/src/su_mvdisc/build/Release/discriminate_interpreter -i ${PERM_LIST} --inputType=1 ${BAGGING_ARGS} --class_map=$INTERP_MAP --sens_spec_class_inclusion=$SENS_FILE --p_sig=0.05 -o ${DISCOUT_DIR}/results_${N}perms_p0.05.csv

