#ifndef all_nodes_H
#define	all_nodes_H
/*
 *  all_nodes.h
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 8/24/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
//data sources
#include <image_source.h>
#include <TextFileSource.h>

//filters
#include <dimension_select_filter.h>
#include <fill_data.h>
#include <NormalizeFilter.h>
#include <pca_filter.h>
#include <glm_filter.h>
#include <group_select_filter.h>
#include <rv_transform.h>
#include <quantile_regression.h>
//discriminants
#include <lda.h>
#include <qda.h> 
#include <classifierFusion.h> 
#include <decisionTree.h>

#include <log_regression.h>
#include <svm_disc.h>
#include <gp_filter.h>
#include <cluster_filter.h>

////#include <booster.h>
//wrappers
#include <reliability_filter.h>
#include <adaBoost.h>
//#include <resample_node.h>


#endif
