//
//  log_regression.h
//  su_mvdisc
//
//  Created by Natasha Parikh on 6/5/12.
//  Copyright (c) 2012 Stanford University - Etkin Lab. All rights reserved.
//

#ifndef su_mvdisc_log_regression_h
#define su_mvdisc_log_regression_h 1 

#include <vector>
#include <string>
#include <sstream>
#include "base_mvdisc.h"
#include <smatrix.h>

namespace su_mvdisc_name{
 
    typedef clapack_utils_name::smatrix smatrix; 
    
    class log_regression_mvdisc : public base_mvdisc 
    {
        
    public:        
        // Constructor and destructor
        log_regression_mvdisc();
        ~log_regression_mvdisc();
        
        // setOptions
        // Parses a stringstream of options, puts them into a map, updates
        // appropriate data members, and throws an error if the options 
        // are not recognized.
        void setOptions(std::stringstream & ss_options);
        
        
        // estimateParameters
        // Completes the iteratively reweighted least squares method to find 
        // a beta vector that can accurately predict future data.
        void estimateParameters();
        
        
        // set*StepWise*On
        // Allows the user to manually control the data members that handle
        // backward or forward stepwise regression
        void setBackStepWiseOn( bool on = false ) {backwardStepwiseGenAcc_ = on, backwardStepwiseLR_ = on, backwardStepwiseMaxAcc_ = on;}
        void setBackStepWiseGenAccOn( bool on = false, int k = 1 ) {backwardStepwiseGenAcc_ = on, kFold_ = k;} 
        void setBackStepWiseMaxAccOn( bool on = false ) {backwardStepwiseMaxAcc_ = on;}
        void setBackStepWiseLROn( bool on = false ) {backwardStepwiseLR_ = on;}
        void setForwardStepWiseOn( bool on = false ) {forwardStepwiseGenAcc_ = on, forwardStepwiseLR_ = on, forwardStepwiseMaxAcc_ = on;}
        void setForwardStepWiseGenAccOn( bool on = false, int k = 1 ) {forwardStepwiseGenAcc_ = on, kFold_ = k;} 
        void setForwardStepWiseMaxAccOn( bool on = false ) {forwardStepwiseMaxAcc_ = on;}
        void setForwardStepWiseLROn( bool on = false ) {forwardStepwiseLR_ = on;}
        
        
        // setParameters
        void setParameters();
        
        
        // classifyTrainingData
        // Calculates the distance from the mean for the training data.
        // It also predicts which class each of the samples fall into.
        int classifyTrainingData();
        
        
        // classifyTestData
        // Calculates the distance from the mean for the test data.
        // It also predicts which class each of the test samples fall into.
        int classifyTestData();
        
        
        // coxAndSnell
        // Calculates and outputs the Cox & Snell psuedo R^2 value.
        // Assumes that estimate parameters has already been run for the
        // associated log_regression_mvdisc.
        float coxAndSnell() const;
        
        
        // nagelkerkel
        // Calculates and outputs the Nagelkerke/Cragg & Uhler's psuedo R^2 value. 
        // Assumes that estimate parameters has already been run for the
        // associated log_regression_mvdisc.
        float nagelkerke() const;
        
        
    protected:
        //void runTrackStats();
        
        
    private:
        // private member functions:
        
        // estParamsBackwardStep_maxAcc
        // Runs through the newton method with a decreasing number of parameters 
        // until the accuracy of within group or generalization predictions do not increase
        void estParamsBackwardStep_maxAcc(smatrix& data, const smatrix& yMatrix, 
                                          std::vector<int>& colsDeleted, float oldAcc, bool useGenAcc);
        
        // estParamsBackwardStepLR
        // Runs through the newton method with a decreasing number of parameters 
        // until the likelihood ratio falls above the desired p-value cut off (chiSquaredCV_)
        void estParamsBackwardStepLR(smatrix& data, const smatrix& yMatrix, std::vector<int>& colsDeleted);
        
        
        // estParamsForwardStep_maxAcc
        // Runs through the newton method with an increasing number of parameters 
        // until the accuracy of within group or generalization predictions do not increase
        void estParamsForwardStep_maxAcc(smatrix& currData, const smatrix& fullData, const smatrix& yMatrix, 
                                          std::vector<int>& colsAdded, float oldAcc, bool useGenAcc);
        
        // estParamsForwardStepLR
        // Runs through the newton method with an increasing number of parameters 
        // until the likelihood ratio falls below the desired p-value cut off (chiSquaredCV_)
        void estParamsForwardStepLR(smatrix& currData, const smatrix& fullData, const smatrix& yMatrix, 
                                    std::vector<int>& colsAdded, float oldLR);
        
        // estParamsForwardStepScoreStat
        // Runs through the newton method with an increasing number of parameters 
        // until the score statistic falls below the desired p-value cut off (chiSquaredCV_)
        void estParamsForwardStepScoreStat(smatrix& currData, const smatrix& fullData, const smatrix& yMatrix, 
                                           std::vector<int>& colsAdded, const smatrix& oldBeta);
        
        // newtonAlgorithm
        // Uses the iteratively reweighted least squares method (using 
        // the Newton-Rhapson algorithm) to estimate beta.
        // Inputs: The data matrix and classification targets in yMatrix
        // Output: The log likelihood of the final beta
        //
        // Convergence is measured by comparing old and new betas, using a
        // threshold defined in clapack_utils and implemented in operator==/!=
        float newtonAlgorithm(const smatrix& data, const smatrix& yMatrix, smatrix& beta);
        
        
        // findTrainingAccuracy
        // Calculates the accuracy of classified training data against the 
        // actual classes they are from
        float findTrainingAccuracy(const smatrix& data, const smatrix& beta);
        
        
        // findGenAcc
        // Calculates the accuracy of classified training data against the 
        // actual classes they are from. In order to do this, it uses cross
        // validation 
        float findGenAcc(const smatrix& data_in, const unsigned int& Kin);
        
        
        // findScoreStatistic
        // Calculates the score statistic used to determine whether a variable
        // can enter the model. It is equal to g'*(-H)^(-1)*g, where g is the 
        // first derivative of log-likelihood wrt the null hypothesis' beta, 
        // and H is the corresponding second derivative (aka Hessian)
        float findScoreStatistic(const smatrix& data, const smatrix& beta, const smatrix& yMatrix);
        
        
        
        // private data members:
        
        // ZERO_THRESH is the threshold for when values being inverted 
        // should remain at zero.
        // fracVarThresh_ is a cutoff for how much of the total variance to 
        // use as an offset to the data
        // chiSquareCV_ is the critical value of a chi squared distribution
        // at the probability that is desired. For example, for P = 0.90, 
        // x^2 < 2.71.
        // ( Use http://stattrek.com/online-calculator/chi-square.aspx )
        float ZERO_THRESH, fracVarThresh_, chiSquaredCV_;
        
        unsigned int alloc_Dimensionality, alloc_NumberOfSamples;
        
        // toggles for the option of performing backward stepwise iterations
        bool backwardStepwiseLR_, backwardStepwiseMaxAcc_, backwardStepwiseGenAcc_;
        
        // toggles for the option of performing forward stepwise iterations
        bool forwardStepwiseLR_, forwardStepwiseMaxAcc_, forwardStepwiseGenAcc_;
        
        // to complete the iterative process, keep track of the beta matrix
        // for the full and null models
        smatrix beta_, betaNull_;      
        
        // when running generalization accuracy, this keeps the K-Fold value 
        // to use for cross validation 
        int kFold_;
        
        // also, store the log likelihoods of the full and null models to
        // to use for statistical purposes 
        float logLikelihood_, logLikeliNull_;
    };
    
    
    // likelihoodRatio
    // Calculates the likelihood ratio of two models (-2 ln(l_null/l_full))
    // Inputs: redLikelihood is the reduced model's log likelihood, 
    //         fullLikelikehood is the full model's.
    // Output: The likelihood ratio
    float likelihoodRatio(const float redLikelihood, const float fullLikelihood);
    
    
    
    
}


#endif // su_mvdisc_log_regression_h
