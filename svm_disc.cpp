/*
 *  svm_disc.cpp
 *  su_mvdisc
 *
 *  Created by Brian Patenaude on 7/16/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "svm_disc.h"
//#include <misc_utils/misc_utils.h>
#include <misc_utils.h>
#include <cmath>
#include <fstream>
using namespace std;
using namespace misc_utils_name;

namespace su_mvdisc_name{
	
	

	
SVM_mvdisc::SVM_mvdisc()
{
    nodeTypeSpec=SVM;
	//cout<<"SVM constructor"<<endl;
	libsvm_data.l=0;
	libsvm_data.x=NULL;
	libsvm_data.y=NULL;
	libsvm_model=NULL;

	//set parameters
	libsvm_parameter.svm_type = C_SVC;
	libsvm_parameter.kernel_type = LINEAR;
	libsvm_parameter.cache_size = 100;

	///needs to start at 0 so that libSVM will set it, libSVM save it in command lien version of svm_train
	libsvm_parameter.gamma = 0;//1.0 / Dimensionality_ ;
//gamma will be set in kernel function
	libsvm_parameter.degree = 3;
	libsvm_parameter.eps = 0.001;//using defaults form libsvm website
	libsvm_parameter.C = 1;
	libsvm_parameter.coef0 = 0;

	
	libsvm_parameter.nr_weight = 0 ;
//	int weightLabel[] = NULL;//{0,1};
//	double weights[] = NULL;//{1.0,1.0};
	//libsvm_parameter.wi=1;
	libsvm_parameter.weight_label = NULL;// weightLabel;
	libsvm_parameter.weight = NULL;//weights;
	libsvm_parameter.p = 0.1;
	libsvm_parameter.nu = 0.5;
	libsvm_parameter.shrinking = 1;
	libsvm_parameter.probability = 0;
	NodeName="SVM";

	UseAsFilter = false;
	
}

SVM_mvdisc::~SVM_mvdisc()
{
	//cout<<"SVM destructor1"<<endl;

	
	//cout<<"SVM destructor2"<<endl;

//	if (libsvm_model != NULL)
//		delete libsvm_model;
   	if (libsvm_model !=NULL){
        svm_free_and_destroy_model(&libsvm_model);
        libsvm_model=NULL;
    }
    
//	if ( libsvm_parameter.weight_label != NULL )
//		delete[] libsvm_parameter.weight_label;
//	if ( libsvm_parameter.weight != NULL )
//		delete[] libsvm_parameter.weight;

    //delete target
	if (libsvm_data.y != NULL)
		delete[] libsvm_data.y;
    
    //delete training data 
	for ( vector<svm_node*>::iterator i = svm_training_data.begin(); i != svm_training_data.end(); ++i)
	{
        if (*i!=NULL)
        {
            delete[] *i;
            *i=NULL;
        }
	}
	
	
	svm_destroy_param(&libsvm_parameter);
    
    
    if ( feature_w_sx_ == NULL )
        delete feature_w_sx_;
    if ( feature_w_sxx_ == NULL )
        delete feature_w_sxx_;
    if ( featureAbs_w_sx_ == NULL )
        delete featureAbs_w_sx_;
    if ( featureAbs_w_sxx_ == NULL )
        delete featureAbs_w_sxx_;
    if ( featureAbs_w_sxx_ == NULL )
        delete featureAbs_w_sxx_;
    
    
    
//	delete_svmTrainingData();
	
	//for (unsigned int i=0; i<Nsamples;i++)
//		 delete libsvm_data.x[i];
}
	
	int SVM_mvdisc::set_SVM_type( const string & type )
	{
			if ( type == "C-SVC" ) 
				return 0;
			else if ( type == "nu-SVC" ) 
				return 1;
			else if ( type == "one-class" ) 
				return 2;
			else if ( type == "epsilon-SVR" ) 
				return 3;
			else if ( type == "nu-SVR" ) 
				return 4;

			throw ClassificationTreeNodeException(("Invalid SVM type : " + type).c_str() );
		return -1;
	}
	
	int SVM_mvdisc::set_kernel_type( const string & type )
	{
		if ( type == "linear" ) 
			return LINEAR;
		else if ( type == "polynomial" ) 
			return POLYNOMIAL;
		else if ( type == "RBF" ) 
			return RBF;
		else if ( type == "sigmoid" ) 
			return SIGMOID;
				
		throw ClassificationTreeNodeException(("Invalid SVM kernel type : " + type).c_str() );
		return -1;
	}
	
	void SVM_mvdisc::setOptions( std::stringstream & ss_options)
	{
		
		//-------BEFORE SETTING CUSTOM OPTIONS RESET TO DEFAULT----------//
		//set parameters
		libsvm_parameter.svm_type = C_SVC;
		libsvm_parameter.kernel_type = LINEAR;
		libsvm_parameter.cache_size = 100;
		
		///needs to start at 0 so that libSVM will set it, libSVM save it in command lien version of svm_train
		libsvm_parameter.gamma = 0;//1.0 / Dimensionality_ ;
		//gamma will be set in kernel function
		libsvm_parameter.degree = 3;
		libsvm_parameter.eps = 0.001;//using defaults form libsvm website
		libsvm_parameter.C = 1;
		libsvm_parameter.coef0 = 0;
		
		
		libsvm_parameter.nr_weight = 0 ;
		//	int weightLabel[] = NULL;//{0,1};
		//	double weights[] = NULL;//{1.0,1.0};
		//libsvm_parameter.wi=1;
		libsvm_parameter.weight_label = NULL;// weightLabel;
		libsvm_parameter.weight = NULL;//weights;
		libsvm_parameter.p = 0.1;
		libsvm_parameter.nu = 0.5;
		libsvm_parameter.shrinking = 1;
		libsvm_parameter.probability = 0;
		
		
		//libsvm_parameter.nr_weight = 2 ;

		
		
		//\---------------SET CUTOM TOPTIONS ------------------//
		
		
		map<string,string> options = parse_sstream(ss_options);
		for ( map<string,string>::iterator it = options.begin(); it!=options.end();it++)
		{
			//cout<<"set svm option "<<it->first<<" "<<it->second<<endl;
			
			if ( it->first == "svm_type")
			{
				libsvm_parameter.svm_type = set_SVM_type(it->second);
				//cout<<"svm_type "<<libsvm_parameter.svm_type<<endl;
			}else if ( it->first == "kernel_type")
			{
				libsvm_parameter.kernel_type = set_kernel_type(it->second);
				//cout<<"kernel_type "<<libsvm_parameter.kernel_type<<endl;

			}else if ( it->first == "degree")
			{
				libsvm_parameter.degree = string2num<int>( it->second );
			}else if ( it->first == "gamma")
			{
				libsvm_parameter.gamma = string2num<double>( it->second );
			}else if ( it->first == "coef0")
			{
				libsvm_parameter.C = string2num<double>( it->second );

			}else if ( it->first == "nu")
			{
				libsvm_parameter.svm_type = string2num<double>( it->second );

			}else if ( it->first == "epsilon_SVR")
			{
				libsvm_parameter.p = string2num<double>( it->second );
			}else if ( it->first == "cache_size")
			{
				libsvm_parameter.cache_size = string2num<double>( it->second );

			}else if ( it->first == "epsilon")
			{
				libsvm_parameter.eps = string2num<double>( it->second );
			}else if ( it->first == "shrinking")
			{
				libsvm_parameter.shrinking = string2num<int>( it->second );

			}else {
				//if not in node specific implementation, check to see if option exists in Tree node.
				//by including at the end, can replace option handling of parent (ClassificationTreeNode);
				if (!setTreeNodeOptions(it->first, it->second))
					throw ClassificationTreeNodeException(("SVM: Invalid option into SVM node: "+ it->first).c_str() );
			}


//			else if ( it->first == "quiet")
//			{
//				libsvm_parameter.quiet = string2num<int>( it->second );
//
//			}
		}
	
		
		
		
		
	}
	
	
	
	void SVM_mvdisc::delete_svmTrainingData()
	{
	//	for (vector< vector<svm_node>* >::iterator i = svm_training_data_tracker.begin(); i!= svm_training_data_tracker.end();i++)
	//		delete *i;
		//svm_training_data_tracker.clear();
	}
	

	void SVM_mvdisc::displaySupportVector(){
        //number fo support vectors
        unsigned int nSV = libsvm_model->l;
        cout<<"Support Vectors ("<<nSV<<")"<<endl;
        for ( unsigned int l = 0; l < nSV; ++l)
        {
            cout<<"Support Vector - "<<l<<endl;
            for (unsigned int dim = 0 ; dim < Dimensionality_; ++dim)
                cout<<(libsvm_model->SV)[l][dim].value<<" ";
            cout<<endl;
        }
    }

	
	
void SVM_mvdisc::estimateParameters()
{//Currently does not allow sparse sampling
	//	svm_node *svm_training_data[] = new svm_node*[Mdims*Nsamples];
	//	for (vector<float>::iterator i= training_data.begin(); i!=training_data.end();i++)
	//	{
	//		svm_node *temp = new svm_node;
	//	}
	//print<float>(*(in_data[0]),"svmin_data");
    if (Verbose>0)
    {
        cout<<"SVM_mvdisc::estimateParameters: "<<endl;
        cout<<"Dimensionality: "<<Dimensionality_<<endl;
        cout<<"NumberOfSamples: "<<NumberOfSamples_<<endl;
        cout<<"NumberOfTestSamples: "<<NumberOfTestSamples_<<endl;
    }
	if (libsvm_parameter.gamma == 0 )
			libsvm_parameter.gamma= 1.0 / Dimensionality_;

	//cout<<"GAMMA: "<<Dimensionality_<<" "<<libsvm_parameter.gamma<<endl;
	libsvm_data.l=NumberOfSamples_;
	
    //delete target
	if (libsvm_data.y != NULL)
    {
		delete[] libsvm_data.y;
        libsvm_data.y=NULL;
    }
    //delete training data 
    for ( vector<svm_node*>::iterator i = svm_training_data.begin(); i != svm_training_data.end(); ++i)
	{
        if (*i!=NULL)
        {
            delete[] *i;
            *i=NULL;
        }
    }
    //	delete_svmTrainingData();//doesn't actually do anytihng????
   // cout<<"free and destroy model "<<endl;
	if (libsvm_model !=NULL)
    {
        svm_free_and_destroy_model(&libsvm_model);
        libsvm_model=NULL;
    }    
   // cout<<"done free and destroy model "<<endl;

    //else        
    //    cout<<"libsvm model is null"<<endl;
//cout<<"Kclasses_ "<<Kclasses<<endl;
	libsvm_parameter.nr_weight = Kclasses_;//training_target->getNumberOfClasses();

	if ( libsvm_parameter.weight_label != NULL )
		delete[] libsvm_parameter.weight_label;
    
    
	if ( libsvm_parameter.weight != NULL )
		delete[] libsvm_parameter.weight;
	
   // cout<<"lets create some stuff "<<endl;
    
    libsvm_parameter.weight_label = new int[Kclasses_];// weightLabel;
	libsvm_parameter.weight = new double[Kclasses_];//weights;
	//cout<<"delte dta.y"<<endl;
	if (libsvm_data.y != NULL)
    {
		delete[] libsvm_data.y;
        libsvm_data.y=NULL;
    }
   // cout<<"further into estimate "<<endl;
	
	for (unsigned int i =0 ; i < Kclasses_ ; i++)
	{
		libsvm_parameter.weight_label[i] = training_target->getClassLabel(i);
		libsvm_parameter.weight[i] = 1.0;
	//	cout<<"class label "<<i<<" "<<" with weight "<<libsvm_parameter.weight[i]<<" "<<libsvm_parameter.weight_label[i]<<endl;
		
	}
	//cout<<"The are "<<libsvm_data.l<<" samples."<<endl;
    libsvm_data.y= new double[NumberOfSamples_];//reinterpret_cast<double*>(&training_target[0]);
	
	int count=1;

	//load training data into libsvm friednly format
	//assumes no sparse data 
	//	count=1;
	
	
	//vector<float> training_data;
   // vector<float>* training_data =  new vector<float>();
   // copyToSingleVector(  in_data, training_data , NumberOfSamples_ );

//	copyToSingleVector(  in_data, &training_data , NumberOfSamples_ );
	
	
	//-------------------STORE THE TRAINING DATA IN OUTDATA-------------------
	//then apply clasifier to that
	
	//copyToSingleVector(  in_data, out_data , NumberOfSamples_ );

	//cout<<"resize "<<NumberOfSamples_<<" "<<svm_training_data.size()<<endl;
//	vector<svm_node*> svm_training_data;
//	svm_node* null = NULL;
	
	
	//this contains the LIBSVM nodes
		for ( vector<svm_node*>::iterator i = svm_training_data.begin(); i != svm_training_data.end(); ++i)
		{
			delete[] *i;
			*i=NULL;
		}
	svm_training_data.resize(NumberOfSamples_,NULL);//+1 for -1 at end of subject sample
	//svm_node* svm_training_data = new svm_node[NumberOfSamples_]:
	//cout<<"done resize 2 "<<endl;
	
	vector< svm_node* >::iterator i_train = svm_training_data.begin();
    
   // vector<float>::iterator i= training_data->begin();
	for (unsigned int sample=0;sample<NumberOfSamples_;++sample,++i_train)
	{
		//cout<<"sample traingn svm "<<endl;
        
		if (*i_train == NULL){
            //		cout<<"create new node"<<endl;
			*i_train = new svm_node[Dimensionality_+1];
		}
        
		count=1;
        //		svm_node temp; //create a single node
        for ( vector< vector<float>* >::iterator i = in_data.begin(); i != in_data.end();++i)
        {
            unsigned int dim = (*i)->size()/NumberOfSamples_;
            // cout<<"input : "<<dim<<endl;
            vector<float>::iterator ii = (*i)->begin()+dim*sample;
            for ( unsigned int j = 0 ; j < dim; ++j, ++ii,++count)
            {
                // cout<<count<<","<<*ii<<","<<static_cast<double> (*ii)<<endl;
                (*i_train)[count-1].index = count;
                (*i_train)[count-1].value=static_cast<double> (*ii); 
            }
            // cout<<endl;
        }
        (*i_train)[Dimensionality_].index = -1;
		(*i_train)[Dimensionality_].value=0;                                         
                                                          
	}

   
//svm_free_and_destroy_model(&libsvm_model);
    count=0;
	for (vector<float>::iterator i=training_target->begin(); i!= training_target->end(); i++,count++)
	{
		libsvm_data.y[count]=(*i);//static_cast<int> (*i);
        //cout<<"svm trianing target "<<count<<" "<<*i<<endl;
	}
    count=0;
	for (vector<float>::iterator i=training_target->begin(); i!= training_target->end(); i++,count++)
	{
		libsvm_data.y[count]=(*i);//static_cast<int> (*i);
        //cout<<"svm trianing target "<<count<<" "<<libsvm_data.y[count]<<endl;
	}
//-----------------------------------------------------------this portion of code displays training data-----------------------------------------------------------//
    
    
if (Verbose>2)
{
	for (unsigned int i = 0 ; i<svm_training_data.size(); ++i)
	{
		cout<<"sample: "<<i<<endl;
		cout<<"target: "<<libsvm_data.y[i]<<endl;
		unsigned int j=0;
		while (svm_training_data[i][j].index != -1 )  {
			cout<<svm_training_data[i][j].index<<","<<svm_training_data[i][j].value<<"  ";
			++j;
		}
		cout<<svm_training_data[i][j].index<<","<<svm_training_data[i][j].value<<endl;
	}
}	 
	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		libsvm_data.x=(&svm_training_data[0]);
	//cout<<"load target data "<<endl;
    //if ( 1 == 0 )
    if (Verbose>0)
    {
        if (svm_check_parameter(&libsvm_data, &libsvm_parameter)==NULL)
            cout<<"parameters are within a feasible range"<<endl;
    //    cout<<"Train SVM using libSVM..."<<endl;
        
    }
    
    if (Verbose>0) cout<<"Train SVM ...";
    libsvm_model = svm_train( &libsvm_data, &libsvm_parameter);
    if (Verbose>0) cout<<"done."<<endl;
    
    // displaySupportVector();
    //  delete[] libsvm_data.y;
    //libsvm_data.y=NULL;
    
    
    
    //deleting tgese precent 
   // for ( vector<svm_node*>::iterator i = svm_training_data.begin(); i != svm_training_data.end(); ++i)
//	{
        
  //      delete[] *i;
   //         *i=NULL;
	//}
    //svm_training_data.clear();
	//cout<<"Done estimating SVM parameters."<<endl;
    if (Verbose)
        displaySupportVector();
    
	//cout<<"Number Of Support Vectors "<<libsvm_model->l<<endl;
	//cout<<"done svm estimate paarnms"<<endl;
    
    if (Verbose>0)
        cout<<"Done estimating SVM parameters."<<endl;

}

	void SVM_mvdisc::setParameters( const std::vector<float> & params )
	{
		//FOR NOW WE SHOULD BE ONLY REALLY BE DEALING WITH LINEAR
		if (libsvm_parameter.svm_type == LINEAR)
		{			
			//this will set the data as a single support vector
			//we can just split the "single" support vector all existing support vectors
			//number fo support vectors
			//constant in decision function should stay teh same ? no, need to accumulate as well
			double weightings = libsvm_model->l;//split the support vectors amongst the existing suport vectors
			
			unsigned int nSV = libsvm_model->l;
			
			//set coefficient weighting
			for ( unsigned int l = 0; l < nSV; ++l)
				(*libsvm_model->sv_coef)[l] = weightings;
			
			//set support vectors
			vector<float>::const_iterator i_p = params.begin();
			for (unsigned int dim = 0 ; dim < Dimensionality_; ++i_p, ++dim)
				for ( unsigned int l = 0; l < nSV; ++l)
					(libsvm_model->SV)[l][dim].value = static_cast<double> (*i_p) / nSV;
			
			//set constant 
			unsigned int count = 0 ;
			for ( ; i_p != params.end(); ++i_p)
			{
			//	cout<<"set constant "<<*i_p<<endl;
				libsvm_model->rho[count]=static_cast<double>(*i_p);
			}
			
			
		}else {
			throw ClassificationTreeNodeException("At the moment, setParameters is not defined in SVM for any kernel but linear");
		}
		
	}
	
	void SVM_mvdisc::setParameters( const std::vector<float> & params, const std::vector<unsigned int> & mask )
	{
		//FOR NOW WE SHOULD BE ONLY REALLY BE DEALING WITH LINEAR
		if (libsvm_parameter.svm_type == LINEAR)
		{			
			//this will set the data as a single support vector
			//we can just split the "single" support vector all existing support vectors
			//number fo support vectors
			//constant in decision function should stay teh same ? no, need to accumulate as well
			//
			//the same suport vectors are duplicated, the split is reflected in the coefficients
			//by using number of support vectors calculated don't have to mess with libSVM storage /representation
			//just need to alter wegithgin and supprt vectors
			double weightings = 1.0/ libsvm_model->l;//split the support vectors amongst the existing suport vectors
			
			
			
			unsigned int nSV = libsvm_model->l;
			
			
			{
			//set coefficient weighting, weighting of each suport vector
				//in order for this to work need to write over support vector 
				
			for ( unsigned int l = 0; l < nSV; ++l)
				(*libsvm_model->sv_coef)[l] = weightings;
				
			}
			
			//set support vectors
			//the mask masks out support vectors ? doesnt make sense 
			vector<unsigned int>::const_iterator i_m = mask.begin();
			vector<float>::const_iterator i_p = params.begin();
			for (unsigned int dim = 0 ; dim < Dimensionality_; ++i_p, ++i_m, ++dim)
					if (*i_m != 0)
					{
						for ( unsigned int l = 0; l < nSV; ++l)
							(libsvm_model->SV)[l][dim].value = static_cast<double> (*i_p);
					}else{
						for ( unsigned int l = 0; l < nSV; ++l)
							(libsvm_model->SV)[l][dim].value = 0;
					}
			//set constant 
			unsigned int count = 0 ;
			for ( ; i_p != params.end(); ++i_p)
			{
				//cout<<"set constant "<<*i_p<<endl;
				libsvm_model->rho[count]=static_cast<double>(*i_p);
			}
			
			
		}else {
			throw ClassificationTreeNodeException("At the moment, setParameters is not defined in SVM for any kernel but linear");
		}
		
	}
	void SVM_mvdisc::saveParameters( )
    {
        //   cerr<<"LDA_mvdisc::saveParameters "<<output_basename<<" "<<Verbose<<endl;
        
        if (Verbose >= 2)
            cout<<"SVM_mvdisc::saveParameters "<<output_basename<<endl;
        
        if  ( output_basename.empty() ) //don't save parameters if output name has not been set
            return;
        //want to conatin all models specifics in a single line
        ofstream f_params((output_basename + "." + NodeName + ".params.txt").c_str(),ios_base::out | ios_base::app);
        f_params<<"ASCII SVM"<<" "<<Dimensionality_<<" "<<NumberOfSamples_<<" "<<Kclasses_;//<<" "<<libsvm_parameter.svm_type;
        //  cout<<"SVM_mvdisc::getParameters()"<<endl;
		vector<float> params;
		//add rho onto the end
		//FOR NOW WE SHOULD BE ONLY REALLY BE DEALING WITH LINEAR
		if (libsvm_parameter.svm_type == LINEAR)
		{
            f_params<<" LINEAR";
            //cout<<"linear SVM "<<endl;
			unsigned int Nrhos = Kclasses_ *(Kclasses_-1)/2; //(this is equal to one for two class case)
            
			//number fo support vectors
			unsigned int nSV = libsvm_model->l;
            //  cout<<"loop over DIM "<<nSV<<endl;
			for (unsigned int dim = 0 ; dim < Dimensionality_+DimTrim; ++dim)
			{
                //  cout<<"id "<<dim<<"/"<<Dimensionality_<<" "<<DimTrim<<endl;
				float temp_param = 0.0;
				
				for ( unsigned int l = 0; l < nSV; ++l){
                    temp_param += (*libsvm_model->sv_coef)[l] * (libsvm_model->SV)[l][dim].value;
                    //      cout<<"temp param "<<temp_param<<endl;
                }
                
                f_params<<" "<<temp_param;
//				params.push_back(temp_param);
				
			}
			for (unsigned int i = 0 ; i < Nrhos; ++i)
            {//params.push_back(libsvm_model->rho[i]);
                f_params<<" "<<libsvm_model->rho[i];
            }
            
		}else {
            cerr<<"At the moment, getParameters is not defined in SVM for any kernel but linear"<<endl;
//			throw ClassificationTreeNodeException("At the moment, getParameters is not defined in SVM for any kernel but linear");
		}

        
        
        f_params<<endl;
        f_params.close();
        
    }

	std::vector<float> SVM_mvdisc::getParameters()
	{
        //  cout<<"SVM_mvdisc::getParameters()"<<endl;
		vector<float> params;
		//add rho onto the end 
		//FOR NOW WE SHOULD BE ONLY REALLY BE DEALING WITH LINEAR
		if (libsvm_parameter.svm_type == LINEAR)
		{			
            //cout<<"linear SVM "<<endl;
			unsigned int Nrhos = Kclasses_ *(Kclasses_-1)/2; //(this is equal to one for two class case)

			//number fo support vectors
			unsigned int nSV = libsvm_model->l;
          //  cout<<"loop over DIM "<<nSV<<endl;
			for (unsigned int dim = 0 ; dim < Dimensionality_+DimTrim; ++dim)
			{
              //  cout<<"id "<<dim<<"/"<<Dimensionality_<<" "<<DimTrim<<endl;
				float temp_param = 0.0;
				
				for ( unsigned int l = 0; l < nSV; ++l){
                    temp_param += (*libsvm_model->sv_coef)[l] * (libsvm_model->SV)[l][dim].value;
              //      cout<<"temp param "<<temp_param<<endl;
                }
			
				params.push_back(temp_param);
				
			}
			for (unsigned int i = 0 ; i < Nrhos; ++i)
				params.push_back(libsvm_model->rho[i]);
			
					
		}else {
			throw ClassificationTreeNodeException("At the moment, getParameters is not defined in SVM for any kernel but linear");
		}

		return params;
		
	}
	//this adds parameters to 
	void SVM_mvdisc::getParameters( std::vector< std::list<float> > & all_params)
	{
		unsigned int nparams= Dimensionality_ + Kclasses_ *(Kclasses_-1)/2; //last bit is constant offset
		//FOR NOW WE SHOULD BE ONLY REALLY BE DEALING WITH LINEAR
		if (libsvm_parameter.svm_type == LINEAR)
		{
			//number of parameters = number of support vectors
			if (all_params.empty())
			{
				list<float> params;
				all_params.assign(nparams,params);
			}else if (nparams != all_params.size())
			{
				throw ClassificationTreeNodeException("NumberOfExisting parameters does not match the number being added. Varying size of selection mask.");
			}
			
			//cout<<"Number Of Classes "<<libsvm_model->nr_class<<endl;
		//	cout<<"Support Vector Coefficient: "<<endl;
		//	for (int i=0; i < libsvm_model->l ; i++)
		//		cout<<(* libsvm_model->sv_coef)[i]<<" ";
		//	cout<<endl;
			
			//number fo support vectors
			unsigned int nSV = libsvm_model->l;
			unsigned int dim = 0;
	
			for (vector< list<float> >::iterator i_p = all_params.begin(); i_p != all_params.end(); ++i_p,++dim)
			{

				
				//this part adds cont
				if (dim < Dimensionality_)
				{	
					
					//support vector index
					float temp_param = 0.0;
					
					for ( unsigned int l = 0; l < nSV; ++l)
					{
						//			cout<<"coef "<<l<<" "<<dim<<" "<<(*libsvm_model->sv_coef)[l]<<" "<<(libsvm_model->SV)[l][dim].value<<" "<<endl;
						temp_param += (*libsvm_model->sv_coef)[l] * (libsvm_model->SV)[l][dim].value;
						//			cout<<"tp "<<(*libsvm_model->sv_coef)[l] * (libsvm_model->SV)[l][dim].value<<" "<<temp_param<<endl;
					}		
					//		cout<<"sum param "<<temp_param<<endl;
					i_p->push_back(temp_param);
				}else {
					
				//	cout<<"pushing back rho "<<libsvm_model->rho[dim-Dimensionality_]<<" "<<all_params.size()<<endl;
					i_p->push_back(libsvm_model->rho[dim-Dimensionality_]);
				}

			}
//			for (int l=0; l < nSV ; l++)
//			{
//				cout<<"Support Vector : "<<l<<endl;
///				for (unsigned int j =0 ; j < Dimensionality_ ; j++)
	//				cout<<"("<<(libsvm_model->SV)[l][j].index<<","<<(libsvm_model->SV)[l][j].value<<")"<<endl;
	//		}
	//		cout<<endl;
			
		}else {
			throw ClassificationTreeNodeException("At the moment, getParameters is not defined in SVM for any kernel but linear");
		}
		
		
		
	}
	int SVM_mvdisc::classifyTrainingData(){
		//cout<<"classify training data "<<svm_training_data.size()<<endl;
		float cl=0;
		for (unsigned int sample=0;sample<NumberOfSamples_;sample++)
		{//the training data was stored in svm_trainign_data
          //  cout<<"sample "<<sample<<endl;
        //    for (unsigned int d = 0 ; d < Dimensionality_;++d)
          //    {
            //    cout<<svm_training_data[sample][d].index<<"/"<<svm_training_data[sample][d].value<<" ";
            
            //}
			//cout<<endl;
			//Still use this for the actual prediction as it will make it easier for working with other kernel, one-class svm , etc...
			cl  = svm_predict(libsvm_model, svm_training_data[sample]);
			out_data->push_back(cl);
			
			
		}
	//	DimTrim = Dimensionality_ -1;
		//Dimensionality_ = 1;
		//round in case numerical erros, though there shouldn't be
		return static_cast<int>(cl+0.5);

		
	}

int SVM_mvdisc::classifyTestData( )  
{
	if (Verbose>0)
		cout<<"Classify Data using SVM..."<<endl<<"Number of Test Samples: "<<NumberOfTestSamples_<<endl;

	if (disc_distance==NULL)
		disc_distance = new vector<float>();
	else {
		disc_distance->clear();
	}

//    cout<<"do samples1 "<<endl;

	float cl=0;//classification, last classify is returned	
	
	//vector<svm_node>* one_sample = new vector<svm_node>(); //cannot delete in loop because pointers are used 
	vector<float> test_data;
    
	copyToSingleVector(  in_test_data , &test_data , NumberOfTestSamples_ );
//    cerr<<"testdatasize "<<test_data.size()<<" "<<Dimensionality_<<" "<<DimTrim<<" "<<NumberOfTestSamples_<<" "<<(DimTrim+Dimensionality_)*NumberOfTestSamples_<<endl;
//    print(test_data,"svm test data");

	vector<float>::iterator i= test_data.begin();
//	cout<<"do samples2 "<<endl;

	//classify within loop
	//Do for each test sample
	
		//this function gets the hyperlane parameters, the last is the constant offset
	vector<float> hplane = getParameters();
	float norm_hplane = 0.0;
//    cout<<"got parameters for hyperplane"<<endl;
	for (vector<float>::iterator i_h = hplane.begin(); i_h != hplane.end()-1; ++i_h)
		norm_hplane += (*i_h)*(*i_h);
//    cout<<"take sqrt"<<endl;
	norm_hplane = sqrt(norm_hplane);
	

	if (Verbose>1)
		print<float>(hplane,"hyperplane");
	
//    print(test_data,"svm test data");

//	cout << "do samples " << NumberOfTestSamples_ << " " << endl;
	
	
	for (unsigned int sample=0;sample<NumberOfTestSamples_;sample++)
	{
		//one_sample represent a singles sample, test_data are all test data in a single vector
		vector<svm_node> one_sample ;
		
		unsigned int count=1;//keeps track index for svm_node
	
		//hyperlplane projection
        
        /********************* remember to comment this out *******************************/
        //Verbose = true;
        /**********************************************************************************/
		if (Verbose>0)
        {
            cout<<"testN "<<test_data.size()<<endl;
			cout<<"Test Data :"<<endl;
		}
//        cout<<"Test Data :"<<test_data.size()<<endl;
		float proj_to_hplane  = 0.0;
		vector<float>::iterator i_hplane = hplane.begin();
		for (; i!= test_data.end(); ++i,++count,++i_hplane)//this is all training data in a single vector
		{
			svm_node temp; //create a single node
			temp.index=count;
			temp.value=static_cast<double> (*i);
			one_sample.push_back(temp);
			if (Verbose>2)
				cout<<temp.value<<" ";
         

			//clauclating dot product for distance from hyperplane
//            cerr<<"proj to plane "<<hplane.size()<<" "<<Dimensionality_<<" "<<DimTrim<<" "<<*i_hplane<<" "<<*i<<endl;
			proj_to_hplane += (*i_hplane) * (*i) ; //this is q.d, ie the point projected onto hyperplane normal
//			cerr<<"proj_to_hplane "<<proj_to_hplane<<endl;
			if (count==(Dimensionality_+DimTrim))
			{
				i++;
				
				temp.index=-1;
				temp.value=0.0;
				one_sample.push_back(temp);
             //   cout<<"temp "<<proj_to_hplane<<" "<<(*(i_hplane+1))<<" "<<norm_hplane<<endl;
				//do distance to hyperplane stuff
				//need to add scalar portion of hyperlan plus normalize by size of normal (which is the square root of what is currently in dist_to_hplane)
                //**i_hplane +1 is the Rho value
				disc_distance->push_back( (proj_to_hplane - (*(i_hplane+1)))/ norm_hplane  );//the last value scalar offset
//                cerr<<"proj_to_hplane "<<disc_distance->back()<<endl;

				proj_to_hplane  = 0.0;

				//verbose endl used for output test data
				if (Verbose>2)
					cout<<endl;
               

				break;
			}
		}
				
		if (Verbose){
			
			cout<<"Calculate Distance to Hypeplane "<<endl;
			cout<<"Number of Features: "<<Dimensionality_+DimTrim<<endl;
//			cout<<"Distance to hyperplane "<<disc_distance->back()<<endl;
			print<float>(*disc_distance,"Distance to hyperplane");
			//				disc_distance = 		
		}	
	//	svm_node* test = &one_sample[0];
//		for (int i =0  ; i<Dimensionality_+1 ;i++)
//			cout<<"SVMclaasifyTest "<<test[i].index<<" "<<test[i].value<<" : ";//
//		cout<<endl;
		//push pack endof line
		//svm_node temp_endolf; //create a single node
//		cout<<"Number Of Classes "<<libsvm_model->nr_class<<endl;
//		cout<<"Support Vector Coefficient Predict: "<<endl;
//		for (int i=0; i < libsvm_model->l ; i++)
//			cout<<(* libsvm_model->sv_coef)[i]<<" ";
//		cout<<endl;
		
//		for (int l=0; l < libsvm_model->l ; l++)
//		{
//			cout<<"Support Vector : "<<l<<endl;
//			for (unsigned int j =0 ; j < Dimensionality_ ; j++)
//				cout<<"("<<(libsvm_model->SV)[l][j].index<<","<<(libsvm_model->SV)[l][j].value<<")"<<endl;
//		}
// 		cout<<endl;
//		
//		cout<<"SVM predict "<<one_sample.size()<<endl;
//        cout<<"Make prediction for sample :  "<<endl;
//        for (vector<svm_node>::iterator i = one_sample.begin(); i!=one_sample.end(); ++i)
//            cout<<i->index<<","<<i->value<<" ";
//        cout<<endl;
        
		//Still use this for the actual prediction as it will make it easier for working with other kernel, one-class svm , etc...
//        cerr << "size of one sample: " << one_sample.size() << endl;
		cl  = svm_predict(libsvm_model, &one_sample[0]);

//		cout<<"SVM done [predict"<<endl;
        if (Verbose>0)
			cout<<"applied SVM "<<cl<<endl;
//        cout<<"applied SVM "<<cl<<endl;

//        cout << "Classification " << sample << ": " << cl << endl;

		out_test_data->push_back(cl);
		//out_data->push_back(cl);
        
        //cerr << "size of out_test_data: " << out_test_data->size() << endl;
	
	}
    
	
	
	
	
	
	//float cl  = static_cast<float> (svm_predict(libsvm_model, &(vectorToSVMnodes(in_test_data[0])[0]) ));

//    cerr << "at the end" << endl;
    
	//rounds incase cast causes to go slightly less than number
	return static_cast<int>(cl+0.5);
}
	
	//------------------------PRIVATE FUNCTIONS----------------------//
	
	
	vector<svm_node> SVM_mvdisc::vectorToSVMnodes( const vector<float> * vec ) const 
	{
		vector<svm_node> libsvm_vec;
		int count=0;
		
		//create a single node
		for (vector<float>::const_iterator i = vec->begin(); i!=vec->end();i++,count++)
		{
			svm_node temp;

			//set ndoe values
			temp.index=count;
			temp.value=static_cast<double> (*i);
			libsvm_vec.push_back(temp);
			//cout<<"test data "<<temp.index<<" "<<temp.value<<endl;
		}
		//mark end of line
		svm_node temp;
		temp.index=-1;
		temp.value=0.0;
		libsvm_vec.push_back(temp);
		
		return libsvm_vec;
	}
	
	vector<svm_node> SVM_mvdisc::vectorToSVMnodes( const vector<float> & vec ) const 
	{
		vector<svm_node> libsvm_vec;
		int count=0;
		
		//create a single node
		svm_node temp;
		for (vector<float>::const_iterator i = vec.begin(); i!=vec.end();i++,count++)
		{
			//set ndoe values
			temp.index=count;
			temp.value=static_cast<double> (*i);
			libsvm_vec.push_back(temp);
		}
		//mark end of line
		temp.index=-1;
		temp.value=0.0;
		libsvm_vec.push_back(temp);

		return libsvm_vec;
	}

	
	
	void SVM_mvdisc::runTrackStats()
	{
		if (Verbose>0) {
			cout<<"run track stats SVM"<<endl;
		}


		//----------------------------start handling intializatioin----------------------//
        
        if (feature_w_sx_ == NULL)
		{
			N_stats=0;
			feature_w_sx_ = new vector<float>(output_feature_mask_->size() ,0);
		}
        if (feature_w_sxx_ == NULL)
		{
			N_stats=0;
			feature_w_sxx_ = new vector<float>(output_feature_mask_->size() ,0);
		}
        
        if (featureAbs_w_sx_ == NULL)
		{
			N_stats=0;
			featureAbs_w_sx_ = new vector<float>(output_feature_mask_->size() ,0);
		}
        if (featureAbs_w_sxx_ == NULL)
		{
			N_stats=0;
			featureAbs_w_sxx_ = new vector<float>(output_feature_mask_->size() ,0);
		}

        //
//		if (stats_sum_x == NULL)
//		{
//			N_stats=0;
//			stats_sum_x = new vector<float>(output_feature_mask_->size() ,0);
//		}
//		
//		if (stats_sum_xx == NULL)
//		{
//			N_stats=0;
//			stats_sum_xx = new vector<float>(output_feature_mask_->size() ,0);
//		}
        if (stats_sum_N == NULL)
		{
			N_stats=0;
            //	stats_sum_xx = new vector<float>(Dimensionality_,0);
            stats_sum_N = new vector<unsigned int>(output_feature_mask_->size(),0);
            
		}
		//----------------------------done handling intializatioin----------------------//
		vector<float> weights = getParameters();
        //cout<<"weights size "<<weights.size()<<" "<<Dimensionality_<<" "<<stats_sum_xx->size()<<endl;
       
             vector<float>::iterator i_xx =stats_sum_xx->begin();
             vector<bool>::iterator i_mask =output_feature_mask_->begin();
             vector<unsigned int>::iterator i_N =stats_sum_N->begin();
               int count=0;
        vector<float>::iterator i_w = weights.begin();
        
         vector<float>::iterator i_sxx = feature_w_sxx_->begin();
        vector<float>::iterator i_Abs_w_sx = featureAbs_w_sx_->begin();
        vector<float>::iterator i_Abs_w_sxx = featureAbs_w_sxx_->begin();
             for (vector<float>::iterator i_sx= feature_w_sx_->begin(); i_sx != feature_w_sx_->end(); ++i_sx,++i_sxx,++i_Abs_w_sx,++i_Abs_w_sxx, ++i_N, ++i_mask)
             {
                 if ( *i_mask )
                 {
                     *i_sx += *i_w;
                     *i_sxx += (*i_w)*(*i_w); //don't swquare because square is still 1, yes redundant, but simplier for compatibility with other functions
                     *i_Abs_w_sx += abs(*i_w);
                     *i_Abs_w_sxx += abs(*i_w)*abs(*i_w); //don't swquare because square is still 1, yes
                     
                     ++(*i_N);
                     ++i_w;
                     ++count;
                 }
                 
                 
             }
	     //cout<<"found SVM features stats "<<count<<endl;
        // unsigned int Nd = stats_sum_x->size();		
     //   cblas_saxpy(Nd, 1.0,&weights[0], 1, &(stats_sum_x->at(0)), 1);
        //sum square of fstats
        
        
     //   vector<float>::iterator i_w = weights.begin();
       // for (vector<float>::iterator i = stats_sum_xx->begin(); i != stats_sum_xx->end(); ++i, ++i_w)
         //   *i += (*i_w)*(*i_w);

		++N_stats;//keep track  how how many tiems visited
	}
    void SVM_mvdisc::writeStats() const
	{
        cout<<"write stats tree node "<<NodeName<<" "<<output_basename<<endl;
		///check to see if image exists
        vector<uint3>::const_iterator i_im_dims = src_image_dims_->begin();
        vector<float4>::const_iterator i_im_res = src_image_res_->begin();
        vector<string>::const_iterator i_names = src_names_->begin();
        
        unsigned int offset=0;
        
        for (vector<unsigned int>::const_iterator i_dims = src_dims_->begin(); i_dims != src_dims_->end(); ++i_dims,++i_im_res, ++i_im_dims,++i_names)
        {
            cout<<"input info "<<*i_names<<" "<<*i_dims<<" "<<i_im_res->x<<" "<<i_im_res->y<<" "<<i_im_res->z<<" "<<i_im_dims->x<<" "<<i_im_dims->y<<" "<<i_im_dims->z<<endl;
//            if ((i_im_res->x) || (i_im_res->y) ||(i_im_res->z) || \
                        (i_im_dims->x) || (i_im_dims->y) || (i_im_dims->z))
            if ( (i_im_dims->x >0 ) && (i_im_dims->y > 0 ) &&  (i_im_dims->z > 0))
                {
                        
                        if (output_feature_mask_->size() != (i_im_dims->x*i_im_dims->y*i_im_dims->z))
                        {
                            cerr<<"Output feature mask size does not match number of voxels in image"<<endl;
                            throw ClassificationTreeNodeException("Tried to write stats image, but image dimensions do not match Dimensionality_");
                        }
                        
                        vector<float> mean(i_im_dims->x*i_im_dims->y*i_im_dims->z,0);
                        vector<float> variance(i_im_dims->x*i_im_dims->y*i_im_dims->z,0);
                        vector<float> meanAbs(i_im_dims->x*i_im_dims->y*i_im_dims->z,0);
                        vector<float> varianceAbs(i_im_dims->x*i_im_dims->y*i_im_dims->z,0);
                    vector<float> all_n(i_im_dims->x*i_im_dims->y*i_im_dims->z,0);

                    
                        vector<float>::iterator i_var = variance.begin();
                        vector<float>::iterator i_mean = mean.begin();
                        vector<float>::iterator i_varAbs = varianceAbs.begin();
                        vector<float>::iterator i_meanAbs = meanAbs.begin();
                        vector<float>::iterator i_x = feature_w_sx_->begin()+offset;
                        vector<float>::iterator i_xx = feature_w_sxx_->begin()+offset;
                        vector<float>::iterator i_absx = featureAbs_w_sx_->begin()+offset;
                        vector<float>::iterator i_absxx = featureAbs_w_sxx_->begin()+offset;
                        vector<unsigned int>::iterator i_N = stats_sum_N->begin()+offset;
                        vector<bool>::iterator i_mask = output_feature_mask_->begin()+offset;
//                        long unsigned int count=0;
                    
                    for ( unsigned int count = 0 ; count < (*i_dims); ++count,++i_mask, ++i_mean, ++i_var,++i_meanAbs,++i_varAbs,++i_N,++i_x, ++i_xx,++i_absx,++i_absxx)
                        {
//                            cout<<"count "<<count<<" "<<*i_mask<<" "<<*i_N<<endl;
//                            ++count;
//                            if (*i_mask)
                            {
                                if ((*i_N) > 0){
//                                    cout<<"in mask "<<endl;
                                    *i_mean = *i_x / N_stats;
//                                    cout<<"dobe mean"<<endl;

                                    *i_var = ( *i_xx - (*i_x)*(*i_x)/N_stats ) / (N_stats - 1);
//                                    cout<<"dobe var"<<endl;

                                    *i_meanAbs = *i_absx / N_stats;
//                                    cout<<"dobe abs"<<endl;

                                    *i_varAbs = ( *i_absxx - (*i_absx)*(*i_absx)/N_stats ) / (N_stats - 1);
//                                    cout<<"dobe varabs"<<endl;

//                                    count+=*i_x;
//                                    cout<<"mean "<<*i_x<<" "<<N_stats<<" "<<stats_sum_x->size()<<endl;
//                                    *i_mean = *i_x / N_stats;
//                                    *i_var = ( *i_xx - (*i_x)*(*i_x)/N_stats ) / (N_stats - 1);
                          

                                }


                            }
                        }
//                        cout<<"Sum "<<count<<endl;
                        nifti_Writer::write( mean, *i_im_dims, float3(i_im_res->x,i_im_res->y,i_im_res->z),output_basename+"."+NodeName+"."+(*i_names) + ".featureWeights_mean") ;
                        nifti_Writer::write( variance, *i_im_dims, float3(i_im_res->x,i_im_res->y,i_im_res->z),output_basename+"."+NodeName+"."+(*i_names) + ".featureWeights_var") ;
                        nifti_Writer::write( meanAbs, *i_im_dims, float3(i_im_res->x,i_im_res->y,i_im_res->z),output_basename+"."+NodeName+"."+(*i_names) + ".featureImportance_mean") ;
                        nifti_Writer::write( varianceAbs, *i_im_dims, float3(i_im_res->x,i_im_res->y,i_im_res->z),output_basename+"."+NodeName+"."+(*i_names) + ".featureImportance_var") ;
                        
                        
//                        nifti_Writer::write( variance, *i_im_dims, float3(i_im_res->x,i_im_res->y,i_im_res->z),output_basename+"."+(*i_names) + ".stats_N") ;
                    
                        
                        cout<<"write NIFTI output "<<NodeName<<" "<<N_stats<<endl;
                        
                    }else{
                                cout<<"write track stats text file"<<endl;
                        //            vector<float> mean(output_feature_mask_->size(),0);
                        //            vector<float> variance(output_feature_mask_->size(),0);
                        //
                        //
                        //            vector<float>::iterator i_var = variance.begin();
                        //            vector<float>::iterator i_mean = mean.begin();
                        //            vector<float>::iterator i_x = stats_sum_x->begin();
                        //            vector<float>::iterator i_xx = stats_sum_xx->begin();
                        //            vector<unsigned int>::iterator i_N = stats_sum_N->begin();
                        //            //   long unsigned int count=0;
                        //            for (vector<bool>::iterator i_mask = output_feature_mask_->begin(); i_mask != output_feature_mask_->end(); ++i_mean, ++i_var, ++i_mask,++i_N,++i_x, ++i_xx)
                        //            {
                        //                *i_mean = *i_x / N_stats;
                        //                *i_var = ( *i_xx - (*i_x)*(*i_x)/N_stats ) / (N_stats - 1);
                        //            }
                        //
                        //            ofstream f_mean_var((output_basename+"_stats_mean_var.csv").c_str());
                        //            vector<float>::iterator i_m = mean.begin();
                        //            f_mean_var<<*i_m;
                        //            for ( ;i_m != mean.end();++i_m)
                        //            {
                        //                f_mean_var<<" "<<*i_m;
                        //            }
                        //            f_mean_var<<endl;
                        //            vector<float>::iterator i_v = variance.begin();
                        //            f_mean_var<<*i_v;
                        //            for ( ;i_v != variance.end();++i_v)
                        //            {
                        //                f_mean_var<<" "<<*i_v;
                        //            }
                        //            f_mean_var<<endl;
                        //            
                        //            f_mean_var.close();
                        //
                        
                    }
            
            
            
        }
//		if ((stats_image_res.x) || (stats_image_res.y) ||(stats_image_res.z) || \
//			(stats_image_dims.x) || (stats_image_dims.y) || (stats_image_dims.z))
//		{
//			if (output_feature_mask_->size() != (stats_image_dims.x*stats_image_dims.y*stats_image_dims.z))
//				throw ClassificationTreeNodeException("Tried to write stats image, but image dimensions do not match Dimensionality_");
//			
//            vector<float> mean(stats_image_dims.x*stats_image_dims.y*stats_image_dims.z,0);
//            vector<float> variance(stats_image_dims.x*stats_image_dims.y*stats_image_dims.z,0);
//            vector<float> meanAbs(stats_image_dims.x*stats_image_dims.y*stats_image_dims.z,0);
//            vector<float> varianceAbs(stats_image_dims.x*stats_image_dims.y*stats_image_dims.z,0);
//            
//            
//            vector<float>::iterator i_var = variance.begin();
//            vector<float>::iterator i_mean = mean.begin();
//            vector<float>::iterator i_varAbs = varianceAbs.begin();
//            vector<float>::iterator i_meanAbs = meanAbs.begin();
//            vector<float>::iterator i_x = feature_w_sx_->begin();
//            vector<float>::iterator i_xx = feature_w_sxx_->begin();
//            vector<float>::iterator i_absx = featureAbs_w_sx_->begin();
//            vector<float>::iterator i_absxx = featureAbs_w_sxx_->begin();
//            vector<unsigned int>::iterator i_N = stats_sum_N->begin();
//            //   long unsigned int count=0;
//            for (vector<bool>::iterator i_mask = output_feature_mask_->begin(); i_mask != output_feature_mask_->end(); ++i_mean, ++i_var,++i_meanAbs,++i_varAbs, ++i_mask,++i_N,++i_x, ++i_xx,++i_absx,++i_absxx)
//            {
//                // cout<<"count "<<count<<" "<<*i_mask<<endl;
//                //   ++count;
//				//	if (*i_mask)
//				//	{
//                //    if (*i_N==0){//set to zero region where no samples
//                //      *i_mean = 0;
//                //       *i_var = 0;
//                // ++i_x;
//                // ++i_xx;
//                // }else{
//                *i_mean = *i_x / N_stats;
//                *i_var = ( *i_xx - (*i_x)*(*i_x)/N_stats ) / (N_stats - 1);
//                *i_meanAbs = *i_absx / N_stats;
//                *i_varAbs = ( *i_absxx - (*i_absx)*(*i_absx)/N_stats ) / (N_stats - 1);
//                //   count+=*i_x;
//                //       cout<<"mean "<<*i_x<<" "<<N_stats<<" "<<stats_sum_x->size()<<endl;
//                //					*i_mean = *i_x / N_stats;
//                //					*i_var = ( *i_xx - (*i_x)*(*i_x)/N_stats ) / (N_stats - 1);
//                //	++i_x;
//                //	++i_xx;
//                //}
//				//	}
//            }
//			//cout<<"Sum "<<count<<endl;
//			nifti_Writer::write( mean, stats_image_dims, stats_image_res,output_basename+"_featureWeights_mean") ;
//			nifti_Writer::write( variance, stats_image_dims, stats_image_res,output_basename+"_featureWeights_var") ;
//			nifti_Writer::write( meanAbs, stats_image_dims, stats_image_res,output_basename+"_featureImportance_mean") ;
//			nifti_Writer::write( varianceAbs, stats_image_dims, stats_image_res,output_basename+"_featureImportance_var") ;
//            
//            
//            //nifti_Writer::write( variance, stats_image_dims, stats_image_res,output_basename+"_stats_N") ;
//            
//			//static void write(  const std::vector<float> * data, const std::vector<bool> * mask, const uint3 & dims, const float3 & res );
//            
//			//cout<<"write NIFTI output "<<NodeName<<" "<<N_stats<<endl;
//		}else {
//            cout<<"write track stats"<<endl;
//            vector<float> mean(output_feature_mask_->size(),0);
//            vector<float> variance(output_feature_mask_->size(),0);
//            
//            
//            vector<float>::iterator i_var = variance.begin();
//            vector<float>::iterator i_mean = mean.begin();
//            vector<float>::iterator i_x = stats_sum_x->begin();
//            vector<float>::iterator i_xx = stats_sum_xx->begin();
//            vector<unsigned int>::iterator i_N = stats_sum_N->begin();
//            //   long unsigned int count=0;
//            for (vector<bool>::iterator i_mask = output_feature_mask_->begin(); i_mask != output_feature_mask_->end(); ++i_mean, ++i_var, ++i_mask,++i_N,++i_x, ++i_xx)
//            {
//                *i_mean = *i_x / N_stats;
//                *i_var = ( *i_xx - (*i_x)*(*i_x)/N_stats ) / (N_stats - 1);
//            }
//            
//            ofstream f_mean_var((output_basename+"_stats_mean_var.csv").c_str());
//            vector<float>::iterator i_m = mean.begin();
//            f_mean_var<<*i_m;
//            for ( ;i_m != mean.end();++i_m)
//            {
//                f_mean_var<<" "<<*i_m;
//            }
//            f_mean_var<<endl;
//            vector<float>::iterator i_v = variance.begin();
//            f_mean_var<<*i_v;
//            for ( ;i_v != variance.end();++i_v)
//            {
//                f_mean_var<<" "<<*i_v;
//            }
//            f_mean_var<<endl;
//            
//            f_mean_var.close();
//            
//            
//            
//            
//        }
		
		
		
	}

	
}
